<?php
namespace Adm\Config;

class UserGroupConfig {

    /**
     * Array containing labels for our CMS fields
     * @var array
     */
    public static $labels = [
        'name' => 'Name',
        'admin' => 'Administrator',
        'super_admin' => 'Super Admin',
        'permissions' => 'User Permissions',
        'dashboard_permissions' => 'CMS Dashboard Modules'
    ];

    /**
     * Array containing help text for our CMS fields
     * @var array
     */
    public static $help = [
        'name' => 'The name for this User Group.',
        'admin' => 'Toggle whether this group has administrator access.',
        'super_admin' => 'Toggle whether this group has super admin access.',
        'permissions' => 'Select the areas that users in this group can access.',
        'dashboard_permissions' => 'Select modules on the CMS dashboard users in this group can see.'
    ];
}