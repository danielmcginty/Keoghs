<?php
namespace Adm\Config;

class UserAccountConfig {

    /**
     * Array containing labels for our CMS fields
     * @var array
     */
    public static $labels = [
        'admin' => 'Super Admin',
        'user_group_id' => 'User Group',
        'first_name' => 'First Name',
        'last_name' => 'Last Name',
        'email' => 'Email Address',
        'username' => 'Username',
        'password' => 'Password',
        'status' => 'Enabled'
    ];

    /**
     * Array containing help text for our CMS fields
     * @var array
     */
    public static $help = [
        'admin' => 'Toggle whether the user is a \'Super\' admin or not.',
        'user_group_id' => 'The group of users this person belongs to.',
        'first_name' => 'The user\'s first name',
        'last_name' => 'The user\'s last name',
        'email' => 'The user\'s email address',
        'username' => 'The username, used to login. Must be unique.',
        'password' => 'If set, leave blank to keep the password unchanged.',
        'status' => 'Toggle whether the user is allowed access to the CMS'
    ];
}