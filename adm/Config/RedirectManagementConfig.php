<?php
namespace Adm\Config;

class RedirectManagementConfig {

    /**
     * Array containing labels for our CMS fields
     * @var array
     */
    public static $labels = [
        'old_url' => 'Old URL',
        'new_url' => 'New URL'
    ];

    /**
     * Array containing help text for our CMS fields
     * @var array
     */
    public static $help = [
        'old_url' => 'The URL to redirect from',
        'new_url' => 'The URL to redirect to'
    ];
}