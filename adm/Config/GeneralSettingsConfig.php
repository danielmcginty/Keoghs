<?php
namespace Adm\Config;

class GeneralSettingsConfig {

    /**
     * Array containing labels for our CMS fields
     * @var array
     */
    public static $labels = [
        'default_language' => 'Default Language',
        'google_analytics_ua_code' => 'Google Analytics Tracking Code',
        'image' => 'Default Fallback Image'
    ];

    /**
     * Array containing help text for our CMS fields
     * @var array
     */
    public static $help = [
        'default_language' => 'Select the default language for the website',
        'google_analytics_ua_code' => 'Enter the Google Analytics UA Tracking Code for the website',
        'image' => 'Assign the default fallback image to use on the website where an image field is required, but no image is set'
    ];
}