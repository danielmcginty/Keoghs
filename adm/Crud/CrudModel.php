<?php

namespace Adm\Crud;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Adm\Models\PageBuilderBlock;
use Illuminate\Support\Facades\Schema;

/**
 * Class CrudModel
 * The Crud Model allows CRUD functions to be performed on records with a single primary key
 *
 * @package Adm\Crud
 */
class CrudModel extends Model
{
    /**
     * As a standard across the LaraCrud system, we use soft deletes
     */
    use SoftDeletes;

    /**
     * The static instance of this class is used to retain a table and primary key field between
     * new instances of the CrudModel being created
     *
     * @var CrudModel
     */
    private static $instance;

    /**
     * The init function is called before the CrudModel is ever used.
     * This builds the static instance so the table is kept across instances of the model
     *
     * @param $table
     * @param null $columns
     */
    public static function init( $table, $columns = null ){
        self::$instance = new CrudModel();
        self::$instance->table = $table;
        self::$instance->timestamps = true;
        if( $columns ){
            self::$instance->columns = array_keys( $columns );
            if( !in_array( 'created_at', array_keys( $columns ) ) || !in_array( 'updated_at', array_keys( $columns ) ) ){
                self::$instance->timestamps = false;
            }
        }
    }

    /**
     * Constructor
     * If an instance has been created, use the set attributes to populate a new instance of the model,
     * thus keeping things like the table name consistent
     */
    public function __construct(){
        parent::__construct();
        if( self::$instance ){
            $this->table = self::$instance->table;
            $this->fillable = self::$instance->columns;
            $this->timestamps = self::$instance->timestamps;
        }
    }

    /**
     * Sets the primary key field for the model, also saving into the instance
     *
     * @param $pk
     */
    public function setPrimaryKey( $pk ){
        $this->primaryKey = $pk;
        self::$instance->primaryKey = $pk;
    }

    /**
     * Custom 'accessor' function (https://laravel.com/docs/5.3/eloquent-mutators#defining-an-accessor) for
     * page builder blocks.
     * Provides an easy way of getting the page builder blocks assigned to an item through a simple object property,
     * e.g. $object->page_builder
     *
     * @return array
     */
    public function getPageBuilderAttribute(){
        if( isset( $this->attributes['page_builder'] ) ){
            return $this->attributes['page_builder'];
        }

        $pk_field = $this->primaryKey;
        if( is_array( $this->primaryKey ) ){
            foreach( $this->primaryKey as $field ){
                if( $field != "language_id" && $field != "version" ){
                    $pk_field = $field;
                }
            }
        }

        $blocks = PageBuilderBlock::where([
            'table_name' => $this->table,
            'table_key' => $this->{$pk_field},
            'language_id' => $this->language_id,
            'version' => $this->version
        ])->get();

        $page_builder_arr = [];
        foreach( $blocks as $block ){
            $page_builder_arr[ $block->sort_order ] = [
                'reference' => $block->reference,
                'content' => $block->content
            ];
        }

        $this->attributes['page_builder'] = $page_builder_arr;
        return $page_builder_arr;
    }

    /**
     * Custom Mutator Function.
     * As the 'page_builder' attribute is a Collection of Eloquent Models, we need a customer override function,
     * otherwise we'll get an Overloaded Property Exception.
     *
     * @param $page_builder
     */
    public function setPageBuilderAttribute( $page_builder ){
        $this->attributes['page_builder'] = $page_builder;
    }

    /**
     * Override the softDelete function so that it doesn't fall over with a model with no 'deleted_at' timestamp
     *
     * @return void
     */
    protected function runSoftDelete(){
        $query = $this->newQueryWithoutScopes()->where($this->getKeyName(), $this->getKey());

        if( Schema::hasColumn( $this->getTable(), $this->getDeletedAtColumn() ) ) {
            $this->{$this->getDeletedAtColumn()} = $time = $this->freshTimestamp();

            $query->update([$this->getDeletedAtColumn() => $this->fromDateTime($time)]);
        } else {
            $query->delete();
        }
    }
}
