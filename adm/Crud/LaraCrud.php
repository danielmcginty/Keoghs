<?php
/**
 * Created by PhpStorm.
 * User: santo
 * Date: 06/08/2015
 * Time: 08:51
 */

namespace Adm\Crud;

use Adm\Crud\Traits\CustomValidation;
use Adm\Crud\Traits\Pagination;
use Adm\Library\Cloudflare\Cloudflare;
use App\Library\MyBespokeApi;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Cache;

class LaraCrud
{
    use Pagination;
    use CustomValidation;

    /**
     * Internal variable defining the columns set against the current subject.
     *
     * @var array
     */
    protected $columns = [];

    /**
     * Internal variable defining the field types for the current columns.
     *
     * @var array
     */
    protected $ColumnType = [];

    /**
     * Adds new columns to the item, additional to the database columns.
     *
     * @var array
     */
    private $newColumns = [];

    /**
     * Stores the columns displayed on the list view.
     *
     * @var array
     */
    private $listingColumns = [];

    /**
     * Stores an explicit set of columns to display on the list view.
     *
     * @var array
     */
    private $visibleColumns = [];

    /**
     * Which columns shouldn't we show on the list view?
     *
     * @var array
     */
    private $listingColumnsExclude = [];

    /**
     * Which columns shouldn't we show on the form view?
     *
     * @var array
     */
    private $editColumnsExclude = [];

    /**
     * Store custom ordering of columns on the list view.
     *
     * @var array
     */
    private $columnOrdering = [];

    /**
     * Store a set of field to an array of fields to hide, and the condition in which to show them, for forms.
     *
     * @var array
     */
    private $toggleFields = [];

    /**
     * Internal variable used to set which column types on the list view are orderable
     *
     * @var array
     */
    private $sortableColumnTypes = [
        'string', 'integer', 'boolean', 'relation', 'date', 'datetime', 'path', 'text', 'time'
    ];

    /**
     * Internal variable used to set a default sort column on the list view.
     * If left to false, the system won't apply a sort until the user clicks a column.
     *
     * @var bool|string
     */
    private $defaultSort = false;

    /**
     * Internal variable used to set a default sort order on the list view.
     * Defaults to 'ASC'
     *
     * @var string
     */
    private $defaultSortDirection = 'ASC';

    /**
     * Internal variable used to store table aliases used for SQL joins across functions
     *
     * @var array
     */
    private $joinAliases = [];

    /**
     * Stores groups of fields to display on the form view.
     * Associative array of Group Name => $fields, where fields is an array of
     * field => random value for array key
     *
     * N.B. This isn't restricted to groups of 'main', 'side' and 'seo'.
     * Custom groups can be added via the LaraCrudApi->columnLocation() or LaraCrudApi->columnGroup()
     * functions.
     *
     * @var array
     */
    private $formFieldGroups = [
        'main' => [
            'title' => 1,
            'content' => 1,
            'path' => 1,
        ],
        'side' => [
            'status' => 1,
            'sort_order' => 1,
        ],
        'seo' => [
            'seo_options' => 1
        ]
    ];

    /**
     * The current database table being used
     *
     * @var string
     */
    public $table;

    /**
     * The current URL 'slug' used to reference the CRUD function.
     * Stored as a separate variable because the function may not have the same name as
     * the table.
     *
     * @var string
     */
    public $slug;

    /**
     * The primaryColumn can either be a string or array, storing
     * the primary key fields within the table.
     *
     * E.g. 'id', 'page_id', [ 'page_id', 'language_id' ]
     *
     * @var string|array
     */
    protected $primaryColumn;

    /**
     * This is an internal flag field indicating whether the current
     * item being edited has a primary key field that isn't an
     * auto increment field.
     *
     * It is used to generate a new ID for the record being inserted if true
     * because the database can't automatically assign a new one
     *
     * @var bool
     */
    protected $primaryColumnNoAutoIncrement;

    /**
     * The displayColumn variable is used by the system to determine which field to
     * pull from the current item to display as it's title
     *
     * @var string
     */
    protected $displayColumn = 'title';

    /**
     * The linkedField variable is used by the system to determine which field
     * (if available) to add an edit link to in addition to the normal Edit
     * button
     *
     * @var string
     */
    protected $linkedField = 'title';

    /**
     * The urlGenerationField variable is used by the system to
     * determine which field (if available) to use to generate the URL path
     * from when adding a new item
     *
     * @var string
     */
    public $urlGenerationField              = 'title';

    /**
     * The metaTitleGenerationField variable is used by the system to
     * determine which field (if available) to use to generate the automatic
     * meta title when adding or updating an item
     *
     * @var string
     */
    public $metaTitleGenerationField        = 'title';

    /**
     * The metaDescriptionGenerationField variable is used by the system to
     * determine which field (if available) to use to generate the automatic
     * meta description when adding or updating an item
     *
     * @var string
     */
    public $metaDescriptionGenerationField  = '';

    /**
     * Another internal flag indicating if the current item has a
     * 'language_id' column in the table
     *
     * @var bool
     */
    protected $translatable;

    /**
     * An array storing the relational fields for the item,
     * for example, categories, tags, authors, etc.
     *
     * @var array
     */
    public $relations = [];

    /**
     * An array storing a set of relational items to exclude from a relational field,
     * for example, system pages from a relation page parent
     *
     * @var array
     */
    public $relationExcludes = [];

    /**
     * If not empty, this view template is loaded in rather than performing
     * CRUD functionality
     *
     * @var string
     */
    private $view;

    /**
     * If not empty, this view template is loaded in rather than the default list template
     *
     * @var string
     */
    private $list_view;

    /**
     * Stores variables to pass down to the custom template stored in $this->view
     *
     * @var array
     */
    private $viewParameters;

    /**
     * This is simply the current item being edited/deleted
     *
     * @var null|mixed
     */
    public $currentItem = null;

    /**
     * An array storing the multi-relational fields for the item,
     * for example news articles to news categories
     *
     * @var array
     */
    public $multiRelations = [];

    /**
     * Existing variable being used to store image gallery fields.
     * Will be modified when the new asset manager is built
     *
     * @var array
     */
    public $imageGalleryFields = [];

    /**
     * Internal flag indicating whether custom blocks (page builder blocks) are enabled
     *
     * @var bool
     */
    public $customBlock = false;

    /**
     * Stores custom labels for fields and columns.
     *
     * @var array
     */
    public $displayAs = [];

    /**
     * Stores the field 'help' text
     *
     * @var array
     */
    public $helpText = [];

    /**
     * Internal variable storing options for certain column types
     * (e.g. select, radio, checkboxes)
     *
     * @var array
     */
    public $fieldOptions = [];

    /**
     * Internal variable storing any extra data for columns that is custom for particular field types.
     *
     * @var array
     */
    public $extraData = [];

    /**
     * Internal variable storing any additional attribute for CMS form fields.
     *
     * @var array
     */
    public $fieldAttributes = [];

    /**
     * Array containing default values for CMS form fields
     *
     * @var array
     */
    public $fieldDefaultValues = [];

    /**
     * Internal flag indicating whether system page fields (controller & method) are enabled
     *
     * @var bool
     */
    public $systemPageEnabled = false;

    /**
     * Which controller called me?
     * @var \Adm\Controllers\AdmController
     */
    public $caller;

    /**
     * Boolean toggle to determine whether the page builder form section
     * is enabled or not.
     *
     * @var bool
     */
    public $pageBuilderEnabled = false;

    /**
     * Boolean toggle to determine whether the page builder 'Main Content'
     * block is required and present.
     *
     * @var bool
     */
    public $pageBuilderNeedsMainContent = false;

    /**
     * Array containing any custom dynamic scripts for page builder blocks
     *
     * @var array
     */
    public $pageBuilderDynamicScripts = [];

    /**
     * Which CRUD actions are available?
     *
     * @var array
     */
    private $actions = [
        'create',
        'edit',
        'destroy',
        'listing',
        'store',
        'update',
        'copy',
        'show',
    ];

    /**
     * Are any additional 'bulk' actions available?
     * Array containing any extra actions that can be performed on the list view
     *
     * Structure of each action:
     * [
     *      'url'       => 'AJAX URL to Call',
     *      'text'      => 'Text for Button',
     *      'colour'    => 'CSS Button Colour Class'
     * ]
     *
     * @var array
     */
    private $extra_bulk_actions = [];

    /**
     * Internal flag indicating whether the current CRUD function should
     * display a single record rather than the default of allowing lists
     *
     * @var bool
     */
    private $isSingle = false;

    /**
     * This denotes the current 'subject' of CRUD, i.e. a readable version
     * of the item being manipulated.
     * E.g. Page, News Article, Author
     *
     * @var string
     */
    private $subject;

    /**
     * Because we have tables that can have EITHER:
     * a single primary key field OR A composite primary key
     * we need to keep a reference to the model being used so
     * that it can be switched if a composite key is required.
     *
     * This will either be CrudModel or CompositeCrudModel
     *
     * @var string
     */
    private static $crudModelReference = 'CrudModel';

    /**
     * Is the asset manager going to be used on the form?
     * Controls whether the modal is loaded in or not - set when using the Types\AssetType field
     *
     * @var bool
     */
    public $assetManagerEnabled = false;

    /**
     * Is TinyMCE going to be used on the form?
     * Controls whether the JS script is loaded in or not - set when using the Types\TextType field
     *
     * @var bool
     */
    public $wysiwygEnabled = false;

    /**
     * Do we have any extra information to send down to the view?
     * If so, it can be passed into here with $this->crud->extra_view_data[ 'key' ] => $value
     *
     * @var array
     */
    public $extra_view_data = [];

    /**
     * Stores the default text for buttons on the listing page.
     * Allows customisation of button text.
     *
     * @var array
     */
    private $listing_button_texts = [
        'edit' => 'Edit',
        'copy' => 'Copy'
    ];

    /**
     * Internal Variable Flag setting whether a CrudException has already been built for the error.
     * If so, the system doesn't try to built any more Exceptions, which can create an infinite loop while trying to
     * generate data to pass to the view.
     *
     * @var bool
     */
    private $errorFlagged = false;

    /**
     * Internal variable storing custom 'where' conditions to use when generating item listings.
     * Stored as $field => array( $value => $operator ).
     *
     * @var array
     */
    private $whereConditions = [];

    /**
     * Internal variable storing GET parameters for list views, used in conjunction with Cookie based storage.
     *
     * @var array
     */
    private $list_parameters = [];

    /**
     * Array containing any custom dynamic scripts we might need on list and form templates
     *
     * @var array
     */
    private $dynamic_scripts = [];

    /**
     * Array containing details of the current item's path prefix settings
     *
     * @var array
     */
    public $pathPrefix = [];

    /**
     * Array containing details of 'base' path prefixes, used to allow
     * other resources to be set as a 'base' URL section
     *
     * @var array
     */
    public $basePathPrefix = [];

    /**
     * If the $subject variable is set return that, else try to make a readable version from
     * the current table name
     *
     * @return string
     */
    protected function getSubject(){
        return $this->subject ? $this->subject : ucwords(implode(" ", explode("_", $this->getTable())));
    }

    /**
     * Build the LaraCrud instance, setting the HTTP Request variable, parameters obtained from
     * the route and the current language
     *
     * @param Request $request
     */
    public function __construct( Request $request ){
        App::singleton(
            \Illuminate\Contracts\Debug\ExceptionHandler::class,
            \Adm\Exceptions\Handler::class
        );

        $this->request = $request;
        $this->parameters = $this->request->route()->parameters();

        $this->parameters['url_table'] = isset($this->parameters['table']) ? $this->parameters['table'] : '';
        $this->parameters['table'] = str_replace( '-', '_', $this->parameters['url_table'] );

        // Set the language
        // If we have a language $_GET parameter, use that IF we're also searching
        if( $this->request->input('language') && ($this->request->input('search') || $this->request->input('search-by')) ){
            $this->language_id = $this->_getLanguageIdByLocale( $this->request->input('language') );
        }
        // Else, if the 'locale' is set through the route, use that
        elseif( isset($this->parameters['locale']) ){
            $this->language_id = $this->_getLanguageIdByLocale( $this->parameters['locale'] );
        }
        // Else, use the default language
        else {
            $this->language_id = $this->getDefaultLanguage();
        }
    }

    /**
     * Throws a custom exception
     *
     * @param string $message
     * @throws CrudExceptions
     */
    protected function crudError( $message ){
        if( is_null( $this->caller ) ){
            $this->caller = new \Adm\Controllers\AdmController();
        }

        // If we haven't already flagged an error, we can proceed building the exception
        if( !$this->errorFlagged ) {
            $this->errorFlagged = true;

            $exception = new CrudExceptions(500, $message, null, array(), 404);
            $exception->extra_data = $this->defaultData(array());

            throw $exception;
        }
    }

    /**
     * Initialized the CRUD instance, setting default field types and calling the relevant function
     * if available
     *
     * @param \Adm\Controllers\AdmController $caller
     */
    public function init( $caller ){
        $this->caller = $caller;
        $this->setSlug();
        $this->setDefaultCallback();
        $this->getCallerMethod();
    }

    /**
     * In certain cases, we need to pass a custom 'caller' to the CRUD instance to correctly load resources.
     *
     * @param \Adm\Controllers\AdmController $caller
     */
    public function setCaller( $caller ){
        $this->caller = $caller;
    }

    /**
     * Sets any defaults for the CRUD system, such as field types, help text and labels
     */
    public function setDefaultCallback(){
        $this->changeType( 'password', 'password' );
        $this->changeType( 'image', 'asset' );
        $this->changeType( 'mobile_image', 'asset' );
        $this->changeType( 'file', 'file' );
        $this->changeType( 'sort_order', 'number' );
        $this->changeType( 'colour', 'colour' );
        $this->changeType( 'meta_tags', 'comma_separated' );
        $this->changeType( 'meta_description', 'textarea' );

        $this->changeType( 'path', 'path' );
        $this->displayAs( 'path', 'URL Path' );

        $this->setRelation( 'language_id', 'languages', 'title' );
        $this->displayAs( 'language_id', 'Language' );

        // If our called (adm or module) has a getTableNameDefaultLabels function, auto-populate the field labels for
        // that table name
        $table_specific_method = Str::camel( 'get_' . $this->slug . '_default_labels' );
        if( method_exists( $this->caller, $table_specific_method ) ){
            $labels = $this->caller->{ $table_specific_method }();

            foreach( $labels as $field => $label ){
                $this->displayAs( $field, $label );
            }
        }

        // If our caller (adm or module) has a getDefaultLabels function, auto-populate the field labels
        if( method_exists( $this->caller, 'getDefaultLabels') ){
            $labels = $this->caller->getDefaultLabels();

            foreach( $labels as $field => $label ){
                $this->displayAs( $field, $label );
            }
        }

        // If our called (adm or module) has a getTableNameDefaultHelp function, auto-populate the field labels for
        // that table name
        $table_specific_method = Str::camel( 'get_' . $this->slug . '_default_help' );
        if( method_exists( $this->caller, $table_specific_method ) ){
            $labels = $this->caller->{ $table_specific_method }();

            foreach( $labels as $field => $label ){
                $this->help( $field, $label );
            }
        }

        // If our caller (adm or module) has a getDefaultHelp function, auto-populate the field help text
        if( method_exists( $this->caller, 'getDefaultHelp') ){
            $helps = $this->caller->getDefaultHelp();

            foreach( $helps as $field => $help ){
                $this->help( $field, $help );
            }
        }
    }

    /**
     * Function called within boot() (so we have a subject) that sets default help text for various fields
     * This merges with any custom help text set so we have a much more efficient set-up.
     * I.e. we don't have to define help text for each and every field being displayed
     */
    private function _setDefaultHelpTexts(){
        $subject = strtolower( Str::singular( $this->getSubject() ) );

        $default_help = [
            'title'         => sprintf('The title of this %s. Used in links and meta data', $subject ),
            'h1_title'      => 'Used for the H1 tag on the website, but the title field will be used if empty',
            'path'          => 'Use only hyphens, lowercase letters and numbers (no spaces)',
            'content'       => sprintf('The main %s content', $subject),
            'description'   => sprintf('The main %s content', $subject),
            'parent'        => sprintf('The parent %s is used in this %s\'s url, e.g. /parent/this-url', $subject, $subject),
            'image'         => sprintf('The main %s image', $subject),
            'status'        => sprintf('Toggle whether this %s is published (can be accessed)', $subject),
            'sort_order'    => sprintf('The order this %s is shown', $subject),
            'date'          => sprintf('The date of this %s', $subject),
            'publish_date'  => sprintf('The date this %s will be published on', $subject)
        ];

        foreach( $this->columns as $ind => $column ){
            if( isset( $column['help'] ) && empty( $column['help'] ) && isset( $default_help[ $column['field'] ] ) ){
                $this->columns[ $ind ]['help'] = $default_help[ $column['field'] ];
            }
        }

        $default_types_help = [
            'select'            => 'Select a %s from the list',
            'searchable_select' => 'Select a %s from the list, you can filter the list by typing into the box when it opens',
            'multiple_select'   => 'Select multiple %1$s from the list. You can click the selected %1$s to remove them',
            'boolean'           => 'Toggle whether to %s',
            'comma_separated'   => 'Type a %1$s and press &#x23ce; to add it. You can also click the selected %1$s to remove them',
            'relation'          => 'Select a %s from the list',
            'multirelation'     => 'Click the + icon to move %s you want to assign from the left to the right',
            'number'            => 'Either type a number or click the up and down arrows if you can see them',
            'radio'             => 'Select a %s from the list',
            'checkbox'          => 'Select one or more %s from the list',
            'date'              => 'Click to open a date picker, or type a date in (dd/mm/yyyy)',
            'datetime'          => 'Click to open a date and time picker. Or type a date (dd/mm/yyyy) and time (hh:mm)',
            'time'              => 'Click to open a time picker, or type a time in (hh:mm)',
            'colour'            => 'Click to open a colour picker',
            'icon'              => 'Click to open an icon picker',
            'asset'             => 'Click to open the asset manager',
            'video'             => 'Add a full YouTube or Vimeo video URL',
            'table'             => 'Add rows to build the table, or drag <i class="fi-adm fi-adm-drag-handle font-size-12 colour-slate relative" style="top: 2px;"></i> to re-order the rows',
            'form'              => 'Add fields to build the form, or drag <i class="fi-adm fi-adm-drag-handle font-size-12 colour-slate relative" style="top: 2px;"></i> to re-order the fields',
            'permission'        => 'Select areas this user group can access.',
        ];

        foreach( $this->columns as $ind => $column ) {
            if( isset( $column['help'] ) && empty( $column['help'] ) && isset( $default_types_help[ $column['type'] ] ) ){
                $this->columns[ $ind ]['help'] = sprintf( $default_types_help[ $column['type'] ], $column['label'] );
            }
        }
    }

    /**
     * Set the current $slug for the CMS, based on URL params
     */
    protected function setSlug(){
        $this->slug = $this->parameters['url_table'];
    }

    /**
     * If the current function is available within the controller that called this,
     * call that function
     */
    protected function getCallerMethod(){
        if( $this->getTable() ){
            if( method_exists($this->caller, $this->getTable()) ){
                $this->caller->{$this->getTable()}();
            }
        }
    }

    /**
     * Check User Permission
     * When a request is made to the admin system, make sure that the logged in user can access the area requested.
     * If not, set a flash message stating they don't have access, and redirect them to the first available area they
     * can access.
     *
     * @return bool|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function checkUserPermissions(){
        $navigation = $this->caller->generateNavigation();

        $nav_urls = [];
        foreach( Arr::flatten( $navigation ) as $item ){
            if( stripos( $item, 'adm/' ) === 0 ){
                $nav_urls[] = $item;
            }
        }

        $valid_url = Auth::user()->canAccess( url()->current() );

        if( !$valid_url ){
            $first_url = reset( $nav_urls );

            Session::flash('error', 'Sorry but you don\'t have the sufficient privileges to access that.');
            return redirect( route('adm.path', [str_replace('adm/', '', $first_url)]) );
        }

        return false;
    }

    /**
     * 'Boots' the CRUD instance,
     * Returns a custom template if set, or shows the dashboard if the function isn't set or doesn't exist.
     * If neither, generate the columns for the subject, create the CrudModel instance, and call the
     * appropriate function
     *
     * @return View
     */
    public function boot(){
        $needs_redirect = $this->checkUserPermissions();
        if( $needs_redirect !== false ){
            return $needs_redirect;
        }

        $locked_modules = config( 'modules.locked' );
        if( !empty( $locked_modules ) ){
            $caller_class = get_class( $this->caller );
            preg_match( '/Modules\\\\([^\\\\]*)\\\\Adm.*/i', $caller_class, $matches );
            if( isset( $matches[1] ) && in_array( $matches[1], $locked_modules ) ){
                Session::flash('error', 'Sorry but the ' . $this->getSubject() . ' area of the CMS is currently frozen from editing.');
                return redirect( route('adm.path') );
            }
        }

        if( $this->view ){
            return view( $this->view, array_merge( $this->defaultData( [] ), $this->viewParameters ) );
        }

        if( !$this->checkTable() ){
            $dashboard_data = $this->caller->dashboard_data();
            if( !is_array( $dashboard_data ) ){
                if( get_class( $dashboard_data ) == 'Illuminate\\Http\\RedirectResponse' ){
                    return $dashboard_data;
                }
            }

            return view( $this->caller->dashboard, array_merge( $this->defaultData(array()), $this->caller->dashboard_data()) );
        }

        $this->initColumns();
        $this->initModel();
        $this->_setDefaultHelpTexts();

        return $this->process();
    }

    /**
     * Custom 'Boot' function for the CRUD instance.
     * Instead of performing a full 'Boot' as above, simply boots up the columns and models for use in an AJAX call.
     *
     * @param bool $with_caller_method
     * @return bool
     */
    public function ajax_boot( $with_caller_method = false ){
        if( $with_caller_method ){
            $this->getCallerMethod();
        }

        $this->initColumns();
        $this->initModel();

        return true;
    }

    /**
     * Either call the relevant function for the current CRUD action, or return a
     * CRUD Exception if not available
     *
     * @return mixed
     * @throws CrudExceptions
     */
    private function process(){
        $action = $this->getAction();

        if( method_exists( $this, Str::camel( $action . "_action" ) ) ){
            return $this->{Str::camel( $action . "_action" )}();
        }

        if( $action instanceof \Illuminate\Http\RedirectResponse ){ return $action; }

        $this->crudError('The <strong>' . $action . '</strong> action does not exist!');
        return null;
    }

    /**
     * Generate the column set for the current subject, and other database relevant fields.
     * For example, the primary key set, whether the table is translatable, custom validation rules,
     * and set the column order for the list view
     */
    protected function initColumns(){
        $this->columns = [];
        $has_deleted_at = false;
        foreach( Schema::getColumnListing( $this->getTable() ) as $column ){
            if( $column == 'deleted_at' ){
                $has_deleted_at = true;
            }

            /** @var \Doctrine\DBAL\Schema\Column $col */
            $col = DB::connection()->getDoctrineColumn( $this->getTable(), $column );
            $this->columns[$column] = [
                'field'             => $column,
                'auto_increment'    => (int)$col->getAutoincrement(),
                'label'             => !empty($this->displayAs[$column]) ? $this->displayAs[$column] : ucwords(implode(" ", explode("_", $column))),
                'required'          => ($col->getNotnull() && $col->getDefault() == null),
                'type'              => !empty($this->ColumnType[$column]) ? $this->ColumnType[$column] : (empty($this->relations[$column]) ? $col->getType()->getName() : 'relation'),
                'options'           => isset($this->fieldOptions[$column]) ? $this->fieldOptions[$column] : false,
                'length'            => ($col->getType()->getName() == 'integer' || $col->getType()->getName() == 'boolean' ) ? $col->getPrecision() : $col->getLength(),
                'help'              => !empty($this->helpText[$column]) ? $this->helpText[$column] : '',
                'orderable'         => false,
                'extra_data'        => isset($this->extraData[$column]) ? $this->extraData[$column] : [],
                'attributes'        => isset($this->fieldAttributes[$column]) ? $this->fieldAttributes[$column] : []
            ];

            if( $column == 'password' && $this->getAction() == 'edit' ){
                $this->columns[ $column ]['required'] = false;
            }

            if( !empty($this->pathPrefix) && $column == $this->pathPrefix['field'] ){
                $this->columns[ $column ]['path_prefix_field'] = true;
                $this->columns[ $column ]['path_prefix_table'] = $this->pathPrefix['table'];
                $this->columns[ $column ]['path_prefix_column'] = $this->pathPrefix['column'];
            } else {
                $this->columns[ $column ]['path_prefix_field'] = false;
            }

            if( $col->getAutoincrement() ){
                $this->primaryColumn = $column;
            }

            if( $column == 'language_id' ){
                $this->translatable = true;
            }

            $validation_rules = [];
            if( $this->columns[ $column ]['required'] && $column != 'password' ){
                $validation_rules[] = 'required';
            }

            if( $col->getType()->getName() == 'integer' || $col->getType()->getName() == 'boolean' ){
                $validation_rules[] = 'numeric';
            }

            if( !in_array( $col->getType()->getName(), ['date', 'datetime', 'time', 'integer'] ) && $this->columns[ $column ]['length'] ){
                $validation_rules[] = 'max:' . $this->columns[ $column ]['length'];
            }

            $this->columns[ $column ]['validation'] = implode( '|', $validation_rules );

            if( !empty( $this->newColumns[ $column ] ) ){
                $this->insertColumn( $this->newColumns[ $column ] );
            }
        }

        if( !$this->primaryColumn ){
            $this->primaryColumn = DB::connection()->getDoctrineSchemaManager()->listTableDetails( $this->getTable() )->getPrimaryKeyColumns();
            $this->primaryColumnNoAutoIncrement = true;

            self::$crudModelReference = 'CompositeCrudModel';
        }

        if( !$has_deleted_at ){
            self::$crudModelReference = 'NonDeletableCrudModel';
        }

        foreach( $this->newColumns as $column ){
            $this->insertColumn( $column );
        }

        if( isset( $this->columnOrdering ) && !empty( $this->columnOrdering ) && !isset( $this->parameters['action'] ) ){
            $this->_orderColumns();
        }
    }

    /**
     * Add a column to the internal column set
     *
     * @param string $column
     */
    private function insertColumn( $column ){
        $this->columns[ $column ] = [
            'field'             => $column,
            'auto_increment'    => false,
            'label'             => !empty($this->displayAs[$column]) ? $this->displayAs[$column] : ucwords(implode(" ", explode("_", $column))),
            'required'          => false,
            'type'              => !empty($this->ColumnType[$column]) ? $this->ColumnType[$column] : 'string',
            'options'           => isset($this->fieldOptions[$column]) ? $this->fieldOptions[$column] : false,
            'length'            => null,
            'validation'        => '',
            'path_prefix_field' => !empty($this->ColumnType[$column]) && $this->ColumnType[ $column ] == 'relation' && !empty($this->pathPrefix) && $column == $this->pathPrefix['field'],
            'help'              => !empty($this->helpText[$column]) ? $this->helpText[$column] : '',
            'orderable'         => false,
            'extra_data'        => isset($this->extraData[$column]) ? $this->extraData[$column] : [],
            'attributes'        => isset($this->fieldAttributes[$column]) ? $this->fieldAttributes[$column] : []
        ];

        if( !empty( $this->ColumnType[ $column ] ) && $this->ColumnType[ $column ] == 'path' ){
            $PathField = new \Adm\Crud\Fields\Path( $this );

            $prefix = $base_prefix = '';
            if( !empty( $this->basePathPrefix ) ){
                $base_prefix = $PathField->getPathPrefix( $this->basePathPrefix['table'], $this->basePathPrefix['field'], $this->basePathPrefix['column'] );
            }

            if( !empty( $this->pathPrefix ) && isset( $this->parameters['action'] ) && $this->parameters['action'] == "edit" ){
                $prefix = $PathField->getPathPrefixForItem( $this->pathPrefix['table'], $this->pathPrefix['field'], $this->pathPrefix['column'], $this->getTable(), $this->parameters['id'] );
            }
            $this->columns[ $column ]['path_prefix'] = $base_prefix . str_replace( $base_prefix, '', $prefix );

            $this->columns[ $column ]['system_page_enabled'] = $this->systemPageEnabled;

            $this->ColumnType['seo_options'] = 'seo_options';
            $this->insertColumn('seo_options');
        }
    }

    /**
     * Set custom column ordering for the list view
     */
    private function _orderColumns(){
        $orderedColumns = array();
        foreach( $this->columnOrdering as $column ){
            if( isset( $this->columns[ $column ] ) ){
                $orderedColumns[] = $this->columns[ $column ];
            }
        }

        $this->columns = $orderedColumns;
    }

    /**
     * Generate the CrudModel instance for the subject, based on whether it
     * has a composite key or not
     */
    public function initModel(){
        /** @var \Adm\Crud\CrudModel $crud_class */
        $crud_class = $this->_getCrudModel();
        $crud_class::init( $this->getTable(), $this->columns );
    }

    /**
     * Simply checks whether the table exists, and if not throws a CRUD Exception
     *
     * @return bool
     * @throws CrudExceptions
     */
    protected function checkTable(){
        if( empty( $this->getTable() ) ){
            return false;
        }

        if( !method_exists( $this->caller, $this->getTable() ) && !method_exists( $this->caller, str_replace( '-', '_', $this->slug ) ) ){
            if( !method_exists( $this->caller, $this->getTable() ) ) {
                $this->crudError( 'An action cannot be found for <strong>' . $this->getTable() . '</strong>' );
            }
            if( !method_exists( $this->caller, str_replace( '-', '_', $this->slug ) ) ){
                $this->crudError( 'An action cannot be found for <strong>' . str_replace( '-', '_', $this->slug ) . '</strong>' );
            }
        }

        if( !$this->tableExists( $this->getTable() ) ){
            $this->crudError( 'The table <strong>'.$this->getTable().'</strong> does not exist.' );
        }

        return true;
    }

    /**
     * Does the table exist?
     *
     * @param string $table
     * @return bool
     */
    private function tableExists( $table ){
        return Schema::hasTable( $table );
    }

    /**
     * Does the column exist?
     *
     * @param string $table
     * @param string $column
     * @return bool
     */
    private function columnExists( $table, $column ){
        return in_array( $column, Schema::getColumnListing( $table ) );
    }

    /**
     * Gets the current CRUD action (create, edit, copy, listing, etc)
     * If the current requested action doesn't exist, check whether we are tring to view
     * a 'single' item. If so, then simulate an edit view.
     *
     * @return mixed
     * @throws CrudExceptions
     */
    public function getAction(){
        $crud_class = $this->_getCrudModel();

        $this->parameters['action'] = empty($this->parameters['action']) ? 'listing' : $this->parameters['action'];
        if( !in_array( $this->parameters['action'], $this->actions ) ){
            if( $this->isSingle && ( in_array( 'edit', $this->actions ) || in_array( 'create', $this->actions ) ) ){
                Session::reflash();

                $redirect_params = [$this->slug];

                if( gettype( $this->isSingle ) == "integer" ){
                    $this->parameters['id'] = $this->isSingle;

                    if( $this->parameters['action'] == 'create' && isset( $this->parameters['locale'] ) && !$this->request->isMethod( 'post' ) ){
                        $item = $this->getDefaultItem();
                        $redirect_params = [$this->slug, 'create', $item->{$this->getPrimaryColumn()}, $this->parameters['locale']];
                    } elseif( $this->parameters['action'] == 'edit' ) {
                        $item = $this->getItem();
                        $redirect_params = [$this->slug, 'edit', $item->{$this->getPrimaryColumn()}];
                    }

                    if( empty( $item ) ) {
                        $item = $crud_class::where([
                            $this->getPrimaryColumn() => $this->parameters['id']
                        ])->first();
                        $redirect_params = [$this->slug, 'edit', $item->{$this->getPrimaryColumn()}];
                    }
                } else {
                    if( empty( $item ) ) {
                        $item = $crud_class::get()->first();
                        $redirect_params = [$this->slug, 'edit', $item->{$this->getPrimaryColumn()}];
                    }
                }

                if( !empty( $item ) ){
                    return response()->redirectToRoute( 'adm.path', $redirect_params );
                }

                DB::table( $this->getTable())->insert( [$this->primaryColumn => $this->isSingle] );
                return $this->getAction();
            }

            $this->crudError( 'The <strong>'.$this->parameters['action'].'</strong> action has been disabled!' );
        }

        return $this->parameters['action'];
    }

    /**
     * Get the table name for the current subject
     *
     * @return string|null
     */
    public function getTable(){
        return !empty( $this->table ) ? $this->table : ( !empty( $this->parameters['table'] ) ? $this->parameters['table'] : null );
    }

    /**
     * Allow actions to be unset on a subject by subject basis
     *
     * @param string $action
     */
    private function unsetAction( $action ){
        if( in_array( $action, $this->actions ) !== false ){
            unset( $this->actions[ array_search( $action, $this->actions ) ] );
            $this->actions = array_values( $this->actions );
        }
    }

    /**
     * Allow actions to be set on a subject by subject basis
     *
     * @param string $action
     */
    private function setAction( $action ){
        if( !in_array( $action, $this->actions ) ){
            $this->actions[] = $action;
            $this->actions = array_values( $this->actions );
        }
    }

    /**
     * Generate the columns to be displayed on the list view
     *
     * @return array
     */
    private function listingColumns(){
        if( !empty( $this->listingColumns ) ){
            return $this->listingColumns;
        }

        $columns = [];
        foreach( $this->columns as $col ){
            if( $col['auto_increment'] ){ continue; }
            if( $col['field'] == 'created_at' && !in_array( $col['field'], $this->columnOrdering ) ){ continue; }
            if( $col['field'] == 'updated_at' && !in_array( $col['field'], $this->columnOrdering ) ){ continue; }
            if( $col['field'] == 'deleted_at' && !in_array( $col['field'], $this->columnOrdering ) ){ continue; }
            if( in_array( $col['field'], $this->listingColumnsExclude ) ){ continue;}
            if( $col['field'] == 'language_id' ){ continue; }
            if( (is_array( $this->primaryColumn ) && in_array( $col['field'], $this->primaryColumn )) || $col['field'] == $this->primaryColumn ){ continue; }
            if( is_array( $this->visibleColumns ) && !empty( $this->visibleColumns ) && !in_array( $col['field'], $this->visibleColumns ) ){ continue; }

            $col['orderable'] = false;
            if( in_array( $col['type'], $this->sortableColumnTypes ) ){
                $col['orderable'] = true;
                $col['orderable_order'] = 'asc';
                $link_order = 'asc';
                $col['orderable_active'] = false;

                if(
                    isset( $this->list_parameters['sort-by'] ) && $this->list_parameters['sort-by'] == $col['field']
                    || ($this->defaultSort !== false && $this->defaultSort == $col['field'])
                ){
                    $col['orderable_active'] = true;

                    $order = !empty( $this->list_parameters['order'] ) ? $this->list_parameters['order'] : $this->defaultSortDirection;

                    $col['orderable_order'] = strtolower( $order ) == 'desc' ? 'desc' : 'asc';
                    $link_order = strtolower( $order ) == 'desc' ? 'asc' : 'desc';
                }

                $col['orderable_url'] = $this->_getColumnSortUrl( $col['field'], $link_order );
            }

            $classes = [];
            $data_str = '';
            if( isset( $col['orderable'] ) && $col['orderable'] ){
                $classes[] = "orderable";

                if( $col['orderable_active'] ){
                    $classes[] = "ordered";

                    if( $col['orderable_order'] == 'desc' ){
                        $classes[] = "ordered-desc";
                    } else {
                        $classes[] = "ordered-asc";
                    }
                }

                $data_str = 'data-order-field=' . $col['field'] . ' ';
                if( in_array( 'ordered-desc', $classes ) ){
                    $data_str .= 'data-order=asc';
                } elseif( in_array( 'ordered-asc', $classes) ){
                    $data_str .= 'data-order=desc';
                } else {
                    $data_str .= 'data-order=asc';
                }
            }
            $col['classes'] = $classes;
            $col['data_str'] = $data_str;

            $columns[$col['field']] = $col;
        }

        uasort( $columns, function( $a, $b ){
            $a_keys = array_keys( $this->columnOrdering, $a['field'] );
            $b_keys = array_keys( $this->columnOrdering, $b['field'] );

            $a_pos = !empty( $a_keys ) ? reset( $a_keys ) : 0;
            $b_pos = !empty( $b_keys ) ? reset( $b_keys ) : 0;

            return $a_pos == $b_pos ? 0 : $a_pos > $b_pos;
        });

        if( sizeof( $this->getLanguages() ) > 1 && $this->translatable ){
            $this->changeType( 'language_id', 'relation' );
            $this->setRelation( 'language_id', 'languages', 'title' );
            $this->insertColumn( 'language_id');

            $language_col = $this->columns['language_id'];
            $language_col['orderable'] = false;
            $language_col['classes'] = [];
            $language_col['data_str'] = '';

            $columns['language_id'] = $language_col;
        }

        return $columns;
    }

    /**
     * Generate the 'columns' to be displayed on the form view
     * If the 'path' field is present, add the SEO options field.
     *
     * @return array
     */
    private function formColumns(){
        // Check to see if we need to modify an item prior to generation of fields for forms
        $callback_function = Str::camel( 'callback_before_prepare_' . $this->slug . '_item_for_form' );
        if( method_exists( $this->caller, $callback_function ) ){
            $this->caller->{$callback_function}( $this->currentItem );
        }

        $columns = [];
        $CrudField = new LaraCrudField( $this );
        foreach( $this->columns as $col ){
            if( $col['auto_increment'] ){ continue; }
            if( $col['field'] == 'created_at' ){ continue; }
            if( $col['field'] == 'updated_at' ){ continue; }
            if( $col['field'] == 'deleted_at' ){ continue; }
            if( in_array( $col['field'], $this->editColumnsExclude ) ){ continue;}
            if( ( is_array( $this->primaryColumn ) && in_array( $col['field'], $this->primaryColumn ) ) || $col['field'] == $this->primaryColumn ){ continue; }

            $CrudField->setType( $col['type'] )->prepare( $col, $this->currentItem );

            if( $col['type'] == 'text' ){
                $this->wysiwygEnabled = true;
            }

            $columns[ $col['field'] ] = $col;
        }

        foreach( $this->formFieldGroups as $group => $fields ){
            foreach( $fields as $field_name => $field ){
                if( array_key_exists( $field_name, $columns ) ){
                    $this->formFieldGroups[ $group ][ $field_name ] = $columns[ $field_name ];
                    unset( $columns[ $field_name ] );
                }
            }
        }
        $this->formFieldGroups['main'] = array_merge( $this->formFieldGroups[ "main" ], $columns );

        foreach( $this->formFieldGroups as $group => $fields ){
            foreach( $fields as $field_name => $field ){
                if( $field === 1 ){
                    unset( $this->formFieldGroups[ $group ][ $field_name ] );
                }
            }
        }

        return $this->formFieldGroups;
    }

    /**
     * For certain actions, the column set needs to have just the fields
     * as keys, so remove the column grouping and return the simple column set
     *
     * @return array
     */
    private function unGroupColumns(){
        $unGrouped_cols = array();
        foreach( $this->formFieldGroups as $group => $fields ) {
            foreach ($fields as $field_name => $field) {
                $unGrouped_cols[ $field_name ] = $field;
            }
        }
        return $unGrouped_cols;
    }

    /**
     * Generate a set of items to display on the list view
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    private function getItems(){
        $this->initModel();
        $crud_class = $this->_getCrudModel();

        if( $this->translatable ){
            $query = $crud_class::where( $this->getTable() . ".language_id", $this->language_id );
        } else {
            $query = $crud_class::distinct();
        }

        $this->_dbSearch( $query );
        $this->_dbSort( $query );
        $this->_dbPagination($query);

        if( !empty( $this->whereConditions ) ){
            foreach( $this->whereConditions as $field => $values ){
                foreach( $values as $value => $operator ) {
                    $query->where( $this->getTable() . '.' . $field, $operator, $value );
                }
            }
        }

        if( !$this->request->input('sort-by') ){
            $list_items_custom_order_by_callback = Str::camel( 'callback_order_' . $this->slug . '_list_items' );
            if( method_exists( $this->caller, $list_items_custom_order_by_callback ) ){
                $this->caller->{$list_items_custom_order_by_callback}( $query );
            }
        }

        $list_items_query_filter_callback = Str::camel( 'callback_filter_' . $this->slug . '_list_items' );
        if( method_exists( $this->caller, $list_items_query_filter_callback ) ){
            $this->caller->{$list_items_query_filter_callback}( $query );
        }

        $items = $query->get();

        $LaraCrudField = new LaraCrudField( $this );

        foreach( $items as &$item ){
            if( $this->translatable ){
                $item->locale = $this->_getLocaleByLanguageId( $item->language_id );
            }

            foreach( $this->columns as $col ){
                if( $col['auto_increment'] ){ continue; }
                if( $col['field'] == 'created_at' ){ continue; }
                if( $col['field'] == 'updated_at' ){ continue; }
                if( $col['field'] == 'deleted_at' ){ continue; }
                if( in_array( $col['field'], $this->listingColumnsExclude ) ){ continue;}
                if( $col['field'] != 'language_id' && ( is_array( $this->primaryColumn ) && in_array( $col['field'], $this->primaryColumn ) ) || $col['field'] == $this->primaryColumn ){ continue; }

                $LaraCrudField->setType( $col['type'] )->show( $col, $item );

                $field_specific_method = Str::camel("show_" . $col['field'] . "_field");
                if( method_exists( $this->caller, $field_specific_method ) ){
                    $this->caller->{$field_specific_method}( $col, $item );
                }

                $view = 'columns.' . $LaraCrudField->getBladeName();
                if( View::exists( $view ) ){
                    $item->{$col['field']} = view( $view, array( "item" => $item, "value" => $item->{$col['field']}, "column" => $col ) );
                }
            }
        }
        return $items;
    }

    /**
     * Generate a set of items to export, with an optional array parameter of items to export
     *
     * @param array $filter_items
     * @return \Illuminate\Database\Eloquent\Collection
     */
    private function getExportItems( $filter_items = array() ){
        $this->initModel();
        $crud_class = $this->_getCrudModel();

        if( $this->translatable ){
            $query = $crud_class::where( $this->getTable() . ".language_id", $this->language_id );
        } else {
            $query = $crud_class::distinct();
        }

        if( !empty( $filter_items ) ){
            $query->whereIn( $this->getTable() . '.' . $this->getPrimaryColumn(), $filter_items );
        }

        if( !empty( $this->whereConditions ) ){
            foreach( $this->whereConditions as $field => $values ){
                foreach( $values as $value => $operator ) {
                    $query->where($this->getTable() . '.' . $field, $operator, $value);
                }
            }
        }

        if( !$this->request->input( 'sort-by' ) ){
            $callback_function = Str::camel( 'callback_order_' . $this->slug . '_export_items' );
            if( method_exists( $this->caller, $callback_function ) ){
                $this->caller->{$callback_function}( $query );
            }
        }

        $callback_function = Str::camel( 'callback_filter_' . $this->slug . '_export_items' );
        if( method_exists( $this->caller, $callback_function ) ){
            $this->caller->{$callback_function}( $query );
        }

        $this->_dbSearch( $query );
        $this->_dbSort( $query );

        $items = $query->get();

        $CrudField = new LaraCrudField( $this );
        foreach( $items as &$item ){
            foreach( $this->columns as $col ){
                $CrudField->setType( $col['type'] )->show( $col, $item );
            }
        }

        $callback_function = Str::camel( 'callback_process_' . $this->slug . '_export_items' );
        if( method_exists( $this->caller, $callback_function ) ){
            $this->caller->{$callback_function}( $items );
        }

        return $items;
    }

    /**
     * Get the total count of items on the current list
     *
     * @return int
     */
    private function getItemCount(){
        $this->initModel();
        $crud_class = $this->_getCrudModel();

        if( $this->translatable ){
            $query = $crud_class::where( $this->getTable() . ".language_id", $this->language_id );
        } else {
            $query = $crud_class::distinct();
        }

        $this->_dbSearch( $query );

        if( !empty( $this->whereConditions ) ){
            foreach( $this->whereConditions as $field => $values ){
                foreach( $values as $value => $operator ){
                    $query->where( $this->getTable() . '.' . $field, $operator, $value );
                }
            }
        }

        $items = $query->get();
        return $items->count();
    }

    /**
     * Get the current item
     *
     * @return \Adm\Models\CrudModel|null
     * @throws CrudExceptions
     */
    public function getItem(){
        $this->initModel();
        $crud_class = $this->_getCrudModel();

        if( !empty( $this->currentItem ) ){
            return $this->currentItem;
        }

        if( empty( $this->parameters['id'] ) ){
            $this->crudError('The parameter <strong>id</strong> is required!');
        }

        if( $this->primaryColumn && $this->primaryColumnNoAutoIncrement ){
            if( is_array( $this->primaryColumn ) ){
                $query = $crud_class::where( $this->_generateCompositeModelWhere() );
            } else {
                $query = $crud_class::where( $this->primaryColumn, $this->parameters['id'] );
            }
        } else {
            $query = $crud_class::where( $this->primaryColumn, $this->parameters['id'] );
        }

        if( !empty( $this->whereConditions ) ){
            foreach( $this->whereConditions as $field => $values ){
                foreach( $values as $value => $operator ) {
                    $query->where( $this->getTable() . '.' . $field, $operator, $value );
                }
            }
        }

        $item = $query->first();

        if( empty( $item ) ){
            $this->crudError('Item not found!');
        }

        $item->setPrimaryKey( $this->primaryColumn );

        if( array_key_exists( 'path', $this->columns ) && in_array( $this->parameters['action'], array( "edit" )) ){
            $item->url = url('/') . '/' . \Adm\Models\Url::where('table_name',$this->getTable())->where($this->_generateUrlWhere( $item ))->value('url');
        }

        $this->currentItem = $item;

        return $item;
    }

    /**
     * Get the current item in the default language
     *
     * @return \Adm\Crud\CrudModel|null
     * @throws CrudExceptions
     */
    public function getDefaultItem(){
        $this->initModel();
        $crud_class = $this->_getCrudModel();

        if( !empty( $this->currentItem ) ){
            return $this->currentItem;
        }

        if( empty( $this->parameters['id'] ) ){
            $this->crudError('The parameter <strong>id</strong> is required!');
        }

        if( $this->primaryColumn && $this->primaryColumnNoAutoIncrement ){
            if( is_array( $this->primaryColumn ) ){
                $query = $crud_class::where( $this->getPrimaryColumn(), $this->parameters['id'])->where('language_id', $this->getDefaultLanguage() );
            } else {
                $query = $crud_class::where( $this->primaryColumn, $this->parameters['id'] );
            }
        } else {
            $query = $crud_class::where( $this->primaryColumn, $this->parameters['id'] );
        }

        if( !empty( $this->whereConditions ) ){
            foreach( $this->whereConditions as $field => $values ){
                foreach( $values as $value => $operator ) {
                    $query->where( $this->getTable() . '.' . $field, $operator, $value );
                }
            }
        }

        $defaultItem = $query->first();

        if( empty( $defaultItem ) ){
            $this->crudError('Item not found!');
        }

        $defaultItem->setPrimaryKey( $this->primaryColumn );

        if( array_key_exists( 'path', $this->columns ) && in_array( $this->parameters['action'], array( "edit" )) ){
            $defaultItem->url = url('/') . '/' . \Adm\Models\Url::where('table_name',$this->getTable())->where(
                [
                    'language_id'   => $this->getDefaultLanguage(),
                    'table_key'     => $defaultItem->{$this->getPrimaryColumn()}
                ]
            )->value( 'url' );
        }

        $this->currentItem = $defaultItem;

        return $defaultItem;
    }

    /**
     * Get an 'empty' item in the default language, ready for population by various form elements.
     * Mainly used to ensure that field values are carried over to forms in the result of validation errors.
     *
     * @return \Adm\Crud\CrudModel
     */
    protected function getEmptyItem(){
        $this->initModel();
        $crud_class = $this->_getCrudModel();

        if( !empty( $this->currentItem ) ){
            return $this->currentItem;
        }

        $newItem = new $crud_class();

        // As we're getting an 'empty' item, we can set default field values
        foreach( $this->fieldDefaultValues as $column => $default ){
            data_set( $newItem, $column, $default );
        }

        $this->currentItem = $newItem;

        return $newItem;
    }

    /**
     * Get validation rules for the current column set
     *
     * @return array
     */
    private function validationRules(){
        $validations = [];
        $CrudField = new LaraCrudField( $this );
        foreach( $this->columns as $column ){
            if( in_array($column['field'], $this->editColumnsExclude)){ continue;}
            if( $column['auto_increment'] ){ continue; }
            if( $column['field'] == 'created_at' ){ continue; }
            if( $column['field'] == 'updated_at' ){ continue; }
            if( $column['field'] == 'deleted_at' ){ continue; }
            if( ( is_array( $this->primaryColumn ) && in_array( $column['field'], $this->primaryColumn ) ) || $column['field'] == $this->primaryColumn ){ continue; }
            $validations[ $column['field'] ] = $column['validation'];

            $CrudField->setType( $column['type'] )->getValidationRules( $validations, $this->request->input(), $column );

            $table_specific_method = Str::camel( 'get_' . $this->slug . '_validation_rules' );
            if( method_exists( $this->caller, $table_specific_method ) ){
                $this->caller->{$table_specific_method}( $validations, $this->request->input() );
            }
        }

        return $validations;
    }

    /**
     * Get custom validation messages for the current column set
     *
     * @return array
     */
    private function validationMessages(){
        $messages = [];
        $CrudField = new LaraCrudField( $this );
        foreach( $this->columns as $column ){
            if( in_array($column['field'], $this->editColumnsExclude)){ continue;}
            if( $column['auto_increment'] ){ continue; }
            if( $column['field'] == 'created_at' ){ continue; }
            if( $column['field'] == 'updated_at' ){ continue; }
            if( $column['field'] == 'deleted_at' ){ continue; }
            if( ( is_array( $this->primaryColumn ) && in_array( $column['field'], $this->primaryColumn ) ) || $column['field'] == $this->primaryColumn ){ continue; }

            $CrudField->setType( $column['type'] )->getValidationMessages( $messages, $this->request->input(), $column );

            $table_specific_method = Str::camel( 'get_' . $this->slug . '_validation_messages' );
            if( method_exists( $this->caller, $table_specific_method ) ){
                $this->caller->{$table_specific_method}( $messages, $this->request->input() );
            }
        }

        return $messages;
    }

    /**
     * Perform validation on the current column set
     */
    private function validate(){
        // Check if the CustomValidation trait is in use, and call it to get our custom validation rules
        if( method_exists( $this, "applyCustomValidationRules" ) ){
            $this->applyCustomValidationRules( $this );
        }
        // Check if the CustomValidation trait is in use, and call it to get our custom validation messages
        if( method_exists( $this, "applyCustomValidationMessages" ) ){
            $this->applyCustomValidationMessages();
        }

        $validation = $this->validationRules();
        $validation_messages = $this->validationMessages();

        $this->caller->validate( $this->request, $validation, $validation_messages );
    }

    /**
     * Used after a create/edit/copy form is submitted.
     * Calls the relevant callback functions, sets the submitted data to the
     * current item and saves it
     *
     * @param $item
     * @return int
     */
    private function setItem( $item ){
        $inputs = $this->request->all();
        $CrudField = new LaraCrudField( $this );

        $method = Str::camel("before_saved_" . $this->slug . "_Item");
        if( method_exists( $this->caller, $method ) ){
            $this->caller->{$method}( $inputs, $item );
        }

        foreach( $inputs as $k => $v ){
            if( !empty( $this->multiRelations[ $k ] ) ){
                $CrudField->setType( 'multi_relation' )->save( $v, $item->$k, $k );
            }

            if( !$this->columnExists( $this->getTable(), $k ) ){ continue; }

            $v = $CrudField->setType( $this->columns[ $k ]['type'] )->save( $v, $item->$k, $k );

            if( !$this->columns[$k]['required'] || strlen( $v ) !== 0 ){
                $item->$k = $v;
            }
        }

        $primary_column = $this->getPrimaryColumn();
        if( $this->primaryColumn && $this->primaryColumnNoAutoIncrement ){
            if( isset( $this->parameters['id'] ) && $this->parameters['id'] ){
                $item->$primary_column = $this->parameters['id'];
            } else {
                // We need to get the next Primary Key ID field for this column because it's not an auto increment
                $item->$primary_column = $this->_getNextId();
            }
            $item->language_id = $this->language_id;
        } else {
            if( isset( $this->parameters['id'] ) && $this->parameters['id'] ){
                $item->$primary_column = $this->parameters['id'];
            }
        }

        if( isset( $item->status ) && !isset( $inputs['status'] ) ){
            $item->status = 0;
        }

        if( is_array( $this->primaryColumn ) && in_array( "version", $this->primaryColumn ) ){
            if( $this->getAction() == 'store' ){
                $item->version = 1;
            } else {
                $lastItem = DB::table( $this->getTable() )
                    ->select( 'version' )
                    ->where( $this->getPrimaryColumn(), $item->{$this->getPrimaryColumn()} )
                    ->where( 'language_id', $this->language_id )
                    ->orderBy( 'version', 'desc' )
                    ->first();
                $item->version = $lastItem->version + 1;
            }
        }

        $item->save();
        if( !isset( $this->parameters['id'] ) ){
            $this->parameters['id'] = $item->id;
        }

        $item_primary = $this->primaryColumn;
        if( $this->primaryColumn && $this->primaryColumnNoAutoIncrement ) {
            if( is_array( $this->primaryColumn ) ){
                foreach( $this->primaryColumn as $primary_col ){
                    $item_primary[ $primary_col ] = $item->$primary_col;
                }
            }
        } elseif( !isset( $item->$primary_column ) ) {
            $item->$primary_column = $item->id;
        }

        $method = Str::camel("saved_" . $this->slug . "_Item");
        if( method_exists( $this->caller, $method ) ){
            $this->caller->{$method}( $item_primary, $inputs, $item );
        }

        foreach( $this->multiRelations as $k => $opt ){
            $CrudField->setType( 'multi_relation' )->afterSaved( $item_primary, $inputs, $item, $k );
        }

        foreach( $inputs as $k => $v ){
            if( !empty( $this->columns[$k] ) ){
                $CrudField->setType( $this->columns[ $k ]['type'] )->afterSaved( $item_primary, $inputs, $item, $k );
            }
        }

        // Callback after creation or update of items
        if( $this->getAction() == 'create' ){
            $callback_function = Str::camel( 'callback_after_created_' . $this->slug . '_item' );
            if( method_exists( $this->caller, $callback_function ) ){
                $this->caller->{$callback_function}( $item_primary, $inputs, $item );
            }
        }
        elseif( $this->getAction() == 'update' ){
            $callback_function = Str::camel( 'callback_after_updated_' . $this->slug . '_item' );
            if( method_exists( $this->caller, $callback_function ) ){
                $this->caller->{$callback_function}( $item_primary, $inputs, $item );
            }
        }

        if( !isset($this->parameters['id']) || empty($this->parameters['id']) ){
            $this->parameters['id'] = $item->$primary_column;
        }
        $this->_addChangeLogItem();

        // Now that we've created or updated an item, clear any cache that's in place.
        Cache::flush();
        $this->purgeCloudflare();

        return $item->$primary_column;
    }

    /**
     * Returns a set of default data to pass down to templates.
     * Takes a column set, generated from either listingColumns() or formColumns()
     *
     * @param $columns
     * @return array
     */
    public function defaultData( $columns ){
        $my_bespoke_api = new MyBespokeApi;

        $defaults = [
            'title'                     => $this->getSubject(),
            'table'                     => $this->slug,
            'table_name'                => $this->getTable(),
            'crud_action'               => $this->getAction(),
            'actions'                   => $this->actions,
            'columns'                   => $columns,
            'toggleFields'              => $this->toggleFields,
            'navigation'                => $this->caller->generateNavigation(),
            'primaryColumn'             => $this->getPrimaryColumn(),
            'languageColumn'            => 'language_id',
            'defaultLanguage'           => $this->getDefaultLanguage(),
            'parameters'                => $this->parameters,
            'current_language'          => $this->language_id,
            'breadcrumbs'               => $this->_generateBreadcrumbs(),
            'me'                        => Auth::user(),
            'assetManagerEnabled'       => $this->assetManagerEnabled,
            'wysiwygEnabled'            => $this->wysiwygEnabled,
            'pageBuilderEnabled'        => $this->pageBuilderEnabled,
            'crud_model'                => $this->_getCrudModel(),
            'relationship_manager'      => $my_bespoke_api->getRelationshipManager() ? $my_bespoke_api->getRelationshipManager() : false,
            'pageBuilderDynamicScripts' => $this->pageBuilderDynamicScripts,
            'dynamic_scripts'           => $this->dynamic_scripts
        ];

        $Cloudflare = new Cloudflare();
        $defaults['cloudflare_enabled'] = $Cloudflare->get_status();
        $defaults['cloudflare_dev_mode'] = $Cloudflare->get_development_mode() == 'on';

        if( sizeof( $this->getLanguages() ) > 1 ){
            $defaults['current_language_data'] = $this->getLanguageData( $this->language_id );
        }

        foreach( $this->extra_view_data as $key => $value ){
            $defaults[ $key ] = $value;
        }

        return $defaults;
    }

    /**
     * Called when generating the list view
     *
     * @return View|\Symfony\Component\HttpFoundation\Response
     */
    public function listingAction(){
        $crud_class = $this->_getCrudModel();

        $this->list_parameters = $this->request->all();

        // Use cookie based storage of the parameters used to filter & sort the list
        $cookie_name = 'bespokecms_' . str_replace( '-', '_', $this->slug ) . '_list_filter_cookie';
        $saved_cookie = Cookie::get( $cookie_name );

        // If we don't have any GET parameters, but we DO have a cookie
        if( empty( $this->list_parameters ) && $saved_cookie ){
            // Use the cookie as the current parameters
            $this->list_parameters = json_decode( $saved_cookie, true );
        }
        // Else, if we have GET parameters, update the cookie
        elseif( !empty( $this->list_parameters ) ){
            $a_day = 60 * 60 * 24;
            Cookie::queue( Cookie::make( $cookie_name, json_encode( $this->list_parameters ), $a_day ) );
        }

        $data = $this->defaultData( $this->listingColumns() );
        $data['items'] = $this->getItems();

        $data['linked_field'] = $this->linkedField;

        if( Session::has( 'custom_blocks_popup' ) ){
            $item = $crud_class::find( Session::get( 'custom_blocks_popup' ) );
            Session::pull( 'custom_blocks_popup' );
            return response('<script>top.saveCustomBlock("'.$this->getTable().'", "'.$item->id.'", "'.$item->reference.'");</script>', 200)->send();
        }

        if( in_array( 'create', $this->actions ) ){
            $data['create_action'] = route('adm.path', [$this->slug, 'create']);
        } else {
            $data['create_action'] = false;
        }

        if( in_array( 'export', $this->actions ) ) {
            $data['export_action'] = route('adm.path', [$this->slug, 'export']);
        } else {
            $data['export_action'] = false;
        }

        $data['current_show_option'] = $this->_getDbLimit();
        $data['show_number_options'] = $this->show_number_options;
        $data['pagination'] = $this->_getPaginationArray( $this->getItemCount(), $this->slug );
        $data['search_data'] = $this->_getViewSearchData();
        $data['languages'] = $this->translatable ? $this->getLanguages() : [];
        $data['button_labels'] = $this->listing_button_texts;
        $data['extra_bulk_actions'] = $this->extra_bulk_actions;

        $data['has_status_field'] = array_key_exists( 'status', $data['columns'] );

        $show_action_bar = $data['has_status_field'] || in_array( 'destroy', $data['actions'] );
        if( Auth::user()->userGroup->super_admin ){
            $show_action_bar = true;
        }
        $data['show_action_bar'] = $show_action_bar;

        $view = 'list';
        if( !empty( $this->list_view ) ){
            $view = $this->list_view;

            if( !empty( $this->viewParameters ) ){
                $data = array_merge( $data, $this->viewParameters );
            }
        }

        return view( $view, $data );
    }

    /**
     * Saves a variable into the session
     *
     * @param bool|true $forget
     * @param bool|false $val
     */
    public function setSession($forget = true, $val = false){
        if( $forget ){
            Session::pull( 'custom_blocks_popup' );
            if( !empty( $this->request->input( 'popup' ) ) ){
                Session::set( 'custom_blocks_popup', 1 );
            }
        } else {
            if( Session::has( 'custom_blocks_popup' ) ){
                Session::set( 'custom_blocks_popup', $val );
            }
        }

    }

    /**
     * Called when generating the create form
     *
     * @return View
     */
    public function createAction(){
        if( isset( $this->parameters['id'] ) && isset( $this->parameters['locale'] ) && !$this->request->isMethod('post') ){
            $data = $this->defaultData( $this->formColumns( $this->getDefaultItem() ) );
            $this->setSession();
            $data['item'] = $this->getDefaultItem();
            $data['item_id'] = $data['item']->{$this->getPrimaryColumn()};
            $data['language_links'] = $this->getLanguageLinks();
        } else {
            $data = $this->defaultData( $this->formColumns( $this->getEmptyItem() ) );
            $this->setSession();
            $data['item'] = $this->getEmptyItem();
        }

        $data['title'] = isset( $data['title_override'] ) ? $data['title_override'] : 'Add New ' . ucwords( Str::singular($this->getSubject()) );

        $data['cancel_action'] = route('adm.path', [$this->slug]);

        $data['system_page_enabled'] = $this->systemPageEnabled;

        return view('form', $data);
    }

    /**
     * Called when generating the edit form
     *
     * @return View
     */
    public function editAction(){
        $locked_by_id = config( 'modules.locked_by_id' );
        if( !empty( $locked_by_id ) ){
            $locked_modules = array_keys( $locked_by_id );
            $caller_class = get_class( $this->caller );
            preg_match( '/Modules\\\\([^\\\\]*)\\\\Adm.*/i', $caller_class, $matches );
            if( isset( $matches[1] ) && in_array( $matches[1], $locked_modules ) ){
                $locked_ids = $locked_by_id[ $matches[1] ];
                if( in_array( $this->parameters['id'], $locked_ids ) ){
                    Session::flash('error', 'Sorry but that ' . Str::singular( $this->getSubject() ) . ' is currently frozen from editing.');
                    return redirect( route('adm.path') );
                }
            }
        }

        $data = $this->defaultData( $this->formColumns( $this->getItem() ) );
        $this->setSession();
        $data['item'] = $this->getItem();
        $data['item_id'] = $data['item']->{$this->getPrimaryColumn()};

        if( isset( $data['title_override'] ) ){
            $data['title'] = $data['title_override'];
        } elseif( $this->displayColumn && isset( $data['item']->{$this->displayColumn} ) ){
            $title = $data['item']->{$this->displayColumn};

            if( strlen( $title ) > 50 ){
                $title = substr( $title, 0, 50 ) . '...';
            }

            $data['title'] = "Edit " . $title;
        } else {
            $data['title'] = "Edit " . Str::singular( $this->getSubject() );
        }

        $data['cancel_action'] = route('adm.path', [$this->slug]);
        $data['delete_action'] = route('adm.path', [$this->slug, 'destroy', $this->parameters['id']]);

        $data['last_updated'] = $this->_getLastUpdated();
        $data['language_links'] = $this->getLanguageLinks();
        $data['content_versions'] = false;
        if( is_array( $this->primaryColumn ) && in_array( 'version', $this->primaryColumn ) ){
            $data[ 'content_versions' ] = $this->_getContentVersions( $data[ 'item' ] );
        }

        $data['system_page_enabled'] = $this->systemPageEnabled;

        return view('form', $data);
    }

    /**
     * Called after a form submission from the create form
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeAction(){
        $this->validate();
        $class_name = $this->_getCrudModel();
        $this->setSession(false, $this->setItem(new $class_name));

        Session::flash( 'success', Str::singular( $this->getSubject() ) . ' saved.' );
        return response()->redirectTo( route( 'adm.path', $this->slug ) );
    }

    /**
     * Called when deleting an item
     *
     * N.B. Will only 'delete' if the record has a 'deleted' column for soft deletes.
     * May need revising to remove items otherwise
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroyAction(){
        $this->deleteItem();

        // Now that we've deleted item(s), clear any cache that's in place.
        Cache::flush();
        $this->purgeCloudflare();

        // If we've got past the deleteItem call without an exception being thrown, we're all good
        Session::flash( 'success', Str::singular( $this->getSubject() ) . ' Deleted' );
        return response()->redirectTo( route( 'adm.path', $this->slug ) );
    }

    /**
     * Actually delete an item.
     *
     * Allows deletes to be performed outside of a CRUD method where necessary (i.e. Adm Callbacks)
     *
     * @param null $id
     * @return bool
     */
    public function deleteItem( $id = null ){
        // If we've got a custom ID to delete here, make sure that we record the original ID to revert back to
        $original_id = false;
        if( !is_null( $id ) ){
            if( isset( $this->parameters['id'] ) ) {
                $original_id = $this->parameters['id'];
            }

            // And set the current ID as the passed ID
            $this->parameters['id'] = $id;
        }

        $this->formColumns();
        $data = $this->ungroupColumns(); //$this->formColumns();

        $item = $this->getItem();

        $primary_column = $this->getPrimaryColumn();

        $CrudField = new LaraCrudField( $this );
        foreach( $data as $k => $v ){
            if( !isset( $this->columns[ $k ] ) ){ continue; }
            $CrudField->setType( $this->columns[ $k ]['type'] )->beforeDelete( $item->{$primary_column} );
        }

        // And ensure that any relational fields are removed
        foreach( $this->multiRelations as  $col => $opt ){
            DB::table( $opt['relation_table'] )->where($opt['relation_id'], $this->parameters['id'])->delete();
        }

        // Before deleting the item, get it's ID to pass to a callback function
        $item_id = $item->{$primary_column};

        // Now, delete the item!
        $item->delete();

        foreach( $data as $k => $v ){
            if( !isset( $this->columns[ $k ] ) ){ continue; }
            $CrudField->setType( $this->columns[ $k ]['type'] )->afterDelete( $item->{$primary_column} );
        }

        // Check to see if we need to perform any actions after deleting an item
        $callback_function = Str::camel( 'callback_after_deleted_' . $this->slug . '_item' );
        if( method_exists( $this->caller, $callback_function ) ){
            $this->caller->{$callback_function}( $item_id );
        }

        // And record that an item has been deleted
        if( !isset( $this->parameters['id'] ) || empty( $this->parameters['id'] ) ){
            $this->parameters['id'] = $item_id;
        }
        $this->_addChangeLogItem();

        // If we were passed a custom ID, and we previously had another ID, revert back to the original ID.
        if( $original_id !== false ){
            $this->parameters['id'] = $original_id;
        }

        // Now that we've deleted an item, clear any cache that's in place.
        Cache::flush();
        $this->purgeCloudflare();

        return true;
    }

    /**
     * Called after a form submission from the edit form
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateAction(){
        $item = $this->getItem();
        $this->validate();

        // Before we 'update' the item (i.e. create a new version) we need to 'delete' the old one
        if( isset( $item->version ) ){
            $item->delete();

            // Before creating a new object, check the current number of versions we have for this item
            $other_versions = $this->_getContentVersions( $item );
            // And ensure we aren't storing too many versions
            if( sizeof( $other_versions ) > config( 'database.maximum_number_of_versions' ) ){
                // If we are, get the latest version
                $last_version = max( array_keys( $other_versions ) );
                // And get the oldest version
                $oldest_version = min( array_keys( $other_versions ) );
                // And calculate the most recent version number that we want to keep
                $last_version = $last_version - (config( 'database.maximum_number_of_versions' )-1);

                // And delete them
                for( $i = $oldest_version; $i <= $last_version; $i++ ){
                    if( array_key_exists( $i, $other_versions ) ){
                        $this->_permanentlyDeleteVersion( $this->parameters['id'], $this->language_id, $i );
                    }
                }
            }

            $this->initModel();
            $class_name = $this->_getCrudModel();
            $item = new $class_name;
        }
        $this->setSession( false, $this->setItem( $item ) );

        Session::flash('success', Str::singular( $this->getSubject() ) . ' saved.');

        $redirect_to_list = $this->request->input( 'save_and_go_back' );
        if( $redirect_to_list ){
            $redirect_url = route('adm.path', [$this->slug]);
        } else {
            $redirect_url = route('adm.path', [$this->slug, 'edit', $this->parameters['id'], ((isset($this->parameters['locale']) ? $this->parameters['locale'] : ''))]);
        }

        // If we've been editing a non-default language, redirect back to the listing for that language
        if( $this->language_id != $this->getDefaultLanguage() ){
            $redirect_url .= '?search=&language=' . $this->_getLocaleByLanguageId( $this->language_id ) . '&search-by=all';
        }

        return response()->redirectTo( $redirect_url );
    }

    /**
     * Called when copying an item
     *
     * @return View
     */
    public function copyAction(){
        $data = $this->defaultData( $this->formColumns( $this->getItem() ) );
        $this->setSession();
        $data['item'] = $this->getItem();
        $data['copy'] = true;

        foreach( $data['columns'] as $group => $columns ){
            foreach( $columns as $ind => $column ){
                if( $column['field'] == 'path' ){
                    $data['columns'][ $group ][ $ind ]['values'] = new \Adm\Models\Url;
                }
            }
        }

        $data['cancel_action'] = route('adm.path', [$this->slug]);

        $data['system_page_enabled'] = $this->systemPageEnabled;

        return view('form', $data);
    }

    /**
     * Called when exporting a CSV
     */
    public function exportAction(){
        $data = $this->defaultData( $this->formColumns() );

        $filter_items = $this->request->input('selected') ? $this->request->input('selected') : array();
        $data['items'] = $this->getExportItems( $filter_items );

        $method = Str::camel( "callback_amend_" . $this->slug . "_export_data" );
        if( method_exists( $this->caller, $method ) ){
            $this->caller->{$method}( $data['columns'], $data['items'] );
        }

        header( 'Content-Type: text/csv' );
        header('Content-Disposition: attachment; filename="' . $this->getSubject() . " export " . date("Y-m-d-H-i-s") . '.csv"');

        $fp = fopen( 'php://output', 'w+' );

        $headings = array();
        foreach( $data['columns'] as $group => $columns ){
            foreach( $columns as $column ) {
                $headings[] = $column['label'];
            }
        }
        fputcsv( $fp, $headings );
        ob_flush();
        flush();

        foreach( $data['items'] as $item ){
            $row = array();
            foreach( $data['columns'] as $group => $columns ){
                foreach( $columns as $column ) {
                    $value = data_get( $item, transform_key( $column['field'] ) );
                    $row[] = trim( strip_tags( $value ) );
                }
            }
            fputcsv( $fp, $row );
            ob_flush();
            flush();
        }

        fclose( $fp );
        exit;
    }

    /**
     * CRUD Function ready for a 'View' type action on resources.
     * Not yet feature complete, so is throwing an exception.
     *
     * @return View
     */
    public function showAction(){
        throw new CrudExceptions( 500, 'Sorry, this feature is not yet complete. How did you get here?' );

        $data = $this->defaultData( $this->formColumns() );
        $this->setSession();
        $data['item'] = $this->getItem();
        $data['item_id'] = $data['item']->{$this->getPrimaryColumn()};

        if( isset( $data['title_override'] ) ){
            $data['title'] = $data['title_override'];
        } elseif( $this->displayColumn && isset($data['item']->{$this->displayColumn}) ){
            $title = $data['item']->{$this->displayColumn};

            if( strlen( $title ) > 50 ){
                $title = substr( $title, 0, 50 ) . '...';
            }

            $data['title'] = "View " . $title;
        } else {
            $data['title'] = "View " . Str::singular( $this->getSubject() );
        }

        $data['cancel_action'] = route('adm.path', [$this->slug]);
        $data['delete_action'] = route('adm.path', [$this->slug, 'destroy', $this->parameters['id']]);

        $data['last_updated'] = $this->_getLastUpdated();
        $data['language_links'] = $this->getLanguageLinks();

        $data['system_page_enabled'] = $this->systemPageEnabled;

        return view('show', $data);
    }

    /**
     * Get the current languages available in the system
     * @return array
     */
    protected function getLanguages(){
        return DB::table('languages')->get();
    }

    /**
     * If the current subject is translatable, generate a set of links
     * to display to switch language
     *
     * @return array
     */
    public function getLanguageLinks(){
        if( !Schema::hasColumn( $this->getTable(), 'language_id' ) ){
            return array();
        }

        $languages = $this->getLanguages();

        if( sizeof( $languages ) == 1 ){ return array(); }

        $language_links = array();
        $primary_column = $this->getPrimaryColumn();
        foreach( $languages as $language ){
            $translation_exists = DB::table( $this->getTable() )
                ->where( $primary_column, $this->parameters['id'] )
                ->where( 'language_id', $language->id )
                ->first();

            $locale = $language->id == $this->getDefaultLanguage() ? '' : $language->locale;
            if( $translation_exists ){
                $exists = true;
                $status = isset($translation_exists->status) ? $translation_exists->status : true;
                $url = route('adm.path', [$this->slug, 'edit', $this->parameters['id'], $locale]);
            } else {
                $exists = false;
                $status = 0;
                $url = route('adm.path', [$this->slug, 'create', $this->parameters['id'], $locale]);
            }

            $language_links[] = array(
                'url'       => $url,
                'status'    => $status,
                'exists'    => $exists,
                'locale'    => $language->locale,
                'current'   => $language->id == $this->language_id,
                'title'     => $language->title
            );
        }

        return $language_links;
    }

    /**
     * Get the system's default language
     *
     * @return int
     */
    public function getDefaultLanguage(){
        return DB::table( 'defaults' )->where( 'id', 1 )->value( 'default_language' );
    }

    /**
     * Get all data for a language based on a language ID
     *
     * @param $language_id
     * @return mixed
     */
    public function getLanguageData( $language_id ) {
        return DB::table( 'languages' )->where( 'id', $language_id )->first();
    }

    /**
     * Get a language by the 'locale' field (used in the adm.path route, e.g. en, de, it)
     *
     * @param $locale
     * @return mixed
     */
    public function _getLanguageByLocale( $locale ){
        $language = DB::table('languages')->where('locale', $locale)->first();
        if( !$language ){
            $this->crudError( 'The language <strong>'.$locale.'</strong> does not exist!' );
        }
        return $language;
    }

    /**
     * Get a language ID by the 'locale' field (used in the adm.path route, e.g. en, de, it)
     *
     * @param $locale
     * @return mixed
     */
    public function _getLanguageIdByLocale( $locale ){
        $language = DB::table('languages')->where('locale', $locale)->value('id');
        if( !$language ){
            $this->crudError( 'The language <strong>'.$locale.'</strong> does not exist!' );
        }
        return $language;
    }

    /**
     * Get a 'locale' from the current language ID
     *
     * @param $language_id
     * @return mixed
     */
    public function _getLocaleByLanguageId( $language_id ){
        $language = DB::table('languages')->where('id', $language_id)->value('locale');
        if( !$language ){
            $this->crudError( 'That language does not exist!' );
        }
        return $language;
    }

    /**
     * If the current subject doesn't have an autoincrement field, we need to generate a new ID
     *
     * @return int
     */
    private function _getNextId(){
        if( isset( $this->parameters['id'] ) && !empty( $this->parameters['id'] ) ){
            return $this->parameters['id'];
        }

        $primary_column = $this->getPrimaryColumn();
        return (int)DB::table( $this->getTable() )->orderBy( $primary_column, 'DESC' )->value( $primary_column ) + 1;
    }

    /**
     * Centralised function to return whether the system is using CrudModel or CompositeCrudModel
     *
     * @return \Adm\Crud\CrudModel
     */
    public function _getCrudModel(){
        $crud_class = 'Adm\\Crud\\'.self::$crudModelReference;
        return $crud_class;
    }

    /**
     * Centralised function to generate the ID 'where' query when using a composite primary key.
     * Generates an array of [ 'subject_id' => ID, 'language_id' => language ]
     *
     * @return array
     */
    protected function _generateCompositeModelWhere(){
        $where = array();
        foreach( $this->primaryColumn as $column ){
            if( $column == "version" ){ continue; }

            if( $column == "language_id" ){
                $where[$column] = $this->language_id;
            } else {
                $where[$column] = $this->parameters['id'];
            }
        }
        return $where;
    }

    /**
     * Centralised function to generate a 'base' where query for URLs, taking the current language into account
     *
     * @param $item
     * @return array
     */
    protected function _generateUrlWhere( $item ){
        return [
            'language_id'   => $this->language_id,
            'table_key'     => $item->{$this->getPrimaryColumn()}
        ];
    }

    /**
     * Centralised function to return the ID field for a composite primary key that isn't 'language_id'
     *
     * @return string
     */
    public function getPrimaryColumn(){
        $primary_column = $this->primaryColumn;
        if( is_array( $this->primaryColumn) ){
            foreach( $this->primaryColumn as $primary_col ){
                if( $primary_col != 'language_id' && $primary_col != 'version' ){
                    $primary_column = $primary_col;
                }
            }
        }

        return $primary_column;
    }

    /**
     * Add an item into the change log table (i.e. create, edit, delete) associated with the logged in user
     */
    private function _addChangeLogItem(){
        $user = Auth::user();
        // Stop users with the 'admin' flag set in the user group from being added to the change log
        if( !$user->userGroup->super_admin ) {
            DB::table('change_log')->insert([
                'table_name'    => $this->getTable(),
                'table_key'     => $this->parameters['id'],
                'language_id'   => $this->language_id,
                'action'        => $this->parameters['action'],
                'adm_user_id'   => $user->id
            ]);
        }
    }

    /**
     * For the current item, check if it has been updated, and if so return the update date and user
     *
     * @return array
     */
    private function _getLastUpdated(){
        $last_updated = DB::table('change_log')->where([
            'table_name'    => $this->getTable(),
            'table_key'     => $this->parameters['id'],
            'language_id'   => $this->language_id,
            'action'        => 'update'
        ])->orderBy('changed', 'desc')->first();

        if( $last_updated ){
            $updated_user = DB::table('adm_users')->where('id', $last_updated->adm_user_id)->first();
            $last_updated->who = $updated_user->first_name . ' ' . substr( $updated_user->last_name, 0, 1) . '.';

            return $last_updated;
        }

        return array();
    }

    /**
     * For the current item, check if there are any previous content versions, and if so return them as an array
     * to allow the user to restore a previous version if required.
     *
     * @param $item
     * @return array
     */
    private function _getContentVersions( $item ){
        $versions = DB::table( $this->getTable() )
            ->where([
                $this->getPrimaryColumn()   => $this->parameters['id'],
                'language_id'               => $this->language_id
            ])
            ->where( 'version', '<>', $item->version )
            ->orderBy( 'version', 'desc' )
            ->get();

        if( $versions ){
            $version_arr = [];
            foreach( $versions as $version ){
                $version_arr[ $version->version ] = date( "jS F Y, g:ia", strtotime( $version->created_at ) );
            }

            return $version_arr;
        }

        return [];
    }

    /**
     * Function used to restore a previously saved content version of an item, based on ID, language & version number.
     *
     * @param $item_id
     * @param $language_id
     * @param $version
     * @return bool
     */
    public function restoreContentVersion( $item_id, $language_id, $version ){
        $itemToRestore = DB::table( $this->getTable() )
            ->where([
                $this->getPrimaryColumn()   => $item_id,
                'language_id'               => $language_id,
                'version'                   => $version
            ])
            ->first();

        if( !$itemToRestore ){
            return false;
        }

        // Now that we've got the version requested, we can 'delete' any other versions
        DB::table( $this->getTable() )
            ->where([
                $this->getPrimaryColumn()   => $item_id,
                'language_id'               => $language_id
            ])
            ->where( 'version', '<>', $version )
            ->whereNull( 'deleted_at' )
            ->update([ 'deleted_at' => date("Y-m-d H:i:s") ]);

        // And now that we've done that, we can also 'delete' page builder blocks
        DB::table( 'page_builder_blocks' )
            ->where([
                'table_name'    => $this->getTable(),
                'table_key'     => $item_id,
                'language_id'   => $language_id
            ])
            ->where( 'version', '<>', $version )
            ->whereNull( 'deleted_at' )
            ->update([ 'deleted_at' => date( "Y-m-d H:i:s") ]);

        // Now that we've cleared up other versions, we can restore the requested version
        DB::table( $this->getTable() )
            ->where([
                $this->getPrimaryColumn()   => $item_id,
                'language_id'               => $language_id,
                'version'                   => $version
            ])
            ->update([ 'deleted_at' => null ]);

        // And restore it's page builder blocks
        DB::table( 'page_builder_blocks' )
            ->where([
                'table_name'    => $this->getTable(),
                'table_key'     => $item_id,
                'language_id'   => $language_id,
                'version'   => $version
            ])
            ->update([ 'deleted_at' => null ]);

        Cache::flush();
        $this->purgeCloudflare();
        return true;
    }

    /**
     * Function used to permanently delete an item's version.
     *
     * @param $item_id
     * @param $language_id
     * @param $version
     */
    private function _permanentlyDeleteVersion( $item_id, $language_id, $version ){
        // First, delete the record in the table we're storing it in
        DB::table( $this->getTable() )
            ->where([
                $this->getPrimaryColumn()   => $item_id,
                'language_id'               => $language_id,
                'version'                   => $version
            ])
            ->delete();

        // Before we delete page builder blocks, we need to clear the content
        // So, get the blocks
        $blocks = DB::table( 'page_builder_blocks' )
            ->where([
                'table_name'    => $this->getTable(),
                'table_key'     => $item_id,
                'language_id'   => $language_id,
                'version'       => $version
            ])
            ->get();

        // And delete the content & block
        foreach( $blocks as $block ){
            DB::table( 'page_builder_block_content' )
                ->where( 'page_builder_block_id', $block->id )
                ->delete();

            DB::table( 'page_builder_blocks' )
                ->where( 'id', $block->id )
                ->delete();
        }

        // Then, delete any saved asset groups
        DB::table( 'asset_groups' )
            ->where([
                'table_name'    => $this->getTable(),
                'table_key'     => $item_id,
                'language_id'   => $language_id,
                'version'       => $version
            ])
            ->delete();
    }

    /**
     * Generates the breadcrumbs for the admin system
     *
     * @return array
     */
    private function _generateBreadcrumbs(){
        $breadcrumbs = array();

        // Add the main dashboard link
        $breadcrumbs[] = array(
            'label' => 'Dashboard',
            'url' => route('adm.path')
        );

        // If we're browsing a particular section of the admin, add that
        if( isset( $this->parameters['table'] ) ){
            $breadcrumbs[] = array(
                'label' => $this->getSubject(),
                'url' => route('adm.path', [$this->slug])
            );
        }

        // If we're performing a create, copy or edit, add a breadcrumb
        if( isset( $this->parameters['action'] ) ){
            if( isset( $this->parameters['id'] ) ) {
                $defaultItem = $this->getDefaultItem();
            }

            if( ( $this->parameters['action'] == 'create' && !isset( $this->parameters['id'] ) ) ){
                if( isset( $this->extra_view_data['title_override'] ) ){
                    $label = $this->extra_view_data['title_override'];
                } else {
                    $label = 'Add ' . Str::singular( $this->getSubject() );
                }

                $breadcrumbs[] = array(
                    'label' => $label,
                    'url' => ''
                );
            } elseif( $this->parameters['action'] == 'create' && isset( $this->parameters['id'] ) ){
                if( isset( $this->extra_view_data['title_override'] ) ){
                    $label = $this->extra_view_data['title_override'];
                } elseif( $this->displayColumn && isset( $defaultItem ) && isset( $defaultItem->{$this->displayColumn} ) ){
                    $title = $defaultItem->{$this->displayColumn};

                    if( strlen( $title ) > 25 ){
                        $title = substr( $title, 0, 25 ) . '...';
                    }

                    $label = $title;
                } else {
                    $label = "Add " . Str::singular( $this->getSubject() );
                }

                $breadcrumbs[] = array(
                    'label' => $label,
                    'url' => route('adm.path', [$this->slug, $this->parameters['action'], $this->parameters['id']])
                );
            } elseif( $this->parameters['action'] == 'edit' && isset($this->parameters['id']) ){
                if( isset( $this->extra_view_data['title_override'] ) ){
                    $label = $this->extra_view_data['title_override'];
                } elseif( $this->displayColumn && isset( $defaultItem ) && isset( $defaultItem->{$this->displayColumn} ) ){
                    $title = $defaultItem->{$this->displayColumn};

                    if( strlen( $title ) > 25 ){
                        $title = substr( $title, 0, 25 ) . '...';
                    }

                    $label = $title;
                } else {
                    $label = "Edit " . Str::singular( $this->getSubject() );
                }

                $breadcrumbs[] = array(
                    'label' => $label,
                    'url' => route('adm.path', [$this->slug, $this->parameters['action'], $this->parameters['id']])
                );
            } elseif( $this->parameters['action'] == 'copy' && isset($this->parameters['id']) ){
                if( isset( $this->extra_view_data['title_override'] ) ){
                    $label = $this->extra_view_data['title_override'];
                } elseif( $this->displayColumn && isset( $defaultItem ) && isset( $defaultItem->{$this->displayColumn} ) ){
                    $title = $defaultItem->{$this->displayColumn};

                    if( strlen( $title ) > 25 ){
                        $title = substr( $title, 0, 25 ) . '...';
                    }

                    $label = "Copy " . $title;
                } else {
                    $label = "Copy " . Str::singular( $this->getSubject() );
                }

                $breadcrumbs[] = array(
                    'label' => $label,
                    'url' => route('adm.path', [$this->slug, $this->parameters['action'], $this->parameters['id']])
                );
            }

            // If we're editing a particular language, add a breadcrumb
            if( isset( $this->parameters['id'] ) && isset( $this->parameters['locale'] ) ){
                $language = $this->_getLanguageByLocale( $this->parameters['locale'] );

                $breadcrumbs[] = array(
                    'label' => $language->title,
                    'url' => route('adm.path', [$this->slug, $this->parameters['action'], $this->parameters['id'], $this->parameters['locale']])
                );
            }
        }

        foreach( $breadcrumbs as $ind => $breadcrumb ){
            if( empty( $breadcrumb['label'] ) ){
                unset( $breadcrumbs[ $ind ] );
            }
        }

        return $breadcrumbs;
    }

    /**
     * Get the URL to apply to an orderable column, keeping existing relevant $_GET parameters intact
     *
     * @param string $field
     * @param string $order
     * @return string
     */
    private function _getColumnSortUrl( $field, $order ){
        $searchData = $this->_getViewSearchData();

        $get_parts = [];
        if( !empty( $searchData['search_term'] ) ){ $get_parts[] = 'search=' . $searchData['search_term']; }
        if( !empty( $searchData['search_language'] ) ){ $get_parts[] .= 'language=' . $searchData['search_language']; }
        if( !empty( $searchData['search_field'] ) ){ $get_parts[] .= 'search-by=' . $searchData['search_field']; }

        $get_parts[] = 'sort-by=' . $field;
        $get_parts[] = 'order=' . $order;

        return route('adm.path', $this->slug) . '?' . implode( "&", $get_parts );
    }

    /**
     * Apply an ORDER BY section to a query
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     */
    private function _dbSort( &$query ){
        $sort_column = false;
        if( $this->defaultSort !== false ){ $sort_column = $this->defaultSort; }
        if( !empty( $this->list_parameters['sort-by'] ) ) {
            $sort_column = $this->list_parameters['sort-by'];
        }
        if( !$sort_column ){
            $query->orderBy( $this->getPrimaryColumn() );
            return;
        }

        $column = $this->_getFieldFromSearchColumn($sort_column);
        if( !in_array( $column['type'], $this->sortableColumnTypes ) ){ return; }

        if( empty( $this->list_parameters['search'] ) ) {
            // In case there are any relational fields being queried, we need to generate a set of
            // table aliases to use when joining and selecting
            $alias_length = 8;

            if( $column['type'] == 'relation' ){
                if( !isset( $this->joinAliases[ $column['field'] ] ) ){
                    $alias = substr( str_shuffle( str_repeat( $x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil( $alias_length / strlen( $x ) ) ) ), 1, $alias_length );
                    $this->joinAliases[$column['field']] = $alias;
                } else {
                    $alias = $this->joinAliases[$column['field']];
                }

                $thisRelation = $this->relations[$column['field']];
                $table = $thisRelation['table'];
                $reference_col = $thisRelation['reference_col'];
                $query->leftJoin( $table . ' AS ' . $alias, $this->getTable() . '.' . $column['field'], '=', $alias . '.' . $reference_col );
            } elseif( $column['type'] == 'path' ){
                if( !isset( $this->joinAliases[ $column['field'] ] ) ){
                    $alias = substr( str_shuffle( str_repeat( $x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil( $alias_length / strlen( $x ) ) ) ), 1, $alias_length );
                    $this->joinAliases[$column['field']] = $alias;
                } else {
                    $alias = $this->joinAliases[$column['field']];
                }

                $query->leftJoin( 'urls AS ' . $alias, function( $join ) use( $alias ){
                    $join->on( $this->getTable() . '.' . $this->getPrimaryColumn(), '=', $alias . '.table_key' )->where( $alias . '.table_name', '=', $this->getTable() );
                });
            }
        }

        $sort_aliases = $this->joinAliases;
        // And if we've got any joins or aliases, only select fields from the base table
        if( !empty( $sort_aliases ) ){
            $query->select( $this->getTable() . '.*' )->distinct();
        }

        $sort_order = !empty( $this->list_parameters['order'] ) ? $this->list_parameters['order'] : $this->defaultSortDirection;

        switch( $column['type'] ){
            case "relation":
                $thisRelation = $this->relations[$column['field']];
                $rel_column = $thisRelation['column'];

                $alias = $sort_aliases[ $column['field'] ];
                $query->orderBy( $alias . '.' . $rel_column, $sort_order );

                break;
            case "path":
                $alias = $sort_aliases[ $column['field'] ];
                $query->orderBy( $alias . '.url', $sort_order );
                break;
            default:
                $query->orderBy( $this->getTable() . '.' . $column['field'], $sort_order );
                break;
        }
    }

    /**
     * Internal function allowing a column to be prepared without being rendered.
     * This function allows us to run a page builder block column through the usual CRUD preparation, but returns the
     * column, rather than using it for output on a form.
     *
     * @param string $type
     * @param array $column
     * @param \Adm\Crud\CrudModel $item
     * @return array
     */
    public function prepareColumn( $type, &$column, &$item ){
        $CrudField = new LaraCrudField( $this );
        $CrudField->setType( $type )->prepare( $column, $item );
        return $column;
    }

    /**
     * Enable Page Builder
     * Sets the $pageBuilderEnabled flag to true
     */
    public function enablePageBuilder(){
        $this->pageBuilderEnabled = true;
    }

    /**
     * Disable Main Content Page Builder Block
     * Sets the $pageBuilderNeedsMainContent flag to false
     */
    public function disableMainContentPageBuilderBlock(){
        $this->pageBuilderNeedsMainContent = false;
    }

    /**
     * Field AJAX Call Exists
     * Checks whether an AJAX call function is available within any of the
     * CRUD system's fields
     *
     * @param string $ajax_call
     * @return bool
     */
    public function fieldAjaxCallExists( $ajax_call ){
        $FieldType = new LaraCrudField( $this );
        return $FieldType->ajaxMethodExists( $ajax_call );
    }

    /**
     * Field AJAX Call
     * Calls the provided AJAX call function on the CRUD system field that
     * contains the function
     *
     * @param string $ajax_call
     * @return \Illuminate\Http\Response
     */
    public function fieldAjaxCall( $ajax_call ){
        $FieldType = new LaraCrudField( $this );
        return $FieldType->ajaxMethodCall( $ajax_call );
    }

    /**
     * Purge Cloudflare
     * Centralised function used to purge Cloudflare cache within the CMS
     * We're just clearing ALL cache when saving due to the interconnectivity
     * of everything
     */
    public function purgeCloudflare(){
        $Cloudflare = new Cloudflare();
        $Cloudflare->purge_site();
    }

    /*
     * FUNCTIONS FROM Traits/LaraCrudApi.php
     */

    /**
     * Force to use a specific view
     *
     * @param string $view
     * @param array $parameters
     */
    public function setView( $view, $parameters = [] ){
        $this->view = $view;
        $this->viewParameters = $parameters;
    }

    /**
     * For the list view to use a specific template
     *
     * @param string $view
     * @param array $parameters
     */
    public function setListView( $view, $parameters = [] ){
        $this->list_view = $view;
        $this->viewParameters = $parameters;
    }

    /**
     * Set the subject of the view
     *
     * @param string $subject
     */
    public function setSubject( $subject ){
        $this->subject = $subject;
    }

    /**
     * Set which field from the item to use as it's display title.
     *
     * @param string $field
     */
    public function displayColumn( $field ){
        $this->displayColumn = $field;
    }

    /**
     * Set which field from the item to add an edit link to in the list view.
     *
     * @param string $field
     */
    public function linkedField( $field ){
        $this->linkedField = $field;
    }

    /**
     * Set which field to use to auto-generate the URL path when adding an item
     *
     * @param string|array $field
     */
    public function urlGenerationField( $field ){
        $this->urlGenerationField = $field;
    }

    /**
     * Set which field to use to auto-generate the meta title when adding an
     * item
     *
     * @param string $field
     */
    public function metaTitleGenerationField( $field ){
        $this->metaTitleGenerationField = $field;
    }

    /**
     * Set which field to use to auto-generate the meta description when adding
     * an item
     *
     * @param string $field
     */
    public function metaDescriptionGenerationField( $field ){
        $this->metaDescriptionGenerationField = $field;
    }

    /**
     * If a resource requires a path prefix of another resource (i.e. a system page rather than a parent of the same type)
     * then we can set this using setBasePathPrefix
     *
     * @param string $table
     * @param string $field
     * @param string $column
     */
    public function setBasePathPrefix( $table, $field, $column = 'id' ){
        $this->basePathPrefix = [
            'table' => $table,
            'field' => $field,
            'column' => $column
        ];
    }

    /**
     * Use a specific field as path prefix
     *
     * @param string $table
     * @param string $field
     * @param string $column
     */
    public function setPathPrefix( $table, $field, $column = 'id' ){
        $this->pathPrefix = [
            'table' => $table,
            'field' => $field,
            'column' => $column
        ];
    }

    /**
     * Enable the relation for a field
     *
     * @param string $field
     * @param string $table
     * @param string $column
     * @param string $reference_column
     * @param string|int $empty_value
     * @param string|bool $adm_slug
     * @param array|bool $where_conditions
     */
    public function setRelation( $field, $table, $column, $reference_column = 'id', $empty_value = 0, $adm_slug = false, $where_conditions = false ){
        if( !$this->tableExists( $table ) ){
            $this->crudError( 'The table <strong>'.$table.'</strong> does not exist!' );
        }

        if( is_array( $column ) ){
            foreach( $column as $col ){
                if( !$this->columnExists( $table, $col ) ){
                    $this->crudError( 'The column <strong>' . $col . '</strong> in the table <strong>' . $table . '</strong> does not exist.' );
                }
            }
        } else {
            if( !$this->columnExists( $table, $column ) ){
                $this->crudError( 'The column <strong>' . $column . '</strong> in the table <strong>' . $table . '</strong> does not exist.' );
            }
        }

        $this->relations[ $field ] = [
            'table'             => $table,
            'column'            => $column,
            'reference_col'     => $reference_column,
            'translatable'      => Schema::hasColumn( $table, "language_id" ),
            'empty_value'       => $empty_value,
            'where_conditions'  => $where_conditions
        ];

        if( $adm_slug ){
            $this->relations[ $field ]['adm_link'] = route('adm.path', [$adm_slug, 'create']);
        } else {
            $this->relations[ $field ]['adm_link'] = route('adm.path', [$table, 'create']);
        }
    }

    /**
     * Exclude a set of values from a relational field for the given field
     *
     * @param string $field
     * @param array $excluded_values
     */
    public function setRelationExcludes( $field, $excluded_values ){
        $this->relationExcludes[ $field ] = $excluded_values;
    }

    /**
     * Relation n to n
     *
     * @param string $field
     * @param string $relation_table
     * @param string $selection_table
     * @param string $relation_id
     * @param string $selection_id
     * @param string $selection_title
     * @param bool|false $relation_priority
     * @param string|bool $adm_slug
     */
    public function multiRelation( $field, $relation_table, $selection_table, $relation_id, $selection_id, $selection_title, $relation_priority = false, $adm_slug = false, $selection_id_save_alias = false ){
        if( !$this->tableExists( $relation_table ) ){
            $this->crudError( 'The table <strong>'.$relation_table.'</strong> does not exist!' );
        }

        if( !$this->tableExists( $selection_table ) ){
            $this->crudError( 'The table <strong>'.$selection_table.'</strong> does not exist!' );
        }

        if( !$this->columnExists( $relation_table, $relation_id ) ){
            $this->crudError( 'The column <strong>'.$relation_id.'</strong> in the table <strong>'.$relation_table.'</strong> does not exist.' );
        }

        if( !$this->columnExists( $relation_table, $selection_id ) ){
            $this->crudError( 'The column <strong>'.$selection_id.'</strong> in the table <strong>'.$relation_table.'</strong> does not exist.' );
        }

        if( !$this->columnExists( $selection_table, $selection_title ) ){
            $this->crudError( 'The column <strong>'.$selection_title.'</strong> in the table <strong>'.$selection_table.'</strong> does not exist.' );
        }

        if( $relation_priority ){
            if( !$this->columnExists( $relation_table, $relation_priority ) ){
                $this->crudError( 'The column <strong>'.$relation_priority.'</strong> in the table <strong>'.$relation_table.'</strong> does not exist.' );
            }
        }

        if( $selection_id_save_alias ){
            if( !$this->columnExists( $relation_table, $selection_id_save_alias ) ){
                $this->crudError( 'The column <strong>' . $selection_id_save_alias . '</strong> in the table <strong>' . $relation_table . '</strong> does not exist.' );
            }
        }

        $this->addColumn( $field );
        $this->changeType( $field, 'multi_relation' );
        $this->multiRelations[ $field ] = [
            'relation_table'            => $relation_table,
            'selection_table'           => $selection_table,
            'relation_id'               => $relation_id,
            'selection_id'              => $selection_id,
            'selection_title'           => $selection_title,
            'relation_priority'         => $relation_priority,
            'selection_id_save_alias'   => $selection_id_save_alias
        ];

        if( $adm_slug ){
            $this->multiRelations[ $field ]['adm_link'] = route('adm.path', [$adm_slug, 'create']);
        } else {
            $this->multiRelations[ $field ]['adm_link'] = route('adm.path', [$selection_table, 'create']);
        }
    }

    /**
     * Force to use a specific table
     *
     * @param string $table
     */
    public function setTable( $table ){
        $this->table = $table;
    }

    /**
     * Sets the 'primary' column for the current table
     *
     * @param string $column
     */
    public function setPrimaryColumn( $column ){
        $this->primaryColumn = $column;
    }

    /**
     * Unset the 'create' action for the current subject
     */
    public function unsetAdd(){
        $this->unsetAction( 'create' );
    }

    /**
     * Unset the 'edit' action for the current subject
     */
    public function unsetEdit(){
        $this->unsetAction( 'edit' );
    }

    /**
     * Unset the 'destroy' action for the current subject
     */
    public function unsetDelete(){
        $this->unsetAction( 'destroy' );
    }

    /**
     * Unset the 'copy' action for the current subject
     */
    public function unsetCopy(){
        $this->unsetAction( 'copy' );
    }

    /**
     * Unset the 'export' action for the current subject
     */
    public function setExport(){
        $this->setAction( 'export' );
    }

    /**
     * Instead of displaying the default CRUD actions for a subject, set that the current
     * subject is a 'single'. This will then only generate the edit form for the item based on
     * the ID passed.
     *
     * For example, use to set site defaults
     *
     * @param int|null $id
     */
    public function single( $id = null ){
        $this->isSingle = !empty( $id ) ? $id : true;
        $this->unsetAdd();
        $this->unsetDelete();
        $this->unsetAction( 'listing' );
    }

    /**
     * Instead of displaying the default CRUD actions for a subject, set that the current
     * subject is a 'single'. This will then only generate the edit form for the item based on
     * the ID passed, but allow creation of translations.
     *
     * For example, use to have a dedicated URL for the home page
     *
     * @param int|null $id
     */
    public function multiLingualSingle( $id = null ){
        $this->isSingle = !empty( $id ) ? $id : true;
        $this->unsetDelete();
        $this->unsetAction( 'listing' );
    }

    /**
     * Sets which columns should be displayed on the list view, and in which order
     *
     * @param ...$columns
     */
    public function columns( ...$columns ){
        $this->visibleColumns = $this->columnOrdering = [];
        foreach( $columns as $column ) {
            $this->visibleColumns[] = $column;
            $this->columnOrdering[] = $column;
        }
    }

    /**
     * Exclude a column(s) from the list view
     *
     * @param $columns
     */
    public function excludeFromListing( $columns ){
        $columns = is_array( $columns ) ? $columns : func_get_args();
        $this->listingColumnsExclude = array_merge( $this->listingColumnsExclude, $columns );
    }

    /**
     * Exclude a column(s) from the edit view
     *
     * @param $columns
     */
    public function excludeFromEdit( $columns ){
        $columns = is_array( $columns ) ? $columns : func_get_args();
        $this->editColumnsExclude = array_merge( $this->editColumnsExclude, $columns );
    }

    /**
     * Exclude a column(s) from both list and edit views
     *
     * @param $columns
     */
    public function excludeColumns( $columns ){
        $columns = is_array( $columns ) ? $columns : func_get_args();
        $this->excludeFromEdit( $columns );
        $this->excludeFromListing( $columns );
    }

    /**
     * Add a new column
     *
     * @param string $column
     * @param string $after
     * @param string $position
     */
    public function addColumn( $column, $after = '', $position = 'main' ){
        if( $after ){
            $this->newColumns[ $after ] = $column;
        } else {
            $this->newColumns[] = $column;
        }

        $this->columnLocation( $column, $position );
    }

    /**
     * Set a custom order for list columns
     *
     * @param ...$columns
     */
    public function orderColumns( ...$columns ){
        foreach( $columns as $column ){
            $this->columnOrdering[] = $column;
        }
    }

    /**
     * Set a custom default order for the CMS list view.
     *
     * @param string $column
     * @param string $order
     */
    public function orderBy( $column, $order = 'ASC' ){
        $this->defaultSort = $column;
        $this->defaultSortDirection = $order;
    }

    /**
     * Set custom where conditions for getting items for list views
     *
     * @param string $field
     * @param mixed $value
     * @param string $operator
     */
    public function where( $field, $value, $operator = '=' ){
        if( !isset( $this->whereConditions[ $field ] ) ){
            $this->whereConditions[ $field ] = [];
        }

        if( !array_key_exists( $value, $this->whereConditions[ $field ] ) ) {
            $this->whereConditions[ $field ][ $value ] = $operator;
        }
    }

    /**
     * Set a column's location on the form
     *
     * @param string $column
     * @param string $location
     */
    public function columnLocation( $column, $location ){
        if( !isset( $this->formFieldGroups[ $location ] ) ){
            $this->formFieldGroups[ $location ] = [];
        }

        $this->formFieldGroups[ $location ][ $column ] = 1;
    }

    /**
     * Add/Modify a group of fields on the form
     *
     * @param string $group
     * @param array $fields
     * @param bool|false $merge
     */
    public function columnGroup( $group, $fields, $merge = false ){
        // Convert the field array into array keys
        $field_keys = array();
        foreach( $fields as $field ){ $field_keys[ $field ] = 1; }
        $fields = $field_keys;

        if( !isset( $this->formFieldGroups[ $group ] ) || !$merge ){
            $this->formFieldGroups[ $group ] = $fields;
        } else {
            $this->formFieldGroups[ $group ] = array_merge( $this->formFieldGroups[ $group ], $fields );
        }
    }

    /**
     * For a given field, fields to hide, and show condition, add the information to our array.
     * If the $field is set to $show_condition, then show the fields in $fields_to_show.
     *
     * e.g.
     * $this->crud->toggleFields( 'my_checkbox', array( 'field_1', 'field_2' ), '1' );
     * If the field 'my_checkbox' on a form has a value of 1 then show field_1 and field_2, else hide them.
     *
     * @param string $field
     * @param array $fields_to_show
     * @param string|int $show_condition
     */
    public function toggleFields( $field, $fields_to_show, $show_condition ){
        $this->toggleFields[ $field ] = array(
            'condition' => $show_condition,
            'fields' => $fields_to_show
        );
    }

    /**
     * Change the label of a field/column
     *
     * @param string $col
     * @param string $name
     */
    public function displayAs( $col, $name ){
        $this->displayAs[ $col ] = $name;
    }

    /**
     * Add help text to a field
     *
     * @param string $col
     * @param string $help
     */
    public function help( $col, $help ){
        $this->helpText[ $col ] = $help;
    }

    /**
     * Change the type of a field
     *
     * @param string $col
     * @param string $type
     */
    public function changeType( $col, $type ){
        $this->ColumnType[ $col ] = $type;
    }

    /**
     * Set the options available for a field
     * (e.g. select, radio, checkboxes)
     *
     * @param string $col
     * @param array $options
     */
    public function fieldOptions( $col, $options = [] ){
        $this->fieldOptions[ $col ] = $options;
    }

    /**
     * Set any extra data against a column
     *
     * @param string $col
     * @param mixed $data
     */
    public function extraData( $col, $data ){
        $this->extraData[ $col ] = $data;
    }

    /**
     * Set any custom attributes against a field
     *
     * @param string $col
     * @param mixed $data
     */
    public function fieldAttributes( $col, $data ){
        $this->fieldAttributes[ $col ] = $data;
    }

    /**
     * Set Default Value
     * Sets a default field value against a field
     *
     * @param string $col
     * @param mixed $default
     */
    public function setDefaultValue( $col, $default ){
        $this->fieldDefaultValues[ transform_key( $col ) ] = $default;
    }

    /**
     * Get Default Value
     * Returns a default field value for a field, or null if not set
     *
     * @param string $col
     * @return mixed|null
     */
    public function getDefaultValue( $col ){
        return isset( $this->fieldDefaultValues[ transform_key( $col ) ] ) ? $this->fieldDefaultValues[ transform_key( $col ) ] : null;
    }

    /**
     * Set custom text for the 'Edit' button on the list view
     *
     * @param string $edit_text
     */
    public function editButtonText( $edit_text ){
        $this->listing_button_texts['edit'] = $edit_text;
    }

    /**
     * Set custom text for the 'Copy' button on the list view
     *
     * @param string $copy_text
     */
    public function copyButtonText( $copy_text ){
        $this->listing_button_texts['copy'] = $copy_text;
    }

    /**
     * Add a 'bulk' action to be performed on the CMS list view.
     *
     * @param string $url
     * @param string $text
     * @param string $colour
     */
    public function addBulkAction( $url, $text, $colour = 'colour-default' ){
        $action = [
            'url' => $url,
            'text' => $text,
            'colour' => $colour
        ];

        $this->extra_bulk_actions[] = $action;
    }

    /**
     * Add a dynamic script to the current view.
     *
     * @param string $view
     */
    public function addDynamicScript( $view ){
        $this->dynamic_scripts[] = $view;
    }

    /*
     * FUNCTIONS FROM Traits/Search.php
     */

    /**
     * Generate a set of columns to be displayed within the list view search area.
     *
     * @return array
     */
    private function _getSearchColumns(){
        $searchable = array();
        $searchable_types = array(
            'string', 'text', 'integer', 'boolean', 'relation', 'path'
        );

        foreach( $this->columns as $column ){
            if( in_array( $column['type'], $searchable_types ) ) {
                $searchable[(string)$column['field']] = $column['label'];
            }
        }

        return $searchable;
    }

    /**
     * Return all information relating to a search in a single array for the view template
     *
     * @return array
     */
    private function _getViewSearchData(){
        return array(
            'columns' => $this->_getSearchColumns(),
            'search_term' => isset( $this->list_parameters['search'] ) ? $this->list_parameters['search'] : false,
            'search_field' => isset( $this->list_parameters['search-by'] ) ? $this->list_parameters['search-by'] : false,
            'search_language' => isset( $this->list_parameters['language'] ) ? $this->list_parameters['language'] : false
        );
    }

    /**
     * Because columns are ordered and are no longer associative, we need
     * to be able to find the field for the column being searched
     *
     * @param $search_column
     * @return mixed|bool
     */
    private function _getFieldFromSearchColumn( $search_column ){
        foreach( $this->columns as $column ){
            if( $column['field'] == $search_column ){
                return $column;
            }
        }

        return false;
    }

    /**
     * If the current query has search options, apply the search conditions to the query
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     */
    private function _dbSearch( &$query ){
        if( !empty( $this->list_parameters['search'] ) ){
            $search_data = $this->_getViewSearchData();
            $search_term = $search_data['search_term'];
            $search_field = $search_data['search_field'];
            $searchable = $search_data['columns'];

            // In case there are any relational fields being queried, we need to generate a set of
            // table aliases to use when joining and selecting
            $alias_length = 8;
            foreach( $searchable as $column => $field ){
                $column = $this->_getFieldFromSearchColumn($column);
                if( $column['type'] == 'relation' ){
                    if( !isset($this->joinAliases[ (string)$column['field'] ]) ) {
                        $alias = substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($alias_length / strlen($x)))), 1, $alias_length);
                        $this->joinAliases[ (string)$column['field'] ] = $alias;
                    } else {
                        $alias = $this->joinAliases[ (string)$column['field'] ];
                    }

                    $thisRelation = $this->relations[ (string)$column['field'] ];
                    $table = $thisRelation['table'];
                    $reference_col = $thisRelation['reference_col'];
                    $query->leftJoin( $table . ' AS ' . $alias, $this->getTable() . '.' . $column['field'], '=', $alias . '.' . $reference_col );
                }
                elseif( $column['type'] == 'path' ){
                    if( !isset($this->joinAliases[ (string)$column['field'] ]) ) {
                        $alias = substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($alias_length / strlen($x)))), 1, $alias_length);
                        $this->joinAliases[ (string)$column['field'] ] = $alias;
                    } else {
                        $alias = $this->joinAliases[ (string)$column['field'] ];
                    }

                    $query->leftJoin( 'urls AS ' . $alias, function( $join ) use ($alias){
                        $join->on( $this->getTable() . '.' . $this->getPrimaryColumn(), '=', $alias . '.table_key' )
                            ->where( $alias . '.table_name', $this->getTable() );
                    });
                }
            }

            $search_aliases = $this->joinAliases;
            // And if we've got any joins or aliases, only select fields from the base table
            if( !empty($search_aliases) ){
                $query->select( $this->getTable() . '.*' )->distinct();
            }

            // Now, generate the grouped WHERE clause for the search.
            // It's grouped because there may be additional WHERE clauses set on the
            // base table itself
            $query->where( function($query) use ($search_term, $search_field, $searchable, $search_aliases) {
                if (empty($search_field) || $search_field == 'all') {
                    $this->_dbSearchAll($query, $search_term, $search_aliases);
                } else {
                    if (array_key_exists($search_field, $searchable)) {
                        $column = $this->_getFieldFromSearchColumn($search_field);
                        if ($column !== false) {
                            $this->_dbSearchColumn($query, $column, $search_term, 'where', $search_aliases);
                        }
                    }
                }
            });
        }
    }

    /**
     * Search ALL available columns
     *
     * @param $query
     * @param $search_term
     * @param $search_aliases
     */
    private function _dbSearchAll( &$query, $search_term, $search_aliases ){
        $searchable = $this->_getSearchColumns();
        foreach( $searchable as $column => $field ){
            $column = $this->_getFieldFromSearchColumn( $column );
            if( $column !== false ){
                $this->_dbSearchColumn( $query, $column, $search_term, 'orWhere', $search_aliases );
            }
        }
    }

    /**
     * Add a condition to the WHERE query for the search
     *
     * @param $query
     * @param $column
     * @param $search_term
     * @param $operation - either 'where' or 'orWhere'
     * @param $search_aliases
     */
    private function _dbSearchColumn( &$query, $column, $search_term, $operation, $search_aliases ){
        switch( $column['type'] ){
            case "string":
                if( Schema::hasColumn($this->getTable(), $column['field']) ) {
                    $query->$operation($this->getTable() . '.' . $column['field'], 'like', '%' . $search_term . '%');
                }
                break;
            case "integer":
                if( Schema::hasColumn($this->getTable(), $column['field']) && is_int( $search_term ) ) {
                    $query->$operation($this->getTable() . '.' . $column['field'], $search_term);
                }
                break;
            case "boolean":
                if( strtolower($search_term) == 'enabled' ){ $search_term = 1; }
                elseif( strtolower($search_term) == 'disabled' ){ $search_term = 0; }
                if( Schema::hasColumn($this->getTable(), $column['field']) && is_int( $search_term ) ) {
                    $query->$operation($this->getTable() . '.' . $column['field'], $search_term);
                }
                break;
            case "relation":
                $thisRelation = $this->relations[ (string)$column['field'] ];
                $rel_column = $thisRelation['column'];

                $alias = $search_aliases[ (string)$column['field'] ];
                if( is_array( $rel_column ) ){
                    foreach( $rel_column as $col ){
                        $query->$operation($alias . '.' . $col, 'like', '%' . $search_term . '%');
                    }
                } else {
                    $query->$operation($alias . '.' . $rel_column, 'like', '%' . $search_term . '%');
                }

                break;
            case "path":
                $alias = $search_aliases[ (string)$column['field'] ];
                $query->$operation( $alias . '.url', 'like', '%' . $search_term . '%' );
                break;
        }
    }
}