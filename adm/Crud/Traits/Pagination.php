<?php
namespace Adm\Crud\Traits;

trait Pagination {

    /**
     * Internal variable determining the default number of items per page to show
     * at a listing level
     *
     * @var int
     */
    private $pagination_limit   = 20;

    /**
     * Internal variable determining the options to show within the list view
     * 'Show:' <select> box. Associative array of count => label
     *
     * @var array
     */
    private $show_number_options = [
        20 => "20",
        40 => "40",
        80 => "80",
        100 => "100",
        -1 => "All"
    ];

    /**
     * Get the current page number
     *
     * @return int
     */
    private function _getCurrentPageNumber(){
        $page = 1;
        if( request()->input('page') && request()->input('page') > 0 ){
            $page = (int)request()->input('page');
        }

        return $page;
    }

    /**
     * Gets the current offset for pagination
     */
    private function _getDbSkip(){
        $page = $this->_getCurrentPageNumber();

        return (($page-1) * $this->_getDbLimit());
    }

    /**
     * Gets the current limit for pagination
     */
    private function _getDbLimit(){
        $limit = $this->pagination_limit;
        if( request()->input('show') && array_key_exists(request()->input('show'), $this->show_number_options) ){
            $limit = (int)request()->input('show');
        }

        return $limit > -1 ? $limit : false;
    }

    /**
     * Sets the offset and limit parts of a query for pagination
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     */
    private function _dbPagination( &$query ){
        $limit = $this->_getDbLimit();
        if( $limit !== false ){
            $query->offset( $this->_getDbSkip() )->limit( $limit );
        }
    }

    /**
     * Generates an array used for listing pagination
     *
     * @param int $total
     * @param string $slug
     * @return array
     */
    private function _getPaginationArray( $total, $slug ){
        $page = $this->_getCurrentPageNumber();
        $offset = $this->_getDbSkip();
        $limit = $this->_getDbLimit();
        $total_pages = $limit !== false ? ceil($total / $limit) : 1;

        $url_gets = '?page=%d';
        $url_limit = '';
        if( $limit != $this->pagination_limit ){
            $url_limit = '?show=' . $limit;
            $url_gets .= '&show=' . $limit;
        }

        $invalid_gets = ['page', 'show'];
        foreach( request()->all() as $input => $value ){
            if( !in_array( $input, $invalid_gets ) ){
                $url_limit .= (($url_limit == '') ? '?' : '&') . $input . '=' . $value;
                $url_gets .= '&' . $input . '=' . $value;
            }
        }

        $to = $total;
        if( $limit !== false && ($offset + $limit) < $total ){
            $to = ($offset + $limit);
        }

        return array(
            'current_page' => $page,
            'from' => $total > 0 ? ($offset+1) : 0,
            'to' => $to,
            'total' => $total,
            'num_pages' => $total_pages,
            'visible_pages' => ($total_pages < 5) ? $total_pages : 5,
            'show_next' => $page < $total_pages,
            'show_prev' => $page > 1,
            'url_base' => route('adm.path', [$slug]) . $url_limit,
            'url_structure' => route('adm.path', [$slug]) . $url_gets
        );
    }
}