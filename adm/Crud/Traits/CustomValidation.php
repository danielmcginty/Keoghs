<?php
namespace Adm\Crud\Traits;

// The Validator class is registered on config/app.php so we don't need to provide a namespace
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Adm\Crud\Fields\Path;
use Adm\Models\Url;

trait CustomValidation {

    /**
     * Custom Validation Rules can be placed in here to make them globally accessible to the CRUD
     *
     * @param \Adm\Crud\LaraCrud $crud
     */
    protected function applyCustomValidationRules( $crud ){

        Validator::extend( 'alpha_dash_no_underscore', function( $attribute, $value, $parameters, $validator ){
            return preg_match( '/^[a-zA-Z0-9\\-]*$/', $value );
        });

        if( class_exists( '\Adm\Crud\Fields\Path' ) ){
            Validator::extend( 'path', function( $attribute, $value, $parameters, $validator ) use ( $crud ){

                $value = request()->input( 'path' );

                $PathField = new Path( $crud );

                if( isset( $value['system_page'] ) && $value['system_page'] ){
                    $path = $value['url'];
                } else {
                    $path = $PathField->generatePath( $value['url'], request()->all() );
                    if( empty( $path ) ){
                        return true;
                    }
                }

                $urls = Url::where( 'url', $path )->get();
                if( $crud->getAction() != "store" ){
                    $id = request()->route()->parameters()['id'];
                    DB::enableQueryLog();
                    $urls = Url::where( 'url', $path )
                        ->whereRaw( "CONCAT(table_name,'-', table_key) != '" . $crud->getTable()."-".$id . "'" )
                        ->where( 'language_id', $crud->language_id )
                        ->get();
                }

                return count( $urls ) < 1;
            });
        }

        // We can also check to see if any modules have their own validation rules.
        $modules_dir = base_path( 'modules' );
        $modules = array_filter( glob( $modules_dir . '/*', GLOB_ONLYDIR ) );

        $available_modules = (array)config( 'modules.available' );

        foreach( $modules as $module_path ) {
            $path_parts = explode( '/', $module_path );
            $module_ns = end( $path_parts );

            if( !in_array( $module_ns, $available_modules ) ){
                continue;
            }

            if( class_exists( '\\Modules\\' . $module_ns . '\\Adm\\Crud\\CustomValidation' ) ) {
                $class_ref = '\\Modules\\' . $module_ns . '\\Adm\\Crud\\CustomValidation';
                /** @var \Adm\Interfaces\ValidationInterface $validation_class */
                $validation_class = new $class_ref();

                $validation_class->applyCustomValidationRules( $crud );
            }
        }

    }

    /**
     * If we've created any custom validation rules, we can set custom error messages for those rules in here
     */
    protected function applyCustomValidationMessages(){

        Validator::replacer( 'alpha_dash_no_underscore', function( $message, $attribute, $rule, $parameters ){
            $message = sprintf(
                "%s can only contain letters, numbers and dashes (-)",
                ucwords( str_replace( array( "_", "-" ), " ", $attribute ) )
            );

            return $message;
        });

        Validator::replacer( 'path', function( $message, $attribute, $rule, $parameters ){
            return "That " . $attribute . " is already being used.";
        });

        // We can also check to see if any modules have their own validation error messages.
        $modules_dir = base_path( 'modules' );
        $modules = array_filter( glob( $modules_dir . '/*', GLOB_ONLYDIR ) );

        $available_modules = (array)config( 'modules.available' );

        foreach( $modules as $module_path ) {
            $path_parts = explode( '/', $module_path);
            $module_ns = end( $path_parts );

            if( !in_array( $module_ns, $available_modules ) ){
                continue;
            }

            if( class_exists( '\\Modules\\' . $module_ns . '\\Adm\\Crud\\CustomValidation' ) ) {
                $class_ref = '\\Modules\\' . $module_ns . '\\Adm\\Crud\\CustomValidation';
                /** @var \Adm\Interfaces\ValidationInterface $validation_class */
                $validation_class = new $class_ref();

                $validation_class->applyCustomValidationMessages();
            }
        }

    }
}