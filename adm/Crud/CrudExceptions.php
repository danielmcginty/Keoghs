<?php
/**
 * Created by PhpStorm.
 * User: santo
 * Date: 06/08/2015
 * Time: 10:20
 */

namespace Adm\Crud;

use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class CrudExceptions
 * @package Adm\Crud
 *
 * Custom Exception class for the CMS
 */
class CrudExceptions extends HttpException {
    public $extra_data;

    public function __construct($statusCode, $message = null, \Exception $previous = null, array $headers = array(), $code = 0) {
        parent::__construct($statusCode, $message, $previous, $headers, $code);
    }
}