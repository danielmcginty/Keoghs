<?php
namespace Adm\Crud;

class LaraCrudField {

    /**
     * @var LaraCrud
     */
    protected $LaraCrud;

    /**
     * The current field type being accessed
     * Defaults to 'string' as this is the default field / input type used
     *
     * @var string
     */
    protected $field_type = 'string';

    /**
     * The current field type being accessed, represented as it's class
     *
     * @var null|\Adm\Crud\Fields\BaseField
     */
    protected $field_class = null;

    /**
     * Construct
     * Optionally Loads the field type directly into the instance on load
     * Returns itself to allow method chaining
     *
     * @param LaraCrud $LaraCrud
     * @param string|null $field_type
     */
    public function __construct( LaraCrud $LaraCrud, $field_type = null ){
        $this->LaraCrud = $LaraCrud;

        if( !is_null( $field_type ) ){
            $this->field_type = $field_type;
        }

        $this->_setFieldClass();

        return $this;
    }

    /**
     * Set Type
     * Sets the current field type against the class instance
     * Returns itself to allow method chaining
     *
     * @param string $type
     * @return LaraCrudField
     */
    public function setType( $type ){
        if( empty( $type ) ){ $type = 'string'; }

        $this->field_type = $type;
        $this->_setFieldClass();

        return $this;
    }

    /**
     * Get Blade Name
     * Returns the partial blade name used when finding column & form field
     * templates for a field type
     *
     * @return string
     */
    public function getBladeName(){
        if( !is_null( $this->field_class ) && method_exists( $this->field_class, 'getBladeName' ) ){
            return $this->field_class->getBladeName();
        }

        return $this->field_type;
    }

    /**
     * Show
     * Prepares a field ready for displaying on the list view
     *
     * @param array $column
     * @param \Adm\Crud\CrudModel $item
     */
    public function show( $column, &$item ){
        if( !is_null( $this->field_class ) && method_exists( $this->field_class, 'show' ) ){
            $this->field_class->show( $column, $item );
        }
    }

    /**
     * Prepare
     * Prepares a field ready for display on the form view
     *
     * @param array $column
     * @param \Adm\Crud\CrudModel $item
     */
    public function prepare( &$column, &$item ){
        if( !is_null( $this->field_class ) && method_exists( $this->field_class, 'prepare' ) ){
            $this->field_class->prepare( $column, $item );
        }
    }

    /**
     * Save
     * Parses a the POST data for a field into a value that can be saved in
     * the database
     *
     * @param mixed $posted_value
     * @param mixed $original_value
     * @param string $field
     * @return mixed
     */
    public function save( $posted_value, $original_value, $field ){
        if( !is_null( $this->field_class ) && method_exists( $this->field_class, 'save' ) ){
            return $this->field_class->save( $posted_value, $original_value, $field );
        }

        return $posted_value;
    }

    /**
     * After Saved
     * Perform an action on an item AFTER it has been stored in the system.
     * Useful for functions that need an item's ID.
     *
     * @param string $primary_key_column
     * @param array $post_data
     * @param \Adm\Crud\CrudModel $item
     * @param string $field
     */
    public function afterSaved( $primary_key_column, $post_data, $item, $field ){
        if( !is_null( $this->field_class ) && method_exists( $this->field_class, 'afterSaved' ) ){
            $this->field_class->afterSaved( $primary_key_column, $post_data, $item, $field );
        }
    }

    /**
     * Before Delete
     * Perform an action on an item BEFORE it has been deleted from the system
     *
     * @param int $item_id
     */
    public function beforeDelete( $item_id ){
        if( !is_null( $this->field_class ) && method_exists( $this->field_class, 'beforeDelete' ) ){
            $this->field_class->beforeDelete( $item_id );
        }
    }

    /**
     * After Delete
     * Perform an action on an item AFTER it has been deleted from the system
     *
     * @param int $item_id
     */
    public function afterDelete( $item_id ){
        if( !is_null( $this->field_class ) && method_exists( $this->field_class, 'afterDelete' ) ){
            $this->field_class->afterDelete( $item_id );
        }
    }

    /**
     * Get Validation Rules
     * Updates the current set of validation rules based on any field specific
     * constraints
     *
     * @param array $existing_rules
     * @param array $post_data
     * @param array $column
     */
    public function getValidationRules( &$existing_rules, $post_data, $column ){
        if( !is_null( $this->field_class ) && method_exists( $this->field_class, 'getValidationRules' ) ){
            $this->field_class->getValidationRules( $existing_rules, $post_data, $column );
        }
    }

    /**
     * Get Validation Messages
     * Updates the current set of validation messages based on any field
     * specific validation rules
     *
     * @param array $existing_messages
     * @param array $post_data
     * @param array $column
     */
    public function getValidationMessages( &$existing_messages, $post_data, $column ){
        if( !is_null( $this->field_class ) && method_exists( $this->field_class, 'getValidationMessages' ) ){
            $this->field_class->getValidationMessages( $existing_messages, $post_data, $column );
        }
    }

    /**
     * AJAX Method Exists
     * Based on the provided method name, check to see if the AJAX method
     * is available in any of the installed field types
     *
     * @param string $ajax_method
     * @return bool
     */
    public function ajaxMethodExists( $ajax_method ){
        return $this->_mapAjaxCallToFieldType( $ajax_method ) !== false;
    }

    /**
     * AJAX Method Call
     * Based on the provided method name, call it from the appropriate,
     * available field type
     *
     * @param string $ajax_method
     * @return mixed
     */
    public function ajaxMethodCall( $ajax_method ){
        $field_type = $this->_mapAjaxCallToFieldType( $ajax_method );
        if( !$field_type ){ return false; }

        $original_type = $this->field_type;

        $this->setType( $field_type );
        $return_value = $this->field_class->{$ajax_method}();
        $this->setType( $original_type );

        return $return_value;
    }

    /**
     * Set Field Class
     * Based on the current instance's field type, attempt to load the
     * relevant class from the CRUD system, ready for use within functions
     */
    private function _setFieldClass(){
        $this->field_class = null;

        if( empty( $this->field_type ) ){
            throw new CrudExceptions( 500, 'Field Type Not Set!' );
        }

        $class_name = ucfirst( \Illuminate\Support\Str::camel( $this->field_type ) );
        $fq_class_name = '\Adm\Crud\Fields\\' . $class_name;

        if( class_exists( $fq_class_name ) ){
            $this->field_class = new $fq_class_name( $this->LaraCrud );
        }
    }

    /**
     * Map AJAX Call To Field Type
     * Based on the provided method name, check to see if it exists in any
     * of the available field types.
     * If so, return the field type, else return false.
     *
     * @param string $ajax_method
     * @return bool|string
     */
    private function _mapAjaxCallToFieldType( $ajax_method ){
        $fields_dir = base_path( '/adm/Crud/Fields' );
        $fields = array_filter( glob( $fields_dir . '/*' ) );

        foreach( $fields as $field_path ){
            $path_parts = explode( '/', $field_path );
            $file = end( $path_parts );

            if( file_exists( $fields_dir . '/' . $file ) ){
                $field_type = str_replace( '.php', '', $file );
                $field_class = \Illuminate\Support\Str::camel( $field_type );
                $class = '\Adm\Crud\Fields\\' . ucfirst( $field_class );

                if( class_exists( $class ) && method_exists( $class, $ajax_method ) ){
                    return $field_type;
                }
            }
        }

        return false;
    }

}