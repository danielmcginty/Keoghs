<?php
namespace Adm\Crud;

/**
 * Class NonDeletableCrudModel
 * The Non-Deletable Crud Model allows CRUD functions to be performed on records with a single primary key
 * BUT
 * Without a deleted_at timestamp field
 *
 * @package Adm\Crud
 */
class NonDeletableCrudModel extends CrudModel {

    /**
     * Override the bootSoftDeletes function to do NOTHING if we don't have a deleted_at timestamp
     */
    public static function bootSoftDeletes(){}

}