<?php

namespace Adm\Crud;

use App\Models\Traits\HasCompositePrimaryKey;
use Illuminate\Support\Facades\Schema;

/**
 * Class CompositeCrudModel
 * The Composite Crud Model allows CRUD functions to be performed on records with a composite primary key
 * This needed to be created as a separate model from the standard CrudModel because of a few custom functions
 *
 * @package Adm\Crud
 */
class CompositeCrudModel extends CrudModel {
    // We've got a composite primary key, so include the relevant trait
    use HasCompositePrimaryKey;

    /**
     * The static instance of this class is used to retain a table and primary key fields between
     * new instances of the CompositeCrudModel being created
     *
     * @var CrudModel
     */
    private static $instance;

    /**
     * The init function is called before the CompositeCrudModel is ever used.
     * This builds the static instance so the table is kept across instances of the model
     *
     * @param $table
     * @param null $columns
     */
    public static function init($table, $columns = null){
        self::$instance = new CrudModel();
        self::$instance->table = $table;
        self::$instance->timestamps = true;
        if( $columns ){
            self::$instance->columns = array_keys( $columns );
            if( !in_array( 'created_at', array_keys( $columns ) ) || !in_array( 'updated_at', array_keys( $columns ) ) ){
                self::$instance->timestamps = false;
            }
        }
    }

    /**
     * Constructor
     * If an instance has been created, use the set attributes to populate a new instance of the model,
     * thus keeping things like the table name consistent
     */
    public function __construct(){
        parent::__construct();
        if( self::$instance ){
            $this->table = self::$instance->table;
            $this->fillable = self::$instance->columns;
            $this->timestamps = self::$instance->timestamps;
        }
    }

    /**
     * Sets the primary key field for the model, also saving into the instance
     *
     * @param $pk
     */
    public function setPrimaryKey( $pk ){
        $this->primaryKey = $pk;
        self::$instance->primaryKey = $pk;
    }

    /**
     * Override the softDelete function so that it doesn't fall over with a
     * composite primary key
     */
    protected function runSoftDelete(){
        $query = $this->newQueryWithoutScopes();
        foreach( $this->getKeyName() as $key ){
            if( $this->$key ){
                $query->where( $key, '=', $this->$key );
            }
        }

        if( Schema::hasColumn( $this->getTable(), $this->getDeletedAtColumn() ) ) {
            $this->{$this->getDeletedAtColumn()} = $time = $this->freshTimestamp();

            $query->update([$this->getDeletedAtColumn() => $this->fromDateTime($time)]);
        } else {
            $query->delete();
        }
    }

}
