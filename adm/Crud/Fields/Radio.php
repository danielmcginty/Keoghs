<?php
namespace Adm\Crud\Fields;

class Radio extends BaseField {

    public function show( $column, &$item ){
        $output_label = '';
        foreach( (array)$column['options'] as $value => $label ){
            if( $value == $item->{ $column['field'] } ){
                $output_label = $label;
            }
        }
        $item->{ $column[ 'field' ] } = $output_label;
    }

    public function prepare( &$column, &$item ){
        $column['selected'] = false;
        if( !is_object( $item ) ){ return; }

        $column['selected'] = data_get( $item, transform_key( $column['field'] ) );
    }

}