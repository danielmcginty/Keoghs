<?php
namespace Adm\Crud\Fields;

use Adm\Models\Url;
use Illuminate\Support\Facades\DB;

/**
 * Class MultiRelation
 * CMS Field Type class for Multi-Relation Fields
 */
class MultiRelation extends BaseField {

    /**
     * Show
     * Function called before a field is displayed in the list view
     *
     * @param array $column
     * @param \Adm\Crud\CrudModel $item
     */
    public function show( $column, &$item ){
        $thiMultiRelation = $this->LaraCrud->multiRelations[$column['field']];
        $relation_table = $thiMultiRelation['relation_table'];
        $selection_table = $thiMultiRelation['selection_table'];
        $selection_id = $thiMultiRelation['selection_id'];
        $relation_id = $thiMultiRelation['relation_id'];
        $relation_priority = $thiMultiRelation['relation_priority'];
        $selection_title = $thiMultiRelation['selection_title'];

        $relation_id_selection_id = !empty($thiMultiRelation['selection_id_save_alias']) ? $thiMultiRelation['selection_id_save_alias'] : $thiMultiRelation['selection_id'];

        $opt = DB::table( $relation_table )
            ->leftJoin( $selection_table, $selection_table . '.' . $selection_id, '=', $relation_table . '.' . $relation_id_selection_id )
            ->where( $relation_id, $item->{$relation_id} );

        if( $relation_priority ){
            $opt = $opt->orderBy( $relation_table . '.' . $relation_priority );
        }

        $opt = $opt->pluck( $selection_table . '.' . $selection_title );

        $opts = array();
        foreach( $opt as $option ){
            $opts[] = $option;
        }

        $item->{$column['field']} = implode( ", ", $opts );
    }

    /**
     * Prepare
     * Function called before a field is displayed in the form view
     *
     * @param array $column
     * @param \Adm\Crud\CrudModel $item
     */
    public function prepare( &$column, &$item ){
        $class_name = $this->LaraCrud->_getCrudModel();

        $class_name::init($this->LaraCrud->multiRelations[$column['field']]['selection_table']);
        $opts_qry = $class_name::distinct();

        if( $this->LaraCrud->multiRelations[$column['field']]['selection_table'] == 'pages' && !\Auth::user()->userGroup->super_admin ){
            $system_pages = Url::where('table_name', 'pages')->where('system_page', 1)->pluck('table_key');

            $opts_qry->whereNotIn($this->LaraCrud->multiRelations[$column['field']]['selection_id'], $system_pages);
        }

        $opts = $opts_qry->orderBy( $this->LaraCrud->multiRelations[$column['field']]['selection_title'] )
            ->pluck( $this->LaraCrud->multiRelations[$column['field']]['selection_title'], $this->LaraCrud->multiRelations[$column['field']]['selection_id'] );

        $column['values'] = $opts->toArray();
        $this->LaraCrud->initModel();
        $column['selected'] = [];

        if( !empty( $item ) ){
            $relation_id_selection_id = !empty($this->LaraCrud->multiRelations[$column['field']]['selection_id_save_alias']) ? $this->LaraCrud->multiRelations[$column['field']]['selection_id_save_alias'] : $this->LaraCrud->multiRelations[$column['field']]['selection_id'];

            $selected = \DB::table( $this->LaraCrud->multiRelations[$column['field']]['relation_table'] )
                ->where( $this->LaraCrud->multiRelations[$column['field']]['relation_id'], $item->{ $this->LaraCrud->getPrimaryColumn() } );

            if( $this->LaraCrud->multiRelations[$column['field']]['relation_priority'] ){
                $selected = $selected->orderBy( $this->LaraCrud->multiRelations[$column['field']]['relation_priority'] );
            }

            $column['selected'] = $selected->pluck( $relation_id_selection_id );
        }

        if( !empty( $this->LaraCrud->request->old( $column['field'] ) ) ){
            $column['selected'] = $this->LaraCrud->request->old( $column['field'] );
        }

        if( isset( $this->LaraCrud->multiRelations[$column['field']]['adm_link'] ) ){
            $column['admin_relation_link'] = $this->LaraCrud->multiRelations[$column['field']]['adm_link'];
        }
    }

    /**
     * After Saved
     * Function called after an item is saved.
     *
     * @param mixed $item_primary
     * @param array $input
     * @param \Adm\Crud\CrudModel $item
     * @param string $field
     */
    public function afterSaved( $item_primary, $input, $item, $field ){
        if( is_array( $item_primary ) ){
            $item_primary = $item_primary[ $this->LaraCrud->getPrimaryColumn() ];
        } else {
            $item_primary = $item->{ $this->LaraCrud->getPrimaryColumn() };
        }

        foreach( $this->LaraCrud->multiRelations as $col => $opt ){
            \DB::table($opt['relation_table'])->where($opt['relation_id'], $item_primary)->delete();

            if( !empty( $input[$col] ) ){
                foreach( $input[$col] as $sort => $v ){
                    $save_id = !empty($opt['selection_id_save_alias']) ? $opt['selection_id_save_alias'] : $opt['selection_id'];

                    $insert = [
                        $opt['relation_id']     => $item_primary,
                        $save_id                => $v,
                    ];

                    if( $opt['relation_priority'] ){
                        $insert[$opt['relation_priority']] = $sort;
                    }

                    \DB::table($opt['relation_table'])->insert($insert);
                }
            }
        }
    }

    /**
     * After Delete
     * Function called after an item is deleted from the database.
     *
     * @param int $item_primary
     */
    public function afterDelete( $item_primary ){
        foreach( $this->LaraCrud->multiRelations as $col => $opt ){
            \DB::table( $opt['relation_table'] )->where( $opt['relation_id'], $item_primary )->delete();
        }
    }

}