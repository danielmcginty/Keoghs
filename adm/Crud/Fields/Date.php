<?php
namespace Adm\Crud\Fields;

use Carbon\Carbon;

/**
 * Class Date
 * CMS Field Type class for Date Fields
 */
class Date extends BaseField {

    /**
     * Show
     * Function called before a field is displayed in the list view
     *
     * @param array $column
     * @param \Adm\Crud\CrudModel $item
     */
    public function show( $column, &$item ){
        $item->{$column['field']} = Carbon::parse( $item->{$column['field']} )->format( "d/m/Y" );
    }

    /**
     * Prepare
     * Function called before a field is displayed in the form view
     *
     * @param array $column
     * @param \Adm\Crud\CrudModel $item
     */
    public function prepare( &$column, &$item ){
        if( $item ){
            $item_value = data_get( $item, transform_key( $column['field'] ) );
            if( stristr( $column['field'], 'page_builder' ) !== false ){
                $page_builder = $item->page_builder;
                $pb_key = str_replace( 'page_builder.', '', transform_key( $column['field'] ) );
                data_set( $page_builder, $pb_key, $item_value );
                $item->page_builder = $page_builder;
            } else {
                $item->{$column['field']} = $item_value;
            }
        }
    }

    /**
     * Save
     * Function called before an item is saved into the database
     *
     * @param mixed $value
     * @return \Carbon\Carbon
     */
    public function save( $value ){
        if( $value ){
            return Carbon::createFromFormat("d/m/Y", $value);
        }
        return Carbon::now();
    }

}