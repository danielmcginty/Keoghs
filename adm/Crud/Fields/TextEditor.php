<?php
namespace Adm\Crud\Fields;

use App\Helpers\ShortCodeHelper;

class TextEditor extends BaseField {

    public function prepare( &$column, &$item ){
        $this->LaraCrud->wysiwygEnabled = true;

        if( $item ){
            $key = transform_key( $column['field'] );
            if( stristr( $key, 'page_builder.' ) !== false ){
                $page_builder = $item->page_builder;
                $key = str_replace( 'page_builder.', '', $key );
                $data = data_get( $page_builder, $key );
                data_set( $page_builder, $key, ShortCodeHelper::shortCodeToURLs( $data ) );
                $item->page_builder = $page_builder;
            } else {
                $item->{$column['field']} = ShortCodeHelper::shortCodeToURLs( $item->{$column['field']} );
            }
        }
    }

    public function save( $value ){
        return ShortCodeHelper::urlsToShortCode( $value );
    }

}