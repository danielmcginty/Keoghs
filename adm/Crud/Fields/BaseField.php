<?php
namespace Adm\Crud\Fields;

use Adm\Crud\LaraCrud;

/**
 * Class BaseField
 * Base Field extended by ALL CMS Field Types, setting the LaraCrud instance
 * against the class.
 */
abstract class BaseField {

    /**
     * Local class instance of the current LaraCrud class.
     *
     * @var \Adm\Crud\LaraCrud
     */
    protected $LaraCrud;

    /**
     * Construct
     * When loading, set the LaraCrud instance against this class instance.
     *
     * @param \Adm\Crud\LaraCrud $LaraCrud
     */
    public function __construct( LaraCrud $LaraCrud ){
        $this->LaraCrud = $LaraCrud;
    }

}