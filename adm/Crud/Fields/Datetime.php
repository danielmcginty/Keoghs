<?php
namespace Adm\Crud\Fields;

use Carbon\Carbon;

/**
 * Class DateTime
 * CMS Field Type class for Date/Time Fields
 */
class Datetime extends BaseField {

    /**
     * Show
     * Function called before a field is displayed in the list view
     *
     * @param array $column
     * @param \Adm\Crud\CrudModel $item
     */
    public function show( $column, &$item ){
        if( in_array( $column['field'], ['created_at', 'updated_at', 'deleted_at'] ) ){ return; }

        $item->{$column['field']} = Carbon::parse( $item->{$column['field']} )->format( "d/m/Y H:i" );
    }

    /**
     * Prepare
     * Function called before a field is displayed in the form view
     *
     * @param array $column
     * @param \Adm\Crud\CrudModel $item
     */
    public function prepare( &$column, &$item ){
        if( $item ){
            $item_value = data_get( $item, transform_key( $column['field'] ) );
            $datetime_data = [
                'date' => Carbon::parse($item_value)->format("d/m/Y"),
                'time' => Carbon::parse($item_value)->format("H:i")
            ];
            if( stristr( $column['field'], 'page_builder' ) !== false ){
                $page_builder = $item->page_builder;
                $pb_key = str_replace( 'page_builder.', '', transform_key( $column['field'] ) );
                data_set( $page_builder, $pb_key, $datetime_data );
                $item->page_builder = $page_builder;
            } else {
                $item->{$column['field']} = $datetime_data;
            }
        }
    }

    /**
     * Save
     * Function called before an item is saved into the database
     *
     * @param mixed $value
     * @return \Carbon\Carbon
     */
    public function save( $value ){
        if( $value ){
            $date = !empty( $value['date'] ) ? $value['date'] : date('d/m/Y');
            $time = !empty( $value['time'] ) ? $value['time'] : date('H:i');

            return Carbon::createFromFormat("d/m/Y H:i", $date . ' ' . $time);
        } else {
            return Carbon::now();
        }
    }

}