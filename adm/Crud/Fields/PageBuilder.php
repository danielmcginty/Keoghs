<?php
namespace Adm\Crud\Fields;

use Adm\Crud\LaraCrudField;
use Adm\Models\PageBuilderBlock;
use Adm\Models\PageBuilderBlockContent;
use Adm\Models\PageBuilderBlockType;
use Illuminate\Database\Eloquent\Collection;

class PageBuilder extends BaseField {

    private static $_AVAILABLE_BLOCKS = [];

    public function prepare( &$col, &$item ){
        $this->LaraCrud->wysiwygEnabled = true;

        $col['addable_page_builder_blocks'] = $this->_generatePageBuilderBlocks( true );

        uasort( $col['addable_page_builder_blocks'], function( $a, $b ){
            return strcmp( $a['name'], $b['name'] );
        });

        $col['page_builder_blocks'] = $this->_generatePageBuilderBlocks( false );

        $col['item_blocks'] = [];

        if( is_object( $item ) ){
            $this->_setItemPageBuilderBlockValues( $col, $item );
            $col['item_blocks'] = $item->page_builder_blocks;
        }

        /*
         * If we're creating a new item, and we haven't received any POSTed
         * blocks, add the default set (if there is a default set)
         */
        if( $this->LaraCrud->getAction() == 'create' && empty( $col['item_blocks'] ) ){
            $template_blocks = $this->_getPageBuilderBlockTemplate();
            if( !empty( $template_blocks ) ){
                foreach( $template_blocks as $index => $reference ){
                    $this->_addDefaultBlock( $reference, $col, $index );
                }
            }
        }

        if( empty( $col['item_blocks'] ) && $this->LaraCrud->pageBuilderNeedsMainContent ){
            $this->_addDefaultBlock( 'main_content', $col, 0 );
        }
    }

    public function afterSaved( $item_primary, $inputs, $item, $field ){
        // First, check if we're updating a multilingual item and get the primary ID field
        if( is_array( $item_primary ) ){
            foreach( $item_primary as $field => $value ){
                if( $field != "language_id" && $field != "version" ){
                    $item_primary = $value;
                }
            }
        }

        // Now delete existing page builder blocks set to that item
        PageBuilderBlock::where([
            'table_name' => $this->LaraCrud->getTable(),
            'table_key' => $item_primary,
            'language_id' => $this->LaraCrud->language_id
        ])->delete();

        $page_builder_blocks = $this->_generatePageBuilderBlocks( false );

        // Once deleted, we can save the new blocks
        if( isset( $inputs['page_builder'] ) && is_array( $inputs['page_builder'] ) ) {

            $LaraCrudField = new LaraCrudField( $this->LaraCrud );

            foreach ($inputs['page_builder'] as $sort_order => $page_builder_block) {

                $block_requested = isset( $page_builder_blocks[ $page_builder_block['reference'] ] ) ? $page_builder_blocks[ $page_builder_block['reference'] ] : false;
                $callbacks = [];
                $block_callbacks = [];
                if( $block_requested !== false && isset( $page_builder_block['content'] ) ) {

                    $class = new $block_requested['class']( $this->LaraCrud );
                    $class->reference = $page_builder_block['reference'];

                    $pb_columns = $class->generateColumns($sort_order);

                    foreach ($page_builder_block['content'] as $k => $v) {
                        $LaraCrudField->setType( $pb_columns[ $k ]['type'] );

                        //Callbacks
                        if (!empty($pb_columns[$k])) {
                            $callbacks[$k] = $pb_columns[$k];

                            $block_method = \Illuminate\Support\Str::camel("after_saved_" . $class->reference . "_block");
                            if( method_exists( $class, $block_method ) ){
                                $block_callbacks[$k] = $block_method;
                            }
                        }

                        $v = $LaraCrudField->save( $v, $item->{$pb_columns[$k]['field']}, $pb_columns[$k]['field'] );

                        $block_method = \Illuminate\Support\Str::camel("save_" . $class->reference . "_block");
                        if( method_exists( $class, $block_method ) ){
                            $class->{$block_method}( $v, $item->{$pb_columns[$k]['field']}, $pb_columns[$k]['field'] );
                        }

                        if ( (is_string($v) && strlen($v) !== 0) || (!is_string($v) && !empty($v)) ) {
                            $page_builder_block['content'][$k] = $v;
                        }
                    }

                }

                $new_block = new PageBuilderBlock();

                $new_block->reference = $page_builder_block['reference'];
                $new_block->table_name = $this->LaraCrud->getTable();
                $new_block->table_key = $item_primary;
                $new_block->language_id = $this->LaraCrud->language_id;
                $new_block->version = $item->version;

                $new_block->sort_order = $sort_order;
                $new_block->status = isset( $page_builder_block['status'] ) ? (int)$page_builder_block['status'] : 0;

                if( $new_block->save() ){
                    if( isset( $page_builder_block['content'] ) ) {
                        foreach ($page_builder_block['content'] as $field => $value) {
                            $block_content = new PageBuilderBlockContent();

                            $block_content->page_builder_block_id = $new_block->id;
                            $block_content->field = $field;
                            $block_content->value = $value;

                            $block_content->save();
                        }
                    }
                }

                foreach ($callbacks as $field => $column){
                    $LaraCrudField->setType( $column['type'] )->afterSaved( $item_primary, $inputs, $item, $pb_columns[ $field ]['field'] );
                }

                if( isset( $class ) ){
                    foreach( $block_callbacks as $field => $method ){
                        $class->{$method}($item_primary, $inputs, $item, $pb_columns[ $field ]['field']);
                    }
                }
            }
        }
    }

    public function getValidationRules( &$validation, $inputs ){
        if( isset( $inputs['page_builder'] ) && is_array( $inputs['page_builder'] ) ) {
            $page_builder_blocks = $this->_generatePageBuilderBlocks(false);
            foreach ($inputs['page_builder'] as $sort_order => $input_block) {
                $block_has_reference = isset( $page_builder_blocks[ $input_block['reference'] ] );
                if( $block_has_reference ){
                    $block_reference = $page_builder_blocks[ $input_block['reference'] ];

                    $class_name = $block_reference['class'];
                    $block_class = new $class_name( $this->LaraCrud );

                    foreach( $block_class->builder_fields as $field_name => $details ){
                        if( isset( $details['validation'] ) && !empty( $details['validation'] ) ) {

                            foreach( $details['validation'] as $validation_field_name => $rules ){
                                // If we're validating a set of sub fields
                                if( is_array( $rules ) ){

                                    // Create the set of rules for the sub field
                                    $separator = '.*.';
                                    if( isset( $details['array_validation_type'] ) && $details['array_validation_type'] == 'single' ){
                                        $separator = '.';
                                    }

                                    $field_str = 'page_builder.' . $sort_order . '.content.' . $field_name . $separator . $validation_field_name;
                                    $validation_str = implode('|', array_keys( $rules ) );

                                    $validation[ $field_str ] = $validation_str;

                                }
                                // Else, we're simply validating a main field
                                else {

                                    // So create the rule
                                    $field_str = 'page_builder.' . $sort_order . '.content.' . $field_name;
                                    $validation_rules = [];
                                    foreach( $details['validation'] as $rule => $message ){
                                        if( !is_array( $message ) ){
                                            $validation_rules[] = $rule;
                                        }
                                    }
                                    $validation_str = implode('|', $validation_rules);

                                    $validation[$field_str] = $validation_str;

                                }
                            }

                        }
                    }
                }
            }
        }
    }

    public function getValidationMessages( &$messages, $inputs ){
        if( isset( $inputs['page_builder'] ) && is_array( $inputs['page_builder'] ) ) {
            $page_builder_blocks = $this->_generatePageBuilderBlocks(false);
            foreach ($inputs['page_builder'] as $sort_order => $input_block) {
                $block_has_reference = isset( $page_builder_blocks[ $input_block['reference'] ] );
                if( $block_has_reference ){
                    $block_reference = $page_builder_blocks[ $input_block['reference'] ];

                    $class_name = $block_reference['class'];
                    $block_class = new $class_name( $this->LaraCrud );

                    foreach( $block_class->builder_fields as $field_name => $details ){
                        if( isset( $details['validation'] ) && !empty( $details['validation'] ) ) {

                            foreach( $details['validation'] as $validation_field_name => $rules ){
                                // If we're validating a set of sub fields
                                if( is_array( $rules ) ){

                                    // Create a field name, to use in conjunction with the condition to set the message
                                    $separator = '.*.';
                                    if( isset( $details['array_validation_type'] ) && $details['array_validation_type'] == 'single' ){
                                        $separator = '.';
                                    }

                                    $field_str = 'page_builder.' . $sort_order . '.content.' . $field_name . $separator . $validation_field_name;

                                    foreach( $rules as $rule => $message ){
                                        $rule = stristr( $rule, ':' ) === false ? $rule : substr( $rule, 0, stripos( $rule, ':' ) );
                                        $message_str = $field_str . '.' . $rule;
                                        $messages[ $message_str ] = $message;
                                    }

                                }
                                // Else, we're simply validating a main field
                                else {

                                    // So create the message
                                    $rule = stristr( $validation_field_name, ':' ) === false ? $validation_field_name : substr( $validation_field_name, 0, stripos( $validation_field_name, ':' ) );;
                                    $message = $rules;

                                    $message_str = 'page_builder.' . $sort_order . '.content.' . $field_name . '.' . $rule;
                                    $messages[ $message_str ] = $message;

                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Function called by the AssetType trait used to generate asset sizing for page builder blocks with
     * asset type fields within them
     *
     * @param $field
     * @return array
     */
    public function getPageBuilderAssetConfig( $field ){
        return $this->_getBlockFieldColumnExtras( $field, 'sizes' );
    }

    /**
     * For the given field, return an array of repeatable fields associated
     * with it
     *
     * @param string $field
     * @return array
     */
    public function getPageBuilderRepeatableFieldFields( $field ){
        return $this->_getBlockFieldColumnExtras( $field, 'repeatable_fields' );
    }

    /**
     * For the given field, return an array of grouped fields associated
     *
     * @param $field
     * @return array
     */
    public function getPageBuilderGroupedFields( $field ){
        return $this->_getBlockFieldColumnExtras( $field, 'grouped_fields' );
    }

    /**
     * For the provided field, pull out the data set within $column_extras for
     * that field, nested under the provided $extras_key
     *
     * E.g.
     * $column_extras['my_field']['sizes'] = ['single' => ['width' => 10]]
     * ->_getBlockFieldColumnExtras( 'my_field', 'sizes' ) would return
     * ['single' => ['width' => 10]]
     *
     * @param $field
     * @param $extras_key
     * @return array
     */
    protected function _getBlockFieldColumnExtras( $field, $extras_key ){
        $page_builder_blocks = $this->_generatePageBuilderBlocks();
        foreach ((array)$this->LaraCrud->request->input('page_builder') as $sort_order => $page_builder_block) {
            if( !isset( $page_builder_block['content'] ) ){ continue; }

            $current_block_field = false;
            foreach( (array)$page_builder_block['content'] as $block_field => $value ){
                $current_field = 'page_builder[' . $sort_order . '][content][' . $block_field . ']';

                // If this field is the field we're looking for, use that
                if( $field == $current_field ){
                    $current_block_field = $block_field;
                }
                // Else, if this field is the array containing the fields we're looking for, use that
                elseif( preg_replace( '/^(.*)(\\[\\d*\\])$/', '$1', $field ) == $current_field ){
                    // Get the array container for this field
                    $current_block_field = preg_replace( '/^(.*)(\\[\\d*\\])$/', '$1', $field );
                    // Now get the field name to use for the config.
                    $current_block_field = preg_replace( '/^(.*)\\[(.*)\\]$/', '$2', $current_block_field );
                }
                // Else, if we're referencing a repeatable field
                elseif( preg_replace( '/^(.*)(\\[\\d*\\])(.*)$/', '$1', $field ) == $current_field ){
                    // Get the array container for this field
                    $current_block_field = preg_replace( '/^(.*)(\\[\\d*\\])(.*)$/', '$1', $field );
                    $current_block_field = preg_replace( '/^(.*)\\[(.*)\\]$/', '$2', $current_block_field );

                    $block_field_required = preg_replace( '/^(.*)(\\[\\d*\\])(.*)$/', '$3', $field );
                    $block_field_required = preg_replace( '/^(.*)\\[(.*)\\]$/', '$2', $block_field_required );

                    $current_block_field = $current_block_field . '[repeatable_fields][' . $block_field_required . ']';
                }
            }

            if( $current_block_field !== false ){
                $block_requested = isset( $page_builder_blocks[ $page_builder_block['reference'] ] ) ? $page_builder_blocks[ $page_builder_block['reference'] ] : false;
                if( $block_requested !== false ){
                    $class = new $block_requested['class']( $this->LaraCrud );

                    if( !empty( $class->column_extras ) && \Illuminate\Support\Arr::get( $class->column_extras, transform_key( $current_block_field ) ) ){
                        $fields = \Illuminate\Support\Arr::get( $class->column_extras, transform_key( $current_block_field ) );
                        return isset( $fields[ $extras_key ] ) ? $fields[ $extras_key ] : [];
                    }
                }
            }
        }

        return [];
    }

    /**
     * AJAX function used to render a new page builder block.
     * Output through JSON to append to the page.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax_get_page_builder_block(){
        $reference = $this->LaraCrud->request->input('reference');
        $index = $this->LaraCrud->request->input('index');
        $table_name = $this->LaraCrud->request->input( 'table_name' );

        if( $table_name ){
            $this->LaraCrud->setTable( $table_name );
        }

        $page_builder_blocks = $this->_generatePageBuilderBlocks();

        $block_requested = isset( $page_builder_blocks[ $reference ] ) ? $page_builder_blocks[ $reference ] : false;
        if( $block_requested === false ){
            return response()->json( ['success' => false, 'message' => 'Couldn\'t find that page builder block.'] );
        }

        /** @var \Adm\Models\PageBuilder\PageBuilderBase $class */
        $class = new $block_requested['class']( $this->LaraCrud );

        if( method_exists( $class, 'callbackBeforePrepare' ) ){
            $class->callbackBeforePrepare( $this->LaraCrud );
        }

        $class->reference = $reference;
        $columns = $class->generateColumns( $index );

        $CrudField = new LaraCrudField( $this->LaraCrud );
        $item = null;
        foreach( $columns as $ind => $column ){

            if( !is_null( $column['default'] ) ){
                data_set( $item, 'page_builder.' . $index . '.content.' . transform_key( $ind ), $column['default'] );
            }

            $CrudField->setType( $column['type'] )->prepare( $columns[ $ind ], $item );

            $block_method = \Illuminate\Support\Str::camel("prepare_" . $reference . "_block");
            if( method_exists( $class, $block_method ) ){
                $class->{$block_method}( $columns[$ind], $item);
            }
        }

        $template = $class->getTemplate( $this->LaraCrud->defaultData(array()), $index, $columns, $item, true, true );

        $json = [
            'success' => true,
            'template' => $template
        ];
        if( isset( $class->ajax_callbacks ) ){
            $json['callbacks'] = $class->ajax_callbacks;
        }

        return response()->json( $json );
    }

    /**
     * AJAX function used to call a function on a provided page builder block class
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax_page_builder_function(){
        $post_reference = $this->LaraCrud->request->input('page_builder_reference');
        $post_function = $this->LaraCrud->request->input('page_builder_function');

        if( $post_reference ) {
            $page_builder_blocks = $this->_generatePageBuilderBlocks(false);
            if( array_key_exists( $post_reference, $page_builder_blocks ) ){
                $block_class_name = $page_builder_blocks[ $post_reference ]['class'];

                $block_class = new $block_class_name( $this->LaraCrud );

                if( method_exists( $block_class, $post_function ) ){
                    return $block_class->{$post_function}( $this->LaraCrud->request );
                }
            }
        }

        return response()->json( ['success' => false] );
    }

    /**
     * Grab all available page builder blocks from the database to pass down to the form template
     *
     * @param bool $default
     * @return array
     */
    private function _generatePageBuilderBlocks( $default = true ){
        if( !empty( self::$_AVAILABLE_BLOCKS ) ){ return self::$_AVAILABLE_BLOCKS; }

        $page_builder_blocks = [];
        $block_types = $this->_getPageBuilderBlockTypes( $default );

        $available_modules = (array)config( 'modules.available' );

        foreach( $block_types as $block_type ){
            $core_ns = '\\Adm\\Models\\PageBuilder';
            $this_class = $core_ns . '\\' . ucfirst( \Illuminate\Support\Str::camel( $block_type->reference ) );

            $custom_block_class = false;
            if( class_exists( $this_class ) ){
                $custom_block_class = new $this_class( $this->LaraCrud );
            } else {
                $modules_dir = base_path( 'modules' );
                $modules = array_filter( glob($modules_dir.'/*', GLOB_ONLYDIR) );

                foreach( $modules as $module_path ) {
                    $path_parts = explode('/', $module_path);
                    $module_ns = end($path_parts);

                    if( !in_array( $module_ns, $available_modules ) ){
                        continue;
                    }

                    $modular_class = '\\Modules\\' . $module_ns . '\\Adm\\Models\\PageBuilder\\' . ucfirst( \Illuminate\Support\Str::camel( $block_type->reference ) );
                    if( class_exists( $modular_class ) ) {
                        $custom_block_class = new $modular_class( $this->LaraCrud );
                    }
                }
            }

            if( $custom_block_class !== false ) {
                $page_builder_blocks[ $block_type->reference ] = [
                    'reference' => $block_type->reference,
                    'name' => $custom_block_class->display_name,
                    'description' => $custom_block_class->description,
                    'fields' => $custom_block_class->builder_fields,
                    'class' => $custom_block_class,
                    'editable' => $block_type->editable,
                    'deletable' => $block_type->deletable
                ];

                if( isset( $custom_block_class->dynamic_scripts ) ){
                    foreach( $custom_block_class->dynamic_scripts as $script ){
                        $this->LaraCrud->pageBuilderDynamicScripts[ $script ] = $script;
                    }
                }
            }
        }

        self::$_AVAILABLE_BLOCKS = $page_builder_blocks;

        return $page_builder_blocks;
    }

    /**
     * Get block types from the database
     *
     * @param bool $default
     * @return mixed
     */
    private function _getPageBuilderBlockTypes( $default = true ){
        $type_names = [];

        // Skip the database check here and just load all PB models as are in the system
        $core_path = base_path( 'adm/Models/PageBuilder' );
        $core_models = array_filter( glob($core_path.'/*') );
        foreach( $core_models as $core_model ){
            if( is_file( $core_model ) ){
                $model_parts = explode( '/', $core_model );
                $model_file = end( $model_parts );
                $model_class = str_replace( '.php', '', $model_file );

                if( $model_class == 'PageBuilderBase' ){ continue; }

                $type_names[] = \Illuminate\Support\Str::snake( $model_class );
            }
        }

        $modules_dir = base_path( 'modules' );
        $modules = array_filter( glob($modules_dir.'/*', GLOB_ONLYDIR) );

        foreach( $modules as $module_path ) {
            $module_models = glob( $module_path . '/Adm/Models/PageBuilder/*' );
            foreach( $module_models as $model ){
                if( is_file( $model ) ){
                    $model_parts = explode( '/', $model );
                    $model_file = end( $model_parts );
                    $model_class = str_replace( '.php', '', $model_file );

                    $type_names[] = \Illuminate\Support\Str::snake( $model_class );
                }
            }
        }

        $types = [];
        foreach( $type_names as $type_name ){
            $Type = new \StdClass();
            $Type->reference = $type_name;
            $Type->deletable = 1;
            $Type->editable = 1;

            $types[] = $Type;
        }

        return $types;

        if( $default ){
            return PageBuilderBlockType::where('deletable', 1)->get();
        }
        return PageBuilderBlockType::get();
    }

    /**
     * If we have a current item, get the current page builder blocks saved to the item.
     *
     * @param $col
     * @param $item
     */
    private function _setItemPageBuilderBlockValues( $col, &$item ){
        // Before doing anything, we need to determine the current set of page builder blocks.
        // So, if we have POST data through the old() function - we can assume that validation has failed
        if( $this->LaraCrud->request->old('page_builder') ){

            // So, reconstruct the blocks from the POST
            $old_inputs = [];
            foreach( $this->LaraCrud->request->old('page_builder') as $sort_order => $block ){
                if( !isset( $block['reference'] ) ){ continue; }

                $this_block = new \StdClass();
                $this_block->sort_order = $sort_order;
                $this_block->reference = $block['reference'];
                $this_block->status = isset( $block['status'] ) ? $block['status'] : 0;
                $this_block->content = isset( $block['content'] ) ? $block['content'] : [];

                $old_inputs[ $sort_order ] = $this_block;
            }
            $item_blocks = new Collection( $old_inputs );

            // And set the block values against the item
            $item->page_builder = $item_blocks;

        }
        // Else, we don't have POST data - so assume we're loading the form for the first time
        else {

            // So, get the blocks from the database
            $item_blocks = PageBuilderBlock::where([
                'table_name' => $this->LaraCrud->getTable(),
                'table_key' => $item->{$this->LaraCrud->getPrimaryColumn()},
                'language_id' => $item->language_id,
                'version' => $item->version
            ])->get();

        }

        // We'll need to set the item to the Form instance manually, because the template is rendered
        // before the Form is.
        \Form::setModel( $item );

        $page_builder_blocks = [];
        $CrudField = new LaraCrudField( $this->LaraCrud );
        foreach( $item_blocks as $item_block ){

            $class_name = isset($col['page_builder_blocks'][ $item_block->reference ]) ? $col['page_builder_blocks'][ $item_block->reference ]['class'] : false;
            if( $class_name === false ){ continue; }

            /** @var \Adm\Models\PageBuilder\PageBuilderBase $class */
            $class = new $class_name( $this->LaraCrud );

            if( method_exists( $class, 'callbackBeforePrepare' ) ){
                $class->callbackBeforePrepare( $this->LaraCrud );
            }

            $columns = $class->generateColumns( $item_block->sort_order );

            foreach( $columns as $ind => $column ){
                $CrudField->setType( $column['type'] )->prepare( $columns[ $ind ], $item );

                $block_method = \Illuminate\Support\Str::camel("prepare_" . $item_block->reference . "_block");
                if( method_exists( $class, $block_method ) ){
                    $class->{$block_method}( $columns[ $ind ], $item );
                }
            }

            $class->reference = $item_block->reference;
            $page_builder_blocks[] = [
                'reference' => $item_block->reference,
                'content' => isset($item_block->content) ? $item_block->content : [],
                'template' => $class->getTemplate( $this->LaraCrud->defaultData(array()), $item_block->sort_order, $columns, $item, $item_block->status )
            ];
        }

        $item->page_builder_blocks = $page_builder_blocks;
    }

    /**
     * Returns an array of Page Builder Block References to use as a template
     * for the resources being accessed.
     * Note - This is only used when creating a new item.
     *
     * @return array
     */
    private function _getPageBuilderBlockTemplate(){
        $template_blocks = [];

        $callback = \Illuminate\Support\Str::camel( 'get_' . $this->LaraCrud->slug . '_page_builder_template' );
        if( method_exists( $this->LaraCrud->caller, $callback ) ){
            $template_blocks = $this->LaraCrud->caller->{$callback}();
        }

        return $template_blocks;
    }

    /**
     * Add Default Block
     * Adds a default, empty block to the column set (item fields) based on
     * the provided reference (block reference) and index (block sort order).
     *
     * @param $reference
     * @param $col
     * @param $index
     */
    private function _addDefaultBlock( $reference, &$col, $index ){
        $page_builder_blocks = $this->_generatePageBuilderBlocks( false );

        $block_requested = isset( $page_builder_blocks[ $reference ] ) ? $page_builder_blocks[ $reference ] : false;
        if( $block_requested !== false ) {

            /** @var \Adm\Models\PageBuilder\PageBuilderBase $class */
            $class = new $block_requested['class']( $this->LaraCrud );
            $class->reference = $reference;

            if( method_exists( $class, 'callbackBeforePrepare' ) ){
                $class->callbackBeforePrepare( $this->LaraCrud );
            }

            $columns = $class->generateColumns($index);

            $CrudField = new LaraCrudField( $this->LaraCrud );
            foreach( $columns as $col_index => $column ){
                $temp_item = null;
                $CrudField->setType( $column['type'] )->prepare( $columns[ $col_index ], $temp_item );
            }

            $template = $class->getTemplate( $this->LaraCrud->defaultData( array() ), $index, $columns );

            $col['item_blocks'][] = [
                'reference' => $reference,
                'content' => [],
                'template' => $template
            ];
        }
    }

}