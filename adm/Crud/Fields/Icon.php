<?php
namespace Adm\Crud\Fields;

/**
 * Class Icon
 * CMS Field Type class for Icon Fields
 */
class Icon extends BaseField {

    /**
     * Prepare
     * Function called before a field is displayed in the form view
     *
     * @param array $column
     * @param \Adm\Crud\CrudModel $item
     */
    public function prepare( &$column, &$item ){
        $theme_path = config('app.theme_name', 'default') . '/dist/assets/fonts/svg';
        $fonts_folder = public_path( $theme_path );

        $fonts = glob( $fonts_folder . '/*.svg' );

        $icons = array();
        foreach( $fonts as $font ){
            $font_name = str_replace( array( $fonts_folder . '/', '.svg' ), '', $font );
            if( is_array( $column['options'] ) && isset( $column['options']['allowed_icons'] ) && !empty( $column['options']['allowed_icons'] ) ){
                if( in_array( $font_name, $column['options']['allowed_icons'] ) ){
                    $icons[] = $font_name;
                }
            } else {
                $icons[] = $font_name;
            }
        }

        $column['icons'] = $icons;
    }

}