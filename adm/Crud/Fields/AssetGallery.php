<?php
namespace Adm\Crud\Fields;

/**
 * Class AssetGallery
 * CMS Field Type class for Asset Gallery Fields
 */
class AssetGallery extends BaseField {

    /**
     * Prepare
     * Function used before an asset gallery field is displayed on a form.
     * Takes a reference to the column and the current item.
     *
     * @param array $column
     * @param \Adm\Crud\CrudModel $item
     */
    public function prepare( &$column, &$item ){
        $this->_getAssetGalleryFieldConfig( $column );

        if( class_exists( '\Adm\Crud\Fields\Asset' ) ) {
            if( is_object( $item ) && $item ){
                if (data_get( $item, transform_key( $column['field'] ) )) {
                    $AssetField = new \Adm\Crud\Fields\Asset( $this->LaraCrud );

                    $gallery_assets = json_decode( data_get( $item, transform_key( $column['field'] ) ), true );
                    foreach( $gallery_assets as $index => $asset ){
                        $temp_col = $column;
                        $temp_col['field'] .= '[' . $index . ']';
                        $temp_col['label'] = '';

                        $AssetField->prepare( $temp_col, $item );
                    }
                }
            }
        }
    }

    /**
     * Save
     * Function called before saving a resource to set the correct value
     * for the database.
     * Uses the Asset Field save function to iterate over the gallery
     *
     * @param mixed $value
     * @param mixed $old_value
     * @param string $field
     * @return string
     */
    public function save( $value, $old_value, $field ){
        $images = [];
        if( class_exists( '\Adm\Crud\Fields\Asset' ) ) {
            $AssetField = new \Adm\Crud\Fields\Asset( $this->LaraCrud );

            foreach( $value as $index => $gallery_image ){
                $images[] = $AssetField->save( $gallery_image, $old_value, $field . '[' . $index . ']' );
            }
        }

        return json_encode( $images );
    }

    /**
     * After Saved
     * Function called after an item is saved.
     * Saves the POSTed asset sizing and crop information into the asset group
     * table.
     *
     * @param mixed $item_primary
     * @param array $inputs
     * @param \Adm\Crud\CrudModel $item
     * @param string $field
     */
    public function afterSaved( $item_primary, $inputs, $item, $field ){
        $images = \Illuminate\Support\Arr::get( $inputs, transform_key( $field ) );
        if( class_exists( '\Adm\Crud\Fields\Asset' ) ) {
            $AssetField = new \Adm\Crud\Fields\Asset( $this->LaraCrud );

            foreach( $images as $index => $gallery_image ){
                $images[] = $AssetField->afterSaved( $item_primary, $inputs, $item, $field . '[' . $index . ']' );
            }
        }
    }

    /**
     * AJAX function called when adding a new 'row' to an asset gallery.
     * Based on the field's size config generates a new set of asset preview fields.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Throwable
     */
    public function ajax_add_gallery_asset(){
        $temp_col = [
            'label' => '',
            'help' => '',
            'required' => false,
            'field' => $this->LaraCrud->request->input('field'),
            'multi_size' => $this->LaraCrud->request->input('multi_size'),
            'sizes' => $this->LaraCrud->request->input('sizes')
        ];

        $item = null;

        if( class_exists( '\Adm\Crud\Fields\Asset' ) ) {
            $AssetField = new \Adm\Crud\Fields\Asset( $this->LaraCrud );
            $AssetField->prepare( $temp_col, $item );

            $json = [
                'success' => true,
                'new_asset' => view( 'fields.form.partials.asset-gallery.row', ['column' => $temp_col, 'index' => $this->LaraCrud->request->input('index')] )->render()
            ];
        } else {
            $json = ['success' => false];
        }

        return response()->json( $json );
    }

    /**
     * Private function called to generate the asset sizes for previews based on the field name
     * and the config provided either through the page builder, or through the module
     *
     * @param array $column
     */
    private function _getAssetGalleryFieldConfig( &$column ){
        if( isset($column['sizes']) && !empty($column['sizes']) ){ return; }

        $field_config = [];
        if( stristr( $column['field'], 'page_builder' ) !== false && class_exists( 'Adm\Crud\Fields\PageBuilder' ) ){
            $PageBuilder = new \Adm\Crud\Fields\PageBuilder( $this->LaraCrud );
            $field_config = $PageBuilder->getPageBuilderAssetConfig( $column['field'] );
        }
        else {
            $table_specific_function = \Illuminate\Support\Str::camel( 'get_' . $this->LaraCrud->slug . '_asset_config' );
            if( method_exists( $this->LaraCrud->caller, $table_specific_function ) ){
                $field_config = $this->LaraCrud->caller->{$table_specific_function}( $column['field'] );
            }
            elseif( method_exists( $this->LaraCrud->caller, 'getAssetConfig') ){
                $field_config = $this->LaraCrud->caller->getAssetConfig( $column['field'] );
            }
        }

        $column['multi_size'] = false;
        $column['sizes'] = [];

        if( !empty( $field_config ) ){
            $column['multi_size'] = true;
            $column['sizes'] = $field_config;
        }
    }

}