<?php
namespace Adm\Crud\Fields;

use Adm\Models\Url;

class Relation extends BaseField {

    public function show( $column, &$item ){
        $thisRelation = $this->LaraCrud->relations[$column['field']];
        $table = $thisRelation['table'];
        $rel_column = $thisRelation['column'];
        $reference_col = $thisRelation['reference_col'];
        $translatable = $thisRelation['translatable'];

        $class_name = $this->LaraCrud->_getCrudModel();
        $class_name::init( $table );

        $obj = $class_name::where($reference_col, $item->{$column['field']});
        if( $translatable ){
            $obj = $obj->where( "language_id", $this->LaraCrud->language_id );
        }
        $obj = $obj->first();

        if( is_array( $rel_column ) ){
            $display_parts = [];
            foreach( $rel_column as $rel_col ){
                $display_parts[] = $obj ? $obj->{$rel_col} : '';
            }

            $item->{$column['field']} = implode( " ", $display_parts );
        } else {
            $item->{$column['field']} = $obj ? $obj->$rel_column : '';
        }
    }

    public function prepare( &$column, &$item ){
        $thisRelation = $this->LaraCrud->relations[$column['field']];
        $table = $thisRelation['table'];
        $rel_column = $thisRelation['column'];
        $reference_col = $thisRelation['reference_col'];
        $translatable = $thisRelation['translatable'];
        $where_conditions = $thisRelation['where_conditions'];

        $class_name = $this->LaraCrud->_getCrudModel();
        $class_name::init( $table );

        if( $translatable ){
            $opts_qry = $class_name::where("language_id", $this->LaraCrud->language_id);
        } else {
            $opts_qry = $class_name::distinct();
        }

        if( $where_conditions ){
            foreach( $where_conditions as $property => $value ){
                $opts_qry->where( $property, $value );
            }
        }

        if( $table == 'pages' && !\Auth::user()->userGroup->super_admin ){
            $system_pages = Url::where('table_name', 'pages')->where('system_page', 1)->pluck('table_key');

            $opts_qry->whereNotIn($reference_col, $system_pages);
        }

        if( is_object( $item ) && $table == $this->LaraCrud->getTable() ){
            $opts_qry->where($reference_col, '<>', $item->{$reference_col});
        }

        if( isset( $this->LaraCrud->relationExcludes ) && isset( $this->LaraCrud->relationExcludes[ $column['field'] ] ) ){
            foreach( $this->LaraCrud->relationExcludes[ $column['field'] ] as $excluded_value ){
                $opts_qry->where($reference_col, '<>', $excluded_value);
            }
        }

        if( is_array( $rel_column ) ){
            foreach( $rel_column as $rel_col ){
                $opts_qry->orderBy( $rel_col );
            }
        }
        $results = $opts_qry->get();

        $opts = [];
        foreach( $results as $result ){
            if( is_array( $rel_column ) ){
                $display_parts = [];
                foreach( $rel_column as $rel_col ){
                    $display_parts[] = $result->{$rel_col};
                }

                $opts[ $result->{$reference_col} ] = implode( " ", $display_parts );
            } else {
                $opts[ $result->{$reference_col} ] = $result->$rel_column;
            }
        }

        $column['values'][$thisRelation['empty_value']] = ( isset( $column['extra_data']['empty_label'] ) ) ? $column['extra_data']['empty_label'] : "Choose ".$column['label'];
        $column['values'] = $column['values'] + $opts;

        if( isset( $thisRelation['adm_link'] ) ){
            $column['admin_relation_link'] = $thisRelation['adm_link'];
        }

        $this->LaraCrud->initModel();
    }

}