<?php
namespace Adm\Crud\Fields;

use Adm\Models\FileUsage;
use App\Models\AssetGroup;
use App\Models\File;

/**
 * Class Asset
 * CMS Field Type class for Asset Fields
 */
class Asset extends BaseField {

    /**
     * Show
     * Function called before displaying an asset field in the list view.
     *
     * @param array $column
     * @param \Adm\Crud\CrudModel $item
     */
    public function show( $column, &$item ){
        $this->_getAssetFieldConfig( $column );

        $file = File::find( $item->{$column['field']} );
        if( $file ){
            $item->{$column['field']} = $file;
        } else {
            $item->{$column['field']} = '';
        }
    }

    /**
     * Prepare
     * Function used before an asset field is displayed on a form.
     * Takes a reference to the column and the current item.
     *
     * @param array $column
     * @param \Adm\Crud\CrudModel $item
     */
    public function prepare( &$column, &$item ){
        $this->LaraCrud->assetManagerEnabled = true;

        $this->_getAssetFieldConfig( $column );

        if( $column['multi_size'] ) {
            $sizes_array = [];
            foreach ($column['sizes'] as $reference => $details) {
                $sizes_array[$reference] = false;
            }
        } else {
            $sizes_array['single'] = false;
        }

        $current_field_name = "current_" . $column['field'];

        if( is_object( $item ) && $item ){
            foreach( $sizes_array as $reference => $value ){

                // If we've just POSTed the asset, but there's a validation error
                if( $this->LaraCrud->request->old( transform_key( $column['field'] ) . '.' . $reference ) ){
                    // If we're looking for a multi-size image, but we don't have the size, skip over it
                    if( $reference != 'single' && !isset( $column['sizes'][ $reference ] ) ){ break; }

                    $file_id = $this->LaraCrud->request->old( transform_key( $column['field'] ) . '.' . $reference );
                    $file = File::find( $file_id );

                    // If the file doesn't exist, skip it
                    if( !$file ){ break; }

                    // Else, recreate the file
                    $asset_group = new AssetGroup();
                    $asset_group->width = isset( $column['sizes'][ $reference ]['width'] ) ? $column['sizes'][ $reference ]['width'] : 0;
                    $asset_group->height = isset( $column['sizes'][ $reference ]['height'] ) ? $column['sizes'][ $reference ]['height'] : 0;

                    if( $this->LaraCrud->request->old( 'asset_group.' . transform_key( $column['field'] ) . '.' . $reference ) ){
                        $request_asset_group = $this->LaraCrud->request->old( 'asset_group.' . transform_key( $column['field'] ) . '.' . $reference );
                        if( !empty( $request_asset_group ) ){
                            $asset_group->width = $request_asset_group['crop_width'];
                            $asset_group->height = $request_asset_group['crop_height'];
                            $asset_group->x_coordinate = $request_asset_group['x_coordinate'];
                            $asset_group->y_coordinate = $request_asset_group['y_coordinate'];
                            $asset_group->cropbox_left = $request_asset_group['cropbox_left'];
                            $asset_group->cropbox_top = $request_asset_group['cropbox_top'];
                        }
                    }

                    $file->asset_group = $asset_group;
                    $file->crop = AssetGroup::generateAssetCrop( $asset_group );

                    $sizes_array[ $reference ] = $file;

                    // And move on to the next $reference.
                    continue;
                }

                $whereConditions = [
                    'table_name' => $this->LaraCrud->getTable(),
                    'table_key' => $item->{ $this->LaraCrud->getPrimaryColumn() },
                    'field' => transform_key( $column['field'] ),
                    'image_size' => $reference
                ];

                if( !is_null( $item->language_id ) ){
                    $whereConditions['language_id'] = $item->language_id;
                }

                if( !is_null( $item->version ) ){
                    $whereConditions['version'] = $item->version;
                }

                $asset_group = AssetGroup::where( $whereConditions )->first();

                if( $asset_group ){
                    $file = File::find($asset_group->file_id);

                    if( !$file ){ continue; }

                    $file->asset_group = $asset_group;
                    $file->crop = AssetGroup::generateAssetCrop( $asset_group );

                    $sizes_array[ $reference ] = $file;
                }
            }

            $item->$current_field_name = $sizes_array;
        }
    }

    /**
     * Save
     * Function called before saving a resource to set the correct value
     * for the database.
     * Sets whether assets are in use or not and returns the 'primary' asset
     * size to save against the current item.
     *
     * @param mixed $value
     * @param mixed $old_value
     * @param string $field
     * @param bool|array $column
     * @return int
     */
    public function save( $value, $old_value, $field, $column = false ){
        if( $column === false ){
            // We'll need to check if the saved asset field is a set of assets, or a singular
            $column = array('field' => $field);
        }
        $this->_getAssetFieldConfig( $column );

        // We need to get the existing assets that were set to the various image sizes available.
        if( is_object( $this->LaraCrud->currentItem ) ){
            $current_assets = AssetGroup::where([
                'table_name' => $this->LaraCrud->getTable(),
                'table_key' => $this->LaraCrud->currentItem->{ $this->LaraCrud->getPrimaryColumn() },
                'language_id' => $this->LaraCrud->language_id,
                'version' => ($this->LaraCrud->currentItem->version ? $this->LaraCrud->currentItem->version : 0),
                'field' => transform_key( $field )
            ])->get();

            // And then we need to disassociate those files from the record
            foreach( $current_assets as $asset ){
                $currently_used = FileUsage::where([
                    'file_id' => $asset['file_id'],
                    'table_name' => $this->LaraCrud->getTable(),
                    'table_key' => $this->LaraCrud->currentItem->{ $this->LaraCrud->getPrimaryColumn() }
                ])->first();

                if( !empty($currently_used) ){
                    $currently_used->delete();
                }
            }
        }

        if( $column['multi_size'] ) {
            foreach( $column['sizes'] as $reference => $details ){
                // If it's a set, and we have an array of images set, grab the 'primary' image to save to the
                // basic record (e.g. to the page or news article)
                if( $details['primary'] == true ){
                    $value = is_array( $value ) ? $value[ $reference ] : $value;
                }
            }
        } else {
            $value = is_array( $value ) ? $value['single'] : $value;
        }

        return (int)$value;
    }

    /**
     * After Saved
     * Function called after an item is saved.
     * Saves the POSTed asset sizing and crop information into the asset group
     * table.
     *
     * @param mixed $item_primary
     * @param array $inputs
     * @param \Adm\Crud\CrudModel $item
     * @param string $field
     * @param bool|FALSE $column
     */
    public function afterSaved( $item_primary, $inputs, $item, $field, $column = false ){
        if( $column === false ){
            // We'll need to check if the saved asset field is a set of assets, or a singular
            $column = array('field' => $field);
        }
        $this->_getAssetFieldConfig( $column );

        if( $column['multi_size'] ){
            foreach( $column['sizes'] as $reference => $details ){
                $this->_saveAssetGroup( $field, $reference, $inputs, $item );
            }
        } elseif( !empty( \Illuminate\Support\Arr::get( $inputs, transform_key( $field ) ) ) ) {
            $this->_saveAssetGroup( $field, 'single', $inputs, $item );
        }
    }

    /**
     * Get Validation Rules.
     * Populates the validation array with custom rules based on the current
     * asset field.
     *
     * @param array $validation
     * @param array $inputs
     * @param array $column
     */
    public function getValidationRules( &$validation, $inputs, $column ){
        $this->_getAssetFieldConfig( $column );

        if( $column['multi_size'] ){

            // Set a flag to denote whether any images have been set on the form
            $any_images_set = false;

            foreach( $column['sizes'] as $reference => $details ){
                // And if any images HAVE been set, toggle our flag.
                if( isset( $inputs[ $column['field'] ][ $reference ] ) && !empty( $inputs[ $column['field'] ][ $reference ] ) ){
                    $any_images_set = true;
                }

                if( isset( $details['primary'] ) && $details['primary'] ){
                    $primary_size = $reference;
                }

                // If the particular size is required, ensure we validate it
                if( isset( $details['required'] ) && $details['required'] ){
                    $validation[ $column['field'] . '.' . $reference ] = 'required';
                }
            }

            // If any images have been set, or the field is required, then we need to make the 'primary' image size required.
            if( isset( $primary_size ) && ($any_images_set || $column['required']) ) {
                $validation[$column['field'] . '.' . $primary_size] = 'required';
            }

        }
        elseif( $column['required'] ){
            $validation[ $column['field'] . '.single'] = 'required';
        }
    }

    /**
     * Get Validation Messages.
     * Populates the validation message array with custom messages based on
     * the current asset field.
     *
     * @param array $messages
     * @param array $inputs
     * @param array $column
     */
    public function getValidationMessages( &$messages, $inputs, $column ){
        $this->_getAssetFieldConfig( $column );

        $size_label = $column['field'];
        if( $column['multi_size'] ) {

            // Set a flag to denote whether any images have been set on the form
            $any_images_set = false;

            foreach ($column['sizes'] as $reference => $details) {
                // And if any images HAVE been set, toggle our flag.
                if( isset( $inputs[ $column['field'] ][ $reference ] ) && !empty( $inputs[ $column['field'] ][ $reference ] ) ){
                    $any_images_set = true;
                }

                if (isset($details['primary']) && $details['primary']) {
                    $primary_size = $reference;
                    $size_label = $details['label'];
                }

                // If the particular size is required, ensure we validate it
                if( isset( $details['required'] ) && $details['required'] ){
                    $messages[ $column['field'] . '.' . $reference . '.required' ] = 'You need to select a ' . $details['label'] . ' image.';
                }
            }

            // If any images have been set, or the field is required, then we need to make the 'primary' image size required.
            if( isset( $primary_size ) && ($any_images_set || $column['required']) ) {
                $messages[$column['field'] . '.' . $primary_size . '.required'] = 'You need to select a ' . $size_label . ' image.';
            }
        }
        elseif( $column['required'] ) {
            $messages[$column['field'] . '.single.required'] = 'You need to select an image.';
        }
    }

    /**
     * Function used to get the size config for use with asset previews
     *
     * @param array $column
     */
    private function _getAssetFieldConfig( &$column ){
        if( isset($column['sizes']) && !empty($column['sizes']) ){ return; }

        $field_config = [];
        if( stristr( $column['field'], 'page_builder' ) !== false && class_exists( 'Adm\Crud\Fields\PageBuilder' ) ){
            $PageBuilder = new \Adm\Crud\Fields\PageBuilder( $this->LaraCrud );
            $field_config = $PageBuilder->getPageBuilderAssetConfig( $column['field'] );
        }
        else {
            $table_specific_function = \Illuminate\Support\Str::camel( 'get_' . $this->LaraCrud->slug . '_asset_config' );
            if( method_exists( $this->LaraCrud->caller, $table_specific_function ) ){
                $field_config = $this->LaraCrud->caller->{$table_specific_function}( $column['field'] );
            }
            elseif( method_exists( $this->LaraCrud->caller, 'getAssetConfig') ){
                $field_config = $this->LaraCrud->caller->getAssetConfig( $column['field'] );
            }
        }

        $column['multi_size'] = false;
        $column['sizes'] = [];

        if( !empty( $field_config ) ){
            $column['multi_size'] = true;
            $column['sizes'] = $field_config;
        }
    }

    /**
     * Private function called to save an asset's sizing and crop data
     *
     * @param string $field
     * @param string $reference
     * @param array $inputs
     * @param \Adm\Crud\CrudModel $item
     */
    private function _saveAssetGroup( $field, $reference, $inputs, $item ){
        $key_to_search_for = transform_key( $field . '[' . $reference . ']' );
        $size_asset = data_get( $inputs, $key_to_search_for ) != null ? data_get( $inputs, $key_to_search_for ) : false;

        if( $size_asset !== false ){

            $asset_group_exists = AssetGroup::where([
                'table_name' => $this->LaraCrud->getTable(),
                'table_key' => $item->{ $this->LaraCrud->getPrimaryColumn() },
                'language_id' => $this->LaraCrud->language_id,
                'version' => ($item->version ? $item->version : 0),
                'field' => transform_key( $field ),
                'image_size' => $reference
            ])->first();

            if( empty( $asset_group_exists ) ) {
                // First, we need to associate the selected image with this record
                $asset_group = new AssetGroup();

                $asset_group->table_name = $this->LaraCrud->getTable();
                $asset_group->table_key = $item->{$this->LaraCrud->getPrimaryColumn()};
                $asset_group->language_id = $this->LaraCrud->language_id;
                $asset_group->version = ($item->version ? $item->version : 0);
                $asset_group->field = transform_key($field);
                $asset_group->image_size = $reference;
            } else {
                $asset_group = $asset_group_exists;
            }
            $asset_group->file_id = $size_asset;

            $crop_details = isset($inputs['asset_group'][transform_key($field)][$reference]) ? $inputs['asset_group'][transform_key($field)][$reference] : array();
            if( !empty( $crop_details ) ){
                $asset_group->alt = $crop_details['alt'];
                $asset_group->width = (int)$crop_details['crop_width'];
                $asset_group->height = (int)$crop_details['crop_height'];
                $asset_group->x_coordinate = (int)$crop_details['x_coordinate'];
                $asset_group->y_coordinate = (int)$crop_details['y_coordinate'];
                $asset_group->cropbox_left = (int)$crop_details['cropbox_left'];
                $asset_group->cropbox_top = (int)$crop_details['cropbox_top'];
            } else {
                $file = File::find($size_asset);

                $asset_group->alt = $file->name;
            }

            $asset_group->save();

            // And then, we need to register that the asset has been used.
            $new_usage = new FileUsage();

            $new_usage->file_id = $size_asset;
            $new_usage->table_name = $this->LaraCrud->getTable();
            $new_usage->table_key = $item->{ $this->LaraCrud->getPrimaryColumn() };

            $new_usage->save();
        }
    }

}