<?php
namespace Adm\Crud\Fields;

use Carbon\Carbon;

class Time extends BaseField {

    public function show( $column, &$item ){
        $item->{$column['field']} = Carbon::parse( $item->{$column['field']} )->format( "H:i" );
    }

    public function prepare( &$column, &$item ){
        if ($item){
            $item_value = data_get( $item, transform_key( $column['field'] ) );
            $item_value = Carbon::parse( $item_value )->format("H:i");
            if( stristr( $column['field'], 'page_builder' ) !== false ){
                $page_builder = $item->page_builder;
                $pb_key = str_replace( 'page_builder.', '', transform_key( $column['field'] ) );
                data_set( $page_builder, $pb_key, $item_value );
                $item->page_builder = $page_builder;
            } else {
                $item->{$column['field']} = $item_value;
            }
        }
    }

    public function save( $value, $old_value ){
        if( $value ){
            return Carbon::createFromFormat("H:i", $value);
        }
        elseif( $old_value ){
            return Carbon::createFromFormat("H:i", $old_value);
        }

        return Carbon::now();
    }

}