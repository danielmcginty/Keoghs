<?php
namespace Adm\Crud\Fields;

use Adm\Models\Url;
use App\Helpers\LinkHelper;

/**
 * Class Link
 * CMS Field Type class for Link Fields
 */
class Link extends BaseField {

    /**
     * Internal link array
     * Used as a load time cache to store URL IDs and their titles
     *
     * @var array
     */
    protected static $_LINK_ARR = [];

    /**
     * Show
     * Function called before a field is displayed in the list view
     *
     * @param array $column
     * @param \Adm\Crud\CrudModel $item
     */
    public function show( $column, &$item ){
        if( $item ){
            $link_value = data_get( $item, transform_key( $column['field'] ) );
            if( $link_value ){
                $link_arr = json_decode( $link_value, true );
                $link = LinkHelper::parseLink( $link_arr );
                if( isset( $link['url'] ) && $link['url'] ){
                    $item->{$column['field']} = $link['url'];
                    return;
                }
            }
        } else {
            $item->{$column['field']} = '';
        }
    }

    /**
     * Prepare
     * Function called before a field is displayed in the form view
     *
     * @param array $column
     * @param \Adm\Crud\CrudModel $item
     */
    public function prepare( &$column, &$item ){
        if( empty( self::$_LINK_ARR ) ){
            $urls = Url::query()
                ->where('language_id', $this->LaraCrud->language_id)
                ->whereNotIn('table_name', ['people', 'insights'])
                ->get();

            $links = [];
            foreach( $urls as $url ){
                if( $url->url_object ) {
                    $title = $this->getUrlObjectTitle($url);

                    $links[$url->id] = '[' . ucwords( str_replace( ['_', '-'], ' ', $url->table_name ) ) . '] ' . $title;
                }
            }
            asort( $links );

            self::$_LINK_ARR = $links;
        } else {
            $links = self::$_LINK_ARR;
        }

        $column['internal_links'] = $links;

        if( !empty( $item ) ){
            // Add the 'true' parameter to ensure that returned objects are casted to arrays for compatibility with the
            // form field
            $raw_value = data_get( $item, transform_key( $column['field'] ) );
            $field_value = is_json( $raw_value ) ? json_decode( $raw_value, true ) : [];
            if (is_array($raw_value) && isset($raw_value['type'])) {
                $field_value = $raw_value;
            }
            if( stristr( $column['field'], 'page_builder' ) !== false ){
                $page_builder = $item->page_builder;
                $pb_key = str_replace( 'page_builder.', '', transform_key( $column['field'] ) );
                data_set( $page_builder, $pb_key, $field_value );
                $item->page_builder = $page_builder;
            } else {
                $item->{$column['field']} = $field_value;
            }
        }
    }

    /**
     * Save
     * Function called before an item is saved into the database
     *
     * @param mixed $value
     * @return string
     */
    public function save( $value ){
        return json_encode( $value );
    }

    private function getUrlObjectTitle($url)
    {
        if (isset($url->url_object->title)) {
            return $url->url_object->title;
        }

        throw new \Exception("Unknown Title attribute for url with ID {$url->id}");
    }

}