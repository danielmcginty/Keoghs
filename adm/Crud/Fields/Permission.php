<?php
namespace Adm\Crud\Fields;

class Permission extends BaseField {

    public function show( $column, &$item ){
        $output_labels = array();
        $raw_data = data_get( $item, transform_key( $column['field'] ) );
        if( is_serialized( $raw_data ) ){ $raw_data = json_encode( unserialize( $raw_data ) ); }
        $values = json_decode( $raw_data, true );

        foreach( (array)$column['options'] as $value => $label ){
            if( in_array( $value, $values ) ){
                $output_labels[] = is_array( $label ) ? $label['title'] : $label;
            }
        }

        $item->{$column['field']} = implode(", ", $output_labels);
    }

    public function prepare( &$column, &$item ){
        $field_name = $column['field'];
        if( stristr($column['field'], '[]') === false ){
            $field_name .= '[]';
        }

        if( !is_object( $item ) ){
            $column['field'] = $field_name;
            $column['selected'] = array();

            return;
        }

        $permissions = data_get( $item, transform_key( $column['field'] ) );
        if( is_serialized( $permissions ) ){ $permissions = json_encode( unserialize( $permissions ) ); }
        $column['selected'] = $permissions ? json_decode( $permissions, true ) : [];
        $column['field'] = $field_name;
    }

    public function save( $value, $old_value ){
        return json_encode( $value );
    }

}