<?php
namespace Adm\Crud\Fields;

use \App\Models\File as FileModel;

/**
 * Class File
 * CMS Field Type class for File Fields
 */
class File extends BaseField {

    /**
     * Prepare
     * Function called before a field is displayed in the form view
     *
     * @param array $column
     * @param \Adm\Crud\CrudModel $item
     */
    public function prepare( &$column, &$item ){
        if( $item ){

            $file_id = \Illuminate\Support\Arr::get( $item, transform_key( $column['field'] ) );

            $file = FileModel::find( $file_id );

            if( $file ){

                $column['file_path'] = $file->path;

            }
        }
    }

    /**
     * Save
     * Function called before an item is saved into the database
     *
     * @param mixed $value
     * @return int
     */
    public function save( $value ){
        if( !$value ){ return 0; }
        return $value;
    }

}