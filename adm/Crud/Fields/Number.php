<?php
namespace Adm\Crud\Fields;

/**
 * Class Number
 * CMS Field Type class for Number Fields
 */
class Number extends BaseField {

    /**
     * Save
     * Function called before saving a resource to set the correct value
     * for the database.
     * Sets whether assets are in use or not and returns the 'primary' asset
     * size to save against the current item.
     *
     * @param mixed $value
     * @return int
     */
    public function save( $value ){
        return (float)$value;
    }

}