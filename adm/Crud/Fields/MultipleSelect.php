<?php
namespace Adm\Crud\Fields;

/**
 * Class MultipleSelect
 * CMS Field Type class for Multiple Select Fields
 */
class MultipleSelect extends BaseField {

    /**
     * Local variable dictating which delimiter to use when saving
     * and loading checkbox values.
     *
     * @var string
     */
    private $_delimiter = '|';

    /**
     * Show
     * Function called before a field is displayed in the list view
     *
     * @param array $column
     * @param \Adm\Crud\CrudModel $item
     */
    public function show( $column, &$item ){
        $output_labels = array();
        $values = explode($this->_delimiter, $item->{ $column['field'] });
        foreach( (array)$column['options'] as $value => $label ){
            if( in_array( $value, $values ) ){
                $output_labels[] = $label;
            }
        }

        $item->{$column['field']} = implode(", ", $output_labels);
    }

    /**
     * Prepare
     * Function called before a field is displayed in the form view
     *
     * @param array $column
     * @param \Adm\Crud\CrudModel $item
     */
    public function prepare( &$column, &$item ){
        $opt = is_object($item) ? explode( $this->_delimiter, data_get( $item, transform_key( $column['field'] ) ) ) : array();

        $opts = array();
        foreach( $opt as $option ){
            if( isset( $column['extra_data']['select_value_type'] ) ){
                switch( $column['extra_data']['select_value_type'] ){
                    case 'int':
                        $option = (int)$option;
                        break;
                    case 'float':
                        $option = (float)$option;
                        break;
                }
            }
            $opts[] = $option;
        }

        $field_name = $column['field'];
        if( stristr($column['field'], '[]') === false ){
            $field_name .= '[]';
        }

        if( is_object( $item ) ) {
            if( stristr( $column['field'], 'page_builder' ) !== false ){
                $page_builder = $item->page_builder;
                $pb_key = str_replace( 'page_builder.', '', transform_key( $column['field'] ) );
                data_set( $page_builder, $pb_key, $opts );
                $item->page_builder = $page_builder;
            } else {
                $item->{$column['field']} = $opts;
            }
        }

        $column['field'] = $field_name;
    }

    /**
     * Save
     * Function called before an item is saved into the database
     *
     * @param mixed $value
     * @return string
     */
    public function save( $value ){
        return is_array( $value ) ? implode( $this->_delimiter, $value) : $value;
    }

}