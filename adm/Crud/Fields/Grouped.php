<?php
namespace Adm\Crud\Fields;

use Adm\Crud\LaraCrudField;

class Grouped extends BaseField {

    public function prepare( &$column, &$item ){
        $grouped_fields = $this->_getGroupedFields( $column );
        $column['grouped_fields'] = $grouped_fields;

        if( $this->LaraCrud->request->old( $column['field'] ) ){
            $item->{$column['field']} = $this->LaraCrud->request->old( $column['field'] );
        }
        elseif( $item ){
            $data = data_get( $item, transform_key( $column['field'] ) );
            $data = is_array( $data ) ? $data : (is_json( $data ) ? json_decode( $data ) : $data);
            if( stristr( $column['field'], 'page_builder' ) !== false ){
                $page_builder = $item->page_builder;
                $pb_key = str_replace( 'page_builder.', '', transform_key( $column['field'] ) );
                data_set( $page_builder, $pb_key, $data );
                $item->page_builder = $page_builder;
            } else {
                $item->{$column['field']} = $data;
            }
        }

        $this->_mapColumnAttributesToFields( $column, $column['grouped_fields'] );

        $CrudField = new LaraCrudField( $this->LaraCrud );
        foreach( $column['grouped_fields'] as $index => $field ){
            switch( $field['type'] ){
                case 'repeatable':
                case 'stacked_repeatable':
                    // Disabled at present
                    unset( $column['grouped_fields'][ $index] );
                    break;

                case 'asset':
                    // We need to pass in each repeatable item (if set) to the prepare to ensure we can pull out the assigned asset
                    if( $item ){
                        $item_fields = data_get( $item, transform_key( $column['field'] ) );
                        foreach( $item_fields as $item_field_index => $item_field_value ){
                            $tmp_column = $column;
                            $tmp_column['grouped_fields'][ $index ]['field'] = $column['field'] . '[' . $item_field_index . '][' . $index . ']';
                            $CrudField->setType( 'asset' )->prepare( $tmp_column['grouped_fields'][ $index ], $item );
                        }
                    }
                    break;

                case 'link':
                    // We first only need to get internal links and not overwrite item values, so pass in a null item
                    $tmp_item = null;
                    $CrudField->setType( $field['type'] )->prepare( $column['grouped_fields'][ $index ], $tmp_item );

                    // But we also need to prep any stored links
                    if( $item ){
                        $item_fields = data_get( $item, transform_key( $column['field'] ) );
                        foreach( $item_fields as $item_field_index => $item_field_value ){
                            $tmp_column = $column;
                            $tmp_column['grouped_fields'][ $index ]['field'] = $column['field'] . '[' . $item_field_index . '][' . $index . ']';
                            $CrudField->setType( $field['type'] )->prepare( $tmp_column['grouped_fields'][ $index ], $item );
                        }
                    }
                    break;

                default:
                    $CrudField->setType( $field['type'] )->prepare( $column['grouped_fields'][ $index ], $item );
                    break;
            }
        }
    }

    public function save( $value, $old_value, $field ){
        if( !empty( $value ) ){
            $column = ['field' => $field];
            $fields = $this->_getGroupedFields( $column );

            if( !empty( $fields ) ){
                $CrudField = new LaraCrudField( $this->LaraCrud );

                foreach( $value as $post_sort_order => $posted_value ){
                    foreach( $fields as $field_name => $details ){
                        if( !isset( $posted_value[ $field_name ] ) ){
                            continue;
                        }

                        $tmp_field = $field;
                        $tmp_field .= '[' . $post_sort_order . '][' . $field_name . ']';

                        $value[ $post_sort_order ][ $field_name ] = $CrudField->setType( $details['type'] )->save( $value[ $post_sort_order ][ $field_name ], null, $tmp_field );
                    }
                }
            }
        }

        return json_encode( $value );
    }

    public function afterSaved( $primary_key_column, $post_data, $item, $field ){
        if( !empty( $post_data ) ){
            $posted_fields = data_get( $post_data, transform_key( $field ) );

            if( !empty( $posted_fields ) ){
                $column = ['field' => $field];
                $fields = $this->_getGroupedFields( $column );

                if( !empty( $fields ) ){
                    $CrudField = new LaraCrudField( $this->LaraCrud );

                    foreach( $posted_fields as $post_sort_order => $posted_value ){
                        foreach( $fields as $field_name => $details ){
                            if( !isset( $posted_value[ $field_name ] ) ){
                                continue;
                            }

                            $tmp_field = $field;
                            $tmp_field .= '[' . $post_sort_order . '][' . $field_name . ']';

                            $CrudField->setType( $details['type'] )->afterSaved( $primary_key_column, $post_data, $item, $tmp_field );
                        }
                    }
                }
            }
        }
    }

    public function getValidationRules( &$validation, $inputs, $column ){
        $repeatable_fields = $this->_getGroupedFields( $column );

        foreach( $repeatable_fields as $field_name => $details ){
            if( isset( $details['validation'] ) && !empty( $details['validation']) ){

                $field_string = transform_key( $column['field'] ) . '.*.' . $field_name;
                $validation[ $field_string ] = implode( '|', array_keys( $details['validation'] ) );

            }
        }
    }

    public function getValidationMessages( &$messages, $inputs, $column ){
        $repeatable_fields = $this->_getGroupedFields( $column );

        foreach( $repeatable_fields as $field_name => $details ){
            if( isset( $details['validation'] ) && !empty( $details['validation'] ) ){
                $field_string = transform_key( $column['field'] ) . '.*.' . $field_name;

                foreach( $details['validation'] as $rule => $message ){
                    $messages[ $field_string . '.' . $rule ] = $message;
                }
            }
        }
    }

    protected function _getGroupedFields( &$column ){
        if( isset( $column['grouped_fields'] ) && !empty( $column['grouped_fields'] ) ){ return $column['grouped_fields']; }

        $fields = [];
        if( stristr( $column['field'], 'page_builder' ) !== false && class_exists( 'Adm\Crud\Fields\PageBuilder' ) ){
            $PageBuilder = new \Adm\Crud\Fields\PageBuilder( $this->LaraCrud );
            $fields = $PageBuilder->getPageBuilderGroupedFields( $column['field'] );
        }
        else {
            $slug_specific_method = \Illuminate\Support\Str::camel( 'get_' . $this->LaraCrud->slug . '_grouped_fields' );
            if( method_exists( $this->LaraCrud->caller, $slug_specific_method ) ){
                $fields = $this->LaraCrud->caller->{$slug_specific_method}( $column['field'] );
            }
            elseif( method_exists( $this->LaraCrud->caller, 'getGroupedFields' ) ){
                $fields = $this->LaraCrud->caller->getGroupedFields( $column['field'] );
            }
        }

        return $fields;
    }

    protected function _mapColumnAttributesToFields( $column, &$grouped_fields ){
        foreach( $grouped_fields as $field_name => $attributes ){
            foreach( $column as $key => $value ){
                if( $key == 'grouped_fields' ){ continue; }

                if( !array_key_exists( $key, $attributes ) ){
                    if( $key == 'help' ){ $value = ''; }

                    $grouped_fields[ $field_name ][ $key ] = $value;
                }
            }
        }
    }
}