<?php
namespace Adm\Crud\Fields;

/**
 * Class OrderableMultipleSelect
 * CMS Field Type class for Orderable Multiple Select Fields
 */
class OrderableMultipleSelect extends BaseField {

    /**
     * Prepare
     * Function called before a field is displayed in the form view
     *
     * @param array $column
     * @param \Adm\Crud\CrudModel $item
     */
    public function prepare( &$column, &$item ){
        $opts = [];
        if( is_object( $item ) ){
            $item_data = data_get( $item, transform_key( $column['field'] ) );
            if( is_json( $item_data ) ){
                $opts = json_decode( $item_data, true );
            }
            elseif( is_array( $item_data ) ){
                foreach( $item_data as $ind => $value ){
                    $opts[] = [
                        'order' => $ind,
                        'value' => $value['value']
                    ];
                }
            }
        }

        $selected = [];
        if( !empty( $opts ) ) {
            foreach( $opts as $opt ){
                if (isset($column['extra_data']['select_value_type'])) {
                    switch ($column['extra_data']['select_value_type']) {
                        case 'int':
                            $opt['value'] = (int)$opt['value'];
                            break;
                        case 'float':
                            $opt['value'] = (float)$opt['value'];
                            break;
                    }
                }

                $selected[$opt['order']] = $opt;
            }

            ksort( $selected );
        }

        $field_name = $column['field'];
        if( stristr($column['field'], '[]') === false ){
            $field_name .= '[]';
        }

        if( is_object( $item ) ) {
            if( stristr( $column['field'], 'page_builder' ) !== false ){
                $page_builder = $item->page_builder;
                $pb_key = str_replace( 'page_builder.', '', transform_key( $column['field'] ) );
                data_set( $page_builder, $pb_key, $opts );
                $item->page_builder = $page_builder;
            } else {
                $item->{$column['field']} = $opts;
            }
        }

        if( !isset( $column['options'] ) ){
            $column['options'] = [];
        }

        if( !is_array( $column['options'] ) ){
            $column['options'] = [ $column['options'] ];
        }

        $column['selected'] = $selected;
        $column['field'] = $field_name;
    }

    /**
     * Save
     * Function called before an item is saved into the database
     *
     * @param mixed $value
     * @return string
     */
    public function save( $value ){
        $opts = [];
        foreach( (array)$value as $index => $value ){
            if( !empty( trim( $value ) ) ) {
                $opts[] = ['order' => $index, 'value' => $value];
            }
        }

        return !empty( $opts ) ? json_encode( $opts ) : '[]';
    }

}