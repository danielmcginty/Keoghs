<?php
namespace Adm\Crud\Fields;

use Adm\Models\Url;

class SeoOptions extends BaseField {

    public function prepare( &$column, &$item ){
        if( $item ){
            $url = Url::where( [
                ['table_name', $this->LaraCrud->getTable()],
                ['language_id', $this->LaraCrud->language_id],
                ['table_key', $item->{$this->LaraCrud->getPrimaryColumn()}]
            ] )->first();
        }

        if( empty( $url ) ){
            $url = new Url();
        }

        $column['values'] = $url;
    }

    public function getValidationRules( &$validation, $inputs, $column ){
        $validation['seo_options.meta_title'] = 'max:255';
        $validation['seo_options.meta_description'] = 'max:320';
    }

    public function getValidationMessages( &$messages, $inputs, $column ){
        $messages['seo_options.meta_title.max'] = 'The Meta Title must be 255 characters or less.';
        $messages['seo_options.meta_description.max'] = 'The Meta Description should be 320 characters or less.';
    }

}