<?php
namespace Adm\Crud\Fields;

class StandardSelect extends BaseField {

    public function show( $column, &$item ){
        $output_label = '';
        foreach( (array)$column['options'] as $value => $label ){
            if( $value == $item->{ $column['field'] } ){
                $output_label = $label;
            }
        }
        $item->{$column['field']} = $output_label;
    }

    public function prepare( &$column, &$item ){
        // Make the first option as a null value with label: "Select xxx" so the field can be left empty if required
        $column['options'] = ['' => 'Select ' . $column['label']] + $column['options'];
    }

}