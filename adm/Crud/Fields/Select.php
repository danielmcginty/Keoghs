<?php
namespace Adm\Crud\Fields;

class Select extends BaseField {

    public function show( $column, &$item ){
        $output_label = '';
        foreach( (array)$column['options'] as $value => $label ){
            if( $value == $item->{ $column['field'] } ){
                $output_label = $label;
            }
        }
        $item->{$column['field']} = $output_label;
    }

    public function prepare( &$column, &$item ){
        $empty_value = ( isset( $column['extra_data']['empty_value'] ) ) ? $column['extra_data']['empty_value'] : 0;
        $empty_label = ( isset( $column['extra_data']['empty_label'] ) ) ? $column['extra_data']['empty_label'] : 'Select ' . $column['label'];

        $column['options'] = [$empty_value => $empty_label] + $column['options'];
    }

}