<?php
namespace Adm\Crud\Fields;

/**
 * Class Checkbox
 * CMS Field Type class for Checkbox Fields
 */
class Checkbox extends BaseField {

    /**
     * Local variable dictating which delimiter to use when saving
     * and loading checkbox values.
     *
     * @var string
     */
    private $_delimiter = '|';

    /**
     * Show
     * Function called before a field is displayed in the list view
     *
     * @param array $column
     * @param \Adm\Crud\CrudModel $item
     */
    public function show( $column, &$item ){
        $output_labels = array();
        $values = explode($this->_delimiter, $item->{ $column['field'] });
        foreach( (array)$column['options'] as $value => $label ){
            if( in_array( $value, $values ) ){
                $output_labels[] = $label;
            }
        }

        $item->{$column['field']} = implode(", ", $output_labels);
    }

    /**
     * Prepare
     * Function called before a field is displayed in the form view
     *
     * @param array $column
     * @param \Adm\Crud\CrudModel $item
     */
    public function prepare( &$column, &$item ){
        $field_name = $column['field'];
        if( stristr($column['field'], '[]') === false ){
            $field_name .= '[]';
        }

        if( !is_object( $item ) ){
            $column['field'] = $field_name;
            $column['selected'] = array();

            return;
        }

        $column['selected'] = explode( $this->_delimiter, $item->{$column['field']} );
        $column['field'] = $field_name;
    }

    /**
     * Save
     * Function called before an item is saved into the database
     *
     * @param mixed $value
     * @return string
     */
    public function save( $value ){
        return implode( $this->_delimiter, $value );
    }

}