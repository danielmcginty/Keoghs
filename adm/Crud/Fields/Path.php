<?php
namespace Adm\Crud\Fields;

use Adm\Crud\CrudModel;
use Adm\Models\Url;
use App\Models\Redirect;

class Path extends BaseField {

    public function show( $column, &$item ){
        $url = $this->_getUrlForItem( $item );

        if( $url ){
            $item->{$column['field']} = $url->url;
            $item->system_page = $url->system_page;
            $item->full_path = $url->url;

            if( strlen($url->url) > 50 ){
                $item->{$column['field']} = substr( $url->url, 0, 25 ) . '...' . substr( $url->url, -25 );
            }
        } else {
            $item->{$column['field']} = '';
        }
    }

    public function prepare( &$column, &$item ){
        if( $item ){
            $url = $this->_getUrlForItem( $item );

            if( is_object( $url ) ) {
                $item->system_page = $url->system_page;

                // Check for empty / duplicate meta titles & descriptions
                $needs_meta_title_error = $needs_meta_description_error = false;
                if( empty( $url->meta_title ) ){
                    $needs_meta_title_error = true;
                } else {
                    $duplicate_meta_title = Url::where([
                        [ 'meta_title', $url->meta_title ],
                        [ 'language_id', $this->LaraCrud->language_id ],
                        [ 'table_key', '<>', $item->{$this->LaraCrud->getPrimaryColumn()} ]
                    ])->get();
                    if( count( $duplicate_meta_title ) > 0 ){
                        $needs_meta_title_error = true;
                    }
                }

                if( empty( $url->meta_description ) ){
                    $needs_meta_description_error = true;
                } else {
                    $duplicate_meta_description = Url::where([
                        [ 'meta_description', $url->meta_description ],
                        [ 'language_id', $this->LaraCrud->language_id ],
                        [ 'table_key', '<>', $item->{$this->LaraCrud->getPrimaryColumn()} ]
                    ])->get();
                    if( count( $duplicate_meta_description ) > 0 ){
                        $needs_meta_description_error = true;
                    }
                }

                $meta_error = false;
                if( $needs_meta_title_error && $needs_meta_description_error ){
                    $meta_error = 'Ensure a unique meta title and description are assigned before publishing.';
                }
                elseif( $needs_meta_title_error ){
                    $meta_error = 'Ensure a unique meta title is assigned before publishing.';
                }
                elseif( $needs_meta_description_error ){
                    $meta_error = 'Ensure a unique meta description is assigned before publishing.';
                }

                if( !empty( $meta_error ) && !\Session::has('notice') ){
                    \Session::flash('notice', $meta_error );
                }
            }
        }

        if( empty( $url ) ){
            $url = new Url();
        }

        $column['values'] = $url;

        $column['generation_field'] = $this->LaraCrud->urlGenerationField;
    }

    public function afterSaved( $item_primary, $inputs, $item, $field ){
        if( is_array( $item_primary ) ){
            foreach( $item_primary as $field => $value ){
                if( $field != "language_id" && $field != "version" ){
                    $item_primary = $value;
                }
            }
        }

        if( !isset($inputs['path']['system_page']) ){
            $inputs['path']['system_page'] = false;
        }

        if( !isset($inputs['path']['no_index_meta']) ){
            $inputs['path']['no_index_meta'] = 0;
        }

        if( isset($inputs['seo_options']['meta_title']) && empty($inputs['seo_options']['meta_title']) ){
            if( isset($inputs[ $this->LaraCrud->metaTitleGenerationField ]) && !empty($inputs[ $this->LaraCrud->metaTitleGenerationField]) ){
                $generated_meta_title = $inputs[ $this->LaraCrud->metaTitleGenerationField ];

                // Ensure that the meta title, even if auto-generated, is less than 70 characters
                if( strlen( $generated_meta_title ) > 69 ){
                    $generated_meta_title = substr( $generated_meta_title, 0, 69 );
                }

                $inputs['seo_options']['meta_title'] = $generated_meta_title;
            }
        }

        if(
            !empty( $this->LaraCrud->metaDescriptionGenerationField )
            && isset( $inputs['seo_options']['meta_description'] )
            && empty( $inputs['seo_options']['meta_description'] )
        ){
            if( isset( $inputs[ $this->LaraCrud->metaDescriptionGenerationField ] ) && !empty( $inputs[ $this->LaraCrud->metaDescriptionGenerationField ] ) ){
                $generated_meta_description = $inputs[ $this->LaraCrud->metaDescriptionGenerationField ];

                // Ensure that no HTML is included
                $generated_meta_description = strip_tags( $generated_meta_description );

                // And sure that it's less than 320 characters
                if( strlen( $generated_meta_description ) > 320 ){
                    $generated_meta_description = substr( $generated_meta_description, 0, 320 );
                }

                $inputs['seo_options']['meta_description'] = $generated_meta_description;
            }
        }

        $path = $this->generatePath($inputs['path']['url'], $inputs);
        $existing_url = Url::where('table_name',$this->LaraCrud->getTable())
            ->where('table_key', $item_primary)
            ->where('language_id', $this->LaraCrud->language_id)
            ->first();

        if( $existing_url ){

            // If the new path isn't what the item already had, we'll need to add a 301 redirect to the system
            if( $existing_url->url != $path ) {
                $redirect = Redirect::where('old_url', $existing_url->url)->first();

                if ($redirect) {
                    if ($redirect->new_url != $path) {
                        $redirect->new_url = $path;
                    }
                } else {
                    $redirect = new Redirect();

                    $redirect->old_url = $existing_url->url;
                    $redirect->new_url = $path;
                }

                $redirect->save();

                // Check that the URL being redirected to doesn't have a redirect
                $redirect_for_new = Redirect::where('old_url', $path)->first();
                if( $redirect_for_new ){
                    $redirect_for_new->delete();
                }
            }

            $existing_url->url = $path;
            $existing_url->meta_title = isset($inputs['seo_options']['meta_title']) ? $inputs['seo_options']['meta_title'] : '';
            $existing_url->meta_description = isset($inputs['seo_options']['meta_description']) ? $inputs['seo_options']['meta_description'] : '';
            $existing_url->system_page = (int)$inputs['path']['system_page'] ? (int)$inputs['path']['system_page'] : 0;
            $existing_url->no_index_meta = (int)$inputs['path']['no_index_meta'] ? (int)$inputs['path']['no_index_meta'] : 0;
            $existing_url->route = $inputs['path']['route'];

            unset( $existing_url->url_object );

            $existing_url->save();

        } else {

            Url::create([
                'url' => $path,
                'table_name' => $this->LaraCrud->getTable(),
                'table_key' => $item_primary,
                'language_id' => $this->LaraCrud->language_id,
                'meta_title' => $inputs['seo_options']['meta_title'],
                'meta_description' => $inputs['seo_options']['meta_description'],
                'system_page' => (int)$inputs['path']['system_page'] ? (int)$inputs['path']['system_page'] : 0,
                'no_index_meta' => (int)$inputs['path']['no_index_meta'] ? (int)$inputs['path']['no_index_meta'] : 0,
                'route' => $inputs['path']['route']
            ]);

        }
    }

    public function afterDelete( $item_id ){
        Url::where( 'table_name', $this->LaraCrud->getTable() )->where( 'table_key', $item_id )->delete();
    }

    public function getValidationRules( &$validation, $inputs, $column ){
        $value = $inputs[ $column['field'] ];

        $rules = '';
        if( !isset($value['system_page']) || !$value['system_page']) {
            $rules = 'required';
        }

        $validation[$column['field']] = 'path';
        $validation[$column['field'] . '.url'] = $rules;
    }

    public function getValidationMessages( &$messages, $inputs, $column ){
        $messages[ $column['field'] . '.url.required' ] = 'The ' . ucwords($column['label']) . ' is required.';
    }

    public function getPathPrefix( $table, $field_id, $column ){
        $prefix = '';
        CrudModel::init( $table );
        $field = CrudModel::where($column, $field_id)->get()->first();
        if ( is_object( $field ) && isset( $field->{$column} ) ) {
            $url = Url::where('table_name', $table )->where('table_key', $field->{$column})->where('language_id', $this->LaraCrud->language_id)->get()->first();
        }
        if (isset($url) && !empty($url)){
            $prefix = $url->url . '/';
        }

        return $prefix;
    }

    public function getPathPrefixForItem( $table, $field, $column, $item_table, $item_id ){
        // Set a default value
        $prefix = '';

        $primaryCol = $this->LaraCrud->getPrimaryColumn();
        if( empty( $primaryCol ) ){ return $prefix; }

        // $item_id is the ID of the item being edited, so we need to get the particular field we're after
        CrudModel::init( $item_table );
        $original_item = CrudModel::where($primaryCol, $item_id)->get()->first();
        if( !is_object( $original_item ) ){ return $prefix; }

        // Now we've got the original item, we can query the pathPrefix field to get the correct ID to search the URL table by
        CrudModel::init( $table );
        $prefix_item = CrudModel::where($column, $original_item->{$field})->get()->first();
        if( !is_object( $prefix_item ) ){ return $prefix; }

        // Now we can get the URL for the prefix item
        $url = is_object( $prefix_item ) ? Url::where('table_name', $table)->where('table_key', $prefix_item->{$column})->where('language_id', $this->LaraCrud->language_id)->get()->first() : false;

        // If it's a URL that exists
        if( !empty( $url ) ){
            $prefix = $url->url . '/';
        }

        return $prefix;
    }

    public function ajax_get_path_prefix(){
        $table = $this->LaraCrud->request->input('table', false);
        $table_key = $this->LaraCrud->request->input('table_key', -1);
        $column = $this->LaraCrud->request->input('column', -1);

        if( $table === false || $table_key === -1 || $column === -1 ) {
            return json_encode( array( "success" => false, "message" => "Missing Inputs" ) );
        }

        if( empty( $table_key ) ) {
            return json_encode( array( "success" => true, "prefix" => "" ) );
        }

        $path_prefix = $this->getPathPrefix( $table, $table_key, $column );
        return json_encode( array( "success" => true, "prefix" => $path_prefix ) );
    }

    private function _getUrlForItem( $item ){
        return Url::where( [
            ['table_name', $this->LaraCrud->getTable()],
            ['language_id', $this->LaraCrud->language_id],
            ['table_key', $item->{$this->LaraCrud->getPrimaryColumn()}]
        ] )->first();
    }

    /**
     * Private function called to generate the full URL path
     * Takes a path prefix and sanitizes the POSTed url to ensure that all URLs are SEO friendly
     *
     * @param string $string
     * @param array $fields
     * @return string
     */
    public function generatePath( $string, $fields = [] ){
        $base_prefix = '';
        if( !empty( $this->LaraCrud->basePathPrefix ) ){
            $base_prefix = $this->getPathPrefix( $this->LaraCrud->basePathPrefix['table'], $this->LaraCrud->basePathPrefix['field'], $this->LaraCrud->basePathPrefix['column'] );
            $this->LaraCrud->initModel();
        }

        $prefix = '';
        if( !empty( $this->LaraCrud->pathPrefix ) && !empty( $fields[ $this->LaraCrud->pathPrefix['field'] ] ) ){
            $value = is_numeric( $this->LaraCrud->pathPrefix['field'] ) ? $this->LaraCrud->pathPrefix['field'] : $fields[ $this->LaraCrud->pathPrefix['field'] ];
            $prefix = $this->getPathPrefix( $this->LaraCrud->pathPrefix['table'], $value, $this->LaraCrud->pathPrefix['column'] );
            $this->LaraCrud->initModel();
        }

        // In case we have a 'base' prefix that is contained within the prefix, remove it
        $prefix = str_replace( $base_prefix, '', $prefix );

        return $base_prefix . $prefix . str_replace( $prefix, "", \Illuminate\Support\Str::slug($string ? $string : $fields['title'] , "-") );
    }

}