<?php
namespace Adm\Crud\Fields;

class Password extends BaseField {

    public function show( $column, &$item ){
        $item->{$column['field']} = '&bullet;&bullet;&bullet;&bullet;';
    }

    public function prepare( &$column, &$item ){
        if( $item ){
            $item->{$column['field']} = '';
        }
    }

    public function save( $value, $old_value ){
        if( $value ){
            return bcrypt( $value );
        }
        elseif( $old_value ){
            return $old_value;
        }

        return $value;
    }

    public function getValidationRules( &$validations, $inputs, $column ){
        if( $this->LaraCrud->getAction() == 'store' ){
            $validations[ $column['field'] ] = 'required';
        }
    }

}