<?php
namespace Adm\Crud\Fields;

/**
 * Class FormBuilder
 * CMS Field Type class for Form Builder Fields
 */
class FormBuilder extends BaseField {

    /**
     * Prepare
     * Function called before a field is displayed in the form view
     *
     * @param array $column
     * @param \Adm\Crud\CrudModel $item
     */
    public function prepare( &$column, &$item ){
        $column['field_types'] = [
            'text' => 'Text',
            'email' => 'Email Address',
            'number' => 'Number',
            'tel' => 'Phone Number',
            'textarea' => 'Textarea (Multiple Lines)',
            'radio' => 'Radio Button',
            'checkbox' => 'Checkbox',
            'select' => 'Select Dropdown',
            'file' => 'File Upload',
        ];

        $column['show_options_type'] = [
            'select',
            'radio',
            'checkbox',
        ];

        if( $item && is_object( $item ) ) {
            $raw_data = data_get( $item, transform_key( $column['field'] ) );
            if( is_serialized( $raw_data ) ){ $raw_data = json_encode( unserialize( $raw_data ) ); }
            $item_value = json_decode( $raw_data, true );

            if( stristr( $column['field'], 'page_builder' ) !== false ){
                $page_builder = $item->page_builder;
                $pb_key = str_replace( 'page_builder.', '', transform_key( $column['field'] ) );
                data_set( $page_builder, $pb_key, $item_value );
                $item->page_builder = $page_builder;
            } else {
                $item->{$column['field']} = $item_value;
            }
        }
    }

    /**
     * Save
     * Function called before an item is saved into the database
     *
     * @param mixed $value
     * @return string
     */
    public function save( $value ){
        return json_encode( $value );
    }

    /**
     * AJAX function called when adding a new form field to a form builder instance
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Throwable
     */
    public function ajax_add_form_row(){
        $json = array();

        $column = $this->LaraCrud->request->input('column');
        $row_index = $this->LaraCrud->request->input('row_index');

        $view = view('fields.form.partials.form-builder.row', array('column' => $column, 'table_row' => array(), 'row_index' => $row_index));
        $rendered = $view->render();
        $json['rows'] = $rendered;

        return response()->json($json);
    }

}