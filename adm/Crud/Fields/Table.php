<?php
namespace Adm\Crud\Fields;

class Table extends BaseField {

    public function prepare( &$column, &$item ){
        if( $item ) {
            if( $this->LaraCrud->request->old( transform_key( $column['field'] ) ) ){
                $data = $this->LaraCrud->request->old( transform_key( $column['field'] ) );
            } else {
                $data = data_get( $item, transform_key( $column['field'] ) );
            }

            if( is_serialized( $data ) ){ $data = json_encode( unserialize( $data ) ); }
            $data = is_json( $data ) ? json_decode( $data, true ) : $data;
            if( stristr( $column['field'], 'page_builder' ) !== false ){
                $page_builder = $item->page_builder;
                $pb_key = str_replace( 'page_builder.', '', transform_key( $column['field'] ) );
                data_set( $page_builder, $pb_key, $data );
                $item->page_builder = $page_builder;
            } else {
                $item->{$column['field']} = $data;
            }
        }
    }

    public function save( $value ){
        return json_encode( $value );
    }

    /**
     * AJAX function called when adding a new row to the table builder field
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Throwable
     */
    public function ajax_add_table_row(){
        $json = array();

        $column = $this->LaraCrud->request->input('column');
        $row_index = $this->LaraCrud->request->input('row_index');

        $table_row = array();

        $view = view('fields.form.partials.table-builder.row', array('column' => $column, 'table_row' => $table_row, 'row_index' => $row_index));
        $rendered = $view->render();
        $json['rows'] = $rendered;

        return response()->json($json);
    }

}