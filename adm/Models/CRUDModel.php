<?php
namespace Adm\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CrudModel
 * @package Adm\Models
 * @property string $table
 * @property array $custom_field_types
 * @property array $fields
 */
class CrudModel extends Model {
    /**
     * Save instances to CRUD Models for DB purposes
     *
     * @var CrudInstance
     */
    protected $instance;

    /**
     * The table name for the model
     * @var string
     */
    protected $table;

    /**
     * An array to store the 'key' fields for the model,
     * used to retrieve the current object from the database
     * @var string
     */
    protected $primaryKey;

    /**
     * The ID for the model, used to retrieve the current object from the database
     * @var int
     */
    protected $id;

    /**
     * The URL slug for the model, used to generate navigation items
     * @var string
     */
    protected $admSlug;

    /**
     * The language ID currently being used
     * @var int
     */
    protected $language_id;

    /**
     * Whether the current model is translatable,
     * i.e. has a language_id column in the DB table
     * @var bool
     */
    protected $has_language = true;

    /**
     * Whether the current model can be edited
     * @var bool
     */
    protected $can_edit = true;

    /**
     * Whether the current model can be copied
     * @var bool
     */
    protected $can_copy = true;

    /**
     * Whether the current model can be deleted
     * @var bool
     */
    protected $can_delete = true;

    /**
     * Does this model have any fields that shouldn't
     * be displayed in the form?
     * @var array
     */
    protected $hidden_fields = array();

    /**
     * Do we need to show a specific field set on add?
     *
     * @var array
     */
    protected $add_fields = array();

    /**
     * Do we need to show a specific field set on edit?
     *
     * @var array
     */
    protected $edit_fields = array();

    /**
     * Array to store customisable field types for the model
     * @var array
     */
    protected $field_types = array();

    /**
     * Array to store customisable labels for fields
     * @var array
     */
    protected $labels = array();

    /**
     * Array to store customisable help text for fields
     * @var array
     */
    protected $help = array();

    /**
     * Array containing custom columns to display on the list view
     * @var array
     */
    protected $columns = array();

    /**
     * Array containing fields that shouldn't have a wysiwyg text area
     * @var array
     */
    protected $no_wysiwyg = array();

    /**
     * Array containing the column -> Field mappings
     * @var array
     */
    protected $fields = array();

    /**
     * Local variable used to store the current Request
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Constructor
     * Based on the set $table and $custom_field_types attributes,
     * generate the field set that this model will use
     *
     * @param int|bool $id
     * @param array $attributes
     */
    public function __construct( $id = false, array $attributes = []){
        $this->instance = CrudInstance::init( $this->table, $this->primaryKey );

        parent::__construct($attributes);
    }

    /**
     * Initialize
     * Based on the assigned table name, set the field types, and if we've got an ID, load the resource.
     */
    public function initialize(){
        $this->setFieldTypes();

        if ($this->primaryKey && $this->id) {
            $this->loadResource();
        }
    }

    /**
     * Set the language of the object we're trying to retrieve
     *
     * @param $language_id
     */
    public function setLanguage( $language_id ){
        $this->instance->language_id = $language_id;
        $this->language_id = $language_id;
    }

    /**
     * Allow a HTTP Request object to be set to the model
     *
     * @param $request
     */
    public function setRequest( $request ){
        $this->request = $request;
    }

    /**
     * Set ID.
     * Dedicated function to set a model's ID field outside of __construct
     *
     * @param $id
     */
    public function setId( $id ){
        $this->id = $id;
        $this->instance->id = $id;
        $this->loadResource();
    }

    /**
     * Function to remove the language constraint
     */
    public function unsetLanguage(){
        $this->has_language = false;
        $this->instance->has_language = false;
    }

    /**
     * Unset Edit
     * Function to disable item edits if not specified in a model
     */
    public function unsetEdit(){
        $this->can_edit = false;
    }

    /**
     * Unset Copy
     * Function to disable item copies if not specified in a model
     */
    public function unsetCopy(){
        $this->can_copy = false;
    }

    /**
     * Unset Delete
     * Function to disable item deletes if not specified in a model
     */
    public function unsetDelete(){
        $this->can_delete = false;
    }

    /**
     * Unset Text Editor
     * Allows a specific field to have it's WYSIWYG removed
     *
     * @param $field
     */
    public function unsetTextEditor( $field ){
        if( !in_array( $field, $this->no_wysiwyg ) ) {
            $this->no_wysiwyg[] = $field;
        }
    }

    /**
     * Hidden Field.
     * Allows a specific field to be set as hidden if not specified in a model's $hidden_fields array
     *
     * @param $field
     */
    public function hiddenField( $field ){
        if( !in_array( $field, $this->hidden_fields ) ) {
            $this->hidden_fields[] = $field;
        }
    }

    /**
     * Set Field Types.
     * Based on the table schema and any custom fields set
     * generate the $fields array, matching column -> field type
     */
    protected function setFieldTypes(){
        $schema = \DB::getDoctrineSchemaManager();
        foreach( $schema->listTableColumns( $this->table ) as $column ){
            if( $column == 'language_id' ){
                $this->has_language = true;
            }

            if( in_array( $column, array( "created_at", "updated_at" ) ) ){
                $this->timestamps = true;
            }

            if( isset($this->field_types[ $column->getName() ]) && !empty($this->field_types[ $column->getName() ]) ){
                $this->fields[ $column->getName() ] = $this->field_types[ $column->getName() ];
            } else {
                $this->fields[ $column->getName() ] = $column->getType()->getName();
            }
        }

        $this->mapFields();
    }

    /**
     * Map Fields
     * Based on the $fields array, match columns up to field models
     */
    private function mapFields(){
        foreach( $this->fields as $column => $field_type ){
            if( in_array( $column, $this->hidden_fields ) ){
                $field_type = 'hidden';
            }

            // If the particular field type exists, and is a 'fillable' element, and isn't hidden
            if( class_exists( '\\Adm\\Models\\Fields\\' . $field_type ) && !in_array( $column, $this->hidden ) ){
                // Create a namespace reference to the class
                $reference = 'Adm\Models\Fields\\' . $field_type;
                $this_field = new $reference();

                // Set field options
                $this_field->name = $column;
                $this_field->label = isset($this->labels[$column]) ? $this->labels[$column] : ucwords(str_replace("_",
                    " ", $column));
                if (isset($this->help[$column]) && !empty($this->help[$column])) {
                    $this_field->help = "<span class=\"block help-text\">" . $this->help[$column] . "</span>";
                }

                if ($field_type == 'text' && in_array($column, $this->no_wysiwyg)) {
                    $this_field->no_wysiwyg();
                }

                $this->fields[$column] = $this_field;

            } else {
                unset( $this->fields[ $column ] );
            }

            if( $column == "language_id" ){
                $this->fields[ $column ]->value = $this->language_id;
            }
        }
    }

    /**
     * Load Resource
     * Based on the model's $id field, load in the record from the database and
     * set field values
     */
    protected function loadResource(){
        $resource = $this->getResource();

        if( !empty($resource->attributes) ){
            $this->exists = true;
            $this->instance->exists = true;

            foreach( $resource->attributes as $column => $attribute ){
                if( isset($this->fields[ $column ] ) ) {
                    $this->fields[ $column ]->value = $attribute;
                }
            }
        }
    }

    /**
     * Get Resource
     * Returns the model from the database based on the ID field
     *
     * @return mixed
     */
    protected function getResource(){
        if( $this->has_language ) {
            $resource = $this->instance->where($this->primaryKey, '=', $this->id)->where('language_id', $this->language_id)->limit(1)->first();
        } else {
            $resource = $this->instance->where($this->primaryKey, '=', $this->id)->limit(1)->first();
        }

        return $resource;
    }

    /**
     * Get Form Fields.
     * Process the $fields array into an array of $column => field template
     *
     * @return array
     */
    public function getFormFields(){
        $form_fields = array();

        foreach( $this->fields as $column => $field ){
            $form_fields[ $column ] = $field->render();
        }

        return $form_fields;
    }

    /**
     * Get Column Headings
     * Retrieves the column headings for the list view table
     *
     * @return array
     */
    public function getColumnHeadings(){
        $columns = array();
        if( !empty($this->columns) ){
            foreach( $this->columns as $column ){
                $columns[] = $this->fields[ $column ]->label;
            }
        } else {
            foreach( $this->fields as $column => $field ){
                $columns[] = $field->label;
            }
        }

        $columns[] = "Actions";

        return $columns;
    }

    /**
     * Get Items
     * Retrieves the items for the list view table
     *
     * @return array
     */
    public function getItems(){
        $items = array();

        if( $this->has_language ) {
            $resources = $this->instance->where('language_id', config('app.default_language'))->get();
        } else {
            $resources = $this->instance->all();
        }

        foreach( $resources as $resource ){
            $items[] = $this->getColumns( $resource );
        }

        return $items;
    }

    /**
     * Get Columns
     * Retrieves the values for a resource to display in the list view table
     *
     * @param $resource
     * @return array
     */
    public function getColumns( $resource ){
        $columns = array();
        if( !empty($this->columns) ){
            foreach( $this->columns as $column ){
                $columns[] = $resource->{$column};
            }
        } else {
            foreach( $this->fields as $column => $field ){
                $columns[] = $resource->{$column};
            }
        }

        $this->generateActions( $resource, $columns );

        return $columns;
    }

    /**
     * Generate Actions
     * For a given resource, generate a set of actions to perform
     * e.g. Edit, Copy, Delete
     *
     * @param $resource
     * @param $columns
     */
    protected function generateActions( $resource, &$columns ){
        $actions = array();

        if( $this->can_edit ) {
            $actions[] = "<a href=\"" . url('adm/' . $this->admSlug . '/edit/' . $resource->{$this->primaryKey}) . "\" title=\"Edit\">Edit</a>";
        }

        if( $this->can_copy ){
            $actions[] = "<a href=\"copy/" . $resource->{$this->primaryKey} . "\" title=\"Copy\">Copy</a>";
        }

        if( $this->can_delete ){
            $actions[] = "<a href=\"delete/" . $resource->{$this->primaryKey} . "\" title=\"Delete\">Delete</a>";
        }

        $columns[] = implode( "", $actions );
    }

    /**
     * Update the current item with the POST data provided.
     *
     * @param $post_data
     */
    public function updateItem( $post_data ){
        foreach( $post_data as $key => $value ){
            if( array_key_exists( $key, $this->fields ) && !empty($value) ) {
                $processed_value = $this->fields[ $key ]->processValue( $value );
                $this->instance->$key = $processed_value;
            }
        }
        if( $this->instance->save() ){
            \Session::flash('success', 'Record Saved!');
        } else {
            \Session::flash('error', 'Record not Saved!');
        }
    }
}