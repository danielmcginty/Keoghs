<?php
namespace Adm\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PageBuilderBlock
 * @package Adm\Models
 *
 * The PageBuilderBlock model interacts with the page_builder_blocks table, and adds functionality on top of Eloquent
 */
class PageBuilderBlock extends Model {
    // We've got a deleted_at flag, so include the relevant trait
    use SoftDeletes;

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'page_builder_blocks';

    /**
     * The fields that are 'bulk assignable' for this model
     * @var array
     */
    protected $fillable = ['content', 'sort_order'];

    /**
     * This table has timestamps, so state that
     * @var bool
     */
    public $timestamps = true;

    /**
     * Custom Accessor Function.
     * Allows us to pull out the individual fields set against a block, ready for use.
     *
     * @return array
     */
    public function getContentAttribute(){
        $raw_fields = PageBuilderBlockContent::where([
            'page_builder_block_id' => $this->id
        ])->get();

        $field_arr = [];
        foreach( $raw_fields as $field ){
            $field_arr[ $field->field ] = $field->value;
        }

        return $field_arr;
    }
}