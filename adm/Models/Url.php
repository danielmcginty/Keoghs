<?php

namespace Adm\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Url
 * @package Adm\Models
 *
 * Adm Model for URLs.
 * Has slightly different functionality to the Front-End Model.
 */
class Url extends Model {
    // As we've got a deleted_at flag  include the relevant trait
    use SoftDeletes;

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'urls';

    /**
     * The fields that are 'bulk assignable' for this model
     * @var array
     */
    protected $fillable = ['url', 'table_name', 'table_key', 'language_id', 'meta_title', 'meta_description', 'no_index_meta', 'system_page', 'route'];

    /**
     * The urls table doesn't have timestamp fields, so dictate that
     * @var bool
     */
    public $timestamps = false;

    /**
     * Custom Accessor Function.
     * Returns the resource (page, news article, etc) that the URL relates to.
     *
     * @return bool|void
     */
    public function getUrlObjectAttribute(){
        if( !isset($this->table_name) ){ return; }

        $primary_keys = \DB::connection()->getDoctrineSchemaManager()->listTableDetails( $this->table_name )->getPrimaryKeyColumns();

        $primary_key = false;
        $has_version = false;
        foreach( $primary_keys as $pk ){
            if( $pk != 'language_id' && $pk != 'version' ){
                $primary_key = $pk;
            }
            elseif( $pk == 'version' ){
                $has_version = true;
            }
        }

        if( $primary_key == false ){ return false; }

        $query = \DB::table( $this->table_name )->where([
            $primary_key => $this->table_key,
            'language_id' => $this->language_id
        ]);

        if( $has_version ){
            $query->whereNull( 'deleted_at' );
        }

        return $query->first();
    }
}
