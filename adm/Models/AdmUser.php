<?php

namespace Adm\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Adm\Notifications\AdmResetPassword as ResetPasswordNotification;

/**
 * Class AdmUser
 * @package Adm\Models
 *
 * The AdmUser Model is used to log CMS users in, as well as associate with various elements of the CMS, such as change
 * logging, and location logging
 */
class AdmUser extends Model implements AuthenticatableContract, CanResetPasswordContract {
    // Adm Users can be notified, can log in, can reset their passwords, and have a deleted_at flag, so include the relevant traits
    use Notifiable, Authenticatable, CanResetPassword, SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'adm_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'username', 'password', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Any fields to be appended to the object should it be output as an array or JSON object.
     * @var array
     */
    protected $appends = [
        'name'
    ];

    /**
     * Eloquent Relationship.
     * Returns the user group that this user belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userGroup(){
        return $this->belongsTo('\\Adm\\Models\\AdmUserGroup');
    }

    /**
     * Custom Accessor Function used to return a user's full name, rather than split into first & second name.
     *
     * @return string
     */
    public function getNameAttribute(){
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Function used to send the user a reset password email.
     *
     * @param string $token
     */
    public function sendPasswordResetNotification( $token ){
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * Custom Function allowing us to quickly check if the current user has access to a certain area of the CMS,
     * based on the permissions assigned to the user group.
     *
     * @param $area
     * @return bool
     */
    public function canAccess( $area ){
        $adm_path = parse_url( route('adm.path'), PHP_URL_PATH );
        $area = str_replace( $adm_path, '', parse_url( $area, PHP_URL_PATH ) );

        // Ensure that the CMS dashboard is accessible
        if( $area == '' ){
            return true;
        }

        $current_path = str_replace( env('APP_URL'), '', $area );
        $valid_url = false;

        foreach( $this->userGroup->permissions as $available_section ){
            if( $valid_url ){ continue; }

            $available_section = str_replace( env('APP_URL'), '', $available_section );

            if( $available_section == '/general-settings/edit/1' && $current_path == '/general-settings/update/1' ){
                return true;
            }

            if( $available_section == 'adm/' && $current_path == '' ){
                $valid_url = true;
            }
            elseif( stripos( $available_section, $current_path ) === 0 ){ // Almost perfect
                $valid_url = true;
            }
            elseif( $available_section != 'adm/' && stripos( $current_path, $available_section ) === 0 ){
                $valid_url = true;
            }
        }

        if( $this->userGroup->super_admin ){
            $valid_url = true;
        }

        return $valid_url;
    }
}
