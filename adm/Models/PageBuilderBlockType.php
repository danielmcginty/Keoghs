<?php
namespace Adm\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PageBuilderBlockType
 * @package Adm\Models
 *
 * The PageBuilderBlockType Model interacts with the page_builder_block_types table, determining which blocks are
 * available.
 */
class PageBuilderBlockType extends Model {

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'page_builder_block_types';

    /**
     * The fields that are 'bulk assignable' for this model
     * @var array
     */
    protected $fillable = ['reference', 'editable'];

    /**
     * This table doesn't have timestamps, so state that
     * @var bool
     */
    public $timestamps = false;
}