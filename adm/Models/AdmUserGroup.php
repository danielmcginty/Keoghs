<?php

namespace Adm\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AdmUserGroup
 * @package Adm\Models
 *
 * The AdmUserGroup is used to set CMS permissions for different user types
 */
class AdmUserGroup extends Model {
    // We've got a deleted_at flag, so include the relevant trait
    use SoftDeletes;

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'adm_user_groups';

    /**
     * The fields that are 'bulk assignable' for this model
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Custom Accessor Function used to get the stored permissions for this group in a readable array.
     *
     * @param $value
     * @return array
     */
    public function getPermissionsAttribute( $value ){
        if( empty($value) ){ $value = json_encode( array() ); }
        elseif( is_serialized( $value ) ){ $value = json_encode( unserialize( $value ) ); }
        $permissions = array_unique( json_decode( $value, true ) );

        foreach( $permissions as $ind => $permission ){
            $adm_path = parse_url( route('adm.path'), PHP_URL_PATH );
            $permissions[ $ind ] = str_replace( $adm_path, '', parse_url( $permission, PHP_URL_PATH ) );
        }

        // Ensure that the CMS dashboard is accessible
        if( empty( $permissions ) || !in_array( 'adm/', $permissions ) ){
            $permissions[] = 'adm/';
        }

        return $permissions;
    }

    /**
     * Custom Accessor Function used to get the CMS Dashboard permissions for this group in a readable array.
     *
     * @param $value
     * @return array
     */
    public function getDashboardPermissionsAttribute($value){
        if( empty($value) ){ $value = json_encode( array() ); }
        elseif( is_serialized( $value ) ){ $value = json_encode( unserialize( $value ) ); }
        return array_unique( json_decode( $value, true ) );
    }
}
