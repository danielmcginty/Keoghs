<?php

namespace Adm\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * Class UserLocationLog
 * @package Adm\Models
 *
 * The UserLocationLog class is used to interact with the database, recording which users are on the same page in
 * the CMS at any given time.
 */
class UserLocationLog extends Model {

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'user_location_log';

    /**
     * The fields that are 'bulk assignable' for this model
     * @var array
     */
    protected $fillable = ['adm_user_id', 'location'];

    /**
     * Custom Query Scope.
     * Simply filters the current query to only return records for users on the same URL.
     *
     * @param $query
     * @param $location
     * @return mixed
     */
    public function scopeUsersOnSamePage($query, $location) {
        $query
            ->where('location', 'like', $location . '%')
            ->where('adm_user_id', '!=', \Auth::user()->id)
            ->where('created_at', '>=', Carbon::now()->subSeconds(5)->toDateTimeString())
            ->orderBy('adm_user_id', 'ASC');

        return $query;
    }

    /**
     * Eloquent Relationship.
     * Returns the AdmUser that this log associates with.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function adm_user(){
        return $this->belongsTo( 'Adm\\Models\\AdmUser', 'adm_user_id' );
    }
}
