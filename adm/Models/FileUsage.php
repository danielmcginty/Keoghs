<?php
namespace Adm\Models;

use Illuminate\Database\Eloquent\Model;

use Adm\Crud\CrudModel;

/**
 * Class FileUsage
 * @package Adm\Models
 *
 * The FileUsage Model allows us to track which assets are in use where
 */
class FileUsage extends Model {

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'file_usage';

    /**
     * The fields that are 'bulk assignable' for this model
     * @var array
     */
    protected $fillable = ['file_id', 'table_name', 'table_key'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Custom Accessor Function.
     * Allows us to pull out the resource (page, news article, etc) that this record relates to.
     *
     * @param $usage_record
     * @return array
     */
    public static function getUsageItem( $usage_record ){
        CrudModel::init( $usage_record->table_name );

        $primaryKey = \DB::connection()->getDoctrineSchemaManager()->listTableDetails( $usage_record->table_name )->getPrimaryKeyColumns();
        $key_to_use = $primaryKey;
        if( is_array( $primaryKey ) ){
            foreach( $primaryKey as $key_field ){
                if( $key_field != 'language_id' && $key_field != 'version' ){
                    $key_to_use = $key_field;
                }
            }
        }

        $item = CrudModel::where( $key_to_use, '=', $usage_record->table_key )->first();

        if( $item ) {
            $name = $item->$key_to_use;
            if (isset($item->title)) {
                $name = $item->title;
            } elseif (isset($item->name)) {
                $name = $item->name;
            }

            return array(
                'table' => $usage_record->table_name,
                'key_field' => $key_to_use,
                'key' => $usage_record->table_key,
                'name' => $name
            );
        }

        return [];
    }
}