<?php
namespace Adm\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PageBuilderBlockContent
 * @package Adm\Models
 *
 * The PageBuilderBlockContent Model interacts with the page_builder_block_content table, and provides functionality
 * on top of Eloquent.
 */
class PageBuilderBlockContent extends Model {

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'page_builder_block_content';

    /**
     * The fields that are 'bulk assignable' for this model
     * @var array
     */
    protected $fillable = ['page_builder_block_id', 'field', 'value'];

    /**
     * This table doesn't have timestamps, so state that
     * @var bool
     */
    public $timestamps = false;

    /**
     * Custom Accessor Function.
     * Used to pull data out of the database in a readable format, rather than a serialized string
     *
     * @param $value
     * @return mixed
     */
    public function getValueAttribute( $value ){
        if( is_serialized( $value ) ){ $value = json_encode( unserialize( $value ) ); }
        return is_json( $value ) ? json_decode( $value, true ) : $value;
    }

    /**
     * Custom Mutator Function.
     * Ensures that we're not trying to save arrays into the database as arrays.
     *
     * @param $value
     */
    public function setValueAttribute( $value ){
        $this->attributes['value'] = is_array( $value ) ? json_encode( $value ) : $value;
    }
}