<?php
namespace Adm\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ChangeLog
 * @package Adm\Models
 *
 * The ChangeLog Model is used to monitor changes to pages/etc in the CMS
 */
class ChangeLog extends Model {

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'change_log';

    /**
     * The fields that are 'bulk assignable' for this model
     * @var array
     */
    protected $fillable = ['table_name', 'table_key', 'language_id', 'action', 'adm_user_id'];

    /**
     * Any fields to be appended to the object should it be output as an array or JSON object.
     * @var array
     */
    protected $appends = ['edited_item'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Local array containing a 'map' of table names to CMS URLs, to correct links on the Recent Changes on the
     * CMS Dashboard.
     *
     * @var array
     */
    private $table_aliases = [
        'news_articles'     => 'news',
        'adm_users'         => 'user-accounts',
        'adm_user_groups'   => 'user-groups',
        '301_redirects'     => 'redirect-management',
        'forms'             => 'form-builder'
    ];

    /**
     * Relationship to magically get the admin user who is associated with this change
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function adm_user(){
        return $this->belongsTo( 'Adm\\Models\\AdmUser', 'adm_user_id' )->withTrashed();
    }

    /**
     * Custom Function allowing us to get the CMS Slug for the current item type.
     *
     * @return mixed
     */
    public function getTableNameLink(){
        if( isset( $this->table_aliases[ $this->table_name ] ) ){ return $this->table_aliases[ $this->table_name ]; }
        return str_replace( '_', '-', $this->table_name );
    }

    /**
     * Custom Accessor for the Change Log's edited item, rather than using a set_edited_item function
     *
     * @return bool|\Illuminate\Database\Eloquent\Model
     */
    public function getEditedItemAttribute(){
        $primary_keys = \DB::connection()->getDoctrineSchemaManager()->listTableDetails( $this->table_name )->getPrimaryKeyColumns();

        $primary_key = false;
        foreach( $primary_keys as $pk ){
            if( $pk != 'language_id' && $pk != 'version' ){
                $primary_key = $pk;
            }
        }

        if( $primary_key == false ){ return false; }

        $whereConditions = array(
            $primary_key => $this->table_key
        );
        if( in_array( 'language_id', $primary_keys ) ){
            $whereConditions[ 'language_id' ] = $this->language_id;
        }

        $edited_item = \DB::table( $this->table_name )->where( $whereConditions )->first();

        if( !property_exists( $edited_item, 'title' ) ){
            if( property_exists( $edited_item, 'name' ) ){
                $edited_item->title = $edited_item->name;
            } else {
                $edited_item->title = 'No Title could be found.';
            }
        }

        return $edited_item;
    }
}
