<?php

namespace Adm\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CrudInstance
 * @package Adm\Models
 *
 * The CrudInstance model is used to keep a consistent record of what's happening in the CMS through multiple object
 * loads - through the use of static variables.
 */
class CrudInstance extends Model {

    /**
     * Internal static variable, keeping references to CRUD functionality consistent.
     *
     * @var CrudInstance
     */
    private static $instance;

    /**
     * The language ID we're currently accessing in the CMS.
     *
     * @var
     */
    public $language_id;

    /**
     * Init Function.
     * Initialized the CrudInstance model, setting the static instance, as well as settings within it, and optionally
     * loads in the fields we need to 'watch' (save/show/etc)
     *
     * @param $table
     * @param $primaryKey
     * @param null $columns
     * @return CrudInstance
     */
    public static function init( $table, $primaryKey, $columns = null ){
        self::$instance = new CrudInstance();
        self::$instance->table = $table;
        self::$instance->primaryKey = $primaryKey;
        self::$instance->timestamps = true;
        if( $columns ){
            self::$instance->columns = array_keys( $columns );
            if( !in_array( 'created_at', array_keys( $columns ) ) || !in_array( 'updated_at', array_keys( $columns ) ) ){
                self::$instance->timestamps = false;
            }
        }

        return self::$instance;
    }

    /**
     * Constructor.
     * When this model is built, pull in settings from the static variable if they're not set against the model.
     */
    public function __construct(){
        parent::__construct();
        if( self::$instance ){
            $this->table = self::$instance->table;
            $this->primaryKey = self::$instance->primaryKey;
            $this->fillable = self::$instance->columns;
            $this->timestamps = self::$instance->timestamps;
        }
    }

}
