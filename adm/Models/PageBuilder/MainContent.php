<?php
namespace Adm\Models\PageBuilder;
use Adm\Crud\LaraCrud;
use App\Helpers\BlockHelper;
use App\Helpers\ColourHelper;

/**
 * Class MainContent
 * @package Adm\Models\PageBuilder
 *
 * The Adm Model defining how the MainContent Page Builder Block functions
 */
class MainContent extends PageBuilderBase {

    /**
     * The name of the block, to be displayed in the block selection modal, and in the collapsible element on forms.
     *
     * @var string
     */
    public $display_name = 'WYSIWYG Content';

    /**
     * A description of the block, displayed in the block selection modal.
     *
     * @var string
     */
    public $description = 'Add a block, allowing WYSIWYG (What You See Is What You Get) text to be added to the page';

    /**
     * The fields to be set by the user for this block.
     *
     * @var array
     */
    public $builder_fields = [
        'heading'   => [
            'display_name'  => 'Heading',
            'help'          => 'Add a heading to the block',
            'type'          => 'string',
            'required'      => false,
            'validation'    => [],
            'group'         => 'Main'
        ],
        'content'   => [
            'display_name'  => 'Content',
            'help'          => 'Set the main content for the block',
            'type'          => 'text',
            'wysiwyg'       => true,
            'options'       => 'text_editor',
            'required'      => true,
            'validation'    => [
                'required' => 'The Content field is required.'
            ],
            'group'         => 'Main'
        ],
        'buttons'   => [
            'display_name'  => 'CTA Buttons',
            'help'          => 'Add buttons to the block',
            'type'          => 'repeatable',
            'required'      => false,
            'validation'    => [],
            'group'         => 'Main'
        ]
    ];

    /**
     * Any additional, extra settings for block fields
     *
     * @var array
     */
    public $column_extras = [
        'buttons'   => [
            'repeatable_fields' => [
                'text'  => [
                    'label' => 'Text',
                    'type'  => 'string'
                ],
                'link'  => [
                    'label' => 'Link',
                    'type'  => 'link'
                ],
                'colour'    => [
                    'label'     => 'Colour',
                    'type'      => 'select',
                    'options'   => []
                ]
            ]
        ]
    ];

    /**
     * Callback actions performed before the block is 'prepared' for output
     * on a form
     *
     * @param \Adm\Crud\LaraCrud $crud
     */
    public function callbackBeforePrepare( LaraCrud $crud ){
        $this->column_extras['buttons']['repeatable_fields']['colour']['options'] = ColourHelper::getButtonColours();
    }
}