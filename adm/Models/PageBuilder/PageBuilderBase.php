<?php
namespace Adm\Models\PageBuilder;
use Adm\Crud\LaraCrud;
use App\Helpers\BlockHelper;

/**
 * Class PageBuilderBase
 * @package Adm\Models\PageBuilder
 *
 * The 'Base' Class for all Page Builder Blocks within the CMS
 */
class PageBuilderBase {

    /**
     * The name of the block, to be displayed in the block selection modal, and in the collapsible element on forms.
     *
     * @var string
     */
    public $display_name;

    /**
     * A description of the block, displayed in the block selection modal.
     *
     * @var string
     */
    public $description;

    /**
     * The fields to be set by the user for this block.
     *
     * @var array
     */
    public $builder_fields = [];

    /**
     * If a custom template is required to render out the fields for multiple buttons, assign it here.
     *
     * @var string
     */
    public $template;

    /**
     * If any extra 'option' data is required against block fields, assign them here.
     *
     * @var array
     */
    public $column_extras = [];

    /**
     * Centralised function used to get the form template for this block.
     *
     * @param $crud_data
     * @param $index
     * @param $columns
     * @param bool|false $item
     * @param bool|true $status
     * @param bool|false $is_ajax
     * @return string
     * @throws \Exception
     * @throws \Throwable
     */
    public function getTemplate( $crud_data, $index, $columns, $item = false, $status = true, $is_ajax = false ){
        $data = [
            'index' => $index,
            'reference' => $this->reference,
            'columns' => $columns,
            'groups' => $this->generateGroups(),
            'item' => $item,
            'block_status' => $status,
            'block_title' => $this->display_name,
            'is_ajax' => $is_ajax,
            'block_class' => $this
        ];

        $data = array_merge( $crud_data, $data );

        if( !empty( $this->template ) ){

            $data['innards'] = view( $this->template, $data )->render();

        }

        return view( 'fields.page-builder.block_wrapper', $data )->render();
    }

    /**
     * Centralised function used to parse the set of $builder_fields into an array that's compatible with the
     * general column / field format used by the CMS, to prevent any issues and inconsistencies between the two.
     *
     * @param int $index
     * @return array
     */
    public function generateColumns( $index = 0 ){
        $columns = [];

        foreach ( $this->builder_fields as $field_name => $options ){
            $columns[$field_name] = [
                'field'             => sprintf( 'page_builder[%d][content][%s]', $index, $field_name ),
                'label'             => isset($options['display_name']) ? $options['display_name'] : ucwords(implode(" ", explode("_", $field_name))),
                'required'          => isset($options['required']) ? $options['required'] : false,
                'type'              => isset($options['type']) ? $options['type'] : '',
                'options'           => isset($options['options']) ? $options['options'] : [],
                'default'           => isset($options['default']) ? $options['default'] : null,
                'help'              => isset($options['help']) ? $options['help'] : '',
                'validation'        => isset($options['validation']) ? $options['validation'] : false,
                'extra_data'        => isset($options['extra_data']) ? $options['extra_data'] : [],
                'attributes'        => isset($options['attributes']) ? $options['attributes'] : [],
                'grouped_fields'    => isset($options['grouped_fields']) ? $options['grouped_fields'] : [],
            ];

            if( isset( $this->column_extras[$field_name] ) && is_array( $this->column_extras[$field_name] ) ){
                $columns[ $field_name ] = array_merge( $columns[ $field_name ], $this->column_extras[ $field_name ] );
            }
        }

        return $columns;
    }

    /**
     * Centralised function used to generate a set of 'Groups' or 'Tabs' within a Page Builder Block.
     * Allows control over which fields are visible by default, and which are hidden behind an 'Advanced' tab for example.
     * 
     * @return array
     */
    public function generateGroups(){
        $groups = [];
        foreach( $this->builder_fields as $field_name => $options ){
            $group = isset( $options['group'] ) ? \Illuminate\Support\Str::slug( $options['group'], '_' ) : 'main';
            if( !isset( $groups[ $group ] ) ){
                $groups[ $group ] = [];
            }

            $groups[ $group ][] = $field_name;
        }

        return $groups;
    }
}