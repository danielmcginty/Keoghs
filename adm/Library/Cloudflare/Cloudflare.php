<?php
namespace Adm\Library\Cloudflare;

/**
 * Class Cloudflare
 * Library class used to abstract functionality from the base Cloudflare API
 * class
 */
class Cloudflare {

    /**
     * Local instance of the Cloudflare API Class
     *
     * @var \Adm\Library\Cloudflare\CloudflareAPI
     */
    protected $_API;

    /**
     * Boolean setting determining if the Cloudflare API is enabled or not
     * Set depending on whether all configuration settings are available
     *
     * @var bool
     */
    private static $_ENABLED        = true;

    /**
     * Authentication Email Address
     *
     * @var string
     */
    private static $_LOGIN_EMAIL    = '';

    /**
     * Authentication API Key for the site
     *
     * @var string
     */
    private static $_API_KEY        = '';

    /**
     * Centralised reference to the site domain for consistency
     *
     * @var string
     */
    protected static $_DOMAIN       = '';

    /**
     * Centralised reference to domain names to prepend to URL slugs in
     * the purge_slugs() function
     *
     * @var array
     */
    protected static $_SLUG_DOMAIN = '';

    /**
     * Constructor
     * Loads the Cloudflare API Class ready for instantiation, and loads in
     * configuration parameters, setting the overall status based on the
     * presence of variables
     */
    public function __construct(){
        self::$_LOGIN_EMAIL = config( 'cache.cloudflare.email' );
        self::$_API_KEY = config( 'cache.cloudflare.api_key' );
        self::$_DOMAIN = config( 'cache.cloudflare.domain' );
        self::$_SLUG_DOMAIN = config( 'cache.cloudflare.slug_domain' );

        if( empty( self::$_LOGIN_EMAIL ) || empty( self::$_API_KEY ) || empty( self::$_DOMAIN ) || empty( self::$_SLUG_DOMAIN ) ){
            self::$_ENABLED = false;
        }

        if( self::$_ENABLED ){
            $this->_API = new CloudflareAPI( self::$_LOGIN_EMAIL, self::$_API_KEY );
        }
    }

    /**
     * Get Status
     * Returns the enabled / disabled status of the Cloudflare integration
     *
     * @return bool
     */
    public function get_status(){
        return self::$_ENABLED;
    }

    /**
     * Purge Site
     * Purges Cloudflare cache for the entirety of MCW & KMS
     *
     * @return bool
     */
    public function purge_site(){
        if( !self::$_ENABLED ){ return false; }

        $Response = $this->_API->purge_site( $this->_get_identifier( self::$_DOMAIN ) );
        if( $Response->success ){
            return true;
        } else {
            $this->_email_error( __FUNCTION__, self::$_DOMAIN, $Response );
        }

        return false;
    }

    /**
     * Purge URLs
     * Purges Cloudflare cache for selected URLs on MCW & KMS
     *
     * @param array $urls
     * @return bool
     */
    public function purge_urls( $urls = array() ){
        if( !self::$_ENABLED ){ return false; }

        $Response = $this->_API->purge_files( $this->_get_identifier( self::$_DOMAIN ), $urls );
        if( $Response->success ){
            return true;
        } else {
            $this->_email_error( __FUNCTION__, self::$_DOMAIN, $Response );
        }

        return false;
    }

    /**
     * Purge Slugs
     * Purges Cloudflare cache for the selected URL slugs on MCW & KMS
     * Alternative to purge_urls as the domain doesn't have to be specified
     *
     * @param array $slugs
     * @return bool
     */
    public function purge_slugs( $slugs = array() ){
        if( !self::$_ENABLED ){ return false; }

        $urls = array();

        foreach( $slugs as $slug ){
            $urls[] = self::$_SLUG_DOMAIN . ltrim( $slug, '/' );
        }

        return $this->purge_urls( $urls );
    }

    /**
     * Switch Development Mode
     * Turns Development mode on or off for MCW & KMS
     *
     * @return bool
     */
    public function switch_development_mode(){
        if( !self::$_ENABLED ){ return false; }

        $current_mode = $this->get_development_mode();

        if( $current_mode == 'on' ){
            return $this->setDevelopmentMode( 'off' );
        } else {
            return $this->setDevelopmentMode( 'on' );
        }
    }

    /**
     * Set Development Mode
     * Centralised function used to set the development mode through the API
     *
     * @param string $status
     * @return bool
     */
    protected function setDevelopmentMode( $status ){
        if( !self::$_ENABLED ){ return false; }

        $Response = $this->_API->set_development_mode( $this->_get_identifier( self::$_DOMAIN ), $status );
        if( $Response->success ){
            return $Response->result->value;
        } else {
            $this->_email_error( __FUNCTION__, self::$_DOMAIN, $Response );
        }

        return false;
    }

    /**
     * Get Development Mode
     * Returns the current development mode status for MCW
     *
     * @return string
     */
    public function get_development_mode(){
        if( !self::$_ENABLED ){ return false; }

        $Response = $this->_API->get_development_mode( $this->_get_identifier( self::$_DOMAIN ) );

        if( $Response->success ){
            return $Response->result->value;
        } else {
            $this->_email_error( __FUNCTION__, self::$_DOMAIN, $Response );
        }

        return 'off';
    }

    /**
     * Get Indentifier
     * Centralised function used to return the Cloudflare identifier for the
     * provided domain
     *
     * @param string $site_url
     * @return bool|string
     */
    protected function _get_identifier( $site_url ){
        if( !self::$_ENABLED ){ return false; }

        return $this->_API->identifier( $site_url );
    }

    /**
     * Centralised frunction used to email support in the event of an API error
     *
     * @param string $function
     * @param string $domain
     * @param null|\StdClass $Response
     */
    protected function _email_error( $function, $domain, $Response = null ){
        $mail_string = 'Cloudflare ' . $function . ' API Call failed for ' . $domain . '. Please log a support ticket';

        if( !is_null( $Response ) ){
            $mail_string .= PHP_EOL . PHP_EOL;
            $mail_string .= 'API Response:' . PHP_EOL;
            $mail_string .= json_encode( $Response, JSON_PRETTY_PRINT );
        }

        mail(
            'support+atgaccess@bespokedigital.agency',
            'ATG Access Cloudflare Error',
            $mail_string
        );
    }

}