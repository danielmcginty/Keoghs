<?php
namespace Adm\Library\Cloudflare;

/**
 * Cloudflare API
 * Basic class used to interact with the Cloudflare server via it's API
 */
class CloudflareAPI {

    /**
     * API Request Timeout in seconds
     */
    const TIMEOUT = 5;

    /**
     * Centralised variable pointing at the API Base URL
     *
     * @var string
     */
    private $URL = 'https://api.cloudflare.com/client/v4/';

    /**
     * Centralised variable storing authentication email address
     *
     * @var string
     */
    private $auth_email;

    /**
     * Centralised variable storing authentication API Key
     *
     * @var string
     */
    private $auth_key;

    /**
     * Array containing valid DNS Entry Types
     *
     * @var array
     */
    private static $VALID_DNS_TYPES = array( 'A', 'AAAA', 'CNAME', 'TXT', 'SRV', 'LOC', 'MX', 'NS', 'SPF' );

    /**
     * Constructor
     * Loads the class and sets authentication data against it
     */
    public function __construct() {

        $num_args = func_num_args();
        if ( $num_args == 2 ) {
            $parameters       = func_get_args();
            $this->auth_email = $parameters[ 0 ];
            $this->auth_key   = $parameters[ 1 ];
        } else {
            //throw error
        }

    }

    /**
     * Identifier
     * Returns the identifier for the specified domain
     *
     * @param string $domain
     * @return string|bool
     */
    public function identifier( $domain ) {
        $result = $this->get_zone( $domain );
        if ( isset( $result->result ) && count( $result->result ) == 1 )
            return $result->result[ 0 ]->id;

        return false;
    }

    /**
     * Analytics
     * Provides total and timeseries data for the provided identifier
     * @see https://api.cloudflare.com/#zone-analytics-dashboard
     *
     * @param string $identifier
     * @param int $since
     * @param int $until
     * @param bool $continuous
     * @return \StdClass
     */
    public function analytics( $identifier, $since = -10080, $until = 0, $continuous = true ) {
        $continuous = ( $continuous ) ? "true" : "false"; //need to pass as string
        $data       = array(
            'since' => $since,
            'until' => $until,
            'continuous' => $continuous
        );
        return $this->get( 'zones/' . $identifier . '/analytics/dashboard', $data );
    }

    /**
     * Purge Files
     * Purges cache for the provided URLs / Files for the given identifier
     *
     * @param string $identifier
     * @param array $files
     * @return \StdClass
     */
    public function purge_files( $identifier, $files = array() ) {
        $data = array(
            'files' => $files
        );
        return $this->delete( 'zones/' . $identifier . '/purge_cache', $data );
    }

    /**
     * Purge Site
     * Purges cache for all URLs / Files for the given identifier
     *
     * @param string $identifier
     * @return \StdClass
     */
    public function purge_site( $identifier ) {
        $data = array(
            'purge_everything' => true
        );
        return $this->delete( 'zones/' . $identifier . '/purge_cache', $data );
    }

    /**
     * Get Development Mode
     * Returns the current development mode status for the provided identifier
     *
     * @param string $identifier
     * @return \StdClass
     */
    public function get_development_mode($identifier) {
        return $this->get('zones/' . $identifier . '/settings/development_mode', array());
    }

    /**
     * Set Development Mode
     * Sets the development mode status for the provided identifier
     *
     * @param string $identifier
     * @param string $value
     * @return \StdClass
     */
    public function set_development_mode($identifier, $value) {
        $data = array(
            'value' => $value
        );
        return $this->patch('zones/' . $identifier . '/settings/development_mode', $data);
    }

    /**
     * DNS Records
     * Returns the current set of DNS Records for the provided identifier
     *
     * @param string $identifier
     * @return \StdClass
     */
    public function dns_records( $identifier ) {
        return $this->get( 'zones/' . $identifier . '/dns_records', array ());
    }

    /**
     * Get DNS Record ID
     * Return the DNS record ID if only 1 record is found
     *
     * @param string $identifier
     * @param string $type
     * @param string $domain
     * @return string
     */
    public function get_dns_record_id( $identifier, $type = '', $domain = '' ) {
        $response = $this->get_dns_record( $identifier, $type, $domain );
        if ( $response && count( $response->result ) == 1 ) {
            return $response->result[ 0 ]->id;
        }
        return "failed to get dns id use get_dns_record";
    }

    /**
     * Get DNS Record
     * Returns a DNS record based on the provided identifier, type & domain
     * @see https://api.cloudflare.com/#dns-records-for-a-zone-list-dns-records
     *
     * @param string $identifier
     * @param string $type
     * @param string $domain
     * @return string|\StdClass
     */
    public function get_dns_record( $identifier, $type = '', $domain = '' ) {

        if ( !in_array( $type, self::$VALID_DNS_TYPES ) )
            return "incorrect dns type";

        $data = array(
            'type' => $type,
            'name' => $domain,
            'per_page' => 20,
            'order' => 'type',
            'match' => 'all'
        );
        return $this->get( 'zones/' . $identifier . '/dns_records', $data );
    }

    /**
     * Delete DNS Record
     * Deletes a DNS Record based on identifier and ID
     * @see https://api.cloudflare.com/#dns-records-for-a-zone-delete-dns-record
     *
     * @param string $identifier
     * @param string $dns_record_id
     * @return \StdClass
     */
    public function delete_dns_record( $identifier, $dns_record_id ) {

        return $this->delete( 'zones/' . $identifier . '/dns_records/' . $dns_record_id, array ());
    }

    /**
     * Update DNS Record
     * Updates a DNS record based on the provided identifier, ID, type, name,
     * value & TTL
     * @see https://api.cloudflare.com/#dns-records-for-a-zone-update-dns-record
     *
     * @param string $identifier
     * @param string $dns_record_id
     * @param string $type
     * @param string $name
     * @param string $content
     * @param int $ttl
     * @return string|\StdClass
     */
    public function update_dns_record( $identifier, $dns_record_id, $type, $name, $content, $ttl = 1 ) {

        if ( !in_array( $type, self::$VALID_DNS_TYPES ) )
            return "incorrect dns type";

        if ( $type === 'SRV' ) {
            //$content = "$weight, $priority, $target, $service, $proto, $port"
            //Example: $content = "3, 5, examplewebpage.com, _web, _tcp, 8765"
            $contentData = explode( ", ", $content );

            $data = array(
                "type" => $type,
                "data" => array(
                    "name" => $name,
                    "weight" => $contentData[ 0 ],
                    "priority" => $contentData[ 1 ],
                    "target" => $contentData[ 2 ],
                    "service" => $contentData[ 3 ],
                    "proto" => $contentData[ 4 ],
                    "port" => $contentData[ 5 ],
                    "ttl" => $ttl
                )
            );
        } else {
            $data = array(
                'type' => $type,
                'name' => $name,
                'content' => $content,
                'ttl' => $ttl
            );
        }
        return $this->put( 'zones/' . $identifier . '/dns_records/' . $dns_record_id, $data );
    }

    /**
     * Create DNS Record
     * Creates a DNS record based on the provided identifier, ID, type, name,
     * value & TTL
     * @see https://api.cloudflare.com/#dns-records-for-a-zone-create-dns-record
     *
     * @param string $identifier
     * @param string $type
     * @param string $name
     * @param string $content
     * @param int $ttl
     * @return string|\StdClass
     */
    public function create_dns_record( $identifier, $type, $name, $content, $ttl = 1 ) {

        if ( !in_array( $type, self::$VALID_DNS_TYPES ) )
            return "incorrect dns type";

        if ( $type === 'SRV' ) {
            //$content = "$weight, $priority, $target, $service, $proto, $port"
            //Example: $content = "3, 5, examplewebpage.com, _web, _tcp, 8765"
            $contentData = explode( ", ", $content );

            $data = array(
                "type" => $type,
                "data" => array(
                    "name" => $name,
                    "weight" => $contentData[ 0 ],
                    "priority" => $contentData[ 1 ],
                    "target" => $contentData[ 2 ],
                    "service" => $contentData[ 3 ],
                    "proto" => $contentData[ 4 ],
                    "port" => $contentData[ 5 ],
                    "ttl" => $ttl
                )
            );
        } else {
            $data = array(
                'type' => $type,
                'name' => $name,
                'content' => $content,
                'ttl' => $ttl
            );
        }
        return $this->post( 'zones/' . $identifier . '/dns_records', $data );
    }

    /**
     * Get Zone
     * Returns the zones matching the provided name
     *
     * @param string $name
     * @return \StdClass
     */
    public function get_zone( $name ) {
        $data = array(
            'name' => $name,
            'status' => 'active',
            'page' => 1,
            'match' => 'all'
        );
        return $this->get( 'zones', $data );
    }

    /**
     * Get Zones
     * Returns all zones
     *
     * @return \StdClass
     */
    public function get_zones() {
        return $this->get( 'zones', array ());
    }

    /**
     * Get User Details
     * Returns the details associated with the authenticated user
     * @see https://api.cloudflare.com/#user-user-details
     *
     * @return \StdClass
     */
    public function get_user_details() {
        return $this->get( 'user', array ());
    }

    /**
     * Update User Details
     * Updates the authenticated user's details
     * @see https://api.cloudflare.com/#user-user-details
     *
     * @param string $first_name
     * @param string $last_name
     * @param string $telephone
     * @param string $country
     * @param string $zipcode
     * @return \StdClass
     */
    public function update_user_details( $first_name, $last_name, $telephone, $country, $zipcode ) {
        $data = array(
            "first_name" => $first_name,
            "last_name" => $last_name,
            "telephone" => $telephone,
            "country" => $country,
            "zipcode" => $zipcode
        );
        return $this->patch( 'user', $data );
    }

    /**
     * Delete
     * Performs a HTTP DELETE request to the provided endpoint with the
     * provided data
     *
     * @param string $endpoint
     * @param mixed $data
     * @return \StdClass
     */
    private function delete( $endpoint, $data ) {
        return $this->http_request( $endpoint, $data, 'delete' );
    }

    /**
     * Post
     * Performs a HTTP POST request to the provided endpoint with the
     * provided data
     *
     * @param string $endpoint
     * @param mixed $data
     * @return \StdClass
     */
    private function post( $endpoint, $data ) {
        return $this->http_request( $endpoint, $data, 'post' );
    }

    /**
     * Get
     * Performs a HTTP GET request to the provided endpoint with the
     * provided data
     *
     * @param string $endpoint
     * @param mixed $data
     * @return \StdClass
     */
    private function get( $endpoint, $data ) {
        return $this->http_request( $endpoint, $data, 'get' );
    }

    /**
     * Put
     * Performs a HTTP PUT request to the provided endpoint with the
     * provided data
     *
     * @param string $endpoint
     * @param mixed $data
     * @return \StdClass
     */
    private function put( $endpoint, $data ) {
        return $this->http_request( $endpoint, $data, 'put' );
    }

    /**
     * Patch
     * Performs a HTTP PATCH request to the provided endpoint with the
     * provided data
     *
     * @param string $endpoint
     * @param mixed $data
     * @return \StdClass
     */
    private function patch( $endpoint, $data ) {
        return $this->http_request( $endpoint, $data, 'patch' );
    }

    /**
     * HTTP Request
     * Performs a HTTP request to Cloudflare server at the provided endpoint,
     * with the provided data, using the provided method
     *
     * @param string $endpoint
     * @param mixed $data
     * @param string $method
     * @return \StdClass
     */
    private function http_request( $endpoint, $data, $method ) {
        //setup url
        $url = $this->URL . $endpoint;

        //echo $url;exit;

        //headers set
        $headers        = array(
            "X-Auth-Email: {$this->auth_email}",
            "X-Auth-Key: {$this->auth_key}",
            "Content-Type: application/json"
        );
        $headersarray[] = 'Content-type: application/json';

        //json encode data
        $json_data = json_encode( $data );

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_VERBOSE, 0 );
        curl_setopt( $ch, CURLOPT_FORBID_REUSE, true );

        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );

        curl_setopt( $ch, CURLOPT_TIMEOUT, self::TIMEOUT );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );

        if ( $method === 'post' )
            curl_setopt( $ch, CURLOPT_POST, true );

        if ( $method === 'put' )
            curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'PUT' );

        if ( $method === 'delete' )
            curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'DELETE' );

        if ( $method === 'patch' )
            curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'PATCH' );

        //get request otherwise pass post data
        if ( !isset( $method ) || $method == 'get' )
            $url .= '?' . http_build_query( $data );
        else
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data );
        //echo $url;

        //add headers
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch, CURLOPT_URL, $url );


        $http_response = curl_exec( $ch );
        $http_code     = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
        curl_close( $ch );

        if ( $http_code != 200 ) {
            //hit error will add in error checking but for now will return back to user to handle
            return json_decode( $http_response );
        } else {
            return json_decode( $http_response );
        }
    }
}