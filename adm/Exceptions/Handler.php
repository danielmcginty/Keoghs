<?php

namespace Adm\Exceptions;

use Adm\Controllers\AdmController;
use Throwable;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Throwable  $exception
     * @return void
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Throwable $exception)
    {
        // If the thrown error is a token mismatch, redirect to the login page
        if( $exception instanceof TokenMismatchException ){
            return redirect()->route('adm.login')->with('token_message', true);
        }

        // If the thrown exception is a HTTP Exception, then use the normal routine
        if( $exception instanceof HttpException ){
            return $this->renderHttpException( $exception );
        }

        // We need to check the type of exception, to ensure that it's not just a 500 error
        $exception_throwable = false;
        if( $exception instanceof \Illuminate\Auth\AuthenticationException ){ $exception_throwable = true; }
        if( $exception instanceof \Illuminate\Auth\Access\AuthorizationException ){ $exception_throwable = true; }
        if( $exception instanceof \Symfony\Component\HttpKernel\Exception\HttpException ){ $exception_throwable = true; }
        if( $exception instanceof \Illuminate\Database\Eloquent\ModelNotFoundException ){ $exception_throwable = true; }
        if( $exception instanceof \Illuminate\Session\TokenMismatchException ){ $exception_throwable = true; }
        if( $exception instanceof \Illuminate\Validation\ValidationException ){ $exception_throwable = true; }

        // Else, if the theme has a generic error template, throw a controlled error
        // but ONLY if the exception isn't a form validation error
        if( !$exception_throwable && view()->exists( "errors.generic" ) ){
            // So, load the AdmController and throw a generic error
            $AdmController = new AdmController();
            return $AdmController->throwGenericError( $exception );
        }

        // Else, let Laravel decide what to do
        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->route('adm.login');
    }

    /**
     * Render the given HttpException.
     *
     * @param  \Symfony\Component\HttpKernel\Exception\HttpExceptionInterface  $e
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function renderHttpException(HttpExceptionInterface $e)
    {
        $status = $e->getStatusCode();

        if (view()->exists("errors.{$status}")) {
            return response()->view("errors.{$status}", ['exception' => $e], $status, $e->getHeaders());
        } else {
            return $this->convertExceptionToResponse($e);
        }
    }
}
