<?php
namespace Adm\Controllers;

use Adm\Config\GeneralSettingsConfig;
use Adm\Config\RedirectManagementConfig;
use Adm\Config\UserAccountConfig;
use Adm\Config\UserGroupConfig;
use Adm\Crud\LaraCrud;
use Adm\Library\Cloudflare\Cloudflare;
use Adm\Models\ChangeLog;
use Adm\Models\UserLocationLog;
use App\Library\MyBespokeApi;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Exception\NotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config as Config;

/**
 * Class AdmController
 * @package Adm\Controllers
 * @property LaraCrud $crud
 * @property string $dashboard
 */
class AdmController extends Controller
{
    /**
     * Local LaraCrud object
     * @var LaraCrud
     */
    protected $crud;

    /**
     * The default dashboard template
     * @var string
     */
    public $dashboard = 'dashboard';

    /**
     * Array containing any items within the CMS that have their own dedicated route, and should only be accessed
     * by that route to keep any custom functionality for certain items based on ID (for example the home page)
     *
     * @var array
     */
    protected $items_with_custom_cms_routes = [
        'pages' => [
            1 => 'home-page',
            5 => 'news-archive',
            6 => 'case-study-archive'
        ]
    ];

    /**
     * Default function called.
     * This function creates a CRUD instance and boots it
     *
     * @param LaraCrud $crud
     * @return bool|\Illuminate\Support\Facades\View
     */
    public function index(LaraCrud $crud){
        $this->crud = $crud;

        // If we're updating an item...
        if( isset( $this->crud->parameters['table'] ) && isset( $this->crud->parameters['id'] ) ){
            // ... and that item is set in our items with custom routes array...
            if( isset( $this->items_with_custom_cms_routes[ $this->crud->parameters['table'] ][ $this->crud->parameters['id'] ] ) ){
                // ... then get the custom route...
                $new_route = $this->items_with_custom_cms_routes[ $this->crud->parameters['table'] ][ $this->crud->parameters['id'] ];

                // ... and redirect to it!
                header( "Location: " . route( 'adm.path', $new_route ) );
                exit;
            }
        }

        if( get_class($this) == 'Adm\\Controllers\\AdmController' && isset($this->crud->parameters['table']) && !empty($this->crud->parameters['table']) ){
            $output = $this->_handleModularLoad();
            if( $output !== false ){
                return $output;
            }
        }
        $this->crud->init($this);
        return $this->crud->boot();
    }

    /**
     * Function called by index() to determine if the current request needs to be routed through a module
     *
     * @return bool
     */
    private function _handleModularLoad(){

        $controller = $this->_findModuleController();
        if( $controller !== false ) {
            return $controller->index($this->crud);
        }

        return false;
    }

    /**
     * Centralised function to return a module's AdmController if it contains the function for the current
     * CRUD table.
     *
     * @return bool
     */
    private function _findModuleController(){
        $controller = false;

        $modules_dir = base_path('modules');
        $modules = array_filter(glob($modules_dir . '/*', GLOB_ONLYDIR));

        $available_modules = (array)config( 'modules.available' );

        foreach ($modules as $module_path) {
            $path_parts = explode('/', $module_path);
            $module_ns = end($path_parts);

            if( !in_array( $module_ns, $available_modules ) ){
                continue;
            }

            $reference = '\\Modules\\' . $module_ns . '\\Adm\\Controllers\\ModuleController';

            if (class_exists($reference) && method_exists($reference, $this->crud->parameters['table'])) {
                $controller = new $reference();
                $controller->setCrudInstance( $this->crud );
            }
        }

        return $controller;
    }

    /**
     * Function called by ajax_call() to determine if the current request needs to be routed through a module
     *
     * @param $function
     * @param $request
     * @return bool
     */
    private function _handleModularAjaxLoad( $function, $request ){
        $modules_dir = base_path( 'modules' );
        $modules = array_filter( glob($modules_dir.'/*', GLOB_ONLYDIR) );

        $available_modules = (array)config( 'modules.available' );

        foreach( $modules as $module_path ) {
            $path_parts = explode('/', $module_path);
            $module_ns = end($path_parts);

            if( !in_array( $module_ns, $available_modules ) ){
                continue;
            }

            $reference = '\\Modules\\' . $module_ns . '\\Adm\\Controllers\\ModuleController';

            if (class_exists( $reference ) && method_exists( $reference, $function ) ) {
                $controller = new $reference();
                $controller->crud = $this->crud;
                return $controller->$function( $request );
            }
        }

        return false;
    }

    /**
     * Default AJAX function called.
     * All AJAX function calls get routed through here
     *
     * @param Request $request
     * @return bool
     */
    public function ajax_call( Request $request ){
        $this->crud = new LaraCrud( $request );
        $this->crud->init( $this );
        $function = $request->route()->parameter('ajax_call');
        if( method_exists( $this, $function ) ){
            $response = $this->{$function}();
        }
        elseif( method_exists( $this->crud, $function ) ){
            $response = $this->crud->{$function}();
        }
        elseif( $this->crud->fieldAjaxCallExists( $function ) ){
            $response = $this->crud->fieldAjaxCall( $function );
        }
        else {
            $response = $this->_handleModularAjaxLoad( $function, $request );
        }

        if( $response !== false ){
            return $response;
        } else {
            throw new NotFoundException;
        }
    }

    /**
     * Function called to get the data required for the dashboard template
     * Gets Recent Changes, Latest Form Submissions, Google Analytics Data, and data from the API
     *
     * @return array
     */
    public function dashboard_data(){
        $dashboard_data = [];

        // Dashboard Permission Overrides
        if( \Auth::user()->userGroup->super_admin ){
            $can_view_latest_edits = $can_view_form_submissions = true;
        } else {
            $can_view_latest_edits = $can_view_form_submissions = false;

            $dashboard_permissions = \Auth::user()->userGroup->dashboard_permissions;
            foreach ($dashboard_permissions as $permission) {
                if ($permission == 'show_latest_edits') {
                    $can_view_latest_edits = true;
                }
                if ($permission == 'show_form_submissions') {
                    $can_view_form_submissions = true;
                }
            }
        }

        // Get Latest Edits
        $last_updated = $can_view_latest_edits ? ChangeLog::limit(15)->orderBy('changed', 'desc')->get() : array();

        $dashboard_data['has_latest_edits'] = sizeof( $last_updated ) > 0;
        $dashboard_data['latest_edits'] = $last_updated;

        $available_modules = (array)config( 'modules.available' );

        $dashboard_data['has_latest_submissions'] = false;
        if( in_array( 'FormBuilder', $available_modules ) && class_exists( '\\Modules\\FormBuilder\\Models\\FormSubmission' ) && $can_view_form_submissions ){
            $latest_submissions = \Modules\FormBuilder\Models\FormSubmission::where([
                'language_id' => (!empty($this->crud) ? $this->crud->language_id : 1)
            ])->orderBy('created_at', 'desc')->take(10)->with('form')->get();

            if( !empty( $latest_submissions ) && sizeof( $latest_submissions ) > 0 ){
                $dashboard_data['has_latest_submissions'] = true;

                $submission_display_data = [];
                foreach( $latest_submissions as $submission ){
                    if( !$submission->form || !$submission->fields ){ continue; }

                    $field_data = [];
                    foreach( $submission->fields as $field => $value ){
                        $field_data[ $field ] = is_array( $value ) ? implode( ", ", $value ) : $value;
                    }

                    $field_string = implode( ", ", array_filter( $field_data ) );
                    if( strlen( $field_string ) > 50 ){
                        $field_string = substr( $field_string, 0, 50 ) . "...";
                    }

                    $this_submission_display_data = [
                        'form_id' => $submission->form->form_id,
                        'submission_id' => $submission->forms_log_id,
                        'form_title' => $submission->form->title,
                        'form_submission' => $field_string,
                        'submitted_on' => $submission->created_at,
                        'email' => false
                    ];

                    $has_email_field = $submission->form->email_field;
                    if( $has_email_field ){
                        if( isset( $submission->fields[ $has_email_field ] ) ){
                            $this_submission_display_data['email'] = $submission->fields[ $has_email_field ];
                        }
                    }

                    $submission_display_data[] = $this_submission_display_data;
                }

                $dashboard_data['latest_submissions'] = $submission_display_data;
            }
        }

        // True: Show bar across top of admin, False: Dont.
        $dashboard_data['show_top_error'] = false;

        $my_bespoke_api = new MyBespokeApi;

        $dashboard_data['bespoke_hosted_tier']  = $my_bespoke_api->getOrganisationSupportTier() ? $my_bespoke_api->getOrganisationSupportTier()->title : false;
        $dashboard_data['bespoke_hosted'] = $dashboard_data['bespoke_hosted_tier'] ? true : false;

        $defaultDefaults = new \StdClass();
        $defaultDefaults->phone = "01772 591100";
        $defaultDefaults->opening_hours = "Mon-Fri 09:00-17:30";
        $defaultDefaults->support_document_url = "https://www.bespokeinternet.com/documents/2016-Fast-Response-Support+High-Speed-Hosting.pdf";

        $dashboard_data['bespoke_cms_defaults']  = $my_bespoke_api->getCmsDefaults() ? $my_bespoke_api->getCmsDefaults() : $defaultDefaults;

        // True: Show news, False: Show fallback
        $dashboard_data['bespoke_news'] =  $my_bespoke_api->getCmsMessages() ? $my_bespoke_api->getCmsMessages() : false;
        // Limit the CMS News to 15 items
        $dashboard_data['bespoke_news'] = is_array( $dashboard_data['bespoke_news'] ) ? array_slice( $dashboard_data['bespoke_news'], 0, 15, true ) : [];

        // Get the site's RM
        $dashboard_data['relationship_manager'] = $my_bespoke_api->getRelationshipManager();

        // True: Hide support/hosting/news blocks, False: Dont.
        $dashboard_data['migrated_to_client'] = false;

        // Custom overrides for demo
        if( !empty($this->crud) && $this->crud->request->input('no_analytics') ){
            $dashboard_data['show_analytics'] = false;
        }

        if( !empty($this->crud) && $this->crud->request->input('no_rm') ){
            $dashboard_data['relationship_manager'] = false;
        }

        if( !empty($this->crud) && $this->crud->request->input('no_tier') ){
            $dashboard_data['bespoke_hosted'] = false;
        }

        if( !empty($this->crud) && $this->crud->request->input('no_news') ){
            $dashboard_data['bespoke_news'] = false;
        }

        return $dashboard_data;
    }

    /**
     * AJAX Function used to load in Google Analytics API data.
     * Instead of loading this in on load of the dashboard, use AJAX to render it - speeding up initial CMS load time.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ajax_dashboard_analytics_data(){
        $dashboard_data = [];

        // Dashboard Permission Overrides
        if( \Auth::user()->userGroup->super_admin ){
            $can_view_analytics = $can_view_latest_edits = $can_view_form_submissions = true;
        } else {
            $can_view_analytics = $can_view_latest_edits = $can_view_form_submissions = false;

            $dashboard_permissions = \Auth::user()->userGroup->dashboard_permissions;
            foreach ($dashboard_permissions as $permission) {
                if ($permission == 'show_analytics') {
                    $can_view_analytics = true;
                }
            }
        }

        // True: Show live analytics, False: Show upsell
        $dashboard_data['show_analytics'] = $can_view_analytics ? Config::get('cms-analytics.enabled') : false;

        // True: Show analytics, False: Show error placeholder
        $dashboard_data['analytics_available'] = false;
        if( $dashboard_data['show_analytics'] ) {
            try {
                $cms_analytics = new \App\Library\CmsAnalytics();
                $analytics_data = $cms_analytics->getData();

                if( !empty( $analytics_data ) ) {
                    $dashboard_data['analytics_available'] = true;
                    $dashboard_data['analytics_data'] = $analytics_data;
                }
            }
            catch( \Google_Service_Exception $e ){}
            catch( \Google_Auth_Exception $e ){}
        }

        // Get the site's RM
        $my_bespoke_api = new MyBespokeApi;
        $dashboard_data['relationship_manager'] = $my_bespoke_api->getRelationshipManager();

        return view( 'dashboard.analytics', $dashboard_data );
    }

    /**
     * Serve the image from my.bespoke for the current site's relationship manager
     */
    public function rm_image(){
        $my_bespoke_api = new MyBespokeApi;
        $rm = $my_bespoke_api->getRelationshipManager();
        $image_path = 'http://my.bespokedigital.agency/plugins/timthumb.php?src=http://my.bespokedigital.agency/profilephotos/' . $rm->user_photo . '&w=80&h=80&zc=1';

        header( 'Content-type: image/jpeg' );
        header( 'Content-type: image/png' );
        header( 'Content-type: image/gif' );
        echo file_get_contents( $image_path );
        exit;
    }

    /**
     * AJAX Call used to determine who is viewing a particular form
     * Excludes Super-Admin users
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax_whos_on_this_page(){
        if( \Auth::user()->userGroup->super_admin ){
            return response()->json( '' );
        }

        UserLocationLog::where('adm_user_id', \Auth::user()->id)->delete();

        UserLocationLog::create([
            'adm_user_id' => \Auth::user()->id,
            'location' => $this->crud->request->all()['location']
        ]);

        $users_on_same_page = UserLocationLog::UsersOnSamePage($this->crud->request->all()['location'])->get();

        $users = [];
        foreach ( $users_on_same_page as $user ) {
            $users[] = $user->adm_user->name;
        }

        if ( $users ) {
            $message = join(' and ', array_filter(array_merge(array(join(', ', array_slice($users, 0, -1))), array_slice($users, -1)), 'strlen'));
            $message .= (count($users) > 1 ? ' are' : ' is' ) . ' also editing this page.';
        } else {
            $message = '';
        }

        return response()->json( $message );
    }

    /**
     * CRUD Function for the 'defaults' table
     */
    public function general_settings(){
        $this->crud->setTable('defaults');
        $this->crud->setSubject('General Settings');
        $this->crud->single();
        $this->crud->changeType('website_address', 'textarea');
        $this->crud->setRelation('default_language', 'languages', 'title');

        $this->crud->addColumn( 'image' );

        $edit_exclusions = ['website_title', 'website_email', 'google_analytics_ua_code', 'image'];

        if( !\Auth::user()->userGroup->super_admin ){
            $edit_exclusions[] = 'default_language';
        }
        $this->crud->excludeFromEdit( $edit_exclusions );

        if( isset($this->crud->parameters['action']) ){
            if( $this->crud->parameters['action'] == 'edit' ){
                $this->crud->extra_view_data['title_override'] = 'Edit General Settings';
            }
        }

        $this->crud->columnGroup( 'main', ['default_language', 'google_analytics_ua_code', 'image'] );
    }

    /**
     * Callback function used to get field labels for General Settings
     * @return array
     */
    public function getGeneralSettingsDefaultLabels(){
        return GeneralSettingsConfig::$labels;
    }

    /**
     * Callback function used to get field help text for General Settings
     * @return array
     */
    public function getGeneralSettingsDefaultHelp(){
        return GeneralSettingsConfig::$help;
    }

    /**
     * CRUD Function for admin user accounts
     */
    public function user_accounts(){
        $this->crud->setSubject('User Accounts');
        $this->crud->setTable('adm_users');
        $this->crud->displayColumn('first_name');

        //set required
        if( \Auth::user()->userGroup->super_admin ){
            $group_conditions = array();
        }
        elseif( \Auth::user()->userGroup->admin ){
            $group_conditions = array( 'super_admin' => 0 );
            $this->crud->where( 'user_group_id', 1, '!=' );
        }
        else {
            $group_conditions = array( 'super_admin' => 0, 'admin' => 0 );
            $this->crud->where('user_group_id', 1, '!=');
        }
        $this->crud->setRelation('user_group_id', 'adm_user_groups', 'name', 'id', '', false, $group_conditions);

        $this->crud->fieldAttributes( 'username', ['autocomplete' => 'nope'] );

        $this->crud->columns('username', 'first_name', 'last_name', 'email', 'user_group_id', 'status');

        $this->crud->excludeFromEdit( 'remember_token' );

        $this->crud->columnGroup( 'main', array( 'first_name', 'last_name', 'email', 'username' ) );
        $this->crud->columnGroup( 'side', array( 'user_group_id', 'admin', 'status' ) );

        $this->crud->extra_view_data['extra_actions'][] = [
            'url' => route('adm.path', ['user-groups']),
            'name' => 'Manage User Groups'
        ];

    }

    /**
     * Callback function used to get field labels for User Accounts
     * @return array
     */
    public function getUserAccountsDefaultLabels(){
        return UserAccountConfig::$labels;
    }

    /**
     * Callback function used to get field help text for User Accounts
     * @return array
     */
    public function getUserAccountsDefaultHelp(){
        return UserAccountConfig::$help;
    }

    /**
     * Callback function called before a $_POST array is processed and saved to the item.
     * Used with AdmUsers as non super-admin users shouldn't be able to disable or enable super-admin users.
     *
     * @param $input_array
     * @param $item
     */
    public function beforeSavedAdmUsersItem( &$input_array, $item ){
        if( $item->user_group_id == 1 && !\Auth::user()->userGroup->super_admin ){
            // If the POSTed user group is the super-admin user group and  the current user isn't a super-admin,
            // revert ALL fields back to what they were as they shouldn't be changing this
            foreach( $input_array as $field => $value ){
                $input_array[ $field ] = $item->{$field};
            }
        }
    }

    /**
     * Custom Validator specifying validation rules for admin user accounts
     *
     * @param $validation
     * @param $inputs
     */
    public function getUserAccountsValidationRules( &$validation, $inputs ){
        if( isset( $this->crud->parameters['id'] ) ){
            $validation['email'] = 'unique:adm_users,email,' . $this->crud->parameters['id'];
            $validation['username'] = 'unique:adm_users,username,' . $this->crud->parameters['id'];
        } else {
            $validation['email'] = 'unique:adm_users';
            $validation['username'] = 'unique:adm_users';
        }
    }

    /**
     * CRUD Function for admin user groups
     */
    public function user_groups(){
        $this->crud->setSubject('User Account Groups');
        $this->crud->setTable('adm_user_groups');
        $this->crud->displayColumn('name');

        $this->crud->changeType('permissions', 'permission');

        $options = $this->generateNavigation(true);
        $options[] = ['url' => route('adm.path', ['user-groups']), 'title' => 'User Groups', 'position' => 0];

        // Now, strip the CMS path from the permission URLs
        $adm_path = parse_url( route( 'adm.path' ), PHP_URL_PATH );
        foreach( $options as $ind => $option ){
            $option_url = $option['url'];
            $options[ $ind ]['url'] = str_replace( $adm_path, '', parse_url( $option_url, PHP_URL_PATH ) );
        }

        if( !\Auth::user()->userGroup->super_admin ){
            foreach( $options as $ind => $option ){
                if( !in_array( $option['url'], \Auth::user()->userGroup->permissions ) ){
                    unset( $options[ $ind ] );
                }
            }
        }

        $this->crud->fieldOptions('permissions', $options);

        $this->crud->changeType('dashboard_permissions', 'permission');
        $dashboard_options = [
            ['title' => 'Google Analytics', 'url' => 'show_analytics', 'position' => 0],
            ['title' => 'Latest Edits', 'url' => 'show_latest_edits', 'position' => 1],
            ['title' => 'Latest Form Submissions', 'url' => 'show_form_submissions', 'position' => 2]
        ];
        $this->crud->fieldOptions('dashboard_permissions', $dashboard_options);

        if ( isset( $this->crud->parameters['id'] ) && $this->crud->parameters['action'] == 'edit' ) {
            $adm_user_group = \Adm\Models\AdmUserGroup::findOrFail( $this->crud->parameters['id'] );

            if ( $adm_user_group->super_admin ) {
                $this->crud->excludeFromEdit( 'permissions' );
                $this->crud->excludeFromEdit( 'dashboard_permissions' );
            }
        }

        $this->crud->extra_view_data['extra_actions'][] = [
            'url' => route('adm.path', ['user-accounts']),
            'name' => 'Manage User Accounts'
        ];

        $this->crud->columnGroup( 'main', array( 'name' ) );
        $this->crud->columnGroup( 'side', array( 'admin', 'super_admin' ) );

        if( \Auth::user()->userGroup->super_admin ){
            $this->crud->columns('name', 'admin', 'super_admin');
        }
        elseif( \Auth::user()->userGroup->admin ){
            $this->crud->columns('name', 'admin');
            $this->crud->where('super_admin', 0);
            $this->crud->excludeFromEdit( 'super_admin' );
        }
        else {
            $this->crud->columns('name');
            $this->crud->where('super_admin', 0);
            $this->crud->where('admin', 0);
            $this->crud->excludeFromEdit('admin', 'super_admin');
        }
    }

    /**
     * Callback function used to get field labels for User Groups
     * @return array
     */
    public function getUserGroupsDefaultLabels(){
        return UserGroupConfig::$labels;
    }

    /**
     * Callback function used to get field help text for User Groups
     * @return array
     */
    public function getUserGroupsDefaultHelp(){
        return UserGroupConfig::$help;
    }

    /**
     * CRUD Function for 301 Redirects
     */
    public function redirect_management(){
        $this->crud->setSubject('301 Redirects');
        $this->crud->setTable('301_redirects');
        $this->crud->displayColumn('old_url');
        $this->crud->linkedField('old_url');

        $this->crud->columns( 'old_url', 'new_url' );
        $this->crud->columnGroup('main', ['old_url', 'new_url']);
    }

    /**
     * Callback function used to get field labels for Redirect Management
     * @return array
     */
    public function getRedirectManagementDefaultLabels(){
        return RedirectManagementConfig::$labels;
    }

    /**
     * Callback function used to get field help text for Redirect Management
     * @return array
     */
    public function getRedirectManagementDefaultHelp(){
        return RedirectManagementConfig::$help;
    }

    /**
     * Custom 301 Redirect CSV Import Functionality
     */
    public function redirect_importer(){
        if( !\Auth::user()->userGroup->super_admin ){
            header( "Location: " . route( 'adm.path' ) );
            exit;
        }

        $view_data = [
            'title' => '301 Redirect Importer'
        ];

        if( $this->crud->request->isMethod( 'post' ) ){
            $rules = [
                'csv_file' => 'required|file'
            ];
            $messages = [
                'csv_file.required' => 'Please provide a CSV file',
                'csv_file.file' => 'Please provide a CSV file'
            ];

            $request = $this->crud->request;
            $validator = Validator::make( $request->all(), $rules, $messages );

            $validator->after( function( $validator ) use ( $request ){
                if( $request->csv_file->getClientOriginalExtension() != 'csv' ){
                    $validator->errors()->add( 'csv_file', 'Please provide a file with a .csv extension' );
                }
            });

            if( !$validator->fails() ){
                $csv_handle = fopen( $request->csv_file->path(), 'r' );
                if( $csv_handle !== false ){
                    $headers_processed = false;
                    $row = 1;

                    $old_domain = $request->input( 'old_domain' );
                    if( !$old_domain ){ $old_domain = ''; }

                    while( ($data = fgetcsv( $csv_handle )) !== false ){
                        if( !$headers_processed ){
                            $headers_processed = true;
                            continue;
                        }

                        $row++;

                        $old = isset( $data[0] ) ? $data[0] : false;
                        $new = isset( $data[1] ) ? $data[1] : false;

                        if( empty( $old ) || empty( $new ) ){
                            $validator->errors( 'csv_file', 'Row ' . $row . ' of the CSV could not be processed.' );
                            continue;
                        }

                        // Remove the old domain name if required
                        $old = str_replace( $old_domain, '', $old );
                        $new = str_replace( $old_domain, '', $new );

                        // And remove leading slashes
                        $old = ltrim( $old, '/' );
                        $new = ltrim( $new, '/' );

                        // Now, we can add the redirect!
                        \App\Models\Redirect::create( ['old_url' => $old, 'new_url' => $new] );
                    }

                    $view_data['success_message'] = 'Redirect CSV Imported Successfully!';
                } else {
                    $validator->errors()->add( 'csv_file', 'Sorry, we couldn\'t open the uploaded file. Please try again' );
                }
            }

            $view_data['errors'] = $validator->errors();
        }

        $this->crud->setView( 'redirect-import', $view_data );
    }

    /**
     * Function called that generates the admin navigation.
     * Checks whether the logged in user can access elements of the site and generated the sidebar menu
     *
     * @param bool|false $override_permissions
     * @return array
     */
    public function generateNavigation($override_permissions = false){
        $nav_items = array();

        $nav_items[] = array( 'title' => 'Dashboard', 'url' => 'adm/', 'position' => 0 );
        $nav_items[] = array( 'title' => 'Asset Manager', 'url' => route('adm.asset-manager'), 'position' => 998 );
        $nav_items[] = array( 'title' => 'Settings', 'url' => route('adm.path', 'text-translations'), 'position' => 999, 'sub' => [
            //[ 'title' => 'General Settings', 'url' => route('adm.path', 'general-settings'), 'position' => 0 ],
            [ 'title' => 'Common Text', 'url' => route('adm.path', 'text-translations'), 'position' => 1 ],
            [ 'title' => 'Social Media', 'url' => route('adm.path', 'social-media'), 'position' => 2 ],
            [ 'title' => 'User Accounts', 'url' => route('adm.path', 'user-accounts'), 'position' => 3 ],
            [ 'title' => '301 Redirects', 'url' => route('adm.path', 'redirect-management'), 'position' => 4 ]
        ]);

        if( \Auth::user()->userGroup->super_admin ){
            $nav_items[] = array(
                'title' => 'Bespoke Tools', 'url' => '#', 'position' => 1000, 'sub' => [
                    [ 'title' => '301 Redirect Import', 'url' => route('adm.path', 'redirect-importer'), 'position' => 1 ],
                    [ 'title' => 'Page Builder Wizard', 'url' => route('adm.path', 'page-builder-wizard'), 'position' => 10 ],
                    [ 'title' => 'Module Management', 'url' => route('adm.path', 'module-management'), 'position' => 20 ]
                ]
            );
        }

        $modules_dir = base_path( 'modules' );
        $modules = array_filter( glob($modules_dir.'/*', GLOB_ONLYDIR) );

        $available_modules = (array)config( 'modules.available' );

        foreach( $modules as $module_path ){
            $path_parts = explode('/', $module_path );
            $module_ns = end($path_parts);

            if( !in_array( $module_ns, $available_modules ) ){
                continue;
            }

            if( class_exists( '\\Modules\\' . $module_ns . '\\Config\\Navigation' ) ){
                $class_ref = '\\Modules\\' . $module_ns . '\\Config\\Navigation';
                $nav_class = new $class_ref();
                $nav_class_nav_items = $nav_class->getNavItems();

                foreach( $nav_class_nav_items as $nav_class_item ){
                    $nav_items[] = $nav_class_item;
                }
            }
        }

        uasort( $nav_items, array($this, "_sortNavItems") );
        foreach( $nav_items as $ind => $item ){
            if( !empty( $item['sub'] ) ){
                uasort( $nav_items[ $ind ]['sub'], array($this, "_sortNavItems") );

                foreach( $item['sub'] as $sub_ind => $item ){
                    uasort( $nav_items[ $ind ]['sub'][ $sub_ind ], array($this, "_sortNavItems") );
                }
            }
        }

        $this->_generateNavigationIndicatorAndCurrent( $nav_items, $override_permissions );

        return $nav_items;
    }

    /**
     * Centralised function to order items within the CMS navigation
     *
     * @param $a
     * @param $b
     * @return bool|int
     */
    private function _sortNavItems( $a, $b ){
        $a_position = isset($a['position']) ? $a['position'] : 0;
        $b_position = isset($b['position']) ? $b['position'] : 0;

        if( $a_position == $b_position ){ return 0; }
        return $a_position > $b_position;
    }

    /**
     * Centralised recursive function used to set whether nav items are current, and whether they should show
     * the cut-in indicator or not.
     * Also, removes navigation items that the current user does not have access to.
     *
     * @param $nav_items
     * @param bool|false $override_permissions
     */
    private function _generateNavigationIndicatorAndCurrent( &$nav_items, $override_permissions = false ){
        $user_group_permissions = \Auth::user()->userGroup->permissions;

        $is_super_admin = \Auth::user()->userGroup->super_admin;

        foreach( $nav_items as $ind => $nav_item ) {

            // If we have sub-items, set their show_indicator flags first.
            // If a sub-item has the indicator, the parent shouldn't!
            $child_has_indicator = false;
            if( !empty( $nav_items[ $ind ]['sub'] ) ){
                $this->_generateNavigationIndicatorAndCurrent( $nav_items[ $ind ]['sub'], $override_permissions );

                foreach( $nav_items[ $ind ]['sub'] as $child ){
                    if( isset( $child['show_indicator'] ) && $child['show_indicator'] ){
                        $child_has_indicator = true;
                    }
                }
            }

            // If the current item's children has the indicator, this item is 'current'
            $nav_items[$ind]['current'] = $child_has_indicator;

            // Generate some variables to make the below if conditions more readable
            $current_url = url()->current() . '/';
            $is_dashboard = ($nav_item['url'] == 'adm/' && url()->current() == url($nav_item['url']));
            $current_is_prefix = ($nav_item['url'] != 'adm/' && stripos($current_url, url($nav_item['url']) . '/') === 0);

            if ( !$child_has_indicator && ($is_dashboard || $current_is_prefix) ) {

                // If the current URL equals the current nav item's URL
                if ($current_url == url($nav_item['url']) . '/') {
                    // Then check for any sibling links.
                    foreach ($nav_items as $ind_to_remove => $link) {
                        // And if the sibling link has the indicator, remove it.
                        if (isset($link['show_indicator']) && $link['show_indicator'] == true && $ind_to_remove != $ind) {
                            $nav_items[$ind_to_remove]['show_indicator'] = false;
                            $nav_items[$ind_to_remove]['current'] = false;
                        }
                    }
                }

                $nav_items[$ind]['show_indicator'] = true;
                $nav_items[$ind]['current'] = true;
            }

            // If the current user can't access the nav item
            $nav_item_minus_url = str_replace( route('adm.path'), '', $nav_item['url'] );
            if ( !$override_permissions && !$is_super_admin && !in_array($nav_item_minus_url, $user_group_permissions) ) {
                // If the nav item doesn't have any children, we can remove it
                if( empty($nav_items[$ind]['sub']) ) {
                    unset($nav_items[$ind]);
                }
                // Else, we need to set the current item to have a link of the first available item
                else {
                    $first_child = reset( $nav_items[$ind]['sub'] );
                    $nav_items[$ind]['url'] = $first_child['url'];
                }
            }
        }
    }

    /**
     * AJAX Function called to bulk update the status field on items
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax_bulk_update_status(){
        $this->crud->setTable( $this->crud->request->input('table') );

        $non_updates = [];
        foreach( $this->crud->request->input('items') as $item_id ){
            if( $item_id == 'on' ){ continue; }

            $this->crud->parameters['id'] = $item_id;

            if( $this->crud->ajax_boot() ){
                $item = $this->crud->getItem();

                if( $item->status !== false ){
                    $item->status = $this->crud->request->input('status');
                    if( !$item->save() ){
                        $non_updates[] = $item->title;
                    }
                }

                $this->crud->currentItem = false;
            }
        }

        $json = [];
        if( sizeof( $non_updates ) == 0 ){
            $json['success'] = true;
        } else {
            $json['success'] = false;
            $json['non_updates'] = $non_updates;
        }

        Cache::flush();

        return response()->json( $json );
    }

    /**
     * AJAX Function called to bulk delete items
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax_bulk_delete(){
        $this->crud->setTable( $this->crud->request->input('table') );

        $non_deletes = [];
        foreach( $this->crud->request->input('items') as $item_id ){
            if( $item_id == 'on' ){ continue; }

            $this->crud->parameters['action'] = 'destroy';
            $this->crud->parameters['url_table'] = $this->crud->request->input('table');
            $this->crud->parameters['table'] = str_replace( '-', '_', $this->crud->parameters['url_table'] );
            $this->crud->parameters['id'] = $item_id;

            $module_controller = $this->_findModuleController();
            if( $module_controller !== false ){
                $this->crud->setCaller( $module_controller );
            }

            if( $this->crud->ajax_boot( true ) ){
                $item = $this->crud->getItem();

                if( !$this->crud->deleteItem() ){
                    $non_deletes[] = $item->title;
                }

                $this->crud->currentItem = false;
            }
        }

        $json = [];
        if( sizeof( $non_deletes ) == 0 ){
            $json['success'] = true;
        } else {
            $json['success'] = false;
            $json['non_updates'] = $non_deletes;
        }

        Cache::flush();

        return response()->json( $json );
    }

    /**
     * AJAX Function called when generating the 'Link List' within a TinyMCE instance
     * Returns an array of 'categorised' URLs (i.e. Pages -> array of Page URLs, News -> News URLs, etc.)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax_get_urls_for_tinymce(){
        $urls = \App\Models\Url::where([
            'language_id' => $this->crud->language_id
        ])->get();

        $site_urls = array();
        foreach( $urls as $url ){
            if( empty( $url->object ) || !is_object( $url->object ) ){
                continue;
            }

            if( !isset( $site_urls[ $url['table_name'] ] ) ){
                $site_urls[ $url['table_name'] ] = array();
            }

            $site_urls[ $url['table_name'] ][] = array(
                'title' => $url->object->title,
                'value' => route('theme', $url->url)
            );
        }

        $json = array();
        foreach( $site_urls as $table_name => $urls ){
            $json[] = array(
                'title' => ucwords( \Illuminate\Support\Str::singular( str_replace( "_", " ", $table_name ) ) ),
                'menu' => $urls
            );
        }

        return response()->json( $json );
    }

    public function ajax_restore_content_version(){
        $table_name = $this->crud->request->input( 'table_name' );
        $item_id = $this->crud->request->input( 'item_id' );
        $language_id = $this->crud->request->input( 'language_id' );
        $requested_version = $this->crud->request->input( 'version' );

        $success = false;
        $this->crud->setTable( $table_name );
        if( $this->crud->ajax_boot( false ) ){
            $success = $this->crud->restoreContentVersion( $item_id, $language_id, $requested_version );
        }

        if( $success ){
            return response()->json( ['success' => true] );
        } else {
            return response()->json( ['success' => false] );
        }
    }

    /**
     * If the CMS should fall over for any reason, we can render a professional looking error page.
     *
     * @param \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function throwGenericError( \Exception $exception ){
        $view_data = $this->dashboard_data();
        $view_data['me'] = Auth::user();
        $view_data['navigation'] = $this->generateNavigation();
        $view_data['exception'] = $exception;

        // IP Locked error state - I.e. only show the debug info if it's our office IP
        $view_data['show_exception'] = ($_SERVER['REMOTE_ADDR'] == '82.70.220.206') || (app('env') == 'dev');

        return response()->view("errors.generic", $view_data);
    }

    /**
     * Module Management
     * Custom CMS Page allowing super-admin users access to modify the
     * available website modules through the CMS
     */
    public function module_management(){
        if( !\Auth::user()->userGroup->super_admin ){
            header( "Location: " . route( 'adm.path' ) );
            exit;
        }

        $view_data = [
            'title' => 'Module Management',
        ];

        $config_file = config_path( 'modules.php' );

        if( request()->isMethod( 'post' ) ){
            $selected_modules = request()->input( 'modules' );

            $file_contents = '';
            $file_contents .= '<?php' . PHP_EOL;
            $file_contents .= PHP_EOL;
            $file_contents .= 'return [' . PHP_EOL;
            $file_contents .= PHP_EOL;
            $file_contents .= "\t" . "'available' => [" . PHP_EOL;

            foreach( $selected_modules as $module ){
                $file_contents .= "\t\t" . "'" . $module . "'," . PHP_EOL;
            }

            $file_contents .= "\t" . ']' . PHP_EOL;
            $file_contents .= PHP_EOL;
            $file_contents .= '];';

            file_put_contents( $config_file, $file_contents );

            header( 'Location: ' . route('adm.path', 'module-management') );
            exit;
        }

        $available_modules = (array)config( 'modules.available' );

        $all_modules = [];
        $module_paths = glob( base_path( 'modules' ) . '/*', GLOB_ONLYDIR );
        foreach( $module_paths as $module_dir ){
            $all_modules[] = trim( str_replace( base_path( 'modules' ), '', $module_dir ), '/' );
        }

        $view_data['available_modules'] = $available_modules;
        $view_data['all_modules'] = $all_modules;

        $this->crud->setView( 'module-management', $view_data );
    }

    /**
     * AJAX Switch CF Dev Mode
     * Used to switch Cloudflare's development mode between on and off
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax_switch_cf_dev_mode(){
        $Cloudflare = new Cloudflare();
        $json = [
            'success'   => false,
            'response'  => false,
            'error'     => 'Unavailable at this time'
        ];

        $success = $Cloudflare->switch_development_mode();
        if( $success ){
            $json['success'] = $success;
            $json['response'] = $success;
        }

        return response()->json( $json );
    }

    /**
     * AJAX Purge CF Cache
     * Used to purge Cloudflare's cache
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax_purge_cf_cache(){
        $Cloudflare = new Cloudflare();
        $json = [
            'success'   => false,
            'response'  => false,
            'error'     => 'Unavailable at this time'
        ];

        $success = $Cloudflare->purge_site();
        if( $success ){
            $json['success'] = true;
            $json['response'] = 'purged';
        }

        return response()->json( $json );
    }
}
