<?php

namespace Adm\Controllers\Auth;

use Adm\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/adm';
    protected $redirectAfterLogout = '/adm';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        $referrer = url()->previous();
        if( stristr( $referrer, '/adm/' ) !== false && stristr( $referrer, '/adm/log' ) === false ){
            request()->session()->put( 'adm_login_referrer', url()->previous() );
        }
        return view('auth.login');
    }

    /**
     * Authenticated
     * Custom Override to the authenticated function to redirect back to
     * the referrer logged on the login form
     *
     * @param \Illuminate\Http\Request $request
     * @param $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function authenticated( Request $request, $user ){
        if( request()->session()->has( 'adm_login_referrer' ) ){
            $referrer = request()->session()->get( 'adm_login_referrer' );
            request()->session()->remove( 'adm_login_referrer' );
            return redirect()->to( $referrer );
        }

        return redirect()->intended($this->redirectPath());
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('adm');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'username';
    }

    /**
     * Log the user out of the application.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect($this->redirectAfterLogout);
    }

    public function ajaxLogin(Request $request)
    {
        $this->validateLogin($request);

        $credentials = $this->credentials($request);

        if ($this->guard()->attempt($credentials, $request->has('remember'))) {
            return response()->json(
                [
                    'authenticated' => true,
                    'new_csrf' => csrf_token(),
                ]
            );
        } else {
            return response()->json(
                [
                    'authenticated' => false,
                    'new_csrf' => csrf_token(),
                ]
            );
        }
    }

    public function checkAuth()
    {
        return response()->json(
            [
                'authenticated' => $this->guard()->check(),
                'new_csrf' => csrf_token(),
            ]
        );
    }

    /**
     * Get the needed authorization credentials from the request.
     * Overridden to make sure that only 'active' users can log in.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        $credentials = $request->only($this->username(), 'password');
        return \Illuminate\Support\Arr::add( $credentials, 'status', 1 );
    }
}
