<?php
namespace Adm\Controllers;

use Adm\Library\Cloudflare\Cloudflare;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Validator;

use App\Models\File;
use App\Models\FileDirectory;
use Adm\Models\FileUsage;
use App\Library\MyBespokeApi;
use Intervention\Image\Exception\NotFoundException;

/**
 * Class AssetManagerController
 * @package Adm\Controllers
 * @property Request $request
 * @property array $view_data
 */
class AssetManagerController extends Controller {
    /**
     * Internal reference to the current HTTP Request
     *
     * @var Request $request
     */
    protected $request;

    /**
     * Internal set of information to pass down to the view template
     *
     * @var array $view_data
     */
    protected $view_data;

    /**
     * Load in the 'landing' page for the asset manager, showing the directory tree and files listing
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index( Request $request ){
        $crud = new \Adm\Crud\LaraCrud( $request );
        $admController = new \Adm\Controllers\AdmController();
        $crud->init( $admController );
        $needs_redirect = $crud->checkUserPermissions();
        if( $needs_redirect ){
            return $needs_redirect;
        }

        $this->request = $request;

        // Set the default data for the page
        $this->_loadDefaultViewData();
        // And then load in the files listing
        $this->_loadFiles();
        // Then, check if a directory has been requested, and if so pull that through to the view
        $this->_loadCurrentDirectory();
        // Then, the directory listing
        $this->view_data['directories'] = $this->_loadDirectoryTree();

        $flat_dirs = [];
        $this->_flattenDirectoriesForDropdown( $flat_dirs, $this->view_data['directories'] );
        $this->view_data['move_to_directory_list'] = $flat_dirs;

        $template = 'asset-manager';
        if( $this->request->input('ajax') ){
            $template = 'asset-manager.ajax-asset-manager';
        }
        elseif( $this->request->input('modal') ){
            if( $this->request->input('tinymce') ){
                $template = 'asset-manager.tinymce-asset-manager';
            } else {
                $template = 'asset-manager.modal.asset-manager';
            }
        }

        return view($template, $this->view_data);
    }

    /**
     * Handler function used by the route to be able to dynamically link to functions in the controller
     *
     * @param Request $request
     * @return mixed
     */
    public function performAction( Request $request ){
        $this->request = $request;

        // Check that the passed function exists
        $function = $this->request->route()->parameter('function');
        if( method_exists( $this, $function ) ){
            // And if so, call it
            return $this->{$function}();
        }

        // Else throw a 404
        throw new NotFoundException;
    }

    /**
     * AJAX handler for reloading the directory listing.
     * Also loads in the delete folder, new folder & upload modals with a target of the specified directory
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Throwable
     */
    public function load_directory_list(){
        if( $this->request->input('directory_id') !== false && $this->request->input('directory_id') !== null ){
            \Session::put('directory_id', $this->request->input('directory_id') );
        }

        $this->_loadDefaultViewData();
        $this->_loadCurrentDirectory();

        $this->view_data['modal'] = $this->request->input('modal');
        $this->view_data['multiple_select'] = $this->request->input('multiple_select') == 'true';
        $this->view_data['directories'] = $this->_loadDirectoryTree();

        $directory_list = view('asset-manager.directory-list', $this->view_data)->render();
        $delete_action = view('asset-manager.delete-folder-action', $this->view_data )->render();
        $new_folder_modal = view('asset-manager.new-folder-modal', $this->view_data )->render();
        $upload_modal = view('asset-manager.upload-modal', $this->view_data )->render();

        $flat_dirs = [];
        $this->_flattenDirectoriesForDropdown( $flat_dirs, $this->view_data['directories'] );

        $json = ['directory_list' => $directory_list, 'delete_action' => $delete_action, 'new_folder' => $new_folder_modal, 'upload' => $upload_modal, 'flat_directories' => $flat_dirs];
        return $this->_jsonResponse( $json );
    }

    /**
     * AJAX handler for reloading the files listing.
     * Pass a directory ID via POST to load that directory's files
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Throwable
     */
    public function load_files_list(){
        if( $this->request->input('directory_id') !== false && $this->request->input('directory_id') !== null ){
            \Session::put('directory_id', $this->request->input('directory_id') );
        }

        $this->_loadDefaultViewData();
        $this->_loadFiles();
        $this->_loadCurrentDirectory();

        $this->view_data['modal'] = $this->request->input('modal');
        $this->view_data['multiple_select'] = $this->request->input('multiple_select') == 'true';

        $files_list = view($this->request->input('modal') ? 'asset-manager.modal.files-list' : 'asset-manager.files-list', $this->view_data)->render();

        $json = ['files_list' => $files_list, 'files_count' => sizeof( $this->view_data['files'])];
        return $this->_jsonResponse( $json );
    }

    /**
     * AJAX handler for reloading both directories & files.
     * Also loads in the delete folder, new folder & upload modals with a target of the specified directory
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Throwable
     */
    public function load_directory_and_files_list(){
        if( $this->request->input('directory_id') !== false && $this->request->input('directory_id') !== null ){
            \Session::put('directory_id', $this->request->input('directory_id') );
        }

        $this->_loadDefaultViewData();
        $this->_loadFiles();
        $this->_loadCurrentDirectory();

        $this->view_data['modal'] = $this->request->input('modal');
        $this->view_data['multiple_select'] = $this->request->input('multiple_select') == 'true';
        $this->view_data['directories'] = $this->_loadDirectoryTree();

        $directory_list = view('asset-manager.directory-list', $this->view_data)->render();
        $files_list = view($this->request->input('modal') ? 'asset-manager.modal.files-list' : 'asset-manager.files-list', $this->view_data)->render();
        $delete_action = view('asset-manager.delete-folder-action', $this->view_data )->render();
        $new_folder_modal = view('asset-manager.new-folder-modal', $this->view_data )->render();
        $upload_modal = view('asset-manager.upload-modal', $this->view_data )->render();

        $flat_dirs = [];
        $this->_flattenDirectoriesForDropdown( $flat_dirs, $this->view_data['directories'] );

        $json = ['directory_list' => $directory_list, 'files_list' => $files_list, 'delete_action' => $delete_action, 'new_folder' => $new_folder_modal, 'upload' => $upload_modal, 'files_count' => sizeof( $this->view_data['files'] ), 'flat_directories' => $flat_dirs];
        return $this->_jsonResponse( $json );
    }

    /**
     * AJAX handler for searching ALL files and directories with a POSTed search term.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Throwable
     */
    public function search(){
        $search_term = $this->request->input('search');

        $this->_loadDefaultViewData();
        $this->_loadFiles();
        $this->_loadCurrentDirectory();

        $this->view_data['modal'] = $this->request->input('modal');
        $this->view_data['multiple_select'] = $this->request->input('multiple_select') == 'true';
        $this->view_data['list_directories'] = FileDirectory::where('name', 'LIKE', '%' . $search_term . '%')->orderBy( 'name', 'ASC' )->get();
        $this->view_data['files'] = File::where('name', 'LIKE', '%' . $search_term . '%')->get();

        $files_list = view($this->request->input('modal') ? 'asset-manager.modal.files-list' : 'asset-manager.files-list', $this->view_data)->render();

        $json = ['files_list' => $files_list];
        return $this->_jsonResponse( $json );
    }

    /**
     * Load in the directory tree, tiering down the 'is-expanded' state
     *
     * @return mixed
     */
    private function _loadDirectoryTree(){
        $directories = FileDirectory::where('parent', 0)->with('children')->orderBy('name')->get();

        $this->_tierDownCurrentDirectory( $directories, $this->view_data['directory_id'] );

        return $directories;
    }

    /**
     * Recursively tier down through directories, setting whether they should be expanded in the blade or not
     *
     * @param $directories
     * @param $current_directory
     */
    private function _tierDownCurrentDirectory( &$directories, $current_directory ){
        foreach( $directories as $ind => $directory ){
            if( $directory->id == $current_directory ){ $directories[ $ind ]->expanded = true; }

            if( $directory->children ){
                $flat = [];
                $this->_flattenDirectories( $flat, $directory->children );

                if( in_array( $current_directory, $flat ) ){
                    $directories[ $ind ]->expanded = true;
                }

                $this->_tierDownCurrentDirectory( $directory->children, $current_directory );
            }
        }
    }

    /**
     * Custom flatten function for the directory tree.
     * Turns a nested collection into a flat array of directory IDs
     *
     * @param $flat
     * @param $directories
     */
    private function _flattenDirectories( &$flat, &$directories ){
        foreach( $directories as $directory ){
            $flat[] = $directory->id;

            if( $directory->children ){
                $this->_flattenDirectories( $flat, $directory->children );
            }
        }
    }

    /**
     * Custom flatten function for the 'Move Assets To Folder' dropdown.
     * Turns a nested collection into a flat array, padded to the left, of directory IDs and their names
     *
     * @param $flat
     * @param $directories
     */
    private function _flattenDirectoriesForDropdown( &$flat, &$directories ){
        foreach( $directories as $directory ){
            $flat[] = $directory;

            if( $directory->children ){
                $this->_flattenDirectoriesForDropdown( $flat, $directory->children );
            }
        }
    }

    /**
     * Upload function - called through the performAction handler
     * Upload a file to the system
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function upload(){
        $isModal = $this->request->input('modal');
        if( !$this->request->isMethod( 'post' ) ){
            $json['error'] = 'No information was received. Please try again.';
            return $this->_jsonResponse( $json );
        }

        if( !$this->request->file('asset') ){
            $json['error'] = 'No file was uploaded. Please try again.';
            return $this->_jsonResponse( $json );
        }

        // Get all the assets uploaded - the validation will be different for a single upload than for multiple uploads
        $assets = $this->request->file('asset');

        $isDragAndDrop = request()->input( 'drag_and_drop' );

        // If we only have 1 uploaded file, then the 'name' field is also required.
        if( sizeof( $assets ) == 1 && !$isDragAndDrop ) {
            $validator = Validator::make($this->request->all(), [
                'name' => 'required|max:255',
                'asset' => 'array|min:1'
            ]);
        }
        // Else, we only need to validate the file array
        else {
            $validator = Validator::make($this->request->all(), [
                'asset' => 'array|min:1'
            ]);
        }

        if( $validator->fails() ){
            $json['error'] = 'Something isn\'t right. Please check for errors.';
            $json['field_errors'] = $validator->errors();

            $json['flash_notification'] = view('includes.flash', ['error_message' => $json['error']])->render();

            return $this->_jsonResponse( $json );
        } else {
            $file_errors = array();
            foreach( $assets as $ind => $asset ){
                $uploadFilename = $asset->getClientOriginalName();
                $name = $this->_rename_uploaded_asset_filepath( $uploadFilename );

                // We should also check the uploaded file size
                if( $asset->getError() != UPLOAD_ERR_OK ){
                    $file_errors['asset[]'] = 'The file \'' . $name . '\' could not be uploaded. ' . $this->_translateUploadErrorMessage( $asset->getError() );
                }
            }

            if( !empty( $file_errors ) ){
                $json = array(
                    'error' => 'Something isn\'t right. Please check for errors.',
                    'field_errors' => $file_errors,
                    'flash_notification' => view('includes.flash', ['error_message' => 'Something isn\'t right. Please check for errors.'])->render()
                );

                return $this->_jsonResponse( $json );
            }
        }

        $uploaded_files = array();

        // Iterate over the asset[] array, storing each file and creating a DB entry for it
        foreach( $assets as $ind => $asset ) {

            $uploadPath = Storage::putFile('', $this->request->file('asset.' . $ind));

            $extension = pathinfo(storage_path('app/' . $uploadPath), PATHINFO_EXTENSION);

            // Make sure the file has an extension (just in case validation passes somehow)
            if( empty( $extension ) ){
                $extension = 'txt';
            }

            // Make sure that we use .jpg instead of .jpeg
            $extension = str_replace('jpeg', 'jpg', $extension);

            // Auto-generate the 'name' attribute, and set the default alt text as that name.
            $name = $this->_rename_uploaded_asset_filepath( $asset->getClientOriginalName() );

            // And default the alt text to the generated name.
            $alt_text = $name;

            // If we've only got 1 uploaded file, we can use the POSTed name & alt text
            if( sizeof( $assets ) == 1 && !$isDragAndDrop ){
                $name = $this->request->input('name');
                $alt_text = $this->request->input('alt_text');

                if( empty( $alt_text ) ){
                    $alt_text = $name;
                }
            }

            if( $isDragAndDrop ){
                $suffix = 0;
                $base_name = $name;
                $FileExists = File::where([
                    'directory_id'  => (int)$this->request->input('directory_id', 0),
                    'file_name'     => \Illuminate\Support\Str::slug($name) . "." . $extension,
                    'name'          => $name
                ])->first();
                while( $FileExists ){
                    $suffix++;
                    $name = $base_name . '-' . $suffix;
                    $FileExists = File::where([
                        'directory_id'  => (int)$this->request->input('directory_id', 0),
                        'file_name'     => \Illuminate\Support\Str::slug($name) . "." . $extension,
                        'name'          => $name
                    ])->first();
                }
            }

            $new_file = new File();
            $new_file->directory_id = (int)$this->request->input('directory_id', 0);
            $new_file->storage_path = $uploadPath;
            $new_file->file_name = \Illuminate\Support\Str::slug($name) . "." . $extension;
            $new_file->name = $name;
            $new_file->alt_text = $alt_text;
            $new_file->extension = $extension;
            $new_file->created_by = \Illuminate\Support\Facades\Auth::user()->id;

            $new_file->save();

            if ($new_file->is_image) {
                // Scale the image down if it's too big
                $this->_scale_down_image($uploadPath);
            }

            // Add the uploaded files to our internal array
            $uploaded_files[] = $new_file->id;

        }

        \Session::push('asset-manager.uploaded-files', $uploaded_files);

        Cache::flush();
        $this->purgeCloudflare();

        $json['success'] = 'Upload complete.';
        if( sizeof( $assets ) > 1 ){
            $json['success'] .= ' Please make sure that you assign suitable names and alt text to your uploads.';
        }
        $json['flash_notification'] = view('includes.flash', ['success_message' => $json['success']])->render();
        return $this->_jsonResponse( $json );
    }

    /**
     * Scale Image Down - called from the upload() function.
     * If an uploaded image is greater than 3000 pixels in either width or height, scale it down to a maximum
     * dimension of 3000 pixels, retaining the original aspect ratio
     *
     * @param $file_path
     */
    private function _scale_down_image( $file_path ){
        $size = getimagesize( storage_path( 'app/' . $file_path ) );

        $max_size = 3000;
        if( $size[0] > $max_size || $size[1] > $max_size ) {

            ini_set( "memory_limit", "2G" );

            $image = \Intervention\Image\Facades\Image::make(storage_path('app/' . $file_path));

            $width = $height = null;
            if( $size[0] > $size[1] ){
                $width = $size[0];
            } else {
                $height = $size[1];
            }

            if (!empty($width)) {
                $image->resize($max_size, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            } elseif (!empty($height)) {
                $image->resize(null, $max_size, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }

            $image->save( storage_path( 'app/' . $file_path ) );

        }
    }

    /**
     * Centralised function used to convert an uploaded asset's filename into something human readable.
     *
     * @param $file_path
     * @return mixed|string
     */
    private function _rename_uploaded_asset_filepath( $file_path ){
        $file_path = substr( $file_path, 0, strripos( $file_path, '.' ) );
        $file_path = preg_replace( '/[\s]/', '-', $file_path ); // Replace any white space with a dash
        $file_path = preg_replace( '/-[-]+/', '-', $file_path ); // Replace any instances of 2 hyphens with 1 (----- to -)
        $file_path = preg_replace( '/[^a-zA-Z0-9-]/', '', $file_path ); // Replace any invalid characters with nothing
        $file_path = preg_replace( '/-+$/', '', $file_path ); // Replace any trailing hyphens from the URL
        $file_path = preg_replace( '/-/', ' ', $file_path ); // Replace any dashes with a space
        $file_path = ucwords( strtolower( $file_path ) );

        return $file_path;
    }

    /**
     * Validate Upload - called through the performAction handler
     * Validates a form submission for uploading a new file, returning JSON to show in
     * the modal form
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function validate_upload(){
        if( !$this->request->isMethod( 'post' ) ){
            $json['error'] = 'No information was received. Please try again.';
            return $this->_jsonResponse( $json );
        }

        // Get all the assets uploaded - the validation will be different for a single upload than for multiple uploads
        $assets = $this->request->file('asset');

        // If we only have 1 uploaded file, then the 'name' field is also required.
        if( sizeof( $assets ) == 1 ) {
            $validator = Validator::make($this->request->all(), [
                'name' => 'required|max:255',
                'asset' => 'array|min:1'
            ]);

            $validator->after( function($validator){
                $extension = $this->request->input('file_extension');

                if( empty( $extension ) || $extension == 'undefined' ){
                    $validator->errors()->add('name', 'That file doesn\'t contain a file extension (e.g. jpg, png).' );
                }

                $file_name_exists = File::where('file_name', \Illuminate\Support\Str::slug($this->request->input('name')) . '.' . $extension)->first();

                if( $extension == 'jpg' || $extension == 'jpeg' ){
                    $input = $this->request->input();
                    $file_name_exists = File::where( function( $query ) use( $input ){
                        $query->where( 'file_name', \Illuminate\Support\Str::slug( $input['name'] ) . '.jpg' )
                            ->where( 'directory_id', $input['directory_id'] );
                    })->orWhere( function( $query ) use( $input ){
                        $query->where( 'file_name', \Illuminate\Support\Str::slug( $input['name'] ) . '.jpeg' )
                            ->where( 'directory_id', $input['directory_id'] );
                    })->first();
                }

                if( $file_name_exists ){
                    $validator->errors()->add('name', 'A file with that name already exists. Please choose something else.');
                }

                // We should also check the uploaded file size
                if( $this->request->file( 'asset.0' )->getError() != UPLOAD_ERR_OK ){
                    $validator->errors()->add('asset[]', 'The file could not be uploaded. ' . $this->_translateUploadErrorMessage( $this->request->file( 'asset.0' )->getError() ) );
                }
            });
        }
        // Else, we only need to validate the file array
        else {
            $validator = Validator::make($this->request->all(), [
                'asset' => 'array|min:1'
            ]);

            $validator->after( function($validator){
                $assets = $this->request->file('asset');
                if( !empty( $assets ) ) {

                    foreach ($assets as $ind => $asset) {
                        $uploadFilename = $asset->getClientOriginalName();
                        $extension = substr($uploadFilename, strripos($uploadFilename, '.') + 1);
                        $name = $this->_rename_uploaded_asset_filepath($uploadFilename);

                        $file_name_exists = File::where('file_name', \Illuminate\Support\Str::slug($name) . '.' . $extension)->first();

                        if ($extension == 'jpg' || $extension == 'jpeg') {
                            $file_name_exists = File::where( function( $query ) use( $name ){
                                $query->where([
                                    ['file_name', '=', \Illuminate\Support\Str::slug($name) . '.jpg'],
                                    ['directory_id', '=', request()->input('directory_id')]
                                ])->orWhere([
                                    ['file_name', '=', \Illuminate\Support\Str::slug($name) . '.jpeg'],
                                    ['directory_id', '=', request()->input('directory_id')]
                                ]);
                            })->first();
                        }

                        if ($file_name_exists) {
                            $validator->errors()->add('asset[]', 'The file \'' . $name . '\' already exists. Please choose something else.');
                        }

                        if ($asset->getError() != UPLOAD_ERR_OK) {
                            $validator->errors()->add('asset[]', 'The file \'' . $name . '\' could not be uploaded. ' . $this->_translateUploadErrorMessage($asset->getError()));
                        }
                    }

                } else {
                    // If we've got in here, no file has been uploaded. Probably due to file size
                    $validator->errors()->add( 'asset[]', 'Sorry the file couldn\'t be uploaded. ' . $this->_translateUploadErrorMessage( UPLOAD_ERR_INI_SIZE ) );
                }
            });
        }

        if( $validator->fails() ){
            $json['error'] = 'Something isn\'t right. Please check for errors.';
            $json['field_errors'] = $validator->errors();
            return $this->_jsonResponse( $json );
        }

        $json = ['success' => true];
        return $this->_jsonResponse( $json );
    }

    /**
     * Create Folder function - called through the performAction Handler
     * Create a new folder in the system
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function new_folder(){

        $json = [];

        $isModal = $this->request->input('modal');
        if( !$this->request->isMethod( 'post' ) ){
            $json = ['error' => 'No information was received. Please try again.'];
            return $this->_jsonResponse( $json );
        }

        $validator = Validator::make( $this->request->all(), [
            'name' => 'required|max:255'
        ]);

        $validator->after( function($validator){
            $directory_path_exists = FileDirectory::where('path', \Illuminate\Support\Str::slug($this->request->input('name')))->where('parent', $this->request->input('parent', 0))->first();

            if( $directory_path_exists ){
                $validator->errors()->add('name', 'That directory path already exists. Please choose something else.');
            }

            if( strlen(\Illuminate\Support\Str::slug($this->request->input('name'))) == 0 ){
                $validator->errors()->add('name', 'Sorry but that name is invalid. please provide at least 1 letter or number.');
            }
        });

        if( $isModal ){
            if( $validator->fails() ){
                $json['error'] = 'Something isn\'t right. Please check for errors.';
                $json['field_errors'] = $validator->errors();
                return $this->_jsonResponse( $json );
            }
        } else {
            $validator->validate();
        }

        $new_directory = new FileDirectory();
        $new_directory->parent = (int)$this->request->input('parent', 0);
        $new_directory->path = \Illuminate\Support\Str::slug($this->request->input('name'));
        $new_directory->name = $this->request->input('name');

        $new_directory->save();

        $json['new_directory'] = $new_directory->id;
        $json['success'] = 'Folder "' . $new_directory->name . '" created.';
        $json['flash_notification'] = view('includes.flash', ['success_message' => $json['success']])->render();

        Cache::flush();
        $this->purgeCloudflare();

        return $this->_jsonResponse( $json );
    }

    /**
     * Validate New Folder - called through the performAction Handler
     * Validates a form submission for creating a new folder, returning JSON to show in
     * the modal form
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function validate_new_folder(){
        if( !$this->request->isMethod( 'post' ) ){
            $json['error'] = 'No information was received. Please try again.';
            return $this->_jsonResponse( $json );
        }

        $validator = Validator::make( $this->request->all(), [
            'name' => 'required|max:255'
        ]);

        $validator->after( function($validator){
            $directory_path_exists = FileDirectory::where('path', \Illuminate\Support\Str::slug($this->request->input('name')))->where('parent', $this->request->input('parent', 0))->first();

            if( $directory_path_exists ){
                $validator->errors()->add('name', 'That directory path already exists. Please choose something else.');
            }

            if( strlen(\Illuminate\Support\Str::slug($this->request->input('name'))) == 0 ){
                $validator->errors()->add('name', 'Sorry but that name is invalid. please provide at least 1 letter or number.');
            }
        });

        if( $validator->fails() ){
            $json['error'] = 'Something isn\'t right. Please check for errors.';
            $json['field_errors'] = $validator->errors();
            return $this->_jsonResponse( $json );
        }

        $json = ['success' => true];
        return $this->_jsonResponse( $json );
    }

    /**
     * Rename a folder/directory
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function rename_folder(){

        $json = [];

        if( !$this->request->isMethod( 'post' ) ){
            $json['error'] = 'No information was received. Please try again.';
            return $this->_jsonResponse( $json );
        }

        if ($this->request->input('get_form')) {
            // The form hasn't been loaded yet, so just return the form for the modal to display
            $directory = FileDirectory::find($this->request->input('directory_id'));
            return view('asset-manager.modal.rename-folder-form', ['directory' => $directory]);
        }

        $directory = FileDirectory::find($this->request->input('directory_id'));
        $original_name = $directory->name;

        if ($directory->name != $this->request->input('name')) {

            // The form has been submitted, so validate and update

            $validator = Validator::make( $this->request->all(), [
                'name' => 'required|max:255'
            ]);

            $validator->after( function($validator) {
                $directory_path_exists = FileDirectory::where('path', \Illuminate\Support\Str::slug($this->request->input('name')))->where('parent', $this->request->input('parent', 0))->first();

                if( $directory_path_exists ){
                    $validator->errors()->add('name', 'That directory path already exists. Please choose something else.');
                }

                if( strlen(\Illuminate\Support\Str::slug($this->request->input('name'))) == 0 ){
                    $validator->errors()->add('name', 'Sorry but that name is invalid. please provide at least 1 letter or number.');
                }
            });

            if( $validator->fails() ){
                $json['error'] = 'Something isn\'t right. Please check for errors.';
                $json['field_errors'] = $validator->errors();
                return $this->_jsonResponse( $json );
            }

            // Update
            $directory->name = $this->request->input('name');
            $directory->path = \Illuminate\Support\Str::slug($this->request->input('name'));
            $directory->save();

            Cache::flush();
            $this->purgeCloudflare();

            $json['success'] = 'Folder successfully renamed from "' . $original_name . '" to "' . $this->request->input('name') . '".';
            $json['flash_notification'] = view('includes.flash', ['success_message' => $json['success']])->render();
            return $this->_jsonResponse( $json );

        } else {
            // The name hasn't been changed, so return and close modal without doing anything
            $json['success'] = 'Folder saved.';
            $json['flash_notification'] = view('includes.flash', ['success_message' => $json['success']])->render();
            return $this->_jsonResponse( $json );
        }
    }

    /**
     * Move a file - called through the performAction handler
     * Moves a file to the directory specified
     * Returns a JSON array containing either errors or a success message (json.error or json.success)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function move_files(){
        $json = [];
        if( !$this->request->isMethod( 'post' ) ){
            $json['error'] = 'No information was received. Please try again.';
            return $this->_jsonResponse( $json );
        }

        $inputs = $this->request->all();
        if( !isset($inputs['file_ids']) || empty($inputs['file_ids']) ){
            $json['error'] = 'No file was specified.';
            return $this->_jsonResponse( $json );
        }
        if( !isset($inputs['directory_id']) ){
            $json['error'] = 'No directory was specified. Pass 0 to move the file to \'root\'.';
            return $this->_jsonResponse( $json );
        }

        if( !isset($json['error']) ) {
            $files_changed_count = 0;

            $files_not_moved = [];
            $files_in_directory_raw = File::where('directory_id', $this->request->input('directory_id'))->get();
            $files_in_directory = [];
            foreach( $files_in_directory_raw as $directory_file ){
                $files_in_directory[] = $directory_file->file_name;
            }

            foreach( (array)$this->request->input('file_ids') as $file_id ){
                $current_file = File::where( 'id', '=', $file_id )->first();

                if( $current_file ){
                    if( in_array( $current_file->file_name, $files_in_directory ) ){
                        $files_not_moved[] = $current_file->file_name;
                    } else {
                        $current_file->directory_id = $this->request->input('directory_id');
                        $current_file->save();

                        $files_changed_count++;
                    }
                }
            }

            $json['files_not_moved'] = $files_not_moved;
            $json['files_moved'] = $files_changed_count;
            $json['success'] = $files_changed_count . ' files moved successfully.';

            if( $files_changed_count > 0 ){
                Cache::flush();
                $this->purgeCloudflare();
            }
        }

        return $this->_jsonResponse( $json );
    }

    /**
     * Move a folder - called through the performAction handler
     * Moves a folder to the parent specified (0 if Home)
     * Returns a JSON array containing either errors or a success message (json.error or json.success)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function move_folders(){
        $json = [];
        if( !$this->request->isMethod( 'post' ) ){
            $json['error'] = 'No information was received. Please try again.';
            return $this->_jsonResponse( $json );
        }

        $inputs = $this->request->all();
        if( !isset($inputs['directory_ids']) || empty($inputs['directory_ids']) ){
            $json['error'] = 'No folder was specified.';
            return $this->_jsonResponse( $json );
        }
        if( !isset($inputs['parent']) ){
            $json['error'] = 'No parent was specified. Pass 0 to move the folder to \'root\'.';
            return $this->_jsonResponse( $json );
        }

        if( !isset($json['error']) ) {
            $folders_changed_count = 0;

            $folders_not_moved = [];
            $folders_in_directory_raw = FileDirectory::where('parent', $this->request->input('parent'))->get();
            $folders_in_directory = [];
            foreach( $folders_in_directory_raw as $directory ){
                $folders_in_directory[] = $directory->path;
            }

            foreach ((array)$this->request->input('directory_ids') as $directory_id) {
                $current_folder = FileDirectory::where( 'id', '=', $directory_id )->first();

                if( $current_folder ) {
                    if( in_array( $current_folder->path, $folders_in_directory ) ){
                        $folders_not_moved[] = $current_folder->name;
                    } else {
                        $current_folder->parent = $this->request->input('parent');
                        $current_folder->save();

                        $folders_changed_count++;
                    }
                }
            }

            $json['folders_not_moved'] = $folders_not_moved;
            $json['folders_moved'] = $folders_changed_count;
            $json['success'] = $folders_changed_count . ' folders moved successfully.';

            if( $folders_changed_count > 0 ){
                Cache::flush();
                $this->purgeCloudflare();
            }
        }

        return $this->_jsonResponse( $json );
    }

    /**
     * Delete Files - called through the performAction handler
     * Deletes a set of files specified, or generates a modal asking whether files should be deleted or not.
     * Returns a JSON array containing either errors or a success message (json.error or json.success)
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     * @throws \Exception
     * @throws \Throwable
     */
    public function delete_files(){

        $json = [];

        if( !$this->request->isMethod( 'post' ) ){
            $json = ['error' => 'No information was received. Please try again.'];
            return $this->_jsonResponse( $json );
        }

        $inputs = $this->request->all();

        if ( isset($inputs['get_form']) ) {
            // The form hasn't been loaded yet, so just return the form for the modal to display
            $files = File::whereIn('id', $inputs['files'])->get();
            return view('asset-manager.delete-file-form', ['file_ids' => $inputs['files'], 'files' => $files]);
        }

        if( !isset($inputs['files']) ){
            $json = ['error' => 'No files were received. Please try again.'];
            return $this->_jsonResponse( $json );
        }

        $files_in_use = [];
        $files_to_delete = [];
        foreach( (array)$inputs['files'] as $file_id ){
            $current_file = File::where( 'id', '=', $file_id )->first();

            if( !$current_file ){
                $json = ['error' => 'Sorry, we couldn\'t find "' . $current_file->file_name . '". Please try again.'];
                return $this->_jsonResponse( $json );
            }

            if ( !empty($current_file->usages ) ) {
                $files_in_use[] = $current_file->file_name;
            }

            $files_to_delete[] = $current_file;
        }

        if( isset($inputs['check_file_usage']) && !empty($files_in_use) ) {
            $json['files_in_use'] = $files_in_use;
            return $this->_jsonResponse( $json );
        } else {
            foreach($files_to_delete as $file) {

                // Register who deleted the file
                $file->deleted_by = \Auth::user()->id;
                $file->save();

                $file->delete();
            }
        }

        $json['success'] = 'File(s) deleted.';
        $json['flash_notification'] = view('includes.flash', ['success_message' => $json['success']])->render();

        Cache::flush();
        $this->purgeCloudflare();

        return $this->_jsonResponse( $json );
    }

    /**
     * Delete a folder - Called through the performAction handler
     * Deletes a folder
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete_folder(){

        $json = [];

        if( !$this->request->isMethod( 'post' ) ){
            $json['error'] = 'No information was received. Please try again.';
            return $this->_jsonResponse( $json );
        }

        if ($this->request->input('get_form')) {
            // The form hasn't been loaded yet, so just return the form for the modal to display
            $directory = FileDirectory::find($this->request->input('directory_id'));
            return view('asset-manager.delete-folder-form', ['directory' => $directory]);
        }

        $inputs = $this->request->all();
        if( !isset($inputs['directory_id'] ) ){
            $json['error'] = 'No directory was specified. Please try again.';
            return $this->_jsonResponse( $json );
        }

        $current_folder = FileDirectory::where( 'id', '=', $this->request->input('directory_id') )->first();

        if( !$current_folder ) {
            $json['error'] = 'Sorry, we couldn\'t find the folder requested. Please try again.';
            return $this->_jsonResponse( $json );
        }

        $files_in_use = [];
        if( isset($inputs['check_file_usage']) ) {
            // Perform a usage check on all files in this folder to warn the user of possibly breaking images/files
            $current_folder->get_folder_usages($files_in_use);
        }

        if ( empty($files_in_use) ) {

            // Register who deleted the folder
            $current_folder->deleted_by = \Auth::user()->id;
            $current_folder->save();

            $current_folder->delete_folder();

            $parent_folder = $current_folder->parent;
            \Session::put('directory_id', $parent_folder);

            Cache::flush();
            $this->purgeCloudflare();

            $json['success'] = 'The Folder "' . $current_folder->name . '" has been deleted.';
            $json['parent'] = $parent_folder;
            $json['flash_notification'] = view('includes.flash', ['success_message' => $json['success']])->render();
            return $this->_jsonResponse( $json );

        } else {
            $json['files_in_use'] = $files_in_use;
            return $this->_jsonResponse( $json );
        }
    }

    /**
     * Edit a file - Called through the performAction handler
     * Updates a file's name (and therefore path)
     * Returns a JSON array containing either errors or a success message (json.error or json.success)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit_file(){
        if( !$this->request->isMethod( 'post' ) ){
            $json['error'] = 'No information was received. Please try again.';
            $json['flash_notification'] = view('includes.flash', ['error_message' => $json['error']])->render();
            return $this->_jsonResponse( $json );
        }

        $validator = Validator::make( $this->request->all(), [
            'file_id' => 'required',
            'name' => 'required|max:255'
        ]);

        $validator->after( function($validator){
            $extension = $this->request->input('file_extension');
            $file_name_exists = File::where('file_name', \Illuminate\Support\Str::slug($this->request->input('file_name')) . '.' . $extension)->first();
            if( $extension == 'jpg' ){
                $file_name_exists = File::where('file_name', \Illuminate\Support\Str::slug($this->request->input('file_name')) . '.jpg')->orWhere('file_name', \Illuminate\Support\Str::slug($this->request->input('file_name')) . '.jpeg')->first();
            }

            if( $file_name_exists ){
                $validator->errors()->add('file_name', 'That file name already exists. Please choose something else.');
            }
        });

        if( $validator->fails() ){
            $json['error'] = 'Something isn\'t right. Please check for errors.';
            $json['field_errors'] = $validator->errors();
            $json['flash_notification'] = view('includes.flash', ['error_message' => $json['error']])->render();
            return $this->_jsonResponse( $json );
        }

        $current_file = File::where( 'id', '=', $this->request->input('file_id') )->first();

        if( !$current_file ){
            $json['error'] = 'Sorry, we couldn\'t find the file requested. Please try again.';
            $json['flash_notification'] = view('includes.flash', ['error_message' => $json['error']])->render();
            return $this->_jsonResponse( $json );
        }

        $current_file->name = $this->request->input('name');
        $current_file->alt_text = $this->request->input('alt_text');
        $current_file->file_name = \Illuminate\Support\Str::slug($this->request->input('name')) . '.' . $current_file->extension;
        $current_file->save();

        Cache::flush();
        $this->purgeCloudflare();

        // Set the directory_id session value ready for the JS redirect...
        session(['directory_id' => $this->request->input('directory_id', 0)]);
        $json = array(
            'success' => 'File saved.',
            'flash_notification' => view('includes.flash', ['success_message' => $current_file->name . ' Saved Successfully.'])->render(),
            'name' => $current_file->name,
            'path' => route( 'image', $current_file->path ),
            'redirect' => route('adm.asset-manager')
        );

        return $this->_jsonResponse( $json );
    }

    /**
     * Select a file - Called through the performAction handler
     * Called when an asset is selected through the modal asset manager
     * Generates an asset preview to pass down to the template for cropping, etc.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Throwable
     */
    public function select_file(){
        if( !$this->request->isMethod( 'post' ) ){
            $json['error'] = 'No information was received. Please try again.';
            return $this->_jsonResponse( $json );
        }

        $current_file = File::where( 'id', '=', $this->request->input('file_id') )->first();

        if( !$current_file ){
            $json['error'] = 'Sorry, we couldn\'t find the file requested. Please try again.';
            return $this->_jsonResponse( $json );
        }

        if( $this->request->input('table') && $this->request->input('item_id') && $this->request->input('language') ){
            $column = $this->request->input('column');
            $asset_group = \App\Models\AssetGroup::where([
                'table_name' => $this->request->input('table'),
                'table_key' => $this->request->input('item_id'),
                'language_id' => $this->request->input('language'),
                'field' => $column['field'],
                'image_size' => $this->request->input('reference')
            ])->first;

            $current_file->asset_group = $asset_group;
        }

        $partial_data = [
            'reference' => $this->request->input('reference'),
            'details' => $this->request->input('details'),
            'column' => $this->request->input('column'),
            'field_id' => $this->request->input('field_id'),
            'dimensions' => $this->request->input('dimensions'),
            'table_name' => $this->request->input('table'),
            'item_id' => $this->request->input('item_id'),
            'current_language' => $this->request->input('language')
        ];
        $partial_data['current_asset'] = $current_file;

        $partial_data['details']['primary'] = isset($partial_data['details']['primary']) && $partial_data['details']['primary'] != 'false';

        $html = view( 'fields.form.partials.asset.group-asset', $partial_data);
        $json = array(
            'success' => true,
            'output' => $html->render()
        );

        return $this->_jsonResponse( $json );
    }

    /**
     * Reset a file - Called through the performAction handler
     * Called when an asset is reset after being selected.
     * Generates a placeholder state for the asset selector.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Throwable
     */
    public function reset_file(){
        if( !$this->request->isMethod( 'post' ) ){
            $json['error'] = 'No information was received. Please try again.';
            return $this->_jsonResponse( $json );
        }

        $partial_data = [
            'reference' => $this->request->input('reference'),
            'details' => $this->request->input('details'),
            'column' => $this->request->input('column'),
            'field_id' => $this->request->input('field_id'),
            'dimensions' => $this->request->input('dimensions'),
            'table_name' => $this->request->input('table'),
            'item_id' => $this->request->input('item_id'),
            'current_language' => $this->request->input('language')
        ];

        $partial_data['details']['primary'] = isset($partial_data['details']['primary']) && $partial_data['details']['primary'] != 'false';

        $html = view( 'fields.form.partials.asset.group-asset-empty', $partial_data);
        $json = array(
            'success' => true,
            'output' => $html->render()
        );

        return $this->_jsonResponse( $json );
    }

    public function replace_file(){
        if( !$this->request->isMethod( 'post' ) ){
            $json['error'] = 'No information was received. Please try again.';
            return $this->_jsonResponse( $json );
        }

        if( !$this->request->file('asset') ){
            $json['error'] = 'No file was uploaded. Please try again.';
            return $this->_jsonResponse( $json );
        }

        // Get all the assets uploaded - the validation will be different for a single upload than for multiple uploads
        $assets = $this->request->file('asset');

        $validator = Validator::make($this->request->all(), [
            'file_id'   => 'required',
            'asset'     => 'array|min:1'
        ]);

        if( $validator->fails() ){
            $json['error'] = 'Something isn\'t right. Please check for errors.';
            $json['field_errors'] = $validator->errors();

            $json['flash_notification'] = view('includes.flash', ['error_message' => $json['error']])->render();

            return $this->_jsonResponse( $json );
        }

        $File = File::find( request()->input( 'file_id' ) );
        if( !$File ){
            $json['error'] = 'We couldn\'t find that file. Please try again.';
            return $this->_jsonResponse( $json );
        }

        foreach( $assets as $ind => $asset ){
            // Get the file's original storage path to allow us to clear the cache
            $originalStoragePath = $File->storage_path;

            $uploadPath = Storage::putFile('', $this->request->file('asset.' . $ind));

            $extension = pathinfo(storage_path('app/' . $uploadPath), PATHINFO_EXTENSION);

            // Make sure that we use .jpg instead of .jpeg
            $extension = str_replace('jpeg', 'jpg', $extension);

            // If the new extension is different, we'll need to rename the file
            if( $File->extension != $extension ){
                $File->file_name = str_ireplace( '.' . $File->extension, '.' . $extension, $File->file_name );
            }

            $File->storage_path = $uploadPath;
            $File->extension = $extension;
            $File->save();

            if( $File->is_image ){
                // Scale the image down if it's too big
                $this->_scale_down_image( $uploadPath );
            }

            Cache::flush();
            $this->purgeCloudflare();

            // Clear cache files
            $cache_dir = storage_path( 'app/cache/' );
            $cached_files = glob( $cache_dir . '*' . $originalStoragePath );
            if( !empty( $cached_files ) ){
                foreach( $cached_files as $cached_file ){
                    unlink( $cached_file );
                }
            }
        }

        $json['success'] = 'Upload complete.';
        $json['flash_notification'] = view('includes.flash', ['success_message' => $json['success']])->render();
        $json['directory'] = $File->directory_id;

        return $this->_jsonResponse( $json );
    }

    /**
     * Load in the default view data used by the base template,
     * including the logged in user, breadcrumbs & navigation
     */
    private function _loadDefaultViewData(){
        $this->view_data['me'] = \Illuminate\Support\Facades\Auth::user();

        $breadcrumbs = array();

        // Add the main dashboard link
        $breadcrumbs[] = array(
            'label' => 'Dashboard',
            'url' => route('adm.path')
        );

        $breadcrumbs[] = array(
            'label' => 'Asset Manager',
            'url' => route('adm.asset-manager')
        );

        $this->view_data['breadcrumbs'] = $breadcrumbs;

        $admController = new \Adm\Controllers\AdmController();
        $this->view_data['navigation'] = $admController->generateNavigation();

        $this->view_data['dimensions'] = '123x123';
        if( $this->request->input('dimensions') ){
            $this->view_data['dimensions'] = $this->request->input('dimensions');
        }

        $this->view_data['table'] = 'asset-manager';

        $my_bespoke_api = new MyBespokeApi;
        $this->view_data['relationship_manager'] = $my_bespoke_api->getRelationshipManager();
    }

    /**
     * Load details for the currently browsed directory into the view data
     */
    private function _loadCurrentDirectory(){
        $directory_id = 0;
        if( session('directory_id') ) { $directory_id = session('directory_id'); }
        if( $this->request->input('directory_id') ){ $directory_id = $this->request->input('directory_id'); }
        $this->view_data['directory_id'] = $directory_id;

        if( $this->view_data['directory_id'] > 0 ){
            $this->view_data['current_directory'] = FileDirectory::find( $this->view_data['directory_id'] );
        }
    }

    /**
     * Load in the file listing, either from the 'root' directory, or the specified directory
     */
    private function _loadFiles(){
        $directory_id = 0;
        if( session('directory_id') ) { $directory_id = session('directory_id'); }
        if( $this->request->input('directory_id') ){ $directory_id = $this->request->input('directory_id'); }

        if( $directory_id == 0 ){
            $files_query = File::rootFiles();
            $directory_query = FileDirectory::where('parent', 0);
        } else {
            $files_query = File::where('directory_id', $directory_id);
            $directory_query = FileDirectory::where('parent', $directory_id);
        }

        if( $this->request->input( 'images_only' ) && $this->request->input( 'images_only' ) != 'false' ){
            $files_query->imagesOnly();
        }

        // Custom functions for sorting and pass the details down to templates
        $this->view_data['sort_field'] = false;
        $this->view_data['sort_direction'] = false;
        if( $this->request->input('sort_by') && $this->request->input('sort_direction') ){
            $this->view_data['sort_field'] = $this->request->input('sort_by');
            $this->view_data['sort_direction'] = $this->request->input('sort_direction');

            $files_query->orderBy( $this->request->input('sort_by'), $this->request->input('sort_direction') );
        }

        $files = $files_query->with('usage')->get();

        if( $this->request->input('sort_by') && $this->request->input('sort_by') != 'extension' && $this->request->input('sort_direction') ) {
            $directories = $directory_query->orderBy( $this->request->input('sort_by'), $this->request->input('sort_direction') )->get();
        } else {
            $directories = $directory_query->orderBy( 'name', 'ASC' )->get();
        }

        $this->view_data['list_directories'] = $directories;
        $this->view_data['files'] = $files;

        $recently_uploaded = \Session::get('asset-manager.uploaded-files');
        $this->view_data['uploaded_files'] = is_array( $recently_uploaded ) ? reset($recently_uploaded) : array();

        \Session::forget('asset-manager.uploaded-files');
    }

    /**
     * Centralised function 'translating' a PHP upload error message into something human readable.
     *
     * @param $error_code
     * @return string
     */
    private function _translateUploadErrorMessage( $error_code ){
        switch( $error_code ){
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                return "The file is too large, please try compressing it (https://tinypng.com).";
                break;
            case UPLOAD_ERR_PARTIAL:
                return "Only part of the file was received, please try again.";
                break;
            case UPLOAD_ERR_NO_FILE:
                return "No file was uploaded.";
                break;
            case UPLOAD_ERR_NO_TMP_DIR:
            case UPLOAD_ERR_CANT_WRITE:
            case UPLOAD_ERR_EXTENSION:
                return "An internal error has occurred.";
                break;
            default:
                return "An unknown error occurred.";
                break;
        }
    }

    /**
     * Centralised function for returning JSON.
     * Useful for IE9 browsers that don't know what to do with the Content-Type: application/json header.
     *
     * @param $json
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    private function _jsonResponse( $json ){
        if( config( 'app.debug' ) ){
            \Debugbar::disable();
        }

        if( $this->request->input( 'is_ie9' ) ){
            return response( json_encode( $json ) );
        }

        return response()->json( $json );
    }

    /**
     * Purge Cloudflare
     * Centralised function used to purge Cloudflare cache within the CMS
     * We're just clearing ALL cache when saving due to the interconnectivity
     * of everything
     */
    public function purgeCloudflare(){
        $Cloudflare = new Cloudflare();
        $Cloudflare->purge_site();
    }
}