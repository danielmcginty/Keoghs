<?php

namespace Adm\Controllers;

use Adm\Crud\LaraCrud;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/**
 * Class Controller
 * Base Controller for the admin system.
 * Sets the default user guard to 'adm' so that the system uses the 'adm_users' table rather than 'users' when
 * performing authentication
 *
 * @package Adm\Controllers
 */
class Controller extends BaseController {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Keep a reference to the LaraCrud instance currently in use.
     *
     * @var LaraCrud
     */
    protected $crud;

    /**
     * Constructor
     * Ensure that database caching is OFF in the CMS
     * Register the Adm Exception Handler
     * Assign the /resources/adm/views/ directory as the basis for blades in the CMS
     */
    public function __construct(){
        // Disable the database cache for the CMS
        config(['database.cache' => false]);

        // And bind our custom error handler
        app()->singleton( \Illuminate\Contracts\Debug\ExceptionHandler::class, \Adm\Exceptions\Handler::class );

        // Set the Adm system to load all views from resources/adm/views instead of resources/views/adm
        // Get the current 'view finder' to use after we've reset this to include previously namespaced view paths
        $currentViewFinder = \View::getFinder();

        // Now, register the laravel 'view finder' to read from our theme directory
        $themeFinder = new \Illuminate\View\FileViewFinder(app()['files'], array(realpath(base_path('resources/adm/views'))));
        \View::setFinder($themeFinder);

        // And after that, re-register the previous namespaced view paths from the previous view finder
        foreach( $currentViewFinder->getHints() as $namespace => $paths ){
            view()->addNamespace( $namespace, $paths );
        }
    }

    /**
     * Set the LaraCrud instance against the controller.
     *
     * @param LaraCrud $crud
     */
    protected function setCrudInstance( LaraCrud $crud ){
        $this->crud = $crud;
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard(){
        return Auth::guard('adm');
    }
}
