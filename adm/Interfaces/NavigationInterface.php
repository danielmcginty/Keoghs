<?php
namespace Adm\Interfaces;

/**
 * Interface NavigationInterface
 * @package Adm\Interfaces
 *
 * Interface designed to ensure that all Navigation classes within modules are of the correct format, and can be used
 * by the CMS
 */
interface NavigationInterface {
    /**
     * Function to override that lays out the CMS Navigation links for the module.
     *
     * @return array
     */
    public function getNavItems();
}