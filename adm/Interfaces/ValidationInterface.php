<?php
namespace Adm\Interfaces;

interface ValidationInterface {

    /**
     * Custom Validation Rules can be placed in here to make them globally accessible to the CRUD
     *
     * @param \Adm\Crud\LaraCrud $crud
     */
    public function applyCustomValidationRules( $crud );

    /**
     * If we've created any custom validation rules, we can set custom error messages for those rules in here
     */
    public function applyCustomValidationMessages();

}