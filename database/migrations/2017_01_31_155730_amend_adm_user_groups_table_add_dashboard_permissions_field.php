<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AmendAdmUserGroupsTableAddDashboardPermissionsField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adm_user_groups', function (Blueprint $table) {
            $table->text('dashboard_permissions')->after('permissions')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adm_user_groups', function (Blueprint $table) {
            $table->dropColumn('dashboard_permissions');
        });
    }
}
