<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AmendCoreTablesForContentVersioning extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'asset_groups', function( Blueprint $table ){
            $table->integer( 'version' )->after( 'language_id' )->default( 1 );
            $table->dropIndex( 'table_asset_size' );
            $table->unique( ['table_name', 'table_key', 'language_id', 'version', 'field', 'image_size'], 'table_asset_size' );
        });

        Schema::table( 'page_builder_blocks', function( Blueprint $table ){
            $table->integer( 'version' )->after( 'language_id' )->default( 1 );
            $table->dropIndex( 'table_id_language_order' );
            $table->index( ['table_name', 'table_key', 'language_id', 'version', 'sort_order'], 'table_id_language_version_order' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'asset_groups', function( Blueprint $table ){
            $table->dropColumn( 'version' );
            $table->dropIndex( 'table_asset_size' );
            $table->unique( ['table_name', 'table_key', 'language_id', 'field', 'image_size'], 'table_asset_size' );
        });

        Schema::table( 'page_builder_blocks', function( Blueprint $table ){
            $table->dropColumn( 'version' );
            $table->dropIndex( 'table_id_language_version_order' );
            $table->index( ['table_name', 'table_key', 'language_id', 'sort_order'], 'table_id_language_order' );
        });
    }
}
