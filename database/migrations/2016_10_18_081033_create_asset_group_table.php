<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_groups', function(Blueprint $table){
            $table->increments('id');
            $table->string('table_name', 100);
            $table->integer('table_key');
            $table->integer('language_id');
            $table->string('field', 100);
            $table->string('image_size', 100);
            $table->integer('file_id');
            $table->string('alt');
            $table->integer('width')->nullable();
            $table->integer('height')->nullable();
            $table->integer('x_coordinate')->nullable();
            $table->integer('y_coordinate')->nullable();
            $table->integer('cropbox_left')->nullable();
            $table->integer('cropbox_top')->nullable();
            $table->unique(['table_name', 'table_key', 'language_id', 'field', 'image_size'], 'table_asset_size');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('asset_groups');
    }
}
