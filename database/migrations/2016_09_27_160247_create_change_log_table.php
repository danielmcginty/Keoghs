<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChangeLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'change_log', function( Blueprint $table ){
            $table->increments('id');
            $table->string('table_name');
            $table->integer('table_key');
            $table->integer('language_id');
            $table->string('action');
            $table->integer('adm_user_id');
            $table->timestamp('changed')->default(\DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('change_log');
    }
}
