<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUrlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('urls', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->string('table_name');
            $table->string('table_key');
            $table->integer('language_id')->default(1);
            $table->string('path_prefix')->nullable();
            $table->tinyInteger('manual')->default(0);
            $table->tinyInteger('no_index_meta')->default(0);
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->tinyInteger('system_page')->default(0);
            $table->string('route')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index('url');
            $table->index('language_id');
            $table->index('table_name');
            $table->index('table_key');
            $table->index('route');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('urls');
    }
}
