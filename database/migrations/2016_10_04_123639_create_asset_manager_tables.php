<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetManagerTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function(Blueprint $table){
            $table->increments('id');
            $table->integer('directory_id')->nullable()->default(0);
            $table->string('storage_path');
            $table->string('file_name');
            $table->string('name');
            $table->string('extension');
            $table->integer('created_by');
            $table->timestamps();
            $table->softDeletes();
            $table->index('directory_id');
            $table->index('file_name');
        });

        Schema::create('file_directories', function(Blueprint $table){
            $table->increments('id');
            $table->integer('parent')->nullable()->default(0);
            $table->string('path');
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();
            $table->index('parent');
            $table->index('path');
        });

        Schema::create('file_usage', function(Blueprint $table){
            $table->increments('id');
            $table->integer('file_id');
            $table->string('table_name');
            $table->string('table_key');
            $table->index('table_name');
            $table->index('table_key');
            $table->index('file_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('files');
        Schema::drop('file_directories');
        Schema::drop('file_usage');
    }
}
