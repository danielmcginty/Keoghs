<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageBuilderBlocksTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_builder_block_types', function( Blueprint $table ){
            $table->increments( 'id' );
            $table->string( 'reference' )->unique();
            $table->boolean('editable')->default(1);
            $table->boolean('deletable')->default(1);
            $table->index('reference');
        });

        Schema::create('page_builder_blocks', function( Blueprint $table ){
            $table->increments( 'id' );
            $table->string( 'reference' );
            $table->string( 'table_name' );
            $table->integer( 'table_key' );
            $table->integer( 'language_id' );
            $table->integer( 'sort_order' );
            $table->boolean( 'status' );
            $table->timestamps();
            $table->softDeletes();
            $table->index(['table_name', 'table_key', 'language_id', 'sort_order'], 'table_id_language_order');
        });

        Schema::create('page_builder_block_content', function( Blueprint $table ){
            $table->increments('id');
            $table->integer('page_builder_block_id');
            $table->string('field');
            $table->text('value');
            $table->index(['page_builder_block_id', 'field']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'page_builder_block_types' );
        Schema::dropIfExists( 'page_builder_blocks' );
        Schema::dropIfExists( 'page_builder_block_content' );
    }
}
