<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AmendFilesAndFoldersAddDeletedBy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'files', function( Blueprint $table ){
            $table->integer( 'deleted_by' )->after( 'deleted_at' )->nullable();
        });

        Schema::table( 'file_directories', function( Blueprint $table ){
            $table->integer( 'deleted_by' )->after( 'deleted_at' )->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'files', function( Blueprint $table ){
            $table->dropColumn( 'deleted_by' );
        });

        Schema::table( 'file_directories', function( Blueprint $table ){
            $table->dropColumn( 'deleted_by' );
        });
    }
}
