<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create301RedirectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('301_redirects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('old_url');
            $table->string('new_url');
            $table->timestamps();
            $table->softDeletes();
            $table->unique( ['old_url', 'deleted_at'] );
            $table->index('old_url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('301_redirects');
    }
}
