<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

/**
 * Class PageBuilderBlockTypeSeeder
 * Seeder to create the default Page Builder Block Types.
 * Used on install.
 */
class PageBuilderBlockTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            'main_content' => [
                'editable' => 1,
                'deletable' => 0
            ]
        ];

        foreach( $types as $reference => $details ){
            \DB::table('page_builder_block_types')->insert([
                'reference' => $reference,
                'editable' => $details['editable'],
                'deletable' => $details['deletable']
            ]);
        }
    }
}
