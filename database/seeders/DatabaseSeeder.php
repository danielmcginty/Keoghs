<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(UserTableSeeder::class);

        \Adm\Models\AdmUserGroup::create(['name' => 'Super Administrator', 'admin' => 1, 'super_admin' => 1]);
        \Adm\Models\AdmUserGroup::create(['name' => 'Site Administrator', 'admin' => 1]);
        \Adm\Models\AdmUser::create(['first_name' => 'Bespoke', 'last_name' => 'Digital', 'username' => 'cms-2016', 'email' => 'support@bespokedigital.agency', "password" => bcrypt("!Staff0nly!"), "user_group_id" => 1]);
        \Adm\Models\AdmUser::create(['first_name' => 'Bespoke', 'last_name' => 'Digital', 'username' => 'bespoke', 'email' => 'developer@bespokedigital.agency', 'password' => bcrypt("!Staff0nly!"), 'user_group_id' => 2]);

        \DB::table('languages')->insert([
            'title' => 'English',
            'locale' => 'en',
            'url' => 'http://cms-2016.dev'
        ]);

        \DB::table('defaults')->insert([
            'website_title' => 'Website Name',
            'default_language' => 1
        ]);

        Model::reguard();

        // Iterate over the current directory, calling any available seeds
        $default_dir = dirname( __FILE__ );
        $default_seeders = array_filter( glob($default_dir.'/*') );

        foreach( $default_seeders as $seeder_path ) {
            // We don't want the change directory directories...
            if( in_array( $seeder_path, array( '.', '..' ) ) ){ continue; }

            $path_parts = explode('/', $seeder_path);
            $seeder = str_replace( '.php', '', end($path_parts) );

            // Nor do we want ourselves
            if( $seeder == 'DatabaseSeeder'){ continue; }

            $seeder_class = '\\Database\\Seeders\\' . $seeder;

            $this->call( $seeder_class );
        }

        // Now, iterate over the modules, calling any available seeders
        $modules_dir = base_path( 'modules' );
        $modules = array_filter( glob($modules_dir.'/*', GLOB_ONLYDIR) );

        foreach( $modules as $module_path ) {
            if( is_dir( $module_path . '/Database' ) && is_dir( $module_path . '/Database/Seeders' ) ){
                $seed_dir = $module_path . '/Database/Seeders';

                $path_parts = explode('/', $module_path);
                $module_ns = end($path_parts);

                $module_seeders = array_filter( glob($seed_dir.'/*') );

                foreach( $module_seeders as $seeder_path ) {
                    // We don't want the change directory directories...
                    if( in_array( $seeder_path, array( '.', '..' ) ) ){ continue; }

                    $path_parts = explode('/', $seeder_path);
                    $seeder = str_replace( '.php', '', end($path_parts) );

                    $seeder_class = '\\Modules\\' . $module_ns . '\\Database\\Seeders\\' . $seeder;

                    $this->call( $seeder_class );
                }
            }
        }
    }
}
