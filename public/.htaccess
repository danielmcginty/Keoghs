<IfModule mod_rewrite.c>

    <IfModule mod_negotiation.c>
        Options -MultiViews
    </IfModule>

    RewriteEngine On

    # Force non-www.
    RewriteCond %{HTTP_HOST} !^keoghs-2021.bespoke-demo.co.uk$
    RewriteCond %{HTTP_HOST} !^keoghs-co-uk.laragon$
    RewriteCond %{HTTP_HOST} ^www\. [NC]
    RewriteRule ^(.*)$  https://keoghs.co.uk%{REQUEST_URI} [R=301,L]

    # Force HTTPS
    RewriteCond %{HTTP_HOST} !^keoghs-co-uk.laragon$
    RewriteCond %{HTTP_HOST} !^keoghs.localhost$
    RewriteCond %{HTTPS} off
    RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]

    # Redirect Trailing Slashes If Not A Folder...
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_URI} !^/api
    RewriteRule ^(.*)/$ /$1 [L,R=301]

    # Handle Front Controller...
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^ index.php [L]

    # Handle Authorization Header
    RewriteCond %{HTTP:Authorization} .
    RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]

</IfModule>


#Pagespeed
<IfModule pagespeed_module>

    # Disable Pagespeed
    ModPagespeed off
    #ModPagespeed on

    ModPagespeedRewriteLevel CoreFilters

    # CSS optimisation
    ModPagespeedEnableFilters rewrite_css,prioritize_critical_css

    # JS Max Conbined Size
    ModPagespeedMaxCombinedJsBytes 368640

    # JS optimisation
    ModPagespeedEnableFilters combine_javascript,rewrite_javascript
    #ModPagespeedEnableFilters make_google_analytics_async

    # Other optimisations
    ModPagespeedEnableFilters rewrite_images,collapse_whitespace

    # Disable for specific pages (e.g. admin, sitemap)
    ModPagespeedDisallow */adm*
    ModPagespeedDisallow */*.xml
    ModPagespeedDisallow *assets/favicons/*

</IfModule>

AddType image/svg+xml .svg .svgz

<ifModule mod_gzip.c>
mod_gzip_on Yes
mod_gzip_dechunk Yes
mod_gzip_item_include file .(html?|txt|css|js|php|pl)$
mod_gzip_item_include handler ^cgi-script$
mod_gzip_item_include mime ^text/.*
mod_gzip_item_include mime ^application/x-javascript.*
mod_gzip_item_exclude mime ^image/.*
mod_gzip_item_exclude rspheader ^Content-Encoding:.*gzip.*
</ifModule>

<ifModule mod_filter.c>
AddOutputFilterByType DEFLATE "application/atom+xml" \
                                 "application/javascript" \
                                 "application/json" \
                                 "application/ld+json" \
                                 "application/manifest+json" \
                                 "application/rdf+xml" \
                                 "application/rss+xml" \
                                 "application/schema+json" \
                                 "application/vnd.geo+json" \
                                 "application/vnd.ms-fontobject" \
                                 "application/x-font-ttf" \
                                 "application/x-javascript" \
                                 "application/x-web-app-manifest+json" \
                                 "application/xhtml+xml" \
                                 "application/xml" \
                                 "font/eot" \
                                 "font/opentype" \
                                 "image/bmp" \
                                 "image/svg+xml" \
                                 "image/vnd.microsoft.icon" \
                                 "image/x-icon" \
                                 "text/cache-manifest" \
                                 "text/css" \
                                 "text/html" \
                                 "text/javascript" \
                                 "text/plain" \
                                 "text/vcard" \
                                 "text/vnd.rim.location.xloc" \
                                 "text/vtt" \
                                 "text/x-component" \
                                 "text/x-cross-domain-policy" \
                                 "text/xml"
</ifModule>

#file compression
<IfModule mod_deflate.c>
   <filesMatch "\.(js|css|svg|png|jpg)$">
       SetOutputFilter DEFLATE
   </filesMatch>
   <ifModule mod_filter.c>
   AddOutputFilterByType DEFLATE text/css text/html text/javascript text/plain
   </ifModule>
</IfModule>

# Expire headers
<ifModule mod_expires.c>
 ExpiresActive On
 ExpiresDefault "access plus 1 seconds"
 ExpiresByType image/x-icon "access plus 2592000 seconds"
 ExpiresByType image/jpeg "access plus 2592000 seconds"
 ExpiresByType image/png "access plus 2592000 seconds"
 ExpiresByType image/gif "access plus 2592000 seconds"
 ExpiresByType image/svg+xml "access plus 2592000 seconds"
 ExpiresByType application/x-shockwave-flash "access plus 2592000 seconds"
 ExpiresByType text/css "access plus 2592000 seconds"
 ExpiresByType text/javascript "access plus 216000 seconds"
 ExpiresByType application/javascript "access plus 216000 seconds"
 ExpiresByType application/x-javascript "access plus 216000 seconds"
 ExpiresByType application/xhtml+xml "access plus 600 seconds"
 ExpiresByType font/woff2 "access plus 2592000 seconds"
</ifModule>

# Cache-Control Headers
<ifModule mod_headers.c>

    #cross origin font files
    <FilesMatch "\.(ttf|otf|woff|woff2)$">
        Header set Access-Control-Allow-Origin "*"
        Header set Cache-Control "max-age=43200, public"
    </FilesMatch>

    #month
     <filesMatch "\.(ico|jpe?g|png|gif|swf|svg)$">
       Header set Cache-Control "max-age=2592000, public"
     </filesMatch>

    #week
     <filesMatch "\.(css|js)$">
       Header set Cache-Control "max-age=2592000, public"
     </filesMatch>

    #day
     <filesMatch "\.(x?html?)$">
       Header set Cache-Control "max-age=43200, private, must-revalidate"
     </filesMatch>

</ifModule>
# END Cache-Control Headers

# Turn ETags Off
<ifModule mod_headers.c>
 Header unset ETag
</ifModule>
FileETag None

# Remove Last-Modified Header
<ifModule mod_headers.c>
 Header unset Last-Modified
</ifModule>