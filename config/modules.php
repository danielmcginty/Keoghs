<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Available Modules
    |--------------------------------------------------------------------------
    |
    | This area of the config file contains the modules that are available or
    | active within the CMS
    |
    */

	'available' => [
		'Awards',
		'Banners',
		'Cards',
//		'CaseStudies',
		'ContentWithImageBlock',
		'Expertise',
//		'Faqs',
		'FormBuilder',
		'HomePageSliderBlock',
		'Insights',
//      'InteractiveMapBlock',
        'Journeys',
        'Locations',
//        'Logos',
		'MenuBuilder',
//		'News',
		'Offices',
		'OurPeople',
		'Page',
		'PageBuilderWizard',
//      'PopupVideoBlock',
		'QuoteBlock',
        'RowWidthImageBlock',
        'ScreenWidthImageBlock',
		'Search',
		'Tags',
//		'TeamMembers',
//		'Testimonials',
		'TextTranslation',
		'ValuesBlock',
//		'VideoUploadBlock',
	],

    /*
    |--------------------------------------------------------------------------
    | Locked Modules
    |--------------------------------------------------------------------------
    |
    | This area of the config file contains the modules that are currently
    | 'frozen', or not accessible editable to the user
    |
    */

    'locked'        => [
        /*
        'CaseStudies',
        'Page'
        */
    ],

    /*
    |--------------------------------------------------------------------------
    | Locked Modules by ID
    |--------------------------------------------------------------------------
    |
    | This area of the config file contains a more specific 'frozen'
    | configuration for the CMS, allowing specific pages to be locked without
    | revoking access to the entire CMS section
    |
    */

    'locked_by_id'  => [
        /*
        'News'  => [
            1
        ]
        */
    ]

];