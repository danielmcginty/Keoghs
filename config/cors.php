<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Origins
    |--------------------------------------------------------------------------
    |
    | This array contains the valid Origin URLs that can send data to the API.
    | Note - this is only required for POST requests, as GET requests pull
    | down data available to the public.
    */

    'origins' => [
        'http://localhost:8000'
    ],

    /*
    |--------------------------------------------------------------------------
    | Auth Token
    |--------------------------------------------------------------------------
    |
    | Contains the Auth Token that's expected in the HTTP Header to allow
    | access to the API
    */

    'auth_token'    => 's3HwSSDXgZd4GZg2QNaszRxJZzDgpme3',

    /*
    |--------------------------------------------------------------------------
    | Auth Header
    |--------------------------------------------------------------------------
    |
    | Contains the HTTP Header name used for authenticating the request
    */

    'auth_header'   => 'X-Bspk-Auth-Token',

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------
    |
    | This array contains the valid HTTP Methods that the API will accept.
    */

    'methods' => [
        'GET',
        'POST',
        'OPTIONS'
    ],

    /*
    |--------------------------------------------------------------------------
    | Headers
    |--------------------------------------------------------------------------
    |
    | This array contains the HTTP Headers that the API will allow to be sent
    | to it.
    */

    'headers' => [
        'X-Bspk-Auth-Token',
        'Content-Type'
    ],

];