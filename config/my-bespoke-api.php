<?php

return [

    /*
    |--------------------------------------------------------------------------
    | My Bespoke Organisation ID
    |--------------------------------------------------------------------------
    |
    | This is the ID for the organisation within My Bespoke to
    | get information such as support tier etc
    |
    */
    'organisation_id' => env('MY_BESPOKE_ORGANISATION_ID', 0),

    /*
    |--------------------------------------------------------------------------
    | Sitedocs Website ID
    |--------------------------------------------------------------------------
    |
    | This is the ID for the website within Sitedocs to
    | get information such as support tier etc
    |
    */
    'sitedocs_id' => env('MY_BESPOKE_SITEDOCS_ID', 0),

    /*
    |--------------------------------------------------------------------------
    | API Oauth Token
    |--------------------------------------------------------------------------
    |
    | Generated through the http://myapi.bespokedigital.agency/ service.
    | Used to communicate with myapi.bespokedigital.agency in a secure manner
    |
    */
    'auth_token' => "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImFmYjM1NWQwNzliZTBhMTIwMmMxNDgzZWNkYTg0YTMzMjUyMzIzNTk5ZGYyMjEwZThhYzkwZTc4MDgwZDI3MmFlZmVmMjYzNDczMTZkODI5In0.eyJhdWQiOiIzIiwianRpIjoiYWZiMzU1ZDA3OWJlMGExMjAyYzE0ODNlY2RhODRhMzMyNTIzMjM1OTlkZjIyMTBlOGFjOTBlNzgwODBkMjcyYWVmZWYyNjM0NzMxNmQ4MjkiLCJpYXQiOjE0ODk0MDMzMTIsIm5iZiI6MTQ4OTQwMzMxMiwiZXhwIjo0NjQ1MDc2OTEyLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.VMaPxw1dyprTnivly4Cswnd8Sd5I2MIBI7jQ3fzmtd-S97AEvDwoKzjyGlvK2NMrN5gw0dPvoLMKfMSxGhfmUi0xBFQcD4u0Ys8f2PPVZyu6BD4dHrlvQXsDkI7wNcFQNsoSyMjAiDs0PjjcGwbAbubWIJiDAvMBvtR1lcUIKJweCKEIBnQatIL9M4CnJgULDfzuBwffa5MbdH843usADyX5yYahkBwoCe0G8L1uhNMy6Yuf7stFSd6R5XZ4qcnOw875ZIESNBM_yaUao0fLOHxYTXhwSEu_XvZkRMqZuoWF4XRzkdVv7WdBXNkugE-U6LdBpXBcaHUofwYZmDz-hlguG1i8yy2tca9y3z82qp9piRegFbflU_QA9OVi-qJLompl5WezCxL1MjYZMz66FM2KT713q3X4VMyQ7tc1HJHpmzrAKJMM5LtAP8qpxZyiOs50M2kxq0UYalRTSO5xRGQO88TFuAZnGhAMsB-a8_dC7APzQ4QIkCA85rkkAVez_gk8GInOZ1crnebC2yTQSX3I7B3kEx9pQlT6e2ULggqFPBaVfWT9a-idgm36xh1pLMbvWMSVBHWQlywoZ6EvZWofWke_SX78uRZ7YCWWJOvlOkQXKjjG-3uB0LunvreRIO-ggc59WjgZmYhipE3kSIX0BeWL9tqV_Ef60T7MvVQ"

];
