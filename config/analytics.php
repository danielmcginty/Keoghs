<?php

/**
 * REFER TO https://github.com/spatie/laravel-analytics FOR A ROUGH GUIDE ON GETTING ANALYTICS DETAILS
 */
return

    [
        /*
         * The siteId is used to retrieve and display Google Analytics statistics
         * in the admin-section.
         *
         * Should look like: ga:xxxxxxxx.
         */
        'view_id' => env('ANALYTICS_VIEW_ID'),

        /*
         * You need to download a JSON file from the Google API console
         * Be sure to store this file in a secure location
         */
        'service_account_credentials_json' => storage_path('laravel-analytics/service_key.json'),

        /*
         * The amount of minutes the Google API responses will be cached.
         * If you set this to zero, the responses won't be cached at all.
         */
        'cacheLifetime' => 60 * 24 * 2,

        /*
         * The amount of seconds the Google API responses will be cached for
         * queries that use the real time query method. If you set this to zero,
         * the responses of real time queries won't be cached at all.
         */
        'realTimeCacheLifetimeInSeconds' => 5,
    ];
