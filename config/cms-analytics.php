<?php

/**
 * REFER TO https://github.com/spatie/laravel-analytics FOR A ROUGH GUIDE ON GETTING ANALYTICS DETAILS
 */
return

    [
        /**
         * Set enabled to true to enable dashboard analytics for this build.
         */
        'enabled' => env('ANALYTICS_ENABLED', false),

        /**
         *  Populate this array with the IDs for up to 4 goals to be tracked in the admin dashboard
         */
        'goal_ids' => [
            1 => env('ANALYTICS_GOAL_ID_1', false),
            2 => env('ANALYTICS_GOAL_ID_2', false),
            3 => env('ANALYTICS_GOAL_ID_3', false),
            4 => env('ANALYTICS_GOAL_ID_4', false)
        ],

        'goal_labels' => [
            1 => env('ANALYTICS_GOAL_LABEL_1', false),
            2 => env('ANALYTICS_GOAL_LABEL_2', false),
            3 => env('ANALYTICS_GOAL_LABEL_3', false),
            4 => env('ANALYTICS_GOAL_LABEL_4', false)
        ]
    ];