<?php
/*
|--------------------------------------------------------------------------
| Detect The Application Environment
|--------------------------------------------------------------------------
|
| Laravel takes a dead simple approach to your application environments
| so you can just specify a machine name for the host that matches a
| given environment, then we will automatically detect it for you.
|
| As an example, say we've got the site set up on a vagrant box.
| We can define a .dev.env file at the site root to allow custom
| configuration values.
|
*/
$env = $app->detectEnvironment(function(){
    if( isset($_SERVER['HTTP_HOST']) ) {
        switch ($_SERVER['HTTP_HOST']) {
            case 'keoghs-co-uk.laragon': // MB Vagrant
            case 'keoghs.localhost': // JN Docker
                $my_env = '.dev';
                break;
            case 'keoghs-2021.bespoke-demo.co.uk':
                $my_env = '.bespokedemo';
                break;
            default:
                $my_env = '';
                break;
        }
    } elseif (file_exists(__DIR__ . '/../.environment')) {
        switch (trim(file_get_contents(__DIR__ . '/../.environment'))) {
            case 'dev':
                $my_env = '.dev';
                break;
            case 'demo':
            case 'bespokedemo':
                $my_env = '.bespokedemo';
                break;
            case 'staging':
                $my_env = '.staging';
                break;
            default:
                $my_env = '';
                break;
        }
    } else {
        $my_env = '';
    }

    $customPath = __DIR__ . '/../.env' . $my_env;
    if( file_exists( $customPath ) ) {
        // If it exists, we can load it in!
        $dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/../', '.env' . $my_env);
        $dotenv->load(); // this is important
    } else {
        die( 'The app environment isn\'t correctly configured!' );
    }
});
