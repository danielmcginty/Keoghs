@servers(['localhost' => '127.0.0.1'])

/**
 * Variables:
 *  - repository
 *  - branch
 *  - appdir
 */
@setup
    if (!isset($repository) || is_null($repository)
        || !isset($branch) || is_null($branch)
        || !isset($appdir) || is_null($appdir)
    ) {
        throw new \Exception('Please ensure all required variables are defined.');
    }

    $appdir = rtrim($appdir, '/');
    $releases_dir = rtrim($appdir, '/').'/releases';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
    clone_repository
    setup_environment
    run_composer
    run_migrations
    update_symlinks
    cleanup
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --branch {{ $branch }} --depth 1 {{ $repository }} {{ $new_release_dir }}
    cd {{ $new_release_dir }}
@endtask

@task('setup_environment')
    echo "Setting up environment ({{ $release }})"
    [ -f "{{ $appdir }}/.environment" ] && cp "{{ $appdir }}/.environment" {{ $new_release_dir }}/.environment
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}
    /opt/plesk/php/7.3/bin/php /usr/lib64/plesk-9.0/composer.phar install -q -o
@endtask

@task('run_migrations')
    echo "Running migrations ({{ $release }})"
    cd {{ $new_release_dir }}
    /opt/plesk/php/7.3/bin/php artisan migrate --force
@endtask

@task('update_symlinks')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $appdir }}/storage {{ $new_release_dir }}/storage

    echo "Linking current release"
    ln -nfs {{ $new_release_dir }} {{ $appdir }}/current
@endtask

@task('cleanup')
	cd {{ $releases_dir }}
	find . -maxdepth 1 -name "20*" | sort | head -n -4 | xargs rm -Rf
	echo "Cleaned up old deployments"
@endtask