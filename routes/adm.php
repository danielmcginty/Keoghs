<?php

$this->app->singleton('document', function($app)
{
    return new \App\Library\Document;
});

// Auth Routes
// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('adm.login');
$this->get('login/checkAuth', 'Auth\LoginController@checkAuth')->name('adm.check-auth');
$this->post('login/ajaxLogin', 'Auth\LoginController@ajaxLogin')->name('adm.login-ajax');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('adm.logout');

// Registration Routes...
$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('adm.register');
$this->post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('adm.forgot-password');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
$this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('adm.reset-password');

Route::group(['middleware' => 'auth:adm'], function(){
    Route::any('asset-manager/{function}', 'AssetManagerController@performAction')->name('adm.asset-manager.function');
    Route::any('asset-manager', 'AssetManagerController@index')->name('adm.asset-manager');

    Route::any('{ajax_call}', 'AdmController@ajax_call')->where('ajax_call', '(ajax_.*)')->name('adm.ajax');
    Route::any('{table?}/{action?}/{id?}/{locale?}', 'AdmController@index')->name('adm.path');
});