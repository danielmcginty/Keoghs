<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');
*/
Route::get('/resource/{table_name}', 'Api\RouteController@table');
Route::get('/component/{component}/{component_param?}', 'Api\RouteController@component');

/*
Route::get('/{url?}', 'Api\RouteController@index')->where('ur;', '(?!(adm|_|dist|css|js))(.*)');
*/