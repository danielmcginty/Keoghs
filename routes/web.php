<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/*
 * Uncomment the below line if the website needs customer login functionality
 */
// Auth::routes();

Route::group(['middleware' => ['front']], function () {
    Route::get('/images/{dimensions}/{crop}/{path}', 'ImageController@index')->where('path', '(.*)')->where('dimensions', '(\d*x\d*)')->where('crop', '(\d+x\d+(-\d*x\d*)?)')->name('image.crop');
    Route::get('/images/{dimensions}/{path}', 'ImageController@index')->where('path', '(.*)')->where('dimensions', '(\d*x\d*)')->name('image.resize');
    Route::get('/images/inline/{dimensions}/{path}', 'ImageController@index')->where('path', '(.*)')->where('dimensions', '(\d*x\d*)')->name('image.inline-resize');
    Route::get('/images/{path}', 'ImageController@index')->where('path', '(.*)')->name('image');
    Route::get('/files/{path}', 'FileController@index')->where('path', '(.*)')->name('file');
    Route::get('/custom-form-uploads/{filename}', 'FileController@customFormUpload')->where('filename', '(.*)')->name('custom_form_upload');

    Route::any('/ajax/{controller}/{ajax_function}', 'AjaxController@index')->name('theme.ajax');

    // Beanstalk Pre/Post hooks
    Route::any('/beanstalk/pre', 'BeanstalkController@pre');
    Route::any('/beanstalk/post', 'BeanstalkController@post');

    // Catch all for URLs table
    Route::group(['middleware' => ['web', 'front']], function(){
        Route::any('/sitemap.xml', 'SitemapController@xml')->name( 'sitemap.xml' );
        Route::any('/robots.txt', 'RobotsController@index' );
        Route::any('/amp/{url?}', 'RouteController@amp')->where('url', '(?!(adm|_|dist|css|js))(.*)')->name('amp');
        Route::any('/{url?}', 'RouteController@index')->where('url', '(?!(adm|_|dist|css|js))(.*)')->name('theme');
    });
});
