<?php

namespace Modules\Locations\Adm\Models\PageBuilder;

use Adm\Crud\LaraCrud;
use Adm\Models\PageBuilder\PageBuilderBase;
use Modules\Locations\Theme\Models\Location;

class Locations extends PageBuilderBase
{
    public $display_name = 'Locations';

    public $description = 'Add a map of Keoghs Locations to the page';

    public $builder_fields = [
        'first_location'    => [
            'display_name'      => 'First location',
            'help'              => 'Select a location to display first, or leave empty to show the first alphabetically',
            'type'              => 'select',
            'options'           => [],
            'required'          => false,
            'validation'        => []
        ]
    ];

    public function callbackBeforePrepare(LaraCrud $crud) 
    {
        $locs = [];
        foreach (Location::orderBy('title')->get() as $Location) {
            $locs[$Location->location_id] = $Location->title;
        }
        $this->builder_fields['first_location']['options'] = $locs;
    }
}