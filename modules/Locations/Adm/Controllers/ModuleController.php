<?php

namespace Modules\Locations\Adm\Controllers;

use Adm\Controllers\AdmController;
use Modules\Locations\Config\LocationConfig;

class ModuleController extends AdmController
{
    public function locations()
    {
        $this->crud->setTable('locations');
        $this->crud->setSubject('Locations');

        $this->crud->columns('title', 'people_filter', 'status', 'created_at');

        $this->crud->columnGroup('main', ['title', 'address', 'contact_details']);
        $this->crud->columnGroup('side', ['status', 'latitude', 'longitude', 'people_filter']);
    }


    public function getLocationsDefaultLabels()
    {
        return LocationConfig::$labels;
    }

    public function getLocationsDefaultHelp()
    {
        return LocationConfig::$help;
    }
}