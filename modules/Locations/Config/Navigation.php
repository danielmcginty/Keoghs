<?php

namespace Modules\Locations\Config;

use Adm\Interfaces\NavigationInterface;

class Navigation implements NavigationInterface
{
    public function getNavItems()
    {
        return [
            [
                'title' => 'Locations',
                'url' => route('adm.path', 'locations'),
                'position' => 75
            ]
        ];
    }
}