<?php

namespace Modules\Locations\Config;

class LocationConfig
{
    public static $labels = [
        'title' => 'Title',
        'address' => 'Address',
        'telephone' => 'Telephone Number',
        'contact_details' => 'Contact Details',
        'latitude' => 'Latitude',
        'longitude' => 'Longitude',
        'people_filter' => 'Our People - Filter'
    ];

    public static $help = [
        'address' => 'The office address',
        'telephone' => 'The office telephone number',
        'latitude' => 'The latitude position of the office, used for positioning a map marker',
        'longitude' => 'The longitude position of the office, used for positioning a map marker',
        'people_filter' => 'Set the location to use as a filter for the \'Our People at this Location\' link',
    ];
}