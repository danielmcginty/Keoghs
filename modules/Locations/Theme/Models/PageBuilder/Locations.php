<?php

namespace Modules\Locations\Theme\Models\PageBuilder;

use App\Models\PageBuilder\BaseBlockModel;
use App\Models\Url;
use Illuminate\Database\Eloquent\Collection;
use Modules\Locations\Theme\Models\Location;

class Locations extends BaseBlockModel
{
    public function getValue($page_builder_block, $field, $original_value)
    {
        switch ($field) {
            case 'first_location':
                if (!$original_value) { $original_value = 1; }
                return Location::where('location_id', $original_value)->first();
            default:
                return parent::getValue($page_builder_block, $field, $original_value);
        }
    }

    public function postProcess($fields)
    {
        $Locations = new Collection();
        if (!empty($fields['first_location']) && $fields['first_location']) {
            $Locations->push($fields['first_location']);
            foreach (Location::query()->where('location_id', '<>', $fields['first_location']->location_id)->orderBy('title')->get() as $Loc) {
                $Locations->push($Loc);
            }
        } else {
            $Locations = Location::query()->orderBy('title')->get();
        }
        $fields['locations'] = $Locations;
        $fields['PeoplePage'] = Url::getSystemPage('person', 'index');

        return $fields;
    }
}