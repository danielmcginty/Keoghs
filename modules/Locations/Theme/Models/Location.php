<?php

namespace Modules\Locations\Theme\Models;

use App\Models\CommonModel;
use App\Models\Scopes\LanguageScope;
use App\Models\Scopes\PublishedScope;
use App\Models\Traits\HasCompositePrimaryKey;
use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends CommonModel
{
    use SoftDeletes, HasCompositePrimaryKey, HasQueryBuilderCache;

    protected $table = 'locations';

    protected $primaryKey = ['location_id', 'language_id'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new PublishedScope);
        static::addGlobalScope(new LanguageScope);
    }
}