<?php
namespace Modules\MenuBuilder\Adm\Controllers;

use Adm\Controllers\AdmController;
use Illuminate\Support\Facades\Cache;
use Modules\MenuBuilder\Models\Menu;
use Adm\Models\Url;

class ModuleController extends AdmController {
    protected static $_EXCLUDED_TABLES  = [
        'insights',
        'people',
    ];

    /**
     * CRUD Functionality for the Menu Builder.
     * Performs a custom action based on the CRUD state
     */
    public function menu_builder(){
        $use_menu_builder = false;

        if( $this->crud->getAction() == 'edit' ){ $use_menu_builder = true; }
        if( $this->crud->getAction() == 'create' && $this->crud->request->route()->parameter('locale') ){ $use_menu_builder = true; }

        if( $use_menu_builder ) {
            $this->menu_edit();
        } else {
            $this->menu_crud();
        }
    }

    /**
     * Basic CRUD Functionality displayed when creating a form, or updating it's details
     */
    private function menu_crud(){
        $this->crud->setTable('menus');

        $this->crud->excludeFromListing( 'reference', 'structure', 'nest_limit' );
        $this->crud->excludeFromEdit( 'structure' );

        $this->crud->orderColumns( 'title' );

        $this->crud->displayAs( 'title', 'Menu' );
        $this->crud->help( 'title', 'The title of this Menu' );

        $this->crud->help( 'reference', 'Used as a key reference for accessing the menu on the front end.<br>e.g. <strong>main_menu</strong> which would be called on the front end via <code>$MenuHelper::getMenu( \'<strong>main_menu</strong>\' );</code>' );

        $this->crud->columnGroup( 'main', array( 'title', 'reference', 'nest_limit' ) );
        $this->crud->columnGroup( 'side', array() );

        $this->crud->help( 'nest_limit', 'Includes the top level. 1 = Top level, 2 = Dropdown Level etc.' );

        $this->crud->unsetCopy();

        if( !\Auth::user()->userGroup->super_admin ){
            $this->crud->unsetDelete();
            $this->crud->unsetAdd();
        }
    }

    /**
     * Custom Functionality used to create a menu through JavaScript drag & drop
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    private function menu_edit(){
        $this->crud->setTable('menus');
        $this->crud->setPrimaryColumn('menu_id');

        $this_menu = new \StdClass();
        if ($this->crud->getAction() == 'edit') {
            $this_menu = Menu::where([
                'menu_id' => $this->crud->request->route()->parameter('id'),
                'language_id' => $this->crud->language_id
            ])->first();
        } elseif( $this->crud->getAction() == 'create' && $this->crud->request->route()->parameter('locale') ){
            $this_menu = Menu::where([
                'menu_id' => $this->crud->request->route()->parameter('id'),
                'language_id' => $this->crud->getDefaultLanguage()
            ])->first();

            $this_menu->structure = json_encode( array() );
        }

        if( $this_menu ) {
            if( is_serialized( $this_menu->structure ) ){ $this_menu->structure = json_encode( unserialize( $this_menu->structure ) ); }
            $data['menu_array'] = json_decode( $this_menu->structure, true );
            $data['menu_id'] = $this->crud->request->route()->parameter('id');
            $data['menu_name'] = $this_menu->title;
            $data['menu_reference'] = $this_menu->reference;
            $data['nest_limit'] = $this_menu->nest_limit;
        } else {
            return response()->redirectToRoute( 'adm.path', ['table' => 'menu-builder'] );
        }

        $data['page_types'] = [];//$this->getMenuBuilderOptions();

        $data['language_links'] = $this->crud->getLanguageLinks();

        $this->crud->setView( 'menu-builder.menu-builder', $data);
    }

    public function ajax_get_menu_builder_urls(){
        $raw_options = $this->getMenuBuilderOptions();
        $options = [];

        foreach( $raw_options as $page_type => $pages ){
            $option_pages = [];
            foreach( $pages as $page ){
                $option_pages[] = [
                    'id'    => $page->id,
                    'title' => $page->url_object->title
                ];
            }

            if( !empty( $option_pages ) ){
                $options[ $page_type ] = $option_pages;
            }
        }

        uksort( $options, function( $a, $b ){
            if( strtolower( $a ) == 'pages' ){ return -1; }
            if( strtolower( $b ) == 'pages' ){ return 1; }
            return strcmp( $a, $b );
        });

        return response()->json( ['urls' => $options] );
    }

    /**
     * Internal function used to generate a 'sectioned' list of URLs, sectioned by table name
     *
     * @return array
     */
    private function getMenuBuilderOptions(){
        $urls = Url::where('language_id', $this->crud->language_id)
            ->whereNotIn('table_name', self::$_EXCLUDED_TABLES)
            ->get();

        $sectioned_urls = [];
        foreach( $urls as $ind => $url ){

            // Skip any URLs which are deleted
            if( !$url->url_object || !empty($url->url_object->deleted_at) ){ continue; }

            $index = ucwords( str_replace( "_", " ", $url->table_name ) );
            if( !isset( $sectioned_urls[ $index ] ) ){ $sectioned_urls[ $index ] = []; }

            $sectioned_urls[ $index ][] = $url;
        }

        return $sectioned_urls;
    }

    /**
     * Return menu items by Ajax
     *
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Throwable
     */
    public function ajax_get_menu_item($request) {

        $json = array();
        $json['items'] = array();

        $next_id = (int)$request->input('total_menu_items') + 1;

        $menu_array = array();

        switch ($request->input('item_type')) {
            case 'custom':

                $menu_array[] = array(
                    'id' => $next_id, // Incremented value (arbitrary value for nestedSortable to use as an id="" attribute)
                    'index' => $next_id,
                    // Note: custom links can't have a 'url_id'
                    'item_type' => 'custom',
                    'link_label' => $request->input('item_label'), // Menu item label
                    // Note: custom links can't have an 'original_label'
                    'link_url' => $request->input('item_url'),
                    'item_target' => $request->input('item_target'),
                    'page_type' => 'Custom',
                );

                $next_id++;

                $view = view('menu-builder.partials.menu-list', array('menu_array' => $menu_array, 'index' => $next_id));
                $rendered = $view->render();
                $json['items'] = $rendered;
                break;

            case 'dynamic':

                foreach ($request->input('url_ids') as $url) {
                    $this_url = Url::find($url);

                    if( $this_url ) {
                        $menu_array[] = array(
                            'id' => $next_id,
                            'index' => $next_id,
                            // Incremented value (arbitrary value for nestedSortable to use as an id="" attribute)
                            'url_id' => $url,
                            // Database ID of the URL row (for now I've just used the $url until model is in place)
                            'item_type' => 'dynamic',
                            'link_label' => $this_url->url_object->title,
                            // Menu item label
                            'original_label' => $this_url->url_object->title,
                            // Actual page title
                            'link_url' => $this_url->url,
                            'page_type' => ucwords( \Illuminate\Support\Str::singular( str_replace( '_', ' ', $this_url->table_name ) ) ),
                            // e.g. Page, Service, Testimonial
                        );

                        $next_id++;
                    }
                }

                $view = view('menu-builder.partials.menu-list', array('menu_array' => $menu_array, 'index' => $next_id));
                $rendered = $view->render();
                $json['items'] = $rendered;

                break;
        }

        return response()->json($json);
    }

    /**
     * Save menu by Ajax
     *
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax_save_menu($request) {

        $json = array();

        $this->validate_menu_inputs($request->input('menu'), $json['errors']);

        if (empty($json['errors'])) {

            $menu_exists = Menu::where([
                'menu_id' => $request->input('menu_id'),
                'language_id' => $request->input('language_id')
            ])->first();

            if( $menu_exists ){
                $menu = $menu_exists;
            } else {
                $original_menu = Menu::where([
                    'menu_id' => $request->input('menu_id'),
                    'language_id' => $this->crud->getDefaultLanguage()
                ])->first();

                $translation = new Menu;
                $translation->menu_id = $original_menu->menu_id;
                $translation->language_id = $request->input('language_id');
                $translation->title = $original_menu->title;
                $translation->reference = $original_menu->reference;
                $translation->nest_limit = $original_menu->nest_limit;

                $menu = $translation;
            }

            if( $request->input('language_id') != $this->crud->getDefaultLanguage() ) {
                $json['redirect'] = route('adm.path', [
                    'table' => 'menu-builder',
                    'action' => 'edit',
                    'id' => $request->input('menu_id'),
                    'locale' => $this->crud->_getLocaleByLanguageId($request->input('language_id'))
                ]);
            } else {
                $json['redirect'] = route('adm.path', [
                    'table' => 'menu-builder',
                    'action' => 'edit',
                    'id' => $request->input('menu_id')
                ]);
            }

            $menu->structure = json_encode( $request->input('menu') );
            $menu->save();

            \Session::flash('success', $menu->title . ' saved.');

        }

        Cache::flush();

        return response()->json($json);
    }

    /**
     * Validate all inputs to make sure they're not empty
     *
     * @param $menu_items
     * @param $errors
     * @return array
     */
    private function validate_menu_inputs($menu_items, &$errors) {

        if( empty($errors) ){ $errors = array(); }

        if( !is_array( $menu_items ) ){ return true; }

        foreach($menu_items as $item_index => $menu_item) {
            foreach($menu_item as $input_name => $value) {
                if (!in_array($input_name, array('id', 'children','item_target'))) {

                    if (empty($value)) {
                        $errors['menu_item['.$menu_item['id'].']['.$input_name.']'] = 'This is required.';
                    }

                } elseif( $input_name == 'children' ){
                    $this->validate_menu_inputs( $value, $errors );
                }
            }
        }

        return $errors;
    }

    /**
     * Returns a set of custom validation rules for the menu builder.
     *
     * @param $validation
     * @param $inputs
     */
    public function getMenuBuilderValidationRules( &$validation, $inputs ){
        if( isset( $this->crud->parameters['id'] ) ){
            $validation['reference'] = 'unique:menus,reference,' . $this->crud->parameters['id'] . ',menu_id';
        } else {
            $validation['reference'] = 'unique:menus';
        }
    }

    /**
     * Callback Function before a Menu is saved.
     * If the user hasn't provided a nest limit value, default this to 1.
     *
     * @param $inputs
     * @param $item
     */
    public function beforeSavedMenuBuilderItem( &$inputs, &$item ){
        if( empty( $inputs['nest_limit'] ) || $inputs['nest_limit'] <= 0 ){
            $inputs['nest_limit'] = 1;
        }
    }
}