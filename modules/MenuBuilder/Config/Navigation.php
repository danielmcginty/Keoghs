<?php
namespace Modules\MenuBuilder\Config;

use Adm\Interfaces\NavigationInterface;

/**
 * Class Navigation
 * @package Modules\MenuBuilder\Config
 *
 * The CMS Navigation Builder for the Menu Module
 */
class Navigation implements NavigationInterface {
    /**
     * Returns the CMS Navigation links for the module
     *
     * @return array
     */
    public function getNavItems(){
        return [
            [ 'title' => 'Menus', 'url' => route('adm.path', 'menu-builder'), 'position' => 850 ]
        ];
    }
}