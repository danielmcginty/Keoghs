<?php
namespace Modules\MenuBuilder\Models;

use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Url;
use App\Models\Traits\HasCompositePrimaryKey;
use App\Models\Scopes\LanguageScope;

class Menu extends Model {
    // As we've got a deleted_at flag, a composite primary key, and want to cache DB calls, include the relevant traits
    use SoftDeletes, HasCompositePrimaryKey, HasQueryBuilderCache;

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'menus';

    /**
     * The primary keys in the table
     * @var array
     */
    protected $primaryKey = [ 'menu_id', 'language_id' ];

    /**
     * The fields that are 'bulk assignable' for this model
     * @var array
     */
    protected $fillable = ['title', 'reference', 'structure', 'nest_limit'];

    /**
     * Any date fields for this model
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * When 'booting' this model, we only want to include menus in the correct language, so add the global scope
     */
    protected static function boot(){
        parent::boot();

        static::addGlobalScope(new LanguageScope);
    }

    /**
     * Required for nestedSortable rendering so each item has it's own unique 'index'
     * And also because of how Laravel loads in blades, it doesn't keep a reference to the 'index' variable
     * in between nested @includes
     *
     * @var int
     */
    private $item_count = 1;

    /**
     * Create a new model instance that is existing.
     *
     * @param  array  $attributes
     * @param  string|null  $connection
     * @return static
     */
    public function newFromBuilder($attributes = [], $connection = null)
    {
        $model = $this->newInstance([], true);

        $model->setRawAttributes((array) $attributes, true);

        $model->setConnection($connection ?: $this->getConnectionName());

        if( is_serialized( $model->structure ) ){ $model->structure = json_encode( unserialize( $model->structure ) ); }
        $structure = json_decode( $model->structure, true );
        if( !empty( $structure ) ) {
            $this->_processItems($structure);
            $model->structure = json_encode($structure);
        }

        return $model;
    }

    /**
     * Internal function used to parse the URLs set against the menu, and generate a nested array of all items,
     * assigning labels, URLs, etc.
     * @param $items
     */
    private function _processItems( &$items ){
        foreach( $items as $ind => &$item ){
            $item['index'] = $this->item_count++;
            if( !isset( $item['original_label'] ) ){ $item['original_label'] = ''; }
            if( !isset( $item['link_url'] ) ){ $item['link_url'] = ''; }
            $item['page_type'] = 'Custom';

            if( isset( $item['url_id'] ) && !empty( $item['url_id'] ) ){
                $url = Url::find( $item['url_id'] );

                if( !empty( $url ) && !empty( $url->object ) ) {
                    $item['original_label'] = $url->object->title;
                    $item['link_url'] = $url->url;
                    $item['page_type'] = ucwords( \Illuminate\Support\Str::singular( str_replace( '_', ' ', $url->table_name ) ) );
                } else {
                    unset( $items[ $ind ] );
                }
            }

            if( isset( $item['children'] ) && !empty( $item['children'] ) ){
                $this->_processItems( $item['children'] );
            }
        }
    }
}