<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function(Blueprint $table){
            $table->integer('menu_id');
            $table->integer('language_id');
            $table->string('title');
            $table->string('reference');
            $table->text('structure')->nullable();
            $table->integer('nest_limit')->default(3);
            $table->timestamps();
            $table->softDeletes();
            $table->primary(['menu_id', 'language_id']);
            $table->unique(['menu_id', 'language_id', 'reference']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menus');
    }
}
