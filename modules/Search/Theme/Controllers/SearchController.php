<?php
namespace Modules\Search\Theme\Controllers;

use App\Models\Url;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Modules\Search\Theme\Models\Search;
use Illuminate\Http\Request;

class SearchController extends Controller {

    /**
     * Front-End Search Function.
     * Based on the GET search term parameter, perform a website search and display paginated results.
     *
     * @param Request $request
     */
    public function search( Request $request ){
        $this_page = Url::getSystemPage('search', 'search');

        if( $this_page ) {

            $home = Url::getSystemPage('home', 'index');
            if( $home ) {
                $this->addBreadcrumb(route('theme', ['url' => $home->url]), $home->title);
            }

            foreach( $this->getBreadcrumbs( $this_page, 'title', 'parent_page' ) as $breadcrumb ){
                $this->addBreadcrumb( route('theme', ['url' => $breadcrumb['url']]), $breadcrumb['label'] );
            }

            $term = $request->input('term');
            if( !empty( $term ) ){
                $this_page->description = 'Search results for "' . $term . '"';
                $this->term = $term;
                $this->search_term = $term;

                $search_model = new Search( $term );

                $this->results = $search_model->search();
                $this->results->setPath( route('theme', $this_page->url ) );
                $this->results->appends( request()->except( 'page' ) );
            }

            $this->pageTitle = $this_page->title;
            $this->this_page = $this_page;
            $this->view = 'search.results';
        }
    }
}