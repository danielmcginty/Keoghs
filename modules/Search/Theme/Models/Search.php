<?php
namespace Modules\Search\Theme\Models;

use App\Models\Url;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Session;

/**
 * Class Search
 * @package Modules\Search\Theme\Models
 *
 * The Search Model performs all website search functionality.
 * Calling the search() function will return a paginated set of results, completely ready for output.
 */
class Search {
    /**
     * The current search term, set on Construct
     * @var string
     */
    private $search_term = '';

    /**
     * The current language ID to search
     * @var int
     */
    private $language_id = 1;

    /**
     * The number of results to display per page
     * @var int
     */
    private $results_per_page = 10;

    /**
     * Array containing the Database Tables to search for results from.
     * Simply add, or remove/comment table names to include or exclude them from results.
     *
     * @var array
     */
    private $search_tables = [
        'case_studies',
        'case_study_categories',
        'expertise',
        'insights',
        'journeys',
        'news_articles',
        'news_categories',
        'offices',
        'pages',
        'people',
        'team_members'
    ];

    /**
     * A 'map' array, telling the model what the Primary Key ID field is for each table within $search_tables.
     *
     * @var array
     */
    private $search_table_ids = [
        'case_studies'          => 'case_study_id',
        'case_study_categories' => 'case_study_category_id',
        'expertise'             => 'expertise_id',
        'insights'              => 'insight_id',
        'journeys'              => 'journey_id',
        'news_articles'         => 'news_article_id',
        'news_categories'       => 'news_category_id',
        'offices'               => 'office_id',
        'pages'                 => 'page_id',
        'people'                => 'person_id',
        'team_members'          => 'team_member_id'
    ];

    /**
     * A 'map' array, telling the model what the Model is for each table within $search_tables.
     *
     * @var array
     */
    private $search_table_models = [
        'case_studies'          => '\Modules\CaseStudies\Theme\Models\CaseStudy',
        'case_study_categories' => '\Modules\CaseStudies\Theme\Models\CaseStudyCategory',
        'expertise'             => '\Modules\Expertise\Theme\Models\Expertise',
        'insights'              => '\Modules\Insights\Theme\Models\Insight',
        'journeys'              => '\Modules\Journeys\Theme\Models\Journey',
        'news_articles'         => '\Modules\News\Theme\Models\NewsArticle',
        'news_categories'       => '\Modules\News\Theme\Models\NewsCategory',
        'offices'               => '\Modules\Offices\Theme\Models\Office',
        'pages'                 => '\Modules\Page\Theme\Models\Page',
        'people'                => '\Modules\OurPeople\Theme\Models\Person',
        'team_members'          => '\Modules\TeamMembers\Theme\Models\TeamMember'
    ];

    /**
     * A 'map' array, telling the model which fields to search for a match in each table in $search_tables.
     *
     * @var array
     */
    private $search_fields = [
        'case_studies'          => [
            'title'                 => 'title',
            'content'               => 'excerpt',
            'h1_title',
            'page_builder'
        ],
        'case_study_categories' => [
            'title'                 => 'title',
            'content'               => 'content',
            'h1_title',
            'page_builder'
        ],
        'expertise'             => [
            'title'                 => 'title',
            'content'               => 'body',
            'introduction',
        ],
        'insights'              => [
            'title'                 => 'title',
            'content'               => 'body',
            'introduction',
        ],
        'journeys'              => [
            'title'                 => 'title',
            'content'               => 'content',
            'excerpt',
            'position'
        ],
        'news_articles'         => [
            'title'                 => 'title',
            'content'               => 'excerpt',
            'h1_title',
            'page_builder'
        ],
        'news_categories'       => [
            'title'                 => 'title',
            'content'               => 'description',
            'h1_title',
            'page_builder'
        ],
        'offices'               => [
            'title'                 => 'title',
            'content'               => 'address',
            'h1_title',
            'page_builder'
        ],
        'pages'                 => [
            'title'                 => 'title',
            'content'               => 'title',
            'h1_title',
            'page_builder'
        ],
        'people'                => [
            'title'                 => 'first_name',
            'content'               => 'biography',
            'last_name',
            'job_title',
            'location'
        ],
        'team_members'          => [
            'title'                 => 'title',
            'content'               => 'title',
            'job_title',
            'page_builder'
        ]
    ];

    /**
     * A 'map' array, telling the model which fields we're searching are more relevant than others for the table.
     * A higher relevance means that the result will appear higher up in results.
     *
     * E.g. 'case_studies' => [ 'title' => 3, 'excerpt' => 2 ]
     * This means that if the case study title contains the search term, it will appear higher in results than if
     * the search term was in the excerpt.
     *
     * If a certain table needs prioritising over another, the relevance values can simply be increased.
     * E.g. 'news_articles' => [ 'title' => 3, 'excerpt' => 2 ], 'pages' => [ 'title' => 6, 'page_builder' => 4 ]
     * This means that the pages table will appear higher in results than news articles.
     *
     * @var array
     */
    private $search_field_relevance = [
        'case_studies'          => [
            'title'                 => 3,
            'h1_title'              => 3,
            'excerpt'               => 2,
            'page_builder'          => 2
        ],
        'case_study_categories' => [
            'title'                 => 3,
            'h1_title'              => 3,
            'content'               => 2,
            'page_builder'          => 2
        ],
        'expertise'             => [
            'title'                 => 3,
            'body'                  => 2,
            'introduction'          => 2,
        ],
        'insights'              => [
            'title'                 => 3,
            'body'                  => 2,
            'introduction'          => 2
        ],
        'journeys'              => [
            'title'                 => 3,
            'content'               => 2,
            'excerpt'               => 2,
            'position'              => 1,
        ],
        'news_articles'         => [
            'title'                 => 3,
            'h1_title'              => 3,
            'excerpt'               => 2,
            'page_builder'          => 2
        ],
        'news_categories'       => [
            'title'                 => 3,
            'h1_title'              => 3,
            'description'           => 2,
            'page_builder'          => 2
        ],
        'offices'               => [
            'title'                 => 3,
            'h1_title'              => 3,
            'address'               => 2,
            'page_builder'          => 2
        ],
        'pages'                 => [
            'title'                 => 3,
            'h1_title'              => 3,
            'page_builder'          => 2
        ],
        'people'                => [
            'first_name'            => 3,
            'last_name'             => 3,
            'biography'             => 1,
            'job_title'             => 2,
            'location'              => 2
        ],
        'team_members'          => [
            'title'                 => 3,
            'job_title'             => 3,
            'page_builder'          => 2
        ]
    ];

    private $table_order_priority = [
        'insights',
        'expertise',
        'people',
    ];

    private $table_section_headings = [
        'insights'  => 'Insights',
        'expertise' => 'Expertise',
        'people'    => 'People'
    ];

    private $page_builder_field_exclude_patterns    = [
        'colour',
        'class'
    ];

    /**
     * Constructor.
     * Takes a search term and maps it up to the model. Also sets the language ID we're searching.
     *
     * @param $term
     */
    public function __construct( $term ){
        $this->search_term = $term;

        // As a default, set the language ID to the currently browsed language.
        $this->language_id = Session::get( 'language_id' );
    }

    /**
     * Custom Override function.
     * Allows us to specify a different number of results per page as required.
     *
     * @param $limit
     */
    public function setLimit( $limit ){
        $this->results_per_page = ($limit > 0) ? $limit : 10;
    }

    /**
     * The main bulk of the model.
     * Returns a paginated set of results matching the search term the model was constructed with.
     *
     * @return LengthAwarePaginator
     */
    public function search(){
        $all_results_query = $this->_buildQuery();
        $all_results = $this->_buildResults( \DB::select( \DB::raw( $all_results_query ) ) );
        $collection = collect( $all_results );

        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        $results_to_display = $collection->slice( ($currentPage-1) * $this->results_per_page, $this->results_per_page );

        return new LengthAwarePaginator( $results_to_display, count( $collection ), $this->results_per_page );
    }

    /**
     * Builds up a complex query of UNIONs for each table we're searching.
     *
     * @return string
     */
    private function _buildQuery(){
        $queries = $this->_buildQueries();

        $order_by = 'relevance DESC';
        if (!empty($this->table_order_priority)) {
            $order_by = 'table_priority ASC, relevance DESC';
        }

        $whole_query = 'SELECT search.* FROM (' . implode( ' UNION ALL ', $queries ) . ') search ORDER BY ' . $order_by;

        return $whole_query;
    }

    /**
     * Builds up an array of distinct SQL queries, for each table we're searching, ready for putting into the UNION
     * query in _buildQuery()
     *
     * @return array
     */
    private function _buildQueries(){
        // first, we need to build up a query for each table we're searching
        $queries = [];
        foreach( $this->search_tables as $table ){
            if( !empty( $this->search_fields[ $table ] ) ){

                if( !empty( $this->search_field_relevance[ $table ] ) ){
                    $priority_string = ", (CASE ";
                    foreach( $this->search_field_relevance[ $table ] as $field => $priority ){
                        switch( $field ){
                            case 'page_builder':
                                $priority_string .= ' WHEN pbbc.value LIKE ' . \DB::connection()->getPdo()->quote("%{$this->search_term}%") . ' THEN ' . $priority . ' ';
                                break;
                            default:
                                $priority_string .= ' WHEN ' . $table . '.' . $field . ' LIKE ' . \DB::connection()->getPdo()->quote("%{$this->search_term}%") . ' THEN ' . $priority . ' ';
                                break;
                        }
                    }
                    $priority_string .= " END) AS relevance";
                } else {
                    $priority_string = ", 0 AS relevance";
                }

                if (!empty($this->table_order_priority)) {
                    $table_priority = in_array($table, $this->table_order_priority) ? array_search($table, $this->table_order_priority) : 999;
                    $priority_string .= ", " . (int)$table_priority . " AS table_priority";
                }

                $query = "SELECT DISTINCT '" . $table . "' AS table_name, " . $this->search_table_ids[ $table ] . " AS table_key" . $priority_string . " FROM `" . $table . "`";

                $wheres = [];
                foreach( $this->search_fields[ $table ] as $field ){
                    switch( $field ){
                        case 'page_builder':
                            $query .= ' LEFT JOIN page_builder_blocks pbb ON ( pbb.table_key = ' . $table . '.' . $this->search_table_ids[ $table ] . ' AND pbb.table_name = "' . $table . '" AND pbb.version = ' . $table . '.version AND pbb.deleted_at IS NULL )';
                            $query .= ' LEFT JOIN page_builder_block_content pbbc ON ( pbbc.page_builder_block_id = pbb.id ';

                            // Generate a list of fields to exclude from the page builder
                            // search (e.g. colour classes/etc)
                            foreach( $this->page_builder_field_exclude_patterns as $pattern ){
                                $query .= ' AND pbbc.field NOT LIKE "%' . $pattern . '%"';
                            }

                            $query .= ' )';
                            $wheres[] = 'pbbc.value LIKE ' . \DB::connection()->getPdo()->quote("%{$this->search_term}%") . '';
                            break;
                        default:
                            $wheres[] = $table . '.' . $field . ' LIKE ' . \DB::connection()->getPdo()->quote("%{$this->search_term}%") . '';
                            break;
                    }
                }

                $query .= " WHERE " . $table . ".language_id = '" . (int)$this->language_id . "' ";
                $query .= ' AND (' . implode( ' OR ', $wheres ) . ') ';

                $query .= ' AND ' . $table . '.deleted_at IS NULL';
                $query .= ' AND ' . $table . '.status = "1"';

                $query .= ' GROUP BY ' . $table . '.' . $this->search_table_ids[ $table ];
                if( !empty( $this->search_field_relevance[ $table ] ) ){
                    foreach( $this->search_field_relevance[ $table ] as $field => $priority ){
                        switch( $field ){
                            case 'page_builder':
                                $query .= ', pbbc.value';
                                break;
                            default:
                                $query .= ', ' . $table . '.' . $field;
                                break;
                        }
                    }
                }


                $queries[] = $query;
            }
        }

        return $queries;
    }

    /**
     * Based on the set of search results returned from the database, constructs an array of results to put in
     * the paginator instance.
     *
     * @param $raw_results
     * @return array
     */
    private function _buildResults( $raw_results ){
        $results = [];
        foreach( $raw_results as $result ){
            if( !empty( $result->table_name ) && !empty( $this->search_table_models[ $result->table_name ] ) ){
                $table_name = $result->table_name;

                $class = \App::make( $this->search_table_models[ $table_name ] );

                $result_obj = $class->where( [
                    $this->search_table_ids[ $table_name ] => $result->table_key,
                    'language_id' => $this->language_id
                ])->first();

                if( !empty( $result_obj ) ){

                    // First of all, ensure there is a URL and the page isn't set to no-index
                    $url_obj = Url::where( [
                        'table_name' => $table_name,
                        'table_key' => $result->table_key
                    ])->first();
                    if( empty( $url_obj ) || $url_obj->no_index_meta ){
                        continue;
                    }

                    $result_arr = [
                        'section_heading' => isset($this->table_section_headings[$table_name]) ? $this->table_section_headings[$table_name] : 'Website Results',
                        'url_object' => $url_obj,
                        'url' => route('theme', $result_obj->url),
                        'title' => $result_obj->{ $this->search_fields[ $table_name ]['title'] },
                        'excerpt' => ''
                    ];

                    if ($table_name == 'people') {
                        $result_arr['title'] = $result_obj->first_name . ' ' . $result_obj->last_name;
                    }

                    // If we're searching the page builder blocks, we can get a custom excerpt based on the blocks
                    if( in_array( 'page_builder', $this->search_fields[ $table_name ] ) ){
                        $excerpt_field = $this->_getPageBuilderExcerpt( $table_name, $result_obj->{ $this->search_table_ids[ $table_name ] } );
                    } else {
                        $excerpt_field = $result_obj->{ $this->search_fields[ $table_name ]['content'] };
                    }

                    $excerpt_field = strip_tags( $excerpt_field );

                    $result_arr['excerpt'] = $this->_trimExcerpt( $excerpt_field );

                    $results[] = $result_arr;
                };
            }
        }

        return $results;
    }

    /**
     * Centralised function used to get an excerpt for display on search results from a matched Page Builder block.
     *
     * @param $table_name
     * @param $table_key
     * @return string
     */
    private function _getPageBuilderExcerpt( $table_name, $table_key ){
        $content_blocks = \DB::table( 'page_builder_block_content AS pbbc' )
            ->select( 'pbbc.*' )
            ->join( 'page_builder_blocks AS pbb', 'pbb.id', '=', 'pbbc.page_builder_block_id' )
            ->where( 'pbb.table_name', '=', $table_name )
            ->where( 'pbb.table_key', '=', $table_key )
            ->where( 'pbb.language_id', '=', $this->language_id )
            ->whereNull( 'pbb.deleted_at' )
            ->where( 'pbbc.field', '=', 'content' )
            ->get();

        $excerpt_field = '';
        foreach( $content_blocks as $block ){
            if( stristr( $block->value, $this->search_term ) !== false || empty( $excerpt_field ) ){
                $excerpt_field = $block->value;
            }
        }

        return $excerpt_field;
    }

    /**
     * Centralised function used to ensure all returns search result excerpt text follows the same format.
     *
     * @param $excerpt
     * @return string
     */
    private function _trimExcerpt( $excerpt ){
        $excerpt = strip_tags( html_entity_decode( $excerpt, ENT_QUOTES, 'UTF-8' ) );

        $substr_start = 0;
        $substr_limit = 200;

        $excerpt = substr( $excerpt, $substr_start, $substr_limit );

        return utf8_encode( htmlentities( $excerpt, ENT_QUOTES, 'UTF-8' ) );
    }
}