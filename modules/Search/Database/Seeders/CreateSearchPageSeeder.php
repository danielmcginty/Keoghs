<?php
namespace Modules\Search\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CreateSearchPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // Search Page
        \Modules\Page\Theme\Models\Page::create([
            'page_id' => 7,
            'language_id' => 1,
            'parent' => 0,
            'title' => 'Search',
            'h1_title' => '',
            'image' => 0,
            'sort_order' => 0,
            'status' => 1
        ]);
        \App\Models\Url::create([
            'url' => 'search',
            'table_name' => 'pages',
            'table_key' => 7,
            'language_id' => 1,
            'path_prefix' => null,
            'manual' => 0,
            'no_index_meta' => 0,
            'meta_title' => 'Search',
            'meta_description' => '',
            'system_page' => 1,
            'route' => 'controller=search&method=search'
        ]);

        Model::reguard();
    }
}
