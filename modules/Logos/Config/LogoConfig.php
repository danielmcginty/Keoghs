<?php
namespace Modules\Logos\Config;

class LogoConfig {

    public static $labels   = [
        'title'             => 'Title / Reference',
        'image'             => 'Logo',
        'default_in_block'  => 'Block Default'
    ];

    public static $help     = [
        'title'             => 'The title, or reference, for this logo. Used in the CMS and image alt text',
        'image'             => 'The logo to display',
        'default_in_block'  => 'Whether this logo appears in blocks by default or not',
        'sort_order'        => 'The default order this logo appears in blocks'
    ];

    public static $image_sizes  =   [
        'image' => [
            'single'    => [
                'label'     => 'Logo',
                'width'     => 0,
                'height'    => 100,
                'primary'   => true
            ]
        ]
    ];

}