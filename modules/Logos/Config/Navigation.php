<?php
namespace Modules\Logos\Config;

use Adm\Interfaces\NavigationInterface;

class Navigation implements NavigationInterface {

    public function getNavItems(){
        return [
            [ 'title' => 'Logos', 'url' => route('adm.path', 'logos'), 'position' => 60 ]
        ];
    }

}