<?php
namespace Modules\Logos\Theme\Models\PageBuilder;

use App\Models\PageBuilder\BaseBlockModel;
use Illuminate\Database\Eloquent\Collection;
use Modules\Logos\Theme\Models\Logo;

class LogosScrolling extends BaseBlockModel {

    public function getValue( $page_builder_block, $field, $original_value ){
        switch( $field ){
            case 'logos':
                $Logos = new Collection();
                if( !empty( $original_value ) && is_json( $original_value ) ){
                    foreach( (array)json_decode( $original_value, true ) as $raw_logo ){
                        if( empty( $raw_logo['value'] ) ){ continue; }

                        $Logo = Logo::where( 'logo_id', $raw_logo['value'] )->first();
                        if( $Logo ){
                            $Logos->push( $Logo );
                        }
                    }
                }

                if( $Logos->isEmpty() ){
                    $Logos = Logo::where( 'default_in_block', true )->orderBy( 'sort_order' )->get();
                }

                return $Logos;
            default:
                return parent::getValue( $page_builder_block, $field, $original_value );
        }
    }

}