<?php
namespace Modules\Logos\Theme\Models;

use App\Helpers\AssetHelper;
use App\Models\CommonModel;
use App\Models\Scopes\LanguageScope;
use App\Models\Scopes\PublishedScope;
use App\Models\Traits\HasCompositePrimaryKey;
use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Logos\Config\LogoConfig;

class Logo extends CommonModel {
    use SoftDeletes, HasCompositePrimaryKey, HasQueryBuilderCache;

    protected $table        = 'logos';
    protected $primaryKey   = ['logo_id', 'language_id', 'version'];

    protected static function boot(){
        parent::boot();

        static::addGlobalScope( new LanguageScope );
        static::addGlobalScope( new PublishedScope );
    }

    public function getImageAttribute(){
        return AssetHelper::getAssets(
            $this->table,
            $this->logo_id,
            $this->language_id,
            $this->version,
            'image',
            LogoConfig::$image_sizes['image']
        );
    }
}