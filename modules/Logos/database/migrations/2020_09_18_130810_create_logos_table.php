<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'logos', function( Blueprint $table ){
            $table->integer( 'logo_id' );
            $table->integer( 'language_id' );
            $table->integer( 'version' );
            $table->string( 'title' );
            $table->string( 'image' );
            $table->integer( 'sort_order' )->default( 0 );
            $table->boolean( 'default_in_block' )->default( 0 );
            $table->boolean( 'status' )->default( 0 );
            $table->timestamps();
            $table->softDeletes();
            $table->primary( ['logo_id', 'language_id', 'version'], 'logo_pk' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'logos' );
    }
}
