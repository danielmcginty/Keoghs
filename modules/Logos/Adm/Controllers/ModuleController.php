<?php
namespace Modules\Logos\Adm\Controllers;

use Adm\Controllers\AdmController;
use Modules\Logos\Config\LogoConfig;

class ModuleController extends AdmController {

    public function logos(){
        $this->crud->setTable( 'logos' );
        $this->crud->setSubject( 'Logos' );

        $this->crud->columns( 'title', 'image', 'default_in_block', 'sort_order', 'status' );

        $this->crud->columnGroup( 'main', ['title', 'image'] );
        $this->crud->columnGroup( 'side', ['status', 'default_in_block', 'sort_order'] );
    }

    public function getLogosDefaultLabels(){
        return LogoConfig::$labels;
    }

    public function getLogosDefaultHelp(){
        return LogoConfig::$help;
    }

    public function getLogosAssetConfig( $field_name ){
        return isset( LogoConfig::$image_sizes[ $field_name ] ) ? LogoConfig::$image_sizes[ $field_name ] : [];
    }

}