<?php
namespace Modules\Logos\Adm\Models\PageBuilder;

use Adm\Crud\LaraCrud;
use Adm\Models\PageBuilder\PageBuilderBase;
use App\Models\Scopes\LanguageScope;
use Modules\Logos\Theme\Models\Logo;

class LogosStacked extends PageBuilderBase {

    public $display_name    = 'Logos - Stacked';
    public $description     = 'Add a set of logos to the page';

    public $builder_fields  = [
        'logos' => [
            'display_name'  => 'Logos',
            'help'          => 'Select the logos to display, or leave empty to use the default set',
            'type'          => 'orderable_multiple_select',
            'options'       => [],
            'required'      => false,
            'validation'    => [],
            'group'         => 'Main'
        ]
    ];

    public function callbackBeforePrepare( LaraCrud $crud ){
        $opts = [];
        foreach( Logo::withoutGlobalScope( LanguageScope::class )->where( 'language_id', $crud->language_id )->orderBy( 'title' )->get() as $Logo ){
            $opts[ $Logo->logo_id ] = $Logo->title;
        }
        $this->builder_fields['logos']['options'] = $opts;
    }

}