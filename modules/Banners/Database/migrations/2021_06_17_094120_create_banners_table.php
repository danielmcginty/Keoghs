<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->integer('banner_id');
            $table->integer('language_id');
            $table->integer('version');
            $table->string('title');
            $table->text('content')->nullable();
            $table->string('image');
            $table->text('buttons');
            $table->boolean('status')->default(1);
            $table->string('template');
            $table->timestamps();
            $table->softDeletes();
            $table->primary(['banner_id', 'language_id', 'version'], 'banners_pk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
