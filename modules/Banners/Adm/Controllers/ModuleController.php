<?php

namespace Modules\Banners\Adm\Controllers;

use Adm\Controllers\AdmController;
use Modules\Banners\Config\BannerConfig;

class ModuleController extends AdmController
{
    public function banners()
    {
        $this->crud->setTable('banners');
        $this->crud->setSubject('Banners');

        $this->crud->setDefaultValue('status', true);

        $this->crud->orderBy('created_at', 'desc');

        $this->crud->columns('title', 'template', 'status', 'created_at');

        $this->crud->changeType('buttons', 'repeatable');
        $this->crud->changeType('background_colour', 'select');
        $this->crud->fieldOptions('background_colour', BannerConfig::$colour_options);
        $this->crud->changeType('template', 'select');

        $this->crud->fieldOptions('template', [
            'main_banner' => 'Main Banner'
        ]);

        $this->crud->columnGroup(
            'main',
            ['title', 'image', 'background_colour', 'content', 'buttons']
        );
        $this->crud->columnGroup('side', ['status', 'template']);
    }

    public function getBannersDefaultLabels()
    {
        return BannerConfig::$labels;
    }

    public function getBannersDefaultHelp()
    {
        return BannerConfig::$help;
    }

    public function getBannersAssetConfig($field_name)
    {
        return BannerConfig::$image_sizes[$field_name] ?? [];
    }

    public function getBannersRepeatableFieldFields($field)
    {
        switch ($field) {
            case 'buttons':
                return [
                    'text' => [
                        'label' => 'Text',
                        'type' => 'string'
                    ],
                    'link' => [
                        'label' => 'Link',
                        'type' => 'link'
                    ]
                ];
            default:
                return [];
        }
    }
}