<?php

namespace Modules\Banners\Adm\Models\PageBuilder;

use Adm\Crud\LaraCrud;
use Adm\Models\PageBuilder\PageBuilderBase;
use App\Helpers\ColourHelper;
use App\Models\Scopes\LanguageScope;

class Banner extends PageBuilderBase
{
    public $display_name = 'Banner';

    public $description = 'Add a visual banner to the page';

    public $builder_fields = [
        'banner' => [
            'display_name' => 'Banner',
            'help' => 'Select the banner to use',
            'type' => 'select',
            'required' => true,
            'validation' => [
                'required' => 'Please select a banner'
            ],
            'options' => [],
            'group' => 'Main'
        ],
    ];

    public function callbackBeforePrepare(LaraCrud $crud)
    {
        $banners = \Modules\Banners\Theme\Models\Banner::query()
            ->withoutGlobalScope(LanguageScope::class)
            ->where('language_id', $crud->language_id)
            ->get();

        foreach ($banners as $banner) {
            $this->builder_fields['banner']['options'][$banner->banner_id] = $banner->title;
        }
    }
}