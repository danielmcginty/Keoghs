<?php

namespace Modules\Banners\Theme\Models\PageBuilder;

use App\Models\PageBuilder\BaseBlockModel;

class Banner extends BaseBlockModel
{
    public function getValue($page_builder_block, $field, $original_value)
    {
        switch ($field) {
            case 'banner':
                return \Modules\Banners\Theme\Models\Banner::query()->where('banner_id', $original_value)->first();
            default:
                return parent::getValue($page_builder_block, $field, $original_value);
        }
    }
}