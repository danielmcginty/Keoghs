<?php

namespace Modules\Banners\Theme\Models;

use App\Helpers\AssetHelper;
use App\Helpers\BlockHelper;
use App\Models\CommonModel;
use App\Models\Scopes\LanguageScope;
use App\Models\Scopes\PublishedScope;
use App\Models\Traits\HasCompositePrimaryKey;
use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Banners\Config\BannerConfig;

class Banner extends CommonModel
{
    use SoftDeletes, HasCompositePrimaryKey, HasQueryBuilderCache;

    protected $table = 'banners';

    protected $primaryKey = ['banner_id', 'language_id'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new LanguageScope);
        static::addGlobalScope(new PublishedScope);
    }

    public function getImageAttribute()
    {
        return (new AssetHelper())->getAssets(
            $this->table,
            $this->banner_id,
            $this->language_id,
            $this->version,
            'image',
            BannerConfig::$image_sizes['image']
        );
    }

    public function getButtonsAttribute($value)
    {
        return BlockHelper::parseBlockButtonsField($value);
    }
}