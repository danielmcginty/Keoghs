<?php

namespace Modules\Banners\Config;

class BannerConfig
{
    /**
     * @var array
     */
    public static $labels = [
        'title' => 'Title',
        'image' => 'Image',
        'content' => 'Content',
        'buttons' => 'Buttons',
        'template' => 'Template',
        'status' => 'Status',
    ];

    /**
     * @var array
     */
    public static $help = [
        //
    ];

    /**
     * @var array
     */
    public static $image_sizes = [
        'image' => [
            'primary' => [
                'label' => 'Primary',
                'width' => 960,
                'height' => 540,
                'primary' => false
            ]
        ]
    ];

    public static $colour_options = [
        'card-orange'   => 'Orange',
        'card-green'    => 'Green',
        'card-red'      => 'Red',
        'card-blue'     => 'Blue',
        'card-purple'   => 'Purple',
        'pink'          => 'Pink',
    ];
}