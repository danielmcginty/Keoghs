<?php

namespace Modules\Banners\Config;

use Adm\Interfaces\NavigationInterface;

class Navigation implements NavigationInterface
{
    public function getNavItems()
    {
        return [
            [
                'title' => 'Banners',
                'url' => route('adm.path', 'banners'),
                'position' => 70,
            ]
        ];
    }
}