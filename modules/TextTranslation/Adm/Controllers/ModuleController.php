<?php
namespace Modules\TextTranslation\Adm\Controllers;

use Adm\Controllers\AdmController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Validation\Rule;
use Modules\TextTranslation\Adm\Models\TranslationGroup;
use Modules\TextTranslation\Adm\Models\TranslationString;
use Modules\TextTranslation\Adm\Models\TranslationStringTranslation;

class ModuleController extends AdmController {

    /**
     * CRUD Functionality for the Common Text area of the CMS
     */
    public function text_translations(){
        if( $this->crud->request->isMethod('post') ){
            $this->_handleRequest();

            $this->crud->request->session()->flash('success', 'Text changes saved.');
        }

        $this->crud->setTable('translation_groups');
        $this->crud->setSubject('Text Translations');

        $data = [];

        $groups = TranslationGroup::where('id', '!=', 3)->with('strings')->orderBy( 'sort_order' )->get();
        $data['groups'] = $groups;

        $languages_raw = \DB::table('languages')->get();
        $languages = [];
        $default_language = false;
        foreach( $languages_raw as $ind => $language ){
            if( $language->id == env('DEFAULT_LANGUAGE') ){
                $default_language = $language;
            } else {
                $languages[] = $language;
            }
        }
        $data['languages'] = $languages;
        $data['default_language'] = $default_language;

        $data['title'] = sizeof( $languages ) < 1 ? 'Common Text' : 'Common Text Translations';

        $data['super_admin'] = \Auth::user()->userGroup->super_admin;

        view()->addNamespace('TextTranslation', base_path('/modules/TextTranslation/Adm/views'));
        $this->crud->setView('TextTranslation::translations', $data);
    }

    /**
     * CRUD Functionality for the Common Text 'Groups' in the CMS
     */
    public function translation_group(){
        if( $this->crud->getAction() == "listing" || !\Auth::user()->userGroup->super_admin ){
            \App::abort( 301, '', ['Location' => route('adm.path', ['table' => 'text_translations'])] );
        }

        $this->crud->setTable('translation_groups');
        $this->crud->setSubject('Text Translation Groups');

        $this->crud->help('name', 'The name of the group of common text');
    }

    /**
     * CRUD Functionality for singular Common Text elements in the CMS
     */
    public function translation_string(){
        if( $this->crud->getAction() == "listing" || !\Auth::user()->userGroup->super_admin ){
            \App::abort( 301, '', ['Location' => route('adm.path', ['table' => 'text_translations'])] );
        }

        $this->crud->setTable('translation_strings');
        $this->crud->setSubject('Common Text Strings');

        $this->crud->displayAs('group_id', 'Translation Group');
        $this->crud->setRelation('group_id', 'translation_groups', 'name', 'id', null);
        $this->crud->help('group_id', 'Which group of text does this string belong to?');

        $key_help = 'The internal reference for the string, used on the front end to display the text';
        if( $this->crud->getAction() == 'edit' ){
            $key_help .= '. <span class="block colour-warning">Don\'t forget to update any reference to this in the code if the key is changed.</span>';
        }
        $this->crud->help('key', $key_help);

        $this->crud->help('name', 'The name for this piece of text, shown to users in the CMS');

        $this->crud->help('description', 'What is this piece of text for?');

        $this->crud->changeType( 'input_type', 'select' );
        $this->crud->fieldOptions( 'input_type', ['text' => 'Standard text input', 'textarea' => 'Multiline text input', 'wysiwyg' => 'WYSIWYG Editor']);
        $this->crud->help('input_type', 'What type of input if this string?');
    }

    /**
     * Callback Function for Validation Rules.
     * When saving a common text string, ensure that the key is unique (ignoring any deleted strings).
     *
     * @param $rules
     * @param $inputs
     */
    public function getTranslationStringValidationRules( &$rules, $inputs ){
        $unique_key_rule = Rule::unique( 'translation_strings' );
        if( !empty( $this->crud->parameters['id'] ) ){
            $currentItem = $this->crud->getItem();
            if( $currentItem ){
                $unique_key_rule->ignore( $currentItem->id );
            }
        }

        $unique_key_rule->where( function( $query ){
            $query->whereNull( 'deleted_at' );
        });

        $rules['key'] = $unique_key_rule;
    }

    /**
     * Callback Function for Validation Messages.
     * Apply custom validation rules for common text strings.
     *
     * @param $messages
     * @param $inputs
     */
    public function getTranslationStringValidationMessages( &$messages, $inputs ){
        $messages['key.unique'] = 'That key has been taken. Please use something different.';
    }

    /**
     * CRUD Functionality for a subset of Common Text elements, related to Social Media, in the CMS
     */
    public function social_media(){
        if( $this->crud->request->isMethod('post') ){
            $this->_handleRequest();

            $this->crud->request->session()->flash('success', 'Text changes saved.');
            \App::abort( 301, '', ['Location' => route('adm.path', ['social-media'])] );
        }

        $this->crud->setTable('translation_groups');
        $this->crud->setSubject('Social Media Settings');

        $data = [];

        $groups = TranslationGroup::where('id', 3)->with('strings')->get();
        $data['groups'] = $groups;

        $languages_raw = \DB::table('languages')->get();
        $languages = [];
        $default_language = false;
        foreach( $languages_raw as $ind => $language ){
            if( $language->id == env('DEFAULT_LANGUAGE') ){
                $default_language = $language;
            } else {
                $languages[] = $language;
            }
        }
        $data['languages'] = $languages;
        $data['default_language'] = $default_language;

        $data['title'] = 'Social Media Settings';

        $data['super_admin'] = \Auth::user()->userGroup->super_admin;

        view()->addNamespace('TextTranslation', base_path('/modules/TextTranslation/Adm/views'));
        $this->crud->setView('TextTranslation::translations', $data);
    }

    /**
     * Custom Function used to save Common Text strings from the custom form page.
     */
    private function _handleRequest(){
        $inputs = $this->crud->request->input();

        foreach( $inputs as $string_key => $translations ){
            if( !is_array( $translations ) ){ continue; }

            foreach( $translations as $language_id => $text_translation ){
                $translation = TranslationStringTranslation::where([
                    'key' => $string_key,
                    'language_id' => $language_id
                ])->first();

                if( $translation ){
                    $translation->translation = $text_translation;
                    $translation->save();
                } else {
                    $translation = new TranslationStringTranslation();

                    $translation->key = $string_key;
                    $translation->language_id = $language_id;
                    $translation->translation = $text_translation;
                }

                $translation->save();
            }
        }

        Cache::flush();
    }

    /**
     * AJAX Function used to display a warning message when deleting a Common Text Group.
     *
     * @param $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ajax_get_delete_group_modal( $request ){
        $group_id = $request->input('group_id') ? $request->input('group_id') : false;

        $group = $group_id;
        if( $group_id !== false ) {
            $group = TranslationGroup::find($group_id);
        }

        view()->addNamespace('TextTranslation', base_path('/modules/TextTranslation/Adm/views'));
        return view('TextTranslation::modals.delete-group-modal', ['group' => $group] );
    }

    /**
     * AJAX Function used to display a warning message when deleting a Common Text String.
     *
     * @param $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ajax_get_delete_string_modal( $request ){
        $string_id = $request->input('string_id') ? $request->input('string_id') : false;

        $string = $string_id;
        if( $string_id !== false ) {
            $string = TranslationString::find($string_id);
        }

        view()->addNamespace('TextTranslation', base_path('/modules/TextTranslation/Adm/views'));
        return view('TextTranslation::modals.delete-string-modal', ['string' => $string] );
    }

    /**
     * AJAX Function used to delete a Common Text Group.
     *
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Throwable
     */
    public function ajax_delete_translation_group( $request ){
        $group_id = $request->input('group_id') ? $request->input('group_id') : false;

        if( $group_id === false ){
            $flash = view( 'includes.flash', ['error_message' => 'Sorry, you didn\'t provide a group!'] )->render();
            return response()->json( ['success' => false, 'flash_notification' => $flash] );
        }

        $group = TranslationGroup::find( $group_id );

        if( !$group ){
            $flash = view( 'includes.flash', ['error_message' => 'Sorry, we couldn\'t find that group!'] )->render();
            return response()->json( ['success' => false, 'flash_notification' => $flash] );
        }

        foreach( $group->strings as $string ){
            foreach( $string->translations as $translation ){
                $translation->delete();
            }

            $string->delete();
        }

        \Session::flash('success', $group->name . ' successfully deleted.');

        $group->delete();

        return response()->json( ['success' => true] );
    }

    /**
     * AJAX Function used to delete a Common Text String.
     *
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Throwable
     */
    public function ajax_delete_translation_string( $request ){
        $string_id = $request->input('string_id') ? $request->input('string_id') : false;

        if( $string_id === false ){
            $flash = view( 'includes.flash', ['error_message' => 'Sorry, you didn\'t provide a string!'] )->render();
            return response()->json( ['success' => false, 'flash_notification' => $flash] );
        }

        $string = TranslationString::find( $string_id );

        if( !$string ){
            $flash = view( 'includes.flash', ['error_message' => 'Sorry, we couldn\'t find that string!'] )->render();
            return response()->json( ['success' => false, 'flash_notification' => $flash] );
        }

        foreach( $string->translations as $translation ){
            $translation->delete();
        }

        \Session::flash('success', $string->name . ' successfully deleted.');

        $string->delete();

        return response()->json( ['success' => true] );
    }
}