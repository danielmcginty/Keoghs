<?php
namespace Modules\TextTranslation\Adm\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TranslationGroup extends Model {
    // As we've got a deleted_at flag, include the relevant trait
    use SoftDeletes;

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'translation_groups';

    /**
     * The fields that are 'bulk assignable' for this model
     * @var array
     */
    protected $fillable = ['name', 'sort_order'];

    /**
     * Eloquent Relationship.
     * Returns the set of Common Text Strings assigned to this Group.
     *
     * @return mixed
     */
    public function strings(){
        return $this->hasMany('\\Modules\\TextTranslation\\Adm\\Models\\TranslationString', 'group_id')->with('translations')->orderBy( 'sort_order' );
    }
}