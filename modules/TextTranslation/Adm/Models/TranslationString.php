<?php
namespace Modules\TextTranslation\Adm\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TranslationString extends Model {
    // As we've got a deleted_at flag, include the relevant trait
    use SoftDeletes;

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'translation_strings';

    /**
     * The fields that are 'bulk assignable' for this model
     * @var array
     */
    protected $fillable = ['group_id', 'key', 'name', 'description', 'input_type', 'sort_order'];

    /**
     * Eloquent Relationship.
     * Returns the Common Text Group this String belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group(){
        return $this->belongsTo('\\Modules\\TextTranslation\\Adm\\Models\\TranslationGroup', 'group_id');
    }

    /**
     * Eloquent Relationships.
     * Returns all translations for this Common Text String.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translations(){
        return $this->hasMany('\\Modules\\TextTranslation\\Adm\\Models\\TranslationStringTranslation', 'key', 'key');
    }

    /**
     * Quick function used to retrieve a translated version of this string for a particular language.
     *
     * @param $language_id
     * @return string
     */
    public function getTranslation( $language_id ){
        $translation = TranslationStringTranslation::where([
            'key' => $this->key,
            'language_id' => $language_id
        ])->first();

        return ($translation) ? $translation->translation : '';
    }
}