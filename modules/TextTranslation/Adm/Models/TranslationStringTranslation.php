<?php
namespace Modules\TextTranslation\Adm\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\HasCompositePrimaryKey;
use App\Models\Traits\CompositeSoftDeletes;

class TranslationStringTranslation extends Model {
    // As we've got a deleted_at flag, a composite primary key, include the relevant traits
    use HasCompositePrimaryKey, SoftDeletes;

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'translation_string_translations';

    /**
     * The primary keys in the table
     * @var array
     */
    protected $primaryKey = ['key', 'language_id'];

    /**
     * The fields that are 'bulk assignable' for this model
     * @var array
     */
    protected $fillable = ['key', 'language_id', 'translation'];

    /**
     * Eloquent Relationship.
     * Returns the Common Text String this translation is for.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function string(){
        return $this->belongsTo('\\Modules\\TextTranslation\\Adm\\Models\\TranslationString');
    }

    /**
     * Override the softDelete function so that it doesn't fall over with a
     * composite primary key
     */
    protected function runSoftDelete(){
        $query = $this->newQueryWithoutScopes();
        foreach( $this->getKeyName() as $key ){
            if( $this->$key ){
                $query->where( $key, '=', $this->$key );
            }
        }

        $this->{$this->getDeletedAtColumn()} = $time = $this->freshTimestamp();

        $query->update([$this->getDeletedAtColumn() => $this->fromDateTime($time)]);
    }
}