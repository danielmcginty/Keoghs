<div class="js-modal-form-flash top-notification relative top-0 left-0 width-100 text-center">
    <div class="alert-box alert alert-dismissable margin-b0 bottom-0" style="display: none;"></div>
    <div class="alert-box success alert-dismissable margin-b0 bottom-0" style="display: none;"></div>
</div>

<div class="modal-content">
    <p class="modal-heading padding-r40 font-weight-600 margin-b10">Delete the translation group "{{ $group->name }}"?</p>
    <p class="font-size-14">Permanently delete <strong>all common text &amp; translations</strong> in this group? This action <strong>cannot</strong> be reversed.</p>
</div>
<div class="modal-actions">
    <a href="#" data-confirm-delete-group data-group-id="{{ $group->id }}" class="button round gradient bordered colour-error margin-b0 margin-r10">Delete Translation Group</a>
    <a href="#" data-close-reveal class="button round bordered colour-cancel margin-b0">Cancel</a>
</div>
<a class="close-reveal-modal visually-hidden" aria-label="Close">&#215;</a>