@extends( 'layouts.adm' )

@section('page_title', $title)

@section('content')

    {!! Form::open(['url' => route('adm.path', ['table' => $table]), 'id' => 'translation-form']) !!}
        <div class="js-section-action-bar-container relative" style="height: 79px;">
            <div class="js-section-action-bar section-action-bar top-0 left-0 width-100">
                <div class="row padding-y-gutter-full">
                    <div class="column">
                        <div class="table width-100">
                            <div class="table-cell">
                                <h1 class="font-size-24 font-weight-600 margin-0">{{ $title }}</h1>
                            </div>
                            <div class="table-cell text-right">
                                @if( $super_admin )
                                    <a href="{{ route('adm.path', ['table' => 'translation_group', 'action' => 'create']) }}" class="button round bordered colour-default margin-b0 margin-l-gutter margin-b0">Add String Group</a>
                                    <a href="{{ route('adm.path', ['table' => 'translation_string', 'action' => 'create']) }}" class="button round bordered colour-default margin-b0 margin-l-gutter">Add Text String</a>
                                @endif
                                <input type="submit" class="button round bordered colour-success margin-b0 margin-l-gutter" value="Save" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-area margin-b-gutter-full">
            <div class="row">

                <div class="column small-9">

                    <div class="edit-main-content section-container background colour-white radius margin-b-gutter-full">

                        @if( sizeof( $groups ) > 0 )

                            <?php
                                if( sizeof( $groups ) == 1 ){
                                    $firstGroup = $groups[0];
                                    $tabTitle = $firstGroup->name;
                                } else {
                                    $tabTitle = 'Common Text';
                                }
                            ?>

                            <div data-content-tabs class="edit-content-tabs">
                                <div class="table width-100">
                                    <div class="table-cell width-100">
                                        <ul data-tab-group="edit-main-content" class="tabs simple-list inline-list font-size-0">
                                            <li>
                                                <a href="#" data-tab-id="content" title="Edit {{ $tabTitle }}" class="active font-size-14 block padding-x-gutter-full padding-y-gutter">
                                                    {{ $tabTitle }}
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div data-tab-group="edit-main-content" class="tabs-content">

                                <div id="content" data-tab-id="content" class="content active">
                                    @foreach( $groups as $ind => $group )
                                        <div class="input-row padding-gutter-full">

                                            <h4>
                                                {{ $group->name }}
                                                @if( $super_admin )
                                                    <a href="{{ route('adm.path', ['translation_group', 'edit', $group->id]) }}" class="inline-block font-size-14 margin-l-gutter" title="Edit Group">Edit</a>
                                                    <a href="#" data-delete-translation-group data-group-id="{{ $group->id }}" class="inline-block font-size-14 margin-l-gutter colour-error">Delete</a>
                                                @endif
                                            </h4>

                                            <div class="row font-size-14 font-weight-600 margin-b-gutter-full">
                                                <div class="column small-3">
                                                    Text
                                                </div>
                                                <div class="column small-9">
                                                    <div class="table width-100">
                                                        <div class="table-cell vertical-top width-50 padding-r-gutter">
                                                            {{ $default_language->title }} [{{ $default_language->locale }}] <span class="colour-grey-light font-weight-400">(Default)</span>
                                                        </div>
                                                        <div class="table-cell vertical-top width-50 padding-l-gutter">
                                                            @foreach( $languages as $language )
                                                                <div data-language-field="{{ $language->locale }}" class="none">{{ $language->title }} [{{ $language->locale }}]</div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            @if( sizeof( $group->strings ) > 0 )

                                                @foreach( $group->strings as $string )

                                                    <div class="row margin-b-gutter-full">
                                                        <div class="column small-3 font-size-14">
                                                            <h6 class="margin-b0 font-size-14">{{ $string->name }}</h6>
                                                            <div class="colour-grey">{{ $string->description }}</div>
                                                            @if( $super_admin )
                                                                <code class="inline-block font-size-12 colour-grey margin-t2">{{ $string->key }}</code>
                                                                <span class="inline-block margin-t2 margin-l-gutter">
                                                                    <a href="{{ route('adm.path', ['translation_string', 'edit', $string->id]) }}" class="inline-block margin-r-gutter" title="Edit String">Edit</a>
                                                                    <a href="#" data-delete-translation-string data-string-id="{{ $string->id }}" class="inline-block font-size-14 colour-error">Delete</a>
                                                                </span>
                                                            @endif
                                                        </div>
                                                        <div class="column small-9">
                                                            <div class="table width-100">
                                                                <div class="table-cell vertical-top width-50 padding-r-gutter">
                                                                    <label for="{{ $default_language->locale }}_{{ $string->key }}" class="visually-hidden">{{ $default_language->title }}</label>
                                                                    @if( $string->input_type == 'wysiwyg' )
                                                                        <div class="tinymce-wrap">
                                                                            <textarea id="{{ $default_language->locale }}_{{ $string->key }}" name="{{ $string->key }}[{{ $default_language->id }}]" class="visually-hidden" rows="0" cols="0" data-auto-expand data-tinymce data-mce-config="default">{{ $string->getTranslation( $default_language->id ) }}</textarea>
                                                                        </div>
                                                                    @elseif( $string->input_type == 'textarea' )
                                                                        <textarea id="{{ $default_language->locale }}_{{ $string->key }}" name="{{ $string->key }}[{{ $default_language->id }}]" class="margin-0 resize-vertical" rows="0" cols="0" style="min-height:0;max-height:200px;">{{ $string->getTranslation( $default_language->id ) }}</textarea>
                                                                    @else
                                                                        <input type="text" id="{{ $default_language->locale }}_{{ $string->key }}" name="{{ $string->key }}[{{ $default_language->id }}]" value="{{ $string->getTranslation( $default_language->id ) }}" class="margin-0" />
                                                                    @endif
                                                                </div>
                                                                <div class="table-cell vertical-top width-50 padding-l-gutter">
                                                                    @foreach( $languages as $language )
                                                                        <div data-language-field="{{ $language->locale }}" class="none">
                                                                            <label for="{{ $language->locale }}_{{ $string->key }}" class="visually-hidden">{{ $language->title }}</label>
                                                                            @if( $string->input_type == 'wysiwyg' )
                                                                                <div class="tinymce-wrap">
                                                                                    <textarea id="{{ $language->locale }}_{{ $string->key }}" name="{{ $string->key }}[{{ $language->id }}]" class="visually-hidden" rows="0" cols="0" data-auto-expand data-tinymce data-mce-config="default">{{ $string->getTranslation( $language->id ) }}</textarea>
                                                                                </div>
                                                                            @elseif( $string->input_type == 'textarea' )
                                                                                <textarea id="{{ $language->locale }}_{{ $string->key }}" name="{{ $string->key }}[{{ $language->id }}]" class="margin-0 resize-vertical" rows="0" cols="0" style="min-height:0;max-height:200px;">{{ $string->getTranslation( $language->id ) }}</textarea>
                                                                            @else
                                                                                <input type="text" id="{{ $language->locale }}_{{ $string->key }}" name="{{ $string->key }}[{{ $language->id }}]" value="{{ $string->getTranslation( $language->id ) }}" class="margin-0" />
                                                                            @endif
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                @endforeach

                                            @else

                                                <div class="row font-size-14 font-weight-400 margin-b-gutter-full">
                                                    <div class="column">
                                                        <span class="block padding-y-gutter">No common text found.</span>
                                                    </div>
                                                </div>

                                            @endif
                                        </div>
                                    @endforeach
                                </div>

                            </div>

                        @else

                            <span class="block padding-gutter">No common text found.</span>

                        @endif
                    </div>

                </div>

                <div class="column small-3">

                    <div class="language-select section-container background colour-white radius margin-b-gutter-full font-size-14 overflow-hidden">
                        <ul class="simple-list margin-0">
                            <li>
                                <a href="#" data-language-id="{{ $default_language->locale }}" title="Select {{ $default_language->title }}" class="button colour-white block text-left margin-0 current">
                                    <span class="">
                                        {{ $default_language->title }}
                                        <span class="colour-grey-light font-weight-400">(Default)</span>
                                    </span>
                                </a>
                            </li>
                            @foreach( $languages as $language )
                                <li>
                                    <a href="#" data-language-id="{{ $language->locale }}" title="Select {{ $language->title }}" class="button colour-white block text-left margin-0">
                                        <span class="">{{ $language->title }}</span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                </div>

            </div>

        </div>
    {!! Form::close() !!}

    <div id="delete-group-modal" class="reveal-modal notice-modal" data-reveal aria-hidden="true" role="dialog">
        <!-- Form gets ajax-ed into here -->
    </div>

    <div id="delete-string-modal" class="reveal-modal notice-modal" data-reveal aria-hidden="true" role="dialog">
        <!-- Form gets ajax-ed into here -->
    </div>

    <div id="language-switch-confirm-modal" class="reveal-modal notice-modal" data-reveal aria-hidden="true" role="dialog">
        <div class="modal-content">
            <p class="modal-heading padding-r40 font-weight-600 margin-b10">Are you sure you want to edit <span data-language-name>Language</span>?</p>
            <p>If you switch language you might lose your changes. Are you sure you want to switch to <span data-language-name>Language</span>?</p>
        </div>
        <div class="modal-actions">
            <a href="#" data-confirm-language-switch class="button round bordered colour-success margin-b0 margin-r10">Yes, Edit <span data-language-name>Language</span></a>
            <a href="#" data-deny-language-switch class="button round bordered colour-cancel margin-b0">Cancel</a>
        </div>
    </div>

@endsection

@push('footer_scripts')
<script>
    var formSubmitting = false;
    var edited = false;

    $('form').on('submit', function () {
        formSubmitting = true;
    });

    $('form').on('change', function () {
        edited = true;
    });

    window.onload = function() {
        window.addEventListener("beforeunload", function (e) {

            if (formSubmitting || !edited) {
                return undefined;
            }

            var confirmationMessage = 'It looks like you have been editing something. '
                    + 'If you leave before saving, your changes will be lost.';

            (e || window.event).returnValue = confirmationMessage; //Gecko + IE
            return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
        });
    };
</script>

<script type="text/javascript">

$(document).ready(function () {
    // Internal flag denoting if a common text translation has changed.
    var translations_changed = false;

    // If we change a language specific common text translation, update our variable to true
    $(document).on('change', '[data-language-field] > input', function(){
        translations_changed = true;
    });

    $('[data-language-id]').on('click', function(e) {
        e.preventDefault();

        // Set a variable for our language link
        var $languageLink = $(this);

        // If we've updated any text translations, ask the user if they want to switch language.
        // If not, the can_change variable will switch to false
        if( translations_changed ){
            var $confirmDiv = $('#language-switch-confirm-modal');

            $confirmDiv.find('[data-confirm-language-switch]').on('click', function(e){
                e.preventDefault();

                switch_language( $languageLink );
                $confirmDiv.foundation('reveal', 'close');
            });

            $confirmDiv.find('[data-deny-language-switch]').on('click', function(e){
                e.preventDefault();

                $confirmDiv.foundation('reveal', 'close');
            });

            var language = $languageLink.find('span').text();

            $confirmDiv.find('[data-language-name]').text( language );

            $confirmDiv.foundation('reveal', 'open');

        } else {

            switch_language( $languageLink );

        }

    });

    function switch_language( $languageLink ){
        // Get language code
        var langCode = $languageLink.data('language-id');

        // Disable the hidden language fields
        $('[data-language-field]').find('input, textarea').attr('disabled', 'disabled');
        $('[data-language-field=' + langCode + ']').find('input, textarea').removeAttr('disabled');

        // Show correct language fields
        $('[data-language-field]').addClass('none');
        $('[data-language-field=' + langCode + ']').removeClass('none');

        // Update toggles current state
        $('[data-language-id]').removeClass('current');
        $languageLink.addClass('current');
    }

});

</script>

<?php // DELETE STRINGS & GROUPS ?>
<script type="text/javascript">
$(document).ready( function(){
    $(document).on('click', '[data-delete-translation-group]', function( evt ){
        evt.preventDefault();
        evt.stopPropagation();

        var group_id = $(this).data('group-id');

        $('#delete-group-modal').foundation('reveal', 'open', {
            url: '{{ route('adm.ajax', ['ajax_get_delete_group_modal']) }}',
            type: 'GET',
            data: {
                group_id: group_id
            }
        });
    });

    $(document).on('click', '#delete-group-modal [data-confirm-delete-group]', function( evt ){
        evt.preventDefault();
        evt.stopPropagation();

        var group_id = $(this).data('group-id');

        $.ajax({
            url: '{{ route('adm.ajax', ['ajax_delete_translation_group']) }}',
            type: 'POST',
            data: {
                group_id: group_id
            },
            dataType: 'json',
            beforeSend: function(){
                showPageLoadingOverlay( true );
                $('.admin-container .top-notification').first().remove();
            },
            success: function( json ){
                if( json.success ){
                    window.location.reload();
                } else {
                    if (json.flash_notification) {
                        $('.admin-container').prepend(json.flash_notification);
                        $('.admin-container .top-notification').first().css('zIndex', 1005);
                    }
                }
            },
            complete: function(){
                showPageLoadingOverlay();
            }
        });
    });

    $(document).on('click', '[data-delete-translation-string]', function( evt ){
        evt.preventDefault();
        evt.stopPropagation();

        var string_id = $(this).data('string-id');

        $('#delete-string-modal').foundation('reveal', 'open', {
            url: '{{ route('adm.ajax', ['ajax_get_delete_string_modal']) }}',
            type: 'GET',
            data: {
                string_id: string_id
            }
        });
    });

    $(document).on('click', '#delete-string-modal [data-confirm-delete-string]', function( evt ){
        evt.preventDefault();
        evt.stopPropagation();

        var string_id = $(this).data('string-id');

        $.ajax({
            url: '{{ route('adm.ajax', ['ajax_delete_translation_string']) }}',
            type: 'POST',
            data: {
                string_id: string_id
            },
            dataType: 'json',
            beforeSend: function(){
                showPageLoadingOverlay( true );
                $('.admin-container .top-notification').first().remove();
            },
            success: function( json ){
                if( json.success ){
                    window.location.reload();
                } else {
                    if (json.flash_notification) {
                        $('.admin-container').prepend(json.flash_notification);
                        $('.admin-container .top-notification').first().css('zIndex', 1005);
                    }
                }
            },
            complete: function(){
                showPageLoadingOverlay();
            }
        });
    });
});
</script>

<?php // TINYMCE ?>
<script src="/admin/dist/assets/plugins/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    var TINYMCE_CONFIG_SETS = [];
</script>
<script src="/admin/dist/assets/plugins/tinymce/tinymce.config.js"></script>
@include('dynamic-scripts.tinymce.tinymce-configs')
<script type="text/javascript">
    function initialize_tinymce(){
        for( var i = 0; i < TINYMCE_CONFIG_SETS.length; i++ ){
            var config = TINYMCE_CONFIG_SETS[ i ];
            tinymce.init( config );
        }
    }
    // Destroy all instances of TinyMCE editors
    function rebuild_tinymce(){
        // Because tinymce doesn't have a destroy() function, we need to manually clear the editor instances that
        // it current has
        tinymce.EditorManager.editors = [];

        // Now the instances are removed, we can reinitialize TinyMCE with the configurations again
        initialize_tinymce();
    }
    initialize_tinymce();
</script>
@endpush