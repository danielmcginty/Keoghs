<?php
namespace Modules\TextTranslation\Theme\Models;

use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\Model;

class TranslationString extends Model {
    // As we want to cache DB calls, include the relevant trait
    use HasQueryBuilderCache;

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'translation_strings';

    /**
     * The fields that are 'bulk assignable' for this model
     * @var array
     */
    protected $fillable = ['group_id', 'key', 'name', 'description'];

    /**
     * Quick function used to retrieve a translated version of this string for the current language.
     *
     * @return string
     */
    public function getTranslation(){
        $translation = TranslationStringTranslation::where([
            'key' => $this->key
        ])->first();

        return ($translation) ? $translation->translation : '';
    }
}