<?php
namespace Modules\TextTranslation\Theme\Models;

use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\HasCompositePrimaryKey;
use App\Models\Scopes\LanguageScope;

class TranslationStringTranslation extends Model {
    // As we've got a composite primary key, and want to cache DB calls, include the relevant traits
    use HasCompositePrimaryKey, HasQueryBuilderCache;

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'translation_string_translations';

    /**
     * The primary keys in the table
     * @var array
     */
    protected $primaryKey = ['key', 'language_id'];

    /**
     * The fields that are 'bulk assignable' for this model
     * @var array
     */
    protected $fillable = ['key', 'language_id', 'translation'];

    /**
     * When 'booting' this model, we only want translations in the correct language so add the as global scope
     */
    protected static function boot(){
        parent::boot();

        static::addGlobalScope(new LanguageScope);
    }
}