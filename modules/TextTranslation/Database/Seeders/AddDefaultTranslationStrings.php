<?php
namespace Modules\TextTranslation\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Modules\TextTranslation\Adm\Models\TranslationString;
use Modules\TextTranslation\Adm\Models\TranslationGroup;

class AddDefaultTranslationStrings extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // Website Defaults
        $websiteDefaults = new TranslationGroup;
        $websiteDefaults->name = "Website Defaults";
        $websiteDefaults->sort_order = 0;
        $websiteDefaults->save();

        $websiteDefaultStrings = [
        [
            "group_id" => $websiteDefaults->id,
            "key" => "meta_title",
            "name" => "Meta Title",
            "description" => "The default, fallback meta title",
            "input_type" => "text",
            "sort_order" => 0
        ],[
            "group_id" => $websiteDefaults->id,
            "key" => "meta_description",
            "name" => "Meta Description",
            "description" => "The default, fallback meta description",
            "input_type" => "textarea",
            "sort_order" => 10
        ],[
            "group_id" => $websiteDefaults->id,
            "key" => "telephone_number",
            "name" => "Telephone Number",
            "description" => "The website telephone number",
            "input_type" => "text",
            "sort_order" => 20
        ],[
            "group_id" => $websiteDefaults->id,
            "key" => "website_email",
            "name" => "Website Email",
            "description" => "The main website email address",
            "input_type" => "text",
            "sort_order" => 30
        ],[
            "group_id" => $websiteDefaults->id,
            "key" => "copyright_text",
            "name" => "Footer Copyright Text",
            "input_type" => "text",
            "sort_order" => 40
        ]];

        foreach ($websiteDefaultStrings as $string) {
            TranslationString::create($string);
        }


        // Address
        $address = new TranslationGroup;
        $address->name = "Address";
        $address->sort_order = 10;
        $address->save();

        $addressStrings = [[
            "group_id" => $address->id,
            "key" => "address_company_name",
            "name" => "Company Name",
            "description" => "Optional",
            "input_type" => "text",
            "sort_order" => 0
        ],[
            "group_id" => $address->id,
            "key" => "address_line_one",
            "name" => "First Line of Address",
            "input_type" => "text",
            "sort_order" => 10
        ],[
            "group_id" => $address->id,
            "key" => "address_line_two",
            "name" => "Second Line of Address",
            "description" => "Optional",
            "input_type" => "text",
            "sort_order" => 20
        ],[
            "group_id" => $address->id,
            "key" => "address_city",
            "name" => "City",
            "input_type" => "text",
            "sort_order" => 30
        ],[
            "group_id" => $address->id,
            "key" => "address_county",
            "name" => "County",
            "input_type" => "text",
            "sort_order" => 40
        ],[
            "group_id" => $address->id,
            "key" => "address_postcode",
            "name" => "Postcode",
            "input_type" => "text",
            "sort_order" => 50
        ],[
            "group_id" => $address->id,
            "key" => "address_country",
            "name" => "Country",
            "input_type" => "text",
            "sort_order" => 60
        ]];

        foreach ($addressStrings as $string) {
            TranslationString::create($string);
        }


        // Social Settings
        $socialSettings = new TranslationGroup;
        $socialSettings->name = "Social Settings";
        $socialSettings->sort_order = 0;
        $socialSettings->save();

        $socialSettingsStrings = [[
            "group_id" => $socialSettings->id,
            "key" => "social_facebook",
            "name" => "Facebook Link",
            "input_type" => "text",
            "sort_order" => 0
        ],[
            "group_id" => $socialSettings->id,
            "key" => "social_twitter",
            "name" => "Twitter Link",
            "input_type" => "text",
            "sort_order" => 10
        ],[
            "group_id" => $socialSettings->id,
            "key" => "social_linkedin",
            "name" => "LinkedIn Link",
            "input_type" => "text",
            "sort_order" => 20
        ],[
            "group_id" => $socialSettings->id,
            "key" => "social_youtube",
            "name" => "YouTube Link",
            "input_type" => "text",
            "sort_order" => 30
        ],[
            "group_id" => $socialSettings->id,
            "key" => "social_google_plus",
            "name" => "Google+ Link",
            "input_type" => "text",
            "sort_order" => 40
        ],[
            "group_id" => $socialSettings->id,
            "key" => "social_pinterest",
            "name" => "Pinterest",
            "input_type" => "text",
            "sort_order" => 50
        ],[
            "group_id" => $socialSettings->id,
            "key" => "social_instagram",
            "name" => "Instagram",
            "input_type" => "text",
            "sort_order" => 60
        ],[
            "group_id" => $socialSettings->id,
            "key" => "twitter_username",
            "name" => "Twitter Username (Handle)",
            "description" => "Twitter Username to be used in by the site in links and widgets.",
            "input_type" => "text",
            "sort_order" => 70
        ]];

        foreach ($socialSettingsStrings as $string) {
            TranslationString::create($string);
        }


        // Maintenance Page
        $maintenancePage = new TranslationGroup;
        $maintenancePage->name = "Maintenance Page";
        $maintenancePage->sort_order = 99;
        $maintenancePage->save();

        $maintenancePageStrings = [[
            "group_id" => $maintenancePage->id,
            "key" => "503_title",
            "name" => "Title",
            "description" => "The message shown to the user when the website is down for maintenance.",
            "input_type" => "text",
            "sort_order" => 0
        ]];

        foreach ($maintenancePageStrings as $string) {
            TranslationString::create($string);
        }

        Model::reguard();
    }
}
