<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTextTranslationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translation_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->integer( 'sort_order' )->default( 0 );
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('translation_strings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id');
            $table->string('key');
            $table->string('name');
            $table->string('description');
            $table->integer( 'sort_order' )->default( 0 );
            $table->index('group_id');
            $table->index('key');
            $table->timestamps();
            $table->softDeletes();
            $table->unique( ['key', 'deleted_at'], 'translation_strings_key_unique' );
        });

        Schema::create('translation_string_translations', function (Blueprint $table) {
            $table->string('key');
            $table->integer('language_id');
            $table->string('translation');
            $table->unique(['key', 'language_id']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('translation_groups');
        Schema::dropIfExists('translation_strings');
        Schema::dropIfExists('translation_string_translations');
    }
}
