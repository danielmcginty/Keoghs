<?php
namespace Modules\CaseStudies\Adm\Controllers;

use Adm\Controllers\AdmController;
use App\Models\Url;
use Modules\CaseStudies\Config\CaseStudiesConfig;
use Modules\CaseStudies\Config\CaseStudyCategoriesConfig;
use Modules\Page\Config\PageConfig;

class ModuleController extends AdmController {

    /**
     * CRUD Functionality for the 'case_studies' table
     */
    public function case_studies(){
        // Just in case, define which database table we're reading from.
        $this->crud->setTable( 'case_studies' );

        // And manually set a 'Subject', which is a human readable version of this resource type.
        $this->crud->setSubject( 'Case Studies' );

        // Case Studies have a URL, so we need to set that up
        $this->crud->addColumn( 'path' );

        /*
         * And the URL should contain the category, so set that up
         *
         * Here, we're saying that the path should use the URL from the current case study's case_study_category_id.
         * This is then referencing the 'case_study_categories' table, using the 'case_study_category_id' field
         */
        $this->crud->setPathPrefix( 'case_study_categories', 'case_study_category_id', 'case_study_category_id' );
        $this->crud->metaDescriptionGenerationField( 'excerpt' );

        /*
         * As the case_study_category_id field is a referential field to a Case Study Category, define that
         * So, we're using the case_study_category_id field, referencing the case_study_categories table, pulling back
         * the title field, and using the case_study_category_id as the foreign/primary key
         */
        $this->crud->setRelation( 'case_study_category_id', 'case_study_categories', 'title', 'case_study_category_id', '' );

        /*
         * Make the excerpt field a textarea instead of a WYSIWYG
         */
        $this->crud->changeType('excerpt', 'textarea');

        /*
         * Case Study pages should be built up using the page builder, so state that that is the case.
         * We also need to say that the new 'page_builder' field is of the Page Builder field type.
         * And we also need to say that the page builder is enabled
         */
        $this->crud->addColumn( 'page_builder' );
        $this->crud->changeType( 'page_builder', 'page_builder' );
        $this->crud->enablePageBuilder();

        // Now, set out which columns we want to see on the list view
        $this->crud->columns( 'title', 'path', 'sort_order', 'status' );

        /*
         * And now, set out how the form will display in the CMS
         *
         * Here, we're saying that the title, URL path, category, H1 title, excerpt, image and page builder fields
         * appear in the 'main' column and that the status and sort order appear in the sidebar.
         */
        $this->crud->columnGroup( 'main', ['title', 'path', 'case_study_category_id', 'h1_title', 'image', 'excerpt', 'content', 'page_builder'] );
        $this->crud->columnGroup( 'side', ['status', 'sort_order'] );
    }

    /**
     * Callback function used to get field labels for the 'case_studies' table
     * @return array
     */
    public function getCaseStudiesDefaultLabels(){
        return CaseStudiesConfig::$labels;
    }

    /**
     * Callback function used to get field help text for the 'case_studies' table
     * @return array
     */
    public function getCaseStudiesDefaultHelp(){
        return CaseStudiesConfig::$help;
    }

    /**
     * Callback function used to get image sizes for a provided asset field
     * @param $field_name
     * @return array
     */
    public function getCaseStudiesAssetConfig( $field_name ){
        return isset( CaseStudiesConfig::$image_sizes[ $field_name ] ) ? CaseStudiesConfig::$image_sizes[ $field_name ] : [];
    }

    /**
     * CRUD Functionality for the 'case_study_categories' table
     */
    public function case_study_categories(){
        // Just in case, define which database table we're reading from.
        $this->crud->setTable( 'case_study_categories' );

        // And manually set a 'Subject', which is a human readable version of this resource type.
        $this->crud->setSubject( 'Case Study Categories' );

        // Case Studies have a URL, so we need to set that up
        $this->crud->addColumn( 'path' );

        /*
         * And the URL should begin with the system page ID we've set up
         *
         * Here, we're saying that the path should use the URL for item 5 in the pages table, using page_id as the
         * foreign/primary key. This is set in the default database seeder
         */
        $SystemPage = Url::getSystemPage( 'case_study', 'archive' );
        if( $SystemPage ){
            $this->crud->setBasePathPrefix( 'pages', $SystemPage->page_id, 'page_id' );
        }

        $this->crud->addColumn('page_builder', 'image');
        $this->crud->changeType('page_builder', 'page_builder');
        $this->crud->enablePageBuilder();

        // Now, set out which columns we want to see on the list view
        $this->crud->columns( 'title', 'path', 'sort_order', 'status' );

        /*
         * And now, set out how the form will display in the CMS
         *
         * Here, we're saying that the title, URL path, H1 title and content fields appear in the 'main' column and
         * that the status and sort order appear in the sidebar.
         */
        $this->crud->columnGroup( 'main', ['title', 'path', 'h1_title', 'image', 'page_builder'] );
        $this->crud->columnGroup( 'side', ['status', 'sort_order'] );

        $this->crud->excludeFromEdit( ['content'] );
    }

    /**
     * Callback function used to get field labels for the 'case_study_categories' table
     * @return array
     */
    public function getCaseStudyCategoriesDefaultLabels(){
        return CaseStudyCategoriesConfig::$labels;
    }

    /**
     * Callback function used to get field help text for the 'case_study_categories' table
     * @return array
     */
    public function getCaseStudyCategoriesDefaultHelp(){
        return CaseStudyCategoriesConfig::$help;
    }

    /**
     * Callback function used to get asset field image sizes for Case Study Categories
     * @param $field_name
     * @return array
     */
    public function getCaseStudyCategoriesAssetConfig( $field_name ){
        return isset( CaseStudyCategoriesConfig::$image_sizes[ $field_name ] ) ? CaseStudyCategoriesConfig::$image_sizes[ $field_name ] : [];
    }

    /**
     * CRUD Functionality for the Case Study Archive page
     */
    public function case_study_archive(){
        $this->crud->setTable('pages');
        $this->crud->setSubject('Case Study Archive Page');
        $this->crud->multiLingualSingle( 6 );

        $this->crud->displayColumn('title');

        $this->crud->addColumn('path', '', 'side');
        $this->crud->systemPageEnabled = true;

        $this->crud->addColumn('page_builder', 'image');
        $this->crud->changeType('page_builder', 'page_builder');
        $this->crud->enablePageBuilder();

        $this->crud->columns( 'title', 'path', 'parent', 'status' );

        $this->crud->columnGroup( 'main', array( 'title', 'path', 'h1_title', 'image', 'page_builder' ) );
        $this->crud->columnGroup( 'side', array( 'status' ) );

        // And stop the page's parent from being changed
        $this->crud->excludeFromEdit( ['parent', 'sort_order'] );
    }

    /**
     * Callback function used to get field labels for the 'case_study_archive' function
     * @return array
     */
    public function getCaseStudyArchiveDefaultLabels(){
        return PageConfig::$labels;
    }

    /**
     * Callback function used to get field help text for the 'case_study_archive' function
     * @return array
     */
    public function getCaseStudyArchiveDefaultHelp(){
        return PageConfig::$help;
    }

    /**
     * Callback function used to get asset field image sizes for the Case Study Archive page
     * @param $field_name
     * @return array
     */
    public function getCaseStudyArchiveAssetConfig( $field_name ){
        return isset( PageConfig::$image_sizes[ $field_name ] ) ? PageConfig::$image_sizes[ $field_name ] : [];
    }
}