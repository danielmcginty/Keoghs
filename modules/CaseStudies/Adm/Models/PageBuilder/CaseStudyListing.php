<?php
namespace Modules\CaseStudies\Adm\Models\PageBuilder;

use Adm\Crud\LaraCrud;
use Adm\Models\PageBuilder\PageBuilderBase;
use App\Models\Scopes\LanguageScope;
use Modules\CaseStudies\Theme\Models\CaseStudy;

class CaseStudyListing extends PageBuilderBase {

    public $display_name    = 'Case Study Listing';
    public $description     = 'Add case study listings to the page';

    public $builder_fields  = [
        'heading'   => [
            'display_name'  => 'Heading',
            'help'          => 'Add a heading to the block, defaulting to \'Case Studies\' if left empty',
            'type'          => 'string',
            'required'      => false,
            'validation'    => [],
            'group'         => 'Main'
        ],
        'case_studies'  => [
            'display_name'  => 'Case Studies',
            'help'          => 'Select the case studies to display, or leave empty to show the first 3 based on sort order',
            'type'          => 'orderable_multiple_select',
            'options'       => [],
            'required'      => false,
            'validation'    => [],
            'group'         => 'Main'
        ]
    ];

    public function callbackBeforePrepare( LaraCrud $crud ){
        $opts = [];
        foreach( CaseStudy::withoutGlobalScope( LanguageScope::class )->where( 'language_id', $crud->language_id )->orderBy( 'title' )->get() as $CaseStudy ){
            $opts[ $CaseStudy->case_study_id ] = $CaseStudy->title;
        }
        $this->builder_fields['case_studies']['options'] = $opts;
    }
}