<?php
namespace Modules\CaseStudies\Config;

use Adm\Interfaces\NavigationInterface;

/**
 * Class Navigation
 * @package Modules\CaseStudies\Config
 *
 * The CMS Navigation Builder for the Case Studies Module
 */
class Navigation implements NavigationInterface {
    /**
     * Returns the CMS Navigation links for the module
     *
     * @return array
     */
    public function getNavItems(){
        return [
            [
                'title' => 'Case Studies', 'url' => route( 'adm.path', 'case-studies' ), 'position' => 15, 'sub' => [
                    ['title' => 'Archive Page', 'url' => route( 'adm.path', 'case-study-archive' ), 'position' => 1],
                    ['title' => 'Case Studies', 'url' => route( 'adm.path', 'case-studies' ), 'position' => 2],
                    ['title' => 'Case Study Categories', 'url' => route( 'adm.path', 'case-study-categories' ), 'position' => 3],
                ]
            ]
        ];
    }
}