<?php
namespace Modules\CaseStudies\Config;

class CaseStudiesConfig {
    /**
     * Array containing labels for our CMS fields
     * @var array
     */
    public static $labels = [
        'case_study_category_id' => 'Category',
        'title' => 'Title',
        'h1_title' => 'H1 Title',
        'image' => 'Feature Image',
        'excerpt' => 'Excerpt Text',
        'sort_order' => 'Sort Order',
        'status' => 'Published?'
    ];

    /**
     * Array containing help text for our CMS fields
     * @var array
     */
    public static $help = [
        'case_study_category_id' => 'Which category the case study belongs to',
        'title' => 'The case study title. Used in links and meta data generation',
        'h1_title' => 'The case study page\'s &lt;h1&gt; tag title. Uses the Title field if left empty',
        'image' => 'The main feature image for the case study, used on the archive page',
        'excerpt' => 'A short, succinct excerpt for the case study, used on the archive page',
        'content' => 'The main article content',
        'sort_order' => 'Which order should the case study appear in on the listing page',
        'status' => 'Whether or not the case study is visible on the website'
    ];

    /**
     * Array containing the various required image sizes for Case Studies
     * @var array
     */
    public static $image_sizes = [
        'image' => [
            'desktop' => [
                'label' => 'Banner',
                'width' => 1920,
                'height' => 470,
                'primary' => true
            ],
            'archive' => [
                'label' => 'Listing',
                'width' => 960,
                'height' => 0,
                'primary' => false
            ]
        ]
    ];
}