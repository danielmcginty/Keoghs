<?php
namespace Modules\CaseStudies\Config;

class CaseStudyCategoriesConfig {
    /**
     * Array containing labels for our CMS fields
     * @var array
     */
    public static $labels = [
        'title' => 'Title',
        'h1_title' => 'H1 Title',
        'content' => 'Content',
        'sort_order' => 'Sort Order',
        'status' => 'Published?'
    ];

    /**
     * Array containing help text for our CMS fields
     * @var array
     */
    public static $help = [
        'title' => 'The case study category title. Used in links and meta data generation',
        'h1_title' => 'The case study category page\'s &lt;h1&gt; tag title. Uses the Title field if left empty',
        'content' => 'The content to display when viewing this category on the website',
        'sort_order' => 'Which order should the case study category appear in on the listing page',
        'status' => 'Whether or not the case study category is visible on the website'
    ];

    /**
     * Stores a shared configuration for image sizes
     * @var array
     */
    public static $image_sizes = [
        'image' => [
            'desktop' => [
                'label' => 'Desktop',
                'width' => 1920,
                'height' => 400,
                'primary' => true
            ],
            'mobile' => [
                'label' => 'Mobile',
                'width' => 640,
                'height' => 300,
                'primary' => false
            ]
        ]
    ];
}