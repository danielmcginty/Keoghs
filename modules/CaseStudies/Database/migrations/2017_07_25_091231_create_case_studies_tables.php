<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaseStudiesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'case_studies', function( Blueprint $table ){
            $table->integer( 'case_study_id' );
            $table->integer( 'language_id' );
            $table->integer( 'case_study_category_id' );
            $table->string( 'title' );
            $table->string( 'h1_title' )->nullable();
            $table->text( 'image' )->nullable();
            $table->text( 'excerpt' );
            $table->integer( 'sort_order' )->default( 0 );
            $table->boolean( 'status' )->default( 0 );
            $table->timestamps();
            $table->softDeletes();
            $table->primary( ['case_study_id', 'language_id'] );
        });

        Schema::create( 'case_study_categories', function( Blueprint $table ){
            $table->integer( 'case_study_category_id' );
            $table->integer( 'language_id' );
            $table->string( 'title' );
            $table->string( 'h1_title' )->nullable();
            $table->text( 'image' )->nullable();
            $table->text( 'content' )->nullable();
            $table->integer( 'sort_order' )->default( 0 );
            $table->boolean( 'status' )->default( 0 );
            $table->timestamps();
            $table->softDeletes();
            $table->primary( ['case_study_category_id', 'language_id'] );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'case_studies' );
        Schema::drop( 'case_study_categories' );
    }
}
