<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AmendCaseStudiesTablesForContentVersioning extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'case_studies', function( Blueprint $table ){
            $table->integer( 'version' )->after( 'language_id' )->default( 1 );
            $table->dropPrimary( 'case_studies_case_study_id_language_id_primary' );
            $table->primary( ['case_study_id', 'language_id', 'version'], 'case_studies_primary_key' );
        });

        Schema::table( 'case_study_categories', function( Blueprint $table ){
            $table->integer( 'version' )->after( 'language_id' )->default( 1 );
            $table->dropPrimary( 'case_study_categories_case_study_category_id_language_id_primary' );
            $table->primary( ['case_study_category_id', 'language_id', 'version'], 'case_study_categories_primary_key' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'case_studies', function( Blueprint $table ){
            $table->dropColumn( 'version' );
            $table->dropPrimary( 'case_studies_primary_key' );
            $table->primary( ['case_study_id', 'language_id'] );
        });

        Schema::table( 'case_study_categories', function( Blueprint $table ){
            $table->dropColumn( 'version' );
            $table->dropPrimary( 'case_study_categories_primary_key' );
            $table->primary( ['case_study_category_id', 'language_id'] );
        });
    }
}
