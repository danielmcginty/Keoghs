<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AmendCaseStudiesTableAddContentField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'case_studies', function( Blueprint $table ){
            $table->text( 'content' )->after( 'excerpt' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'case_studies', function( Blueprint $table ){
            $table->dropColumn( 'content' );
        });
    }
}
