<?php
namespace Modules\CaseStudies\Database\Seeders;

use Illuminate\Database\Seeder;

class CaseStudiesPageBuilderTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table( 'page_builder_block_types' )->insert([
            'reference' => 'case_studies',
            'editable' => 1,
            'deletable' => 1
        ]);
    }
}
