<?php
namespace Modules\CaseStudies\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CaseStudiesPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // News Page
        \Modules\Page\Theme\Models\Page::create([
            'page_id' => 6,
            'language_id' => 1,
            'version' => 1,
            'parent' => 0,
            'title' => 'Case Studies',
            'h1_title' => '',
            'image' => 0,
            'sort_order' => 0,
            'status' => 1
        ]);
        \App\Models\Url::create([
            'url' => 'case-studies',
            'table_name' => 'pages',
            'table_key' => 6,
            'language_id' => 1,
            'path_prefix' => null,
            'manual' => 0,
            'no_index_meta' => 0,
            'meta_title' => 'Case Studies',
            'meta_description' => '',
            'system_page' => 1,
            'route' => 'controller=case_study&method=archive'
        ]);

        Model::reguard();
    }
}
