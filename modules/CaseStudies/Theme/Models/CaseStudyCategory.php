<?php
namespace Modules\CaseStudies\Theme\Models;

use App\Helpers\AssetHelper;
use App\Models\CommonModel;
use App\Models\Scopes\LanguageScope;
use App\Models\Scopes\PublishedScope;
use App\Models\Traits\HasCompositePrimaryKey;
use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\CaseStudies\Config\CaseStudyCategoriesConfig;

class CaseStudyCategory extends CommonModel {
    /*
     * We need to include several 'traits', which affect how the model functions:
     *
     * SoftDeletes. As content isn't actually deleted, this trait tells the query builder to only look for records
     * with a NULL deleted_at field
     *
     * HasCompositePrimaryKey. As we are using a composite primary key (multiple fields), we need to tell the query
     * builder not just to look for a single PK field
     *
     * HasQueryBuilderCache. To optimise the website for users, we need to cache database queries for the front-end
     */
    use SoftDeletes, HasCompositePrimaryKey, HasQueryBuilderCache;

    // Which database table we are using
    protected $table = 'case_study_categories';

    // What the primary key fields are
    protected $primaryKey = ['case_study_category_id', 'language_id'];

    protected $appends = ['meta_title', 'meta_description', 'no_index'];

    /**
     * As we need to tell the model about a couple of other fields in the database table, we need to override the
     * boot() function to register these fields
     */
    protected static function boot(){
        parent::boot();

        // As we have a language ID, tell the model to only use the current system language in look ups
        static::addGlobalScope( new LanguageScope );

        // And because we have a published / status flag, tell the model to only load items with a status of 1
        static::addGlobalScope( new PublishedScope );
    }

    /**
     * As a case study category has many case studies assigned to it, we can get these simply by defining a custom
     * 'Accessor' function. This means we can iterate over all case studies belonging to the category using the
     * ->case_studies property of the model
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function case_studies(){
        return $this->hasMany( '\\Modules\\CaseStudies\\Theme\\Models\\CaseStudy', 'case_study_category_id', 'case_study_category_id' );
    }

    /**
     * Return the set of images assigned to the article in an easy to use array.
     *
     * @return array
     */
    public function getImageAttribute(){
        return AssetHelper::getAssets(
            $this->table,
            $this->case_study_category_id,
            $this->language_id,
            $this->version,
            'image',
            CaseStudyCategoriesConfig::$image_sizes['image']
        );
    }
}