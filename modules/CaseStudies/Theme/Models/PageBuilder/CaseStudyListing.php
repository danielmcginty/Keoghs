<?php
namespace Modules\CaseStudies\Theme\Models\PageBuilder;

use App\Models\PageBuilder\BaseBlockModel;
use Illuminate\Database\Eloquent\Collection;
use Modules\CaseStudies\Theme\Models\CaseStudy;

class CaseStudyListing extends BaseBlockModel {

    public function getValue( $page_builder_block, $field, $original_value ){
        switch( $field ){
            case 'case_studies':
                $CaseStudies = new Collection();
                if( !empty( $original_value ) && is_json( $original_value ) ){
                    foreach( (array)json_decode( $original_value, true ) as $raw_case_study ){
                        if( empty( $raw_case_study['value'] ) ){ continue; }

                        $Article = CaseStudy::where( 'case_study_id', $raw_case_study['value'] )->first();
                        if( $Article ){
                            $CaseStudies->push( $Article );
                        }
                    }
                }

                if( $CaseStudies->isEmpty() ){
                    $CaseStudies = CaseStudy::orderBy( 'sort_order', 'asc' )->limit( 3 )->get();
                }

                return $CaseStudies;
            case 'heading':
                return !empty( $original_value ) ? $original_value : 'Case Studies';
            default:
                return parent::getValue( $page_builder_block, $field, $original_value );
        }
    }

}