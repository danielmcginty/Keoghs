<?php
namespace Modules\CaseStudies\Theme\Models;

use App\Helpers\AssetHelper;
use App\Models\CommonModel;
use App\Models\Scopes\LanguageScope;
use App\Models\Scopes\PublishedScope;
use App\Models\Traits\HasCompositePrimaryKey;
use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\CaseStudies\Config\CaseStudiesConfig;

class CaseStudy extends CommonModel {
    /*
     * We need to include several 'traits', which affect how the model functions:
     *
     * SoftDeletes. As content isn't actually deleted, this trait tells the query builder to only look for records
     * with a NULL deleted_at field
     *
     * HasCompositePrimaryKey. As we are using a composite primary key (multiple fields), we need to tell the query
     * builder not just to look for a single PK field
     *
     * HasQueryBuilderCache. To optimise the website for users, we need to cache database queries for the front-end
     */
    use SoftDeletes, HasCompositePrimaryKey, HasQueryBuilderCache;

    // Which database table we are using
    protected $table = 'case_studies';

    // What the primary key fields are
    protected $primaryKey = ['case_study_id', 'language_id'];

    protected $appends = ['meta_title', 'meta_description', 'no_index'];

    /**
     * As we need to tell the model about a couple of other fields in the database table, we need to override the
     * boot() function to register these fields
     */
    protected static function boot(){
        parent::boot();

        // As we have a language ID, tell the model to only use the current system language in look ups
        static::addGlobalScope( new LanguageScope );

        // And because we have a published / status flag, tell the model to only load items with a status of 1
        static::addGlobalScope( new PublishedScope );
    }

    /**
     * As case studies belong to categories, we can define a relationship within the Eloquent model to get the category
     * as a model, accessible to the case study directly
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category(){
        return $this->belongsTo( '\\Modules\\CaseStudies\\Theme\\Models\\CaseStudyCategory', 'case_study_category_id', 'case_study_category_id' );
    }

    /**
     * A custom 'scope' that allows us to affect queries without having to do anything fiddly.
     * This one is used to modify a query to specify the case_study_category_id
     *
     * @param $query
     * @param $category_id
     * @return mixed
     */
    public function scopeInCategory( $query, $category_id ){
        return $query->where( 'case_study_category_id', $category_id );
    }

    /**
     * Simple query scope.
     * Allows us to query the table against a search term in a central location.
     *
     * @param $query
     * @param $search_term
     * @return mixed
     */
    public function scopeMatchingSearchTerm( $query, $search_term ){
        $query->distinct();
        $query->select( $this->table . '.*' );
        $query->join( 'page_builder_blocks', 'page_builder_blocks.table_key', '=', $this->table . '.case_study_id' );
        $query->join( 'page_builder_block_content', 'page_builder_block_content.page_builder_block_id', '=', 'page_builder_blocks.id' );
        $query->where( 'page_builder_blocks.table_name', '=', $this->table );
        $query->where( function( $sub_query ) use ( $search_term ) {
            return $sub_query->where( 'page_builder_block_content.value', 'LIKE', '%' . $search_term . '%' )
                ->orWhere( $this->table . '.title', 'LIKE', '%' . $search_term . '%' )
                ->orWhere( $this->table . '.h1_title', 'LIKE', '%' . $search_term . '%' )
                ->orWhere( $this->table . '.excerpt', 'LIKE', '%' . $search_term . '%' );
        });

        return $query;
    }

    /**
     * As the image set against the case study can have multiple sizes, we can define a custom 'Accessor' function
     * to load the array of images & their sizes when we call the image attribute of the case study
     *
     * @param $image_id
     * @return array
     */
    public function getImageAttribute( $image_id ){
        $AssetHelper = new AssetHelper();
        return $AssetHelper->getAssets( $this->table, $this->case_study_id, $this->language_id, $this->version, 'image', CaseStudiesConfig::$image_sizes['image'] );
    }
}