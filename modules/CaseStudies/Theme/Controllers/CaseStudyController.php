<?php
namespace Modules\CaseStudies\Theme\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Url;
use Modules\CaseStudies\Theme\Models\CaseStudy;
use Modules\CaseStudies\Theme\Models\CaseStudyCategory;

class CaseStudyController extends Controller {

    /**
     * Load the page template for an individual case study.
     *
     * @param $case_study_id
     */
    public function index( $case_study_id ){
        // Try to find the case study from the database
        $case_study = CaseStudy::where( 'case_study_id', $case_study_id )->first();

        // If we've found it, we can perform our front-end code
        if( $case_study ){
            // Generate breadcrumbs
            // So, get the home page
            $home = Url::getSystemPage( 'home', 'index' );
            if( $home ){
                // And add it as a breadcrumb
                $this->addBreadcrumb( route('theme', $home->url), $home->getAttributes()['title'] );
            }

            // And get the Case Study archive page
            $archive_page = Url::getSystemPage( 'case_study', 'archive' );
            if( $archive_page ){
                // And add it as a breadcrumb
                $this->addBreadcrumb( route('theme', $archive_page->url), $archive_page->getAttributes()['title'] );
            }

            // And get the Case Study category
            if( $case_study->category ){
                // And add it as a breadcrumb
                $this->addBreadcrumb( route('theme', $case_study->category->url), $case_study->category->getAttributes()['title'] );
            }

            // And finally, add the case study as a breadcrumb
            $this->addBreadcrumb( route('theme', $case_study->url), $case_study->getAttributes()['title'] );

            // Assign the case study resource and title to the current set of output variables
            $this->pageTitle = $case_study->title;
            $this->this_page = $case_study;

            // And set the case study categories as a variable for output in the sidebar
            $this->case_study_categories = $this->_getCaseStudyCategories();

            // And set the view we'll be using as output
            $this->view = 'case-studies.case-study';
        }
    }

    /**
     * Load the page template for a case study category.
     *
     * @param $case_study_category_id
     */
    public function category( $case_study_category_id ){
        if( $case_study_category_id == 0 ){
            $case_study_category = Url::getSystemPage( 'case_study', 'archive' );
        } else {
            $case_study_category = CaseStudyCategory::where( 'case_study_category_id', $case_study_category_id )->with( 'case_studies' )->first();
        }

        if( $case_study_category ){
            $home = Url::getSystemPage( 'home', 'index' );
            if( $home ){
                $this->addBreadcrumb( route('theme', $home->url), $home->getAttributes()['title'] );
            }

            $archive_page = Url::getSystemPage( 'case_study', 'archive' );
            if( $archive_page ){
                $this->addBreadcrumb( route('theme', $archive_page->url), $archive_page->getAttributes()['title'] );
            }

            if( $case_study_category_id != 0 ){
                $this->addBreadcrumb( route('theme', $case_study_category->url), $case_study_category->getAttributes()['title'] );
            }

            $this->pageTitle = $case_study_category->title;
            $this->this_page = $case_study_category;

            $this->case_study_archive_page = Url::getSystemPage( 'case_study', 'archive' );
            if( $this->request->input( 'search_term' ) ){
                $this->search_term = $this->request->input('search_term');

                // If we have a search term, add another breadcrumb
                $this->addBreadcrumb( '#', 'Search: \'' . $this->search_term . '\'' );
            }

            $this->case_study_categories = $this->_getCaseStudyCategories();

            $this->case_studies = $this->_getArchiveArticles( $case_study_category_id );

            $this->case_studies->setPath( route('theme', $case_study_category->url ) );
            $this->case_studies->appends( request()->except( 'page' ) );

            // And set the view we'll be using as output
            $this->view = 'case-studies.archive';
        }
    }

    /**
     * Load the page template for the case study archive / landing page.
     */
    public function archive(){
        $this->category( 0 );
    }

    /**
     * Centralised function used to generate sidebar navigation links for case study categories.
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    private function _getCaseStudyCategories(){
        // So, get all case study categories, with their case studies
        $categories = CaseStudyCategory::with('case_studies')->get();

        foreach( $categories as $key => $category ){
            if( !count( $category->case_studies ) ){
                unset( $categories[ $key ] );
            }
        }

        return $categories;
    }

    private function _getArchiveArticles( $category_id = false ){
        $qry = CaseStudy::distinct();

        if( $category_id ){
            $qry->inCategory( $category_id );
        }

        if( $this->request->input( 'search_term' ) ){
            $qry->matchingSearchTerm( $this->request->input( 'search_term' ) );
        }

        $qry->orderBy( 'sort_order', 'asc' );
        $qry->orderBy( 'case_study_id', 'desc' );

        return $qry->paginate( 6 );
    }
}