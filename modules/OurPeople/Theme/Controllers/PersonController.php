<?php

namespace Modules\OurPeople\Theme\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Url;
use Illuminate\Http\Request;
use Modules\Expertise\Theme\Models\Expertise;
use Modules\OurPeople\Config\OurPeopleConfig;
use Modules\OurPeople\Theme\Models\Person;

class PersonController extends Controller
{
    public function index(Request $request)
    {
        $this_page = Url::getSystemPage('person', 'index');

        if (!$this_page) {
            return;
        }

        $home = Url::getSystemPage('home', 'index');
        if ($home) {
            $this->addBreadcrumb(route('theme', ['url' => $home->url]), $home->getAttributes()['title']);
        }
        $this->addBreadcrumb(route('theme', ['url' => $this_page->url]), $this_page->getAttributes()['title']);

        //$people_data = $this->getPeopleData($request);

        $this->this_page = $this_page;
        //$this->people = $people_data['people'];
        //$this->selected_expertise = $people_data['selected_expertise'];
        //$this->selected_locations = $people_data['selected_locations'];
        //$this->selected_sort_key = $people_data['selected_sort_key'];
        //$this->selected_sort = $people_data['selected_sort'];
        $this->expertise = $this->getExpertise();
        $this->locations = $this->getLocations();
        $this->sort_options = $this->getSortOptions();

        $AllPeople = Person::orderBy('last_name', 'asc')->get();
        $surname_letters = [];
        foreach ($AllPeople as $Person) {
            $letter = strtoupper(substr($Person->last_name, 0, 1));
            if (!in_array($letter, $surname_letters)) {
                $surname_letters[] = $letter;
            }
        }
        $this->people = $AllPeople;
        $this->surname_letters = $surname_letters;

        $this->view = 'our-people.archive';
    }

    public function person($id)
    {
        $this_page = Person::query()->where('person_id', $id)->first();

        if (!$this_page) {
            return;
        }

        $home = Url::getSystemPage('home', 'index');
        if ($home) {
            $this->addBreadcrumb(route('theme', ['url' => $home->url]), $home->getAttributes()['title']);
        }

        $our_people_page = Url::getSystemPage('person', 'index');
        $this->addBreadcrumb(route('theme', [$our_people_page->url]), $our_people_page->title);

        $this->addBreadcrumb(route('theme', [$this_page->url]), $this_page->title);

        $this->canonical_url = route('theme', $this_page->url);
        $this->this_page = $this_page;

        if (!empty($this_page->image) && !empty($this_page->image['article'])) {
            $this->addSocialMeta('og:image', $this_page->image['article']['src']);
            $this->addSocialMeta('og:image:width', $this_page->image['article']['width']);
            $this->addSocialMeta('og:image:height', $this_page->image['article']['height']);

            $this->addSocialMeta('twitter:image', $this_page->image['article']['src']);
        }

        $this->our_people_page = $our_people_page;
        $this->authored_insights = $this->getAuthoredInsights($this_page);
        $this->insights_page = Url::getSystemPage('insight', 'index');

        $this->view = 'our-people.person';
    }

    public function loadMorePeople(Request $request)
    {
        return $this->getPeopleData($request);
    }

    public function filterPeople(Request $request)
    {
        return $this->getPeopleData($request);
    }

    private function getPeopleData(Request $request)
    {
        $selected_expertise = $request->has('expertise') ? explode(',', $request->query('expertise')) : [];

        $selected_locations = $request->has('locations') ? explode(',', $request->query('locations')) : [];
        $locations = $this->getLocations();

        $selected_sort_key = $request->query('sort-by', 'latest');
        $selected_sort = $this->getSortOptions()[$selected_sort_key] ?? null;

        $people = Person::query()
            ->when(!empty($selected_expertise), function ($query) use ($selected_expertise) {
                foreach ($selected_expertise as $expertise) {
                    if (empty($expertise)) {
                        continue;
                    }

                    $query->orWhere(function ($query) use ($expertise) {
                        return $query->withExpertise($expertise);
                    });
                }
            })
            ->when(!empty($selected_locations), function ($query) use ($selected_locations, $locations) {
                foreach ($selected_locations as $location) {
                    if (strlen($location) === 0) {
                        continue;
                    }

                    //$location = $locations[$location];

                    $query->orWhere(function ($query) use ($location) {
                        return $query->withLocation($location);
                    });
                }
            })
            ->when(!empty($selected_sort), function ($query) use ($selected_sort) {
                return $query->orderBy($selected_sort['field'], $selected_sort['direction']);
            }, function ($query) {
                return $query->orderBy('last_name', 'asc');
            });

        return [
            'selected_expertise' => $selected_expertise,
            'selected_locations' => $selected_locations,
            'selected_sort_key' => $selected_sort_key,
            'selected_sort' => $selected_sort,
            'people' => $people->paginate(20),
        ];
    }

    private function getExpertise()
    {
        return Expertise::query()->get();
    }

    private function getLocations()
    {
        return Person::query()
            ->groupBy('location')
            ->orderBy('location')
            ->pluck('location')
            ->filter(function ($location) {
                return !empty(trim($location));
            });
    }

    private function getSortOptions()
    {
        return OurPeopleConfig::$sort_options;
    }

    private function getAuthoredInsights(Person $person)
    {
        return $person->insights()->take(2)->orderByDesc('published_at')->get();
    }
}