<?php

namespace Modules\OurPeople\Theme\Models;

use App\Helpers\AssetHelper;
use App\Models\CommonModel;
use App\Models\Scopes\LanguageScope;
use App\Models\Scopes\PublishedScope;
use App\Models\Traits\HasCompositePrimaryKey;
use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Expertise\Theme\Models\Expertise;
use Modules\Insights\Theme\Models\Insight;
use Modules\OurPeople\Config\OurPeopleConfig;
use Modules\Tags\Theme\Models\Tag;

class Person extends CommonModel
{
    use SoftDeletes, HasCompositePrimaryKey, HasQueryBuilderCache;

    protected $table = 'people';

    protected $primaryKey = ['person_id', 'language_id'];

    protected $appends = ['full_name', 'expertise_area'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new LanguageScope);
        static::addGlobalScope(new PublishedScope);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'people_tag', 'person_id', 'tag_id', 'person_id', 'tag_id');
    }

    public function expertise()
    {
        return $this->belongsToMany(Expertise::class, 'people_expertise', 'person_id', 'expertise_id', 'person_id', 'expertise_id');
    }

    public function insights()
    {
        return $this->hasMany(Insight::class, 'author_id', 'person_id');
    }

    public function scopeWithExpertise($query, $expertise)
    {
        return $query->whereHas('expertise', function ($query) use ($expertise) {
            return $query->where('expertise.expertise_id', $expertise);
        });
    }

    public function scopeWithLocation($query, $location)
    {
        return $query->where('location', $location);
    }

    public function scopeWithTag($query, $tag)
    {
        return $query->whereHas('tags', function ($query) use ($tag) {
            return $query->where('title', $tag);
        });
    }

    public function getImageAttribute()
    {
        $AssetHelper = new AssetHelper();
        $Image_Sizes = OurPeopleConfig::$image_sizes;
        $Image_Sizes['image']['schema'] = [
            'label' => 'Schema Image',
            'width' => 696,
            'height' => 392,
            'primary' => false
        ];
        $Image_Sizes['image']['amp'] = [
            'label' => 'AMP',
            'width' => 640,
            'height' => 350,
            'primary' => false
        ];
        return $AssetHelper->getAssets(
            $this->table,
            $this->person_id,
            $this->language_id,
            $this->version,
            'image',
            $Image_Sizes['image']
        );
    }

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function getExpertiseAreaAttribute()
    {
        return $this->related_expertise_override ?: optional($this->expertise()->first())->title;
    }
}