<?php

namespace Modules\OurPeople\Theme\Models\PageBuilder;

use App\Helpers\AssetHelper;
use App\Models\PageBuilder\BaseBlockModel;
use App\Models\Url;
use Modules\OurPeople\Adm\Models\PageBuilder\OurPeople as AdmBlock;
use Modules\OurPeople\Config\OurPeopleBlockSettingsConfig;

class OurPeople extends BaseBlockModel
{
    public function getValue($page_builder_block, $field, $original_value)
    {
        switch ($field) {
            case 'image':
                $AdmBlock = new AdmBlock;
                return parent::getAssetFieldValue(
                    $page_builder_block,
                    $field,
                    $original_value,
                    $AdmBlock->column_extras[$field]['sizes']
                );
            default:
                return parent::getValue($page_builder_block, $field, $original_value);
        }
    }

    public function postProcess($fields)
    {
        $defaults = \DB::table('our_people_block_settings')->where('id', 1)->first();
        if ($defaults) {
            if (empty($fields['heading'])) {
                $fields['heading'] = $defaults->heading;
            }
            if (empty($fields['content'])) {
                $fields['content'] = $defaults->content;
            }
            if (empty($fields['image']) || empty($fields['image']['single'])) {
                $fields['image'] = AssetHelper::getAssets(
                    'our_people_block_settings',
                    1,
                    1,
                    0,
                    'image',
                    OurPeopleBlockSettingsConfig::$image_sizes['image']
                );
            }
        }

        $fields['landing_page'] = Url::getSystemPage('person', 'index');

        if (!isset($fields['heading']) || empty($fields['heading'])) {
            $fields['heading'] = "Our People";
        }

        $fields['link'] = Url::getSystemPage('person', 'index');

        return $fields;
    }
}