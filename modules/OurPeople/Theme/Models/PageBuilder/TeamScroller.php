<?php

namespace Modules\OurPeople\Theme\Models\PageBuilder;

use App\Models\PageBuilder\BaseBlockModel;
use Illuminate\Database\Eloquent\Collection;
use Modules\OurPeople\Theme\Models\Person;

class TeamScroller extends BaseBlockModel
{
    public function getValue($page_builder_block, $field, $original_value)
    {
        switch ($field) {
            case 'contacts':
                $People = new Collection();
                if (!empty($original_value) && is_json($original_value)) {
                    foreach ((array)json_decode($original_value, true) as $contact) {
                        if (empty($contact['value'])) { continue; }

                        $Person = Person::where('person_id', $contact['value'])->first();
                        if (!$Person) { continue; }

                        $People->push($Person);
                    }
                }
                return $People;
            default:
                return parent::getValue($page_builder_block, $field, $original_value);
        }
    }
}
