<?php

namespace Modules\OurPeople\Config;

class OurPeopleConfig
{
    /**
     * @var array
     */
    public static $labels = [
        'first_name' => 'First Name',
        'last_name' => 'Last Name',
        'image' => 'Image',
        'email' => 'Email',
        'phone' => 'Phone',
        'linkedin' => 'LinkedIn',
        'job_title' => 'Position',
        'location' => 'Location',
        'biography' => 'Biography',
        'related_expertise_override' => 'Related Expertise Override',
        'tags' => 'Tags',
        'status' => 'Status',
    ];

    /**
     * @var array
     */
    public static $help = [
        'linkedin' => 'The LinkedIn URL for this person',
        'location' => 'The office location of this person',
        'related_expertise_override' => 'The related expertise to use when displaying this person',
    ];

    /**
     * @var array
     */
    public static $image_sizes = [
        'image' => [
            'primary' => [
                'label' => 'Primary',
                'width' => 640,
                'height' => 640,
                'primary' => true
            ],
            'listing' => [
                'label' => 'Primary',
                'width' => 100,
                'height' => 100,
                'primary' => false
            ],
        ]
    ];

    public static $sort_options = [
        'a-z' => [
            'label' => 'A-Z',
            'field' => 'last_name',
            'direction' => 'asc',
        ],
        'z-a' => [
            'label' => 'Z-A',
            'field' => 'last_name',
            'direction' => 'desc',
        ],
    ];
}