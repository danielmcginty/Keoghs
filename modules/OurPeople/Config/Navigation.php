<?php

namespace Modules\OurPeople\Config;

use Adm\Interfaces\NavigationInterface;

class Navigation implements NavigationInterface
{
    public function getNavItems()
    {
        return [
            [
                'title' => 'Our People',
                'url' => route('adm.path', 'our-people'),
                'position' => 60,
                'sub' => [
                    ['title' => 'Our People', 'url' => route('adm.path', 'our-people'), 'position' => 0],
                    ['title' => 'Block Defaults', 'url' => route('adm.path', 'our-people-block-settings'), 'position' => 10]
                ]
            ]
        ];
    }
}