<?php
namespace Modules\OurPeople\Config;

class OurPeopleBlockSettingsConfig {
    public static $labels              = [

    ];

    public static $help         = [

    ];

    public static $image_sizes  = [
        'image' => [
            'single'    => [
                'label'         => 'Image',
                'width'         => 1920,
                'height'        => 220,
                'primary'       => true
            ]
        ]
    ];
}