<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOurPeopleTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->integer('person_id');
            $table->integer('language_id');
            $table->integer('version');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('linkedin')->nullable();
            $table->integer('author_id')->default(0);
            $table->string('job_title')->nullable();
            $table->string('location')->nullable();
            $table->string('image')->nullable();
            $table->text('biography');
            $table->string('related_expertise_override')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->primary(['person_id', 'language_id', 'version'], 'people_pk');
        });

        Schema::create('people_expertise', function (Blueprint $table) {
            $table->integer('person_id');
            $table->integer('expertise_id');
            $table->primary(['person_id', 'expertise_id'], 'people_expertise_pk');
        });

        Schema::create('people_tag', function (Blueprint $table) {
            $table->integer('person_id');
            $table->integer('tag_id');
            $table->primary(['person_id', 'tag_id'], 'people_tag_pk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
        Schema::dropIfExists('people_expertise');
        Schema::dropIfExists('people_tag');
    }
}
