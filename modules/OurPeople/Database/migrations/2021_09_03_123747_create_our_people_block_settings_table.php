<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOurPeopleBlockSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('our_people_block_settings', function (Blueprint $table) {
            $table->id();
            $table->string('heading');
            $table->text('content');
            $table->string('image');
            $table->timestamps();
        });
        \DB::table('our_people_block_settings')->insert([
            'id'        => 1,
            'heading'   => 'Our People',
            'content'   => '<p>Keoghs\' 1,000+ talented, enthusiastic, hard-working and adaptable workforce is second to none in our marketplace and their skills, ability and experience are critical to our success.</p>',
            'image'     => '',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('our_people_block_settings');
    }
}
