<?php

namespace Modules\OurPeople\Library;

use App\Models\AssetGroup;
use App\Models\File;
use App\Models\FileDirectory;
use App\Models\Url;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Modules\Expertise\Theme\Models\Expertise;
use Modules\Tags\Theme\Models\Tag;

class PeopleImportProcessor
{
    const BASE_IMAGE_URL = "https://keoghs.co.uk";
    const ASSET_PATH = "/assets/site/content/images/";

    private $expertise = [];
    private $tags = [];
    private $prefix_url = null;

    public function process()
    {
        $this->processFileContents(
            $this->parseNewDataFile($this->locateDataFile('modules/OurPeople/assets/new_people_data.csv')),
            $this->parseOldDataFile($this->locateDataFile('modules/OurPeople/assets/old_people_data.csv')),
        );
    }

    private function locateDataFile(string $filename)
    {
        return fopen(base_path($filename), 'r');
    }

    private function parseNewDataFile($file): array
    {
        $processed_header = false;

        $num_processed = 0;

        $people = [];

        $this->log('Processing New People Data CSV ...');

        while (($row = fgetcsv($file)) !== false) {
            if (!$processed_header) {
                $processed_header = true;
                continue;
            }

            $num_processed++;

            $row = $this->mapNewDataFileFields($row);

            if (empty($row['ID'])) {
                continue;
            }

            $people[$row['ID']] = [
                'id' => $row['ID'],
                'first_name' => $row['FIRST_NAME'],
                'last_name' => $row['LAST_NAME'],
                'job_title' => $row['JOB_TITLE'],
                'location' => $row['LOCATION'],
                'biography' => $row['BIOGRAPHY'],
                'related_expertise_override' => $row['RELATED_EXPERTISE_OVERRIDE'],
                'tags' => [
                    $row['PRIMARY_TAG'],
                    $row['SECONDARY_TAG'],
                ],
                'expertise' => [
                    $row['RELATED_EXPERTISE_1'],
                    $row['RELATED_EXPERTISE_2'],
                    $row['RELATED_EXPERTISE_3'],
                ],
            ];
        }

        $this->log("New People Data CSV Processed - {$num_processed} rows processed");

        return $people;
    }

    private function parseOldDataFile($file): array
    {
        $processed_header = false;

        $num_processed = 0;

        $people = [];

        $this->log('Processing Old People Data CSV ...');

        while (($row = fgetcsv($file)) !== false) {
            if (!$processed_header) {
                $processed_header = true;
                continue;
            }

            $num_processed++;

            $row = $this->mapOldDataFileFields($row);

            if (empty($row['ID'])) {
                continue;
            }

            $people[$row['ID']] = [
                'id' => $row['ID'],
                'slug' => $row['ALIAS'],
                'status' => $row['ACTIVE'],
                'email' => $row['EMAIL'],
                'phone' => $row['PHONE'],
                'linkedin' => $row['LINKEDIN'],
                'author_id' => $row['AUTHOR_ID'],
                'image_url' => static::BASE_IMAGE_URL.$row['IMAGE_PATH'],
                'image_name' => $row['IMAGE_NAME'],
                'created_at' => $row['CREATED_AT'],
                'updated_at' => $row['UPDATED_AT'],
            ];
        }

        $this->log("Old People Data CSV Processed - {$num_processed} rows processed");

        return $people;
    }

    private function processFileContents(array $new_people_data, array $old_people_data)
    {
        $this->syncPeople($new_people_data, $old_people_data);
    }

    private function mapNewDataFileFields($row): array
    {
        $fields = [
            'ID',
            'FIRST_NAME',
            'LAST_NAME',
            'JOB_TITLE',
            'LOCATION',
            'PRIMARY_TAG',
            'SECONDARY_TAG',
            'BIOGRAPHY',
            'EXPERTISE',
            'RELATED_EXPERTISE_1',
            'RELATED_EXPERTISE_2',
            'RELATED_EXPERTISE_3',
            'RELATED_EXPERTISE_OVERRIDE',
            'IMAGE',
        ];

        $mapped = [];

        foreach ($fields as $ind => $field) {
            $mapped[$field] = isset($row[$ind]) ? trim($row[$ind]) : null;

            if ($mapped[$field] === 'NULL') {
                $mapped[$field] = null;
            }
        }

        return $mapped;
    }

    private function mapOldDataFileFields($row): array
    {
        $fields = [
            'ID',
            'ALIAS',
            'ACTIVE',
            'EMAIL',
            'PHONE',
            'LINKEDIN',
            'AUTHOR_ID',
            'IMAGE_PATH',
            'IMAGE_NAME',
            'CREATED_AT',
            'UPDATED_AT',
        ];

        $mapped = [];

        foreach ($fields as $ind => $field) {
            $mapped[$field] = isset($row[$ind]) ? trim($row[$ind]) : null;

            if ($mapped[$field] === 'NULL') {
                $mapped[$field] = null;
            }
        }

        return $mapped;
    }

    private function syncPeople(array $new_people_data, array $old_people_data): void
    {
        $this->log("Truncating People Table...");

        $files = DB::table('files')
            ->whereIn('id', function ($query) {
                return $query->select('image')->from('people');
            })
            ->get();

        foreach ($files as $file) {
            Storage::delete($file->storage_path);
        }

        DB::table('files')
            ->whereIn('id', function ($query) {
                return $query->select('image')->from('people');
            })
            ->delete();

        DB::table('asset_groups')
            ->where('table_name', 'people')
            ->delete();

        DB::table('301_redirects')
            ->where('old_url', 'LIKE', 'about-keoghs/our-people/%')
            ->delete();

        DB::table('urls')
            ->where('table_name', 'people')
            ->delete();

        DB::table('people_tag')->truncate();

        DB::table('people_expertise')->truncate();

        DB::table('people')->truncate();

        $this->log("People Table Truncated");

        $people = [];

        foreach ($new_people_data as $key => $person) {
            if (!isset($old_people_data[$key])) {
                throw new \Exception("Old Data does not exist for key: {$key}");
            }

            $people[] = array_merge($person, $old_people_data[$key]);
        }

        $people_collection = collect($people);

        $this->log("Inserting {$people_collection->count()} People...");

        $people_collection->chunk(500)
            ->each(function (Collection $people) {
                DB::table('people')
                    ->insert($people->map(function (array $person) {
                        return [
                            'person_id' => $person['id'],
                            'language_id' => 1,
                            'version' => 1,
                            'first_name' => $person['first_name'],
                            'last_name' => $person['last_name'],
                            'email' => $person['email'],
                            'phone' => $person['phone'],
                            'linkedin' => $person['linkedin'],
                            'author_id' => $person['author_id'],
                            'job_title' => $person['job_title'],
                            'location' => $person['location'],
                            'image' => $this->loadExternalImage($person),
                            'biography' => $person['biography'],
                            'related_expertise_override' => $person['related_expertise_override'],
                            'status' => $person['status'],
                            'created_at' => $person['created_at'],
                            'updated_at' => $person['updated_at'],
                        ];
                    })->toArray());
            });

        $this->log("{$people_collection->count()} People Inserted");

        $people_collection->each(function (array $person) {
            $this->insertUrl($person);

            foreach ($person['expertise'] as $expertise) {
                $this->addExpertiseToPerson($person, $expertise);
            }

            foreach ($person['tags'] as $tag) {
                $this->addTagToPerson($person, $tag);
            }
        });
    }

    private function log(string $message)
    {
        info($message);
    }

    private function loadExternalImage(array $person): ?string
    {
        $image_url = $person['image_url'];
        $image_name = $person['image_name'];

        if (strlen(trim($image_url)) === 0) {
            return null;
        }

        if (strlen(trim($image_name)) === 0) {
            $image_name = "{$person['first_name']} {$person['last_name']}";
        }

        $full_prefix = static::BASE_IMAGE_URL.static::ASSET_PATH;

        $relative_path = substr_replace($image_url, '', strpos($image_url, $full_prefix), strlen($full_prefix));

        $directory_path = trim(rtrim($relative_path, basename($relative_path)), '/');

        $directories = array_filter(explode('/', $directory_path));

        $directory = $this->createDirectoryPath($directories);

        try {
            $image = file_get_contents($image_url);
        } catch (\Exception $e) {
            return null;
        }

        sleep(0.5);

        return $this->storeImage($person, $image, $relative_path, $image_name, $directory);
    }

    private function createDirectoryPath(array $directories)
    {
        $processed_directories = [];

        foreach ($directories as $index => $directory) {
            $fd = FileDirectory::firstOrCreate([
                'parent' => $index > 0 ? $processed_directories[$index - 1] : 0,
                'path' => $directory,
                'name' => Str::title($directory)
            ]);

            $processed_directories[] = $fd->id;
        }

        return end($processed_directories);
    }

    private function storeImage($person, $image, $path, $name, $directory)
    {
        $extension = pathinfo($path, PATHINFO_EXTENSION);

        Storage::put($uploadPath = Str::random(40).".".$extension, $image);

        if (empty($extension)) {
            $extension = 'txt';
        }

        // Make sure the file has an extension (just in case validation passes somehow)
        if (empty($extension)) {
            $extension = 'txt';
        }

        // Make sure that we use .jpg instead of .jpeg
        $extension = str_replace('jpeg', 'jpg', $extension);

        // And default the alt text to the generated name.
        $alt_text = $name;

        $new_file = new File();
        $new_file->directory_id = $directory;
        $new_file->storage_path = $uploadPath;
        $new_file->file_name = Str::slug($name).".".$extension;
        $new_file->name = $name;
        $new_file->alt_text = $alt_text;
        $new_file->extension = $extension;
        $new_file->created_by = 1;

        $new_file->save();

        $this->insertAsset($person, 'primary', $new_file);

        return $new_file->id;
    }

    private function insertAsset(array $person, string $image_size, $new_file)
    {
        $asset = new AssetGroup();

        $asset->table_name = 'people';
        $asset->table_key = $person['id'];
        $asset->language_id = 1;
        $asset->version = 1;
        $asset->field = 'image';
        $asset->image_size = $image_size;
        $asset->file_id = $new_file->id;
        $asset->alt = $new_file->name;
        $asset->width = 0;
        $asset->height = 0;
        $asset->x_coordinate = 0;
        $asset->y_coordinate = 0;
        $asset->cropbox_left = 0;
        $asset->cropbox_top = 0;

        $asset->save();
    }

    private function addExpertiseToPerson(array $person, $expertise_name)
    {
        if (empty(trim($expertise_name))) {
            return;
        }

        if (isset($this->expertise[$expertise_name])) {
            return $this->connectExpertiseToPerson($person['id'], $this->expertise[$expertise_name]);
        }

        $expertise = Expertise::query()->where('title', $expertise_name)->first();

        if (is_null($expertise)) {
            $expertise = new Expertise();
            $expertise->language_id = 1;
            $expertise->version = 1;
            $expertise->title = $expertise_name;
            $expertise->status = 1;
            $expertise->save();
        }

        $this->expertise[$expertise_name] = $expertise->expertise_id;

        return $this->connectExpertiseToPerson($person['id'], $expertise->expertise_id);
    }

    private function connectExpertiseToPerson($person, $expertise)
    {
        DB::table('people_expertise')->insertOrIgnore([
            'person_id' => $person,
            'expertise_id' => $expertise,
        ]);
    }

    private function addTagToPerson(array $person, $tag_name)
    {
        if (empty(trim($tag_name))) {
            return;
        }

        if (isset($this->tags[$tag_name])) {
            return $this->connectTagToPerson($person['id'], $this->tags[$tag_name]);
        }

        $tag = Tag::query()->where('title', $tag_name)->first();

        if (is_null($tag)) {
            $tag = new Tag();
            $tag->title = $tag_name;
            $tag->save();
        }

        $this->tags[$tag_name] = $tag->tag_id;

        return $this->connectTagToPerson($person['id'], $tag->tag_id);
    }

    private function connectTagToPerson($person, $tag)
    {
        DB::table('person_tag')->insertOrIgnore([
            'person_id' => $person,
            'tag_id' => $tag,
        ]);
    }

    private function insertUrl(array $person)
    {
        if (empty($this->prefix_url)) {
            $this->prefix_url = Url::getSystemPage('person', 'index');
        }

        $new_slug = Str::slug($person['slug']);

        DB::table('urls')->insert([
            'url' => $url = "{$this->prefix_url->url}/{$new_slug}",
            'table_name' => 'people',
            'table_key' => $person['id'],
            'language_id' => 1,
            'meta_title' => "{$person['first_name']} {$person['last_name']}",
        ]);

        DB::table('301_redirects')->insert([
            'old_url' => "about-keoghs/our-people/{$person['slug']}",
            'new_url' => $url,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}