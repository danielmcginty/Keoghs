<?php

namespace Modules\OurPeople\Adm\Models\PageBuilder;

use Adm\Models\PageBuilder\PageBuilderBase;

class OurPeople extends PageBuilderBase
{
    public $display_name = 'Our People';

    public $description = 'Add a set of People to the page';

    public $builder_fields = [
        'heading' => [
            'display_name' => 'Heading',
            'help' => 'Add a heading to the block',
            'type' => 'string',
            'required' => false,
            'validation' => [],
            'group' => 'Main'
        ],
        'description' => [
            'display_name' => 'Description',
            'help' => 'Add a description to the block',
            'type' => 'text',
            'wysiwyg' => true,
            'required' => false,
            'validation' => [],
            'group' => 'Main'
        ],
        'image' => [
            'display_name'  => 'Image',
            'help'          => 'Override the default image in the block',
            'type'          => 'asset',
            'required'      => false,
            'validation'    => []
        ]
    ];

    public $column_extras = [
        'image' => [
            'multi_size'    => true,
            'sizes'         => [
                'single'        => [
                    'label'         => 'Image',
                    'width'         => 1920,
                    'height'        => 220,
                    'primary'       => true
                ]
            ]
        ]
    ];
}