<?php

namespace Modules\OurPeople\Adm\Models\PageBuilder;

use Adm\Crud\LaraCrud;
use Adm\Models\PageBuilder\PageBuilderBase;
use Modules\OurPeople\Theme\Models\Person;

class TeamScroller extends PageBuilderBase
{
    public $display_name = 'Team Scroller';

    public $description = 'Add a scrolling set of team members to the page';

    public $builder_fields = [
        'heading' => [
            'display_name' => 'Heading',
            'help' => 'Add a heading to the scroller',
            'type' => 'string',
            'required' => false,
            'validation' => [],
            'group' => 'Main'
        ],
        'contacts' => [
            'display_name'  => 'Team members',
            'help'          => 'Select the team members to display',
            'type'          => 'orderable_multiple_select',
            'options'       => [],
            'required'      => false,
            'validation'    => []
        ]
    ];

    public function callbackBeforePrepare(LaraCrud $crud)
    {
        $people = [];
        foreach (Person::orderBy('last_name')->orderBy('first_name')->get() as $Person) {
            $people[$Person->person_id] = $Person->full_name;
        }
        $this->builder_fields['contacts']['options'] = $people;
    }
}
