<?php

namespace Modules\OurPeople\Adm\Controllers;

use Adm\Controllers\AdmController;
use App\Models\Url;
use Modules\OurPeople\Config\OurPeopleBlockSettingsConfig;
use Modules\OurPeople\Config\OurPeopleConfig;

class ModuleController extends AdmController
{
    public function our_people()
    {
        $this->crud->setTable('people');
        $this->crud->setSubject('People');

        $this->crud->addColumn('path');
        $SystemPage = Url::getSystemPage('person', 'index');
        if ($SystemPage) {
            $this->crud->setBasePathPrefix('pages', $SystemPage->page_id, 'page_id');
        }
        $this->crud->urlGenerationField(['first_name', 'last_name']);

        $this->crud->setDefaultValue('status', true);

        $this->crud->addColumn('expertise');
        $this->crud->multiRelation(
            'expertise',
            'people_expertise',
            'expertise',
            'person_id',
            'expertise_id',
            'title'
        );

        $this->crud->addColumn('tags');
        $this->crud->multiRelation(
            'tags',
            'people_tag',
            'tags',
            'person_id',
            'tag_id',
            'title'
        );

        $this->crud->columns('image', 'first_name', 'last_name', 'path', 'status');

        $this->crud->excludeColumns('author_id');

        $this->crud->columnGroup(
            'main',
            [
                'first_name', 'last_name', 'path', 'image', 'email', 'phone', 'linkedin', 'job_title', 'location',
                'biography', 'related_expertise_override', 'tags'
            ]
        );
        $this->crud->columnGroup('side', ['status']);
    }

    public function getOurPeopleDefaultLabels()
    {
        return OurPeopleConfig::$labels;
    }

    public function getOurPeopleDefaultHelp()
    {
        return OurPeopleConfig::$help;
    }

    public function getOurPeopleAssetConfig($field_name)
    {
        return OurPeopleConfig::$image_sizes[$field_name] ?? [];
    }

    public function our_people_block_settings()
    {
        $this->crud->setTable('our_people_block_settings');
        $this->crud->setSubject('Our People Block Defaults');
        $this->crud->single();

        $this->crud->columnGroup('main', ['heading', 'content', 'image']);
    }
    public function getOurPeopleBlockSettingsDefaultLabels()
    {
        return OurPeopleBlockSettingsConfig::$labels;
    }
    public function getOurPeopleBlockSettingsDefaultHelp()
    {
        return OurPeopleBlockSettingsConfig::$help;
    }
    public function getOurPeopleBlockSettingsAssetConfig($field_name)
    {
        return isset(OurPeopleBlockSettingsConfig::$image_sizes[$field_name]) ? OurPeopleBlockSettingsConfig::$image_sizes[$field_name] : [];
    }
}