<?php

namespace Modules\Tags\Config;

use Adm\Interfaces\NavigationInterface;

class Navigation implements NavigationInterface
{
    public function getNavItems()
    {
        return [
            [
                'title' => 'Tags',
                'url' => route('adm.path', 'tags'),
                'position' => 70,
            ]
        ];
    }
}