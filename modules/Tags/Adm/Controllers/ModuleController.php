<?php

namespace Modules\Tags\Adm\Controllers;

use Adm\Controllers\AdmController;

class ModuleController extends AdmController
{
    public function tags()
    {
        $this->crud->setTable('tags');
        $this->crud->setSubject('Tags');

        $this->crud->columns('title');

        $this->crud->columnGroup('main', ['title']);
    }
}