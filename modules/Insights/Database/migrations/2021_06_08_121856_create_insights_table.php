<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insights', function (Blueprint $table) {
            $table->integer('insight_id');
            $table->integer('language_id');
            $table->integer('version');
            $table->string('title');
            $table->text('introduction');
            $table->text('body');
            $table->string('image');
            $table->integer('author_id')->nullable();
            $table->string('author')->nullable();
            $table->string('type')->nullable();
            $table->string('region')->nullable();
            $table->integer('category_1')->default(0)->nullable();
            $table->integer('category_2')->default(0)->nullable();
            $table->boolean('status')->default(1);
            $table->date('published_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->primary(['insight_id', 'language_id', 'version'], 'insights_pk');
        });

        Schema::create('insight_categories', function (Blueprint $table) {
            $table->integer('category_id');
            $table->integer('language_id');
            $table->integer('version');
            $table->string('title');
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->primary(['category_id', 'language_id', 'version'], 'insight_categories_pk');
        });

        Schema::create('insight_tags', function (Blueprint $table) {
            $table->id('tag_id');
            $table->string('title');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('insight_tag', function (Blueprint $table) {
            $table->integer('insight_id');
            $table->integer('tag_id');
            $table->primary(['insight_id', 'tag_id'], 'insight_tag_pk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insights');
        Schema::dropIfExists('insight_categories');
        Schema::dropIfExists('insight_tags');
        Schema::dropIfExists('insight_tag');
    }
}
