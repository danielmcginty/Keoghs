<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelatedInsightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insight_to_related_insight', function (Blueprint $table) {
            $table->integer('insight_id');
            $table->integer('related_insight_id');
            $table->integer('sort_order')->default(0);
            $table->primary(['insight_id', 'related_insight_id'], 'i2ir_pk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insight_to_related_insight');
    }
}
