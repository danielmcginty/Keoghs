<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyInsightsAddCategoryPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('insights', function (Blueprint $table) {
            $table->dropColumn(['category_1', 'category_2']);
        });

        Schema::create('insight_category', function (Blueprint $table) {
            $table->integer('insight_id');
            $table->integer('category_id');
            $table->primary(['insight_id', 'category_id'], 'insight_category_pk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('insights', function (Blueprint $table) {
            $table->integer('category_1')->default(0)->nullable();
            $table->integer('category_2')->default(0)->nullable();
        });

        Schema::dropIfExists('insight_category');
    }
}
