<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AmendInsightCategoriesTableAddHiddenField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('insight_categories', function (Blueprint $table) {
            $table->boolean('hidden_in_listings')->default(0)->after('title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('insight_categories', function (Blueprint $table) {
            $table->dropColumn('hidden_in_listings');
        });
    }
}
