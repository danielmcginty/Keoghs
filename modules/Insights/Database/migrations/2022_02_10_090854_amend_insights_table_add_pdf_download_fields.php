<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AmendInsightsTableAddPdfDownloadFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('insights', function (Blueprint $table) {
            $table->string('pdf_download')->nullable()->after('image');
            $table->string('pdf_download_text')->nullable()->after('pdf_download');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('insights', function (Blueprint $table) {
            $table->dropColumn('pdf_download');
            $table->dropColumn('pdf_download_text');
        });
    }
}
