<?php

namespace Modules\Insights\Adm\Controllers;

use Adm\Controllers\AdmController;
use App\Models\Url;
use Modules\Insights\Config\InsightCategoryConfig;
use Modules\Insights\Config\InsightConfig;

class ModuleController extends AdmController
{
    public function insights()
    {
        $this->crud->setTable('insights');
        $this->crud->setSubject('Insights');

        $this->crud->addColumn('path');
        $SystemPage = Url::getSystemPage('insight', 'index');
        if ($SystemPage) {
            $this->crud->setBasePathPrefix('pages', $SystemPage->page_id, 'page_id');
        }

        $this->crud->orderBy('published_at', 'desc');

        $this->crud->setDefaultValue('status', true);

        $this->crud->addColumn('tags');
        $this->crud->multiRelation(
            'tags',
            'insight_tag',
            'tags',
            'insight_id',
            'tag_id',
            'title'
        );

        $this->crud->addColumn('categories');
        $this->crud->multiRelation(
            'categories',
            'insight_category',
            'insight_categories',
            'insight_id',
            'category_id',
            'title'
        );

        $this->crud->setRelation(
            'author_id',
            'people',
            ['first_name', 'last_name'],
            'person_id',
            0,
            'our-people'
        );

        $this->crud->addColumn('related_insights');
        $this->crud->multiRelation(
            'related_insights',
            'insight_to_related_insight',
            'insights',
            'insight_id',
            'insight_id',
            'title',
            'sort_order',
            'insights',
            'related_insight_id'
        );

        $this->crud->changeType('pdf_download', 'file');

        $this->crud->columns('image', 'title', 'path', 'status', 'published_at');

        $this->crud->columnGroup(
            'main',
            ['title', 'path', 'content', 'image', 'type', 'region', 'categories', 'tags', 'related_insights', 'introduction', 'body', 'pdf_download', 'pdf_download_text']
        );
        $this->crud->columnGroup('side', ['author_id', 'author', 'status', 'published_at']);
    }

    public function getInsightsDefaultLabels()
    {
        return InsightConfig::$labels;
    }

    public function getInsightsDefaultHelp()
    {
        return InsightConfig::$help;
    }

    public function getInsightsAssetConfig($field_name)
    {
        return InsightConfig::$image_sizes[$field_name] ?? [];
    }

    public function insight_categories()
    {
        $this->crud->setTable('insight_categories');
        $this->crud->setSubject('Insight Categories');

        $this->crud->setDefaultValue('status', true);

        $this->crud->columns('title', 'path', 'hidden_in_listings', 'status');

        $this->crud->columnGroup('main', ['title', 'path']);
        $this->crud->columnGroup('side', ['status', 'hidden_in_listings']);
    }

    public function getInsightCategoriesDefaultLabels()
    {
        return InsightCategoryConfig::$labels;
    }

    public function getInsightCategoriesDefaultHelp()
    {
        return InsightCategoryConfig::$help;
    }
}