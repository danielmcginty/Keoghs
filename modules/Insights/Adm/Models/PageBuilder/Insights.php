<?php

namespace Modules\Insights\Adm\Models\PageBuilder;

use Adm\Crud\LaraCrud;
use Adm\Models\PageBuilder\PageBuilderBase;
use App\Models\Scopes\LanguageScope;
use Modules\Insights\Theme\Models\Insight;

class Insights extends PageBuilderBase
{
    public $display_name = 'Insights';

    public $description = 'Add a set of Insights to the page';

    public $builder_fields = [
        'heading' => [
            'display_name' => 'Heading',
            'help' => 'Add a heading to the block',
            'type' => 'string',
            'required' => false,
            'validation' => [],
            'group' => 'Main'
        ],
        'insights' => [
            'display_name' => 'Insights',
            'help' => 'Select the Insights to display, or leave empty to show the latest 6',
            'type' => 'orderable_multiple_select',
            'options' => [],
            'required' => false,
            'validation' => [],
            'group' => 'Main'
        ]
    ];

    public function callbackBeforePrepare(LaraCrud $crud)
    {
        $options = [];
        $insights = Insight::query()
            ->withoutGlobalScope(LanguageScope::class)
            ->where('language_id', $crud->language_id)
            ->orderBy('title')
            ->get();

        foreach ($insights as $insight) {
            $options[$insight->insight_id] = $insight->title;
        }
        $this->builder_fields['insights']['options'] = $options;
    }
}