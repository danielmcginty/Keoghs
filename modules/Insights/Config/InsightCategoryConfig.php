<?php

namespace Modules\Insights\Config;

class InsightCategoryConfig
{
    /**
     * @var array
     */
    public static $labels = [
        'title'                 => 'Title',
        'hidden_in_listings'    => 'Hidden in listings',
        'status'                => 'Status',
    ];

    /**
     * @var array
     */
    public static $help = [
        'title'                 => 'The title of this insight category',
        'hidden_in_listings'    => 'Toggle to on/green to hide this category from the website - and only use it to link insights within the CMS'
    ];
}