<?php

namespace Modules\Insights\Config;

class InsightConfig
{
    /**
     * @var array
     */
    public static $labels = [
        'title' => 'Title',
        'content' => 'Content',
        'image' => 'Image',
        'type' => 'Type',
        'region' => 'Region',
        'category_1' => 'Category 1',
        'category_2' => 'Category 2',
        'tags' => 'Tags',
        'author_id' => 'Author',
        'author' => 'Author Name',
        'status' => 'Status',
        'published_at' => 'Published Date',
        'related_insights'  => 'Related Insights',
        'pdf_download'      => 'PDF Download',
        'pdf_download_text' => 'PDF Download Button Text',
    ];

    /**
     * @var array
     */
    public static $help = [
        'title' => 'The title of this insight article',
        'content' => 'The content of this insight article',
        'category_1' => 'The first category to which this article belongs',
        'category_2' => 'The second category to which this article belongs',
        'tags' => 'All the tags which are associated with this article',
        'author_id' => 'The author of this article',
        'author' => 'The name of the author of this article',
        'related_insights'  => 'Select a specific set of related insights to display on this page',
        'pdf_download'      => 'Select a PDF to assign as a download for this insight',
        'pdf_download_text' => 'Set the text to display on the PDF download button, or leave empty to use \'Download PDF\''
    ];

    /**
     * @var array
     */
    public static $image_sizes = [
        'image' => [
            'article' => [
                'label' => 'Banner',
                'width' => 1920,
                'height' => 640,
                'primary' => true
            ],
            'listing' => [
                'label' => 'Listing',
                'width' => 720,
                'height' => 405,
                'primary' => false
            ]
        ]
    ];

    public static $sort_options = [
        'latest' => [
            'label' => 'Latest',
            'field' => 'published_at',
            'direction' => 'desc',
        ],
        'oldest' => [
            'label' => 'Oldest',
            'field' => 'published_at',
            'direction' => 'asc',
        ],
        'a-z' => [
            'label' => 'A-Z',
            'field' => 'title',
            'direction' => 'asc',
        ],
        'z-a' => [
            'label' => 'Z-A',
            'field' => 'title',
            'direction' => 'desc',
        ],
    ];
}