<?php

namespace Modules\Insights\Config;

use Adm\Interfaces\NavigationInterface;

class Navigation implements NavigationInterface
{
    public function getNavItems()
    {
        return [
            [
                'title' => 'Insights',
                'url' => route('adm.path', 'insights'),
                'position' => 50,
                'sub' => [
                    ['title' => 'Insights', 'url' => route('adm.path', 'insights'), 'position' => 0],
                    ['title' => 'Categories', 'url' => route('adm.path', 'insight-categories'), 'position' => 10],
                ]
            ]
        ];
    }
}