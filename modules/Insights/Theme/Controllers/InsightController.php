<?php

namespace Modules\Insights\Theme\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Url;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Modules\Expertise\Theme\Models\Expertise;
use Modules\Insights\Config\InsightConfig;
use Modules\Insights\Theme\Models\Insight;
use Modules\Insights\Theme\Models\InsightCategory;

class InsightController extends Controller
{
    public function index(Request $request)
    {
        $this_page = Url::getSystemPage('insight', 'index');

        if (!$this_page) {
            return;
        }

        $home = Url::getSystemPage('home', 'index');
        if ($home) {
            $this->addBreadcrumb(route('theme', ['url' => $home->url]), $home->getAttributes()['title']);
        }
        $this->addBreadcrumb(route('theme', ['url' => $this_page->url]), $this_page->getAttributes()['title']);

        $insight_data = $this->getInsightData($request);

        $this->this_page = $this_page;
        $this->landing_page = $this_page;
        $this->insights = $insight_data['insights'];
        $this->selected_categories = $insight_data['selected_categories'];
        $this->selected_sort_key = $insight_data['selected_sort_key'];
        $this->selected_sort = $insight_data['selected_sort'];
        $this->search_term = $insight_data['search_term'];
        $this->top_categories = $this->getTopInsightCategories();
        $this->categories = $this->getInsightCategories();
        $this->sort_options = $this->getSortOptions();

        $this->view = 'insight.archive';
    }

    public function article($id)
    {
        $this_page = Insight::query()->where('insight_id', $id)->first();

        if (!$this_page) {
            return;
        }

        $home = Url::getSystemPage('home', 'index');
        if ($home) {
            $this->addBreadcrumb(route('theme', ['url' => $home->url]), $home->getAttributes()['title']);
        }

        $insight_page = Url::getSystemPage('insight', 'index');
        $this->addBreadcrumb(route('theme', [$insight_page->url]), $insight_page->title);

        $this->addBreadcrumb(route('theme', [$this_page->url]), $this_page->title);

        $this->canonical_url = route('theme', $this_page->url);
        $this->landing_page = $insight_page;
        $this->this_page = $this_page;

        if (!empty($this_page->image) && !empty($this_page->image['article'])) {
            $this->addSocialMeta('og:image', $this_page->image['article']['src']);
            $this->addSocialMeta('og:image:width', $this_page->image['article']['width']);
            $this->addSocialMeta('og:image:height', $this_page->image['article']['height']);

            $this->addSocialMeta('twitter:image', $this_page->image['article']['src']);
        }

        $this->author = $this->getArticleAuthor($this_page);
        $this->related_insights = $this->getRelatedInsights($this_page);
        $this->insights_page = Url::getSystemPage('insight', 'index');
        $this->other_expertise = $this->getOtherExpertise($this_page);

        $this->view = 'insight.article';
    }

    public function loadMoreInsights(Request $request)
    {
        return $this->getInsightData($request, true);
    }

    public function filterInsights(Request $request)
    {
        return $this->getInsightData($request, true);
    }

    private function getInsightData(Request $request, $render = false)
    {
        $selected_categories = $request->has('categories') ? explode(',', $request->query('categories')) : [];

        $selected_sort_key = $request->query('sort-by', 'latest');
        $selected_sort = InsightConfig::$sort_options[$selected_sort_key] ?? null;

        $search_term = $request->query('term');

        $insights = Insight::query()
            ->when(!empty($selected_categories), function ($query) use ($selected_categories) {
                foreach ($selected_categories as $category) {
                    if (empty($category)) {
                        continue;
                    }

                    $query->orWhere(function ($query) use ($category) {
                        return $query->withCategory($category);
                    });
                }
            })
            ->when(!empty($selected_sort), function ($query) use ($selected_sort) {
                return $query->orderBy($selected_sort['field'], $selected_sort['direction']);
            })
            ->when(!empty($search_term), function ($query) use ($search_term) {
                return $query->where('insights.title', 'LIKE', '%' . $search_term . '%')
                    ->orWhere('insights.body', 'LIKE', '%' . $search_term . '%')
                    ->orWhere('insights.introduction', 'LIKE', '%' . $search_term . '%');
            })
            ->paginate(10);

        $rendered_insights = '';
        if ($render) {
            $landing_page = Url::getSystemPage('insight', 'index');
            foreach ($insights as $Insight) {
                $rendered_insights .= view('insight.partials.ajax-tile', ['insight' => $Insight, 'landing_page' => $landing_page])->render();
            }
        }

        return [
            'selected_categories' => $selected_categories,
            'selected_sort_key' => $selected_sort_key,
            'selected_sort' => $selected_sort,
            'search_term' => $search_term,
            'insights' => $insights,
            'rendered_insights' => $rendered_insights
        ];
    }

    private function getInsightCategories()
    {
        return InsightCategory::query()->where('hidden_in_listings', false)->orderBy('title')->get();
    }

    private function getTopInsightCategories()
    {
        $order = [
            'Casualty',
            'Complex',
            'Corporate',
            'Legacy',
            'Motor',
            'Keoghs News',
            'Covid-19',
            'Corporate & Sector Risks'
        ];

        $TopCats = InsightCategory::whereIn('title', $order)->where('hidden_in_listings', false)->get();
        $TopCats = $TopCats->sort( function ($a, $b) use ($order) {
            $a_order = array_search($a->title, $order);
            $b_order = array_search($b->title, $order);

            if ($a_order == $b_order) {
                return 0;
            }
            return ($a_order < $b_order) ? -1 : 1;
        });
        return $TopCats;
    }

    private function getSortOptions()
    {
        return InsightConfig::$sort_options;
    }

    private function getArticleAuthor(Insight $insight)
    {
        return $insight->person ?: (object) [
            'full_name' => $insight->author
        ];
    }

    private function getRelatedInsights(Insight $insight)
    {
        if ($insight->related_insights) {
            return $insight->related_insights;
        }
        
        return Insight::query()
            ->withCategories(
                $insight->categories->map(function ($category) {
                    return $category->category_id;
                })->toArray()
            )
            ->withTags(
                $insight->tags->map(function ($tag) {
                    return $tag->tag_id;
                })->toArray()
            )
            ->orderBy('published_at', 'desc')
            ->take(2)
            ->get();
    }

    private function getOtherExpertise(Insight $insight)
    {
        $Expertise = new Collection();
        $TopLevelExpertise = Expertise::query()->topLevel()->orderBy('title')->get();
        foreach ($TopLevelExpertise as $Article) {
            if (!empty($Article->svg_icon) || (!empty($Article->icon) && !empty($Article->icon['primary']))) {
                $Expertise->push($Article);
            }
        }
        return $Expertise;
    }
}