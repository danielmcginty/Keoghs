<?php

namespace Modules\Insights\Theme\Models\PageBuilder;

use App\Models\PageBuilder\BaseBlockModel;
use App\Models\Url;
use Illuminate\Support\Collection;
use Modules\Insights\Theme\Models\Insight;

class Insights extends BaseBlockModel
{
    public function getValue($page_builder_block, $field, $original_value)
    {
        switch ($field) {
            case 'insights':
                $Insights = new Collection();
                if (is_json($original_value) && !empty($original_value)) {
                    foreach ((array) json_decode($original_value, true) as $raw_insight) {
                        $Insight = Insight::where('insight_id', $raw_insight['value'])->first();
                        if ($Insight) {
                            $Insights->push($Insight);
                        }
                    }
                }
                if ($Insights->isEmpty()) {
                    $Insights = Insight::orderBy('published_at', 'desc')->limit(6)->get();
                }
                return $Insights;
            default:
                return parent::getValue($page_builder_block, $field, $original_value);
        }
    }

    public function postProcess($fields)
    {
        $fields['landing_page'] = Url::getSystemPage('insight', 'index');

        if (!isset($fields['heading']) || empty($fields['heading'])) {
            $fields['heading'] = "Insight";
        }

        return $fields;
    }
}