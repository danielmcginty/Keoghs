<?php

namespace Modules\Insights\Theme\Models;

use App\Helpers\AssetHelper;
use App\Models\CommonModel;
use App\Models\File;
use App\Models\Scopes\LanguageScope;
use App\Models\Scopes\PublishedScope;
use App\Models\Traits\HasCompositePrimaryKey;
use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Insights\Config\InsightConfig;
use Modules\OurPeople\Theme\Models\Person;
use Modules\Tags\Theme\Models\Tag;

class Insight extends CommonModel
{
    use SoftDeletes, HasCompositePrimaryKey, HasQueryBuilderCache;

    protected $table = 'insights';

    protected $primaryKey = ['insight_id', 'language_id'];

    protected $dates = [
        'published_at',
        'created_at',
        'updated_at',
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new LanguageScope);
        static::addGlobalScope(new PublishedScope);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'insight_tag', 'insight_id', 'tag_id', 'insight_id', 'tag_id');
    }

    public function categories()
    {
        return $this->belongsToMany(InsightCategory::class, 'insight_category', 'insight_id', 'category_id', 'insight_id', 'category_id');
    }

    public function person()
    {
        return $this->belongsTo(Person::class, 'author_id', 'person_id');
    }

    public function related_insights()
    {
        return $this->belongsToMany(Insight::class, 'insight_to_related_insight', 'insight_id', 'related_insight_id')->orderBy('insight_to_related_insight.sort_order');
    }

    public function getVisibleCategoriesAttribute()
    {
        return $this->categories()->where('hidden_in_listings', false)->get();
    }

    public function scopeWithCategory($query, $category)
    {
        return $query->whereHas('categories', function ($query) use ($category) {
            return $query->where('insight_categories.category_id', $category);
        });
    }

    public function scopeWithCategories($query, $categories)
    {
        return $query->whereHas('categories', function ($query) use ($categories) {
            return $query->whereIn('insight_categories.category_id', $categories);
        });
    }

    public function scopeWithTag($query, $tag)
    {
        return $query->whereHas('tags', function ($query) use ($tag) {
            return $query->where('tags.title', $tag);
        });
    }

    public function scopeWithTags($query, $tags)
    {
        return $query->whereHas('tags', function ($query) use ($tags) {
            return $query->whereIn('tags.tag_id', $tags);
        });
    }

    public function getImageAttribute()
    {
        $AssetHelper = new AssetHelper();
        $Image_Sizes = InsightConfig::$image_sizes;
        $Image_Sizes['image']['schema'] = [
            'label' => 'Schema Image',
            'width' => 696,
            'height' => 392,
            'primary' => false
        ];
        $Image_Sizes['image']['amp'] = [
            'label' => 'AMP',
            'width' => 640,
            'height' => 350,
            'primary' => false
        ];
        return $AssetHelper->getAssets(
            $this->table,
            $this->insight_id,
            $this->language_id,
            $this->version,
            'image',
            $Image_Sizes['image']
        );
    }

    public function getPdfDownloadAttribute($value)
    {
        return File::find($value);
    }
    public function getPdfDownloadTextAttribute($value)
    {
        return !empty($value) ? $value : 'Download PDF';
    }

    public function getKeyName()
    {
        return 'insight_id';
    }
}