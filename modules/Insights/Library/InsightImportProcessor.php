<?php

namespace Modules\Insights\Library;

use App\Models\AssetGroup;
use App\Models\File;
use App\Models\FileDirectory;
use App\Models\Url;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Modules\Insights\Theme\Models\InsightCategory;
use Modules\Tags\Theme\Models\Tag;

class InsightImportProcessor
{
    const BASE_IMAGE_URL = "https://keoghs.co.uk";
    const ASSET_PATH = "/assets/site/content/images/";

    private $categories = [];
    private $tags = [];
    private $prefix_url = null;

    public function process()
    {
        $this->processFileContents(
            $this->parseNewDataFile($this->locateDataFile('modules/Insights/assets/new_insight_data.csv')),
            $this->parseOldDataFile($this->locateDataFile('modules/Insights/assets/old_insight_data.csv')),
        );
    }

    public function process_new()
    {
        $this->processAppendFileContents(
            $this->parseNewDataFile($this->locateDataFile('modules/Insights/assets/launch_new_insight_data.csv')),
            $this->parseOldDataFile($this->locateDataFile('modules/Insights/assets/launch_old_insight_data.csv'))
        );
    }

    public function process_additional_categories()
    {
        $this->processAdditionalCategories(
            $this->parseNewDataFile($this->locateDataFile('modules/Insights/assets/launch_new_insight_data.csv')),
            $this->parseOldDataFile($this->locateDataFile('modules/Insights/assets/launch_old_insight_data.csv'))
        );
    }

    private function locateDataFile(string $filename)
    {
        return fopen(base_path($filename), 'r');
    }

    private function parseNewDataFile($file): array
    {
        $processed_header = false;

        $num_processed = 0;

        $insights = [];

        $this->log('Processing New Insight Data CSV ...');

        while (($row = fgetcsv($file)) !== false) {
            if (!$processed_header) {
                $processed_header = true;
                continue;
            }

            $num_processed++;

            $row = $this->mapNewDataFileFields($row);

            if (empty($row['ID'])) {
                continue;
            }

            $insights[$row['ID']] = [
                'id' => $row['ID'],
                'title' => $row['TITLE'],
                'slug' => $row['SLUG'],
                'status' => $row['STATUS'],
                'author_id' => $row['AUTHOR_ID'],
                'author' => $row['AUTHOR'],
                'type' => $row['TYPE'],
                'region' => $row['REGION'],
                'categories' => [
                    $row['NEW_CATEGORY_1'] ?: null,
                    $row['NEW_CATEGORY_2'] ?: null,
                    $row['NEW_CATEGORY_3'] ?: null,
                    $row['NEW_CATEGORY_4'] ?: null,
                    $row['NEW_CATEGORY_5'] ?: null,
                    $row['NEW_CATEGORY_6'] ?: null,
                ],
                'tags' => [
                    $row['TAG_1'] ?: null,
                    $row['TAG_2'] ?: null,
                    $row['TAG_3'] ?: null,
                    $row['TAG_4'] ?: null,
                    $row['TAG_5'] ?: null,
                    $row['TAG_6'] ?: null,
                ],
            ];
        }

        $this->log("New Insight Data CSV Processed - {$num_processed} rows processed");

        return $insights;
    }

    private function parseOldDataFile($file): array
    {
        $processed_header = false;

        $num_processed = 0;

        $insights = [];

        $this->log('Processing Old Insight Data CSV ...');

        while (($row = fgetcsv($file)) !== false) {
            if (!$processed_header) {
                $processed_header = true;
                continue;
            }

            $num_processed++;

            $row = $this->mapOldDataFileFields($row);

            if (empty($row['ID'])) {
                continue;
            }

            $insights[$row['ID']] = [
                'id' => $row['ID'],
                'introduction' => $row['INTRODUCTION'],
                'body' => $row['BODY'],
                'image_url' => static::BASE_IMAGE_URL.$row['IMAGE_PATH'],
                'image_name' => $row['IMAGE_NAME'],
                'published_at' => $row['PUBLISH_DATE'],
                'created_at' => $row['CREATED_AT'],
                'updated_at' => $row['UPDATED_AT'],
            ];
        }

        $this->log("Old Insight Data CSV Processed - {$num_processed} rows processed");

        return $insights;
    }

    private function processFileContents(array $new_insight_data, array $old_insight_data)
    {
        $this->syncInsights($new_insight_data, $old_insight_data);
    }

    private function processAppendFileContents(array $new_insight_data, array $old_insight_data)
    {
        $this->appendSyncInsights($new_insight_data, $old_insight_data);
    }

    private function processAdditionalCategories(array $new_insight_data, array $old_insight_data)
    {
        $insights = [];

        foreach ($new_insight_data as $key => $insight) {
            if (!isset($old_insight_data[$key])) {
                throw new \Exception("Old Data does not exist for key: {$key}");
            }

            $insights[] = array_merge($insight, $old_insight_data[$key] ?? []);
        }

        $insights_collection = collect($insights);

        $this->log("Updating {$insights_collection->count()} Insights...");
        foreach ($insights_collection as $insight) {
            $Insight = DB::table('insights')
                ->where('insight_id', $insight['id'])
                ->first();
            if (!$Insight) {
                $this->log("Insight " . $insight['id'] . ' (' . $insight['title'] . ') not found. Skipping...');
                continue;
            }

            foreach ($insight['categories'] as $category) {
                $this->addCategoryToInsight($insight, $category);
            }
        }
        exit;
    }

    private function mapNewDataFileFields($row): array
    {
        $fields = [
            'ID',
            'TITLE',
            'SLUG',
            'STATUS',
            'CURRENT_CATEGORY',
            'AUTHOR_ID',
            'AUTHOR',
            'TYPE',
            'REGION',
            'TAG_1',
            'TAG_2',
            'TAG_3',
            'TAG_4',
            'TAG_5',
            'TAG_6',
            'NEW_CATEGORY_1',
            'NEW_CATEGORY_2',
            'NEW_CATEGORY_3',
            'NEW_CATEGORY_4',
            'NEW_CATEGORY_5',
            'NEW_CATEGORY_6',
        ];

        $mapped = [];

        foreach ($fields as $ind => $field) {
            $mapped[$field] = isset($row[$ind]) ? trim($row[$ind]) : null;

            if ($mapped[$field] === 'NULL') {
                $mapped[$field] = null;
            }
        }

        return $mapped;
    }

    private function mapOldDataFileFields($row): array
    {
        $fields = [
            'ID',
            'INTRODUCTION',
            'BODY',
            'IMAGE_PATH',
            'IMAGE_NAME',
            'PUBLISH_DATE',
            'CREATED_AT',
            'UPDATED_AT',
        ];

        $mapped = [];

        foreach ($fields as $ind => $field) {
            $mapped[$field] = isset($row[$ind]) ? trim($row[$ind]) : null;

            if ($mapped[$field] === 'NULL') {
                $mapped[$field] = null;
            }
        }

        return $mapped;
    }

    private function syncInsights(array $new_insight_data, array $old_insight_data): void
    {
        $this->log("Truncating Insights Table...");

        $files = DB::table('files')
            ->whereIn('id', function ($query) {
                return $query->select('image')->from('insights');
            })
            ->get();

        foreach ($files as $file) {
            Storage::delete($file->storage_path);
        }

        DB::table('files')
            ->whereIn('id', function ($query) {
                return $query->select('image')->from('insights');
            })
            ->delete();

        DB::table('asset_groups')
            ->where('table_name', 'insights')
            ->delete();

        DB::table('301_redirects')
            ->where('old_url', 'LIKE', 'keoghs-insight/%')
            ->delete();

        DB::table('urls')
            ->where('table_name', 'insights')
            ->delete();

        DB::table('insight_tag')->truncate();

        DB::table('insight_category')->truncate();

        DB::table('insight_categories')->truncate();

        DB::table('insights')->truncate();

        $this->log("Insights Table Truncated");

        $insights = [];

        foreach ($new_insight_data as $key => $insight) {
            if (!isset($old_insight_data[$key])) {
                throw new \Exception("Old Data does not exist for key: {$key}");
            }

            $insights[] = array_merge($insight, $old_insight_data[$key] ?? []);
        }

        $insights_collection = collect($insights);

        $this->log("Inserting {$insights_collection->count()} Insights...");

        $insights_collection->chunk(500)
            ->each(function (Collection $insights) {
                DB::table('insights')
                    ->insert($insights->map(function (array $insight) {
                        return [
                            'insight_id' => $insight['id'],
                            'language_id' => 1,
                            'version' => 1,
                            'title' => $insight['title'],
                            'introduction' => $insight['introduction'],
                            'body' => $insight['body'],
                            'image' => $this->loadExternalImage($insight),
                            'author_id' => $insight['author_id'],
                            'author' => $insight['author'],
                            'type' => $insight['type'],
                            'region' => $insight['region'],
                            'status' => $insight['status'],
                            'published_at' => $insight['published_at'],
                            'created_at' => $insight['created_at'],
                            'updated_at' => $insight['updated_at'],
                        ];
                    })->toArray());
            });

        $this->log("{$insights_collection->count()} Insights Inserted");

        $insights_collection->each(function (array $insight) {
            $this->insertUrl($insight);

            foreach ($insight['categories'] as $category) {
                $this->addCategoryToInsight($insight, $category);
            }

            foreach ($insight['tags'] as $tag) {
                $this->addTagToInsight($insight, $tag);
            }
        });
    }

    private function appendSyncInsights(array $new_insight_data, array $old_insight_data): void
    {
        $insights = [];

        foreach ($new_insight_data as $key => $insight) {
            if (!isset($old_insight_data[$key])) {
                throw new \Exception("Old Data does not exist for key: {$key}");
            }

            $insights[] = array_merge($insight, $old_insight_data[$key] ?? []);
        }

        $insights_collection = collect($insights);

        $this->log("Inserting {$insights_collection->count()} Insights...");

        // Only add images
        foreach ($insights_collection as $insight) {
            $image = $this->loadExternalImage($insight);
            if ($image) {
                $this->log('Image found for insight ' . $insight['id']);
                DB::table('insights')
                    ->where('insight_id', $insight['id'])
                    ->update([
                        'image' => $image
                    ]);
            } else {
                $this->log('Image not found for insight ' . $insight['id']);
            }
        }
        exit;

        $insights_collection->chunk(500)
            ->each(function (Collection $insights) {
                DB::table('insights')
                ->insert($insights->map(function (array $insight) {
                    return [
                        'insight_id' => $insight['id'],
                        'language_id' => 1,
                        'version' => 1,
                        'title' => $insight['title'],
                        'introduction' => $insight['introduction'],
                        'body' => $insight['body'],
                        'image' => $this->loadExternalImage($insight),
                        'author_id' => $insight['author_id'],
                        'author' => $insight['author'],
                        'type' => $insight['type'],
                        'region' => $insight['region'],
                        'status' => $insight['status'],
                        'published_at' => $insight['published_at'],
                        'created_at' => $insight['created_at'],
                        'updated_at' => $insight['updated_at'],
                    ];
                })->toArray());
            });

        $this->log("{$insights_collection->count()} Insights Inserted");

        $insights_collection->each(function (array $insight) {
            $this->insertUrl($insight);

            foreach ($insight['categories'] as $category) {
                $this->addCategoryToInsight($insight, $category);
            }

            foreach ($insight['tags'] as $tag) {
                $this->addTagToInsight($insight, $tag);
            }
        });
    }

    private function log(string $message)
    {
        info($message);
    }

    private function loadExternalImage(array $insight): ?string
    {
        $image_url = $insight['image_url'];
        $image_name = $insight['image_name'];

        if (strlen(trim($image_url)) === 0) {
            $this->log('Empty image URL...');
            return null;
        }

        if (strlen(trim($image_name)) === 0) {
            $image_name = $insight['title'];
        }

        $full_prefix = static::BASE_IMAGE_URL.static::ASSET_PATH;

        $relative_path = substr_replace($image_url, '', strpos($image_url, $full_prefix), strlen($full_prefix));

        $directory_path = trim(rtrim($relative_path, basename($relative_path)), '/');

        $directories = array_filter(explode('/', $directory_path));

        $directory = $this->createDirectoryPath($directories);

        // Attempt file get contents...
        $image = false;
        try {
            $image = file_get_contents($image_url);
        } catch (\Exception $e) {
            $this->log($e->getMessage());
            //return null;
        }
        // If not, try cURL
        if (!$image) {
            $ch = curl_init($image_url);
            curl_setopt_array($ch, [
                CURLOPT_RETURNTRANSFER  => true,
                CURLOPT_SSL_VERIFYPEER  => false,
                CURLOPT_SSL_VERIFYHOST  => false
            ]);
            $image = curl_exec($ch);
            curl_close($ch);
        }

        if (!$image) {
            return null;
        }

        sleep(0.5);

        return $this->storeImage($insight, $image, $relative_path, $image_name, $directory);
    }

    private function createDirectoryPath(array $directories)
    {
        $processed_directories = [];

        foreach ($directories as $index => $directory) {
            $fd = FileDirectory::firstOrCreate([
                'parent' => $index > 0 ? $processed_directories[$index - 1] : 0,
                'path' => Str::slug(urldecode($directory)),
                'name' => Str::title($directory)
            ]);

            $processed_directories[] = $fd->id;
        }

        return end($processed_directories);
    }

    private function storeImage($insight, $image, $path, $name, $directory)
    {
        $file_name = pathinfo($path, PATHINFO_FILENAME);

        $extension = pathinfo($path, PATHINFO_EXTENSION);

        Storage::put($uploadPath = Str::random(40).".".$extension, $image);

        if (empty($extension)) {
            $extension = 'txt';
        }

        // Make sure the file has an extension (just in case validation passes somehow)
        if (empty($extension)) {
            $extension = 'txt';
        }

        // Make sure that we use .jpg instead of .jpeg
        $extension = str_replace('jpeg', 'jpg', $extension);

        // And default the alt text to the generated name.
        $alt_text = $name;

        $new_file = new File();
        $new_file->directory_id = $directory;
        $new_file->storage_path = $uploadPath;
        $new_file->file_name = Str::slug(urldecode($file_name)).".".$extension;
        $new_file->name = $name;
        $new_file->alt_text = $alt_text;
        $new_file->extension = $extension;
        $new_file->created_by = 1;

        $new_file->save();

        $this->insertAsset($insight, 'article', $new_file);
        $this->insertAsset($insight, 'listing', $new_file);

        return $new_file->id;
    }

    private function insertAsset(array $insight, string $image_size, $new_file)
    {
        $asset = new AssetGroup();

        $asset->table_name = 'insights';
        $asset->table_key = $insight['id'];
        $asset->language_id = 1;
        $asset->version = 1;
        $asset->field = 'image';
        $asset->image_size = $image_size;
        $asset->file_id = $new_file->id;
        $asset->alt = $new_file->name;
        $asset->width = 0;
        $asset->height = 0;
        $asset->x_coordinate = 0;
        $asset->y_coordinate = 0;
        $asset->cropbox_left = 0;
        $asset->cropbox_top = 0;

        $asset->save();
    }

    private function addCategoryToInsight(array $insight, $category_name)
    {
        if (empty(trim($category_name))) {
            return;
        }

        if (isset($this->categories[$category_name])) {
            return $this->connectCategoryToInsight($insight['id'], $this->categories[$category_name]);
        }

        $category = InsightCategory::withoutGlobalScopes()->where('title', $category_name)->first();

        if (is_null($category)) {
            $category = new InsightCategory();
            $category->language_id = 1;
            $category->version = 1;
            $category->title = $category_name;
            $category->status = 1;
            $category->save();
        }

        $this->categories[$category_name] = $category->category_id;

        return $this->connectCategoryToInsight($insight['id'], $category->category_id);
    }

    private function connectCategoryToInsight($insight, $category)
    {
        DB::table('insight_category')->insertOrIgnore([
            'insight_id' => $insight,
            'category_id' => $category,
        ]);
    }

    private function addTagToInsight(array $insight, $tag_name)
    {
        if (empty(trim($tag_name))) {
            return;
        }

        if (isset($this->tags[$tag_name])) {
            return $this->connectTagToInsight($insight['id'], $this->tags[$tag_name]);
        }

        $tag = Tag::withoutGlobalScopes()->where('title', $tag_name)->first();

        if (is_null($tag)) {
            $tag = new Tag();
            $tag->title = $tag_name;
            $tag->save();
        }

        $this->tags[$tag_name] = $tag->tag_id;

        return $this->connectTagToInsight($insight['id'], $tag->tag_id);
    }

    private function connectTagToInsight($insight, $tag)
    {
        DB::table('insight_tag')->insertOrIgnore([
            'insight_id' => $insight,
            'tag_id' => $tag,
        ]);
    }

    private function insertUrl(array $insight)
    {
        if (empty($this->prefix_url)) {
            $this->prefix_url = Url::getSystemPage('insight', 'index');
        }

        $new_slug = Str::slug($insight['slug']);

        DB::table('urls')->insert([
            'url' => $url = "{$this->prefix_url->url}/{$new_slug}",
            'table_name' => 'insights',
            'table_key' => $insight['id'],
            'language_id' => 1,
            'meta_title' => $insight['title'],
        ]);

        DB::table('301_redirects')->insert([
            'old_url' => "keoghs-insight/{$insight['type']}/{$insight['slug']}",
            'new_url' => $url,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}