<?php

namespace Modules\HomePageSliderBlock\Theme\Models\PageBuilder;

use App\Helpers\AssetHelper;
use App\Helpers\LinkHelper;
use App\Models\PageBuilder\BaseBlockModel;
use Modules\HomePageSliderBlock\Adm\Models\PageBuilder\HomePageSlider as AdmBlock;

class HomePageSlider extends BaseBlockModel
{
    public function getValue($page_builder_block, $field, $original_value)
    {
        if ($field == 'slides') {
            $raw_slides = is_json($original_value) ? json_decode($original_value, true) : [];
            $slides = [];

            if (!empty($raw_slides)) {
                $AssetHelper = new AssetHelper();
                $AdmBlock = new AdmBlock();

                foreach ($raw_slides as $sort_order => $slide) {
                    if (!empty($slide['link'])) {
                        $slide['link'] = LinkHelper::parseLink(json_decode($slide['link'], true));
                    }

                    if (!empty($slide['background'])) {
                        $slide['background'] = $AssetHelper->getAssets(
                            $page_builder_block->table_name,
                            $page_builder_block->table_key,
                            $page_builder_block->language_id,
                            $page_builder_block->version,
                            'page_builder.'.$page_builder_block->sort_order.'.content.'.$field.'.'.$sort_order.'.background',
                            $AdmBlock->column_extras[$field]['repeatable_fields']['background']['sizes']
                        );
                    }

                    $slides[] = $slide;
                }
            }

            return $slides;
        }

        return is_serialized($original_value) ? unserialize($original_value) : $original_value;
    }
}