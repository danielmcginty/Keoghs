<?php

namespace Modules\HomePageSliderBlock\Adm\Models\PageBuilder;

use Adm\Models\PageBuilder\PageBuilderBase;

class HomePageSlider extends PageBuilderBase
{
    public $display_name = 'Home Page Slider';

    public $description = 'The main home page slider';

    public $builder_fields = [
        'slides' => [
            'display_name' => 'Slides',
            'help' => 'Add Slides',
            'type' => 'repeatable',
            'required' => false,
            'validation' => []
        ],
    ];

    public $column_extras = [
        'slides' => [
            'repeatable_fields' => [
                'title' => [
                    'label' => 'Title',
                    'type' => 'string',
                ],
                'subtitle' => [
                    'label' => 'Subtitle',
                    'type' => 'textarea',
                ],
                'link' => [
                    'label' => 'Link',
                    'type' => 'link',
                ],
                'link_text' => [
                    'label' => 'Link Label',
                    'type' => 'string',
                ],
                'background' => [
                    'label' => 'Background Image',
                    'type' => 'asset',
                    'multi_size' => true,
                    'sizes' => [
                        'desktop' => [
                            'label' => 'Desktop',
                            'width' => 1920,
                            'height' => 0,
                            'primary' => true
                        ],
                        'mobile' => [
                            'label' => 'Mobile',
                            'width' => 640,
                            'height' => 600,
                            'primary' => false
                        ],
                    ]
                ]
            ]
        ],
    ];
}