<?php
namespace Modules\FormBuilder\Mail;

use App\Models\Mailable\CommonMailable;

/**
 * Class CustomFormAutoResponderMail
 * @package Modules\FormBuilder\Mail
 *
 * Mail Class used to send an email to the user filling out a custom form
 */
class CustomFormAutoResponderMail extends CommonMailable {

    /**
     * The custom form that has been submitted
     *
     * @var \Modules\FormBuilder\Models\Form
     */
    private $form;

    /**
     * The HTML email message to send to the internal recipient
     *
     * @var string
     */
    private $mail_parts;

    /**
     * Create a new message instance.
     *
     * @param \Modules\FormBuilder\Models\Form $custom_form
     * @param string $to_address
     * @param array $mail_parts
     */
    public function __construct( $custom_form, $to_address, $mail_parts )
    {
        parent::__construct();

        $this->form = $custom_form;
        $this->to_address = $to_address;
        $this->mail_parts = $mail_parts;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if( view()->exists( 'emails.custom-form-auto-responder-email' ) ){
           $view = 'emails.custom-form-auto-responder-email';
        } else {
            view()->addNamespace('FormBuilder', base_path('/modules/FormBuilder/Theme/views'));
            $view = 'FormBuilder::emails.custom-form-auto-responder-email';
        }

        if( view()->exists( 'emails.custom-form-auto-responder-email_plain' ) ){
            $plain_text = 'emails.custom-form-auto-responder-email_plain';
        } else {
            view()->addNamespace('FormBuilder', base_path('/modules/FormBuilder/Theme/views'));
            $plain_text = 'FormBuilder::emails.custom-form-auto-responder-email_plain';
        }

        $mail = $this->view( $view )
            ->text( $plain_text )
            ->with('mail_parts', $this->mail_parts)
            ->with('form', $this->form)
            ->subject( $this->form->subject_to_submitter )
            ->to( ['address' => $this->to_address] );

        if( !empty( $this->form->attachment ) && is_object( $this->form->attachment ) ){
            $mail->attach( storage_path('app/' . $this->form->attachment->storage_path), ['as' => $this->form->attachment->file_name] );
        }

        return $mail;
    }
}
