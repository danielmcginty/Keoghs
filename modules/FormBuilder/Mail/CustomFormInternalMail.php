<?php
namespace Modules\FormBuilder\Mail;

use App\Models\Mailable\CommonMailable;

/**
 * Class CustomFormInternalMail
 * @package Modules\FormBuilder\Mail
 *
 * Mail Class used to send an email to the website admin after a user has filled out a custom form
 */
class CustomFormInternalMail extends CommonMailable {

    /**
     * The custom form that has been submitted
     *
     * @var \Modules\FormBuilder\Models\Form
     */
    private $form;

    /**
     * The HTML email message to send to the internal recipient
     *
     * @var string
     */
    private $mail_message;

    /**
     * The Plain Text email message to send to the internal recipient
     *
     * @var string
     */
    private $plain_text_message;

    private $person_email = null;

    /**
     * Create a new message instance.
     *
     * @param \Modules\FormBuilder\Models\Form $custom_form
     * @param string $mail_message
     */
    public function __construct( $custom_form, $mail_message, $plain_text_message, $person_email = null )
    {
        parent::__construct();

        $this->form = $custom_form;
        $this->mail_message = $mail_message;
        $this->plain_text_message = $plain_text_message;
        $this->person_email = $person_email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        view()->addNamespace('FormBuilder', base_path('/modules/FormBuilder/Theme/views'));

        $to_addresses = [];
        foreach( explode( ',', $this->form->recipient_email ) as $email_address ){
            $to_addresses[] = $email_address;
        }
        if ($this->person_email && filter_var($this->person_email, FILTER_VALIDATE_EMAIL)) {
            $to_addresses = [$this->person_email];
        }

        $mail = $this->view('FormBuilder::emails.custom-form-internal-email')
            ->text( 'FormBuilder::emails.custom-form-internal-email_plain' )
            ->with('mail_message', $this->mail_message)
            ->with('plain_text_message', $this->plain_text_message)
            ->subject( $this->form->recipient_subject )
            ->to( $to_addresses );

        if( $this->form->send_to_website_email ){
            $website_email = translate( 'website_email' );

            if( !empty( $website_email ) && filter_var( $website_email, FILTER_VALIDATE_EMAIL ) ) {
                $mail->cc( ['address' => $website_email] );
            }
        }

        return $mail;
    }
}
