<?php
namespace Modules\FormBuilder\Models;

use App\Helpers\LinkHelper;
use App\Models\File;
use App\Models\Traits\HasQueryBuilderCache;
use App\Models\Url;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\HasCompositePrimaryKey;

use Modules\FormBuilder\Mail\CustomFormAutoResponderMail;
use Modules\FormBuilder\Mail\CustomFormInternalMail;

class Form extends Model {
    // As we've got a deleted_at flag, a composite primary key, and want to cache DB calls, include the relevant traits
    use SoftDeletes, HasCompositePrimaryKey, HasQueryBuilderCache;

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'forms';

    /**
     * The primary keys in the table
     * @var array
     */
    protected $primaryKey = [ 'form_id', 'language_id' ];

    /**
     * The fields that are 'bulk assignable' for this model
     * @var array
     */
    protected $fillable = ['title', 'reference', 'structure', 'nest_limit'];

    /**
     * Any date fields for this model
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Any fields to be appended to the object should it be output as an array or JSON object.
     * @var array
     */
    protected $appends = ['email_field', 'emailable_cta_link', 'form_fields'];

    /**
     * Custom Accessor Function.
     * Returns an input field relating to an email address, called 'email', for quick reference.
     *
     * @return bool
     */
    public function getEmailFieldAttribute(){
        if( is_serialized( $this->structure ) ){ $this->structure = json_encode( unserialize( $this->structure ) ); }
        $fields = is_json( $this->structure ) ? json_decode( $this->structure, true ) : $this->structure;

        $email_field = false;
        foreach( (array)$fields as $field ){
            if( $field['type'] == 'email' && $email_field === false ){
                $email_field = $field['name'];
            }
        }

        return $email_field;
    }

    /**
     * Custom Accessor Function.
     * Simply returns the form's email attachment as a file, rather than returning the ID.
     *
     * @param $value
     * @return mixed
     */
    public function getAttachmentAttribute( $value ){
        if( !empty( $value ) ){
            $attachment = File::where('id', $value)->first();

            if( !empty( $attachment ) ){
                return $attachment;
            }
        }

        return $value;
    }

    /**
     * Quick function used to send the Internal Email Notification (to website admin) after a form submission.
     *
     * @param $mail_message
     * @param $plain_text_message
     */
    public function sendInternalNotification( $mail_message, $plain_text_message, $person_email = null ){
        \Mail::send( new CustomFormInternalMail( $this, $mail_message, $plain_text_message, $person_email ) );
    }

    /**
     * Quick function used to send an Auto-Response Email Notification (to the user) after a form submission.
     *
     * @param $to_address
     * @param $mail_parts
     */
    public function sendAutoResponderNotification( $to_address, $mail_parts ){
        \Mail::send( new CustomFormAutoResponderMail( $this, $to_address, $mail_parts ) );
    }

    /**
     * Custom Accessor Function used to return the CTA Link URL stored in the database, rather than returning a JSON
     * array for the link.
     *
     * @return array|bool
     */
    public function getEmailableCtaLinkAttribute(){
        $link = $this->cta_link;
        if( empty( $link ) ){ return false; }

        $link_array = json_decode( $link, true );
        $cta_link = LinkHelper::parseLink( $link_array );
        $cta_link = $cta_link['url'];

        return $cta_link;
    }

    /**
     * Eloquent Relationship.
     * Returns the Submissions for this form.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function submissions(){
        return $this->hasMany( 'Modules\\FormBuilder\\Models\\FormSubmission', 'form_id', 'form_id' );
    }

    /**
     * Quick function allowing us to replace template tags within email content with values provided from the form.
     *
     * @param $posted_data
     */
    public function replaceTemplateTags( $posted_data ){
        $replacements = [];
        foreach( $posted_data as $field => $value ){
            $replacements[ '[[' . $field . ']]' ] = $value;
        }

        foreach( $replacements as $search_for => $replace_with ){
            $this->above_cta_content = str_replace( $search_for, $replace_with, $this->above_cta_content );
            $this->below_cta_content = str_replace( $search_for, $replace_with, $this->below_cta_content );
            $this->closing_remark = str_replace( $search_for, $replace_with, $this->closing_remark );
        }
    }

    /**
     * Custom Accessor Function.
     * Used to return the full redirect URL assigned to the form as a success state, rather than the URL ID.
     *
     * @return bool|string
     */
    public function getSuccessPageRedirectAttribute(){
        if ($this->success_page_url_id) {
            $url = Url::find($this->success_page_url_id);

            if (!empty($url) && !empty($url->object)) {
                return route('theme', $url->url);
            }
        }

        return false;
    }

    /**
     * Custom Mutator Function.
     * When accessing the 'use_invisible_recaptcha' attribute, set this to disabled (i.e. unavailable) if we don't
     * have a Google ReCaptcha API Key set up.
     *
     * @param $value
     * @return bool
     */
    public function getUseInvisibleRecaptchaAttribute( $value ){
        if( $value ){
            $site_key = env('GOOGLE_INVISIBLE_RECAPTCHA_SITE_KEY');
            if( !$site_key ){
                $value = false;
            }
        }

        return $value;
    }

    /**
     * Custom Accessor Function.
     * Allows us to return the FULL GDPR consent text from the form, without having to echo 2 strings
     *
     * @return string
     */
    public function getFullGdprComplianceTextAttribute(){
        $text = $this->gdpr_compliance_text;
        if( $this->gdpr_compliance_opt_out_text ){
            $text .= ' ' . $this->gdpr_compliance_opt_out_text;
        }

        return $text;
    }

    public function getSubmitButtonTextAttribute( $value ){
        if( !empty( $value ) ){
            return $value;
        }

        return 'Submit';
    }

    public function getFormFieldsAttribute(){
        if( is_serialized( $this->structure ) ){ $this->structure = json_encode( unserialize( $this->structure ) ); }
        return is_json( $this->structure ) ? json_decode( $this->structure, true ) : [];
    }
}