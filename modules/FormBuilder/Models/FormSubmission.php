<?php
namespace Modules\FormBuilder\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Crypt;

class FormSubmission extends Model {
    // As we've got a deleted_at flag include the relevant trait
    use SoftDeletes;

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'forms_log';

    /**
     * The primary keys in the table
     * @var array
     */
    protected $primaryKey = 'forms_log_id';

    /**
     * The fields that are 'bulk assignable' for this model
     * @var array
     */
    protected $fillable = ['form_id', 'language_id', 'url', 'ip_address', 'fields', 'gdpr_consent', 'gdpr_consent_text', 'gdpr_consent_date'];

    /**
     * Any date fields for this model
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Eloquent Relationship.
     * Returns the form that this submission was for.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function form(){
        return $this->belongsTo('\\Modules\\FormBuilder\\Models\\Form', 'form_id', 'form_id');
    }

    /**
     * Custom Mutator Function.
     * When accessing the created_at attribute, format it as a pretty date.
     *
     * @param $value
     * @return mixed
     */
    public function getCreatedAtAttribute( $value ){
        return \Carbon::parse($value)->format("jS M, H:i");
    }

    /**
     * Custom Mutator Function.
     * When accessing the gdpr_consent_date attribute, format it as a pretty date.
     *
     * @param $value
     * @return mixed
     */
    public function getGdprConsentDateAttribute( $value ){
        return !empty( $value ) ? \Carbon::parse( $value )->format( "jS F Y, H:i" ) : $value;
    }

    /**
     * Custom Mutator Function.
     * As we store the POSTed form fields as a serialized array, simply return them as an unserialized array.
     *
     * @param $value
     * @return mixed
     */

    public function getFieldsAttribute( $value ){
        try {
            $value = Crypt::decryptString($value);
        } catch (\Exception $e) {}

        if (is_json($value)) { $value = json_decode($value, true); }
        return is_serialized( $value ) ? unserialize( $value ) : $value;
    }

    /**
     * Custom Setter Function.
     * Sets the form fields against the form submission as a serialized array, to allow DB storage.
     *
     * @param $value
     */
    public function setFieldsAttribute( $value ){
        $value = is_array( $value ) ? serialize( $value ) : $value;
        $this->attributes['fields'] = Crypt::encryptString($value);
    }
}