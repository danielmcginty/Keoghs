@if( $include_recaptcha_js )
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
@endif

<script>
$(document).ready( function(){

    $(document).on('submit', '[data-custom-form]', function (evt) {

        evt.preventDefault();
        evt.stopPropagation();

        var $form = $(this);

        if ($form.data('submitted') === true) {
            // Previously submitted - don't submit again
            return false;
        } else {
            // Mark it so that the next submit can be ignored
            $form.data('submitted', true);
        }

        @if( $include_recaptcha_js )
        // If the form has no Recaptcha Callback flag set, or the flag is set to false, we need to manually trigger
        // the recaptcha functionality.
        if( typeof( $form.data('recaptcha-callback' ) ) == 'undefined' || !$form.data('recaptcha-callback') ) {

            // We also need to record the widget ID so that we can have multiple forms on 1 page.
            // So, if we've logged it against the form
            var widget_id;
            if( typeof( $form.data('recaptcha-widget-id') ) != 'undefined' ){
                // Get the widget ID
                widget_id = $form.data('recaptcha-widget-id');
            }
            // Else, we need to render the Recaptcha element
            else {
                // So get the recaptcha div
                var form_id = $form.data('form');
                var recaptcha_div_id = 'form-' + form_id + '-recaptcha-div';
                var $recaptcha_div = $form.find('#' + recaptcha_div_id);
                // And render Recaptcha, getting the widget ID
                widget_id = grecaptcha.render(recaptcha_div_id, {
                    'sitekey': $recaptcha_div.data('sitekey'),
                    'callback': eval($recaptcha_div.data('callback')),
                    'size': 'invisible'
                });
                // And register the ID against the form
                $form.data('recaptcha-widget-id', widget_id);
            }

            // Now we can reset the recaptcha widget
            grecaptcha.reset(widget_id);
            // And execute it
            grecaptcha.execute(widget_id);

            // We need to mark the form as 'unsubmitted' so that it can get past the condition above
            $form.data('submitted', false);
            // And return false to let Recaptcha do it's stuff, calling our callback function
            return false;
        } else {
            // Else the form has been submitted, and Recaptcha has been triggered to execute.
            // Reset the flag so that we can call Recaptcha again on the next submission.
            $form.data('recaptcha-callback', false);
        }
        @endif

        // Using https://github.com/malsup/form/ to allow files to be uploaded in <= IE9
        // This is basically a wrapper for jquery's ajax method
        $form.ajaxSubmit({
            url: '{{ route('theme.ajax', ['form_builder', 'custom_form_submission']) }}',
            type: 'post',
            beforeSend: function () {
                $form.find('[data-loading]').show();
                $form.find('button[type="submit"]').addClass('loading-overlay');
                $form.find('.has-error .inline-error').remove();
                $form.find('.has-error').removeClass('has-error');
                $form.find('[data-alert]').hide();
            },
            success: function (json) {

                // IE9 Fix (the JSON response may be a string, and be wrapped in a <textarea> element)
                if( typeof( json ) == 'string' ){
                    json = JSON.parse( json.replace( '<textarea>', '').replace( '</textarea>', '' ) );
                }

                if (json.success) {

                    // Google Analytics Event Code
                    if( typeof( gtag ) != 'undefined' ) {
                        var ga_category = typeof( json.ga_tracking_category ) != 'undefined' ? json.ga_tracking_category : false;
                        var ga_action = typeof( json.ga_tracking_action ) != 'undefined' ? json.ga_tracking_action : false;
                        if( ga_category && ga_action ){
                            var ga_label = typeof( json.ga_tracking_label ) != 'undefined' ? json.ga_tracking_label : false;
                            var ga_value = typeof( json.ga_tracking_value ) != 'undefined' ? json.ga_tracking_value : false;

                            var event_settings = {
                                event_category: ga_category
                            };
                            if( ga_label ){
                                event_settings.event_label = ga_label;
                            }
                            if( ga_value ){
                                event_settings.value = ga_value;
                            }

                            gtag( 'event', ga_action, event_settings );
                        }
                    }
                    if (typeof(ga) != 'undefined') {
                        var ga_category = typeof( json.ga_tracking_category ) != 'undefined' ? json.ga_tracking_category : false;
                        var ga_action = typeof( json.ga_tracking_action ) != 'undefined' ? json.ga_tracking_action : false;
                        if( ga_category && ga_action ){
                            var ga_label = typeof( json.ga_tracking_label ) != 'undefined' ? json.ga_tracking_label : false;
                            var ga_value = typeof( json.ga_tracking_value ) != 'undefined' ? json.ga_tracking_value : false;

                            if (ga_label && ga_value) {
                                ga('send', 'event', ga_category, ga_action, ga_label, ga_value);
                            }
                            else if (ga_label) {
                                ga('send', 'event', ga_category, ga_action, ga_label);
                            }
                            else {
                                ga('send', 'event', ga_category, ga_action);
                            }
                        }
                    }

                    if(json.redirect){
                        window.location.href = json.redirect;
                    } else {
                        $form.find('[data-alert-success]').show();
                        $form.find('[data-field-wrap]').hide();
                    }
                } else {
                    if( typeof( json.recaptcha_error) != 'undefined' ){
                        $form.find('[data-alert-undefined-error]').show();
                    } else {
                        $form.find('[data-alert-error]').show();
                    }

                    if (typeof json.file_error != 'undefined') {
                        // One or more of the files couldn't be uploaded, so show an additional message for this scenario
                        $form.find('[data-alert-file-error]').show();
                    }

                    if (typeof( json.error_fields ) != 'undefined') {
                        for (var field_name in json.error_fields) {
                            var message = json.error_fields[field_name];

                            var $field = $form.find('[name="' + field_name + '"]');
                            var $input_row = $field.parents('[data-field="' + field_name + '"]').first();

                            $input_row.addClass('has-error');
                            $input_row.append('<span class="inline-error">' + message + '</span>');
                        }
                    }
                }
            },
            complete: function () {
                $form.find('[data-loading]').hide();
	            $form.find('button[type="submit"]').removeClass('loading-overlay');
                $form.data('submitted', false);

                @if( $include_recaptcha_js )
                grecaptcha.reset();
                @endif
            }
        });
    });

});
</script>