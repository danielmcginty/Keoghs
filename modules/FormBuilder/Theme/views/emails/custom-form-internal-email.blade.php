@extends('templates.email')

@section('heading')
Enquiry from keoghs.co.uk
@endsection

@section('content')
	<p>You have received a new enquiry from keoghs.co.uk. Please see the contact details below:</p>
    {!! $mail_message !!}
@endsection

@section('mobile_content')
    <div style="{{ \App\Helpers\EmailHelper::$_CLASSES['span.mobile-para'] }}">
        <h1 style="{{ \App\Helpers\EmailHelper::$_CLASSES['h1']  }}">You've received a new Form Submission!</h1>
        {!! $mail_message !!}
    </div>
@endsection