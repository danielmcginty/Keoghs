@extends('templates.email')

@section('heading')
Thank you
@endsection

@section('content')

    @if( isset( $mail_parts['above_cta_content'] ) && $mail_parts['above_cta_content'] )
        <div style="{{ \App\Helpers\EmailHelper::$_CLASSES['p.desktop-para'] }}">
            {!! $mail_parts['above_cta_content'] !!}
        </div>
    @endif

    @if( isset( $mail_parts['cta_link'] ) && $mail_parts['cta_link'] && isset( $mail_parts['cta_text'] ) && $mail_parts['cta_text'] )
        <p style="{{ \App\Helpers\EmailHelper::$_CLASSES['p.desktop-para'] }}">
            <a href="{{ $mail_parts['cta_link'] }}" target="_blank" style="{{ \App\Helpers\EmailHelper::$_CLASSES['button.primary'] }}">{{ $mail_parts['cta_text'] }}</a>
        </p>
    @endif

    @if( isset( $mail_parts['below_cta_content'] ) && $mail_parts['below_cta_content'] )
        <div style="{{ \App\Helpers\EmailHelper::$_CLASSES['p.desktop-para'] }}">
            {!! $mail_parts['below_cta_content'] !!}
        </div>
    @endif

    @if( isset( $mail_parts['closing_remark'] ) && $mail_parts['closing_remark'] )
        <div style="{{ \App\Helpers\EmailHelper::$_CLASSES['p.desktop-para'] }}">
            {!! $mail_parts['closing_remark'] !!}
        </div>
    @endif

@endsection

@section('mobile_content')

    @if( isset( $mail_parts['above_cta_content'] ) && $mail_parts['above_cta_content'] )
        <div style="{{ \App\Helpers\EmailHelper::$_CLASSES['span.mobile-para'] }}">
            {!! $mail_parts['above_cta_content'] !!}
        </div>
    @endif

    @if( isset( $mail_parts['cta_link'] ) && $mail_parts['cta_link'] && isset( $mail_parts['cta_text'] ) && $mail_parts['cta_text'] )
        <p style="{{ \App\Helpers\EmailHelper::$_CLASSES['span.mobile-para'] }}">
            <a href="{{ $mail_parts['cta_link'] }}" target="_blank" style="{{ \App\Helpers\EmailHelper::$_CLASSES['button.primary'] }}">{{ $mail_parts['cta_text'] }}</a>
        </p>
    @endif

    @if( isset( $mail_parts['below_cta_content'] ) && $mail_parts['below_cta_content'] )
        <div style="{{ \App\Helpers\EmailHelper::$_CLASSES['span.mobile-para'] }}">
            {!! $mail_parts['below_cta_content'] !!}
        </div>
    @endif

    @if( isset( $mail_parts['closing_remark'] ) && $mail_parts['closing_remark'] )
        <div style="{{ \App\Helpers\EmailHelper::$_CLASSES['span.mobile-para'] }}">
            {!! $mail_parts['closing_remark'] !!}
        </div>
    @endif

@endsection