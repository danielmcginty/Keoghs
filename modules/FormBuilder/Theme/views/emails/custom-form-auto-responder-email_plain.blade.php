@if( isset( $mail_parts['above_cta_content'] ) && $mail_parts['above_cta_content'] )
    {{ preg_replace( "/\n\s+/", "\n", rtrim( html_entity_decode( strip_tags( $mail_parts['above_cta_content'] ) ) ) ) }}
@endif

@if( isset( $mail_parts['cta_link'] ) && $mail_parts['cta_link'] && isset( $mail_parts['cta_text'] ) && $mail_parts['cta_text'] )
    {{ $mail_parts['cta_text'] }}: {{ $mail_parts['cta_link'] }}
@endif

@if( isset( $mail_parts['below_cta_content'] ) && $mail_parts['below_cta_content'] )
    {{ preg_replace( "/\n\s+/", "\n", rtrim( html_entity_decode( strip_tags( $mail_parts['below_cta_content'] ) ) ) ) }}
@endif

@if( isset( $mail_parts['closing_remark'] ) && $mail_parts['closing_remark'] )
    {{ preg_replace( "/\n\s+/", "\n", rtrim( html_entity_decode( strip_tags( $mail_parts['closing_remark'] ) ) ) ) }}
@endif