<?php
namespace Modules\FormBuilder\Theme\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Modules\FormBuilder\Models\Form;
use Modules\FormBuilder\Models\FormSubmission;
use Modules\OurPeople\Theme\Models\Person;

class FormBuilderController extends Controller {

    /**
     * Array containing valid mime types for form file uploads
     * @var array
     */
    public $file_mime_types = array(
        'text/plain',
        'text/csv',
        'image/tiff',
        'image/x-tiff',
        'image/png',
        'image/x-png',
        'image/jpeg',
        'image/pjpeg',
        'image/gif',
        'application/pdf',

        'image/svg+xml', // .svg

        // MS Office
        'application/excel',
        'application/x-excel',
        'application/x-msexcel',
        'application/vnd.ms-excel',
        'application/msword',
        'application/powerpoint',
        'application/mspowerpoint',
        'application/x-mspowerpoint',
        'application/vnd.ms-powerpoint',

        // Adobe
        'application/octet-stream', // .psd
        'application/postscript', // .ai / .eps
        'application/x-shockwave-flash', // Flash

        // Zip, Rar
        'application/x-compressed',
        'application/x-zip-compressed',
        'application/zip',
        'multipart/x-zip',
        'multipart/x-rar-compressed'
    );

    /**
     * Array containing any files uploaded to the form
     * @var array
     */
    private $uploaded_files = [];

    /**
     * AJAX Function.
     * Handles Custom Form Submissions.
     * Validates & Stores form data, and sends emails to the website admin and user if required.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function custom_form_submission( Request $request ){
        $form = Form::where([
            'form_id' => $request->input('form_id')
        ])->first();

        if( is_serialized( $form->structure ) ){ $form->structure = json_encode( unserialize( $form->structure ) ); }
        $form_fields = is_json( $form->structure ) ? json_decode( $form->structure, true ) : $form->structure;

        $fields_to_labels = [];
        $collated_data = [];
        $invalid_fields = [];
        $validation_rules = [];
        $files_to_upload = [];
        $uploaded_file_paths = [];

        $responder_email = false;

        foreach( $form_fields as $field ){
            $posted_field_name = \Illuminate\Support\Str::slug( $field['name'], '_' );

            $field_validation = [];

            $fields_to_labels[ $posted_field_name ] = $field['name'];

            $posted_value = $request->input( $posted_field_name ) !== null ? $this->_convertPostedValue( $request->input( $posted_field_name ), $field ) : false;

            if( isset( $field['required'] ) ){
                $field_validation[] = 'required';

                if( $posted_value && !empty( $posted_value ) ){
                    $collated_data[ $posted_field_name ] = $posted_value;
                } else {
                    $invalid_fields[ $posted_field_name ] = $field['name'] . $this->_requiredMessageFormFieldType( $field['type'] );
                }
            } elseif( $posted_value ) {
                $collated_data[ $posted_field_name ] = $posted_value;
            }

            $field_validation = array_merge( $field_validation, $this->_getValidationForFieldType( $field['type'] ) );
            if( !empty( $field_validation ) ){
                $validation_rules[ $posted_field_name ] = implode( '|', $field_validation );
            }

            if( $field['type'] == 'email' && $responder_email === false && $request->input( $posted_field_name ) ){
                $responder_email = $request->input( $posted_field_name );
            }

            if( $field['type'] == 'file' && $request->hasFile( $posted_field_name ) ){
                $files_to_upload[ $posted_field_name ] = $request->file( $posted_field_name );
            }
        }

        if ($files_to_upload) {

            if (!is_dir(base_path('storage/app/custom_form_uploads'))) {
                mkdir(base_path('storage/app/custom_form_uploads'), 0755);
            }

            foreach ($files_to_upload as $field => $file) {

                $file_info = pathinfo($file->getClientOriginalName()); // https://laravel.com/docs/4.2/requests#files

                // Preserve the original filename, but add the current date as a suffix
                $filename = \Illuminate\Support\Str::slug($file_info['filename'] . '-' . Carbon::now());
                $filename .= '.' . $file_info['extension'];

                $path = Storage::putFileAs('custom_form_uploads', $file, $filename);

                $collated_data[ $field ] = $path;

                $uploaded_file_paths[] = $path;

                $this->uploaded_files[ $field ] = $path;
            }
        }

        if( !empty( $validation_rules ) ){
            $validator = \Validator::make( $request->all(), $validation_rules );

            $validator->after( function($validator) use ($files_to_upload, $uploaded_file_paths) {
                if (count($files_to_upload) != count($uploaded_file_paths)) {
                    $validator->errors()->add('file_error', 'There was a problem processing your file(s). Please try again.');
                }
            });

            if( $validator->fails() ){
                $invalid_fields = $validator->errors();
            }
        }

        if( !empty( $invalid_fields ) ){

            // Using https://github.com/malsup/form/ to allow files to be uploaded in <= IE9
            if (!$request->ajax()) {
                echo '<textarea>';
                echo json_encode( ['success' => false, 'error_fields' => $invalid_fields] );
                echo '</textarea>';
                exit;
            }

            return response()->json( ['success' => false, 'error_fields' => $invalid_fields] );
        }


        // We've got this far, so the form has validated successfully.

        // If the form is set up to use invisible recaptcha, we'll need to validate the recaptcha response.
        if( $form->use_invisible_recaptcha ){
            $recaptcha_response = $request->input('g-recaptcha-response');
            $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';

            $post_arr = array(
                'secret' => env('GOOGLE_INVISIBLE_RECAPTCHA_SECRET_KEY'),
                'response' => $recaptcha_response
            );

            $ch = curl_init( $recaptcha_url );
            curl_setopt( $ch, CURLOPT_POST, true );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $post_arr );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

            $curl_response = curl_exec( $ch );
            curl_close( $ch );

            $json_response = json_decode( $curl_response );
            $is_valid_recaptcha = $json_response->success;

            // If the recaptcha isn't valid, return back to the form with an error
            if( !$is_valid_recaptcha ){
                return response()->json( ['success' => false, 'recaptcha_error' => true] );
            }
        }

        // Now we can also log the form submission
        $submission = $this->_logFormSubmission( $request, $form, $collated_data, $fields_to_labels );

        try {
            // And we can also send the internal email
            $this->_sendInternalEmail($request, $form, $collated_data, $fields_to_labels, $submission);

            if ($form->send_email_to_submitter && !empty($form->subject_to_submitter) && $responder_email != false) {
                $this->_sendAutoResponderEmail($form, $collated_data, $fields_to_labels, $responder_email);
            }
        } catch (\Exception $e) {}

        $json = [
            'success' => true,
            'redirect' => $form->success_page_redirect
        ];

        if( !empty( $form->ga_tracking_category ) ){
            $json['ga_tracking_category'] = $form->ga_tracking_category;
        }
        if( !empty( $form->ga_tracking_action ) ){
            $json['ga_tracking_action'] = $form->ga_tracking_action;
        }
        if( !empty( $form->ga_tracking_label ) ){
            $json['ga_tracking_label'] = $form->ga_tracking_label;
        }
        if( !empty( $form->ga_tracking_value ) ){
            $json['ga_tracking_value'] = $form->ga_tracking_value;
        }

        // Using https://github.com/malsup/form/ to allow files to be uploaded in <= IE9
        if (!$request->ajax()) {
            echo '<textarea>';
            echo json_encode( $json );
            echo '</textarea>';
            exit;
        }

        return response()->json( $json );
    }

    /**
     * Internal function used to convert certain POSTed values into a format we can store and read.
     *
     * @param $posted_value
     * @param $field
     * @return mixed
     */
    private function _convertPostedValue( $posted_value, $field ){
        switch( $field['type'] ){
            case 'select':
                $options = explode( "\r\n", $field['options'] );

                // Remove the 'placeholder' element if it's set
                if( isset( $field['first_option_placeholder'] ) && $field['first_option_placeholder'] ){
                    array_shift( $options );
                }

                foreach( $options as $ind => $option ){
                    if( $ind == $posted_value ){
                        $posted_value = $option;
                    }
                }
                break;
        }

        return $posted_value;
    }

    /**
     * Internal function used to send an email to the website admin.
     *
     * @param $request
     * @param $form
     * @param $posted_data
     * @param $label_map
     * @param $submission
     */
    private function _sendInternalEmail( $request, $form, $posted_data, $label_map, $submission ){
        $mail_message = '';
        $plain_text = '';

        if( config( 'app.include_details_in_internal_form_emails' ) ){
            $mail_message .= '<strong>Form Submitted: </strong>' . $form->title .'<br/>';
            $plain_text .= 'Form Submitted: ' . $form->title . PHP_EOL;

            $mail_message .= '<strong>URL Submitted From: </strong>' . $request->input( 'page_url' ) . '<br/>';
            $plain_text .= 'URL Submitted From: ' . $request->input( 'page_url' ) . PHP_EOL;

            $mail_message .= '<strong>IP Address: </strong>' . $request->ip() . '<br/>';
            $plain_text .= 'IP Address: ' . $request->ip() . PHP_EOL;

            $mail_message .= '<a href="' . route('adm.form-submissions.submission', ['form_id' => $form->form_id, 'submission_id' => $submission->forms_log_id]) . '" title="View Submission"><strong>View Submission</strong></a>';
            $plain_text .= 'View Submission: ' . route('adm.form-submissions.submission', ['form_id' => $form->form_id, 'submission_id' => $submission->forms_log_id]) . PHP_EOL . PHP_EOL;

            $mail_message .= '<hr style="margin: 30px 0;" />';
            $plain_text .= PHP_EOL;

            foreach( $posted_data as $field => $value ){

                $html_value = $value;

                // If it's a file input, make sure the link goes to the file
                if( !empty( $this->uploaded_files ) && isset( $this->uploaded_files[ $field ] ) ){
                    $file_url = route('custom_form_upload', [str_replace('custom_form_uploads/', '', $this->uploaded_files[ $field ])]);
                    $html_value = '<a target="_blank" href="' . $file_url . '">' . $file_url . '</a>';
                }

                $mail_message .= '<strong>' . $label_map[ $field ] . ': </strong>' . $html_value . '<br/>';
                $plain_text .= $label_map[ $field ] . ': ' . $value . PHP_EOL;

            }
        } else {
            $mail_message .= '<p>Please log into the CMS and click the following link to view the details.</p>';
            $plain_text .= 'Please log into the CMS and click the following link to view the details.';

            $mail_message .= '<a href="' . route('adm.form-submissions.submission', ['form_id' => $form->form_id, 'submission_id' => $submission->forms_log_id]) . '" title="View Submission"><strong>View Submission</strong></a>';
            $plain_text .= 'View Submission: ' . route('adm.form-submissions.submission', ['form_id' => $form->form_id, 'submission_id' => $submission->forms_log_id]) . PHP_EOL . PHP_EOL;
        }

        $person_email = null;
        if ($request->has('person')) {
            $person = Person::query()->where('person_id', $request->input('person'))->first();
            if ($person) {
                $person_email = $person->email;
            }
        }

        $form->sendInternalNotification( $mail_message, $plain_text, $person_email );
    }

    /**
     * Internal function used to store the form submission in the database.
     *
     * @param $request
     * @param $form
     * @param $posted_data
     * @param $label_map
     * @return FormSubmission
     */
    private function _logFormSubmission( $request, $form, $posted_data, $label_map ){
        $submission = new FormSubmission;

        $submission->form_id = $form->form_id;
        $submission->language_id = $form->language_id;
        $submission->url = $request->input('page_url');
        $submission->ip_address = $request->ip();

        $submission->gdpr_consent = ($request->input( 'gdpr_consent' ) ? 1 : 0);
        $submission->gdpr_consent_text = ($request->input( 'gdpr_consent' ) ? $request->input( 'gdpr_consent_text' ) : '');
        $submission->gdpr_consent_date = ($request->input( 'gdpr_consent' ) ? date("Y-m-d H:i:s") : null);

        $recorded_fields = [];
        foreach( $posted_data as $field => $value ){
            $recorded_fields[ $label_map[ $field ] ] = $value;
        }

        if ($request->has('person') && !is_null($person = $this->parseSelectedPerson($request->input('person')))) {
            $recorded_fields[ 'Person' ] = $person;
        }

        $submission->fields = $recorded_fields;

        $submission->save();

        return $submission;
    }

    /**
     * Internal function used to send an email to the user.
     *
     * @param Form $form
     * @param $posted_data
     * @param $label_map
     * @param $responder_email
     */
    private function _sendAutoResponderEmail( Form $form, $posted_data, $label_map, $responder_email ){
        $form->replaceTemplateTags( $posted_data );

        $mail_parts = [
            'above_cta_content' => $form->above_cta_content,
            'cta_link' => $form->emailable_cta_link,
            'cta_text' => $form->cta_text,
            'below_cta_content' => $form->below_cta_content,
            'closing_remark' => $form->closing_remark
        ];

        $form->sendAutoResponderNotification( $responder_email, $mail_parts );
    }

    /**
     * Internal function used to generate validation rules based on field types for the form.
     *
     * @param $field_type
     * @return array
     */
    private function _getValidationForFieldType( $field_type ){
        $validation = [];

        switch( $field_type ){
            case 'email':
                $validation[] = 'email';
                break;
            case 'number':
                $validation[] = 'numeric';
                break;
            case 'file':
                $validation[] = 'file';
                $validation[] = 'max:7000'; // Maximum file size in kilobytes
                $validation[] = 'mimetypes:' . implode(',', $this->file_mime_types);
                break;
        }

        return $validation;
    }

    /**
     * Internal function used to apply an easy to read 'required' error message for certain field types.
     *
     * @param $field_type
     * @return string
     */
    private function _requiredMessageFormFieldType( $field_type ){
        switch( $field_type ){
            case 'radio':
                $message = ' is required, please select an option.';
                break;
            case 'checkbox':
                $message = ' is required, please tick an option.';
                break;
            case 'select':
                $message = ' is required, please select an option.';
                break;
            default:
                $message = ' is required, please fill it in.';
                break;
        }

        return $message;
    }

    /**
     * GDPR Cleanup
     * Function run to clear out any form submissions over a configurable
     * logging period (config/logging.php 'custom_form_retention_months')
     */
    public function gdpr_cleanup(){
        FormSubmission::whereDate( 'created_at', '<', Carbon::now()->subMonths( config( 'logging.custom_form_retention_months' ) )->format( 'Y-m-d' ) )->forceDelete();
    }

    private function parseSelectedPerson($person_id)
    {
        $person = Person::query()->where('person_id', $person_id)->first();

        if (!$person) {
            return null;
        }

        return $person->full_name;
    }
}