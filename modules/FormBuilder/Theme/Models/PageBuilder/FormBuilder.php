<?php
namespace Modules\FormBuilder\Theme\Models\PageBuilder;

use Modules\FormBuilder\Models\Form as FormModel;

/**
 * Class FormBuilder
 * @package Modules\FormBuilder\Theme\Models\PageBuilder
 *
 * Front-End Page Builder Model for the Form Builder block
 */
class FormBuilder {
    /**
     * Static variable flagging whether we need to include Google's ReCaptcha JavaScript file
     * @var bool
     */
    private static $needs_invisible_recaptcha_js = false;

    /**
     * Static variable indicating if we have already included Google's ReCaptcha JavaScript file
     * @var bool
     */
    private static $included_invisible_recaptcha_js = false;

    /**
     * The getValue() function is used to perform any custom functionality on Page Builder Block field values.
     * In this case, it basically creates the form fields ready for output on the website.
     *
     * @param $page_builder_block
     * @param $field
     * @param $original_value
     * @return \Modules\FormBuilder\Models\Form
     */
    public function getValue( $page_builder_block, $field, $original_value ){
        $form = FormModel::where([
            'form_id' => $original_value,
            'language_id' => $page_builder_block->language_id
        ])->first();

        if( is_serialized( $form->structure ) ){ $form->structure = json_encode( unserialize( $form->structure ) ); }
        $form_fields = is_json( $form->structure ) ? json_decode( $form->structure, true ) : $form->structure;

        $form_unique = intval( rand() * 1000 );
        foreach( $form_fields as $index => $input ){
            $form_fields[ $index ]['field_name'] = \Illuminate\Support\Str::slug( $input['name'], '_' );
            $form_fields[ $index ]['field_id'] = \Illuminate\Support\Str::slug( $input['name'] ) . '-' . $form_unique;
            $form_fields[ $index ]['options'] = isset( $input['options'] ) ? explode( "\r\n", $input['options'] ) : array();

            $field_attrs = [
                'id' => $form_fields[ $index ]['field_id'],
                'class' => 'w-full border-2 border-gray-300 text-black text-lg rounded '
            ];
            if( isset($input['required']) && $input['required'] ){
                $field_attrs['required'] = 'required';
            }

            switch( $input['type'] ){
                case 'text':
                    $field_attrs['class'] .= 'form-input ';
                    break;
                case 'email':
                    $field_attrs['class'] .= 'form-input ';
                    break;
                case 'select':
                    $field_attrs['class'] .= 'form-select pr-40 ';

                    if( isset( $input['first_option_placeholder'] ) && $input['first_option_placeholder'] ){
                        $field_attrs['placeholder'] = array_shift( $form_fields[ $index ]['options'] );
                    }

                    break;
                case 'number':
                    $field_attrs['class'] .= 'form-input ';
                    break;
                case 'tel':
                    $field_attrs['class'] .= 'form-input ';
                    break;
                case 'textarea':
                    $field_attrs['class'] .= 'form-textarea ';
                    $field_attrs['size'] = '1x1';
                    break;
            }

            $form_fields[ $index ]['field_attrs'] = $field_attrs;
        }

        $form->structure = $form_fields;

        // If we haven't previously included the Recaptcha JS, and this form needs it, mark it for inclusion
        if( !self::$included_invisible_recaptcha_js && $form->use_invisible_recaptcha ){
            self::$needs_invisible_recaptcha_js = true;
            self::$included_invisible_recaptcha_js = true;
        }
        // Else, we've already included it once, so set it as not required.
        elseif( self::$included_invisible_recaptcha_js ){
            self::$needs_invisible_recaptcha_js = false;
        }

        $dynamic_script_parameters = [
            'include_recaptcha_js' => self::$needs_invisible_recaptcha_js
        ];

        if( \View::exists( 'dynamic-scripts.form-submission' ) ){
            \Document::addDynamicScript( 'dynamic-scripts.form-submission', $dynamic_script_parameters );
        } else {
            view()->addNamespace('FormBuilder', base_path('/modules/FormBuilder/Theme/views'));
            \Document::addDynamicScript('FormBuilder::dynamic-scripts.form-submission', $dynamic_script_parameters );
        }

        return $form;
    }

    /**
     * Get Embed Form
     * Static function used to load a form builder form (as a block) ready
     * for inserting into pages without them having to be added to a page.
     * Useful when adding 'hard-coded' forms to footers, etc.
     *
     * @param int $form_id
     * @return \Modules\FormBuilder\Models\Form
     */
    public static function getEmbedForm( $form_id ){
        $pseudo_block = new \StdClass();
        $pseudo_block->language_id = \Session::get('language_id');
        $FormBuilder = new self();
        return $FormBuilder->getValue( $pseudo_block, 'form', $form_id );
    }
}