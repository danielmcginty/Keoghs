<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AmendFormsTableSplitAutoresponseEmailFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('forms', function( Blueprint $table ){
            $table->dropColumn('email_to_submitter');
            $table->text('above_cta_content')->nullable()->after('subject_to_submitter');
            $table->string('cta_link')->nullable()->after('above_cta_content');
            $table->string('cta_text')->nullable()->after('cta_link');
            $table->text('below_cta_content')->nullable()->after('cta_text');
            $table->text('closing_remark')->nullable()->after('below_cta_content');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('forms', function( Blueprint $table ){
            $table->text('email_to_submitter')->nullable()->after('subject_to_submitter');
            $table->dropColumn('above_cta_content');
            $table->dropColumn('cta_link');
            $table->dropColumn('cta_text');
            $table->dropColumn('below_cta_content');
            $table->dropColumn('closing_remark');
        });
    }
}
