<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AmendFormsTablesForGdprCompliance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'forms', function( Blueprint $table ){
            $table->boolean( 'gdpr_compliance' )->default( 0 )->after( 'template' );
            $table->string( 'gdpr_compliance_text' )->nullable()->after( 'gdpr_compliance' );
            $table->string( 'gdpr_compliance_opt_out_text' )->nullable()->after( 'gdpr_compliance_text' );
        });

        Schema::table( 'forms_log', function( Blueprint $table ){
            $table->boolean( 'gdpr_consent' )->default( 0 )->after( 'fields' );
            $table->string( 'gdpr_consent_text' )->nullable()->after( 'gdpr_consent' );
            $table->dateTime( 'gdpr_consent_date' )->nullable()->after( 'gdpr_consent_text' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'forms', function( Blueprint $table ){
            $table->dropColumn( 'gdpr_compliance' );
            $table->dropColumn( 'gdpr_compliance_text' );
            $table->dropColumn( 'gdpr_compliance_opt_out_text' );
        });

        Schema::table( 'forms_log', function( Blueprint $table ){
            $table->dropColumn( 'gdpr_consent' );
            $table->dropColumn( 'gdpr_consent_text' );
            $table->dropColumn( 'gdpr_consent_date' );
        });
    }
}
