<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFormsTableAutoResponder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('forms', function( Blueprint $table ){
            $table->text('description')->after('title')->nullable();
            $table->boolean('send_email_to_submitter')->after('recipient_subject');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('forms', function( Blueprint $table ){
            $table->dropColumn('description');
            $table->dropColumn('send_email_to_submitter');
        });
    }
}
