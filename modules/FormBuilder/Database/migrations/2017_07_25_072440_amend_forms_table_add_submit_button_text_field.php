<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AmendFormsTableAddSubmitButtonTextField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'forms', function( Blueprint $table ){
            $table->string( 'submit_button_text' )->after( 'use_invisible_recaptcha' )->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'forms', function( Blueprint $table ){
            $table->dropColumn( 'submit_button_text' );
        });
    }
}
