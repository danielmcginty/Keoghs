<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms', function(Blueprint $table){
            $table->integer('form_id');
            $table->integer('language_id');
            $table->string('title');
            $table->string('heading');
            $table->string('recipient_email');
            $table->string('recipient_subject');
            $table->string('subject_to_submitter')->nullable();
            $table->text('email_to_submitter')->nullable();
            $table->text('structure')->nullable();
            $table->boolean('status');
            $table->timestamps();
            $table->softDeletes();
            $table->primary(['form_id', 'language_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('forms');
    }
}
