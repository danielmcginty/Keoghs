<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormsLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms_log', function(Blueprint $table){
            $table->increments('forms_log_id');
            $table->integer('form_id');
            $table->integer('language_id');
            $table->string('url');
            $table->string('ip_address');
            $table->text('fields')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index(['form_id', 'language_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('forms_log');
    }
}
