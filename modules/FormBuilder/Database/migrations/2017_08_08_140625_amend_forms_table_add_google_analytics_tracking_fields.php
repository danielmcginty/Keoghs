<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AmendFormsTableAddGoogleAnalyticsTrackingFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'forms', function( Blueprint $table ){
            $table->string( 'ga_tracking_category' )->after( 'success_page_url_id' )->nullable();
            $table->string( 'ga_tracking_action' )->after( 'ga_tracking_category' )->nullable();
            $table->string( 'ga_tracking_label' )->after( 'ga_tracking_action' )->nullable();
            $table->string( 'ga_tracking_value' )->after( 'ga_tracking_label' )->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'forms', function( Blueprint $table ){
            $table->dropColumn( 'ga_tracking_category' );
            $table->dropColumn( 'ga_tracking_action' );
            $table->dropColumn( 'ga_tracking_label' );
            $table->dropColumn( 'ga_tracking_value' );
        });
    }
}
