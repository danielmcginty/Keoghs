<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AmendFormsTableAddGoogleRecaptchaField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'forms', function( Blueprint $table ){
            $table->boolean( 'use_invisible_recaptcha' )->after( 'attachment' )->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'forms', function( Blueprint $table ){
            $table->dropColumn( 'use_invisible_recaptcha' );
        });
    }
}
