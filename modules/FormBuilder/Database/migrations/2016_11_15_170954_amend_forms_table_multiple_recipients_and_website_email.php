<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AmendFormsTableMultipleRecipientsAndWebsiteEmail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'forms', function( Blueprint $table ){
            $table->text('recipient_email')->change();
            $table->boolean('send_to_website_email')->after('recipient_email')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'forms', function( Blueprint $table ){
            $table->string('recipient_email')->change();
            $table->dropColumn('send_to_website_email');
        });
    }
}
