<?php
namespace Modules\FormBuilder\Database\Seeders;

use Illuminate\Database\Seeder;

class FormBuilderPageBuilderTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('page_builder_block_types')->insert([
            'reference' => 'form_builder',
            'editable' => 1,
            'deletable' => 1
        ]);
    }
}
