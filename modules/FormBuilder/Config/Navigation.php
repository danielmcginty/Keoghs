<?php
namespace Modules\FormBuilder\Config;

use Adm\Interfaces\NavigationInterface;
use Modules\FormBuilder\Models\Form;

/**
 * Class Navigation
 * @package Modules\FormBuilder\Config
 *
 * The CMS Navigation Builder for the Form Builder Module
 */
class Navigation implements NavigationInterface {
    /**
     * Returns the CMS Navigation links for the module
     *
     * @return array
     */
    public function getNavItems(){

        $sub_array = [];
        $forms = Form::all();
        foreach( $forms as $ind => $form ){
            $sub_array[] = [
                'title' => $form->title,
                'url' => route('adm.form-submissions.form', $form->form_id),
                'position' => $ind+1
            ];
        }

        return [
            [ 'title' => 'Forms', 'url' => route('adm.path', 'form-builder'), 'position' => 800 ],
            [ 'title' => 'Form Submissions', 'url' => route('adm.form-submissions'), 'position' => 810, 'sub' => $sub_array ]
        ];
    }
}