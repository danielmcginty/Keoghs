<?php
namespace Modules\FormBuilder\Config;

class FormBuilderConfig {
    /**
     * Stores input labels
     * @var array
     */
    public static $labels = [
        'title' => 'Title',
        'heading' => 'Heading',
        'description' => 'Content',
        'recipient_email' => 'Internal Email Address',
        'sent_to_website_email' => 'BCC Main Website Email Address',
        'recipient_subject' => 'Internal Email Subject',
        'structure' => 'Form Builder',
        'send_email_to_submitter' => 'Send Automatic Response?',
        'subject_to_submitter' => 'Auto-response Email Subject',
        'above_cta_content' => 'Auto-response Email Content (Above CTA)',
        'cta_link' => 'Auto-response Email CTA Link',
        'cta_text' => 'Auto-response Email CTA Link Text',
        'below_cta_content' => 'Auto-response Email Content (Below CTA)',
        'closing_remark' => 'Auto-response Email Closing Remark',
        'status' => 'Status',
        'internal_email_title' => 'Internal Email Settings',
        'external_email_title' => 'Auto-response Email Settings',
        'attachment' => 'Auto-response Email Attachment',
        'use_invisible_recaptcha' => 'Use Google Invisible \'Recaptcha\'?',
        'success_title' => 'Form Success',
        'success_page_url_id' => 'Success Page',
        'submit_button_text' => 'Submit Button Text',
        'ga_tracking_category' => 'Category',
        'ga_tracking_action' => 'Action',
        'ga_tracking_label' => 'Label',
        'ga_tracking_value' => 'Value',
        'gdpr_compliance' => 'GDPR Compliance',
        'gdpr_compliance_text' => 'GDPR Compliance Text',
        'gdpr_compliance_opt_out_text' => 'GDPR Compliance Out-Out Text'
    ];

    /**
     * Stores input help text
     * @var array
     */
    public static $help = [
        'title' => 'The title of the form, used as an internal reference. Is shown when showing the form in the CMS',
        'heading' => 'The heading displayed with the form on the website',
        'description' => 'A short piece of text to display with the form on the website',
        'recipient_email' => 'Email address(es) which will receive notifications of all form submissions',
        'send_to_website_email' => 'Send a copy of form submissions to the main website email address',
        'recipient_subject' => 'The subject line of the email sent to Internal Email Addresses',
        'structure' => 'Add fields to build the form up. You can drag fields to re-order them',
        'send_email_to_submitter' => 'Send an automatic response, by email, to the person who submitted the form.<br>E.g. to thank them for completing the form',
        'subject_to_submitter' => 'The subject line of the email sent to the person submitting the form.<br>E.g. <strong>"Thank you for your enquiry"</strong>',
        'cta_link' => 'Where CTA button in the email will link to.<br>E.g. http://www.google.co.uk/',
        'cta_text' => 'The text to display on the CTA button in the email',
        'status' => 'Enable / Disable the form',
        'attachment' => 'Choose a file to attach to the auto-response emails',
        'use_invisible_recaptcha' => 'Include Google\'s Invisible Recaptcha on your form to help prevent automated submissions from bots',
        'success_page_url_id' => 'Upon a successful submission of a page a success message will be triggered and Google Events will be tracked inline on the form page. Alternatively, you can choose a page to redirect the user to',
        'submit_button_text' => 'The text to display on the submit button for the form',
        'ga_tracking_category' => 'The \'Category\' to send to Google Analytics, as an Event, when the form is submitted',
        'ga_tracking_action' => 'The \'Action\' to send to Google Analytics, as an Event, when the form is submitted',
        'ga_tracking_label' => 'The \'Label\' to send to Google Analytics, as an Event, when the form is submitted',
        'ga_tracking_value' => 'The \'Value\' to send to Google Analytics, as an Event, when the form is submitted',
        'gdpr_compliance' => 'Toggle whether this form includes a GDPR Compliance checkbox',
        'gdpr_compliance_text' => 'Set the text displayed to the user alongside the GDPR Compliance checkbox',
        'gdpr_compliance_opt_out_text' => 'Optionally set extra text to display on the form, telling users that they can opt-out of marketing emails'
    ];
}