<?php
Route::group(['middleware' => 'auth:adm'], function(){
    Route::any('form-submissions/export/{form_id}', 'FormSubmissionController@export')->name('adm.form-submissions.export');
    Route::any('form-submissions/delete', 'FormSubmissionController@delete')->name('adm.form-submissions.delete');
    Route::any('form-submissions/{form_id}/{submission_id}', 'FormSubmissionController@submission')->name('adm.form-submissions.submission');
    Route::any('form-submissions/{form_id}', 'FormSubmissionController@form')->name('adm.form-submissions.form');
    Route::any('form-submissions', 'FormSubmissionController@index')->name('adm.form-submissions');
});