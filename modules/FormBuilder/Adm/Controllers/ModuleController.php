<?php
namespace Modules\FormBuilder\Adm\Controllers;

use Adm\Controllers\AdmController;
use Modules\FormBuilder\Config\FormBuilderConfig;
use Modules\FormBuilder\Models\FormSubmission;
use Modules\FormBuilder\Models\Form;
use Adm\Models\Url;

class ModuleController extends AdmController {

    /**
     * CRUD Functionality for the main Form Builder section
     */
    public function form_builder(){
        $this->crud->setTable('forms');

        $this->crud->excludeFromListing( 'structure' );

        $this->crud->orderColumns( 'title', 'status' );

        $this->crud->changeType( 'recipient_email', 'comma_separated' );
        $this->crud->changeType( 'cta_link', 'link' );
        $this->crud->changeType( 'structure', 'form_builder' );
        $this->crud->changeType( 'template', 'select' );
        $this->crud->changeType( 'attachment', 'file' );
        $this->crud->changeType( 'success_page_url_id', 'select' );

        $this->crud->fieldAttributes( 'recipient_subject', ['autocomplete' => 'noop'] );
        $this->crud->fieldOptions( 'template', $this->_getAvailableFormTemplates() );
        $this->crud->fieldOptions( 'success_page_url_id', $this->_getSuccessPageURLs() );
        $this->crud->extraData( 'success_page_url_id', ['empty_value' => 0, 'empty_label' => 'No Redirect/Use inline Success Message']);

        $above_cta_content_help = 'Content of the autoresponse email, sent to the user submitting the form, positioned above the CTA Button.<br>e.g. <strong>"Thanks for getting in touch through our website. We will get back to you as soon as possible."</strong>';
        if( isset( $this->crud->parameters['id'] ) ){
            $above_cta_content_help = $this->_showAvailableTemplateTags( $this->crud->parameters['id'], $above_cta_content_help );
        }
        $this->crud->help( 'above_cta_content', $above_cta_content_help );

        $below_cta_content_help = 'Content of the autoresponse email, sent to the user submitting the form, positioned below the CTA Button.';
        if( isset( $this->crud->parameters['id'] ) ){
            $below_cta_content_help = $this->_showAvailableTemplateTags( $this->crud->parameters['id'], $below_cta_content_help );
        }
        $this->crud->help( 'below_cta_content', $below_cta_content_help );

        $closing_remark_help = 'Text to display at the end of the autoresponse email, sent to the user submitting the form.<br/>e.g. <strong>"If you\'re having trouble clicking the button above, copy and paste the URL below into your web browser."</strong>';
        if( isset( $this->crud->parameters['id'] ) ){
            $closing_remark_help = $this->_showAvailableTemplateTags( $this->crud->parameters['id'], $closing_remark_help );
        }
        $this->crud->help( 'closing_remark', $closing_remark_help );

        $this->crud->toggleFields( 'send_email_to_submitter', array( 'subject_to_submitter', 'above_cta_content', 'cta_link[type]', 'cta_text', 'below_cta_content', 'closing_remark', 'attachment' ), 1 );

        $this->crud->columnGroup( 'main', ['title', 'heading', 'description', 'structure', 'submit_button_text', 'use_invisible_recaptcha'] );

        $this->crud->columnGroup( 'GDPR Compliance', ['gdpr_compliance', 'gdpr_compliance_text', 'gdpr_compliance_opt_out_text'] );

        $this->crud->columnGroup( 'Success Message', ['success_page_url_id'] );

        $this->crud->columnGroup( 'Admin Email Settings', ['recipient_email', 'recipient_subject', 'send_to_website_email'] );

        $this->crud->columnGroup( 'User Email Settings', ['send_email_to_submitter', 'subject_to_submitter', 'above_cta_content', 'cta_link', 'cta_text', 'below_cta_content', 'closing_remark', 'attachment'] );

        $this->crud->columnGroup( 'Google Analytics', ['ga_tracking_category', 'ga_tracking_action', 'ga_tracking_label', 'ga_tracking_value'] );

        $this->crud->columnGroup( 'side', ['status'] );

        $invisible_recaptcha_site_key = env('GOOGLE_INVISIBLE_RECAPTCHA_SITE_KEY');
        if( !$invisible_recaptcha_site_key ){
            $this->crud->excludeFromEdit( 'use_invisible_recaptcha' );
        }

        if( \Auth::user()->userGroup->super_admin ){
            $this->crud->columnGroup( 'super_admin', array( 'template' ) );
        } else {
            $this->crud->excludeFromEdit( 'template' );
        }
    }

    /**
     * Custom Callback for formatting the URL field in Form Listings
     *
     * @param $col
     * @param $item
     */
    public function showUrlField( $col, &$item ){
        $relative_url = str_replace( route('theme'), '', $item->{ $col['field'] } );
        $item->{ $col['field'] } = '<a href="' . route('theme', $relative_url ) . '" target="_blank">' . $relative_url . '</a>';
    }

    /**
     * Custom function allowing us to get all the custom form templates available for the current theme.
     *
     * @return array
     */
    private function _getAvailableFormTemplates(){
        $form_templates = [];

        $current_theme = config('app.theme_name', 'default');
        $view_directory = resource_path( 'theme/' . $current_theme . '/views/form-templates' );

        if( is_dir( $view_directory ) ){
            $templates = array_filter( glob( $view_directory . '/*' ) );

            foreach( $templates as $template ){
                if( !is_file( $template ) ){ continue; }

                $name = str_replace( $view_directory . '/', '', $template );
                $name = str_replace( '.blade.php', '', $name );

                if( $name == 'default' ){ continue; }

                $key = $name;
                $name = \Illuminate\Support\Str::title( str_replace( '-', ' ', $name ) );

                $form_templates[ $key ] = $name;
            }
        }

        return $form_templates;
    }

    /**
     * When showing the form auto-response email settings, the fields within the form are accessible within the
     * content.
     * This function returns the 'template tags' available for the content.
     *
     * @param $form_id
     * @param $content
     * @return string
     */
    private function _showAvailableTemplateTags( $form_id, $content ){
        $form = Form::where('form_id', $form_id)->first();

        if( !empty( $form ) ){
            if( is_serialized( $form->structure ) ){ $form->structure = json_encode( unserialize( $form->structure ) ); }
            $fields = is_json( $form->structure ) ? json_decode( $form->structure, true ) : $form->structure;

            if( !empty( $fields ) ) {
                $content .= '<br/><span class="block margin-t-gutter">The following tags are available to use in your email templates, and they\'ll be replaced with the submitted details.</span>';

                $tags = [];
                foreach ($fields as $field) {
                    $tags[] = '[[' . \Illuminate\Support\Str::slug( $field['name'], '_' ) . ']]';
                }

                $content .= '<strong>' . implode( ', ', $tags ) . '</strong>';
            }
        }

        return $content;
    }

    /**
     * Get a list of ALL URLs available to the form as a 'redirect' URL.
     *
     * @return array
     */
    private function _getSuccessPageURLs(){
        $urls = Url::where('language_id', $this->crud->language_id)->get();

        $sectioned_urls = [];
        foreach( $urls as $ind => $url ){

            // Skip any URLs which are deleted
            if( !$url->url_object || !empty($url->url_object->deleted_at) ){ continue; }

            $index = ucwords( str_replace( "_", " ", $url->table_name ) );

            if( !isset( $sectioned_urls[ $index ] ) ){ $sectioned_urls[ $index ] = []; }

            $sectioned_urls[ $index ][ $url->id ] = @$url->url_object->title;
        }

        return $sectioned_urls;
    }

    /**
     * Get the Column & Form Field labels for the Form Builder section.
     *
     * @return array
     */
    public function getDefaultLabels(){
        return FormBuilderConfig::$labels;
    }

    /**
     * Get Form Field help text for the Form Builder section.
     *
     * @return array
     */
    public function getDefaultHelp(){
        return FormBuilderConfig::$help;
    }
}