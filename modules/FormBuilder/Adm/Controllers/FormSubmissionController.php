<?php
namespace Modules\FormBuilder\Adm\Controllers;

use Illuminate\Http\Request;
use App\Library\MyBespokeApi;
use Intervention\Image\Exception\NotFoundException;
use Adm\Controllers\Controller;
use Modules\FormBuilder\Models\Form;
use Modules\FormBuilder\Models\FormSubmission;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class FormSubmissionController
 * @package Modules\FormBuilder\Adm\Controllers
 */
class FormSubmissionController extends Controller {
    protected $request;

    protected $view_data = array();

    /**
     * Main Index function for all form submissions.
     * Will provide a table view allow linking to view the submissions for each custom form.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index( Request $request ){
        $this->request = $request;

        $this->_loadDefaultViewData();

        $this->view_data['title'] = 'Form Submissions';

        // Sorts
        $order = $request->get('order') ? $request->get('order') : 'asc';
        $current_sort = $request->get('sort') ? $request->get('sort') : 'title';

        $this->view_data['order'] = $order;
        $this->view_data['sort'] = $current_sort;

        switch( $current_sort ){
            case 'last_submission':
                $this->view_data['forms'] = Form::select('forms.*')
                    ->distinct()
                    ->leftJoin('forms_log AS fl', 'fl.form_id', '=', 'forms.form_id')
                    ->orderBy('fl.created_at', $order)
                    ->get();
                break;
            case 'number_of_submissions':
                $this->view_data['forms'] = Form::select('forms.form_id', 'forms.title')
                    ->addSelect(\DB::raw('COUNT(fl.forms_log_id) AS `aggregate`'))
                    ->leftJoin('forms_log AS fl', 'fl.form_id', '=', 'forms.form_id')
                    ->groupBy('forms.form_id', 'forms.title')
                    ->orderBy('aggregate', $order)
                    ->get();
                break;
            default:
                $this->view_data['forms'] = Form::orderBy($current_sort, $order)->get();
                break;
        }

        if( $this->view_data['forms']->count() == 1 ){
            $first_form = $this->view_data['forms']->first();
            abort( 302, '', ['Location' => route('adm.form-submissions.form', $first_form->form_id)] );
        }

        // Switch the 'order' ready for URLs
        $order = ($order == 'desc') ? 'asc' : 'desc';

        $this->view_data['sorts'] = array(
            'title' => route('adm.form-submissions') . '?sort=title&order=' . ($current_sort == 'title' ? $order : 'asc'),
            'last_submission' => route('adm.form-submissions') . '?sort=last_submission&order=' . ($current_sort == 'last_submission' ? $order : 'asc'),
            'number_of_submissions' => route('adm.form-submissions') . '?sort=number_of_submissions&order=' . ($current_sort == 'number_of_submissions' ? $order : 'asc')
        );

        view()->addNamespace('FormBuilder', base_path('/modules/FormBuilder/Adm/views'));
        return view( 'FormBuilder::form-listing', $this->view_data );
    }

    /**
     * Form view page.
     * Shows all form submissions for a particular form, with column headings of the form fields.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function form( Request $request ){
        $this->request = $request;

        $this->_loadDefaultViewData();

        $form_id = $request->route()->parameter('form_id');

        $form = Form::where( 'form_id', $form_id )->first();

        if( empty( $form ) ){
            throw new NotFoundException;
        }

        $this->view_data['title'] = $form->title . ' Submissions - ' . $form->submissions->count() . ' submissions';
        $this->view_data['form'] = $form;

        $this->view_data['breadcrumbs'][] = array(
            'label' => $form->title . ' Submissions',
            'url' => route('adm.form-submissions.form', $form->form_id)
        );

        $columns = array();
        $last_submission = $form->submissions()->orderBy('created_at', 'DESC')->first();

        if( !empty( $last_submission ) ){
            foreach( $last_submission->fields as $field => $value ){
                $columns[] = $field;
            }
        }

        $this->view_data['columns'] = $columns;

        view()->addNamespace('FormBuilder', base_path('/modules/FormBuilder/Adm/views'));
        return view( 'FormBuilder::form-submissions-listing', $this->view_data );
    }

    /**
     * Custom Form Submission Exports.
     * Exports the form submissions for the given form into a CSV file.
     * If the form requested doesn't exist, throw a 404 error.
     *
     * @param Request $request
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function export( Request $request ){
        $this->request = $request;

        $form_id = $request->route()->parameter('form_id');

        $form = Form::where( 'form_id', $form_id )->first();

        if( empty( $form ) ){
            throw new NotFoundHttpException;
        }

        $columns = array();
        $last_submission = $form->submissions()->orderBy('created_at', 'DESC')->first();

        if( !empty( $last_submission ) ){
            foreach( $last_submission->fields as $field => $value ){
                $columns[] = $field;
            }
        }

        if( $form->gdpr_compliance ){
            $columns[] = 'GDPR Consent';
            $columns[] = 'GDPR Consent Date';
            $columns[] = 'GDPR Consent Text';
        }

        $csv = '';
        $csv .= implode( ',', $columns ) . PHP_EOL;

        foreach( $form->submissions()->orderBy('created_at')->get() as $submission ){

            $submission_data = [];
            foreach( $columns as $column ){
                if( $column == 'GDPR Consent' ){
                    $submission_data[] = ($submission->gdpr_consent ? 'Consented' : 'Not Consented');
                    continue;
                } elseif( $column == 'GDPR Consent Date' ){
                    $submission_data[] = '"' . $submission->gdpr_consent_date . '"';
                    continue;
                } elseif( $column == 'GDPR Consent Text' ){
                    $submission_data[] = '"' . $submission->gdpr_consent_text . '"';
                    continue;
                }

                $data = isset( $submission->fields[ $column ] ) ? $submission->fields[ $column ] : 'No data';

                $submission_data[] = '"' . $data . '"';
            }

            $csv .= implode( ',', $submission_data ) . PHP_EOL;
        }

        $csv_name = \Illuminate\Support\Str::slug( $form->title . ' export ' . date("Y-m-d H:i:s") ) . ".csv";
        return \Response::make( $csv )->header("Content-type", "text/csv")->header("Content-disposition", "attachment; filename=\"" . $csv_name . "\"");
    }

    /**
     * Form Submission view page.
     * Shows a particular form submission, based on ID.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function submission( Request $request ){
        $this->request = $request;

        $this->_loadDefaultViewData();

        $form_id = $request->route()->parameter('form_id');

        $form = Form::where( 'form_id', $form_id )->first();

        if( empty( $form ) ){
            throw new NotFoundException;
        }

        $this->view_data['title'] = $form->title . ' Submission';
        $this->view_data['form'] = $form;

        $this->view_data['breadcrumbs'][] = array(
            'label' => $form->title . ' Submissions',
            'url' => route('adm.form-submissions.form', $form->form_id)
        );

        $submission_id = $request->route()->parameter('submission_id');

        $submission = FormSubmission::where([
            'forms_log_id' => $submission_id
        ])->first();

        if( empty( $form ) ){
            throw new NotFoundException;
        }

        $this->view_data['breadcrumbs'][] = array(
            'label' => $form->title . ' Submission',
            'url' => route('adm.form-submissions.submission', [$form->form_id, $submission->forms_log_id])
        );

        $fields = $submission->fields;
        foreach ($fields as $field => $value) {
            if( is_array( $value ) ){
                $fields[ $field ] = $value;
                continue;
            }

            if ( strstr($value, 'custom_form_uploads') !== false ) {

                $file_url = route('custom_form_upload', [str_replace('custom_form_uploads/', '', $value)]);

                $file_url_path = parse_url($file_url);
                $file_url_parts = explode('/', $file_url_path['path']);
                $filename = end($file_url_parts);

                $value = '<a target="_blank" href="' . $file_url . '">' . $filename . '</a>';
            }

            if ( strstr($value, PHP_EOL) ) {
                $value = nl2br($value);
            }

            $fields[ $field ] = $value;
        }
        $submission->fields = $fields;

        $this->view_data['submission'] = $submission;

        view()->addNamespace('FormBuilder', base_path('/modules/FormBuilder/Adm/views'));
        return view( 'FormBuilder::view-form-submission', $this->view_data);
    }

    /**
     * Delete Form Submissions.
     * Deletes Form Submissions based on ID, passed as a POST argument.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete( Request $request ){
        $submission_ids = $request->input( 'items' );
        if( empty( $submission_ids ) ){
            return response()->json( ['success' => false, 'error' => 'No Form Submissions were received to be deleted.'] );
        }

        $deleted_any = false;
        foreach( $submission_ids as $submission_id ){
            $FormSubmission = FormSubmission::where( 'forms_log_id', $submission_id )->first();

            if( $FormSubmission ){
                $deleted_any = true;
                $FormSubmission->delete();
            }
        }

        if( !$deleted_any ){
            return response()->json( ['success' => false, 'error' => 'No Form Submissions could be deleted. Please try again.'] );
        }

        return response()->json( ['success' => true] );
    }

    /**
     * Load in the default view data used by the base template,
     * including the logged in user, breadcrumbs & navigation
     */
    private function _loadDefaultViewData(){
        $this->view_data['me'] = \Illuminate\Support\Facades\Auth::user();

        $breadcrumbs = array();

        // Add the main dashboard link
        $breadcrumbs[] = array(
            'label' => 'Dashboard',
            'url' => route('adm.path')
        );

        $breadcrumbs[] = array(
            'label' => 'Form Submissions',
            'url' => route('adm.form-submissions')
        );

        $this->view_data['breadcrumbs'] = $breadcrumbs;

        $admController = new \Adm\Controllers\AdmController();
        $this->view_data['navigation'] = $admController->generateNavigation();

        $this->view_data['dimensions'] = '123x123';
        if( $this->request->input('dimensions') ){
            $this->view_data['dimensions'] = $this->request->input('dimensions');
        }

        $this->view_data['table'] = 'form-submissions';

        $my_bespoke_api = new MyBespokeApi;
        $this->view_data['relationship_manager'] = $my_bespoke_api->getRelationshipManager();
    }
}