@extends('layouts.adm')

@section('page_title', $title)

@section('content')

    <div class="js-section-action-bar-container relative" style="height: 79px;">
        <div class="js-section-action-bar section-action-bar top-0 left-0 width-100">
            <div class="row padding-y-gutter-full">
                <div class="column">
                    <div class="table width-100">
                        <div class="table-cell">
                            <h1 class="font-size-24 font-weight-600 margin-0">{{ $title }}</h1>
                        </div>
                        <div class="table-cell text-right">
                            <a href="{{ route('adm.form-submissions.form', [$form->form_id]) }}" class="button round bordered colour-cancel margin-b0 margin-r-gutter margin-b0">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php // Edit View Main Content ?>
    <div class="content-area margin-b-gutter-full">

        <div class="row">
            <div class="column small-9">
                <div class="edit-main-content section-container background colour-white radius margin-b-gutter-full">

                    <div data-content-tabs class="edit-content-tabs">
                        <div class="table width-100">
                            <div class="table-cell width-100">
                                <ul data-tab-group="edit-main-content" class="tabs simple-list inline-list font-size-0">
                                    <li>
                                        <a href="#" data-tab-id="content" title="Edit Content" class="active font-size-14 block padding-x-gutter-full padding-y-gutter">Content</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div data-tab-group="edit-main-content" class="tabs-content">

                        <div id="content" data-tab-id="content" class="content active">

                            <div class="input-row">
                                <span class="block font-size-14 font-weight-600 margin-b-gutter">Form</span>
                                <span class="block font-size-14 margin-b-gutter-full">{{ $submission->form->title }}</span>
                            </div>

                            <div class="input-row">
                                <span class="block font-size-14 font-weight-600 margin-b-gutter">URL Submitted From</span>
                                <span class="block font-size-14 margin-b-gutter-full"><a target="_blank" href="{{ $submission->url }}">{{ str_replace( route('theme'), '', $submission->url ) }}</a></span>
                            </div>

                            <div class="input-row">
                                <span class="block font-size-14 font-weight-600 margin-b-gutter">IP Address</span>
                                <span class="block font-size-14 margin-b-gutter-full">{{ $submission->ip_address }}</span>
                            </div>

                            <div class="input-row">
                                <span class="block font-size-14 font-weight-600 margin-b-gutter">GDPR Consent</span>
                                <span class="block font-size-14 margin-b-gutter-full">
                                    @if( $submission->gdpr_consent )
                                    Consented on {{ $submission->gdpr_consent_date }}<br/>
                                    Message Displayed: {{ $submission->gdpr_consent_text }}
                                    @else
                                    Not Consented
                                    @endif
                                </span>
                            </div>

                            <div class="input-row">
                                <span class="block font-size-14 font-weight-600 margin-b15">Submitted Details</span>
                                <div class="border-1 colour-grey-lightest radius margin-b-gutter-full">
                                    @foreach( $submission->fields as $field => $value )
                                        <?php if( is_array( $value ) ){ $value = implode( "<br/>", $value ); } ?>
                                        <div class="input-row padding-x0 padding-l-gutter colour-grey-dark">
                                            <div class="row margin-b-gutter-full">
                                                <div class="column small-3">
                                                    <span class="block font-size-14 font-weight-600 padding-l-gutter-full">
                                                        {{ ucwords($field) }}
                                                    </span>
                                                </div>
                                                <div class="column small-9">
                                                    <div class="block font-size-14 padding-r-gutter-full">
                                                        @if( preg_match( '/(.*)-\d{4}-\d{2}-\d{2}-\d{6}(\..*)/', $value ) )
                                                            {!! preg_replace( '/(.*)-\d{4}-\d{2}-\d{2}-\d{6}(\..*)/', '$1$2', $value ) !!}
                                                        @else
                                                            {!! $value !!}
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>

    </div>

@endsection