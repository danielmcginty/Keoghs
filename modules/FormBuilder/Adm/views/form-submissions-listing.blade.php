@extends('layouts.adm')

@section('page_title', $title)

@section('content')

    <div class="js-section-action-bar-container relative" style="height: 79px;">
        <div class="js-section-action-bar section-action-bar top-0 left-0 width-100">
            <div class="row padding-y-gutter-full">
                <div class="column">
                    <div class="table width-100">
                        <div class="table-cell">
                            <h1 class="font-size-24 font-weight-600 margin-0">{{ $title }}</h1>
                        </div>
                        <div class="table-cell text-right">
                            <div class="inline-block vertical-middle">
                                <a href="{{ route('adm.form-submissions.export', [$form->form_id]) }}" class="button round bordered colour-default text-shadow-none margin-b0">Export CSV</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content-area margin-b-gutter-full">

        <div class="js-action-form row margin-b-gutter-full none">
            <div class="column">
                <div class="inline-block vertical-middle margin-l5">
                    <a href="#" data-reveal-id="delete-modal" class="js-delete-button button round bordered colour-error margin-b0">Delete Selected</a>

                    <div id="delete-modal" class="reveal-modal notice-modal" data-reveal aria-hidden="true" role="dialog">
                        <div class="modal-content">
                            <p class="modal-heading padding-r40 font-weight-600 margin-b-gutter">Delete Selected {{ $title }}</p>
                            <p class="font-size-14">Are you sure you want to delete the selected {{ $title }}?</p>
                        </div>
                        <div class="modal-actions">
                            <a href="#" class="js-delete-confirm button round gradient bordered colour-error margin-b0 margin-r-gutter">Delete</a>
                            <a href="#" class="js-cancel-delete button round bordered colour-cancel margin-b0">Cancel</a>
                        </div>
                        <a class="close-reveal-modal visually-hidden" aria-label="Close">&#215;</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="column">

                <div class="section-container background colour-white radius margin-b-gutter-full">

                    <table class="list-table width-100 border-0">
                        <thead>
                        <tr>
                            <th class="list-table-checkbox-col">
                                <div class="block">
                                    <input type="checkbox" name="selected[]" id="selected-all" class="js-check-all fancy-checkbox margin-0">
                                    <label for="selected-all" class="font-size-16 fixed-font-size" title="Select All"><span class="visually-hidden">Select All</span></label>
                                </div>
                            </th>
                            @foreach( $columns as $column )
                                <th>{{ $column }}</th>
                            @endforeach
                            <th>Date Submitted</th>
                            <th>GDPR Consent</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>

                        @if( sizeof( $form->submissions()->orderBy('created_at', 'DESC')->get() ) == 0 )

                            <tr>
                                <td colspan="99">
                                    <span class="block padding-gutter">No form submissions found.</span>
                                </td>
                            </tr>

                        @else

                            @foreach( $form->submissions()->orderBy('created_at', 'DESC')->get() as $submission )
                                <tr data-form-submission class="cursor-pointer">
                                    <td class="list-table-checkbox-col text-center">
                                        <div class="block">
                                            <input type="checkbox" name="selected[]" value="<?php echo $submission->forms_log_id; ?>" id="selected-<?php echo $submission->forms_log_id; ?>" class="fancy-checkbox margin-0">
                                            <label for="selected-<?php echo $submission->forms_log_id; ?>" class="font-size-16 fixed-font-size" title="Select"><span class="visually-hidden">Select</span></label>
                                        </div>
                                    </td>
                                    @foreach( $columns as $column )
                                        <td>
                                            @if( isset( $submission->fields[ $column ] ) )
                                                {{ is_array( $submission->fields[ $column ] ) ? implode(", ", $submission->fields[ $column ] ) : $submission->fields[ $column ] }}
                                            @else
                                                No Data
                                            @endif
                                        </td>
                                    @endforeach
                                    <td>{{ $submission->created_at }}</td>
                                    <td>{{ $submission->gdpr_consent ? 'Consented' : 'Not Consented' }}</td>
                                    <td class="text-right">
                                        <div class="inline-block vertical-middle">
                                            <a data-form-submission-link href="{{ route('adm.form-submissions.submission', [$form->form_id, $submission->forms_log_id]) }}" class="button tiny round bordered colour-cancel text-shadow-none margin-b0 margin-l-gutter padding-x-gutter-full">View</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach

                        @endif

                        </tbody>
                    </table>

                </div>

            </div>
        </div>

    </div>

    <?php // Dynamic modal for general messages ?>
    <div id="dynamic-modal" class="reveal-modal notice-modal" data-reveal aria-hidden="true" role="dialog">
        <div class="modal-content">
            <p data-dynamic-modal-title class="modal-heading padding-r40 font-weight-600 margin-b-gutter-full"></p>
            <div data-dynamic-modal-content class="font-size-14"></div>
        </div>
        <div class="modal-actions">
            <a href="#" data-confirm-button class="button round gradient bordered colour-error margin-b0 margin-r10">Confirm</a>
            <a href="#" data-close-reveal class="button round bordered colour-cancel margin-b0">Okay</a>
        </div>
    </div>

@endsection

@push('footer_scripts')
<script type="text/javascript">
var $dynamicModal;
$(document).ready( function(){
    $dynamicModal = $('#dynamic-modal');

    $(document).on('click', '[data-form-submission]', function( e ){
        var proceed_to_open = true;
        var $target = $(e.target);
        if( $target.hasClass('list-table-checkbox-col') ){ proceed_to_open = false; }
        if( proceed_to_open ) {
            $.each( $target.parents(), function( ind, parent ){
                if( $(parent).hasClass('list-table-checkbox-col') ){
                    proceed_to_open = false;
                }
            });
        }

        if( proceed_to_open ) {
            e.preventDefault();
            e.stopPropagation();

            window.location.href = $(this).find('[data-form-submission-link]').attr('href');
            return false;
        }
    });

    $(document).on('change', '.js-check-all', function() {
        if(this.checked) {
            var $notChecked = $('[name*="selected"]:not(:disabled)');
            $notChecked.prop('checked', true);
            $notChecked.closest('tr').addClass('selected-row');
        } else {
            var $checked = $('[name*="selected"]');
            $checked.prop('checked', false);
            $checked.closest('tr').removeClass('selected-row');
        }
    });

    // Show/hide the search and action forms
    $(document).on('change', '[name*="selected"]', function() {
        $(this).closest('tr').toggleClass('selected-row');
        // Toggle selected item actions bar
        if ( $('[name*="selected"]:checked').length > 0 ) {
            $('.js-search-form').addClass('none');
            $('.js-action-form').removeClass('none');
        } else {
            $('.js-search-form').removeClass('none');
            $('.js-action-form').addClass('none');
        }
    });

    $('.js-cancel-delete').on('click touchend', function(e) {
        e.preventDefault();

        //$('#delete-modal').foundation('reveal', 'close'); // more simple alternative but requires having to update the ID

        var parentModal = $(this).closest('[data-reveal]');
        parentModal.find('.close-reveal-modal').click();
    });
    $('#delete-modal .js-delete-confirm').on('click touchend', function(){
        $('#delete-modal').foundation('reveal', 'close');
        bulk_delete();
    });

    // Show a clone of the dynamic modal, with a given title, content and buttons
    function getDynamicModal( options ){
        var $thisModal = $dynamicModal.clone();

        $thisModal.find('[data-dynamic-modal-title]').text( options.title );
        $thisModal.find('[data-dynamic-modal-content]').html( options.content );

        if( options.confirmButton ){
            $thisModal.find('[data-confirm-button]').text( options.confirmButton );
        } else {
            $thisModal.find('[data-confirm-button]').remove();
        }

        if( options.closeButton ){
            $thisModal.find('[data-close-reveal]').text( options.closeButton );
        }

        $(document).on('closed.fndtn.reveal', $thisModal, function () {
            $(this).remove();
        });

        return $thisModal;
    }

    function getSelected(){
        var selectedItems = [];

        // Push all selected items
        $('[name*="selected"]:checked').each(function() {
            // Push each selected id into an array
            var itemID = $(this).val();
            selectedItems.push(itemID);
        });

        return selectedItems;
    }

    function bulk_delete(){
        $.ajax({
            url: '{{ route('adm.form-submissions.delete') }}',
            type: 'POST',
            data: {
                _token: '{{ csrf_token() }}',
                items: getSelected()
            },
            dataType: 'json',
            beforeSend: function(){
                showPageLoadingOverlay( true );
            },
            success: function( json ){
                if( json.success ){
                    window.location.reload();
                } else {
                    var title = 'Delete {{ $title }}';
                    var content = '<p>Some {{ $title }} couldn\'t be deleted.</p>';
                    content += '<p>' + json.error + '</p>';

                    var $modal = getDynamicModal({
                        title: title,
                        content: content
                    });
                    $('body').append($modal);
                    $modal.foundation('reveal', 'open');

                    $modal.find('[data-close-reveal]').off('click').click( function(){
                        window.location.reload();
                    });
                }
            },
            complete: function(){
                showPageLoadingOverlay( false );
            }
        });
    }
});
</script>
@endpush