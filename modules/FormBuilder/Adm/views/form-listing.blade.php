@extends('layouts.adm')

@section('page_title', $title)

@section('content')

    <div class="js-section-action-bar-container relative" style="height: 79px;">
        <div class="js-section-action-bar section-action-bar top-0 left-0 width-100">
            <div class="row padding-y-gutter-full">
                <div class="column">
                    <div class="table width-100">
                        <div class="table-cell">
                            <h1 class="font-size-24 font-weight-600 margin-0">{{ $title }}</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content-area margin-b-gutter-full">

        <div class="row">
            <div class="column">

                <div class="section-container background colour-white radius margin-b-gutter-full">

                    <table class="list-table width-100 border-0">
                        <thead>
                        <tr>
                            <th class="orderable{{ $sort == 'title' ? ' ordered ordered-' . $order : '' }}" data-order-field="title" data-order="{{ $sort == 'title' ? $order : 'asc' }}">
                                <a href="{{ $sorts['title'] }}" class="block colour-grey-dark">Custom Form</a>
                            </th>
                            <th class="orderable{{ $sort == 'last_submission' ? ' ordered ordered-' . $order : '' }}" data-order-field="last_submission" data-order="{{ $sort == 'last_submission' ? $order : 'asc' }}">
                                <a href="{{ $sorts['last_submission'] }}" class="block colour-grey-dark">Last Submission</a>
                            </th>
                            <th class="orderable{{ $sort == 'number_of_submissions' ? ' ordered ordered-' . $order : '' }}" data-order-field="number_of_submissions" data-order="{{ $sort == 'number_of_submissions' ? $order : 'asc' }}">
                                <a href="{{ $sorts['number_of_submissions'] }}" class="block colour-grey-dark">Number of Submissions</a>
                            </th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>

                        @if( sizeof( $forms ) == 0 )

                            <tr>
                                <td colspan="99">
                                    <span class="block padding-gutter">No forms found.</span>
                                </td>
                            </tr>

                        @else

                            @foreach( $forms as $i => $form )
                                <?php $last_submission = $form->submissions()->orderBy('created_at', 'DESC')->first(); ?>
                                <tr>
                                    <td>{{ $form->title }}</td>
                                    <td>{{ is_object( $last_submission ) ? $last_submission->created_at : 'No Submissions' }}</td>
                                    <td>{{ $form->submissions()->count() }}</td>
                                    <td class="text-right">
                                        <div class="inline-block vertical-middle">
                                            <a href="{{ route('adm.form-submissions.form', $form->form_id) }}" class="button tiny round bordered colour-cancel text-shadow-none margin-b0 margin-l-gutter padding-x-gutter-full">View Submissions</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach

                        @endif

                        </tbody>
                    </table>

                </div>

            </div>
        </div>

    </div>

@endsection