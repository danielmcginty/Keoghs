<?php
namespace Modules\FormBuilder\Adm\Models\PageBuilder;

use Adm\Crud\LaraCrud;
use Adm\Models\PageBuilder\PageBuilderBase;
use Modules\FormBuilder\Models\Form as FormModel;

/**
 * Class FormBuilder
 * @package Modules\FormBuilder\Adm\Models\PageBuilder
 *
 * Page Builder Model for the Form Builder.
 */
class FormBuilder extends PageBuilderBase {
    /**
     * The name of the block, to be displayed in the block selection modal, and in the collapsible element on forms.
     *
     * @var string
     */
    public $display_name = 'Custom Form';

    /**
     * A description of the block, displayed in the block selection modal.
     *
     * @var string
     */
    public $description = 'Add a pre-built custom form to the page. (from "Forms")';

    /**
     * The fields to be set by the user for this block.
     *
     * @var array
     */
    public $builder_fields = [
        'form' => [
            'display_name' => 'Custom Form',
            'help' => 'Choose a form you\'ve created in "Forms"',
            'type' => 'select',
            'options' => [],
            'extra_data' => [
                'empty_value' => ''
            ],
            'required' => true,
            'validation' => array(
                'required' => 'You need to choose a form.'
            )
        ]
    ];

    /**
     * Page Builder Callback.
     * Perform this function before rendering the block on a form.
     *
     * @param LaraCrud $crud
     */
    public function callbackBeforePrepare(LaraCrud $crud){
        $forms = FormModel::where('language_id', $crud->language_id)->get();

        $options = [];
        foreach( $forms as $form ){
            $options[ $form->form_id ] = $form->title;
        }

        $this->builder_fields['form']['options'] = $options;
    }
}