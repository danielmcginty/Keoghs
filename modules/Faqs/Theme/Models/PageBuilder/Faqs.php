<?php
namespace Modules\Faqs\Theme\Models\PageBuilder;

use App\Models\PageBuilder\BaseBlockModel;
use App\Models\Url;
use Illuminate\Database\Eloquent\Collection;
use Modules\Faqs\Theme\Models\Faq;

class Faqs extends BaseBlockModel {

    public function getValue( $page_builder_block, $field, $original_value ){
        switch( $field ){
            case 'faqs':
                $FAQs = new Collection();
                if( is_json( $original_value ) && !empty( $original_value ) ){
                    foreach( (array)json_decode( $original_value, true ) as $raw_faq ){
                        $FAQ = Faq::where( 'faq_id', $raw_faq['value'] )->first();
                        if( $FAQ ){
                            $FAQs->push( $FAQ );
                        }
                    }
                }
                return $FAQs;
                break;
            default:
                return parent::getValue( $page_builder_block, $field, $original_value );
                break;
        }
    }

    public function postProcess( $fields ){
        $fields['landing_page'] = Url::getSystemPage( 'faq', 'landing' );

        return $fields;
    }

}