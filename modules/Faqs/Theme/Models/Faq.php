<?php
namespace Modules\Faqs\Theme\Models;

use App\Models\Scopes\LanguageScope;
use App\Models\Scopes\PublishedScope;
use App\Models\Traits\HasCompositePrimaryKey;
use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faq extends Model {

    use SoftDeletes, HasCompositePrimaryKey, HasQueryBuilderCache;

    protected $table = 'faqs';

    protected $primaryKey = ['faq_id', 'language_id'];

    protected static function boot(){
        parent::boot();

        static::addGlobalScope( new LanguageScope );
        static::addGlobalScope( new PublishedScope );
    }

    public function categories(){
        return $this->belongsToMany( '\Modules\Faqs\Theme\Models\FaqCategory', 'faq_to_faq_category', 'faq_id', 'faq_category_id' );
    }

    public function scopeInCategory( $query, $faq_category_id ){
        $query->leftJoin( 'faq_to_faq_category', 'faq_to_faq_category.faq_id', '=', $this->table . '.faq_id' );
        $query->where( 'faq_to_faq_category.faq_category_id', '=', $faq_category_id );

        return $query;
    }

}