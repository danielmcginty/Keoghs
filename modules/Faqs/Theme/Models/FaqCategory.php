<?php
namespace Modules\Faqs\Theme\Models;

use App\Models\CommonModel;
use App\Models\Scopes\LanguageScope;
use App\Models\Scopes\PublishedScope;
use App\Models\Traits\HasCompositePrimaryKey;
use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\SoftDeletes;

class FaqCategory extends CommonModel {

    use SoftDeletes, HasCompositePrimaryKey, HasQueryBuilderCache;

    protected $table = 'faq_categories';

    protected $primaryKey = ['faq_category_id', 'language_id'];

    protected static function boot(){
        parent::boot();

        static::addGlobalScope( new LanguageScope );
        static::addGlobalScope( new PublishedScope );
    }

    public function faqs(){
        return $this->belongsToMany( '\Modules\Faqs\Theme\Models\Faq', 'faq_to_faq_category', 'faq_category_id', 'faq_id' )->orderBy( 'sort_order' );
    }

}