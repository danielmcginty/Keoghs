<?php
namespace Modules\Faqs\Theme\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Url;
use Modules\Faqs\Theme\Models\Faq;
use Modules\Faqs\Theme\Models\FaqCategory;

class FaqController extends Controller {

    public function category( $faq_category_id ){
        $this_page = FaqCategory::where( 'faq_category_id', $faq_category_id )->first();
        if( $this_page ){
            $home = Url::getSystemPage('home', 'index');
            if( $home ) {
                $this->addBreadcrumb(route('theme', ['url' => $home->url]), $home->getAttributes()['title']);
            }

            $LandingPage = Url::getSystemPage( 'faq', 'landing' );
            foreach( $this->getBreadcrumbs( $LandingPage, 'title', 'parent_page' ) as $breadcrumb ){
                $this->addBreadcrumb( route('theme', ['url' => $breadcrumb['url']]), $breadcrumb['label'] );
            }

            $this->addBreadcrumb( route('theme', $this_page->url), $this_page->title );

            $this->LandingPage = $LandingPage;
            
            $this->pageTitle = $this_page->title;
            $this->this_page = $this_page;

            $this->_processListing( $faq_category_id );
        }
    }

    public function landing(){
        $this_page = Url::getSystemPage( 'faq', 'landing' );
        if( $this_page ){
            $home = Url::getSystemPage('home', 'index');
            if( $home ) {
                $this->addBreadcrumb(route('theme', ['url' => $home->url]), $home->getAttributes()['title']);
            }

            foreach( $this->getBreadcrumbs( $this_page, 'title', 'parent_page' ) as $breadcrumb ){
                $this->addBreadcrumb( route('theme', ['url' => $breadcrumb['url']]), $breadcrumb['label'] );
            }

            $this->LandingPage = $this_page;

            $this->pageTitle = $this_page->title;
            $this_page->description = $this_page->main_content;
            $this->this_page = $this_page;

            $this->_processListing();
        }
    }

    private function _processListing( $faq_category_id = null ){
        $this->FaqCategories = FaqCategory::orderBy( 'sort_order' )->get();

        $Faqs = Faq::distinct();
        if( $faq_category_id ){
            $Faqs = $Faqs->inCategory( $faq_category_id );
        }
        $this->Faqs = $Faqs->orderBy( 'faqs.sort_order' )->paginate( 10 );

        $this->view = 'faqs.listing';
    }

}