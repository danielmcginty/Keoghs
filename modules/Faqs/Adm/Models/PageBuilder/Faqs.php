<?php
namespace Modules\Faqs\Adm\Models\PageBuilder;

use Adm\Crud\LaraCrud;
use Adm\Models\PageBuilder\PageBuilderBase;
use App\Models\Scopes\LanguageScope;
use Modules\Faqs\Theme\Models\Faq;

class Faqs extends PageBuilderBase {

    public $display_name        = 'FAQs';

    public $description         = 'Add a set of FAQs to the page';

    public $builder_fields      = [
        'heading'   => [
            'display_name'  => 'Heading',
            'help'          => 'Add a heading to the block',
            'type'          => 'string',
            'required'      => false,
            'validation'    => [],
            'group'         => 'Main'
        ],
        'faqs'      => [
            'display_name'  => 'FAQs',
            'help'          => 'Select the FAQs to display',
            'type'          => 'orderable_multiple_select',
            'options'       => [],
            'required'      => true,
            'validation'    => [
                'required'      => 'You need to select at least 1 FAQ',
                'min:1'         => 'You need to select at least 1 FAQ'
            ],
            'group'         => 'Main'
        ]
    ];

    public function callbackBeforePrepare( LaraCrud $crud ){
        $options = [];
        foreach( Faq::withoutGlobalScope( LanguageScope::class )->where( 'language_id', $crud->language_id )->orderBy( 'title' )->get() as $FAQ ){
            $options[ $FAQ->faq_id ] = $FAQ->title;
        }
        $this->builder_fields['faqs']['options'] = $options;
    }

}