<?php
namespace Modules\Faqs\Adm\Controllers;

use Adm\Controllers\AdmController;
use App\Models\Url;
use Modules\Faqs\Config\FaqCategoryConfig;
use Modules\Faqs\Config\FaqConfig;

class ModuleController extends AdmController {

    public function faqs(){
        $this->crud->setTable( 'faqs' );
        $this->crud->setSubject( 'FAQs' );

        $this->crud->addColumn( 'faq_categories' );
        $this->crud->multiRelation( 'faq_categories', 'faq_to_faq_category', 'faq_categories', 'faq_id', 'faq_category_id', 'title', 'sort_order', 'faq-categories' );

        $this->crud->columns( 'title', 'question', 'sort_order', 'status' );

        $this->crud->columnGroup( 'main', ['title', 'question', 'answer', 'faq_categories'] );
    }

    public function getFaqsDefaultLabels(){
        return FaqConfig::$labels;
    }

    public function getFaqsDefaultHelp(){
        return FaqConfig::$help;
    }

    public function faq_categories(){
        $this->crud->setTable( 'faq_categories' );
        $this->crud->setSubject( 'FAQ Categories' );

        $this->crud->addColumn( 'path' );
        $SystemPage = Url::getSystemPage( 'faq', 'landing' );
        if( $SystemPage ){
            $this->crud->setBasePathPrefix( 'pages', $SystemPage->page_id, 'page_id' );
        }

        $this->crud->addColumn( 'faqs' );
        $this->crud->multiRelation( 'faqs', 'faq_to_faq_category', 'faqs', 'faq_category_id', 'faq_id', 'title', 'sort_order' );

        $this->crud->columns( 'title', 'path', 'sort_order', 'status' );

        $this->crud->columnGroup( 'main', ['title', 'path', 'faqs', 'description'] );
    }

    public function getFaqCategoriesDefaultLabels(){
        return FaqCategoryConfig::$labels;
    }

    public function getFaqCategoriesDefaultHelp(){
        return FaqCategoryConfig::$help;
    }

}