<?php
namespace Modules\Faqs\Config;

use Adm\Interfaces\NavigationInterface;

class Navigation implements NavigationInterface {

    public function getNavItems(){
        return [
            [ 'title' => 'FAQs', 'url' => route('adm.path', 'faqs'), 'position' => 50, 'sub' => [
                    [ 'title' => 'FAQs', 'url' => route('adm.path', 'faqs'), 'position' => 0 ],
                    [ 'title' => 'Categories', 'url' => route('adm.path', 'faq-categories'), 'position' => 10 ]
                ]
            ]
        ];
    }

}