<?php
namespace Modules\Faqs\Config;

class FaqConfig {

    /**
     * Stores input labels
     * @var array
     */
    public static $labels = [
        'title'             => 'Reference',
        'question'          => 'Question',
        'answer'            => 'Answer',
        'faq_categories'    => 'Categories'
    ];

    /**
     * Stores input help text
     * @var array
     */
    public static $help = [
        'title'             => 'A reference for this FAQ, used in the CMS',
        'question'          => 'The Frequently Asked Question',
        'answer'            => 'The answer to the question',
        'faq_categories'    => 'The categories that this FAQ belongs to'
    ];

}