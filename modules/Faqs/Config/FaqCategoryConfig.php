<?php
namespace Modules\Faqs\Config;

class FaqCategoryConfig {

    public static $labels = [
        'title'         => 'Title',
        'h1_title'      => 'H1 Title',
        'description'   => 'Category Content',
        'faqs'          => 'FAQs',
    ];

    public static $help = [
        'description'   => 'Optional content to display when viewing this category',
        'faqs'          => 'Select the FAQs that belong to this category'
    ];

}