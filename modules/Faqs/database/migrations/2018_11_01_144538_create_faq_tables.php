<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaqTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'faqs', function( Blueprint $table ){
            $table->integer( 'faq_id' );
            $table->integer( 'language_id' );
            $table->integer( 'version' );
            $table->string( 'title' );
            $table->string( 'question' );
            $table->text( 'answer' );
            $table->integer( 'sort_order' )->default( 0 );
            $table->boolean( 'status' )->default( 0 );
            $table->timestamps();
            $table->softDeletes();
            $table->primary( ['faq_id', 'language_id', 'version'], 'faq_pk' );
        });

        Schema::create( 'faq_categories', function( Blueprint $table ){
            $table->integer( 'faq_category_id' );
            $table->integer( 'language_id' );
            $table->integer( 'version' );
            $table->string( 'title' );
            $table->text( 'description' )->nullable();
            $table->integer( 'sort_order' )->default( 0 );
            $table->boolean( 'status' )->default( 0 );
            $table->timestamps();
            $table->softDeletes();
            $table->primary( ['faq_category_id', 'language_id', 'version'], 'faq_category_pk' );
        });

        Schema::create( 'faq_to_faq_category', function( Blueprint $table ){
            $table->integer( 'faq_id' );
            $table->integer( 'faq_category_id' );
            $table->integer( 'sort_order' );
            $table->primary( ['faq_id', 'faq_category_id'], 'faq_join_pk' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'faqs' );
        Schema::drop( 'faq_categories' );
        Schema::drop( 'faq_to_faq_category' );
    }
}
