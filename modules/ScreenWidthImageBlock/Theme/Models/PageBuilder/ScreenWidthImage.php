<?php
namespace Modules\ScreenWidthImageBlock\Theme\Models\PageBuilder;

use App\Models\PageBuilder\BaseBlockModel;
use Modules\ScreenWidthImageBlock\Adm\Models\PageBuilder\ScreenWidthImage as AdmBlock;

class ScreenWidthImage extends BaseBlockModel {

    public function getValue( $page_builder_block, $field, $original_value ){
        switch( $field ){
            case 'image':
                $AdmBlock = new AdmBlock();
                return parent::getAssetFieldValue(
                    $page_builder_block,
                    $field,
                    $original_value,
                    $AdmBlock->column_extras['image']['sizes']
                );
            default:
                return parent::getValue( $page_builder_block, $field, $original_value );
        }
    }

}