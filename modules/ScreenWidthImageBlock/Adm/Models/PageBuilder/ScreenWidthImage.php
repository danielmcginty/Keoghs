<?php
namespace Modules\ScreenWidthImageBlock\Adm\Models\PageBuilder;

use Adm\Models\PageBuilder\PageBuilderBase;

class ScreenWidthImage extends PageBuilderBase {

    public $display_name    = 'Screen Width Image';

    public $description     = 'A single image, as wide as the screen.';

    public $builder_fields  = [
        'image' => [
            'display_name'  => 'Image',
            'help'          => 'Select the image you would like to display, optionally selecting different images for different screen sizes.',
            'type'          => 'asset',
            'required'      => true,
            'validation'    => [
                'desktop'       => [
                    'required' => 'You need to select a Desktop image.'
                ]
            ],
            'array_validation_type' => 'single'
        ]
    ];

    public $column_extras   = [
        'image' => [
            'multi_size' => true,
            'sizes' => [
                'desktop' => [
                    'label'     => 'Desktop',
                    'width'     => 1920,
                    'height'    => 0,
                    'primary'   => true
                ],
                'mobile' => [
                    'label'     => 'Mobile',
                    'width'     => 640,
                    'height'    => 0,
                    'primary'   => false
                ]
            ]
        ]
    ];

}