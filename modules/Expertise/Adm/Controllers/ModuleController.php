<?php

namespace Modules\Expertise\Adm\Controllers;

use Adm\Controllers\AdmController;
use App\Models\Url;
use Modules\Expertise\Config\ExpertiseConfig;

class ModuleController extends AdmController
{
    public function expertise()
    {
        $this->crud->setTable('expertise');
        $this->crud->setSubject('Expertise');

        $this->crud->addColumn('path');
        $SystemPage = Url::getSystemPage('expertise', 'index');
        if ($SystemPage) {
            $this->crud->setBasePathPrefix('pages', $SystemPage->page_id, 'page_id');
        }
        $this->crud->setPathPrefix('expertise', 'parent_id', 'expertise_id');

        $this->crud->setRelation('parent_id', 'expertise', 'title', 'expertise_id');

        $this->crud->changeType('icon', 'asset');

        $this->crud->changeType('related_expertise', 'orderable_multiple_select');
        $expt_opts = [];
        foreach (\Modules\Expertise\Theme\Models\Expertise::orderBy('title')->get() as $Expertise) {
            $expt_opts[$Expertise->expertise_id] = $Expertise->title;
        }
        $this->crud->fieldOptions('related_expertise', $expt_opts);

        $this->crud->setDefaultValue('status', true);

        $this->crud->addColumn('tags');
        $this->crud->multiRelation(
            'tags',
            'expertise_tag',
            'tags',
            'expertise_id',
            'tag_id',
            'title'
        );

        $this->crud->columns('image', 'title', 'parent_id', 'path', 'status');

        $this->crud->columnGroup(
            'main',
            ['title', 'path', 'title_long', 'parent_id', 'icon', 'image', 'subtitle', 'introduction', 'body', 'related_expertise']
        );
        $this->crud->columnGroup('side', ['status']);
    }

    public function getExpertiseDefaultLabels()
    {
        return ExpertiseConfig::$labels;
    }

    public function getExpertiseDefaultHelp()
    {
        return ExpertiseConfig::$help;
    }

    public function getExpertiseAssetConfig($field_name)
    {
        return ExpertiseConfig::$image_sizes[$field_name] ?? [];
    }
}