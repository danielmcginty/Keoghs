<?php

namespace Modules\Expertise\Adm\Models\PageBuilder;

use Adm\Crud\LaraCrud;
use Adm\Models\PageBuilder\PageBuilderBase;
use App\Models\Scopes\LanguageScope;
use Modules\Expertise\Theme\Models\Expertise;

class OurExpertise extends PageBuilderBase
{
    public $display_name = 'Our Expertise';

    public $description = 'Add a set of Expertise to the page';

    public $builder_fields = [
        'heading' => [
            'display_name' => 'Heading',
            'help' => 'Add a heading to the block',
            'type' => 'string',
            'required' => false,
            'validation' => [],
            'group' => 'Main'
        ],
        'description' => [
            'display_name' => 'Description',
            'help' => 'Add a description to the block',
            'type' => 'text',
            'wysiwyg' => true,
            'required' => false,
            'validation' => [],
            'group' => 'Main'
        ],
        'expertise' => [
            'display_name' => 'Expertise',
            'help' => 'Select the Expertise to display. Leave empty to show the top-level expertise pages.',
            'type' => 'orderable_multiple_select',
            'options' => [],
            'required' => true,
            'validation' => [
                'required' => 'You need to select at least 1 Expertise page',
                'min:1' => 'You need to select at least 1 Expertise page'
            ],
            'group' => 'Main'
        ]
    ];

    public function callbackBeforePrepare(LaraCrud $crud)
    {
        $options = [];
        $expertise = Expertise::query()
            ->withoutGlobalScope(LanguageScope::class)
            ->where('language_id', $crud->language_id)
            ->orderBy('title')
            ->get();

        foreach ($expertise as $article) {
            if (!empty($article->svg_icon) || (!empty($article->icon) && !empty($article->icon['primary']))) {
                $options[$article->expertise_id] = $article->title;
            }
        }
        $this->builder_fields['expertise']['options'] = $options;
    }
}