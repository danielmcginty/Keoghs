<?php

namespace Modules\Expertise\Config;

class ExpertiseConfig
{
    const CASUALTY_ID = 47;
    const MOTOR_ID = 50;
    const LEGACY_ID = 68;
    const COMPLEX_ID = 79;
    const CORPORATE_ID = 80;

    /**
     * @var array
     */
    public static $labels = [
        'title' => 'Title',
        'title_long' => 'Title (Long)',
        'subtitle' => 'Subtitle',
        'introduction' => 'Introduction',
        'body' => 'Body',
        'image' => 'Image',
        'icon' => 'Icon',
        'parent_id' => 'Parent',
        'status' => 'Status',
        'related_expertise' => 'Related Expertise'
    ];

    /**
     * @var array
     */
    public static $help = [
        //
    ];

    /**
     * @var array
     */
    public static $image_sizes = [
        'image' => [
            'article' => [
                'label' => 'Banner',
                'width' => 1920,
                'height' => 640,
                'primary' => true
            ],
            'listing' => [
                'label' => 'Listing',
                'width' => 960,
                'height' => 0,
                'primary' => false
            ]
        ],
        'icon' => [
            'primary' => [
                'label' => 'Primary',
                'width' => 640,
                'height' => 640,
                'primary' => true
            ],
        ]
    ];

    public static $icon_svg = [
        self::CASUALTY_ID => 'expertise.svg.casualty',
        self::MOTOR_ID => 'expertise.svg.motor',
        self::LEGACY_ID => 'expertise.svg.legacy',
        self::COMPLEX_ID => 'expertise.svg.complex',
        self::CORPORATE_ID => 'expertise.svg.corporate',
    ];
}