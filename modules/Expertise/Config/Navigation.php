<?php

namespace Modules\Expertise\Config;

use Adm\Interfaces\NavigationInterface;

class Navigation implements NavigationInterface
{
    public function getNavItems()
    {
        return [
            [
                'title' => 'Expertise',
                'url' => route('adm.path', 'expertise'),
                'position' => 40,
            ]
        ];
    }
}