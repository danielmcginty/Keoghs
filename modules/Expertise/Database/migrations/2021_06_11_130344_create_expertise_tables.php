<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpertiseTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expertise', function (Blueprint $table) {
            $table->integer('expertise_id');
            $table->integer('language_id');
            $table->integer('version');
            $table->string('title');
            $table->string('title_long')->nullable();
            $table->string('subtitle')->nullable();
            $table->text('introduction')->nullable();
            $table->text('body')->nullable();
            $table->string('image')->nullable();
            $table->string('icon')->nullable();
            $table->integer('parent_id')->default(0)->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->primary(['expertise_id', 'language_id', 'version'], 'expertise_pk');
        });

        Schema::create('expertise_tag', function (Blueprint $table) {
            $table->integer('expertise_id');
            $table->integer('tag_id');
            $table->primary(['expertise_id', 'tag_id'], 'expertise_tag_pk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expertise');
        Schema::dropIfExists('expertise_tag');
    }
}
