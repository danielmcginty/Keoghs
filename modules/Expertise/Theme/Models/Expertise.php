<?php

namespace Modules\Expertise\Theme\Models;

use App\Helpers\AssetHelper;
use App\Models\CommonModel;
use App\Models\Scopes\LanguageScope;
use App\Models\Scopes\PublishedScope;
use App\Models\Traits\HasCompositePrimaryKey;
use App\Models\Traits\HasQueryBuilderCache;
use App\Models\Url;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Expertise\Config\ExpertiseConfig;
use Modules\OurPeople\Theme\Models\Person;
use Modules\Tags\Theme\Models\Tag;

class Expertise extends CommonModel
{
    use SoftDeletes, HasCompositePrimaryKey, HasQueryBuilderCache;

    protected $table = 'expertise';

    protected $primaryKey = ['expertise_id', 'language_id'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new LanguageScope);
        static::addGlobalScope(new PublishedScope);
    }

    public function parent()
    {
        return $this->belongsTo(Expertise::class, 'parent_id', 'expertise_id');
    }

    public function children()
    {
        return $this->hasMany(Expertise::class, 'parent_id', 'expertise_id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'expertise_tag', 'expertise_id', 'tag_id', 'expertise_id', 'tag_id');
    }

    public function people()
    {
        return $this->belongsToMany(Person::class, 'people_expertise', 'expertise_id', 'person_id', 'expertise_id', 'person_id');
    }

    public function scopeTopLevel($query, $except = 0)
    {
        return $query
            ->where(function ($query) {
                $query->where('parent_id', 0)->orWhereNull('parent_id');
            })
            ->when(intval($except) !== 0, function ($query) use ($except) {
                return $query->where('expertise_id', '!=', $except);
            });
    }

    public function scopeWithPeople($query, $people)
    {
        return $query->whereHas('people', function ($query) use ($people) {
            return $query->where('person_id', $people);
        });
    }

    public function scopeWithTag($query, $tag)
    {
        return $query->whereHas('tags', function ($query) use ($tag) {
            return $query->where('title', $tag);
        });
    }

    public function getImageAttribute()
    {
        $AssetHelper = new AssetHelper();
        $Image_Sizes = ExpertiseConfig::$image_sizes;
        $Image_Sizes['image']['schema'] = [
            'label' => 'Schema Image',
            'width' => 696,
            'height' => 392,
            'primary' => false
        ];
        $Image_Sizes['image']['amp'] = [
            'label' => 'AMP',
            'width' => 640,
            'height' => 350,
            'primary' => false
        ];
        return $AssetHelper->getAssets(
            $this->table,
            $this->expertise_id,
            $this->language_id,
            $this->version,
            'image',
            $Image_Sizes['image']
        );
    }

    public function getSvgIconAttribute()
    {
        if (isset(ExpertiseConfig::$icon_svg[$this->expertise_id])) {
            return ExpertiseConfig::$icon_svg[$this->expertise_id];
        }

        return null;
    }

    public function getIconAttribute()
    {
        return (new AssetHelper())->getAssets(
            $this->table,
            $this->expertise_id,
            $this->language_id,
            $this->version,
            'icon',
            ExpertiseConfig::$image_sizes['icon']
        );
    }

    public function getTitleLongAttribute($value)
    {
        return empty(trim($value)) ? $this->title : $value;
    }

    public function getPageBuilderAttribute()
    {
        $blocks = parent::getPageBuilderAttribute();
        if (!empty($blocks)) {
            return $blocks;
        }

        $hub_page = Url::getSystemPage('expertise', 'index');
        if ($hub_page) {
            return $hub_page->page_builder;
        }

        return [];
    }

    public function getRelatedExpertiseAttribute($value)
    {
        $Related = new Collection();
        if (!empty($value) && is_json($value)) {
            foreach ((array)json_decode($value, true) as $raw_expt) {
                if (empty($raw_expt['value'])) { continue; }

                $RelExp = self::where('expertise_id', $raw_expt['value'])->first();
                if ($RelExp) {
                    $Related->push($RelExp);
                }
            }
        }
        return $Related;
    }
}