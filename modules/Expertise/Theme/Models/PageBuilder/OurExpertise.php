<?php

namespace Modules\Expertise\Theme\Models\PageBuilder;

use App\Models\PageBuilder\BaseBlockModel;
use Illuminate\Support\Collection;
use Modules\Expertise\Theme\Models\Expertise;

class OurExpertise extends BaseBlockModel
{
    public function getValue($page_builder_block, $field, $original_value)
    {
        switch ($field) {
            case 'expertise':
                $Expertise = new Collection();
                if (is_json($original_value) && !empty($original_value)) {
                    foreach ((array) json_decode($original_value, true) as $raw_expertise) {
                        $Article = Expertise::where('expertise_id', $raw_expertise['value'])->first();
                        if ($Article) {
                            $Expertise->push($Article);
                        }
                    }
                }

                if ($Expertise->isEmpty()) {
                    $TopLevelExpertise = Expertise::query()->topLevel()->orderBy('title')->get();
                    foreach ($TopLevelExpertise as $Article) {
                        if (!empty($Article->svg_icon) || (!empty($Article->icon) && !empty($Article->icon['primary'])) ) {
                            $Expertise->push($Article);
                        }
                    }
                }
                
                return $Expertise;
            default:
                return parent::getValue($page_builder_block, $field, $original_value);
        }
    }

    public function postProcess($fields)
    {
        if (!isset($fields['heading']) || empty($fields['heading'])) {
            $fields['heading'] = "Expertise";
        }

        return $fields;
    }
}