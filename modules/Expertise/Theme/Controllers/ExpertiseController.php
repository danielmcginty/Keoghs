<?php

namespace Modules\Expertise\Theme\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Url;
use Modules\Expertise\Theme\Models\Expertise;
use Modules\Insights\Theme\Models\Insight;
use Illuminate\Support\Collection;

class ExpertiseController extends Controller
{
    public function index()
    {
        return $this->redirect(Expertise::query()->orderBy('parent_id')->orderBy('title')->first()->url);
    }

    public function article($id)
    {
        $this_page = Expertise::query()->where('expertise_id', $id)->first();

        if (!$this_page) {
            return;
        }

        $home = Url::getSystemPage('home', 'index');
        if ($home) {
            $this->addBreadcrumb(route('theme', ['url' => $home->url]), $home->getAttributes()['title']);
        }

        $expertise_page = Url::getSystemPage('expertise', 'index');
        $this->addBreadcrumb(route('theme', [$expertise_page->url]), $expertise_page->title);

        if ($this_page->parent) {
            $this->addBreadcrumb(route('theme', [$this_page->parent->url]), $this_page->parent->title);
        }

        $this->addBreadcrumb(route('theme', [$this_page->url]), $this_page->title);

        $this->canonical_url = route('theme', $this_page->url);
        $this->this_page = $this_page;

        if (!empty($this_page->image) && !empty($this_page->image['article'])) {
            $this->addSocialMeta('og:image', $this_page->image['article']['src']);
            $this->addSocialMeta('og:image:width', $this_page->image['article']['width']);
            $this->addSocialMeta('og:image:height', $this_page->image['article']['height']);

            $this->addSocialMeta('twitter:image', $this_page->image['article']['src']);
        }

        $this->specialist_areas = $this->getSpecialistAreas($this_page);
        $this->key_contacts = $this->getKeyContacts($this_page);
        $this->contacts_page = Url::getSystemPage('person', 'index');
        $this->related_insights = $this->getRelatedInsights($this_page);
        $this->insights_landing_page = Url::getSystemPage('insight', 'index');
        $this->insights_page = Url::getSystemPage('insight', 'index');
        $this->other_expertise = $this->getOtherExpertise($this_page);

        $this->view = 'expertise.article';
        if ($this_page->parent) {
            $this->view = 'expertise.sub-article';
        }
    }

    private function getSpecialistAreas(Expertise $expertise)
    {
        if ($expertise->related_expertise->isNotEmpty()) {
            return $expertise->related_expertise;
        }
        
        if ($expertise->parent) {
            return $expertise->parent->children;
        }

        return $expertise->children;
    }

    private function getKeyContacts(Expertise $expertise)
    {
        return $expertise->people;
    }

    private function getRelatedInsights(Expertise $expertise)
    {
        return Insight::query()
            ->withTags($expertise->tags->map(function ($tag) {
                return $tag->tag_id;
            })->toArray())
            ->orderBy('published_at', 'desc')
            ->take(2)
            ->get();
    }

    private function getOtherExpertise(Expertise $expertise)
    {
        $exclude = $expertise->parent ? $expertise->parent->expertise_id : $expertise->expertise_id;

        $Expertise = new Collection();
        $TopLevelExpertise = Expertise::query()->topLevel($exclude)->orderBy('title')->get();
        foreach ($TopLevelExpertise as $Article) {
            if (!empty($Article->svg_icon) || (!empty($Article->icon) && !empty($Article->icon['primary']))) {
                $Expertise->push($Article);
            }
        }
        return $Expertise;
    }
}