<?php

namespace Modules\Expertise\Library;

use App\Models\AssetGroup;
use App\Models\File;
use App\Models\FileDirectory;
use App\Models\Url;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Modules\Tags\Theme\Models\Tag;

class ExpertiseImportProcessor
{
    const BASE_IMAGE_URL = "https://keoghs.co.uk";
    const ASSET_PATH = "/assets/site/content/images/";

    private $expertise = [];
    private $tags = [];
    private $prefix_url = null;

    public function process()
    {
        $this->processFileContents(
            $this->parseDataFile($this->locateDataFile('modules/Expertise/assets/expertise_data.csv')),
        );
    }

    private function locateDataFile(string $filename)
    {
        return fopen(base_path($filename), 'r');
    }

    private function parseDataFile($file): array
    {
        $processed_header = false;

        $num_processed = 0;

        $expertise = [];

        $this->log('Processing New Expertise Data CSV ...');

        while (($row = fgetcsv($file)) !== false) {
            if (!$processed_header) {
                $processed_header = true;
                continue;
            }

            $num_processed++;

            $row = $this->mapDataFileFields($row);

            if (empty($row['ID'])) {
                continue;
            }

            $expertise[$row['ID']] = [
                'id' => $row['ID'],
                'title' => $row['TITLE'],
                'title_long' => $row['TITLE_LONG'],
                'slug' => $row['ALIAS'],
                'status' => $row['ACTIVE'],
                'introduction' => $row['INTRODUCTION'],
                'body' => $row['BODY'],
                'image_url' => static::BASE_IMAGE_URL.$row['IMAGE_PATH'],
                'image_name' => $row['IMAGE_NAME'],
                'created_at' => $row['CREATED_AT'],
                'updated_at' => $row['UPDATED_AT'],
            ];
        }

        $this->log("New Expertise Data CSV Processed - {$num_processed} rows processed");

        return $expertise;
    }

    private function processFileContents(array $expertise_data)
    {
        $this->syncExpertise($expertise_data);
    }

    private function mapDataFileFields($row): array
    {
        $fields = [
            'ID',
            'TITLE',
            'TITLE_LONG',
            'ALIAS',
            'ACTIVE',
            'INTRODUCTION',
            'BODY',
            'IMAGE_PATH',
            'IMAGE_NAME',
            'CREATED_AT',
            'UPDATED_AT',
        ];

        $mapped = [];

        foreach ($fields as $ind => $field) {
            $mapped[$field] = isset($row[$ind]) ? trim($row[$ind]) : null;

            if ($mapped[$field] === 'NULL') {
                $mapped[$field] = null;
            }
        }

        return $mapped;
    }

    private function syncExpertise(array $expertise_data): void
    {
        $this->log("Truncating Expertise Table...");

        $files = DB::table('files')
            ->whereIn('id', function ($query) {
                return $query->select('image')->from('expertise');
            })
            ->get();

        foreach ($files as $file) {
            Storage::delete($file->storage_path);
        }

        DB::table('files')
            ->whereIn('id', function ($query) {
                return $query->select('image')->from('expertise');
            })
            ->delete();

        DB::table('asset_groups')
            ->where('table_name', 'expertise')
            ->delete();

        DB::table('301_redirects')
            ->where('old_url', 'LIKE', 'expertise/%')
            ->delete();

        DB::table('urls')
            ->where('table_name', 'expertise')
            ->delete();

        DB::table('expertise_tag')->truncate();

        DB::table('people_expertise')->truncate();

        DB::table('expertise')->truncate();

        $this->log("Expertise Table Truncated");

        $expertise_collection = collect($expertise_data);

        $this->log("Inserting {$expertise_collection->count()} Expertise...");

        $expertise_collection->chunk(500)
            ->each(function (Collection $expertise) {
                DB::table('expertise')
                    ->insert($expertise->map(function (array $expertise) {
                        return [
                            'expertise_id' => $expertise['id'],
                            'language_id' => 1,
                            'version' => 1,
                            'title' => $expertise['title'],
                            'title_long' => $expertise['title_long'],
                            'introduction' => $expertise['introduction'],
                            'body' => $expertise['body'],
                            'image' => $this->loadExternalImage($expertise),
                            'status' => $expertise['status'],
                            'created_at' => $expertise['created_at'],
                            'updated_at' => $expertise['updated_at'],
                        ];
                    })->toArray());
            });

        $this->log("{$expertise_collection->count()} Expertise Inserted");

        $expertise_collection->each(function (array $expertise) {
            $this->insertUrl($expertise);
        });
    }

    private function log(string $message)
    {
        info($message);
    }

    private function loadExternalImage(array $expertise): ?string
    {
        $image_url = $expertise['image_url'];
        $image_name = $expertise['image_name'];

        if (strlen(trim($image_url)) === 0) {
            return null;
        }

        if (strlen(trim($image_name)) === 0) {
            $image_name = $expertise['title'];
        }

        $full_prefix = static::BASE_IMAGE_URL.static::ASSET_PATH;

        $relative_path = substr_replace($image_url, '', strpos($image_url, $full_prefix), strlen($full_prefix));

        $directory_path = trim(rtrim($relative_path, basename($relative_path)), '/');

        $directories = array_filter(explode('/', $directory_path));

        $directory = $this->createDirectoryPath($directories);

        try {
            $image = file_get_contents($image_url);
        } catch (\Exception $e) {
            return null;
        }

        sleep(0.5);

        return $this->storeImage($expertise, $image, $relative_path, $image_name, $directory);
    }

    private function createDirectoryPath(array $directories)
    {
        $processed_directories = [];

        foreach ($directories as $index => $directory) {
            $fd = FileDirectory::firstOrCreate([
                'parent' => $index > 0 ? $processed_directories[$index - 1] : 0,
                'path' => $directory,
                'name' => Str::title($directory)
            ]);

            $processed_directories[] = $fd->id;
        }

        return end($processed_directories);
    }

    private function storeImage($expertise, $image, $path, $name, $directory)
    {
        $extension = pathinfo($path, PATHINFO_EXTENSION);

        if (empty($extension)) {
            return null;
        }

        Storage::put($uploadPath = Str::random(40).".".$extension, $image);

        // Make sure the file has an extension (just in case validation passes somehow)
        if (empty($extension)) {
            $extension = 'txt';
        }

        // Make sure that we use .jpg instead of .jpeg
        $extension = str_replace('jpeg', 'jpg', $extension);

        // And default the alt text to the generated name.
        $alt_text = $name;

        $new_file = new File();
        $new_file->directory_id = $directory;
        $new_file->storage_path = $uploadPath;
        $new_file->file_name = Str::slug($name).".".$extension;
        $new_file->name = $name;
        $new_file->alt_text = $alt_text;
        $new_file->extension = $extension;
        $new_file->created_by = 1;

        $new_file->save();

        $this->insertAsset($expertise, 'article', $new_file);
        $this->insertAsset($expertise, 'listing', $new_file);

        return $new_file->id;
    }

    private function insertAsset(array $expertise, string $image_size, $new_file)
    {
        $asset = new AssetGroup();

        $asset->table_name = 'expertise';
        $asset->table_key = $expertise['id'];
        $asset->language_id = 1;
        $asset->version = 1;
        $asset->field = 'image';
        $asset->image_size = $image_size;
        $asset->file_id = $new_file->id;
        $asset->alt = $new_file->name;
        $asset->width = 0;
        $asset->height = 0;
        $asset->x_coordinate = 0;
        $asset->y_coordinate = 0;
        $asset->cropbox_left = 0;
        $asset->cropbox_top = 0;

        $asset->save();
    }

    private function addTagToExpertise(array $expertise, $tag_name)
    {
        if (empty(trim($tag_name))) {
            return;
        }

        if (isset($this->tags[$tag_name])) {
            return $this->connectTagToExpertise($expertise['id'], $this->tags[$tag_name]);
        }

        $tag = Tag::query()->where('title', $tag_name)->first();

        if (is_null($tag)) {
            $tag = new Tag();
            $tag->title = $tag_name;
            $tag->save();
        }

        $this->tags[$tag_name] = $tag->tag_id;

        return $this->connectTagToExpertise($expertise['id'], $tag->tag_id);
    }

    private function connectTagToExpertise($expertise, $tag)
    {
        DB::table('expertise_tag')->insertOrIgnore([
            'expertise_id' => $expertise,
            'tag_id' => $tag,
        ]);
    }

    private function insertUrl(array $expertise)
    {
        if (empty($this->prefix_url)) {
            $this->prefix_url = Url::getSystemPage('expertise', 'index');
        }

        $new_slug = Str::slug($expertise['slug']);

        DB::table('urls')->insert([
            'url' => $url = "{$this->prefix_url->url}/{$new_slug}",
            'table_name' => 'expertise',
            'table_key' => $expertise['id'],
            'language_id' => 1,
            'meta_title' => "{$expertise['title']}",
        ]);

        DB::table('301_redirects')->insert([
            'old_url' => "expertise/{$expertise['slug']}",
            'new_url' => $url,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}