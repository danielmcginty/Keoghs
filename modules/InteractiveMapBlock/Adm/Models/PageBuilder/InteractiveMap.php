<?php
namespace Modules\InteractiveMapBlock\Adm\Models\PageBuilder;

use Adm\Models\PageBuilder\PageBuilderBase;

class InteractiveMap extends PageBuilderBase {

    public $display_name        = 'Interactive Map';

    public $description         = 'Add an interactive map to the page';

    public $builder_fields      = [
        'latitude'  => [
            'display_name'  => 'Latitude',
            'help'          => 'The latitude position for the center of the map',
            'type'          => 'string',
            'required'      => true,
            'validation'    => [
                'required'      => 'You need to provide a latitude position'
            ],
            'group'         => 'Main'
        ],
        'longitude' => [
            'display_name'  => 'Longitude',
            'help'          => 'The longitude position for the center of the map',
            'type'          => 'string',
            'required'      => true,
            'validation'    => [
                'required'      => 'You need to provide a longitude position'
            ],
            'group'         => 'Main'
        ],
        'zoom'      => [
            'display_name'  => 'Zoom Level',
            'help'          => 'Set the default zoom level for the map',
            'type'          => 'select',
            'options'       => [ 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15 ],
            'required'      => false,
            'validation'    => [],
            'group'         => 'Main'
        ],
        'address'   => [
            'display_name'  => 'Address',
            'help'          => 'Set the address to display within the marker popup',
            'type'          => 'text',
            'required'      => true,
            'validation'    => [
                'required'      => 'You need to provide an address'
            ],
            'group'         => 'Main'
        ]
    ];

}