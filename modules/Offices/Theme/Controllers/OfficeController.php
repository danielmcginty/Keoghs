<?php
namespace Modules\Offices\Theme\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Url;
use Modules\Offices\Theme\Models\Office;

/**
 * Class OfficeController
 * @package Modules\Offices\Theme\Controllers
 *
 * Front-end Controller responsible for the rendering of office pages, and
 * the offices landing page
 */
class OfficeController extends Controller {

    /**
     * Index
     * Renders a single office page, based on the ID passed through the router
     *
     * @param $office_id
     */
    public function index( $office_id ){
        $this_page = Office::where( 'office_id', $office_id )->first();
        if( $this_page ){
            $home = Url::getSystemPage('home', 'index');
            if( $home ) {
                $this->addBreadcrumb(route('theme', ['url' => $home->url]), $home->getAttributes()['title']);
            }

            $landing_page = Url::getSystemPage( 'office', 'landing' );
            if( $landing_page ){
                foreach( $this->getBreadcrumbs( $landing_page, 'title', 'parent_page' ) as $breadcrumb ){
                    $this->addBreadcrumb( route('theme', ['url' => $breadcrumb['url']]), $breadcrumb['label'] );
                }
            }

            $this->addBreadcrumb( route('theme', $this_page->url), $this_page->title );

            $this->pageTitle = $this_page->title;
            $this->this_page = $this_page;

            $this->view = 'offices.office';
        }
    }

    /**
     * Landing
     * Renders the offices landing page, based on a system page route
     */
    public function landing(){
        $this_page = Url::getSystemPage( 'office', 'landing' );
        if( $this_page ){
            $home = Url::getSystemPage('home', 'index');
            if( $home ) {
                $this->addBreadcrumb(route('theme', ['url' => $home->url]), $home->getAttributes()['title']);
            }

            foreach( $this->getBreadcrumbs( $this_page, 'title', 'parent_page' ) as $breadcrumb ){
                $this->addBreadcrumb( route('theme', ['url' => $breadcrumb['url']]), $breadcrumb['label'] );
            }

            $this->pageTitle = $this_page->title;
            $this->this_page = $this_page;

            $this->Offices = Office::orderBy( 'sort_order' )->get();

            $this->view = 'offices.archive';
        }
    }

}