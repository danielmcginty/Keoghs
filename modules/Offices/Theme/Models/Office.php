<?php
namespace Modules\Offices\Theme\Models;

use App\Helpers\AssetHelper;
use App\Library\MinifyHtml;
use App\Models\CommonModel;
use App\Models\Scopes\LanguageScope;
use App\Models\Scopes\PublishedScope;
use App\Models\Traits\HasCompositePrimaryKey;
use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Offices\Config\OfficeConfig;

/**
 * Class Office
 * @package Modules\Offices\Theme\Models
 *
 * Eloquent Model used for interaction with the 'offices' database table
 */
class Office extends CommonModel {
    /*
     * We have a deleted_at field, a composite primary key and want to cache
     * queries, so include the relevant traits
     */
    use SoftDeletes, HasCompositePrimaryKey, HasQueryBuilderCache;

    /**
     * The model's database table
     *
     * @var string
     */
    protected $table = 'offices';

    /**
     * The models' primary key fields
     *
     * @var array
     */
    protected $primaryKey = ['office_id', 'language_id'];

    protected $appends = ['meta_title', 'meta_description', 'no_index'];

    /**
     * When retrieving from the database, we only want records in the correct
     * language that are published, so include some global scopes
     */
    protected static function boot(){
        parent::boot();

        static::addGlobalScope( new PublishedScope );
        static::addGlobalScope( new LanguageScope );
    }

    /**
     * Accessor function
     * Used to return an array of image details, rather than the stored ID,
     * for an office
     *
     * @return array
     */
    public function getImageAttribute(){
        $AssetHelper = new AssetHelper();
        return $AssetHelper->getAssets( $this->table, $this->office_id, $this->language_id, $this->version, 'image', OfficeConfig::$image_sizes['image'] );
    }

    public function getMapPopupContentAttribute(){
        if( view()->exists( 'offices.partials.marker-popup' ) ){
            $html = view()->make( 'offices.partials.marker-popup', ['Office' => $this] )->render();

            $Minifier = new MinifyHtml();
            return $Minifier->minifyContent( $html );
        }

        return '';
    }
}