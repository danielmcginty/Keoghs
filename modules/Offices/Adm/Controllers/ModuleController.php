<?php
namespace Modules\Offices\Adm\Controllers;

use Adm\Controllers\AdmController;
use App\Models\Url;
use Modules\Offices\Config\OfficeConfig;

class ModuleController extends AdmController {

    /**
     * Offices
     *
     * CRUD function for the offices area of the CMS
     */
    public function offices(){
        $this->crud->setTable( 'offices' );
        $this->crud->setSubject( 'Offices' );

        $this->crud->addColumn( 'path' );

        $SystemPage = Url::getSystemPage( 'office', 'landing' );
        if( $SystemPage ){
            $this->crud->setBasePathPrefix( 'pages', $SystemPage->page_id, 'page_id' );
        }

        $this->crud->addColumn( 'page_builder' );
        $this->crud->changeType( 'page_builder', 'page_builder' );
        $this->crud->enablePageBuilder();

        $this->crud->columns( 'title', 'path', 'sort_order', 'status' );

        $this->crud->columnGroup( 'main', ['title', 'path', 'h1_title', 'image', 'address', 'telephone', 'email', 'opening_times', 'page_builder'] );
        $this->crud->columnGroup( 'side', ['status', 'sort_order', 'latitude', 'longitude'] );
    }

    /**
     * Get Default Labels
     * Called by LaraCrud getDefaultsCallback to populate the field labels based on the Config file
     *
     * @return array
     */
    public function getOfficesDefaultLabels(){
        return OfficeConfig::$labels;
    }

    /**
     * Get Default Help
     * Called by LaraCrud getDefaultsCallback to populate the field help text based on the Config file
     *
     * @return array
     */
    public function getOfficesDefaultHelp(){
        return OfficeConfig::$help;
    }

    /**
     * Get Asset Config
     * Called by AssetType _getAssetFieldConfig to determine whether a field is an asset group, or just a simple asset
     * based on the Config file
     *
     * @param $field_name
     * @return array
     */
    public function getOfficesAssetConfig( $field_name ){
        return isset( OfficeConfig::$image_sizes[ $field_name ] ) ? OfficeConfig::$image_sizes[ $field_name ] : [];
    }

}