<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'offices', function( Blueprint $table ){
            $table->integer( 'office_id' );
            $table->integer( 'language_id' );
            $table->integer( 'version' )->default( 1 );
            $table->string( 'title' );
            $table->string( 'h1_title' )->nullable();
            $table->text( 'image' )->nullable();
            $table->text( 'address' )->nullable();
            $table->string( 'telephone' )->nullable();
            $table->string( 'email' )->nullable();
            $table->text( 'opening_times' )->nullable();
            $table->string( 'latitude' )->nullable();
            $table->string( 'longitude' )->nullable();
            $table->integer( 'sort_order' )->default( 0 );
            $table->boolean( 'status' )->default( 0 );
            $table->timestamps();
            $table->softDeletes();
            $table->primary( ['office_id', 'language_id', 'version'], 'offices_primary_key' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'offices' );
    }
}
