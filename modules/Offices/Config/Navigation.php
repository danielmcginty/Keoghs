<?php
namespace Modules\Offices\Config;

use Adm\Interfaces\NavigationInterface;

/**
 * Class Navigation
 * @package Modules\Offices\Config
 *
 * The CMS Navigation Builder for the Offices Module
 */
class Navigation implements NavigationInterface {

    /**
     * Returns the CMS Navigation links for the module
     *
     * @return array
     */
    public function getNavItems(){
        return [
            [ 'title' => 'Offices', 'url' => route('adm.path', 'offices'), 'position' => 30 ]
        ];
    }

}