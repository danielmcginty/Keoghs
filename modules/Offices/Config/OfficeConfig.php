<?php
namespace Modules\Offices\Config;

class OfficeConfig {

    /**
     * Stores input labels
     * @var array
     */
    public static $labels = [
        'title'         => 'Title',
        'h1_title'      => 'H1 Title',
        'image'         => 'Banner Image',
        'address'       => 'Address',
        'telephone'     => 'Telephone Number',
        'email'         => 'Email Address',
        'opening_times' => 'Opening Times',
        'latitude'      => 'Latitude',
        'longitude'     => 'Longitude'
    ];

    /**
     * Stores input help text
     * @var array
     */
    public static $help = [
        'address'       => 'The office address',
        'telephone'     => 'The office telephone number',
        'email'         => 'The office email address',
        'opening_times' => 'The office opening times',
        'latitude'      => 'The latitude position of the office, used for positioning a map marker',
        'longitude'     => 'The longitude position of the office, used for positioning a map marker'
    ];

    /**
     * Stores a shared configuration for image sizes
     * @var array
     */
    public static $image_sizes = [
        'image' => [
            'desktop' => [
                'label'     => 'Desktop',
                'width'     => 1920,
                'height'    => 400,
                'primary'   => true,
                'required'  => false
            ],
            'mobile' => [
                'label'     => 'Mobile',
                'width'     => 640,
                'height'    => 300,
                'primary'   => false,
                'required'  => false
            ]
        ]
    ];

}