@extends( 'layouts.adm' )

@section( 'page_title', 'Page Builder Wizard - Results' )

@section( 'content' )

    <div class="js-section-action-bar-container relative" style="height: 79px;">
        <div class="js-section-action-bar section-action-bar top-0 left-0 width-100">
            <div class="row padding-y-gutter-full">
                <div class="column">
                    <div class="table width-100">
                        <div class="table-cell">
                            <h1 class="font-size-24 font-weight-600 margin-0">Page Builder Wizard - Results</h1>
                        </div>
                        <div class="table-cell text-right">
                            <a href="javascript:void(0)" onclick="window.history.back()" class="button round bordered colour-default margin-b0 margin-l-gutter">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content-area margin-b-gutter-full">
        <div class="row">

            <div class="column small-9">

                <div class="edit-main-content section-container background colour-white radius margin-b-gutter-full">

                    <div class="tabs-content">

                        <div class="input-row">
                            <h2>Using Your Block</h2>
                            <p>{!! nl2br(e($resultsMessage)) !!}</p>
                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>

@endsection
