<div data-asset-size class="table width-100 margin-b-gutter">
    <div class="table-cell medium-4 padding-r-gutter">
        {{ Form::text( $field_prefix . '[asset_size][' . $asset_index . '][label]', null, ['class' => 'form-control margin-b0'] ) }}
    </div>
    <div class="table-cell medium-2 padding-r-gutter">
        {{ Form::number( $field_prefix . '[asset_size][' . $asset_index . '][width]', null, ['class' => 'form-control margin-b0'] ) }}
    </div>
    <div class="table-cell medium-2 padding-r-gutter">
        {{ Form::number( $field_prefix . '[asset_size][' . $asset_index . '][height]', null, ['class' => 'form-control margin-b0'] ) }}
    </div>
    <div class="table-cell medium-1 text-right">
        <a href="javascript:void(0)" data-remove-asset-size title="Remove Size" class="inline-block vertical-top padding-gutter font-size-14">
            <i class="fi-adm fi-adm-close margin-t1 colour-error"></i>
        </a>
    </div>
</div>