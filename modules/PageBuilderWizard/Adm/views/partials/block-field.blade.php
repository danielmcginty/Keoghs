<?php
$id_prefix = uniqid() . '-';
if( !isset( $field_prefix ) ){
    $field_prefix = 'field[' . $index . ']';
}
?>
<div data-block-field data-is-repeatable="{{ isset( $is_repeatable_field ) && $is_repeatable_field ? 'true' : 'false' }}" data-index="{{ $index }}" class="page-builder-block margin-b-gutter-full">
    <div class="block-inner">

        <div class="block-heading relative nowrap no-select font-size-14">
            <div data-{{ isset( $is_repeatable_field ) && $is_repeatable_field ? 'repeatable' : 'block' }}-field-drag-handle class="block-handle cursor-drag cursor-drag-active-state padding-gutter absolute top-0 left-0 colour-grey">
                <i class="fi-adm fi-adm-drag-handle colour-grey-dark"></i>
            </div>
            <div class="block-heading-inner ellipsis colour-grey">
                <span data-block-field-title class="block-title font-weight-600 colour-slate margin-r-gutter"></span>
            </div>
            <div class="block-actions absolute top-0 right-0 font-size-0 padding-r5">
                <a href="javascript:void(0)" data-block-view-toggle title="Expand / Collapse Field" class="block-toggle inline-block vertical-top padding-gutter font-size-14">
                    <i class="fi-adm colour-grey margin-t1"></i>
                </a>
                <a href="javascript:void(0)" data-remove-{{ isset( $is_repeatable_field ) && $is_repeatable_field ? 'repeatable-field' : 'block' }} title="Remove Field" class="inline-block vertical-top padding-gutter font-size-14">
                    <i class="fi-adm fi-adm-close margin-t1 colour-error"></i>
                </a>
            </div>
        </div>

        <div class="block-content">

            <div class="tabs-content">
                <div data-generic-field class="input-row">
                    <div class="table width-100 margin-b-gutter-full">
                        <div class="table-cell medium-2 vertical-middle">
                            <label for="{{ $id_prefix }}[label]" class="inline-block">Label</label>
                        </div>
                        <div class="table-cell medium-10 vertical-middle">
                            {{ Form::text( $field_prefix . '[label]', null, ['data-block-label' => '', 'class' => 'form-control margin-b0', 'id' => $id_prefix . '[label]'] ) }}
                        </div>
                    </div>
                </div>
                <div data-generic-field class="input-row">
                    <div class="table width-100 margin-b-gutter-full">
                        <div class="table-cell medium-2 vertical-middle">
                            <label for="{{ $id_prefix }}[help_text]" class="inline-block">Help Text</label>
                        </div>
                        <div class="table-cell medium-10 vertical-middle">
                            {{ Form::text( $field_prefix . '[help_text]', null, ['class' => 'form-control margin-b0', 'id' => $id_prefix . '[help_text]'] ) }}
                            <div class="help-text margin-0">Optionally add help text to this field.</div>
                        </div>
                    </div>
                </div>
                @if( !isset( $is_repeatable_field ) || !$is_repeatable_field )
                <div data-generic-field data-group-field class="input-row">
                    <div class="table width-100 margin-b-gutter-full">
                        <div class="table-cell medium-2 vertical-middle">
                            <label for="{{ $id_prefix }}[group]" class="inline-block">Field Group</label>
                        </div>
                        <div class="table-cell medium-10 vertical-middle">
                            {{ Form::text( $field_prefix . '[group]', null, ['class' => 'form-control margin-b0', 'id' => $id_prefix . '[group]'] ) }}
                            <div class="help-text margin-0">Optionally add this field to a 'group', for example 'Copy', 'Visual', 'Advanced', etc.</div>
                        </div>
                    </div>
                </div>
                @endif
                <div class="input-row">
                    <div class="table width-100 margin-b-gutter-full">
                        <div class="table-cell medium-2 vertical-middle">
                            <label for="{{ $id_prefix }}[field_type]" class="inline-block">Field Type</label>
                        </div>
                        <div class="table-cell medium-10 vertical-middle">
                            {{ Form::select( $field_prefix . '[field_type]', $field_types, null, ['data-block-field-type' => '', 'class' => 'form-control margin-b0', 'id' => $id_prefix . '[field_type]'] ) }}
                        </div>
                    </div>
                </div>
                @if( !isset( $is_repeatable_field ) || !$is_repeatable_field )
                <div data-generic-field class="input-row">
                    <div class="table width-100 margin-b-gutter-full">
                        <div class="table-cell medium-2 vertical-middle">
                            <label class="inline-block">Required?</label>
                        </div>
                        <div class="table-cell medium-2 vertical-middle">
                            <input data-required-toggle type="checkbox" id="{{ $id_prefix }}[required]" name="{{ $field_prefix }}[required]" value="1" class="fancy-checkbox margin-0" />
                            <label for="{{ $id_prefix }}[required]" class="font-size-16 fixed-font-size">
                                <span class="font-size-14 line-height-1pt2">Yes</span>
                            </label>
                        </div>
                        <div class="table-cell medium-8 vertical-middle">
                            <div data-required-toggle-cell class="none">
                                <div class="table width-100">
                                    <div class="table-cell medium-4 vertical-middle">
                                        <label for="{{ $id_prefix }}[required_message]" class="inline-block">Validation Message</label>
                                    </div>
                                    <div class="table-cell medium-8 vertical-middle">
                                        {{ Form::text( $field_prefix . '[required_message]', null, ['class' => 'form-control margin-b0', 'id' => $id_prefix . '[required_message]'] ) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-generic-field class="input-row">
                    <div class="table width-100 margin-b-gutter-full">
                        <div class="table-cell medium-2 vertical-top">
                            <label class="inline-block padding-y10">Extra Validation</label>
                            <div class="help-text margin-t-gutter-negative">e.g. 'numeric', 'min:value'<br><a href="https://laravel.com/docs/8.x/validation#available-validation-rules">See full list of rules here</a></div>
                        </div>
                        <div class="table-cell medium-10 vertical-top">
                            <div data-field-validation-wrapper class="border-1 colour-grey-light padding-10">
                                <div data-field-validation-header class="table width-100 font-size-14 border-b1 colour-grey-light padding-b5">
                                    <div class="colour-grey-dark table-cell vertical-middle medium-4">
                                        <span class="font-weight-600">Rule</span>
                                    </div>
                                    <div class="colour-grey-dark table-cell vertical-middle medium-8">
                                        <span class="font-weight-600">Error Message</span>
                                    </div>
                                </div>
                                <div data-field-validation-rules>
                                @if( !empty( $block->field[ $index ]['validation_rules'] ) )
                                @foreach( $block->field[ $index ]['validation_rules'] as $rule_ind => $rule )
                                    <div data-rule data-index="{{ $rule_ind }}" class="table width-100 padding-y5 border-b1 colour-grey-light">
                                        <div class="colour-grey-dark table-cell medium-4 padding-r5">
                                            {{ Form::text( $field_prefix . '[validation_rules][' . $rule_ind . '][rule]', null, ['class' => 'form-control margin-b0', 'placeholder' => 'Rule'] ) }}
                                        </div>
                                        <div class="colour-grey-dark table-cell medium-7">
                                            {{ Form::text( $field_prefix . '[validation_rules][' . $rule_ind . '][message]', null, ['class' => 'form-control margin-b0', 'placeholder' => 'Error Message'] ) }}
                                        </div>
                                        <div class="table-cell medium-1 text-right padding-r5">
                                            <a data-remove-field-validation-rule-btn href="javascript:void(0)" class="colour-error font-size-20 font-weight-600">&times;</a>
                                        </div>
                                    </div>
                                @endforeach
                                @endif
                                </div>
                                <div class="margin-t5">
                                    <button data-add-field-validation-rule-btn class="add-block radius colour-white padding-x50 padding-y15 line-height-1 margin-b0">
                                        <i class="fi-adm fi-adm-add inline-block vertical-middle margin-r-gutter"></i>
                                        <span class="inline-block vertical-middle">Add Rule</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <div data-field-options class="input-row none">
                    <div class="table width-100 margin-b-gutter-full">
                        <div class="table-cell medium-2 vertical-top">
                            <div class="margin-b-gutter">
                                <label for="{{ $id_prefix }}[field_options]" class="inline-block">Field Options</label>
                            </div>
                            <div class="help-text margin-t-gutter-negative">(One per line)</div>
                        </div>
                        <div class="table-cell medium-10 vertical-middle">
                            {{ Form::textarea( $field_prefix . '[field_options]', null, ['class' => 'form-control margin-b0', 'id' => $id_prefix . '[field_options]', 'rows' => 3, 'style' => 'min-height: 0'] ) }}
                        </div>
                    </div>
                </div>
                <div data-resource-options class="input-row none">
                    <div class="table width-100 margin-b-gutter-full">
                        <div class="table-cell medium-2 vertical-middle">
                            <label for="{{ $id_prefix }}[cms_resource]" class="inline-block">CMS Resource</label>
                        </div>
                        <div class="table-cell medium-10 vertical-middle">
                            {{ Form::select( $field_prefix . '[cms_resource]', $resource_types, null, ['class' => 'form-control margin-b0', 'id' => $id_prefix . '[cms_resource]'] ) }}
                        </div>
                    </div>
                </div>
                <div data-asset-options class="input-row none">
                    <div class="table width-100 margin-b-gutter-full">
                        <div class="table-cell medium-2 vertical-top">
                            <label class="inline-block">Sizes</label>
                        </div>
                        <div class="table-cell medium-10 vertical-top">
                            <div class="table width-100 margin-b-gutter">
                                <div class="table-cell medium-4 padding-r-gutter">Label</div>
                                <div class="table-cell medium-2 padding-r-gutter">Width</div>
                                <div class="table-cell medium-2 padding-r-gutter">Height</div>
                                <div class="table-cell medium-1"></div>
                            </div>
                            <div data-asset-size-wrapper>
                                @if( isset( $field ) && !empty( $field['asset_size'] ) )
                                    @foreach( $field['asset_size'] as $asset_index => $size )
                                        @include( 'PageBuilderWizardModule::partials.block-asset-size', ['field_prefix' => $field_prefix, 'index' => $index, 'asset_index' => $asset_index] )
                                    @endforeach
                                @else
                                    @include( 'PageBuilderWizardModule::partials.block-asset-size', ['field_prefix' => $field_prefix, 'index' => $index, 'asset_index' => 0] )
                                @endif
                                <div>
                                    <button data-add-asset-size-button class="add-block radius colour-white padding-x50 padding-y15 line-height-1">
                                        <i class="fi-adm fi-adm-add inline-block vertical-middle margin-r-gutter"></i>
                                        <span class="inline-block vertical-middle">Add Size</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if( !isset( $is_repeatable_field ) || !$is_repeatable_field )
                <div data-repeatable-options class="input-row none">
                    <div data-repeatable-field-container>
                        <?php
                            $repeatable_field_view_data = [
                                'field_prefix'          => $field_prefix . '[repeatable][0]',
                                'index'                 => 0,
                                'field_types'           => $repeatable_field_types,
                                'is_repeatable_field'   => true
                            ];
                        ?>
                        @if( !empty( $field['repeatable'] ) )
                            @foreach( $field['repeatable'] as $repeatable_index => $repeatable_field )
                                <?php
                                    $repeatable_field_view_data['field_prefix'] = $field_prefix . '[repeatable][' . $repeatable_index . ']';
                                    $repeatable_field_view_data['index'] = $repeatable_index;
                                    $repeatable_field_view_data['field'] = $repeatable_field;
                                ?>
                                @include( 'PageBuilderWizardModule::partials.block-field', $repeatable_field_view_data )
                            @endforeach
                        @else
                            @include( 'PageBuilderWizardModule::partials.block-field', $repeatable_field_view_data )
                        @endif
                    </div>
                    <button data-add-repeatable-field-button class="add-block radius colour-white padding-x50 padding-y15 line-height-1">
                        <i class="fi-adm fi-adm-add inline-block vertical-middle margin-r-gutter"></i>
                        <span class="inline-block vertical-middle">Add Repeater Field</span>
                    </button>
                </div>
                @endif
            </div>

        </div>

    </div>
</div>