<?php $tab_indent = ''; for( $i = 1; $i < $tabs; $i++ ){ $tab_indent .= "    "; } ?>
@foreach( $details as $key => $value )
@if( is_array( $value ) )
    {{ $tab_indent }}'{{ $key }}' => [
        {{ $tab_indent }}@include( 'PageBuilderWizardModule::partials.adm-block-template-nested-array', ['details' => $value, 'tabs' => $tabs+1] )
    {{ $tab_indent }}],
@else
    {{ $tab_indent }}'{{ $key }}' => {!! is_numeric( $value ) ? $value : (is_bool( $value ) ? ($value ? 'true' : 'false') : "'" . $value . "'") !!},
@endif
@endforeach