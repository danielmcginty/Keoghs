<?php echo '<?' . 'php' . PHP_EOL; ?>
namespace Modules\{{ $module_name }}\Adm\Models\PageBuilder;

use Adm\Models\PageBuilder\PageBuilderBase;
@if( !empty( $field_settings ) )
use Adm\Crud\LaraCrud;
use App\Models\Scopes\LanguageScope;
@foreach( $field_settings as $var => $settings )
use {{ $settings['cms_resource_type'] }};
@endforeach
@endif

/**
 * Class {{ $block_class_name }}
 * @package Modules\{{ $module_name }}\Adm\Models\PageBuilder
 *
 * Adm model defining functionality for the {{ $block_title }} block
 */
class {{ $block_class_name }} extends PageBuilderBase {

    /**
     * The name of the block, displayed in the block selection modal and in the collapsible element on forms.
     *
     * @var string
     */
    public $display_name = '{{ $block_title }}';

    /**
     * A description of the block, displayed in the block selection modal.
     *
     * @var string
     */
    public $description = '{{ $block_description }}';
@if( !$needs_heading_field )

    /**
    * Hide the grouped heading field
    *
    * @var bool
    */
    public $apply_block_heading_field = false;
@endif

@if( !empty( $fields ) )
    /**
     * The fields to be set by the user for this block.
     *
     * @var array
     */
    public $builder_fields = [
@foreach( $fields as $variable => $details )
@if( $variable == 'heading' && $details['type'] == 'heading' )
@if( isset( $details['group'] ) && !empty( $details['group'] ) )
        'heading' => [
            'group' => '{{ $details['group'] }}'
        ],
@else
        'heading' => [],
@endif
@else
        '{{ $variable }}' => [
@foreach( $details as $key => $value )
@if( $key == 'validation' )
            'validation' => [
@foreach( $value as $rule => $message )
                '{{ $rule }}' => '{{ $message }}',
@endforeach
            ],
@elseif( $key == 'required' )
            'required' => {{ $value }},
@elseif( $key == 'options' )
            'options' => [
@foreach( $value as $option )
                '{{ $option }}',
@endforeach
            ],
@else
            '{{ $key }}' => '{{ $value }}',
@endif
@endforeach
        ]
@endif
@endforeach
    ];

@endif
@if( !empty( $field_column_extras ) )
    /**
     * Any additional field settings for this block's fields, e.g. asset sizes, repeatable fields, etc.
     *
     * @var array
     */
    public $column_extras = [
        @include( 'PageBuilderWizardModule::partials.adm-block-template-nested-array', ['details' => $field_column_extras, 'tabs' => 2] )
    ];

@endif
@if( !empty( $field_settings ) )
    /**
     * Page Builder Callback.
     * This function is called before the block is rendered on a form.
     *
     * @param LaraCrud $crud
     */
    public function callbackBeforePrepare( LaraCrud $crud ){
@foreach( $field_settings as $variable => $setting )
        $options = [];
        foreach( {{ $setting['cms_short_class'] }}::withoutGlobalScope( LanguageScope::class )->where( 'language_id', $crud->language_id )->orderBy( 'title' )->get() as ${{ $setting['cms_short_class'] }} ){
            $options[ ${{ $setting['cms_short_class'] }}->{{ $setting['id'] }} ] = ${{ $setting['cms_short_class'] }}->title;
        }
        $this->builder_fields[ '{{ $variable }}' ]['options'] = $options;

@endforeach
    }

@endif
}