<?php echo '<?' . 'php' . PHP_EOL; ?>
namespace Modules\{{ $module_name }}\Theme\Models\PageBuilder;

use App\Models\PageBuilder\BaseBlockModel;
use Modules\{{ $module_name }}\Adm\Models\PageBuilder\{{ $block_class_name }} as AdmBlock;
@if( !empty( $field_settings ) )
@foreach( $field_settings as $var => $settings )
use {{ $settings['cms_resource_type'] }};
@if( $fields[ $var ]['type'] == 'orderable_multiple_select' )
use Illuminate\Database\Eloquent\Collection;
@endif
@if( $fields[ $var ]['type'] == 'link' )
use App\Helpers\LinkHelper;
@endif
@endforeach
@endif

/**
 * Class {{ $block_class_name }}
 * @package Modules\{{ $module_name }}\Theme\Models\PageBuilder
 *
 * Theme (Front-End) model defining functionality for the {{ $block_title }} block
 */
class {{ $block_class_name }} extends BaseBlockModel {

    /**
     * Get Value
     * Process the values stored against the block in the database, returning a sensible value for output.
     *
     * @param \App\Models\PageBuilderBlock $page_builder_block
     * @param string $field
     * @param mixed $original_value
     * @return mixed
     */
    public function getValue( $page_builder_block, $field, $original_value ){
        switch( $field ){
@if( !empty( $field_column_extras ) && array_key_exists( 'sizes', $field_column_extras ) )
@foreach( $field_column_extras as $image_field_name => $settings )
            case '{{ $image_field_name }}':
                $AdmBlock = new AdmBlock();
                return $this->getAssetFieldValue( $page_builder_block, $field, $original_value, $AdmBlock->column_extras[ $field ]['sizes'] );
@endforeach
@endif
@foreach( $fields as $variable => $settings )
@if( $fields[ $variable ]['type'] == 'link' )
            case '{{ $variable }}':
                if( empty( $original_value ) || !is_json( $original_value ) ){ return []; }

                $link = LinkHelper::parseLink( json_decode( $original_value, true ) );
                if( empty( $link['url'] ) ){ return []; }

                return $link;
@endif
@if( in_array( $fields[ $variable ]['type'], ['repeatable', 'stacked_repeatable'] ) )
            case '{{ $variable }}':
                if( empty( $original_value ) || !is_json( $original_value ) ){ return []; }

                $repeatable_rows = json_decode( $original_value, true );
                foreach( $repeatable_rows as $repeatable_index => $row ){
                    // You'll need to add your own processing here, as the wizard doesn't know what's what
                    // BUT, a couple of examples may be listed below depending on field types selected
@if( !empty( $field_column_extras[ $variable ] ) && !empty( $field_column_extras[ $variable ]['repeatable_fields'] ) )
@foreach( $field_column_extras[ $variable ]['repeatable_fields'] as $repeatable_field_var => $repeatable_settings )
@if( $repeatable_settings['type'] == 'link' )
                    $row[ '{{ $repeatable_field_var }}' ] = !empty( $row[ '{{ $repeatable_field_var }}' ] ) && is_json( $row[ '{{ $repeatable_field_var }}' ] ) ? json_decode( $row[ '{{ $repeatable_field_var }}' ], true ) : [];

@endif
@if( $repeatable_settings['type'] == 'asset' )
                    $AdmBlock = new AdmBlock();
                    $row[ '{{ $repeatable_field_var }}' ] = $this->getAssetFieldValue(
                        $page_builder_block,
                        '{{ $variable }}.' . $repeatable_index . '.{{ $repeatable_field_var }}',
                        $original_value,
                        $AdmBlock->column_extras['{{ $variable }}']['repeatable_fields']['{{ $repeatable_field_var }}']['sizes']
                    );

@endif
@endforeach
@endif
                }

                return $repeatable_rows;
@endif
@endforeach
@if( !empty( $field_settings ) )
@foreach( $field_settings as $variable => $setting )
@if( $fields[ $variable ]['type'] == 'select' )
            case '{{ $variable }}':
                return {{ $setting['cms_short_class'] }}::where( '{{ $setting['id'] }}', $original_value )->first();
@elseif( $fields[ $variable ]['type'] == 'orderable_multiple_select' )
            case '{{ $variable }}':
                $Records = new Collection();

                if( is_json( $original_value ) ){
                    $raw_records = json_decode( $original_value, true );

                    foreach( $raw_records as $raw_record ){
                        if( empty( $raw_record['value'] ) ){ continue; }

                        $Record = {{ $setting['cms_short_class'] }}::where( '{{ $setting['id'] }}', $raw_record['value'] )->first();
                        if( $Record ){
                            $Records->push( $Record );
                        }
                    }
                }

                return $Records;
@endif
@endforeach
@endif
            default:
                return parent::getValue( $page_builder_block, $field, $original_value );
        }
    }

}