@extends( 'layouts.adm' )

@section( 'page_title', 'Page Builder Wizard' )

@section( 'content' )

    {!! Form::model( $block ) !!}

        <div class="js-section-action-bar-container relative" style="height: 79px;">
            <div class="js-section-action-bar section-action-bar top-0 left-0 width-100">
                <div class="row padding-y-gutter-full">
                    <div class="column">
                        <div class="table width-100">
                            <div class="table-cell">
                                <h1 class="font-size-24 font-weight-600 margin-0">Page Builder Wizard</h1>
                            </div>
                            <div class="table-cell text-right">
                                <input type="submit" class="button round bordered colour-success margin-b0 margin-l-gutter" value="Create Block" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-area margin-b-gutter-full">
            <div class="row">

                <div class="column small-9">

                    <div class="edit-main-content section-container background colour-white radius margin-b-gutter-full">

                        <div class="tabs-content">
                            <div class="input-row">
                                <p class="font-size-14 colour-grey">This page generates the code for a new Page Builder Module
                                    All the relevant code is generated and the files are enabled for use and created as needed.<br>
                                    This builder does not overwrite any existing modules which could cause conflicts. It will only create the code if the file does not already exist.
                                </p>
                                <p class="font-size-14 colour-grey">Make sure the generate files and modules.php are added into git to track the changes e.g.
                                    <ul class="font-size-14 colour-grey">
                                        <li>/modules/NewModule/*</li>
                                        <li>/config/modules.php</li>
                                    </ul>
                                </p>
                            </div>
                            <div class="input-row">
                                <div><label for="block-title" class="inline-block margin-b-gutter">Block Title</label></div>
                                <div class="help-text margin-t-gutter-negative">The title of this block, used in the block selection modal</div>
                                {{ Form::text( 'block_title', null, ['class' => 'form-control', 'id' => 'block-title'] ) }}
                            </div>

                            <div class="input-row">
                                <div><label for="block-description" class="inline-block margin-b-gutter">Block Description</label></div>
                                <div class="help-text margin-t-gutter-negative">The description of this block, used in the block selection modal</div>
                                {{ Form::text( 'block_description', null, ['class' => 'form-control', 'id' => 'block-description'] ) }}
                            </div>

                            <div class="input-row">
                                <div><label class="inline-block margin-b-gutter">Fields</label></div>
                                <div class="help-text margin-t-gutter-negative">Manage this block's fields</div>

                                <div class="ui-sortable">
                                    <div class="block-field-builder page-builder">

                                        <div data-block-field-container>

                                            @if( !empty( $block->field ) )
                                                @foreach( $block->field as $index => $field )
                                                    @include( 'PageBuilderWizardModule::partials.block-field', ['index' => $index, 'field' => $field] )
                                                @endforeach
                                            @else
                                                @include( 'PageBuilderWizardModule::partials.block-field', ['index' => 0] )
                                            @endif

                                        </div>

                                        <button data-add-block-field-button class="add-block radius colour-white padding-x50 padding-y15 line-height-1">
                                            <i class="fi-adm fi-adm-add inline-block vertical-middle margin-r-gutter"></i>
                                            <span class="inline-block vertical-middle">Add Field</span>
                                        </button>

                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
        </div>

    {!! Form::close() !!}

@endsection

@push( 'footer_scripts' )
<script>
var $blockFieldContainer;
$(document).ready( function(){
    $blockFieldContainer = $('[data-block-field-container]');
    var $addBlockFieldBtn = $('[data-add-block-field-button]');
    var $cloneBlock = $blockFieldContainer.find('[data-block-field][data-is-repeatable="false"]').first();

    // Set field title
    $blockFieldContainer.on( 'change', '[data-block-label]', updateBlockFieldTitle );
    $blockFieldContainer.on( 'keyup', '[data-block-label]', updateBlockFieldTitle );
    $blockFieldContainer.on( 'change', '[data-block-field-type]', updateBlockFieldTitle );

    function updateBlockFieldTitle(){
        var $block = $(this).parents('[data-block-field]').first();
        var $titleField = $block.find('[data-block-label]').first();
        var $typeField = $block.find('[data-block-field-type]').first();

        if( $typeField.find('option:selected').val() == 'heading' ){
            $block.find('[data-block-field-title]').first().text( 'Block Heading - Customisable Text & Size' );
        } else {
            $block.find('[data-block-field-title]').first().text( $titleField.val() + ' - ' + $typeField.find('option:selected').text() );
        }
    }

    // Change field type - toggle hidden fields
    $blockFieldContainer.on( 'change', '[data-block-field-type]', function(){
        var $block = $(this).parents('[data-block-field]').first();
        var field_type = $(this).val();

        var $fieldOptions = $block.find('[data-field-options]');
        var $resourceOptions = $block.find('[data-resource-options]');
        var $assetOptions = $block.find('[data-asset-options]');
        var $repeatableOptions = $block.find('[data-repeatable-options]');

        switch( field_type ){
            case 'heading':
                $block.find('[data-generic-field]').addClass( 'none' );
                $fieldOptions.addClass( 'none' );
                $resourceOptions.addClass( 'none' );
                $assetOptions.addClass( 'none' );
                $repeatableOptions.addClass( 'none' );
                $block.find('[data-group-field]').removeClass( 'none' );
                break;
            case 'select':
            case 'checkbox':
            case 'radio':
            case 'orderable_multiple_select':
                $block.find('[data-generic-field]').removeClass( 'none' );
                $fieldOptions.removeClass( 'none' );
                $resourceOptions.addClass( 'none' );
                $assetOptions.addClass( 'none' );
                $repeatableOptions.addClass( 'none' );
                $block.find('[data-group-field]').removeClass( 'none' );
                break;
            case 'select_cms':
            case 'orderable_multiple_select_cms':
                $block.find('[data-generic-field]').removeClass( 'none' );
                $fieldOptions.addClass( 'none' );
                $resourceOptions.removeClass( 'none' );
                $assetOptions.addClass( 'none' );
                $repeatableOptions.addClass( 'none' );
                $block.find('[data-group-field]').removeClass( 'none' );
                break;
            case 'asset':
                $block.find('[data-generic-field]').removeClass( 'none' );
                $fieldOptions.addClass( 'none' );
                $resourceOptions.addClass( 'none' );
                $assetOptions.removeClass( 'none' );
                $repeatableOptions.addClass( 'none' );
                $block.find('[data-group-field]').removeClass( 'none' );
                break;
            case 'repeatable':
            case 'stacked_repeatable':
                $block.find('[data-generic-field]').removeClass( 'none' );
                $fieldOptions.addClass( 'none' );
                $resourceOptions.addClass( 'none' );
                $assetOptions.addClass( 'none' );
                $repeatableOptions.removeClass( 'none' );
                $block.find('[data-group-field]').removeClass( 'none' );
                break;
            default:
                $block.find('[data-generic-field]').removeClass( 'none' );
                $fieldOptions.addClass( 'none' );
                $resourceOptions.addClass( 'none' );
                $assetOptions.addClass( 'none' );
                $repeatableOptions.addClass( 'none' );
                $block.find('[data-group-field]').removeClass( 'none' );
                break;
        }
    });

    // Change required flag - toggle hidden field
    $blockFieldContainer.on( 'change', '[data-required-toggle]', function(){
        var $block = $(this).parents('[data-block-field]').first();
        var checked = $(this).is(':checked');

        var $toggleCell = $block.find('[data-required-toggle-cell]');

        if( checked ){
            $toggleCell.removeClass( 'none' );
        } else {
            $toggleCell.addClass( 'none' );
        }
    });

    // Add new field
    $addBlockFieldBtn.on( 'click', function( evt ){
        evt.preventDefault();
        evt.stopPropagation();

        var $firstBlockField = $blockFieldContainer.find('[data-block-field][data-is-repeatable="false"]').first();
        var $lastBlockField = $blockFieldContainer.find('[data-block-field][data-is-repeatable="false"]').last();

        // var $newBlockField = $firstBlockField.clone();
        var $newBlockField = $cloneBlock.clone();

        $newBlockField.find('input, textarea, select').each( function(){
            $(this).val('').trigger('change');
        });
        $newBlockField.find('input[type="checkbox"]').each( function(){
            $(this).removeAttr('checked');
        });
        $newBlockField.find('[data-asset-size]').each( function( ind, val ){
            if( ind > 0 ){
                $(this).remove();
            } else {
                $(this).find('input').val('');
            }
        });

        $newBlockField.find('[data-field-options]').addClass( 'none' );
        $newBlockField.find('[data-resource-options]').addClass( 'none' );
        $newBlockField.find('[data-asset-options]').addClass( 'none' );

        $lastBlockField.after( $newBlockField );

        setBlockFieldIndexes();
        setBlockFieldAssetSizeIndexes();
        initializeRepeatableDragAndDrop();

        return false;
    });

    // Toggle block field display
    $blockFieldContainer.on( 'click', '[data-block-field] [data-block-view-toggle]', function(){
        var $block = $(this).parents('[data-block-field]').first();
        $block.toggleClass( 'is-collapsed' );
    });

    // Remove block field
    $blockFieldContainer.on( 'click', '[data-block-field] [data-remove-block]', function(){
        if( $blockFieldContainer.find( '[data-block-field]' ).length <= 2 ){
            alert("Unable to delete - you need at least 1 field in the block");
        }else if( $blockFieldContainer.find( '[data-block-field]' ).length > 2 ){
            $(this).parents('[data-block-field]').first().remove();
            setBlockFieldIndexes();
        }
    });

    // Add asset size
    $blockFieldContainer.on( 'click', '[data-add-asset-size-button]', function( evt ){
        evt.preventDefault();
        evt.stopPropagation();

        var $block = $(this).parents('[data-block-field]').first();
        var $firstAssetSize = $block.find('[data-asset-size]').first();
        var $lastAssetSize = $block.find('[data-asset-size]').last();

        var $newAssetSize = $firstAssetSize.clone();
        $newAssetSize.find('input').each( function(){
            $(this).val('');
        });

        $lastAssetSize.after( $newAssetSize );

        setBlockFieldAssetSizeIndexes();

        return false;
    });

    // Remove asset size
    $blockFieldContainer.on( 'click', '[data-remove-asset-size]', function( evt ){
        evt.preventDefault();
        evt.stopPropagation();

        var $assetSizeWrapper = $(this).parents('[data-asset-size-wrapper]').first();

        if( $assetSizeWrapper.find('[data-asset-size]').length > 1 ){
            $(this).parents('[data-asset-size]').first().remove();
            setBlockFieldAssetSizeIndexes();
        }
    });

    // Add validation rule
    $blockFieldContainer.on( 'click', '[data-add-field-validation-rule-btn]', function( evt ){
        evt.preventDefault();
        var $btn = $(this);
        var $field = $btn.parents('[data-block-field]').first();
        var $wrapper = $btn.parents('[data-field-validation-wrapper]').first();
        var $rules = $wrapper.find('[data-field-validation-rules]');

        var field_index = $field.data( 'index' );
        var $last_rule = $rules.find('[data-rule]').last();
        var rule_index = $last_rule.length ? $last_rule.data('index') + 1 : 1;

        var new_row = '';
        new_row += '<div data-rule data-index="' + rule_index + '" class="table width-100 padding-y5 border-b1 colour-grey-light">';
        new_row += '    <div class="colour-grey-dark table-cell medium-4 padding-r5">';
        new_row += '        <input type="text" name="field[' + field_index + '][validation_rules][' + rule_index + '][rule]" placeholder="Rule" class="form-control margin-0" />';
        new_row += '    </div>';
        new_row += '    <div class="colour-grey-dark table-cell medium-7">';
        new_row += '        <input type="text" name="field[' + field_index + '][validation_rules][' + rule_index + '][message]" placeholder="Error Message" class="form-control margin-0" />';
        new_row += '    </div>';
        new_row += '    <div class="table-cell medium-1 text-right padding-r5">';
        new_row += '        <a data-remove-field-validation-rule-btn href="javascript:void(0)" class="colour-error font-size-20 font-weight-600">&times;</a>';
        new_row += '    </div>';
        new_row += '</div>';

        $rules.append( new_row );
    });

    // Remove validation rule
    $blockFieldContainer.on( 'click', '[data-remove-field-validation-rule-btn]', function( evt ){
        evt.preventDefault();
        $(this).parents('[data-rule]').first().remove();
    });

    // Add repeatable field
    $blockFieldContainer.on( 'click', '[data-add-repeatable-field-button]', function( evt ){
        evt.preventDefault();
        evt.stopPropagation();

        var $repeatableFieldWrap = $(this).prev('[data-repeatable-field-container]');
        var $firstRepeatableField = $repeatableFieldWrap.find('[data-block-field]').first();
        var $lastRepeatableField = $repeatableFieldWrap.find('[data-block-field]').last();

        var $newRepeatableField = $firstRepeatableField.clone();
        $newRepeatableField.find('input, textarea, select').each( function(){
            $(this).val('').trigger('change');
        });
        $newRepeatableField.find('input[type="checkbox"]').each( function(){
            $(this).removeAttr('checked');
        });
        $newRepeatableField.find('[data-asset-size]').each( function( ind, val ){
            if( ind > 0 ){
                $(this).remove();
            } else {
                $(this).find('input').val('');
            }
        });

        $newRepeatableField.find('[data-field-options]').addClass( 'none' );
        $newRepeatableField.find('[data-resource-options]').addClass( 'none' );
        $newRepeatableField.find('[data-asset-options]').addClass( 'none' );

        $lastRepeatableField.after( $newRepeatableField );

        setBlockFieldIndexes();
        setBlockFieldAssetSizeIndexes();

        return false;
    });

    // Remove repeatable field
    $blockFieldContainer.on( 'click', '[data-remove-repeatable-field]', function( evt ){
        var $repeatableFieldWrap = $(this).parents('[data-repeatable-field-container]').first();
        if( $repeatableFieldWrap.find( '[data-block-field]' ).length > 1 ){
            $(this).parents('[data-block-field]').first().remove();
            setBlockFieldIndexes();
        }
    });

    // Reindex fields & labels
    function setBlockFieldIndexes(){
        $blockFieldContainer.find('[data-block-field]').each( function( field_index, field ){
            var $field = $(this);

            if( $field.data('is-repeatable' ) ){
                field_index = $(this).parents('[data-block-field]').first().data( 'index' );
            }

            $field.data( 'index', field_index ).attr( 'data-index', field_index );

            var regex = new RegExp('(field\\[)(\\d*)(]\\[.*)');
            var asset_group_regex = new RegExp('(field\\.)(\\d*)(\\..*)');

            $field.find('input, textarea, select, label').each(function(index, item) {
                if (typeof( this.id ) != 'undefined') {
                    this.id = this.id.replace(regex, function( str, p1, p2, p3 ){
                        return p1 + field_index + p3;
                    });
                    this.id = this.id.replace(asset_group_regex, function( str, p1, p2, p3 ){
                        return p1 + field_index + p3;
                    });
                }

                if (typeof( this.htmlFor ) != 'undefined') {
                    this.htmlFor = this.htmlFor.replace(regex, function( str, p1, p2, p3 ){
                        return p1 + field_index + p3;
                    });
                    this.htmlFor = this.htmlFor.replace(asset_group_regex, function( str, p1, p2, p3 ){
                        return p1 + field_index + p3;
                    });
                }

                if (typeof( this.name ) != 'undefined') {
                    this.name = this.name.replace(regex, function( str, p1, p2, p3 ){
                        return p1 + field_index + p3;
                    });
                    this.name = this.name.replace(asset_group_regex, function( str, p1, p2, p3 ){
                        return p1 + field_index + p3;
                    });
                }
            });

            $field.find('[data-block-field][data-is-repeatable]').each( function( repeatable_field_index, repeatable_field ){
                var $repeatable_field = $(this);

                $repeatable_field.data( 'index', repeatable_field_index ).attr( 'data-index', repeatable_field_index );

                var repeatable_regex = new RegExp( '(repeatable]\\[)(\\d*)(]\\[.*)' );

                $repeatable_field.find('input, textarea, select').each( function(index,item){
                    if (typeof( this.name ) != 'undefined') {
                        this.name = this.name.replace(repeatable_regex, function( str, p1, p2, p3 ){
                            return p1 + repeatable_field_index + p3;
                        });
                    }
                });
            });
        });
    }

    // Reindex asset fields
    function setBlockFieldAssetSizeIndexes(){
        $blockFieldContainer.find('[data-block-field]').each( function( field_index, field ){
            var $field = $(this);
            $field.find('[data-asset-size]').each( function( size_index, field ){
                var $assetSize = $(this);

                var regex = new RegExp('(field\\[\\d*]\\[asset_size]\\[)(\\d*)(]\\[.*)');

                $assetSize.find('input').each( function( index, item){
                    if( typeof( this.name ) != 'undefined' ){
                        this.name = this.name.replace( regex, function( str, p1, p2, p3 ){
                            return p1 + size_index + p3;
                        });
                    }
                });
            });
        });
    }

    // Make the wizard fields sortable
    var widthHelper = function( e, ui ){
        ui.children().each( function(){
            $(this).width( $(this).width() );
        });
        return ui;
    };
    $blockFieldContainer.sortable({
        helper: widthHelper,
        cursor: 'grabbing',
        axis: 'y',
        handle: '[data-block-field-drag-handle]',
        items: '[data-block-field]',
        placeholder: 'sortable-placeholder',
        tolerance: 'pointer',
        revert: 150,
        opacity: 1,
        containment: 'parent',
        forcePlaceholderSize: true,
        start: function(){
            $blockFieldContainer.addClass( 'is-sorting' );
        },
        stop: function(){
            $blockFieldContainer.removeClass( 'is-sorting' );
        },
        update: function(){
            setBlockFieldIndexes();
            setBlockFieldAssetSizeIndexes();
        }
    });

    function initializeRepeatableDragAndDrop(){
        try {
            $('[data-repeatable-field-container]').sortable( 'destroy' );
        } catch( error ){}

        $('[data-repeatable-field-container]').sortable({
            helper: widthHelper,
            cursor: 'grabbing',
            axis: 'y',
            handle: '[data-repeatable-field-drag-handle]',
            items: '[data-block-field]',
            placeholder: 'sortable-placeholder',
            tolerance: 'pointer',
            revert: 150,
            opacity: 1,
            containment: 'parent',
            forcePlaceholderSize: true,
            start: function(){
                $blockFieldContainer.addClass( 'is-sorting' );
            },
            stop: function(){
                $blockFieldContainer.removeClass( 'is-sorting' );
            },
            update: function(){
                setBlockFieldIndexes();
                setBlockFieldAssetSizeIndexes();
            }
        });
    }
    initializeRepeatableDragAndDrop();
});
$(window).load( function(){
    $blockFieldContainer.find( '[data-block-field-type]' ).trigger( 'change' );
});
</script>
@endpush