<?php
namespace Modules\PageBuilderWizard\Adm\Controllers;

use Adm\Controllers\AdmController;
use Illuminate\Support\Str;

class ModuleController extends AdmController {

    public function page_builder_wizard(){
        $data = [
            'field_types' => [
                'string'                        => 'Single Line Text',
                'textarea'                      => 'Multi Line Text',
                'heading'                       => 'Block Heading (Text & Size)',
                'text'                          => 'WYSIWYG Editor',
                'number'                        => 'Number Input',
                'select'                        => 'Select Dropdown',
                'select_cms'                    => 'Select Dropdown (CMS)',
                'checkbox'                      => 'Checkboxes',
                'radio'                         => 'Radio Buttons',
                'orderable_multiple_select'     => 'Orderable Multiple Select',
                'orderable_multiple_select_cms' => 'Orderable Multiple Select (CMS)',
                'boolean'                       => 'Yes / No Toggle',
                'asset'                         => 'Image Picker',
                'file'                          => 'File Picker',
                'link'                          => 'Link Picker',
                'date'                          => 'Date Picker',
                'time'                          => 'Time Picker',
                'datetime'                      => 'Date & Time Picker',
                'repeatable'                    => 'Repeatable',
                'stacked_repeatable'            => 'Stacked Repeatable'
            ],
            'repeatable_field_types'    => [
                'string'                        => 'Single Line Text',
                'textarea'                      => 'Multi Line Text',
                'text'                          => 'WYSIWYG Editor',
                'number'                        => 'Number Input',
                'select'                        => 'Select Dropdown',
                'checkbox'                      => 'Checkboxes',
                'radio'                         => 'Radio Buttons',
                'boolean'                       => 'Yes / No Toggle',
                'asset'                         => 'Image Picker',
                'file'                          => 'File Picker',
                'link'                          => 'Link Picker',
                'date'                          => 'Date Picker',
                'time'                          => 'Time Picker',
                'datetime'                      => 'Date & Time Picker',
            ],
            'resource_types' => [
                'Modules\Page\Theme\Models\Page'                => 'Pages',
                'Modules\CaseStudies\Theme\Models\CaseStudy'    => 'Case Studies',
                'Modules\News\Theme\Models\NewsArticle'         => 'News',
                'Modules\Faqs\Theme\Models\Faq'                 => 'FAQs',
                'Modules\Offices\Theme\Models\Office'           => 'Offices',
                'Modules\TeamMembers\Theme\Models\TeamMember'   => 'Team Members',
                'Modules\Testimonials\Theme\Models\Testimonial' => 'Testimonials'
            ]
        ];

        view()->addNamespace( 'PageBuilderWizardModule', base_path( '/modules/PageBuilderWizard/Adm/views/' ) );

        $block = new \StdClass();
        $block->block_title = request()->input( 'block_title', '' );
        $block->block_description = request()->input( 'block_description', '' );
        $block->field = request()->input( 'field', [] );

        if( request()->session()->has( 'pbbwizard_block_title' ) ){
            $block->block_title = request()->session()->get( 'pbbwizard_block_title' );
            $block->block_description = request()->session()->get( 'pbbwizard_block_description' );
            $block->field = json_decode( request()->session()->get( 'pbbwizard_block_field' ), true );

            request()->session()->remove( 'pbbwizard_block_title' );
            request()->session()->remove( 'pbbwizard_block_description' );
            request()->session()->remove( 'pbbwizard_block_field' );
        }

        $data['block'] = $block;

        if( request()->isMethod( 'post' ) ){
            // Store block data in session
            request()->session()->put( 'pbbwizard_block_title', $block->block_title );
            request()->session()->put( 'pbbwizard_block_description', $block->block_description );
            request()->session()->put( 'pbbwizard_block_field', json_encode( $block->field ) );

            // Process data
            $this->_processPostedBlockFields( $data );
            $resultsMessage = $this->_createBlock( $data );
            $data['resultsMessage'] = $resultsMessage;
            $this->crud->setView( 'PageBuilderWizardModule::result', $data );
        } else {
            $this->crud->setView( 'PageBuilderWizardModule::wizard', $data );
        }
    }

    private function _processPostedBlockFields( &$data ){
        $block_title = request()->input( 'block_title' );
        $block_description = request()->input( 'block_description' );
        $block_fields = request()->input( 'field', array() );

        $block_class_name = Str::studly( Str::slug( $block_title, '_' ) );
        $block_reference_name = Str::slug( $block_title, '_' );

        $types_with_options = [
            'select',
            'checkbox',
            'radio',
            'orderable_multiple_select'
        ];
        $types_needing_callback = [
            'select_cms',
            'orderable_multiple_select_cms'
        ];
        $callback_type_map = [
            'select_cms'                    => 'select',
            'orderable_multiple_select_cms' => 'orderable_multiple_select'
        ];

        $needs_heading_field = false;
        $fields = [];
        $field_column_extras = [];
        $field_settings = [];
        foreach( $block_fields as $index => $field ){

            if( $field['field_type'] == 'heading' ){
                $needs_heading_field = true;
                $fields['heading'] = [
                    'type'  => 'heading'
                ];
                if( !empty( $field['group'] ) ){
                    $fields['heading']['group'] = $field['group'];
                }
                continue;
            }

            $label = $field['label'];
            $variable = \Illuminate\Support\Str::slug( $label, '_' );

            $fields[ $variable ] = [
                'display_name'  => $label,
                'help_text'     => $field['help_text'],
                'type'          => $field['field_type'],
                'required'      => isset( $field['required'] ) && $field['required'] ? 'true' : 'false'
            ];

            if( isset( $field['group'] ) && !empty( $field['group'] ) ){
                $fields[ $variable ]['group'] = $field['group'];
            }

            if( $fields[ $variable ]['required'] == 'true' && isset( $field['required_message'] ) ){
                $fields[ $variable ]['validation'] = [
                    'required' => $field['required_message']
                ];
            }

            if( !empty( $field['validation_rules'] ) ){
                if( !isset( $fields[ $variable ]['validation'] ) ){
                    $fields[ $variable ]['validation'] = [];
                }
                foreach( $field['validation_rules'] as $rule ){
                    if( empty( $rule['rule'] ) || empty( $rule['message'] ) ){ continue; }
                    $fields[ $variable ]['validation'][ $rule['rule'] ] = $rule['message'];
                }
            }

            if( in_array( $fields[ $variable ]['type'], $types_with_options ) && !empty( $field['field_options'] ) ){
                $options = $this->_processFieldOptions( $field['field_options'] );
                if( !empty( $options ) ){
                    $fields[ $variable ]['options'] = $options;
                }
            }

            if( in_array( $fields[ $variable ]['type'], $types_needing_callback ) && !empty( $field['cms_resource'] ) ){
                $class_ns_parts = explode( '\\', $field['cms_resource'] );

                /** @var \Illuminate\Database\Eloquent\Model $Class */
                $Class = new $field['cms_resource']();
                $pk = $Class->getKeyName();
                if( is_array( $pk ) ){
                    $pk_field = false;
                    foreach( $pk as $ind => $tmp_pk ){
                        if( $tmp_pk == 'language_id' || $tmp_pk == 'version' ){ continue; }
                        $pk_field = $tmp_pk;
                    }

                    if( $pk_field ){
                        $pk = $pk_field;
                    }
                }

                $field_settings[ $variable ] = [
                    'id'                => $pk,
                    'cms_resource_type' => $field['cms_resource'],
                    'cms_short_class'   => end( $class_ns_parts )
                ];

                if( isset( $callback_type_map[ $fields[ $variable ]['type'] ] ) ){
                    $fields[ $variable ]['type'] = $callback_type_map[ $fields[ $variable ]['type'] ];
                }
            }

            if( $fields[ $variable ]['type'] == 'asset' && !empty( $field['asset_size'] ) ){
                $asset_sizes = array_filter( $field['asset_size'] );
                if( !empty( $asset_sizes ) ){

                    $parsed_sizes = $this->_processAssetSizes( $asset_sizes );

                    $field_column_extras[ $variable ] = [
                        'multi_size'    => 'true',
                        'sizes'         => $parsed_sizes
                    ];
                }
            }

            if( in_array( $fields[ $variable ]['type'], ['repeatable', 'stacked_repeatable'] ) && !empty( $field['repeatable'] ) ){
                $repeatables = [];
                foreach( $field['repeatable'] as $repeatable_field ){
                    $repeatable_field_variable = \Illuminate\Support\Str::slug( $repeatable_field['label'], '_' );

                    $this_repeatable = [
                        'label' => $repeatable_field['label'],
                        'type'  => $repeatable_field['field_type']
                    ];
                    if( !empty( $repeatable_field['help_text'] ) ){
                        $this_repeatable['help'] = $repeatable_field['help_text'];
                    }

                    if( in_array( $repeatable_field['field_type'], $types_with_options ) && !empty( $repeatable_field['field_options'] ) ){
                        $options = $this->_processFieldOptions( $repeatable_field['field_options'] );
                        if( !empty( $options ) ){
                            $this_repeatable['options'] = $options;
                        }
                    }

                    if( $repeatable_field['field_type'] == 'asset' ){
                        $asset_sizes = array_filter( $repeatable_field['asset_size'] );
                        if( !empty( $asset_sizes ) ){
                            $parsed_sizes = $this->_processAssetSizes( $asset_sizes );

                            $this_repeatable['multi_size'] = true;
                            $this_repeatable['sizes'] = $parsed_sizes;
                        }
                    }

                    $repeatables[ $repeatable_field_variable ] = $this_repeatable;
                }

                $field_column_extras[ $variable ] = [
                    'repeatable_fields' => $repeatables
                ];
            }
        }

        $data['block_title'] = $block_title;
        $data['block_class_name'] = $block_class_name;
        $data['module_name'] = $block_class_name . 'Block';
        $data['block_reference_name'] = $block_reference_name;

        $template_data = [
            'block_class_name'      => $block_class_name,
            'block_title'           => $block_title,
            'block_description'     => $block_description,
            'fields'                => $fields,
            'field_settings'        => $field_settings,
            'field_column_extras'   => $field_column_extras,
            'needs_heading_field'   => $needs_heading_field,
            'module_name'           => $data['module_name']
        ];
        $php_file_string = view()->make( 'PageBuilderWizardModule::adm-block-template', $template_data )->render();

        $data['php_file_string'] = $php_file_string;

        // Determine whether the Theme version of the model is required
        $data['theme_php_file_string'] = false;
        if( !empty( $field_settings ) || !empty( $field_column_extras ) ){
            $template_data = [
                'block_class_name'      => $block_class_name,
                'block_title'           => $block_title,
                'fields'                => $fields,
                'field_settings'        => $field_settings,
                'field_column_extras'   => $field_column_extras,
                'module_name'           => $data['module_name']
            ];
            $theme_php_file_string = view()->make( 'PageBuilderWizardModule::theme-block-template', $template_data )->render();

            $data['theme_php_file_string'] = $theme_php_file_string;
        }
    }

    private function _createBlock( $data ){
        $resultsMessage = "";
        if( !is_dir( base_path( 'modules/'.$data['module_name'].'/Adm/Models/PageBuilder/' ))){
            mkdir( base_path( 'modules/'.$data['module_name'].'/Adm/Models/PageBuilder/' ), 0755, true );
            $resultsMessage .= 'Created: '.'modules/'.$data['module_name']."/Adm/Models/PageBuilder/\n";
        }
        if( !file_exists( base_path( 'modules/'.$data['module_name'].'/Adm/Models/PageBuilder/'.$data['block_class_name'].'.php' ))){
            $handle = fopen(base_path( 'modules/'.$data['module_name'].'/Adm/Models/PageBuilder/'.$data['block_class_name'].'.php'), 'w');
            fwrite($handle, $data['php_file_string']);
            fclose($handle);
            $resultsMessage .= 'Created: '.'modules/'.$data['module_name'].'/Adm/Models/PageBuilder/'.$data['block_class_name'].".php\n";
        }

        if( $data['theme_php_file_string'] ){
            if( !is_dir( base_path( 'modules/'.$data['module_name'].'/Theme/Models/PageBuilder/' ))){
                mkdir( base_path( 'modules/'.$data['module_name'].'/Theme/Models/PageBuilder/' ), 0755, true );
                $resultsMessage .= 'Created: '.'modules/'.$data['module_name']."/Theme/Models/PageBuilder/\n";
            }
            if( !file_exists( base_path( 'modules/'.$data['module_name'].'/Theme/Models/PageBuilder/'.$data['block_class_name'].'.php' ))){
                $handle = fopen(base_path( 'modules/'.$data['module_name'].'/Theme/Models/PageBuilder/'.$data['block_class_name'].'.php'), 'w');
                fwrite($handle, $data['theme_php_file_string']);
                fclose($handle);
                $resultsMessage .= 'Created: '.'modules/'.$data['module_name'].'/Theme/Models/PageBuilder/'.$data['block_class_name'].".php\n";
            }
        }

        if(!$resultsMessage){
            return "Could not create the module as it already exists.";
        }

        header("Content-Type: text/plain");
        $handle = fopen(base_path('config/modules.php'), "r");
        $oldContent = fread($handle, filesize(base_path('config/modules.php')));
        fclose($handle);

        if(!stristr($oldContent, $data['module_name'])){
            list($a, $b) = explode("'available' => [", $oldContent);
            $bNew = "\n                '".$data['module_name']."',\n                ".trim($b);

            $newContent = implode("'available' => [", [$a, $bNew]);

            $handle = fopen(base_path('config/modules.php'), "w");
            fwrite($handle, $newContent);
            fclose($handle);

            $resultsMessage .= 'Module enabled in: '."config/modules.php\n";
        }
        $resultsMessage .= "\nYour module is ready to use! Make sure you add the changes to Git.";
        return $resultsMessage;
    }

    private function _processFieldOptions( $field_options ){
        $options = [];

        $raw_options = array_filter( explode( "\r\n", $field_options ) );
        foreach( $raw_options as $raw_option ){
            if( stristr( $raw_option, ':' ) !== false ){
                $option_parts = explode( ':', $raw_option );
                $options[ trim( $option_parts[0] ) ] = trim( $option_parts[1] );
            } else {
                $options[] = $raw_option;
            }
        }

        return $options;
    }

    private function _processAssetSizes( $asset_sizes ){
        $parsed_sizes = [];
        foreach( $asset_sizes as $ind => $size ){
            $size['primary'] = $ind == 0;
            $size_key = \Illuminate\Support\Str::slug( $size['label'], '_' );

            $size['width'] = (int)$size['width'];
            $size['height'] = (int)$size['height'];

            $parsed_sizes[ $size_key ] = $size;
        }

        return $parsed_sizes;
    }

}
