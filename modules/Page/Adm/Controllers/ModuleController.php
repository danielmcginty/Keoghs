<?php
namespace Modules\Page\Adm\Controllers;

use Adm\Controllers\AdmController;
use Modules\Page\Config\PageConfig;
use Modules\Page\Theme\Models\Page;

class ModuleController extends AdmController {
    /**
     * Pages
     *
     * CRUD function for the pages area of the CMS
     */
    public function pages(){
        $this->crud->setTable('pages');
        $this->crud->displayColumn('title');

        $this->crud->setPathPrefix('pages', 'parent', 'page_id');

        $this->crud->addColumn('path', '', 'side');

        $this->crud->systemPageEnabled = true;

        $this->crud->addColumn('page_builder', 'image');
        $this->crud->changeType('page_builder', 'page_builder');

        $this->crud->setRelation('parent', 'pages', 'title', 'page_id');
        $this->crud->setRelationExcludes( 'parent', [1] );

        $this->crud->enablePageBuilder();

        $this->crud->changeType('single_image', 'asset');
        $this->crud->changeType('description', 'textarea');

        $this->crud->columns( 'title', 'path', 'parent', 'status' );

        $this->crud->columnGroup( 'main', array( 'title', 'path', 'parent', 'h1_title', 'description', 'image', 'intro_content_heading', 'intro_content', 'page_builder' ) );
        $this->crud->columnGroup( 'side', array( 'status' ) );

        $this->crud->excludeFromEdit( ['sort_order'] );

        $pages_with_intro = [
            ['journey', 'index'],   // Working at Keoghs
            ['award', 'index'],     // Awards
        ];
        $intro_page_ids = [];
        foreach ($pages_with_intro as $sys_pg_config) {
            $SysPg = \App\Models\Url::getSystemPage($sys_pg_config[0], $sys_pg_config[1]);
            if ($SysPg) {
                $intro_page_ids[] = $SysPg->page_id;
            }
        }

        // If we're accessing the current home page
        if( isset( $this->crud->parameters['id'] ) && $this->crud->parameters['id'] == 1 ){
            // Pass that detail down to the form
            $this->crud->extra_view_data['is_home_page'] = true;

            // And stop the page's parent from being changed
            $this->crud->excludeFromEdit( ['parent', 'status', 'image', 'description', 'intro_content_heading', 'intro_content'] );
        }
        // Else, if we're accessing the search page
        elseif( isset( $this->crud->parameters['id'] ) && $this->crud->parameters['id'] == 7 ){
            $this->crud->excludeFromEdit( ['image', 'intro_content_heading', 'intro_content'] );
            $this->crud->disableMainContentPageBuilderBlock();
        }
        // Else, if we're accessing the sitemap page
        elseif( isset( $this->crud->parameters['id'] ) && $this->crud->parameters['id'] == 3 ){
            $this->crud->excludeFromEdit( ['image', 'intro_content_heading', 'intro_content'] );
            $this->crud->disableMainContentPageBuilderBlock();
        }
        // Else, if we're accessing a page that should have introductory content
        elseif (isset($this->crud->parameters['id']) && in_array($this->crud->parameters['id'], $intro_page_ids)) {
            
        }
        // Else
        else {
            $this->crud->excludeFromEdit(['intro_content_heading', 'intro_content']);
        }
    }

    /**
     * CRUD Functionality for the Home Page in the CMS
     */
    public function home_page(){
        $this->crud->setTable('pages');
        $this->crud->setSubject('Home Page');
        $this->crud->multiLingualSingle( 1 );

        $this->crud->displayColumn('title');

        $this->crud->addColumn('path', '', 'side');
        $this->crud->systemPageEnabled = true;

        $this->crud->addColumn('page_builder', 'image');
        $this->crud->changeType('page_builder', 'page_builder');

        $this->crud->enablePageBuilder();

        $this->crud->columns( 'title', 'path', 'parent', 'status' );

        $this->crud->columnGroup( 'main', array( 'title', 'h1_title', 'page_builder' ) );
        $this->crud->columnGroup( 'side', array( 'status' ) );

        $this->crud->excludeFromEdit( ['sort_order', 'description'] );

        // Pass that detail down to the form
        $this->crud->extra_view_data['is_home_page'] = true;

        // And stop the page's parent from being changed
        $this->crud->excludeFromEdit( ['parent', 'status', 'image', 'description', 'intro_content_heading', 'intro_content'] );
    }

    /**
     * Get Default Labels
     * Called by LaraCrud getDefaultsCallback to populate the field labels based on the Config file
     *
     * @return array
     */
    public function getDefaultLabels(){
        return PageConfig::$labels;
    }

    /**
     * Get Default Help
     * Called by LaraCrud getDefaultsCallback to populate the field help text based on the Config file
     * @return array
     */
    public function getDefaultHelp(){
        return PageConfig::$help;
    }

    /**
     * Get Asset Config
     * Called by AssetType _getAssetFieldConfig to determine whether a field is an asset group, or just a simple asset
     * based on the Config file
     *
     * @param $field_name
     * @return array
     */
    public function getAssetConfig( $field_name ){
        return isset( PageConfig::$image_sizes[ $field_name ] ) ? PageConfig::$image_sizes[ $field_name ] : [];
    }

    /**
     * Callback function performed after a 'page' is saved.
     * Used to ensure that the Home Page is always published.
     *
     * @param $item_primary
     * @param $inputs
     * @param $item
     */
    public function savedPagesItem( $item_primary, $inputs, $item ){
        if( $item->page_id == 1 ){
            $item->setPrimaryKey( ['page_id', 'language_id'] );

            $item->status = 1;
            $item->save();
        }
    }

    public function savedHomePageItem( $item_primary, $inputs, $item ){
        $item->setPrimaryKey( ['page_id', 'language_id'] );
        $item->status = 1;
        $item->save();
    }
}