<?php
namespace Modules\Page\Theme\Models;

use App\Models\CommonModel;
use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\HasCompositePrimaryKey;

use App\Models\Scopes\LanguageScope;
use App\Models\Scopes\PublishedScope;

use Modules\Page\Config\PageConfig;
use App\Helpers\AssetHelper;

class Page extends CommonModel {
    // As we've got a deleted_at flag, a composite primary key, and want to cache DB calls, include the relevant traits
    use SoftDeletes, HasCompositePrimaryKey, HasQueryBuilderCache;

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'pages';

    /**
     * The primary keys in the table
     * @var array
     */
    protected $primaryKey = ['page_id', 'language_id'];

    protected $appends      = ['meta_title', 'meta_description', 'no_index', 'system_page'];

    /**
     * When 'booting' this model, we only want to include pages that are published and in the correct language,
     * so add those as global scopes
     */
    protected static function boot(){
        parent::boot();

        static::addGlobalScope(new LanguageScope);
        static::addGlobalScope(new PublishedScope);
    }

    /**
     * Eloquent Relationship.
     * Returns the current page's parent as a Page object, ready for use.
     *
     * @return mixed
     */
    public function parent_page(){
        return $this->belongsTo('\\Modules\\Page\\Theme\\Models\\Page', 'parent', 'page_id')->with('parent_page');
    }

    /**
     * Custom Accessor Function.
     * Returns the images assigned to the page in an easy to use array.
     *
     * @return array
     */
    public function getImageAttribute(){
        $AssetHelper = new AssetHelper;
        return $AssetHelper->getAssets( $this->table, $this->page_id, $this->language_id, $this->version, 'image', PageConfig::$image_sizes['image'] );
    }

    public function getSystemPageAttribute(){
        return $this->url_object ? $this->url_object->route : '';
    }
}