<?php

namespace Modules\Page\Theme\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Url;
use Illuminate\Http\Request;
use Modules\Locations\Theme\Models\Location;
use Modules\Page\Theme\Models\Page;

class PageController extends Controller
{

    /**
     * The default page loader
     * Takes a Page ID and a HTTP Request
     *
     * @param $page_id
     * @param  Request  $request
     * @return bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($page_id, Request $request)
    {
        $this_page = Page::where('page_id', $page_id)->first();

        if ($this_page) {

            $home = Url::getSystemPage('home', 'index');
            if ($home) {
                //$home->getAttributes()['title'] gets the original title attribute since it's modified in the model
                $this->addBreadcrumb(route('theme', ['url' => $home->url]), $home->getAttributes()['title']);
            }

            foreach ($this->getBreadcrumbs($this_page, 'title', 'parent_page') as $breadcrumb) {
                $this->addBreadcrumb(route('theme', ['url' => $breadcrumb['url']]), $breadcrumb['label']);
            }

            $this->pageTitle = $this_page->title;
            $this->this_page = $this_page;

            $this->view = 'pages.page';

        }
    }

    /**
     * Dedicated System Page Loader for the Privacy Policy Page.
     * As a privacy policy is required by practically all websites, this is set as a system page
     * so that general users can't delete it
     *
     * @param  Request  $request
     */
    public function privacy_policy(Request $request)
    {
        $this_page = Url::getSystemPage('page', 'privacy_policy');

        if ($this_page) {
            $this->index($this_page->page_id, $request);
        }
    }

    public function latest_jobs()
    {
        $this_page = Url::getSystemPage('page', 'latest_jobs');

        if ($this_page) {

            $home = Url::getSystemPage('home', 'index');

            if ($home) {
                $this->addBreadcrumb(route('theme', ['url' => $home->url]), $home->getAttributes()['title']);
            }

            foreach ($this->getBreadcrumbs($this_page, 'title', 'parent_page') as $breadcrumb) {
                $this->addBreadcrumb(route('theme', ['url' => $breadcrumb['url']]), $breadcrumb['label']);
            }

            $this->pageTitle = $this_page->title;
            $this->this_page = $this_page;

            $this->view = 'pages.latest-jobs';

        }
    }

    public function contact()
    {
        $this_page = Url::getSystemPage('page', 'contact');

        if ($this_page) {
            $home = Url::getSystemPage('home', 'index');

            if ($home) {
                $this->addBreadcrumb(route('theme', ['url' => $home->url]), $home->getAttributes()['title']);
            }

            foreach ($this->getBreadcrumbs($this_page, 'title', 'parent_page') as $breadcrumb) {
                $this->addBreadcrumb(route('theme', ['url' => $breadcrumb['url']]), $breadcrumb['label']);
            }

            $this->pageTitle = $this_page->title;
            $this->this_page = $this_page;

            $Locations = new \Illuminate\Database\Eloquent\Collection();
            $Bolton = Location::where('location_id', 1)->first();
            if ($Bolton) { $Locations->push($Bolton); }
            foreach (Location::query()->where('location_id', '<>', 1)->orderBy('title')->get() as $Loc) {
                $Locations->push($Loc);
            }

            $this->locations = $Locations;
            $this->initial_location = $this->locations->first();

            $this->blue_hero = true;
            $this->view = 'pages.contact';
        }
    }
}