<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AmendPagesTableForContentVersioning extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'pages', function( Blueprint $table ){
            $table->integer( 'version' )->after( 'language_id' )->default( 1 );
            $table->dropPrimary( 'pages_page_id_language_id_primary' );
            $table->primary( ['page_id', 'language_id', 'version'], 'pages_primary_key' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'pages', function( Blueprint $table ){
            $table->dropColumn( 'version' );
            $table->dropPrimary( 'pages_primary_key' );
            $table->primary( ['page_id', 'language_id'] );
        });
    }
}
