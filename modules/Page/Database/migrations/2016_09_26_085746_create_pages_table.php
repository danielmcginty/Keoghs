<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->integer('page_id');
            $table->integer('language_id');
            $table->integer('parent')->default(0);
            $table->string('title');
            $table->string('h1_title')->nullable();
            $table->text('image')->nullable();
            $table->text('content');
            $table->integer('sort_order')->default(0);
            $table->boolean('status')->default(0);
            $table->primary(['page_id', 'language_id']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pages');
    }
}
