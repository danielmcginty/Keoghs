<?php
namespace Modules\Page\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DefaultPagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // Home Page
        \Modules\Page\Theme\Models\Page::create([
            'page_id' => 1,
            'language_id' => 1,
            'version' => 1,
            'parent' => 0,
            'title' => 'Home',
            'h1_title' => '',
            'image' => 0,
            'sort_order' => 0,
            'status' => 1
        ]);
        \App\Models\Url::create([
            'url' => '',
            'table_name' => 'pages',
            'table_key' => 1,
            'language_id' => 1,
            'path_prefix' => null,
            'manual' => 0,
            'no_index_meta' => 0,
            'meta_title' => 'Home',
            'meta_description' => '',
            'system_page' => 1,
            'route' => 'controller=home&method=index'
        ]);

        // 404 Page
        \Modules\Page\Theme\Models\Page::create([
            'page_id' => 2,
            'language_id' => 1,
            'version' => 1,
            'parent' => 0,
            'title' => 'Page Not Found',
            'h1_title' => '',
            'image' => 0,
            'sort_order' => 0,
            'status' => 1
        ]);
        \App\Models\Url::create([
            'url' => 'page-not-found',
            'table_name' => 'pages',
            'table_key' => 2,
            'language_id' => 1,
            'path_prefix' => null,
            'manual' => 0,
            'no_index_meta' => 0,
            'meta_title' => 'Error 404',
            'meta_description' => '',
            'system_page' => 1,
            'route' => 'controller=controller&method=error404'
        ]);

        // Sitemap Page
        \Modules\Page\Theme\Models\Page::create([
            'page_id' => 3,
            'language_id' => 1,
            'version' => 1,
            'parent' => 0,
            'title' => 'Sitemap',
            'h1_title' => '',
            'image' => 0,
            'sort_order' => 0,
            'status' => 1
        ]);
        \App\Models\Url::create([
            'url' => 'sitemap',
            'table_name' => 'pages',
            'table_key' => 3,
            'language_id' => 1,
            'path_prefix' => null,
            'manual' => 0,
            'no_index_meta' => 0,
            'meta_title' => 'Sitemap',
            'meta_description' => '',
            'system_page' => 1,
            'route' => 'controller=sitemap&method=index'
        ]);

        // Privacy Policy Page
        \Modules\Page\Theme\Models\Page::create([
            'page_id' => 4,
            'language_id' => 1,
            'version' => 1,
            'parent' => 0,
            'title' => 'Privacy Policy',
            'h1_title' => '',
            'image' => 0,
            'sort_order' => 0,
            'status' => 1
        ]);
        \App\Models\Url::create([
            'url' => 'privacy-policy',
            'table_name' => 'pages',
            'table_key' => 4,
            'language_id' => 1,
            'path_prefix' => null,
            'manual' => 0,
            'no_index_meta' => 0,
            'meta_title' => 'Privacy Policy',
            'meta_description' => '',
            'system_page' => 1,
            'route' => 'controller=page&method=privacy_policy'
        ]);

        Model::reguard();
    }
}
