<?php
namespace Modules\Page\Config;

class PageConfig {
    /**
     * Stores input labels
     * @var array
     */
    public static $labels = [
        'parent'        => 'Parent Page',
        'content'       => 'Page Body',
        'status'        => 'Published',
        'h1_title'      => 'H1/Banner Title',
        'image'         => 'Banner Image',
        'description'   => 'Banner Subtitle',
        'intro_content_heading' => 'Intro Content - Heading',
        'intro_content'         => 'Intro Content',
    ];

    /**
     * Stores input help text
     * @var array
     */
    public static $help = [
        'title'         => 'The title of this page. Used in links and meta data.',
        'path'          => 'Only use hyphens, lowercase letters and numbers (no spaces).',
        'h1_title'      => 'The page title is used if empty.',
        'content'       => 'The page\'s main content.',
        'image'         => 'The main page banner image.',
        'status'        => 'Toggle whether this page is published (can be accessed).',
        'parent'        => 'Parent page used in this page\'s url e.g. /parent/page',
        'sort_order'    => 'The default order for the page.',
        'description'   => 'Add a subtitle to the page banner',
        'intro_content_heading' => 'Set a heading to use in introductory content, displayed at the top of the page',
        'intro_content'         => 'Set introductory content to display at the top of the page',
    ];

    /**
     * Stores a shared configuration for image sizes
     * @var array
     */
    public static $image_sizes = [
        'image' => [
            'desktop' => [
                'label' => 'Desktop',
                'width' => 1920,
                'height' => 400,
                'primary' => true
            ],
            'mobile' => [
                'label' => 'Mobile',
                'width' => 640,
                'height' => 300,
                'primary' => false
            ]
        ]
    ];
}