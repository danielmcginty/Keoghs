<?php
namespace Modules\Page\Config;

use Adm\Interfaces\NavigationInterface;

/**
 * Class Navigation
 * @package Modules\Page\Config
 *
 * The CMS Navigation Builder for the Page Module
 */
class Navigation implements NavigationInterface {
    /**
     * Returns the CMS Navigation links for the module
     *
     * @return array
     */
    public function getNavItems(){
        return [
            [ 'title' => 'Homepage', 'url' => route('adm.path', 'home-page'), 'position' => 1 ],
            [ 'title' => 'Pages', 'url' => route('adm.path', 'pages'), 'position' => 1]
        ];
    }
}