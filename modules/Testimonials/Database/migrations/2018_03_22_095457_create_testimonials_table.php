<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestimonialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'testimonials', function( Blueprint $table ){
            $table->integer( 'testimonial_id' );
            $table->integer( 'language_id' );
            $table->string( 'title' );
            $table->text( 'testimonial' );
            $table->string( 'citation' )->nullable();
            $table->string( 'image' )->nullable();
            $table->date( 'date' );
            $table->boolean( 'status' )->default( 0 );
            $table->timestamps();
            $table->softDeletes();
            $table->primary( ['testimonial_id', 'language_id'], 'testimonials_primary_key' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'testimonials' );
    }
}
