<?php
namespace Modules\Testimonials\Database\Seeders;

use Illuminate\Database\Seeder;

class TestimonialsPageBuilderTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table( 'page_builder_block_types' )->insert([
            'reference' => 'testimonials',
            'editable' => 1,
            'deletable' => 1
        ]);
    }
}
