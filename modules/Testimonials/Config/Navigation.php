<?php
namespace Modules\Testimonials\Config;

use Adm\Interfaces\NavigationInterface;

/**
 * Class Navigation
 * @package Modules\News\Config
 *
 * The CMS Navigation Builder for the News Module
 */
class Navigation implements NavigationInterface {
    /**
     * Returns the CMS Navigation links for the module
     *
     * @return array
     */
    public function getNavItems(){
        return array(
            array( 'title' => 'Testimonials', 'url' => route('adm.path', 'testimonials'), 'position' => 18 )
        );
    }
}