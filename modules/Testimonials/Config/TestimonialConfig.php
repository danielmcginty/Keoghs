<?php
namespace Modules\Testimonials\Config;

class TestimonialConfig {

    /**
     * Stores input labels
     * @var array
     */
    public static $labels = [
        'title' => 'Title',
        'testimonial' => 'Testimonial',
        'citation' => 'Citation',
        'image' => 'Citation Image',
        'date' => 'Date Received',
        'status' => 'Published?'
    ];

    /**
     * Stores input help text
     * @var array
     */
    public static $help = [
        'title' => 'A title for this testimonial, used as an internal reference',
        'testimonial' => 'The testimonial received',
        'citation' => 'Who gave the testimonial',
        'image' => 'Optionally assign an image to the testimonial, displayed alongside the citation',
        'date' => 'When the testimonial was received',
        'status' => 'Toggle whether the testimonial is visible on the website or not'
    ];

    /**
     * Stores a shared configuration for image sizes
     * @var array
     */
    public static $image_sizes = [
        'image' => [
            'single' => [
                'label' => 'Citation Image',
                'width' => 200,
                'height' => 200,
                'primary' => true
            ]
        ]
    ];
}