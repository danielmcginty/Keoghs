<?php
namespace Modules\Testimonials\Adm\Controllers;

use Adm\Controllers\AdmController;
use Modules\Testimonials\Config\TestimonialConfig;

class ModuleController extends AdmController {

    /**
     * Testimonials
     *
     * CRUD function for the Testimonials area of the CMS
     */
    public function testimonials(){
        $this->crud->setTable( 'testimonials' );
        $this->crud->setSubject( 'Testimonials' );

        $this->crud->changeType( 'testimonial', 'textarea' );
        $this->crud->changeType( 'image', 'asset' );
        $this->crud->changeType( 'date', 'date' );

        $this->crud->columns( 'title', 'date', 'status' );

        $this->crud->columnGroup( 'main', ['title', 'testimonial', 'citation', 'image'] );
        $this->crud->columnGroup( 'side', ['status', 'date'] );
    }

    /**
     * Get Default Labels
     * Called by LaraCrud getDefaultsCallback to populate the field labels based on the Config file
     *
     * @return array
     */
    public function getTestimonialsDefaultLabels(){
        return TestimonialConfig::$labels;
    }

    /**
     * Get Default Help
     * Called by LaraCrud getDefaultsCallback to populate the field help text based on the Config file
     *
     * @return array
     */
    public function getTestimonialsDefaultHelp(){
        return TestimonialConfig::$help;
    }

    /**
     * Get Asset Config
     * Called by AssetType _getAssetFieldConfig to determine whether a field is an asset group, or just a simple asset
     * based on the Config file
     *
     * @param $field_name
     * @return array
     */
    public function getTestimonialsAssetConfig( $field_name ){
        return isset( TestimonialConfig::$image_sizes[ $field_name ] ) ? TestimonialConfig::$image_sizes[ $field_name ] : [];
    }
}