<?php
namespace Modules\Testimonials\Adm\Models\PageBuilder;

use Adm\Crud\LaraCrud;
use Adm\Models\PageBuilder\PageBuilderBase;
use App\Models\Scopes\LanguageScope;
use Modules\Testimonials\Theme\Models\Testimonial;

class Testimonials extends PageBuilderBase {

    public $display_name    = 'Scrolling Testimonials';

    public $description     = 'Add a set of scrolling testimonials to the page';

    public $builder_fields  = [
        'testimonials'  => [
            'display_name'  => 'Testimonials',
            'help'          => 'Select the testimonials you\'d like to display',
            'type'          => 'orderable_multiple_select',
            'options'       => [],
            'required'      => true,
            'validation'    => [
                'required'      => 'You need to select at least 1 testimonial',
                'min:1'         => 'You need to select at least 1 testimonial'
            ]
        ]
    ];

    public function callbackBeforePrepare( LaraCrud $crud ){
        $options = [];
        foreach( Testimonial::withoutGlobalScope( LanguageScope::class )->where( 'language_id', $crud->language_id )->orderBy( 'title' )->get() as $Testimonial ){
            $options[ $Testimonial->testimonial_id ] = $Testimonial->title;
        }
        $this->builder_fields['testimonials']['options'] = $options;
    }

}