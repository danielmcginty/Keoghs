<?php
namespace Modules\Testimonials\Adm\Models\PageBuilder;

use Adm\Crud\LaraCrud;
use Adm\Models\PageBuilder\PageBuilderBase;
use App\Models\Scopes\LanguageScope;

class Testimonial extends PageBuilderBase {

    public $display_name    = 'Single Testimonial';

    public $description     = 'Add a single testimonial to the page';

    public $builder_fields  = [
        'testimonial'   => [
            'display_name'  => 'Testimonial',
            'help'          => 'Select the testimonial you\'d like to display',
            'type'          => 'select',
            'options'       => [],
            'required'      => true,
            'validation'    => [
                'required'      => 'You need to select a testimonial'
            ]
        ]
    ];

    public function callbackBeforePrepare( LaraCrud $crud ){
        $options = [];
        foreach( \Modules\Testimonials\Theme\Models\Testimonial::withoutGlobalScope( LanguageScope::class )->where( 'language_id', $crud->language_id )->orderBy( 'title' )->get() as $Testimonial ){
            $options[ $Testimonial->testimonial_id ] = $Testimonial->title;
        }
        $this->builder_fields['testimonial']['options'] = $options;
    }

}