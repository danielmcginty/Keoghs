<?php
namespace Modules\Testimonials\Theme\Models\PageBuilder;

use App\Models\PageBuilder\BaseBlockModel;
use Modules\Testimonials\Theme\Models\Testimonial as ThemeModel;

class Testimonial extends BaseBlockModel {

    public function getValue( $page_builder_block, $field, $original_value ){
        switch( $field ){
            case 'testimonial':
                return ThemeModel::where( 'testimonial_id', $original_value )->first();
            default:
                return parent::getValue( $page_builder_block, $field, $original_value );
        }
    }

}