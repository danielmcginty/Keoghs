<?php
namespace Modules\Testimonials\Theme\Models\PageBuilder;

use App\Models\PageBuilder\BaseBlockModel;
use Illuminate\Database\Eloquent\Collection;
use Modules\Testimonials\Theme\Models\Testimonial;

class Testimonials extends BaseBlockModel {

    public function getValue( $page_builder_block, $field, $original_value ){
        switch( $field ){
            case 'testimonials':
                $Testimonials = new Collection();
                if( is_json( $original_value ) && !empty( $original_value ) ){
                    foreach( (array)json_decode( $original_value, true ) as $raw_testimonial ){
                        $Testimonial = Testimonial::where( 'testimonial_id', $raw_testimonial['value'] )->first();
                        if( $Testimonial ){
                            $Testimonials->push( $Testimonial );
                        }
                    }
                }
                return $Testimonials;
            default:
                return parent::getValue( $page_builder_block, $field, $original_value );
        }
    }

}