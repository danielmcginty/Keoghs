<?php
namespace Modules\Testimonials\Theme\Models;

use App\Helpers\AssetHelper;
use App\Models\CommonModel;
use App\Models\Scopes\LanguageScope;
use App\Models\Scopes\PublishedScope;
use App\Models\Traits\HasCompositePrimaryKey;
use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Testimonials\Config\TestimonialConfig;

/**
 * Class Testimonial
 * @package Modules\Testimonials\Theme\Models
 *
 * Eloquent Model used for interaction with the testimonials database table
 */
class Testimonial extends CommonModel {
    // As we've got a deleted_at flag, a composite primary key, and want to cache DB calls, include the relevant traits
    use SoftDeletes, HasCompositePrimaryKey, HasQueryBuilderCache;

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'testimonials';

    /**
     * The primary keys in the table
     * @var array
     */
    protected $primaryKey = ['testimonial_id', 'language_id'];

    /**
     * When 'booting' this model, we only want to include testimonials that are published and in the correct language,
     * so add those as global scopes
     */
    protected static function boot(){
        parent::boot();

        static::addGlobalScope( new LanguageScope );
        static::addGlobalScope( new PublishedScope );
    }

    /**
     * Custom Accessor Function.
     * Returns the images assigned to the testimonial in an easy to use array.
     *
     * @return array
     */
    public function getImageAttribute(){
        $AssetHelper = new AssetHelper();
        return $AssetHelper->getAssets( $this->table, $this->testimonial_id, $this->language_id, 0, 'image', TestimonialConfig::$image_sizes['image'] );
    }
}