<?php
namespace Modules\Testimonials\Theme\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Url;
use Modules\Testimonials\Theme\Models\Testimonial;

/**
 * Class TestimonialController
 * @package Modules\Testimonials\Theme\Controllers
 *
 * Front-end Controller responsible for the rendering of the testimonials page
 */
class TestimonialController extends Controller {

    /**
     * Index
     * Renders the testimonials landing page based on a system page route
     */
    public function index(){
        $this_page = Url::getSystemPage( 'testimonials', 'index' );

        if( $this_page ){
            $home = Url::getSystemPage('home', 'index');
            if( $home ) {
                //$home->getAttributes()['title'] gets the original title attribute since it's modified in the model
                $this->addBreadcrumb(route('theme', ['url' => $home->url]), $home->getAttributes()['title']);
            }

            foreach( $this->getBreadcrumbs( $this_page, 'title', 'parent_page' ) as $breadcrumb ){
                $this->addBreadcrumb( route('theme', ['url' => $breadcrumb['url']]), $breadcrumb['label'] );
            }

            $this->pageTitle = $this_page->title;
            $this->this_page = $this_page;

            $this->testimonials = Testimonial::orderBy( 'date', 'desc' )->paginate( 10 );

            $this->view = 'testimonials.listing';
        }
    }

}