<?php

namespace Modules\VideoUploadBlock\Adm\Models\PageBuilder;

use Adm\Models\PageBuilder\PageBuilderBase;

class VideoUpload extends PageBuilderBase
{
    public $display_name = 'Upload Video';

    public $description = 'Embed an uploaded video.';

    public $builder_fields = [
        'video' => [
            'display_name' => 'Video (.mp4)',
            'help' => 'Select an .mp4 video file',
            'type' => 'file',
            'required' => true,
            'validation' => [
                'required' => 'You need to select an .mp4 video'
            ]
        ],
        'video_webm' => [
            'display_name' => 'Video (.webm)',
            'help' => 'Select an .webm video file',
            'type' => 'file',
            'required' => true,
            'validation' => [
                'required' => 'You need to select an .webm video'
            ]
        ]
    ];
}