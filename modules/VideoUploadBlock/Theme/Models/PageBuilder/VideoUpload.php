<?php

namespace Modules\VideoUploadBlock\Theme\Models\PageBuilder;

use App\Models\File;
use App\Models\PageBuilder\BaseBlockModel;

class VideoUpload extends BaseBlockModel
{
    public function getValue($page_builder_block, $field, $original_value)
    {
        if ($field == 'video' || $field == 'video_webm') {
            $VideoFile = File::find($original_value);

            if ($VideoFile) {
                // Return original video path
                return route('file', $VideoFile->path);
            }

            return false;
        }

        return is_serialized($original_value) ? unserialize($original_value) : $original_value;
    }
}