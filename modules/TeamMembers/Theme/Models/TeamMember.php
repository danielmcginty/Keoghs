<?php
namespace Modules\TeamMembers\Theme\Models;

use App\Helpers\AssetHelper;
use App\Models\CommonModel;
use App\Models\Scopes\LanguageScope;
use App\Models\Scopes\PublishedScope;
use App\Models\Traits\HasCompositePrimaryKey;
use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\TeamMembers\Config\TeamMemberConfig;

/**
 * Class TeamMember
 * @package Modules\TeamMembers\Theme\Models
 *
 * Eloquent Model used for interaction with the team_members database table
 */
class TeamMember extends CommonModel {
    /*
     * We have a deleted_at field, a composite primary key and want to cache
     * queries, so include the relevant traits
     */
    use SoftDeletes, HasCompositePrimaryKey, HasQueryBuilderCache;

    /**
     * The model's database table name
     *
     * @var string
     */
    protected $table = 'team_members';

    /**
     * The model's primary key fields
     *
     * @var array
     */
    protected $primaryKey = ['team_member_id', 'language_id'];

    /**
     * When retrieving from the database, we only want records in the correct
     * language that are published, so include some global scopes
     */
    protected static function boot(){
        parent::boot();

        static::addGlobalScope( new PublishedScope );
        static::addGlobalScope( new LanguageScope );
    }

    /**
     * Accessor function
     * Used to return an array of image details, rather than the stored ID,
     * for a team member
     *
     * @return array
     */
    public function getImageAttribute(){
        $AssetHelper = new AssetHelper();
        return $AssetHelper->getAssets( $this->table, $this->team_member_id, $this->language_id, $this->version, 'image', TeamMemberConfig::$image_sizes['image'] );
    }

    /**
     * Simple query scope.
     * Allows us to query the table against a search term in a central location.
     *
     * @param $query
     * @param $search_term
     * @return mixed
     */
    public function scopeMatchingSearchTerm( $query, $search_term ){
        if( empty( $search_term ) ){ return $query; }

        $query->distinct();
        $query->select( $this->table . '.*' );
        $query->where( function( $sub_query ) use ( $search_term ) {
            return $sub_query->where( $this->table . '.title', 'LIKE', '%' . $search_term . '%' )
                ->orWhere( $this->table . '.job_title', 'LIKE', '%' . $search_term . '%' );
        });

        return $query;
    }
}