<?php
namespace Modules\TeamMembers\Theme\Models\PageBuilder;

use App\Models\PageBuilder\BaseBlockModel;
use Illuminate\Database\Eloquent\Collection;
use Modules\TeamMembers\Theme\Models\TeamMember;

class MeetTheTeam extends BaseBlockModel {

    public function getValue( $page_builder_block, $field, $original_value ){
        switch( $field ){
            case 'team_members':
                $TeamMembers = new Collection();
                if( !empty( $original_value ) && is_json( $original_value ) ){
                    foreach( (array)json_decode( $original_value, true ) as $raw_team_member ){
                        if( empty( $raw_team_member['value'] ) ){ continue; }

                        $TeamMember = TeamMember::where( 'team_member_id', $raw_team_member['value'] )->first();
                        if( $TeamMember ){
                            $TeamMembers->push( $TeamMember );
                        }
                    }
                }

                if( $TeamMembers->isEmpty() ){
                    $TeamMembers = TeamMember::where( 'default_in_block', true )->orderBy( 'sort_order' )->get();
                }

                return $TeamMembers;
            case 'heading':
                return !empty( $original_value ) ? $original_value : 'Meet the Team';
            default:
                return parent::getValue( $page_builder_block, $field, $original_value );
        }
    }

}