<?php
namespace Modules\TeamMembers\Theme\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Url;
use Modules\TeamMembers\Theme\Models\TeamMember;

/**
 * Class TeamMemberController
 * @package Modules\TeamMembers\Theme\Controllers
 *
 * Front-end Controller responsible for the rendering of team member pages, and
 * the team member landing page
 */
class TeamMemberController extends Controller {

    /**
     * Index
     * Renders a single team member page, based on the ID passed through the
     * router
     *
     * @param $team_member_id
     */
    public function index( $team_member_id ){
        $this_page = TeamMember::where( 'team_member_id', $team_member_id )->first();
        if( $this_page ){
            $home = Url::getSystemPage('home', 'index');
            if( $home ) {
                $this->addBreadcrumb(route('theme', ['url' => $home->url]), $home->getAttributes()['title']);
            }

            $landing_page = Url::getSystemPage( 'team_member', 'landing' );
            if( $landing_page ){
                foreach( $this->getBreadcrumbs( $landing_page, 'title', 'parent_page' ) as $breadcrumb ){
                    $this->addBreadcrumb( route('theme', ['url' => $breadcrumb['url']]), $breadcrumb['label'] );
                }
            }

            $this->addBreadcrumb( route('theme', $this_page->url), $this_page->title );

            $this->pageTitle = $this_page->title;
            $this->this_page = $this_page;

            $this->view = 'team-members.team-member';
        }
    }

    /**
     * Landing
     * Renders the team members landing page based on a system page route
     */
    public function landing(){
        $this_page = Url::getSystemPage( 'team_member', 'landing' );
        if( $this_page ){
            $home = Url::getSystemPage('home', 'index');
            if( $home ) {
                $this->addBreadcrumb(route('theme', ['url' => $home->url]), $home->getAttributes()['title']);
            }

            foreach( $this->getBreadcrumbs( $this_page, 'title', 'parent_page' ) as $breadcrumb ){
                $this->addBreadcrumb( route('theme', ['url' => $breadcrumb['url']]), $breadcrumb['label'] );
            }

            $this->pageTitle = $this_page->title;
            $this->this_page = $this_page;

            $this->search_term = request()->input( 'search_term' );
            $this->TeamMembers = TeamMember::matchingSearchTerm( request()->input( 'search_term' ) )->orderBy( 'sort_order' )->paginate( 12 );

            $this->view = 'team-members.archive';
        }
    }

}