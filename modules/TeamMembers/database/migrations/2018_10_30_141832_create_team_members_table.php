<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'team_members', function( Blueprint $table ){
            $table->integer( 'team_member_id' );
            $table->integer( 'language_id' );
            $table->integer( 'version' )->default( 1 );
            $table->string( 'title' );
            $table->string( 'job_title' )->nullable();
            $table->text( 'specialisms' )->nullable();
            $table->string( 'telephone' )->nullable();
            $table->string( 'email' )->nullable();
            $table->text( 'image' )->nullable();
            $table->integer( 'sort_order' )->default( 0 );
            $table->boolean( 'status' )->default( 0 );
            $table->timestamps();
            $table->softDeletes();
            $table->primary( ['team_member_id', 'language_id', 'version'], 'team_members_pk' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'team_members' );
    }
}
