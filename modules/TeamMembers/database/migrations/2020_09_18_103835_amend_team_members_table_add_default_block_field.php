<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AmendTeamMembersTableAddDefaultBlockField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'team_members', function( Blueprint $table ){
            $table->boolean( 'default_in_block' )->default( 0 )->after( 'sort_order' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'team_members', function( Blueprint $table ){
            $table->dropColumn( 'default_in_block' );
        });
    }
}
