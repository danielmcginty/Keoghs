<?php
namespace Modules\TeamMembers\Config;

class TeamMemberConfig {

    /**
     * Stores input labels
     * @var array
     */
    public static $labels = [
        'title'             => 'Name',
        'job_title'         => 'Job Title',
        'specialisms'       => 'Specialisms',
        'telephone'         => 'Telephone Number',
        'email'             => 'Email Address',
        'image'             => 'Images',
        'default_in_block'  => 'Block Default',
    ];

    /**
     * Stores input help text
     * @var array
     */
    public static $help = [
        'title'             => 'The team member\'s name',
        'default_in_block'  => 'Set whether this team member should appear in the \'Meet the Team\' block by default',
    ];

    /**
     * Stores a shared configuration for image sizes
     * @var array
     */
    public static $image_sizes = [
        'image' => [
            'listing' => [
                'label'         => 'Image',
                'width'         => 400,
                'height'        => 400,
                'primary'       => true,
                'required'      => false,
                'no_fallback'   => true
            ]
        ]
    ];

}