<?php
namespace Modules\TeamMembers\Config;

use Adm\Interfaces\NavigationInterface;

/**
 * Class Navigation
 * @package Modules\TeamMembers\Config
 *
 * The CMS Navigation Builder for the Team Members Module
 */
class Navigation implements NavigationInterface {

    /**
     * Returns the CMS Navigation links for the module
     *
     * @return array
     */
    public function getNavItems(){
        return [
            [ 'title' => 'Team Members', 'url' => route('adm.path', 'team-members'), 'position' => 40 ]
        ];
    }

}