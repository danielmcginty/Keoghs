<?php
namespace Modules\TeamMembers\Adm\Models\PageBuilder;

use Adm\Crud\LaraCrud;
use Adm\Models\PageBuilder\PageBuilderBase;
use App\Models\Scopes\LanguageScope;
use Modules\TeamMembers\Theme\Models\TeamMember;

class MeetTheTeam extends PageBuilderBase {

    public $display_name        = 'Meet the Team';
    public $description         = 'Add a set of team members to the page';

    public $builder_fields      = [
        'heading'       => [
            'display_name'  => 'Heading',
            'help'          => 'Add a heading to the block, defaults to \'Meet the Team\' if left empty',
            'type'          => 'string',
            'required'      => false,
            'validation'    => [],
            'group'         => 'Main'
        ],
        'team_members'  => [
            'display_name'  => 'Team Members',
            'help'          => 'Select the team members to display, or leave empty to use the default set',
            'type'          => 'orderable_multiple_select',
            'options'       => [],
            'required'      => false,
            'validation'    => [],
            'group'         => 'Main'
        ]
    ];

    public function callbackBeforePrepare( LaraCrud $crud ){
        $opts = [];
        foreach( TeamMember::withoutGlobalScope( LanguageScope::class )->where( 'language_id', $crud->language_id )->orderBy( 'title', 'asc' )->get() as $TeamMember ){
            $opts[ $TeamMember->team_member_id ] = $TeamMember->title;
        }
        $this->builder_fields['team_members']['options'] = $opts;
    }

}