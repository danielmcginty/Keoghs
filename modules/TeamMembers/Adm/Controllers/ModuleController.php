<?php
namespace Modules\TeamMembers\Adm\Controllers;

use Adm\Controllers\AdmController;
use App\Models\Url;
use Modules\TeamMembers\Config\TeamMemberConfig;

class ModuleController extends AdmController {

    /**
     * Team Members
     *
     * CRUD function for the Team Members area of the CMS
     */
    public function team_members(){
        $this->crud->setTable( 'team_members' );
        $this->crud->setSubject( 'Team Members' );

        $this->crud->addColumn( 'path' );

        $SystemPage = Url::getSystemPage( 'team_member', 'landing' );
        if( $SystemPage ){
            $this->crud->setBasePathPrefix( 'pages', $SystemPage->page_id, 'page_id' );
        }

        $this->crud->changeType( 'specialisms', 'comma_separated' );

        $this->crud->addColumn( 'page_builder' );
        $this->crud->changeType( 'page_builder', 'page_builder' );
        $this->crud->enablePageBuilder();

        $this->crud->columns( 'title', 'path', 'job_title', 'sort_order', 'default_in_block', 'status' );

        $this->crud->columnGroup( 'main', ['title', 'path', 'job_title', 'specialisms', 'telephone', 'email', 'image', 'page_builder'] );
        $this->crud->columnGroup( 'side', ['status', 'default_in_block', 'sort_order'] );

        $this->crud->excludeColumns( 'specialisms' );
    }

    /**
     * Get Default Labels
     * Called by LaraCrud getDefaultsCallback to populate the field labels based on the Config file
     *
     * @return array
     */
    public function getTeamMembersDefaultLabels(){
        return TeamMemberConfig::$labels;
    }

    /**
     * Get Default Help
     * Called by LaraCrud getDefaultsCallback to populate the field help text based on the Config file
     *
     * @return array
     */
    public function getTeamMembersDefaultHelp(){
        return TeamMemberConfig::$help;
    }

    /**
     * Get Asset Config
     * Called by AssetType _getAssetFieldConfig to determine whether a field is an asset group, or just a simple asset
     * based on the Config file
     *
     * @param $field_name
     * @return array
     */
    public function getTeamMembersAssetConfig( $field_name ){
        return isset( TeamMemberConfig::$image_sizes[ $field_name ] ) ? TeamMemberConfig::$image_sizes[ $field_name ] : [];
    }

}