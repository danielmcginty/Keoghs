<?php
namespace Modules\RowWidthImageBlock\Adm\Models\PageBuilder;

use Adm\Models\PageBuilder\PageBuilderBase;

class RowWidthImage extends PageBuilderBase {

    public $display_name    = 'Content Width Image';

    public $description     = 'A single image, as wide as the page\'s content.';

    public $builder_fields  = [
        'image' => [
            'display_name'  => 'Image',
            'help'          => 'Select the image you would like to display, optionally selecting different images for different screen sizes.',
            'type'          => 'asset',
            'required'      => true,
            'validation'    => [
                'desktop'       => [
                    'required' => 'You need to select a Desktop image.'
                ]
            ],
            'array_validation_type' => 'single'
        ]
    ];

    public $column_extras   = [
        'image' => [
            'multi_size' => true,
            'sizes' => [
                'desktop' => [
                    'label'     => 'Desktop',
                    'width'     => 1340,
                    'height'    => 0,
                    'primary'   => true
                ],
                'mobile' => [
                    'label'     => 'Mobile',
                    'width'     => 640,
                    'height'    => 0,
                    'primary'   => false
                ]
            ]
        ]
    ];

}