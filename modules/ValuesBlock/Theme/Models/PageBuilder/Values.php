<?php

namespace Modules\ValuesBlock\Theme\Models\PageBuilder;

use App\Helpers\AssetHelper;
use App\Models\PageBuilder\BaseBlockModel;
use Modules\ValuesBlock\Adm\Models\PageBuilder\Values as AdmBlock;

class Values extends BaseBlockModel
{
    public function getValue($page_builder_block, $field, $original_value)
    {
        $AssetHelper = new AssetHelper();
        $AdmBlock = new AdmBlock();

        if ($field == 'sections') {
            $raw_sections = is_json($original_value) ? json_decode($original_value, true) : [];
            $sections = [];

            if (!empty($raw_sections)) {

                foreach ($raw_sections as $sort_order => $section) {
                    if (!empty($section['image'])) {
                        $section['image'] = $AssetHelper->getAssets(
                            $page_builder_block->table_name,
                            $page_builder_block->table_key,
                            $page_builder_block->language_id,
                            $page_builder_block->version,
                            'page_builder.'.$page_builder_block->sort_order.'.content.'.$field.'.'.$sort_order.'.image',
                            $AdmBlock->column_extras[$field]['repeatable_fields']['image']['sizes']
                        );
                    }

                    $sections[] = $section;
                }
            }

            return $sections;
        }

        if ($field == 'summary_image') {
            return $AssetHelper->getAssets(
                $page_builder_block->table_name,
                $page_builder_block->table_key,
                $page_builder_block->language_id,
                $page_builder_block->version,
                'page_builder.'.$page_builder_block->sort_order.'.content.'.$field,
                $AdmBlock->column_extras[$field]['sizes']
            );
        }

        return is_serialized($original_value) ? unserialize($original_value) : $original_value;
    }
}