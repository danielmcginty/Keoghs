<?php

namespace Modules\ValuesBlock\Adm\Models\PageBuilder;

use Adm\Models\PageBuilder\PageBuilderBase;

class Values extends PageBuilderBase
{
    public $display_name = 'Values';

    public $description = 'Add a "Values" block to the page';

    public $builder_fields = [
        'sections' => [
            'display_name' => 'Sections',
            'help' => 'Add Slides',
            'type' => 'repeatable',
            'required' => false,
            'validation' => [],
            'group' => 'Sections',
        ],
        'summary_title' => [
            'display_name' => 'Title',
            'type' => 'string',
            'required' => true,
            'validation' => [],
            'group' => 'Summary',
        ],
        'summary_content' => [
            'display_name' => 'Content',
            'type' => 'text',
            'wysiwyg' => true,
            'required' => true,
            'validation' => [],
            'group' => 'Summary',
        ],
        'summary_image' => [
            'display_name' => 'Image',
            'type' => 'asset',
            'required' => true,
            'validation' => [],
            'group' => 'Summary',
        ],
    ];

    public $column_extras = [
        'sections' => [
            'repeatable_fields' => [
                'image' => [
                    'label' => 'Image',
                    'type' => 'asset',
                    'multi_size' => true,
                    'sizes' => [
                        'single' => [
                            'label' => 'Primary',
                            'width' => 580,
                            'height' => 580,
                            'primary' => true
                        ],
                    ]
                ],
                'title' => [
                    'label' => 'Title',
                    'type' => 'string',
                ],
                'content' => [
                    'label' => 'Content',
                    'type' => 'text',
                    'wysiwyg' => true,
                ],
            ]
        ],
        'summary_image' => [
            'multi_size' => true,
            'sizes' => [
                'single' => [
                    'label' => 'Primary',
                    'width' => 500,
                    'height' => 0,
                    'primary' => true
                ],
            ]
        ]
    ];
}