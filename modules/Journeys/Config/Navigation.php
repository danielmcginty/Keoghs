<?php

namespace Modules\Journeys\Config;

use Adm\Interfaces\NavigationInterface;

class Navigation implements NavigationInterface
{
    public function getNavItems()
    {
        return [
            [
                'title' => 'Journeys',
                'url' => route('adm.path', 'journeys'),
                'position' => 70,
            ]
        ];
    }
}