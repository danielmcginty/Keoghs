<?php

namespace Modules\Journeys\Config;

class JourneyConfig
{
    public static $labels = [
        //
    ];

    public static $help = [
        //
    ];

    public static $image_sizes = [
        'image' => [
            'article' => [
                'label' => 'Primary',
                'width' => 910,
                'height' => 510,
                'primary' => true
            ],
            'listing' => [
                'label' => 'Listing',
                'width' => 625,
                'height' => 350,
                'primary' => false
            ]
        ]
    ];
}