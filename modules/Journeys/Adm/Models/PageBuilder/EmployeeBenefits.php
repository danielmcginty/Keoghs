<?php

namespace Modules\Journeys\Adm\Models\PageBuilder;

use Adm\Models\PageBuilder\PageBuilderBase;

class EmployeeBenefits extends PageBuilderBase
{
    public $display_name    = 'Employee Benefits';
    public $description     = 'Add a 2 column block to the page, showcasing Employee Benefits';

    public $builder_fields  = [
        'heading'   => [
            'display_name'  => 'Heading',
            'help'          => 'Add a heading to the block',
            'type'          => 'string',
            'required'      => false,
            'validation'    => [],
            'group'         => 'Main'
        ],
        'benefits'  => [
            'display_name'  => 'Benefits list',
            'help'          => 'Manage the displayed benefits here',
            'type'          => 'repeatable',
            'required'      => false,
            'validation'    => [],
            'group'         => 'Main'
        ],
        'cta_link'  => [
            'display_name'  => 'CTA - Link',
            'help'          => 'Add a CTA next to the last benefit. Please note that both link & text must be set.',
            'type'          => 'link',
            'required'      => false,
            'validation'    => [],
            'group'         => 'Main'
        ],
        'cta_text'  => [
            'display_name'  => 'CTA - Text',
            'help'          => 'Add a CTA next to the last benefit. Please note that both link & text must be set.',
            'type'          => 'string',
            'required'      => false,
            'validation'    => [],
            'group'         => 'Main'
        ]
    ];

    public $column_extras = [
        'benefits' => [
            'repeatable_fields' => [
                'heading'   => [
                    'label'     => 'Heading',
                    'type'      => 'string'
                ],
                'content'   => [
                    'label'     => 'Content',
                    'type'      => 'string'
                ]
            ]
        ]
    ];
}