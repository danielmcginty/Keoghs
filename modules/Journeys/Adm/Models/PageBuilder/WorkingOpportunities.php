<?php

namespace Modules\Journeys\Adm\Models\PageBuilder;

use Adm\Models\PageBuilder\PageBuilderBase;

class WorkingOpportunities extends PageBuilderBase
{
    public $display_name    = 'Opportunities';
    public $description     = 'Add a block to the page, showcasing career opportunities at Keoghs';

    public $builder_fields  = [
        'heading'   => [
            'display_name'  => 'Heading',
            'help'          => 'Add a heading to the block',
            'type'          => 'string',
            'required'      => false,
            'validation'    => [],
            'group'         => 'Main'
        ],
        'opportunities'  => [
            'display_name'  => 'Opportunities',
            'help'          => 'Manage the displayed opportunities here',
            'type'          => 'repeatable',
            'required'      => false,
            'validation'    => [],
            'group'         => 'Main'
        ],
        'background_image'  => [
            'display_name'  => 'Background image',
            'help'          => 'Set the background image for the block',
            'type'          => 'asset',
            'required'      => false,
            'validation'    => [],
            'group'         => 'Main'
        ]
    ];

    public $column_extras = [
        'opportunities' => [
            'repeatable_fields' => [
                'heading'   => [
                    'label'     => 'Heading',
                    'type'      => 'string'
                ],
                'content'   => [
                    'label'     => 'Content',
                    'type'      => 'textarea'
                ],
                'link'      => [
                    'label'     => 'Link',
                    'type'      => 'link'
                ],
                'link_text' => [
                    'label'     => 'Link Text',
                    'type'      => 'string'
                ]
            ]
        ],
        'background_image'  => [
            'multi_size'    => true,
            'sizes'         => [
                'desktop'       => [
                    'label'         => 'Tablet & Desktop',
                    'width'         => 1920,
                    'height'        => 620,
                    'primary'       => true
                ],
                'mobile'        => [
                    'label'         => 'Mobile',
                    'width'         => 640,
                    'height'        => 0,
                    'primary'       => false
                ]
            ]
        ]
    ];
}
