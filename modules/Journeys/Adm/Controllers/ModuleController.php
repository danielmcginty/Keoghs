<?php

namespace Modules\Journeys\Adm\Controllers;

use Adm\Controllers\AdmController;
use App\Models\Url;
use Modules\Journeys\Config\JourneyConfig;

class ModuleController extends AdmController
{
    public function journeys()
    {
        $this->crud->setTable('journeys');
        $this->crud->setSubject('Journeys');

        $this->crud->addColumn('path');
        $SystemPage = Url::getSystemPage('journey', 'index');
        if ($SystemPage) {
            $this->crud->setBasePathPrefix('pages', $SystemPage->page_id, 'page_id');
        }
        $this->crud->urlGenerationField('name');

        $this->crud->setDefaultValue('status', true);

        $this->crud->changeType('excerpt', 'textarea');

        $this->crud->orderBy('sort_order', 'asc');

        $this->crud->changeType('video', 'video');

        $this->crud->columns('image', 'title', 'name', 'position', 'sort_order');

        $this->crud->columnGroup(
            'main',
            ['title', 'name', 'path', 'position', 'excerpt', 'content', 'image', 'video']
        );
        $this->crud->columnGroup('side', ['status', 'sort_order']);
    }

    public function getJourneysDefaultLabels()
    {
        return JourneyConfig::$labels;
    }

    public function getJourneysDefaultHelp()
    {
        return JourneyConfig::$help;
    }

    public function getJourneysAssetConfig($field_name)
    {
        return JourneyConfig::$image_sizes[$field_name] ?? [];
    }
}