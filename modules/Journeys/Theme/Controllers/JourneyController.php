<?php

namespace Modules\Journeys\Theme\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Url;
use Illuminate\Http\Request;
use Modules\Journeys\Theme\Models\Journey;

class JourneyController extends Controller
{
    public function index(Request $request)
    {
        $this_page = Url::getSystemPage('journey', 'index');

        if (!$this_page) {
            return;
        }

        $home = Url::getSystemPage('home', 'index');
        if ($home) {
            $this->addBreadcrumb(route('theme', ['url' => $home->url]), $home->getAttributes()['title']);
        }
        $this->addBreadcrumb(route('theme', ['url' => $this_page->url]), $this_page->getAttributes()['title']);

        $this->this_page = $this_page;
        $this->journeys = $this->getJourneys($request);

        $this->view = 'journeys.archive';
    }

    public function journey($id)
    {
        $journey = Journey::query()->where('journey_id', $id)->first();

        if (!$journey) {
            return;
        }

        $home = Url::getSystemPage('home', 'index');
        if ($home) {
            $this->addBreadcrumb(route('theme', ['url' => $home->url]), $home->getAttributes()['title']);
        }

        $archive = Url::getSystemPage('journey', 'index');
        if ($archive) {
            $this->addBreadcrumb(route('theme', ['url' => $archive->url]), $archive->getAttributes()['title']);
        }

        $this->journey = $journey;
        $this->other_journeys = $this->getOtherJourneys($id);
        $this->archive = $archive;

        $this->view = 'journeys.journey';
    }

    private function getJourneys(Request $request)
    {
        return Journey::query()->orderBy('sort_order')->get();
    }

    private function getOtherJourneys($except)
    {
        return Journey::query()
            ->where('journey_id', '!=', $except)
            ->inRandomOrder()
            ->take(2)
            ->get();
    }
}