<?php

namespace Modules\Journeys\Theme\Models\PageBuilder;

use App\Helpers\LinkHelper;
use App\Models\PageBuilder\BaseBlockModel;

class EmployeeBenefits extends BaseBlockModel
{
    public function getValue($page_builder_block, $field, $original_value) 
    {
        switch ($field) {
            case 'benefits':
                $benefits = [];
                if (!empty($original_value) && is_json($original_value)) {
                    foreach ((array)json_decode($original_value, true) as $benefit) {
                        if (empty($benefit['heading']) || empty($benefit['content'])) { continue; }

                        $benefits[] = $benefit;
                    }
                }
                return $benefits;
            case 'cta_link':
                if (!empty($original_value) && is_json($original_value)) {
                    return LinkHelper::parseLink(json_decode($original_value, true));
                }
                return false;
            default:
                return parent::getValue($page_builder_block, $field, $original_value);
        }
    }
}