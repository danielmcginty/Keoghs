<?php

namespace Modules\Journeys\Theme\Models\PageBuilder;

use App\Helpers\LinkHelper;
use App\Models\PageBuilder\BaseBlockModel;
use Modules\Journeys\Adm\Models\PageBuilder\WorkingOpportunities as AdmBlock;

class WorkingOpportunities extends BaseBlockModel
{
    public function getValue($page_builder_block, $field, $original_value)
    {
        switch ($field) {
            case 'opportunities':
                $opportunities = [];
                if (!empty($original_value) && is_json($original_value)) {
                    foreach ((array)json_decode($original_value, true) as $opportunity) {
                        if (empty($opportunity['heading']) || empty($opportunity['content'])) {
                            continue;
                        }

                        $opportunity['link'] = LinkHelper::parseLink(json_decode($opportunity['link'], true));

                        $opportunities[] = $opportunity;
                    }
                }
                return $opportunities;
            case 'background_image':
                $AdmBlock = new AdmBlock();
                return parent::getAssetFieldValue(
                    $page_builder_block,
                    $field,
                    $original_value,
                    $AdmBlock->column_extras['background_image']['sizes']
                );
            default:
                return parent::getValue($page_builder_block, $field, $original_value);
        }
    }
}
