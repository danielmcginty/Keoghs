<?php

namespace Modules\Journeys\Theme\Models;

use App\Helpers\AssetHelper;
use App\Models\CommonModel;
use App\Models\Scopes\LanguageScope;
use App\Models\Scopes\PublishedScope;
use App\Models\Traits\HasCompositePrimaryKey;
use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Journeys\Config\JourneyConfig;

class Journey extends CommonModel
{
    use SoftDeletes, HasCompositePrimaryKey, HasQueryBuilderCache;

    protected $table = 'journeys';

    protected $primaryKey = ['journey_id', 'language_id'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new LanguageScope);
        static::addGlobalScope(new PublishedScope);
    }

    public function getImageAttribute()
    {
        return (new AssetHelper())->getAssets(
            $this->table,
            $this->journey_id,
            $this->language_id,
            $this->version,
            'image',
            JourneyConfig::$image_sizes['image']
        );
    }
}