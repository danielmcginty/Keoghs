<?php
namespace Modules\ContentWithImageBlock\Adm\Models\PageBuilder;

use Adm\Crud\LaraCrud;
use Adm\Models\PageBuilder\PageBuilderBase;
use App\Helpers\BlockHelper;
use App\Helpers\ColourHelper;

class ContentWithImage extends PageBuilderBase {

    public $display_name    = 'Featured content';

    public $description     = 'Add a content block, with an image, to the page';

    public $builder_fields  = [
        'heading'   => [
            'display_name'  => 'Heading',
            'help'          => 'Add a heading to the block',
            'type'          => 'string',
            'required'      => false,
            'validation'    => [],
            'group'         => 'Main'
        ],
        'content'   => [
            'display_name'  => 'Content',
            'help'          => 'Set the content for the block',
            'type'          => 'text',
            'wysiwyg'       => true,
            'required'      => true,
            'validation'    => [
                'required'  => 'You need to provide content'
            ],
            'group'         => 'Main'
        ],
        'image' => [
            'display_name'          => 'Image',
            'help'                  => 'Set the image for the block',
            'type'                  => 'asset',
            'required'              => true,
            'array_validation_type' => 'single',
            'validation'            => [
                'single' => [
                    'required'      => 'You need to select an image'
                ]
            ],
            'group'                 => 'Main'
        ],
        'buttons'   => [
            'display_name'  => 'CTA Buttons',
            'help'          => 'Add buttons to the block',
            'type'          => 'repeatable',
            'required'      => false,
            'validation'    => [],
            'group'         => 'Main'
        ],
        'alignment' => [
            'display_name'  => 'Left Alignment',
            'help'          => 'Align the image to the left of the content, instead of the right',
            'type'          => 'boolean',
            'required'      => false,
            'validation'    => [],
            'group'         => 'Main'
        ],
        'video_url' => [
            'display_name'  => 'Video URL',
            'help'          => 'Optionally add a video URL, to play in a pop-up box when clicking the image',
            'type'          => 'string',
            'required'      => false,
            'validation'    => [],
            'group'         => 'Main'
        ]
    ];

    public $column_extras = [
        'image' => [
            'multi_size'    => true,
            'sizes'         => [
                'single'    => [
                    'label'     => 'Image',
                    'width'     => 800,
                    'height'    => 0,
                    'primary'   => true
                ]
            ]
        ],
        'buttons'   => [
            'repeatable_fields' => [
                'text'  => [
                    'label' => 'Text',
                    'type'  => 'string'
                ],
                'link'  => [
                    'label' => 'Link',
                    'type'  => 'link'
                ],
                'colour'    => [
                    'label'     => 'Colour',
                    'type'      => 'select',
                    'options'   => []
                ]
            ]
        ]
    ];

    public function callbackBeforePrepare( LaraCrud $crud ){
        $this->column_extras['buttons']['repeatable_fields']['colour']['options'] = ColourHelper::getButtonColours();
    }

}