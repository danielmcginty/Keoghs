<?php
namespace Modules\ContentWithImageBlock\Theme\Models\PageBuilder;

use App\Helpers\AssetHelper;
use App\Helpers\BlockHelper;
use App\Models\PageBuilder\BaseBlockModel;
use Modules\ContentWithImageBlock\Adm\Models\PageBuilder\ContentWithImage as AdmContentWithImage;

class ContentWithImage extends BaseBlockModel {

    public function getValue( $page_builder_block, $field, $original_value ){
        switch( $field ){
            case 'buttons':
                return BlockHelper::parseBlockButtonsField( $original_value );
            case 'image':
                $AdmContentWithImage = new AdmContentWithImage();
                return $this->getAssetFieldValue( $page_builder_block, $field, $original_value, $AdmContentWithImage->column_extras[ $field ]['sizes'] );
            default:
                return parent::getValue( $page_builder_block, $field, $original_value );
        }
    }

}