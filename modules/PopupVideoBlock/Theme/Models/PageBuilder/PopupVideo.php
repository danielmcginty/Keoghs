<?php

namespace Modules\PopupVideoBlock\Theme\Models\PageBuilder;

use App\Models\PageBuilder\BaseBlockModel;
use Illuminate\Support\Str;
use Modules\PopupVideoBlock\Adm\Models\PageBuilder\PopupVideo as AdmBlock;

class PopupVideo extends BaseBlockModel
{
    public function getValue($page_builder_block, $field, $original_value)
    {
        switch ($field) {
            case 'image':
                $AdmBlock = new AdmBlock();
                return parent::getAssetFieldValue(
                    $page_builder_block,
                    $field,
                    $original_value,
                    $AdmBlock->column_extras['image']['sizes']
                );
            default:
                return parent::getValue($page_builder_block, $field, $original_value);
        }
    }

    public function postProcess($fields)
    {
        $fields['video_type'] = $this->parseVideoTypeFromUrl($fields['video']);

        return $fields;
    }

    private function parseVideoTypeFromUrl($video_url)
    {
        if (Str::contains($video_url, 'youtube')) {
            return 'youtube';
        }

        if (Str::contains($video_url, 'vimeo')) {
            return 'vimeo';
        }

        if (Str::contains($video_url, 'wistia')) {
            return 'wistia';
        }

        return null;
    }
}