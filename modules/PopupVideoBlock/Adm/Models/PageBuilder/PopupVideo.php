<?php
namespace Modules\PopupVideoBlock\Adm\Models\PageBuilder;

use Adm\Models\PageBuilder\PageBuilderBase;

class PopupVideo extends PageBuilderBase {

    public $display_name    = 'Popup Video';

    public $description     = 'Add a video to the page, that opens in a popup modal when clicked.';

    public $builder_fields  = [
        'heading'   => [
            'display_name'  => 'Heading',
            'help'          => 'Add a heading to appear above the image',
            'type'          => 'string',
            'required'      => false,
            'validation'    => [],
            'group'         => 'Main'
        ],
        'video'     => [
            'display_name'  => 'Video',
            'help'          => 'Enter a Youtube, Vimeo, or Wistia URL e.g. https://www.youtube.com/watch?v=ZRCdORJiUgU, https://vimeo.com/259411563, or https://fast.wistia.com/embed/medias/5mtgfn0f2x',
            'type'          => 'string',
            'required'      => true,
            'validation'    => [
                'required'      => 'You need to set the video URL'
            ]
        ],
        'image'     => [
            'display_name'  => 'Image',
            'help'          => 'Select the image you would like to display, optionally selecting different images for different screen sizes.',
            'type'          => 'asset',
            'required'      => true,
            'validation'    => [
                'desktop'       => [
                    'required' => 'You need to select a Desktop image.'
                ]
            ],
            'array_validation_type' => 'single'
        ]
    ];

    public $column_extras   = [
        'image' => [
            'multi_size' => true,
            'sizes' => [
                'desktop' => [
                    'label'     => 'Desktop',
                    'width'     => 1340,
                    'height'    => 0,
                    'primary'   => true
                ],
                'mobile' => [
                    'label'     => 'Mobile',
                    'width'     => 640,
                    'height'    => 0,
                    'primary'   => false
                ]
            ]
        ]
    ];

}