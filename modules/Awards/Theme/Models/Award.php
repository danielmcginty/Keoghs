<?php

namespace Modules\Awards\Theme\Models;

use App\Helpers\AssetHelper;
use App\Helpers\LinkHelper;
use App\Models\CommonModel;
use App\Models\Scopes\LanguageScope;
use App\Models\Scopes\PublishedScope;
use App\Models\Traits\HasCompositePrimaryKey;
use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Awards\Config\AwardConfig;

class Award extends CommonModel
{
    use SoftDeletes, HasCompositePrimaryKey, HasQueryBuilderCache;

    protected $table = 'awards';

    protected $primaryKey = ['award_id', 'language_id'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new LanguageScope);
        static::addGlobalScope(new PublishedScope);
    }

    public function getLogoAttribute()
    {
        return (new AssetHelper())->getAssets(
            $this->table,
            $this->award_id,
            $this->language_id,
            $this->version,
            'logo',
            AwardConfig::$image_sizes['logo']
        );
    }

    public function getLinkAttribute($value)
    {
        return LinkHelper::parseLink(json_decode($value, true));
    }

    public function getWordyDateAttribute()
    {
        return !empty($this->award_date) ? date("j F Y", strtotime($this->award_date)) : null;
    }
}