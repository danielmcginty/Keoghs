<?php

namespace Modules\Awards\Theme\Models\PageBuilder;

use App\Models\PageBuilder\BaseBlockModel;
use Illuminate\Support\Collection;
use Modules\Awards\Theme\Models\Award;

class Awards extends BaseBlockModel
{
    public function getValue($page_builder_block, $field, $original_value)
    {
        switch ($field) {
            case 'awards':
                $Awards = new Collection();
                if (!empty($original_value) && is_json($original_value)) {
                    foreach ((array) json_decode($original_value, true) as $raw_award) {
                        if (empty($raw_award['value'])) {
                            continue;
                        }

                        $Award = Award::where('award_id', $raw_award['value'])->first();
                        if ($Award) {
                            $Awards->push($Award);
                        }
                    }
                }

                if ($Awards->isEmpty()) {
                    $Awards = Award::where('default_in_block', true)->orderBy('title')->get();
                }

                return $Awards;
            default:
                return parent::getValue($page_builder_block, $field, $original_value);
        }
    }
}