<?php

namespace Modules\Awards\Theme\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Url;
use Illuminate\Http\Request;
use Modules\Awards\Config\AwardConfig;
use Modules\Awards\Theme\Models\Award;

class AwardController extends Controller
{
    public function index(Request $request)
    {
        $this_page = Url::getSystemPage('award', 'index');

        if (!$this_page) {
            return;
        }

        $home = Url::getSystemPage('home', 'index');
        if ($home) {
            $this->addBreadcrumb(route('theme', ['url' => $home->url]), $home->getAttributes()['title']);
        }
        $this->addBreadcrumb(route('theme', ['url' => $this_page->url]), $this_page->getAttributes()['title']);

        $awards_data = $this->getAwardsData($request);

        $this->this_page = $this_page;
        $this->featured_award = $this->getFeaturedAward();
        $this->awards = $awards_data['awards'];
        $this->selected_sort_key = $awards_data['selected_sort_key'];
        $this->selected_sort = $awards_data['selected_sort'];
        $this->sort_options = $this->getSortOptions();

        $this->view = 'awards.archive';
    }

    public function loadMoreAwards(Request $request)
    {
        return $this->getAwardsData($request, true);
    }

    public function filterAwards(Request $request)
    {
        return $this->getAwardsData($request, true);
    }

    private function getAwardsData(Request $request, $render = false)
    {
        $selected_sort_key = $request->query('sort-by', 'latest');
        $selected_sort = AwardConfig::$sort_options[$selected_sort_key] ?? null;

        $awards = Award::query()
            ->when(!empty($selected_sort), function ($query) use ($selected_sort) {
                return $query->orderBy($selected_sort['field'], $selected_sort['direction']);
            })
            ->paginate(9);

        $rendered_awards = '';
        if ($render) {
            foreach ($awards as $award) {
                $rendered_awards .= view('awards.partials.card', ['award' => $award])->render();
            }
        }

        return [
            'selected_sort_key' => $selected_sort_key,
            'selected_sort' => $selected_sort,
            'awards' => $awards,
            'rendered_awards' => $rendered_awards
        ];
    }

    private function getFeaturedAward()
    {
        return Award::query()->latest()->first();
    }

    private function getSortOptions()
    {
        return AwardConfig::$sort_options;
    }
}