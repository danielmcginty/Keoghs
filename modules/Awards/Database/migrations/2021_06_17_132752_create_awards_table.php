<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAwardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('awards', function (Blueprint $table) {
            $table->integer('award_id');
            $table->integer('language_id');
            $table->integer('version');
            $table->string('title');
            $table->string('subtitle');
            $table->string('logo');
            $table->text('link');
            $table->boolean('status')->default(1);
            $table->boolean('default_in_block')->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->primary(['award_id', 'language_id', 'version'], 'awards_pk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('awards');
    }
}
