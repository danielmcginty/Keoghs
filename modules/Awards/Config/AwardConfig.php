<?php

namespace Modules\Awards\Config;

class AwardConfig
{
    public static $labels = [
        'title' => 'Title',
        'subtitle' => 'Subtitle',
        'logo' => 'Logo',
        'link' => 'Link',
        'status' => 'Status',
        'default_in_block' => 'Default in Block',
    ];

    public static $help = [
        'default_in_block' => 'Toggle whether or not this award appears by default in the Awards block',
        'excerpt'           => 'Set the excerpt to display if this award is the latest and displayed in the teal banner'
    ];

    public static $image_sizes = [
        'logo' => [
            'single' => [
                'label' => 'Primary',
                'width' => 500,
                'height' => 0,
                'primary' => true
            ],
        ]
    ];

    public static $sort_options = [
        'latest' => [
            'label' => 'Latest',
            'field' => 'created_at',
            'direction' => 'desc',
        ],
        'oldest' => [
            'label' => 'Oldest',
            'field' => 'created_at',
            'direction' => 'asc',
        ],
        'a-z' => [
            'label' => 'A-Z',
            'field' => 'title',
            'direction' => 'asc',
        ],
        'z-a' => [
            'label' => 'Z-A',
            'field' => 'title',
            'direction' => 'desc',
        ],
    ];
}