<?php

namespace Modules\Awards\Config;

use Adm\Interfaces\NavigationInterface;

class Navigation implements NavigationInterface
{
    public function getNavItems()
    {
        return [
            [
                'title' => 'Awards',
                'url' => route('adm.path', 'awards'),
                'position' => 65,
            ]
        ];
    }
}