<?php

namespace Modules\Awards\Adm\Models\PageBuilder;

use Adm\Crud\LaraCrud;
use Adm\Models\PageBuilder\PageBuilderBase;
use App\Models\Scopes\LanguageScope;
use Modules\Awards\Theme\Models\Award;

class Awards extends PageBuilderBase
{
    public $display_name = 'Awards';

    public $description = 'Add a slider of awards to the page';

    public $builder_fields = [
        'awards' => [
            'display_name' => 'Awards',
            'help' => 'Select the awards to display',
            'type' => 'orderable_multiple_select',
            'required' => false,
            'validation' => [],
            'options' => [],
            'group' => 'Main'
        ],
    ];

    public function callbackBeforePrepare(LaraCrud $crud)
    {
        $awards = Award::query()
            ->withoutGlobalScope(LanguageScope::class)
            ->where('language_id', $crud->language_id)
            ->get();

        foreach ($awards as $award) {
            $this->builder_fields['awards']['options'][$award->award_id] = $award->title;
        }
    }
}