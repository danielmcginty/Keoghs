<?php

namespace Modules\Awards\Adm\Controllers;

use Adm\Controllers\AdmController;
use Modules\Awards\Config\AwardConfig;

class ModuleController extends AdmController
{
    public function awards()
    {
        $this->crud->setTable('awards');
        $this->crud->setSubject('Awards');

        $this->crud->setDefaultValue('status', true);

        $this->crud->orderBy('award_date', 'desc');

        $this->crud->changeType('logo', 'asset');
        $this->crud->changeType('link', 'link');
        $this->crud->changeType('excerpt', 'textarea');

        $this->crud->columns('logo', 'title', 'subtitle', 'status', 'award_date');

        $this->crud->columnGroup('main', ['title', 'subtitle', 'excerpt', 'logo', 'link']);
        $this->crud->columnGroup('side', ['status', 'award_date', 'default_in_block']);
    }

    public function getAwardsDefaultLabels()
    {
        return AwardConfig::$labels;
    }

    public function getAwardsDefaultHelp()
    {
        return AwardConfig::$help;
    }

    public function getAwardsAssetConfig($field_name)
    {
        return AwardConfig::$image_sizes[$field_name] ?? [];
    }
}