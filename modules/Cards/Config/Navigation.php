<?php

namespace Modules\Cards\Config;

use Adm\Interfaces\NavigationInterface;

class Navigation implements NavigationInterface
{
    public function getNavItems()
    {
        return [
            [
                'title' => 'Cards',
                'url' => route('adm.path', 'cards'),
                'position' => 75,
            ]
        ];
    }
}