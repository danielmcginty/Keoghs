<?php

namespace Modules\Cards\Config;

class CardConfig
{
    /**
     * @var array
     */
    public static $labels = [
        'title' => 'Title',
        'image' => 'Image',
        'content' => 'Content',
        'buttons' => 'Buttons',
        'template' => 'Template',
        'status' => 'Status',
        'prelogo_text'  => 'Pre-logo content'
    ];

    /**
     * @var array
     */
    public static $help = [
        //
    ];

    /**
     * @var array
     */
    public static $image_sizes = [
        'image' => [
            'single' => [
                'label' => 'Primary',
                'width' => 640,
                'height' => 0,
                'primary' => false
            ]
        ],
        'logo' => [
            'single' => [
                'label' => 'Primary',
                'width' => 175,
                'height' => 0,
                'primary' => true
            ],
        ],
    ];

    public static $colour_options = [
        'card-orange'   => 'Orange',
        'card-green'    => 'Green',
        'card-red'      => 'Red',
        'card-blue'     => 'Blue',
        'card-purple'   => 'Purple',
        'pink'          => 'Pink',
    ];
}