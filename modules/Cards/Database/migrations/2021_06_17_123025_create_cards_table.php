<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->integer('card_id');
            $table->integer('language_id');
            $table->integer('version');
            $table->string('title');
            $table->text('main_content')->nullable();
            $table->string('image');
            $table->string('colour');
            $table->text('left_content')->nullable();
            $table->text('right_content')->nullable();
            $table->text('logos')->nullable();
            $table->text('buttons')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->primary(['card_id', 'language_id', 'version'], 'cards_pk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
