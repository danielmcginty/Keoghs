<?php

namespace Modules\Cards\Adm\Controllers;

use Adm\Controllers\AdmController;
use Modules\Cards\Config\CardConfig;

class ModuleController extends AdmController
{
    public function cards()
    {
        $this->crud->setTable('cards');
        $this->crud->setSubject('Cards');

        $this->crud->setDefaultValue('status', true);

        $this->crud->orderBy('created_at', 'desc');

        $this->crud->columns('title', 'colour', 'status', 'created_at');

        $this->crud->changeType('logos', 'repeatable');
        $this->crud->changeType('buttons', 'repeatable');
        $this->crud->changeType('colour', 'select');
        $this->crud->fieldOptions('colour', CardConfig::$colour_options);

        $this->crud->columnGroup(
            'main',
            ['title', 'image', 'main_content', 'prelogo_text', 'logos', 'left_content', 'right_content', 'buttons']
        );
        $this->crud->columnGroup('side', ['status', 'colour']);
    }

    public function getCardsDefaultLabels()
    {
        return CardConfig::$labels;
    }

    public function getCardsDefaultHelp()
    {
        return CardConfig::$help;
    }

    public function getCardsAssetConfig($field_name)
    {
        return CardConfig::$image_sizes[$field_name] ?? [];
    }

    public function getCardsRepeatableFieldFields($field)
    {
        switch ($field) {
            case 'logos':
                return [
                    'image' => [
                        'label' => 'Logo',
                        'type' => 'asset',
                        'multi_size' => true,
                        'sizes' => CardConfig::$image_sizes['logo']
                    ],
                ];
            case 'buttons':
                return [
                    'text' => [
                        'label' => 'Text',
                        'type' => 'string'
                    ],
                    'link' => [
                        'label' => 'Link',
                        'type' => 'link'
                    ]
                ];
            default:
                return [];
        }
    }
}