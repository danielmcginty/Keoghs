<?php

namespace Modules\Cards\Adm\Models\PageBuilder;

use Adm\Crud\LaraCrud;
use Adm\Models\PageBuilder\PageBuilderBase;
use App\Models\Scopes\LanguageScope;
use Modules\Cards\Theme\Models\Card;

class Cards extends PageBuilderBase
{
    public $display_name = 'Cards';

    public $description = 'Add a set of cards, in an accordion layout, to the page';

    public $builder_fields = [
        'cards' => [
            'display_name' => 'Cards',
            'help' => 'Add a set of cards to the block',
            'type' => 'orderable_multiple_select',
            'required' => false,
            'validation' => [],
            'group' => 'Main',
            'options' => [],
        ],
        'other_heading' => [
            'display_name'  => '\'Other\' Heading',
            'help'          => 'Set text to display between the first card and the rest',
            'type'          => 'string',
            'required'      => false,
            'validation'    => [],
            'group'         => 'Main'
        ]
    ];

    public function callbackBeforePrepare(LaraCrud $crud)
    {
        $cards = Card::query()
            ->withoutGlobalScope(LanguageScope::class)
            ->where('language_id', $crud->language_id)
            ->orderBy('title')
            ->get();

        foreach ($cards as $card) {
            $this->builder_fields['cards']['options'][$card->card_id] = $card->title;
        }
    }
}