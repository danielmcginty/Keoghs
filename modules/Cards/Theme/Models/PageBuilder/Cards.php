<?php

namespace Modules\Cards\Theme\Models\PageBuilder;

use App\Models\PageBuilder\BaseBlockModel;
use Illuminate\Support\Collection;
use Modules\Cards\Theme\Models\Card;

class Cards extends BaseBlockModel
{
    public function getValue($page_builder_block, $field, $original_value)
    {
        switch ($field) {
            case 'cards':
                $cards = new Collection();
                if (!empty($original_value) && is_json($original_value)) {
                    foreach ((array) json_decode($original_value, true) as $raw_card) {
                        if (empty($raw_card['value'])) {
                            continue;
                        }

                        $card = Card::where('card_id', $raw_card['value'])->first();
                        if ($card) {
                            $cards->push($card);
                        }
                    }
                }

                return $cards;
            default:
                return parent::getValue($page_builder_block, $field, $original_value);
        }
    }
}