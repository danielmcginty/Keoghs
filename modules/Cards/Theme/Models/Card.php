<?php

namespace Modules\Cards\Theme\Models;

use App\Helpers\AssetHelper;
use App\Helpers\BlockHelper;
use App\Models\CommonModel;
use App\Models\Scopes\LanguageScope;
use App\Models\Scopes\PublishedScope;
use App\Models\Traits\HasCompositePrimaryKey;
use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Cards\Config\CardConfig;

class Card extends CommonModel
{
    use SoftDeletes, HasCompositePrimaryKey, HasQueryBuilderCache;

    protected $table = 'cards';

    protected $primaryKey = ['card_id', 'language_id'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new LanguageScope);
        static::addGlobalScope(new PublishedScope);
    }

    public function getMainContentAttribute()
    {
        return $this->getAttributes()['main_content'];
    }

    public function getImageAttribute()
    {
        return (new AssetHelper())->getAssets(
            $this->table,
            $this->card_id,
            $this->language_id,
            $this->version,
            'image',
            CardConfig::$image_sizes['image']
        );
    }

    public function getLogosAttribute($raw_logos)
    {
        if (empty($raw_logos)) {
            return [];
        }

        $logos = [];

        foreach (json_decode($raw_logos, true) as $index => $logo) {
            $Logo = AssetHelper::getAssets(
                $this->table,
                $this->card_id,
                $this->language_id,
                $this->version,
                "logos.{$index}.image",
                CardConfig::$image_sizes['logo']
            );

            if( !empty($Logo) ){
                $logos[] = $Logo;
            }
        }

        return $logos;
    }

    public function getButtonsAttribute($value)
    {
        return BlockHelper::parseBlockButtonsField($value);
    }
}