<?php

namespace Modules\QuoteBlock\Adm\Models\PageBuilder;

use Adm\Models\PageBuilder\PageBuilderBase;

class Quote extends PageBuilderBase
{
    public $display_name = 'Quote';

    public $description = 'Add a quote to the page';

    public $builder_fields = [
        'quote' => [
            'display_name' => 'Quote',
            'help' => 'The quote to display',
            'type' => 'textarea',
            'required' => true,
            'validation' => [
                'required' => 'You need to provide a quote'
            ],
            'group' => 'Main'
        ],
        'background_image' => [
            'display_name' => 'Background Image',
            'help' => 'Select the background image to use',
            'type' => 'asset',
            'required' => true,
            'validation' => [
                'required' => 'You need to select an image'
            ],
            'group' => 'Main'
        ],
        'buttons' => [
            'display_name' => 'Buttons',
            'help' => 'Add some buttons to the block',
            'type' => 'repeatable',
            'required' => false,
            'validation' => [],
            'group' => 'Main'
        ],
    ];

    public $column_extras = [
        'background_image' => [
            'multi_size'    => true,
            'sizes'         => [
                'single'    => [
                    'label'     => 'Image',
                    'width'     => 1920,
                    'height'    => 0,
                    'primary'   => true
                ]
            ]
        ],
        'buttons' => [
            'repeatable_fields' => [
                'text' => [
                    'label' => 'Text',
                    'type' => 'string',
                ],
                'link' => [
                    'label' => 'Link',
                    'type' => 'link',
                ],
            ]
        ],
    ];
}