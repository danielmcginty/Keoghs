<?php

namespace Modules\QuoteBlock\Theme\Models\PageBuilder;

use App\Helpers\AssetHelper;
use App\Helpers\BlockHelper;
use App\Models\PageBuilder\BaseBlockModel;
use Modules\QuoteBlock\Adm\Models\PageBuilder\Quote as AdmQuote;

class Quote extends BaseBlockModel
{
    public function getValue($page_builder_block, $field, $original_value)
    {
        switch ($field) {
            case 'background_image':
                $AdmBanner = new AdmQuote();

                return AssetHelper::getAssets(
                    $page_builder_block->table_name,
                    $page_builder_block->table_key,
                    $page_builder_block->language_id,
                    $page_builder_block->version,
                    'page_builder.'.$page_builder_block->sort_order.'.content.'.$field,
                    $AdmBanner->column_extras[$field]['sizes']
                );

            case 'buttons':
                return BlockHelper::parseBlockButtonsField($original_value);

            default:
                return parent::getValue($page_builder_block, $field, $original_value);
        }
    }
}