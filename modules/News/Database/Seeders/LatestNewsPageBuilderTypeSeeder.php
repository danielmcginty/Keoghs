<?php
namespace Modules\News\Database\Seeders;

use Illuminate\Database\Seeder;

class LatestNewsPageBuilderTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table( 'page_builder_block_types' )->insert([
            'reference' => 'latest_news',
            'editable' => 1,
            'deletable' => 1
        ]);
    }
}
