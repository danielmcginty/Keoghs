<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AmendNewsTableAddContentField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'news_articles', function( Blueprint $table ){
            $table->text( 'content' )->after( 'excerpt' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'news_articles', function( Blueprint $table ){
            $table->dropColumn( 'content' );
        });
    }
}
