<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_articles', function ( Blueprint $table ) {
            $table->integer('news_article_id');
            $table->integer('language_id');
            $table->integer('news_category_id');
            $table->integer('news_author_id')->default(0);
            $table->string('title');
            $table->string('h1_title')->nullable();
            $table->text('image')->nullable();
            $table->text('excerpt');
            $table->date('article_date')->nullable();
            $table->integer('sort_order')->default(0);
            $table->boolean('status')->default(0);
            $table->index('news_category_id');
            $table->index('news_author_id');
            $table->timestamps();
            $table->softDeletes();
            $table->primary(['news_article_id', 'language_id']);
        });

        Schema::create('news_categories', function( Blueprint $table ) {
            $table->integer('news_category_id');
            $table->integer('language_id');
            $table->string('title');
            $table->string('h1_title')->nullable();
            $table->text('image')->nullable();
            $table->text('description')->nullable();
            $table->integer('sort_order')->default(0);
            $table->boolean('status')->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->primary(['news_category_id', 'language_id']);
        });

        Schema::create('news_article_to_news_category', function( Blueprint $table ){
            $table->integer('news_article_id');
            $table->integer('news_category_id');
            $table->primary(['news_article_id', 'news_category_id'], 'n_art_to_n_cat');
        });

        Schema::create('news_authors', function( Blueprint $table ){
            $table->increments('news_author_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->text('image')->nullable();
            $table->text('bio')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('news_articles');
        Schema::drop('news_categories');
        Schema::drop('news_article_to_news_category');
        Schema::drop('news_authors');
    }
}
