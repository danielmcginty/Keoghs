<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AmendNewsTablesForContentVersioning extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'news_articles', function( Blueprint $table ){
            $table->integer( 'version' )->after( 'language_id' )->default( 1 );
            $table->dropPrimary( 'news_articles_news_article_id_language_id_primary' );
            $table->primary( ['news_article_id', 'language_id', 'version'], 'news_articles_primary_key' );
        });

        Schema::table( 'news_categories', function( Blueprint $table ){
            $table->integer( 'version' )->after( 'language_id' )->default( 1 );
            $table->dropPrimary( 'news_categories_news_category_id_language_id_primary' );
            $table->primary( ['news_category_id', 'language_id', 'version'], 'news_categories_primary_key' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'news_articles', function( Blueprint $table ){
            $table->dropColumn( 'version' );
            $table->dropPrimary( 'news_articles_primary_key' );
            $table->primary( ['news_article_id', 'language_id'] );
        });

        Schema::table( 'news_categories', function( Blueprint $table ){
            $table->dropColumn( 'version' );
            $table->dropPrimary( 'news_categories_primary_key' );
            $table->primary( ['news_category_id', 'language_id'] );
        });
    }
}
