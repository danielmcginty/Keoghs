<?php
namespace Modules\News\Adm\Models\PageBuilder;

use Adm\Crud\LaraCrud;
use Adm\Models\PageBuilder\PageBuilderBase;
use App\Models\Scopes\LanguageScope;
use Modules\News\Theme\Models\NewsArticle;

class NewsListing extends PageBuilderBase {

    public $display_name    = 'News Listing';
    public $description     = 'Add news listings to the page';

    public $builder_fields  = [
        'heading'   => [
            'display_name'  => 'Heading',
            'help'          => 'Add a heading to the block, defaulting to \'Latest News\' if left empty',
            'type'          => 'string',
            'required'      => false,
            'validation'    => [],
            'group'         => 'Main'
        ],
        'articles'  => [
            'display_name'  => 'News Articles',
            'help'          => 'Select the articles to display, or leave empty to show the latest 3',
            'type'          => 'orderable_multiple_select',
            'options'       => [],
            'required'      => false,
            'validation'    => [],
            'group'         => 'Main'
        ]
    ];

    public function callbackBeforePrepare( LaraCrud $crud ){
        $opts = [];
        foreach( NewsArticle::withoutGlobalScope( LanguageScope::class )->where( 'language_id', $crud->language_id )->orderBy( 'title' )->get() as $Article ){
            $opts[ $Article->news_article_id ] = $Article->title;
        }
        $this->builder_fields['articles']['options'] = $opts;
    }
}