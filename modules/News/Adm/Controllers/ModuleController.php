<?php
namespace Modules\News\Adm\Controllers;

use Adm\Controllers\AdmController;
use App\Models\Url;
use Modules\News\Config\NewsArticleConfig;
use Modules\News\Config\NewsAuthorConfig;
use Modules\News\Config\NewsCategoryConfig;
use Modules\Page\Config\PageConfig;

class ModuleController extends AdmController {

    /**
     * CRUD Functionality for News Articles
     */
    public function news(){
        $this->crud->setTable( 'news_articles' );

        $this->crud->setPathPrefix('news_categories', 'news_category_id', 'news_category_id');
        $this->crud->metaDescriptionGenerationField( 'excerpt' );

        $this->crud->addColumn('path', 'title', 'side');
        $this->crud->addColumn('news_categories', 'news_category_id');

        $this->crud->setRelation('news_category_id', 'news_categories', 'title', 'news_category_id', '');
        $this->crud->multiRelation('news_categories', 'news_article_to_news_category', 'news_categories', 'news_article_id', 'news_category_id', 'title');
        $this->crud->setRelation('news_author_id', 'news_authors', ['first_name', 'last_name'], 'news_author_id', 0 );

        $this->crud->changeType('excerpt', 'textarea');

        $this->crud->addColumn('page_builder', 'image');
        $this->crud->changeType('page_builder', 'page_builder');
        $this->crud->enablePageBuilder();

        $this->crud->columns( 'title', 'path', 'news_category_id', 'news_author_id', 'article_date', 'status' );

        $this->crud->columnGroup( 'main', array( 'title', 'path', 'h1_title', 'news_category_id', 'news_categories', 'image', 'excerpt', 'content' ));
        $this->crud->columnGroup( 'side', array( 'status', 'news_author_id', 'article_date' ) );

        $this->crud->excludeFromEdit( ['sort_order'] );

        if( isset( $this->crud->parameters['action'] ) && $this->crud->parameters['action'] == 'create' ){
            $this->crud->extra_view_data['title_override'] = 'Add News Article';
        }
    }

    /**
     * CRUD Functionality for the News Archive Page
     */
    public function news_archive(){
        $this->crud->setTable('pages');
        $this->crud->setSubject('News Archive Page');
        $this->crud->multiLingualSingle( 5 );

        $this->crud->displayColumn('title');

        $this->crud->addColumn('path', '', 'side');
        $this->crud->systemPageEnabled = true;

        $this->crud->addColumn('page_builder', 'image');
        $this->crud->changeType('page_builder', 'page_builder');
        $this->crud->enablePageBuilder();

        $this->crud->columns( 'title', 'path', 'parent', 'status' );

        $this->crud->columnGroup( 'main', array( 'title', 'path', 'h1_title', 'image', 'page_builder' ) );
        $this->crud->columnGroup( 'side', array( 'status' ) );

        $this->crud->excludeFromEdit( ['sort_order'] );

        // And stop the page's parent from being changed
        $this->crud->excludeFromEdit( ['parent'] );
    }

    /**
     * CRUD Functionality for News Categories
     */
    public function news_categories(){
        $this->crud->setTable( 'news_categories' );

        $this->crud->addColumn('path', 'title', 'main');

        $this->crud->columns( 'title', 'path', 'status' );

        $SystemPage = Url::getSystemPage('news', 'index');
        if( $SystemPage ){
            $this->crud->setBasePathPrefix('pages', 5, 'page_id');
        }

        $this->crud->addColumn('page_builder', 'image');
        $this->crud->changeType('page_builder', 'page_builder');
        $this->crud->enablePageBuilder();

        $this->crud->columnGroup( 'main', array( 'title', 'path', 'h1_title', 'image', 'page_builder' ) );

        $this->crud->excludeFromEdit( ['description'] );
    }

    /**
     * CRUD Functionality for News Authors
     */
    public function news_authors(){
        $this->crud->setTable( 'news_authors' );
        $this->crud->linkedField( 'first_name' );

        $this->crud->columns( 'first_name', 'last_name', 'image' );
    }

    /**
     * Callback function used to get field labels for News Articles
     * @return array
     */
    public function getNewsDefaultLabels(){
        return NewsArticleConfig::$labels;
    }

    /**
     * Callback function used to get field labels for the News Archive Page
     * @return array
     */
    public function getNewsArchiveDefaultLabels(){
        return PageConfig::$labels;
    }

    /**
     * Callback function used to get field labels for News Categories
     * @return array
     */
    public function getNewsCategoriesDefaultLabels(){
        return NewsCategoryConfig::$labels;
    }

    /**
     * Callback function used to get field labels for News Authors
     * @return array
     */
    public function getNewsAuthorsDefaultLabels(){
        return NewsAuthorConfig::$labels;
    }

    /**
     * Callback function used to get field help text for News Articles
     * @return array
     */
    public function getNewsDefaultHelp(){
        return NewsArticleConfig::$help;
    }

    /**
     * Callback function used to get field help text for the News Archive Page
     * @return array
     */
    public function getNewsArchiveDefaultHelp(){
        return PageConfig::$help;
    }

    /**
     * Callback function used to get field help text for News Categories
     * @return array
     */
    public function getNewsCategoriesDefaultHelp(){
        return NewsCategoryConfig::$help;
    }

    /**
     * Callback function used to get field help text for News Authors
     * @return array
     */
    public function getNewsAuthorsDefaultHelp(){
        return NewsAuthorConfig::$help;
    }

    /**
     * Callback function used to get asset field image sizes for News Articles
     *
     * @param $field_name
     * @return array
     */
    public function getNewsAssetConfig( $field_name ){
        return isset( NewsArticleConfig::$image_sizes[ $field_name ] ) ? NewsArticleConfig::$image_sizes[ $field_name ] : [];
    }

    /**
     * Callback function used to get asset field image sizes for the News Archive page
     *
     * @param $field_name
     * @return array
     */
    public function getNewsArchiveAssetConfig( $field_name ){
        return isset( PageConfig::$image_sizes[ $field_name ] ) ? PageConfig::$image_sizes[ $field_name ] : [];
    }

    /**
     * Callback function used to get asset field image sizes for News Categories
     *
     * @param $field_name
     * @return array
     */
    public function getNewsCategoriesAssetConfig( $field_name ){
        return isset( NewsCategoryConfig::$image_sizes[ $field_name ] ) ? NewsCategoryConfig::$image_sizes[ $field_name ] : [];
    }

    /**
     * Callback function used to get asset field image sizes for News Authors
     *
     * @param $field_name
     * @return array
     */
    public function getNewsAuthorsAssetConfig( $field_name ){
        return isset( NewsAuthorConfig::$image_sizes[ $field_name ] ) ? NewsAuthorConfig::$image_sizes[ $field_name ] : [];
    }
}