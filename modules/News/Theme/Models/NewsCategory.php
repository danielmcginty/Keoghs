<?php
namespace Modules\News\Theme\Models;

use App\Helpers\AssetHelper;
use App\Models\CommonModel as Model;
use App\Models\Traits\HasCompositePrimaryKey;
use App\Models\Scopes\LanguageScope;
use App\Models\Scopes\PublishedScope;
use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\News\Config\NewsCategoryConfig;

class NewsCategory extends Model {
    // As we've got a deleted_at flag, a composite primary key, and want to cache DB calls, include the relevant traits
    use SoftDeletes, HasCompositePrimaryKey, HasQueryBuilderCache;

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'news_categories';

    /**
     * The primary key in the table
     * @var array
     */
    protected $primaryKey = ['news_category_id', 'language_id'];

    protected $appends = ['meta_title', 'meta_description', 'no_index'];

    /**
     * When 'booting' the model, we only want to include categories that are published and in the correct language,
     * so add those as global scopes
     */
    protected static function boot(){
        parent::boot();

        static::addGlobalScope(new LanguageScope);
        static::addGlobalScope(new PublishedScope);
    }

    /**
     * Eloquent Relationship.
     * Returns the articles that belong to this 'main' category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articles(){
        return $this->hasMany('\\Modules\\News\\Theme\\Models\\NewsArticle', 'news_category_id', 'news_category_id');
    }

    /**
     * Eloquent Relationship.
     * Returns the articles that belong to this as an 'other' category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function other_articles(){
        return $this->belongsToMany( '\\Modules\\News\\Theme\\Models\\NewsArticle', 'news_article_to_news_category', 'news_category_id', 'news_article_id' );
    }

    /**
     * Simple Accessor function allowing us to pull out ALL articles assigned to this category.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getAllArticlesAttribute(){
        $used_ids = [];
        $articles = [];

        foreach( $this->articles as $article ){
            if( !in_array( $article->news_article_id, $used_ids ) ){
                $articles[] = $article;
                $used_ids[] = $article->news_article_id;
            }
        }

        foreach( $this->other_articles as $article ){
            if( !in_array( $article->news_article_id, $used_ids ) ){
                $articles[] = $article;
                $used_ids[] = $article->news_article_id;
            }
        }

        return collect( $articles );
    }

    /**
     * Return the set of images assigned to the article in an easy to use array.
     *
     * @return array
     */
    public function getImageAttribute(){
        return AssetHelper::getAssets( $this->table, $this->news_category_id, $this->language_id, $this->version, 'image', NewsCategoryConfig::$image_sizes['image'] );
    }

    /**
     * Key Name Override.
     * As we're performing a N to N relationship, we need to tell Laravel what the correct key is (because we're using
     * a composite key)
     *
     * @return string
     */
    public function getKeyName(){
        return 'news_category_id';
    }
}