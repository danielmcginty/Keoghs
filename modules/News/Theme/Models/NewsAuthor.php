<?php
namespace Modules\News\Theme\Models;

use App\Helpers\AssetHelper;
use App\Models\CommonModel;
use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\News\Config\NewsAuthorConfig;

class NewsAuthor extends CommonModel {
    // As we've got a deleted_at flag and want to cache DB calls, include the relevant traits
    use SoftDeletes, HasQueryBuilderCache;

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'news_authors';

    /**
     * The primary keys in the table
     * @var string
     */
    protected $primaryKey = 'news_author_id';

    protected $appends = ['name'];

    /**
     * Eloquent Relationship.
     * Returns the news articles written by this author.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articles(){
        return $this->hasMany( '\\Modules\\News\\Theme\\Models\\NewsArticle', 'news_author_id', 'news_author_id' )->with('category');
    }

    /**
     * Custom Accessor Function
     * Allows us to access the full author name where required.
     *
     * @return string
     */
    public function getNameAttribute(){
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Custom Accessor Function
     * Allows us to return a usable image for the news author, instead of returning the image ID.
     *
     * @return array
     */
    public function getImageAttribute(){
        $AssetHelper = new AssetHelper();
        return $AssetHelper->getAssets( $this->table, $this->news_author_id, 1, 0, 'image', NewsAuthorConfig::$image_sizes['image'] );
    }
}