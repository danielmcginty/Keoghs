<?php
namespace Modules\News\Theme\Models\PageBuilder;

use App\Models\PageBuilder\BaseBlockModel;
use Illuminate\Database\Eloquent\Collection;
use Modules\News\Theme\Models\NewsArticle;

class NewsListing extends BaseBlockModel {

    public function getValue( $page_builder_block, $field, $original_value ){
        switch( $field ){
            case 'articles':
                $Articles = new Collection();
                if( !empty( $original_value ) && is_json( $original_value ) ){
                    foreach( (array)json_decode( $original_value, true ) as $raw_article ){
                        if( empty( $raw_article['value'] ) ){ continue; }

                        $Article = NewsArticle::where( 'news_article_id', $raw_article['value'] )->first();
                        if( $Article ){
                            $Articles->push( $Article );
                        }
                    }
                }

                if( $Articles->isEmpty() ){
                    $Articles = NewsArticle::orderBy( 'article_date', 'desc' )->limit( 3 )->get();
                }

                return $Articles;
            case 'heading':
                return !empty( $original_value ) ? $original_value : 'Latest News';
            default:
                return parent::getValue( $page_builder_block, $field, $original_value );
        }
    }

}