<?php
namespace Modules\News\Theme\Models;

use App\Models\CommonModel;
use App\Models\Traits\HasCompositePrimaryKey;
use App\Models\Scopes\LanguageScope;
use App\Models\Scopes\PublishedScope;
use App\Helpers\AssetHelper;
use App\Models\Traits\HasQueryBuilderCache;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\News\Config\NewsArticleConfig;

class NewsArticle extends CommonModel {
    // As we've got a deleted_at flag, a composite primary key, and want to cache DB calls, include the relevant traits
    use SoftDeletes, HasCompositePrimaryKey, HasQueryBuilderCache;

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'news_articles';

    /**
     * The primary keys in the table
     * @var array
     */
    protected $primaryKey = ['news_article_id', 'language_id'];

    protected $appends = [ 'meta_title', 'meta_description', 'no_index', 'all_categories', 'article_author' ];

    /**
     * When 'booting' this model, we only want to include articles that are published and in the correct language,
     * so add those as global scopes
     */
    protected static function boot(){
        parent::boot();

        static::addGlobalScope(new LanguageScope);
        static::addGlobalScope(new PublishedScope);
    }

    /**
     * Eloquent Relationship.
     * Returns the main category this article is assigned to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category(){
        return $this->belongsTo('\\Modules\\News\\Theme\\Models\\NewsCategory', 'news_category_id', 'news_category_id');
    }

    /**
     * Eloquent Relationship.
     * Returns the other categories this article is assigned to (other than 'Main Category')
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function other_categories(){
        return $this->belongsToMany( '\\Modules\\News\\Theme\\Models\\NewsCategory', 'news_article_to_news_category', 'news_article_id', 'news_category_id' );
    }

    /**
     * Simple Accessor Function allowing us to pull out all categories the news article is assigned to.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getAllCategoriesAttribute(){
        $used_ids = [];
        $categories = [];

        if( $this->category ) {
            $categories[] = $this->category;
            $used_ids[] = $this->category->news_category_id;
        }


        foreach( $this->other_categories as $other_category ){
            if( !in_array( $other_category->news_category_id, $used_ids ) ) {
                $categories[] = $other_category;
                $used_ids[] = $other_category->news_category_id;
            }
        }

        return collect( $categories );
    }

    /**
     * Eloquent Relationship.
     * Returns the author who wrote the article.
     *
     * @return mixed
     */
    public function author(){
        return $this->belongsTo('\\Modules\\News\\Theme\\Models\\NewsAuthor', 'news_author_id', 'news_author_id');
    }

    public function getArticleAuthorAttribute(){
        return $this->author;
    }

    /**
     * Simple query scope.
     * Allows us to query the table against news categories in a central location.
     *
     * @param $query
     * @param $category_id
     * @return mixed
     */
    public function scopeInCategory( $query, $category_id ){
        $query->where( $this->table . '.news_category_id', $category_id );

        $query->leftJoin( 'news_article_to_news_category', 'news_article_to_news_category.news_article_id', '=', $this->table . '.news_article_id' );
        $query->orWhere( 'news_article_to_news_category.news_category_id', '=', $category_id );

        return $query;
    }

    /**
     * Simple query scope.
     * Allows us to query the table against a search term in a central location.
     *
     * @param $query
     * @param $search_term
     * @return mixed
     */
    public function scopeMatchingSearchTerm( $query, $search_term ){
        $query->distinct();
        $query->select( $this->table . '.*' );
        $query->join( 'page_builder_blocks', 'page_builder_blocks.table_key', '=', $this->table . '.news_article_id' );
        $query->join( 'page_builder_block_content', 'page_builder_block_content.page_builder_block_id', '=', 'page_builder_blocks.id' );
        $query->where( 'page_builder_blocks.table_name', '=', $this->table );
        $query->where( function( $sub_query ) use ( $search_term ) {
            return $sub_query->where( 'page_builder_block_content.value', 'LIKE', '%' . $search_term . '%' )
                ->orWhere( $this->table . '.title', 'LIKE', '%' . $search_term . '%' )
                ->orWhere( $this->table . '.h1_title', 'LIKE', '%' . $search_term . '%' )
                ->orWhere( $this->table . '.excerpt', 'LIKE', '%' . $search_term . '%' );
        });

        return $query;
    }

    /**
     * Custom Mutator Function.
     * When accessing the ->article_date property, return it in a nice format.
     *
     * @param $value
     * @return static
     */
    public function getArticleDateAttribute($value) {
        return Carbon::createFromFormat('Y-m-d', $value);
    }

    /**
     * Return the set of images assigned to the article in an easy to use array.
     *
     * @return array
     */
    public function getImageAttribute(){
        $AssetHelper = new AssetHelper();
        $Image_Sizes = NewsArticleConfig::$image_sizes;
        $Image_Sizes['image']['schema'] = [
            'label' => 'Schema Image',
            'width' => 696,
            'height' => 392,
            'primary' => false
        ];
        $Image_Sizes['image']['amp'] = [
            'label' => 'AMP',
            'width' => 640,
            'height' => 350,
            'primary' => false
        ];
        return $AssetHelper->getAssets( $this->table, $this->news_article_id, $this->language_id, $this->version, 'image', $Image_Sizes['image'] );
    }

    /**
     * Simple Accessor Function allowing us to return the 'previous' news article.
     *
     * @return NewsArticle|bool
     */
    public function getPreviousArticleAttribute(){
        $previous_article = self::where([
            ['article_date', '=', date("Y-m-d", strtotime($this->article_date))],
            ['news_article_id', '<', $this->news_article_id]
        ])->orWhere([
            ['article_date', '<', date("Y-m-d", strtotime($this->article_date))],
            ['news_article_id', '<>', $this->news_article_id]
        ])->orderBy('article_date', 'desc')->orderBy( 'news_article_id', 'desc' )->first();

        if( $previous_article ){
            return $previous_article;
        }

        return false;
    }

    /**
     * Simple Accessor Function allowing us to return the 'next' news article.
     *
     * @return NewsArticle|bool
     */
    public function getNextArticleAttribute(){
        $next_article = self::where([
            ['article_date', '=', date("Y-m-d", strtotime($this->article_date))],
            ['news_article_id', '>', $this->news_article_id]
        ])->orWhere([
            ['article_date', '>', date("Y-m-d", strtotime($this->article_date))],
            ['news_article_id', '<>', $this->news_article_id]
        ])->orderBy('article_date', 'asc')->orderBy( 'news_article_id', 'asc' )->first();

        if( $next_article ){
            return $next_article;
        }

        return false;
    }

    /**
     * Custom Accessor Function letting us pull out a shortened version of the excerpt where required.
     * Example Usage: Latest News Page Builder Block.
     *
     * @return string
     */
    public function getShortExcerptAttribute(){
        $short_excerpt_limit = 200;
        $shortened = trim( substr( $this->excerpt, 0, $short_excerpt_limit ) );
        if( strlen( $this->excerpt ) > $short_excerpt_limit ){
            $shortened .= '...';
        }

        return $shortened;
    }

    /**
     * Key Name Override.
     * As we're performing a N to N relationship, we need to tell Laravel what the correct key is (because we're using
     * a composite key)
     *
     * @return string
     */
    public function getKeyName(){
        return 'news_article_id';
    }
}