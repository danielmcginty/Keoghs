<?php
namespace Modules\News\Theme\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Url;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Input;
use Modules\News\Theme\Models\NewsCategory;
use Modules\News\Theme\Models\NewsArticle;

class NewsController extends Controller {

    /**
     * How many news articles do we want to display per page in the archive & categories?
     *
     * @var int
     */
    private $articles_per_page = 6;

    /**
     * The 'index' function for news.
     * Renders the news archive page
     */
    public function index(){
        $this->category( 0 );
    }

    /**
     * Renders a news category page, based on the Category ID provided.
     *
     * @param $id
     */
    public function category( $id ){
        if( $id === 0 ){
            $this_category = Url::getSystemPage('news', 'index');
        } else {
            $this_category = NewsCategory::where('news_category_id', $id)->first();
        }

        if( $this_category ){
            $home = Url::getSystemPage('home', 'index');
            if( $home ) {
                $this->addBreadcrumb(route('theme', ['url' => $home->url]), $home->getAttributes()['title']);
            }

            $news_page = Url::getSystemPage('news', 'index');
            $this->addBreadcrumb(route('theme', [$news_page->url]), $news_page->title);

            if( $id !== 0 ) {
                foreach ($this->getBreadcrumbs($this_category, 'title', 'category') as $breadcrumb) {
                    $this->addBreadcrumb(route('theme', ['url' => $breadcrumb['url']]), $breadcrumb['label']);
                }
            }

            $this_category->content = $id === 0 ? '' : $this_category->description;

            $this->pageTitle = $this_category->title;
            $this->this_page = $this_category;

            $this->news_archive_page = Url::getSystemPage('news', 'index');
            if( $this->request->input('search_term') ){
                $this->search_term = $this->request->input('search_term');

                // If we have a search term, add another breadcrumb
                $this->addBreadcrumb( '#', 'Search: \'' . $this->search_term . '\'' );
            }

            $this->news_categories = $this->_getSidebarCategories();
            if( $id !== 0 ) {
                $this->news_articles = $this->_getArchiveArticles($this_category->news_category_id);
            } else {
                $this->news_articles = $this->_getArchiveArticles();
            }

            $this->news_articles->setPath( route('theme', $this_category->url) );
            $this->news_articles->appends( request()->except( 'page' ) );

            $this->view = 'news.archive';
        }
    }

    /**
     * Renders a news article page, based on the Article ID provided.
     *
     * @param $id
     */
    public function article( $id ){
        $this_page = NewsArticle::where('news_article_id', $id)->first();

        if( $this_page ){
            $home = Url::getSystemPage('home', 'index');
            if( $home ) {
                $this->addBreadcrumb(route('theme', ['url' => $home->url]), $home->getAttributes()['title']);
            }

            $news_page = Url::getSystemPage('news', 'index');
            $this->addBreadcrumb(route('theme', [$news_page->url]), $news_page->title);

            foreach( $this->getBreadcrumbs( $this_page->category, 'title', 'category' ) as $breadcrumb ){
                $this->addBreadcrumb( route('theme', ['url' => $breadcrumb['url']]), $breadcrumb['label'] );
            }

            $this->addBreadcrumb(route('theme', [$this_page->url]), $this_page->title);

            if( $this->amp_page ){
                $this->setCanonicalURL( route('theme', [$this_page->url]) );
            } else {
                $this->setAMPHtmlURL( route('amp', [$this_page->url]) );
            }

            $this->canonical_url = route('theme', $this_page->url);
            $this->articleTitle = $this_page->title;
            $this->this_page = $this_page;

            if( !empty( $this_page->image ) && !empty( $this_page->image['article'] ) ){
                $this->addSocialMeta( 'og:image', $this_page->image['article']['src'] );
                $this->addSocialMeta( 'og:image:width', $this_page->image['article']['width'] );
                $this->addSocialMeta( 'og:image:height', $this_page->image['article']['height'] );

                $this->addSocialMeta( 'twitter:image', $this_page->image['article']['src'] );
            }

            $this->news_categories = $this->_getSidebarCategories();

            $this->news_archive_page = Url::getSystemPage('news', 'index');
            if( $this->request->input('search_term') ){
                $this->search_term = $this->request->input('search_term');
            }

            $this->view = 'news.article';
        }
    }

    /**
     * Loads the news categories to display in the sidebar on archive & article pages.
     *
     * @return mixed
     */
    private function _getSidebarCategories(){
        $news_categories = NewsCategory::get();

        // This will loop through and remove the category if there are no articles associated with it
        foreach( $news_categories as $key => $category ){
            if( $category->all_articles->isEmpty()  ){
                unset( $news_categories[ $key ] );
            }
        }

        return $news_categories;
    }

    /**
     * Common Functionality to show news articles on either the archive page, category page OR news search page.
     *
     * @param bool|false $category_id
     * @return LengthAwarePaginator
     */
    private function _getArchiveArticles( $category_id = false ){
        $articles_qry = NewsArticle::distinct()->select( 'news_articles.*' );
        if( $category_id ){
            $articles_qry->inCategory( $category_id );
        }

        if( $this->request->input('search_term') ){
            $articles_qry->matchingSearchTerm( $this->request->input( 'search_term' ) );
        }

        $articles_qry->orderBy('news_articles.article_date', 'desc');
        $articles_qry->orderBy('news_articles.news_article_id', 'desc');

        $all_articles = $articles_qry->get();

        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        $results_to_display = $all_articles->slice( ($currentPage-1) * $this->articles_per_page, $this->articles_per_page );

        return new LengthAwarePaginator( $results_to_display, $all_articles->count(), $this->articles_per_page );
    }
}