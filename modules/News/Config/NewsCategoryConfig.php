<?php
namespace Modules\News\Config;

class NewsCategoryConfig {
    /**
     * Stores input labels
     * @var array
     */
    public static $labels = [];

    /**
     * Stores input help text
     * @var array
     */
    public static $help = [
        'description' => 'The text content to display when viewing this category'
    ];

    /**
     * Stores a shared configuration for image sizes
     * @var array
     */
    public static $image_sizes = [
        'image' => [
            'desktop' => [
                'label' => 'Desktop',
                'width' => 1920,
                'height' => 400,
                'primary' => true
            ],
            'mobile' => [
                'label' => 'Mobile',
                'width' => 640,
                'height' => 300,
                'primary' => false
            ]
        ]
    ];
}