<?php
namespace Modules\News\Config;

class NewsArticleConfig {
    /**
     * Stores input labels
     * @var array
     */
    public static $labels = [
        'news_category_id' => 'Main Category',
        'news_author_id' => 'Author',
        'status' => 'Published?',
        'excerpt' => 'Article Excerpt'
    ];

    /**
     * Stores input help text
     * @var array
     */
    public static $help = [
        'excerpt' => 'The Excerpt to show on the news archive page.',
        'h1_title' => 'If set, this field will be used to display the article title.',
        'publish_date' => 'If set, the article will be published on the date provided.',
        'content' => 'The main content for the article'
    ];

    /**
     * Stores a shared configuration for image sizes
     * @var array
     */
    public static $image_sizes = [
        'image' => [
            'article' => [
                'label' => 'Banner',
                'width' => 1920,
                'height' => 470,
                'primary' => true
            ],
            'listing' => [
                'label' => 'Listing',
                'width' => 960,
                'height' => 0,
                'primary' => false
            ]
        ]
    ];
}