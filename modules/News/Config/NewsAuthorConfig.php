<?php
namespace Modules\News\Config;

class NewsAuthorConfig {
    /**
     * Stores input labels
     * @var array
     */
    public static $labels = [];

    /**
     * Stores input help text
     * @var array
     */
    public static $help = [
        'first_name' => 'The author\'s first name',
        'last_name' => 'The author\'s last name',
        'image' => 'A \'profile\' picture to display for this author',
        'bio' => 'A short description, or bio, about the author'
    ];

    /**
     * Stores image sizes for asset fields
     * @var array
     */
    public static $image_sizes = [
        'image' => [
            'single' => [
                'label' => 'Profile Picture',
                'width' => 150,
                'height' => 150,
                'primary' => true
            ]
        ]
    ];
}