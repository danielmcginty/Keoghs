<?php
namespace Modules\News\Config;

use Adm\Interfaces\NavigationInterface;

/**
 * Class Navigation
 * @package Modules\News\Config
 *
 * The CMS Navigation Builder for the News Module
 */
class Navigation implements NavigationInterface {
    /**
     * Returns the CMS Navigation links for the module
     *
     * @return array
     */
    public function getNavItems(){
        return array(
            array( 'title' => 'News', 'url' => route('adm.path', ['news']), 'position' => 2, 'sub' => array(
                array( 'title' => 'Archive', 'url' => route('adm.path', ['news-archive']), 'position' => 0 ),
                array( 'title' => 'Articles', 'url' => route('adm.path', ['news']), 'position' => 1 ),
                array( 'title' => 'Categories', 'url' => route('adm.path', ['news-categories']), 'position' => 2 ),
                array( 'title' => 'Authors', 'url' => route('adm.path', ['news-authors']), 'position' => 3 )
            ))
        );
    }
}