
// !ADMIN SPECIFIC CONFIG! (See /resources/theme/gulp for front end config)

var pngcrush = require('imagemin-pngcrush');
var pngquant = require('imagemin-pngquant');

var src = './assets';
var dist = './../../public/admin/dist';
var distAssets = dist + '/assets';

module.exports = {
    paths: {
        src: src,
        dest: dist
    },

    copyAssets: {
        images: {
            src: src + '/ui/**/*.{jpg,png,gif,ico,svg,cur}',
            dest: distAssets + '/ui'
        },
        plugins: {
            src: src + '/plugins/**/*',
            dest: distAssets + '/plugins'
        }
    },

    optimiseAssets: {
        src: distAssets + '/ui/**/*.{jpg,png,gif,svg}', // the distAssets isn't a mistake! We only want to compress things in dist
        dest: distAssets + '/ui',
        options: {
            progressive: true
            //svgoPlugins: [{removeViewBox: false}],
            //use: [pngcrush(), pngquant()]
        }
    },

    favicons: {
        src: src + '/favicons/**/*.{png,ico,svg,xml,json}',
        dest: distAssets + '/favicons'
    },

    styles: {
        src: src + '/scss/**/*.scss',
        dest: distAssets + '/css',
        settings: {
            includePaths: ['./bower_components'],  // so the compiler knows to look for scss files within the bower directory as well
            outputStyle: 'expanded', // nested, expanded, compact, compressed
            sourceComments: 'none', // 'none', 'normal' or 'map'
            errLogToConsole: true
        },
        pleaseOptions: {
            production: {
                //production use rems if < ie8 is not supported
                autoprefixer: {"browsers": ['>1%', 'last 3 versions', 'ie >= 8']},
                filters: {oldIE: true},
                rem: ["16px", {replace: true, atrules: true}],
                pseudoElements: true,
                opacity: true,
                import: true,
                minifier: {preserveHacks: true, removeAllComments: true},
                mqpacker: true,
                sourcemaps: false,
                next: false
            },
            development: {
                //development use px so we dont have to think about rem values
                autoprefixer: {"browsers": ['>1%', 'last 3 versions', 'ie >= 8']},
                filters: false,
                rem: ["16px", {replace: true, atrules: true}],
                pseudoElements: false,
                opacity: false,
                import: false,
                minifier: false,
                mqpacker: false,
                sourcemaps: false,
                next: false
            }
        },
        bless: {
            src: distAssets + '/css/*lt-ie10.css',
            dest: distAssets + '/css'
        }
    },

    scripts: {
        src: src + '/js/**/*.js',
        dest: distAssets + '/js',
        optimise: { // any js files to uglify
            src: src + '/js/**/app*.js',
            dest: distAssets + '/js',
            options: {
                warnings: true,
                mangle: false,
                compress: {}, // object ({}), or false
                preserveComments: false // 'all', 'some', false
            }
        }
    },

    iconFont: {
        src: src + '/fonts/svg/temp/**/*.svg',
        dest: distAssets + '/fonts', // where the font files get saved after being generated
        copySrc: src + '/fonts/svg/**/*.svg', // where the source icons are
        copyDest: src + '/fonts/svg/temp', // where the icons get temporarily copied to while the font is generated
        options: {
            normalize: true,
            fontName: 'iconfont-admin', // the filename of the font (e.g. iconfont.woff)
            appendCodepoints: true,
            fontHeight: 1001 // fixes badly rendered icons
        },
        template: {
            src: src + '/scss/fonts/_template.scss', // where the sass template is
            dest: src + '/scss/fonts/', // where to save the generated scss partial
            sassPartialName: '_iconfont.scss', // the name of the generated scss partial
            options: {
                fontPath: '../fonts/', // path to font directory, relative to the production CSS file
                className: 'fi-adm' // what should the icon class be prefixed with? E.g.`[prefix]-chevron`
            }
        }
    },

    iconFontStyleGuide: {
        src: src + '/fonts/style-guide/temp/**/*.svg',
        dest: distAssets + '/fonts', // where the font files get saved after being generated
        copySrc: src + '/fonts/style-guide/**/*.svg', // where the source icons are
        copyDest: src + '/fonts/style-guide/temp', // where the icons get temporarily copied to while the font is generated
        options: {
            normalize: true,
            fontName: 'iconfont-style-guide', // the filename of the font (e.g. iconfont.woff)
            appendCodepoints: true,
            fontHeight: 1001 // fixes badly rendered icons
        },
        template: {
            src: src + '/scss/fonts/_template.scss', // where the sass template is
            dest: src + '/scss/fonts/', // where to save the generated scss partial
            sassPartialName: '_iconfont-style-guide.scss', // the name of the generated scss partial
            options: {
                fontPath: '../fonts/', // path to font directory, relative to the production CSS file
                className: 'fi-sg' // what should the icon class be prefixed with? E.g.`[prefix]-chevron`
            }
        }
    },

    sprite: {
        src: src + '/ui/svg/temp/**/*.svg',
        svgDest: distAssets + '/ui/sprite.svg',
        pngDest: distAssets + '/ui/sprite.png',
        copySrc: src + '/ui/svg/**/*.svg', // where the source SVGs are
        copyDest: src + '/ui/svg/temp', // where the icons get temporarily copied to while the font is generated
        retina: {
            name: 'sprite@2x.png', // what to call the 2x PNG sprite
            dest: distAssets + '/ui' // where to save the 2x PNG sprite
        },
        options: {
            cssPathSvg: '../ui/sprite.svg',
            templateSrc: src + '/scss/sprite/_template.scss',
            templateDest: src + '/scss/sprite/_sprite.scss',
            // IE8 PNG fallback
            cssPathNoSvg: '../ui/sprite.png',
            padding: 4, // spacing between icons
            pixelBase: 16,
            positioning: 'packed', // vertical | horizontal | diagonal | packed
            units: 'px'
        }
    }

};
