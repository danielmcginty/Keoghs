'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var runSequence = require('run-sequence');
var argv = require('yargs').argv;
var handleErrors = require('../helpers/handle_errors');

// configs
var config = require('../config').styles;
var pleaseOptions = config.pleaseOptions;
var blessConfig = config.bless;


/**
 * Compile Sass to CSS
 *
 * Uses gulp-sass (https://github.com/dlmanning/gulp-sass) and gulp-pleeease (https://github.com/danielhusar/gulp-pleeease)
 *
 * Usage: gulp styles
 */
gulp.task('styles', function(callback) {

    if (argv.prod === undefined) {
        runSequence(
            ['compile'],
            callback);
    } else {
        runSequence(
            ['compile'],
            ['bless'],
            callback);
    }
});

gulp.task('compile', function () {
    return gulp.src(config.src)
        .pipe($.sass.sync(config.settings))
        .on('error', $.sass.logError)
        .on('error', handleErrors)
        .pipe($.pleeease(argv.prod === undefined ? pleaseOptions.development : pleaseOptions.production))
        .pipe($.size({title: 'Styles compiled'}))
        .pipe(gulp.dest(config.dest));
});

gulp.task('bless', function () {
    return gulp.src(blessConfig.src)
        .pipe($.bless({
            imports: false
        }))
        .on('error', handleErrors)
        .pipe(gulp.dest(blessConfig.dest));
});

