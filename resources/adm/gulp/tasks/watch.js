'use strict';

var gulp = require('gulp');

// config
var stylesConfig = require('../config').styles;
var scriptsConfig = require('../config').scripts;
var copyAssetsConfig = require('../config').copyAssets;


/**
 * Watch website files for changes
 *
 * Usage: gulp watch
 */
gulp.task('watch', ['watch:styles', 'watch:scripts', 'watch:assets']);

/**
 * Watch ONLY style files
 *
 * Usage: gulp watch:styles
 */
gulp.task('watch:styles', ['styles'], function () {
  gulp.watch(stylesConfig.src, ['styles']);
});

/**
 * Watch ONLY script files
 *
 * Usage: gulp watch:scripts
 */
gulp.task('watch:scripts', ['scripts'], function () {
  gulp.watch(scriptsConfig.src, ['scripts']);
});

/**
 * Watch ONLY asset files (everything in teh UI directory)
 *
 * Usage: gulp watch:assets
 */
gulp.task('watch:assets', ['copyassets'], function () {
  gulp.watch(copyAssetsConfig.images.src, ['copyassets']);
});
