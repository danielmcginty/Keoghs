'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var runSequence = require('run-sequence');
var argv = require('yargs').argv;

// configs
var config = require('../config').favicons;


/**
 * Favicons
 *
 * Usage: gulp favicons
 */
gulp.task('favicons', function(callback) {

    if (argv.prod === undefined) {
        runSequence(
            ['favicons'],
            callback);
    } else {
        runSequence(
            ['favicons'],
            callback);
    }
});

/**
 * Copy favicons into /dist
 *
 * Usage: gulp favicons
 */
gulp.task('favicons', function () {
    return gulp.src(config.src)
        .pipe(gulp.dest(config.dest));
});
