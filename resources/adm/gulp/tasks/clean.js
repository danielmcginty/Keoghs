'use strict';

var gulp = require('gulp');
var del = require('del');
var $ = require('gulp-load-plugins')();

// configs
var config = require('../config').paths;


/**
 * Clean output directory
 *
 * Gets rid of all the folders and files created by 'gulp build'
 *
 * Usage: gulp clean
 */
gulp.task('clean', function () {
    return del(config.dest, {force: true});
});
