'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var runSequence = require('run-sequence');
var argv = require('yargs').argv;

// configs
var config = require('../config').scripts;
var optimiseConfig = config.optimise;

/**
 * Copy scripts into /dist, and minify app.js
 *
 * Usage: gulp scripts
 */
gulp.task('scripts', function(callback) {

    if (argv.prod === undefined) {
        runSequence(
            ['copyscripts'],
            callback);
    } else {
        runSequence(
            ['copyscripts'],
            ['uglify-app'],
            callback);
    }
});

gulp.task('copyscripts', function() {
    return gulp.src(config.src)
        .pipe(gulp.dest(config.dest))
});

// Documentation - https://github.com/mishoo/UglifyJS2#the-simple-way
gulp.task('uglify-app', function() {
    return gulp.src(optimiseConfig.src)
        .pipe($.uglify(optimiseConfig.options))
        .pipe(gulp.dest(optimiseConfig.dest));
});
