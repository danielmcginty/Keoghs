'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var runSequence = require('run-sequence');
var argv = require('yargs').argv;


/**
 * Build task
 *
 * Runs all the build tasks in this file from one command.
 *
 * Usage: gulp build
 */
gulp.task('build', function(callback) {

    if (argv.prod === undefined) {
        runSequence(
            ['clean'],
            ['iconfont', 'sprite'],
            ['copyassets', 'styles', 'scripts', 'favicons'],
            callback);
    } else {
        runSequence(
            ['clean'],
            ['iconfont', 'sprite'],
            ['copyassets', 'styles', 'scripts', 'favicons'],
            ['bless', 'optimiseassets'],
            callback);
    }
});
