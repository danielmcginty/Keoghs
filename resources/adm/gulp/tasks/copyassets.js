'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var runSequence = require('run-sequence');
var argv = require('yargs').argv;

// It doesn't seem like the following two modules are used on this page, but they are defined in gulp/config.js
var pngcrush = require('imagemin-pngcrush');
var pngquant = require('imagemin-pngquant');

// configs
var config = require('../config').copyAssets;
var imagesConfig = config.images;
var optimiseConfig = require('../config').optimiseAssets;
var pluginsConfig = config.plugins;


/**
 * Copy Assets
 *
 * Usage: gulp copyassets
 */
gulp.task('copyassets', function(callback) {

    if (argv.prod === undefined) {
        runSequence(
            ['copyimages', 'copyplugins'],
            callback);
    } else {
        runSequence(
            ['copyimages', 'copyplugins'],
            ['optimiseassets'],
            callback);
    }

});

/**
 * Copy images into /dist
 *
 * Usage: gulp copyimages
 */
gulp.task('copyimages', function () {
    return gulp.src(imagesConfig.src)
        .pipe(gulp.dest(imagesConfig.dest));
});

/**
 * Compress UI images
 *
 * Usage: gulp optimise
 */
gulp.task('optimiseassets', function () {
    return gulp.src(optimiseConfig.src)
        .pipe($.imagemin(optimiseConfig.options))
        .pipe(gulp.dest(optimiseConfig.dest));
});

/**
 * Copy plugins into /dist
 *
 * Usage: gulp copyplugins
 */
gulp.task('copyplugins', function () {
    return gulp.src(pluginsConfig.src)
        .pipe(gulp.dest(pluginsConfig.dest));
});
