'use strict';

var gulp = require('gulp');
var del = require('del');
var $ = require('gulp-load-plugins')();
var svgspritesheet = require('gulp-svg-spritesheet');
var handleErrors = require('../helpers/handle_errors');
var runSequence = require('run-sequence');

// configs
var config = require('../config').sprite;


/**
 * Sprite generator
 *
 * Converts .svg files into an .svg sprite sheet (with a .png fallback for IE8)
 *
 * Also generates an .scss file with pre-made icon classes referencing each icon from the sheet
 *
 * Usage: $ gulp sprite
 */
gulp.task('sprite', function(callback) {
    runSequence(
        ['cleanTempSpriteIcons'],
        ['copyTempSpriteIcons'],
        ['sprite-normal'],
        ['sprite-retina'],
        ['cleanTempSpriteIcons'],
        callback);
});

gulp.task('sprite-normal', function () {
    return gulp.src(config.src)
        .pipe(svgspritesheet(config.options))
        .on('error', handleErrors)
        .pipe(gulp.dest(config.svgDest))
        .pipe($.svg2png())
        .on('error', handleErrors)
        .pipe(gulp.dest(config.pngDest));
});

gulp.task('sprite-retina', function () {
    return gulp.src(config.svgDest)
        .pipe($.svg2png([2]))
        .pipe($.rename(config.retina.name))
        .pipe(gulp.dest(config.retina.dest));
});

gulp.task('cleanTempSpriteIcons', function(){
    return del(config.copyDest, {force: true});
});

gulp.task('copyTempSpriteIcons', function(){
    return gulp.src(config.copySrc)
        .pipe(gulp.dest(config.copyDest));
});
