'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();

/**
 * Error helper function to output errors to console and popup
 */
module.exports = function(error) {

    $.notify.onError({title: "Gulp Error", message: "Check your terminal"})(error);
    console.log('Error:', error);
    this.emit("end");
};
