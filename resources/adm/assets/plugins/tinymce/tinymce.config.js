
if (typeof location.origin === 'undefined') {
    location.origin = location.protocol + '//' + location.host;
}

/*
 * Default TinyMCE Options within the CMS.
 * This object is extended (where required) within resources/adm/views/dynamic-scripts/tinymce/tinymce-configs.blade.php
 */
var tinymce_default_options = {

    /*
     * The default CSS / jQuery selector for TinyMCE instances
     */
    selector: 'textarea[data-tinymce][data-mce-config="default"]',

    /*
     * Set the base URL for TinyMCE
     */
    document_base_url: window.base_url,

    /*
     * Define which CSS files we wish to display in TinyMCE instances.
     * By default, this is overwritten by the tinymce-configs.blade.php to include theme specific styles.
     */
    content_css : [
        "//fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic",
        "/admin/dist/assets/css/tinymce.css",
        "/default/assets/css/app.css"
    ],

    /*
     * Define which TinyMCE plugins are available
     */
    plugins :[
        'advlist autolink anchor code contextmenu fullscreen hr image insertdatetime link lists',
        'nonbreaking paste searchreplace table textcolor visualblocks visualchars wordcount'
    ],

    /*
     * Set out the toolbar options
     */
    toolbar: "undo redo | cut copy paste pastetext | styleselect | removeformat | bold italic underline | alignleft aligncenter alignright alignjustify | bullistnooptions numlistnooptions | table | link image",

    /*
     * Add extra buttons, providing custom functionality over DOM objects
     * https://stackoverflow.com/questions/16893832/tinymce-disable-numlist-dropdown
     */
    setup: function(editor) {
        editor.addButton('bullistnooptions', {
            text: '',               // Without text label
            icon: 'bullist',        // Use original icon
            tooltip: 'Bullet list', // Tooltip, if you want
            onclick: function() {
                // Call original plugin function
                tinymce.activeEditor.execCommand('InsertUnorderedList');
            }
        });
        editor.addButton('numlistnooptions', {
            text: '',               // Without text label
            icon: 'numlist',        // Use original icon
            tooltip: 'Number list', // Tooltip, if you want
            onclick: function() {
                // Call original plugin function
                tinymce.activeEditor.execCommand('InsertOrderedList');
            }
        });
    },

    /*
     * Default height of the TinyMCE Editor
     */
    height: 300,

    /*
     * This option gives you the ability to disable the resize handle or set it to resize both horizontal and vertically.
     * The option can be true, false or the string "both".
     * False disables the resize
     * True enables vertical resizing only
     * "both" makes it possible to resize in both directions horizontal and vertical.
     */
    resize: true,

    /*
     * TinyMCE init code of elFinderBrowser
     */
    file_browser_callback : cmsAssetManagerBrowser,

    /*
     * This is a true / false value if the usage of the browsers internal spellchecker should be used.
     */
    browser_spellcheck: true,

    /*
     * The 'theme' used to style the TinyMCE editor
     */
    skin: 'lightgray',

    /*
     * This option gives you the ability to disable the auto generation of hidden input fields for inline editing elements.
     * By default all inline editors gets an hidden input element that contents gets saved to when you do editor.save() or tinymce.triggerSave();
     * This can be disabled if you don't need these controls.
     */
    hidden_input: false,

    /*
     * This options allows you to turn on / off the resizing handles on images, tables or media objects.
     */
    object_resizing: false,

    /*
     * This option allows us to define how to encode characters
     * Options: "named", "numeric", "raw"
     * "named": Converts special characters into 'named' entities (e.g. &nbsp; for a non-breaking space)
     * "numeric": Converts special characters into numeric entities (e.g. &#160; for a non-breaking space)
     * "raw": All characters stored as they are, aside from &, <, > and "
     *
     * http://www.tinymce.com/wiki.php/Configuration:entity_encoding
     */
    entity_encoding: "raw",

    /*
     * This true / false option gives you the ability to turn on / off the visual aid for borderless tables.
     * If the border of a table is set to "0", then TinyMCE adds a dotted line around the table by default.
     */
    visual: true,

    /*
     * This true / false option allows specification over whether the Status Bar (word count, etc) is displayed.
     */
    statusbar: true,

    /*
     * This true / false option allows specification of whether to make a tab key press insert 3 &nbsp; characters
     * https://www.tinymce.com/docs/plugins/nonbreaking/#nonbreaking_force_tab
     */
    nonbreaking_force_tab: true,

    /*
     * This true / false option allows specification over whether to include the 'Paste as Text' option in the toolbar
     */
    paste_as_text: true,

    /*
     * This true / false option allows specification over the default state of the visualblocks plugin.
     * https://www.tinymce.com/docs/plugins/visualblocks/#visualblocks_default_state
     */
    visualblocks_default_state: false,

    /*
     * This true / false option allows specification over whether HTML5 time elements are inserted with dates & times
     * https://www.tinymce.com/docs/plugins/insertdatetime/#insertdatetime_element
     */
    insertdatetime_element: true,

    /*
     * This true / false option allows us to toggle whether URLs in the TinyMCE instance are absolute or relative
     * By default, disables relative URLs to ensure that the shortcodes are correctly inserted
     */
    relative_urls: false,

    /*
     * This true / false option allows us to toggle whether URLs returned from a file manager are relative or absolute.
     * By default, disables relative URLs to ensure that shortcodes are correctly inserted
     */
    remove_script_host: false,

    /*
     * This true / false option allows us to toggle whether <img/> tags are saved with width and height attributes
     * Disabled by default.
     */
    image_dimensions: false,

    /*
     * This option allows us to specify a URL to populate a list of URLs within the link selector, providing easy
     * access to the client when linking to internal pages.
     */
    link_list: location.origin + '/adm/ajax_get_urls_for_tinymce',

    /*
     * This option allows us to specify a list of HTML templates to be inserted into TinyMCE
     */
    templates: [],

    /*
     * This option allows us to specify what formatting options are available to users when styling content.
     * E.g. DOM elements, classes, etc.
     */
    style_formats: [

        {title: "Header Tags", items: [
            {title: "Header 2 (h2)", format: "h2"},
            {title: "Header 3 (h3)", format: "h3"},
            {title: "Header 4 (h4)", format: "h4"},
            {title: "Header 5 (h5)", format: "h5"},
            {title: "Header 6 (h6)", format: "h6"}
        ]},

        {title: "Blocks", items: [
            {title: "Paragraph", format: "p"},
            {title: "Div", format: "div"},
            {title: "Blockquote", format: "blockquote"}
        ]},

        {title : 'Text Styles', items: [
            {title: 'Text Size Regular (16px)', selector: 'p,h1,h2,h3,h4,h5,h6,table,td,th,div,span,small,b,strong,i,u,a,ul,ol,li,img', classes: 'text-base'},
            {title: 'Text Size Large (18px)', selector: 'p,h1,h2,h3,h4,h5,h6,table,td,th,div,span,small,b,strong,i,u,a,ul,ol,li,img', classes: 'text-lg'},
            {title: 'Text Size Larger (20px)', selector: 'p,h1,h2,h3,h4,h5,h6,table,td,th,div,span,small,b,strong,i,u,a,ul,ol,li,img', classes: 'text-xl'},
            {title: 'Text Size X-Large (24px)', selector: 'p,h1,h2,h3,h4,h5,h6,table,td,th,div,span,small,b,strong,i,u,a,ul,ol,li,img', classes: 'text-2xl'},
            {title: 'Text Size XX-Large (32px)', selector: 'p,h1,h2,h3,h4,h5,h6,table,td,th,div,span,small,b,strong,i,u,a,ul,ol,li,img', classes: 'text-3xl'},

            {title: 'Font Weight Regular (400)', selector: 'p,h1,h2,h3,h4,h5,h6,table,td,th,div,span,small,b,strong,i,u,a,ul,ol,li,img', classes: 'font-normal'},
            {title: 'Font Weight Semi-Bold (600)', selector: 'p,h1,h2,h3,h4,h5,h6,table,td,th,div,span,small,b,strong,i,u,a,ul,ol,li,img', classes: 'font-semibold'},
            {title: 'Font Weight Bold (700)', selector: 'p,h1,h2,h3,h4,h5,h6,table,td,th,div,span,small,b,strong,i,u,a,ul,ol,li,img', classes: 'font-bold'},

            {title: 'Text Uppercase', selector: 'p,h1,h2,h3,h4,h5,h6,table,td,th,div,span,small,b,strong,i,u,a,ul,ol,li,img', classes: 'uppercase'}

        ]},

        {title : 'Colours', items: [
            {title: 'Main Colour', inline: 'span', selector: 'p,h1,h2,h3,h4,h5,h6,table,td,th,div,span,small,b,strong,i,u,a,ul,ol,li,img', classes: 'text-primary'},
            {title: 'Secondary Colour', inline: 'span', selector: 'p,h1,h2,h3,h4,h5,h6,table,td,th,div,span,small,b,strong,i,u,a,ul,ol,li,img', classes: 'text-secondary'},

            {title: 'White', inline: 'span', selector: 'p,h1,h2,h3,h4,h5,h6,table,td,th,div,span,small,b,strong,i,u,a,ul,ol,li,img', classes: 'text-white'},
            {title: 'Grey Lightest', inline: 'span', selector: 'p,h1,h2,h3,h4,h5,h6,table,td,th,div,span,small,b,strong,i,u,a,ul,ol,li,img', classes: 'text-gray-100'},
            {title: 'Grey Light', inline: 'span', selector: 'p,h1,h2,h3,h4,h5,h6,table,td,th,div,span,small,b,strong,i,u,a,ul,ol,li,img', classes: 'text-gray-300'},
            {title: 'Grey', inline: 'span', selector: 'p,h1,h2,h3,h4,h5,h6,table,td,th,div,span,small,b,strong,i,u,a,ul,ol,li,img', classes: 'text-gray-500'},
            {title: 'Grey Dark', inline: 'span', selector: 'p,h1,h2,h3,h4,h5,h6,table,td,th,div,span,small,b,strong,i,u,a,ul,ol,li,img', classes: 'text-gray-700'},
            {title: 'Grey Darkest', inline: 'span', selector: 'p,h1,h2,h3,h4,h5,h6,table,td,th,div,span,small,b,strong,i,u,a,ul,ol,li,img', classes: 'text-gray-900'},
            {title: 'Black', inline: 'span', selector: 'p,h1,h2,h3,h4,h5,h6,table,td,th,div,span,small,b,strong,i,u,a,ul,ol,li,img', classes: 'text-black'}
        ]}
    ]

};

// Removed from here as it's being pulled in through /resources/adm/views/dynamic-scripts/tinymce/tinymce-configs.blade.php
// Being pulled in there so it can be extended to include the theme's app.css if required.
// TINYMCE_CONFIG_SETS.push( tinymce_default_options );

function elFinderBrowser (field_name, url, type, win) {
    tinymce.activeEditor.windowManager.open({
        file: window.base_url+'admin/elf/elfinder_popup',// use an absolute path!
        title: 'File Manager',
        width: 900,
        height: 470,
        resizable: 'yes'
    }, {
        setUrl: function (url) {
            win.document.getElementById(field_name).value = url;
        }
    });
    return false;
}

function cmsAssetManagerBrowser( field_name, url, type, win ){
    tinymce.activeEditor.windowManager.open({
        file: location.origin + '/adm/asset-manager?modal=1&tinymce=1',
        title: 'Asset Manager',
        width: 1200,
        height: 660,
        resizable: 'no'
    }, {
        setUrl: function(url){
            win.document.getElementById(field_name).value = url;
        }
    });
    return false;
}