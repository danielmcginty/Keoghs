/**
 * DOM READY
 */

$(document).ready(function() {

    var body = $("body");

    if( body.hasClass('show-nav') ){
        if($.cookie('nav-state') == "hidden") {
            $("body").removeClass('show-nav');
        }
    }

    /**
     * Toggle menu with keypress (m)
     */
    $(document).keypress(function(e) {
        if ($('input:focus').length == 0) {
            key = String.fromCharCode(e.which);
            /*if (key == "m") {
                $('body').toggleClass('style-guide');
            }*/
            if (key == "m") {

                if($.cookie('nav-state') == "hidden") {
                    $('body').toggleClass('show-nav');
                    $.cookie('nav-state', 'shown');
                } else {
                    $('body').toggleClass('show-nav');
                    $.cookie('nav-state', 'hidden');
                }

            }

        }
    });

    //$(".style-guide-navigation .simple-list").niceScroll({
    //    cursorcolor: "#808080", // change cursor color in hex
    //    cursoropacitymin: 0, // change opacity when cursor is inactive (scrollabar "hidden" state), range from 1 to 0
    //    cursoropacitymax: 1, // change opacity when cursor is active (scrollabar "visible" state), range from 1 to 0
    //    cursorwidth: "5px", // cursor width in pixel (you can also write "5px")
    //    cursorborder: "0", // css definition for cursor border
    //    cursorborderradius: "4px", // border radius in pixel for cursor
    //    zindex: "auto", //| <number>, // change z-index for scrollbar div
    //    scrollspeed: 60, // scrolling speed
    //    mousescrollstep: 40, // scrolling speed with mouse wheel (pixel)
    //    touchbehavior: false, // enable cursor-drag scrolling like touch devices in desktop computer
    //    hwacceleration: true, // use hardware accelerated scroll when supported
    //    boxzoom: false, // enable zoom for box content
    //    dblclickzoom: true, // (only when boxzoom=true) zoom activated when double click on box
    //    gesturezoom: true, // (only when boxzoom=true and with touch devices) zoom activated when pinch out/in on box
    //    grabcursorenabled: true, // (only when touchbehavior=true) display "grab" icon
    //    autohidemode: true, // how hide the scrollbar works, possible values:
    //    //true | // hide when no scrolling
    //    //"cursor" | // only cursor hidden
    //    //false | // do not hide,
    //    //"leave" | // hide only if pointer leaves content
    //    //"hidden" | // hide always
    //    //"scroll", // show only on scroll
    //    background: "", // change css for rail background
    //    iframeautoresize: true, // autoresize iframe on load event
    //    cursorminheight: 32, // set the minimum cursor height (pixel)
    //    preservenativescrolling: true, // you can scroll native scrollable areas with mouse, bubbling mouse wheel event
    //    railoffset: false, // you can add offset top/left for rail position
    //    bouncescroll: false, // (only hw accell) enable scroll bouncing at the end of content as mobile-like
    //    spacebarenabled: true, // enable page down scrolling when space bar has pressed
    //    railpadding: { top: 0, right: 0, left: 0, bottom: 0 }, // set padding for rail bar
    //    disableoutline: true, // for chrome browser, disable outline (orange highlight) when selecting a div with nicescroll
    //    horizrailenabled: true, // nicescroll can manage horizontal scroll
    //    railalign: "right", // alignment of vertical rail
    //    railvalign: "bottom", // alignment of horizontal rail
    //    enabletranslate3d: true, // nicescroll can use css translate to scroll content
    //    enablemousewheel: true, // nicescroll can manage mouse wheel events
    //    enablekeyboard: true, // nicescroll can manage keyboard events
    //    smoothscroll: true, // scroll with ease movement
    //    sensitiverail: true, // click on rail make a scroll
    //    enablemouselockapi: true, // can use mouse caption lock API (same issue on object dragging)
    //    cursorfixedheight: false, // set fixed height for cursor in pixel
    //    hidecursordelay: 400, // set the delay in microseconds to fading out scrollbars
    //    directionlockdeadzone: 6, // dead zone in pixels for direction lock activation
    //    nativeparentscrolling: true, // detect bottom of content and let parent to scroll, as native scroll does
    //    enablescrollonselection: true, // enable auto-scrolling of content when selection text
    //    cursordragspeed: 0.3, // speed of selection when dragged with cursor
    //    rtlmode: "auto", // horizontal div scrolling starts at left side
    //    cursordragontouch: false, // drag cursor in touch / touchbehavior mode also
    //    oneaxismousemode: "auto", // it permits horizontal scrolling with mousewheel on horizontal only content, if false (vertical-only) mousewheel don't scroll horizontally, if value is auto detects two-axis mouse
    //    scriptpath: "", // define custom path for boxmode icons ("" => same script path)
    //    preventmultitouchscrolling: true // prevent scrolling on multitouch events
    //});


    /**
     * STYLE GUIDE SPECIFIC FUNCTIONS
     */

    /**
     * Toggle menu with click on toggle that slides in from the left
     */
    $('.toggle-style-guide').click(function(e) {
        e.preventDefault();
        $('body').toggleClass('style-guide');
    });

    // Collapse all on page load
    $('.style-guide-navigation .dropdown').hide();

    // On click/tap, if closed open dropdown, if open go to page.
    $('.style-guide-navigation .has-dropdown > a').on('click', function(e){
        e.preventDefault();

        $(this).parent().toggleClass('open');
        $(this).next('.dropdown').slideToggle();

    });


    /**
     * Mobile header demonstration
     */

    $('[class^="js-example-"]').click(function(e) {
        e.preventDefault();
        $('[class^="js-example-"]').removeClass('colour-primary');
        $(this).addClass('colour-primary');
        $('.mobile-example').removeClass('watch');
    });

    $('.js-example-watch').click(function() {
        $('.mobile-example .screen').css('width', '200px').css('height', '200px');
        $('.mobile-example').addClass('watch');
    });

    $('.js-example-mobile').click(function() {
        $('.mobile-example .screen').css('width', '320px').css('height', '480px');
    });
    $('.js-example-mobile-landscape').click(function() {
        $('.mobile-example .screen').css('width', '568px').css('height', '480px');
    });

    $('.js-example-tablet').click(function() {
        $('.mobile-example .screen').css('width', '768px').css('height', '1024px');
    });
    $('.js-example-tablet-landscape').click(function() {
        $('.mobile-example .screen').css('width', '1024px').css('height', '768px');
    });

    $('.js-example-fluid').click(function() {
        $('.mobile-example .screen').css('width', '100%').css('height', '420px');
    });



    if ( $('[data-show-font-family]').length ) {

        $('[data-show-font-family]').each(function() {

            var fontStack = $(this).css('font-family');
            fontStack = fontStack.replace("-", " ");
            //fontStack = fontStack.replace(",", "");
            fontStack = fontStack.replace(/'/g, '');
            fontStack = fontStack.replace(/"/g, '');
            //console.log(fontStack);

            /*var fontStackFonts = [].concat.apply([], fontStack.split('"').map(function(v,i){
             return i%2 ? v : v.split(',')
             })).filter(Boolean);*/

            fontStackFonts = fontStack.split(',');
            //console.log(fontStackFonts);

            $(this).prepend(fontStackFonts[0] + ' | ');

        });

    }



    if ( $('[data-show-font-size]').length ) {

        showFontSize();

        $(window).resize(function() {
            showFontSize();
        });

    }


    if ( $('[data-show-line-height]').length ) {

        showLineHeight();

        $(window).resize(function() {
            showLineHeight();
        });

    }


    if ( $('[data-show-button-sizes]').length ) {

        $('[data-show-button-sizes]').each(function() {

            var dataShowButton = $(this).data('show-button-sizes'),
                $dataShowButtonElm = $('a[class*="' + dataShowButton + '"]');

            if ( !$($dataShowButtonElm).hasClass('disabled') ) {

                if ( $(this).data('show-button-sizes') == "" ) {
                    $dataShowButtonElm = $('a[class="button"]');
                }

                var dataShowButtonFontSize = $dataShowButtonElm.css('font-size'),

                    dataShowButtonPaddingTop = $dataShowButtonElm.css('padding-top'),
                    dataShowButtonPaddingRight = $dataShowButtonElm.css('padding-right'),
                    dataShowButtonPaddingBottom = $dataShowButtonElm.css('padding-bottom'),
                    dataShowButtonPaddingLeft = $dataShowButtonElm.css('padding-left');

                $(this).html('<code>font-size: ' + dataShowButtonFontSize + '; padding: ' + dataShowButtonPaddingTop + ' ' + dataShowButtonPaddingRight + ' ' + dataShowButtonPaddingBottom  + ' ' +dataShowButtonPaddingLeft + ';</code>');

            } else {

                $dataShowButtonElm = $('a.button.disabled');

                var dataShowButtonFullOpacity = $dataShowButtonElm.css('opacity'),
                    dataShowButtonOpacity = Math.round( dataShowButtonFullOpacity * 10 ) / 10,
                    dataShowButtonCursor = $dataShowButtonElm.css('cursor');

                $(this).html('<code>opacity: ' + dataShowButtonOpacity + '; cursor: ' + dataShowButtonCursor + ';</code>');

            }

        });

    }


    if ( $('select[data-show-rating]').length ) {

        $('select[data-show-rating]').on("change", function() {

            var showRating = $('select[data-show-rating]').val();

            //console.log(showRating);

            $('div[data-show-rating]').hide();

            if ( showRating == "All") {
                $('div[data-show-rating]').show();
            } else {
                $('div[data-show-rating="' + showRating + '"]').show();
            }

        });

    }


    if ( $('[data-show-colour]').length ) {

        $('[data-show-colour]').each(function() {

            var dataShowColour = rgb2hex($(this).css('backgroundColor'));

            //$(this).html('<kbd>' + dataShowColour + '</kbd>');
            $(this).html('<div class="padding-5">' + dataShowColour + '</div>');
            //$(this).find('kbd').after('<div class="padding-5">' + dataShowColour + '</div>');
        });

    }

});


function rgb2hex(rgb) {
    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
    }
    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}


function showFontSize() {

    $('[data-show-font-size]').each(function() {

        var fontSize = $(this).css('font-size');

        $(this).html(fontSize);

    });
}


function showLineHeight() {

    $('[data-show-line-height]').each(function() {

        var lineHeight = $(this).css('line-height');

        lineHeightMiusPx = lineHeight.substr(0, lineHeight.length-2);

        var lineHeightRound = Math.round(lineHeightMiusPx);

        $(this).html(lineHeightRound + 'px');

    });
}

