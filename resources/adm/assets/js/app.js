
/**
 * GLOBAL VARIABLES
 */

var hash = window.location.hash;    // Get window url hash


/**
 * FOUNDATION
 * Source: http://foundation.zurb.com/docs
 */
$(document).foundation({
    offcanvas : {
        close_on_click : false
    },
    reveal : {
        animation: 'fade',
        animation_speed: 75,
        multiple_opened: true
    }
});


/**
 * WINDOW LOAD
 */
$(window).load(function () {

    // If the url has a hash, go to that hash on the page
    if (hash.length && $(hash).length) {
        goToHash();
    }

    // Update all match height elements
    $.fn.matchHeight._update();

});


/**
 * DOCUMENT READY
 */
$(document).ready(function() {


    /** ---- START VENDOR LIBRARY CODE ------------------------------------------------------ */


    /**
     * SMOOTH SCROLLING
     * Source: https://css-tricks.com/snippets/jquery/smooth-scrolling/
     * Description: smoooooth scollin on down to anchor minus the height od the sticky breadcrumbs.
     */
    if ($(window).width() >= 641) {
        $(function() {
            $('a[href*=#].js-smooth-scroll:not([href=#])').click(function() {

                var stickyNavHeight = $('.main-header').height();

                if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    var targetHash = this.hash;

                    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: target.offset().top - stickyNavHeight
                        }, 1000);
                        document.location.hash = targetHash;
                        return false;
                    }
                }
            });
        });
    }


    /**
     * UNSTICK HEADER ON MOBILE INPUT FOCUS
     * Set fixed header to static on focus - only needed on touch devices (specifically iOS)
     * Reason: iOS has visual glitches with fixed headers when focusing on inputs
    */

    if (Modernizr.touch) {
        $(document)
            .on('focus', 'input, textarea', function() {
                $("body").addClass('touch-input-focused');
            })
            .on('blur', 'input, textarea', function() {
                $("body").removeClass('touch-input-focused');
            })
    }


    /** ---- START BESPOKE TEAM CODE ------------------------------------------------------ */


    /**
     * ADMIN NAVIGATION LINKS
     */
    $('.has-children').hover(
        function() {

            $(this).addClass('open');

        }, function() {

            $(this).removeClass('open');

        }
    );

    //$('.has-children').hover(
    //    function() {
    //        // Mouse Enter
    //        console.log('mouse enter');
    //
    //        hoverObj = $(this);
    //        openHover = setTimeout(function(){
    //            hoverObj.addClass('open');
    //        }, 150);
    //
    //        //console.log(hoverObj);
    //
    //    }, function() {
    //        // Mouse Leave
    //        console.log('mouse leave');
    //
    //        clearTimeout(openHover);
    //
    //        closeHover = setTimeout(function(){
    //            hoverObj.removeClass('open');
    //        }, 150);
    //
    //    }
    //
    //);

    $(document).on('click touchend', 'navigation-links .has-children > a', function(e) {

        if( $(this).parent().hasClass('open') ){

            var url = $(this).attr('href');

            if (url !== "#"){
                window.location = url;
                //console.log('goto link');
            } else {
                e.preventDefault();
                //console.log('prevent default');
            }

        } else {
            e.preventDefault();
            //console.log('open');

            //$('.has-children').removeClass('open');
            $(this).parent().addClass('open');

        }

    });


    /**
     * DROPDOWN MENUS
     */

    //var dropdownTimeout;
    //
    //$(document).on({
    //
    //    // Mouse In
    //    mouseenter: function () {
    //        //console.log('mouse in');
    //
    //        var dropdown = $(this);
    //
    //        dropdownTimeout = setTimeout(function () {
    //            dropdown.addClass('open');
    //        }, 150);
    //
    //    },
    //    // Mouse Out
    //    mouseleave: function () {
    //        //console.log('mouse out');
    //
    //        clearTimeout(dropdownTimeout);
    //        $(this).removeClass('open');
    //    }
    //
    //}, '.has-dropdown-menu');

    //$('.has-dropdown-menu').hover(
    //
    //    // Mouse In
    //    function() {
    //        console.log('mouse in');
    //
    //        var dropdown = $(this);
    //
    //        dropdownTimeout = setTimeout(function(){
    //            dropdown.addClass('open');
    //        }, 150);
    //
    //        // Mouse Out
    //    }, function() {
    //
    //        console.log('mouse out');
    //
    //        clearTimeout(dropdownTimeout);
    //        $(this).removeClass('open');
    //    }
    //
    //);


    $(document).on('click touchend', '.has-dropdown-menu > .dropdown-menu-link', function (e) {
        e.preventDefault();

        if( !$(this).parent().hasClass('open')){

            // Open Menu
            $(this).parent().addClass('open');

        } else {

            // Close Menu
            $(this).parent().removeClass('open');

        }

    });


    // Hide the account dropdown when someone clicks outside of it.
    $("[data-account-dropdown]").bind( "clickoutside", function(){
        $(this).removeClass('open');
    });

    // Hide the save dropdown when someone clicks outside of it.
    $("[data-save-entry-button]").bind( "clickoutside", function(){
        $(this).find('[data-button-dropdown]').addClass('none');
    });

    $("[data-save-entry-button]").bind( "mouseenter", function(){
        var btn = $(this);
        btn.find('[data-button-dropdown]').removeClass('none');
    }).bind( "mouseleave", function(){
        $(this).find('[data-button-dropdown]').addClass('none');
    });


    /**
     * ASSET MANAGER TREE INDEX
     */
    $(document).on('click', '.tree-index .has-child > a', function (e) {

        if( $(this).parent().hasClass('open') ){

            var url = $(this).attr('href');

            if (url !== "#"){
                window.location = url;
            } else {
                e.preventDefault();
            }

        } else {
            e.preventDefault();
            $(this).parent().addClass('open');
        }

    });


    /**
     * STICKY SECTION TITLE BAR
     */
    function stickyActionBar() {

        var scrollTop = $(window).scrollTop(),
            anchorPoint = $('.js-section-action-bar-container').offset().top;

        //if the position of the bottom of the screen (from the start of the page) is greater than the start of the sticky anchor point
        if (scrollTop > anchorPoint) {

            $('.js-section-action-bar').addClass('is-fixed');

        }

        //if the position of the bottom of the screen is less than the sticky anchor point
        if (scrollTop < anchorPoint) {

            $('.js-section-action-bar').removeClass('is-fixed');

        }

    }

    function calcStickyActionBarHeight(){
        var actionBarHeight = $('.js-section-action-bar').height();
        $('.js-section-action-bar-container').css("height", actionBarHeight);
    }

    if($('.js-section-action-bar').length){

        calcStickyActionBarHeight();
        stickyActionBar();

        $(window).scroll(function (){
            stickyActionBar();
        });

        $(window).resize(function (){
            calcStickyActionBarHeight();
            stickyActionBar();
        });

    }


    /**
     * REMAINING CHARACTERS
     *
     * Show remaining characters besides input
     */

    function updateRemainingCharacters(input) {

        var length = input.val().length;
        var maxLength = input.data('max-length');
        var warningLength = input.data('max-length-warning-length');
        var remaining = maxLength - length;

        // Set default warning length
        if (!warningLength){
            warningLength = 15;
        }

        input.next().html(remaining);

        input.next().removeClass('has-warning has-error');

        if (remaining < warningLength && remaining > 0){
            input.next().addClass('has-warning');
        } else if (remaining <= 0) {
            input.next().addClass('has-error');
        }

    }

    $(document).on('keyup change', '[data-max-length]', function () {
        updateRemainingCharacters($(this));
    });

    if ($('[data-max-length]').length){
        $('[data-max-length]').each(function() {
            $(this).wrap('<div class="relative"></div>').after('<div class="remaining-characters" title="Remaining Characters"></div>');

            updateRemainingCharacters($(this));

        });
    }


    /**
     * EXTERNAL VIDEO PREVIEW
     *
     * Author: Connor Bond
     */

    // Toggle Preview Output
    $(document).on('click', '.js-toggle-external-video-preview', function(e) {
        e.preventDefault();

        // Toggle the preview container (following element)
        $(this).next().toggle();

    });

    var videoFields = $('.js-external-video-field');
    if (videoFields.length){

        // Set preview on load
        videoFields.each(function() {
            checkVideoURL($(this));
        });

        // Update preview on change
        videoFields.on('keyup change' ,function(){
            checkVideoURL($(this));
        });

    }

    function checkVideoURL(videoField){

        var videoID;
        var videoURL = videoField.val();

        var youtubeString = "youtube.com/watch?v=";
        var youtubeShorthandString = "youtu.be/";
        var vimeoString = "vimeo.com/";

        var exampleContainer = videoField.parent().find('.js-external-video-container');
        var examplePreview = exampleContainer.find('.js-external-video-preview');
        var exampleInvalid = videoField.parent().find('.js-external-video-invalid');

        // Youtube Video (Long URL)
        if (videoURL.indexOf(youtubeString) != -1){
            //console.log('youtube');

            // Hide invalid message
            exampleInvalid.addClass('none');

            // Extract Video ID (split at ID, strip any GET variables)
            videoID = videoURL.split('v=')[1].split('?')[0];

            examplePreview.html('<iframe src="https://www.youtube.com/embed/' + videoID + '" class="absolute top-0 left-0 width-100 height-100" frameborder="0" allowfullscreen></iframe>');
            exampleContainer.removeClass('none');

        // Youtube Short URL Video
        } else if (videoURL.indexOf(youtubeShorthandString) != -1){
            //console.log('youtube short');

            // Hide invalid message
            exampleInvalid.addClass('none');

            // Extract Video ID (split at ID, strip any GET variables)
            videoID = videoURL.split('.be/')[1].split('?')[0];

            examplePreview.html('<iframe src="https://www.youtube.com/embed/' + videoID + '" class="absolute top-0 left-0 width-100 height-100" frameborder="0" allowfullscreen></iframe>');
            exampleContainer.removeClass('none');

        // Vimeo Video
        } else if (videoURL.indexOf(vimeoString) != -1){
            //console.log('vimeo');

            // Hide invalid message
            exampleInvalid.addClass('none');

            // Extract Vimeo Video ID
            videoID = videoURL.split('.com/')[1];

            // Show preview embed
            examplePreview.html('<iframe src="https://player.vimeo.com/video/' + videoID + '" class="absolute top-0 left-0 width-100 height-100" frameborder="0" allowfullscreen></iframe>');
            exampleContainer.removeClass('none');

            // Invalid URL
        } else {
            //console.log('invalid URL');

            // Clear any preview embeds
            examplePreview.html('');
            exampleContainer.addClass('none');

            // Show invalid message
            exampleInvalid.removeClass('none');

        }

    }



    /**
     * ICON PICKER INPUT TYPE
     *
     * Author: Connor Bond
     */

    $(document).on('click', '.js-icon-option', function(e) {
        e.preventDefault();

        var inputID = $(this).data('input-id');

        $('.js-icon-option[data-input-id="' + inputID + '"]').removeClass('selected');
        $(this).addClass('selected');

    // Double Click (Select and close)
    }).on('dblclick', '.js-icon-option', function(e){
        e.preventDefault();

        var selectButton = $(this).closest('[data-reveal]').find('.js-select-icon-button');
        selectButton.click();

    });

    // Icon Modal 'Select' Button
    $(document).on('click', '.js-select-icon-button', function(e) {
        e.preventDefault();

        // Get chosen Icon
        var inputID = $(this).data('input-id');
        var chosenIcon = $('.js-icon-option[data-input-id="' + inputID + '"].selected').data('icon-class');
        var iconOutput = $('.icon-select-placeholder[data-icon-input-id="' + inputID + '"]');

        // If an icon has been chosen...
        if (chosenIcon){

            if (chosenIcon == "BLANK"){

                // Set hidden input to blank
                $('input[data-icon-input-id="' + inputID + '"]').val(0);

                // Reset icon preview
                iconOutput.removeClass('selected').find('.icon-preview').html('<i class="fi-adm fi-adm-add font-size-32 block margin-b5"></i><span class="font-size-12 font-weight-400">Select Icon</span>');

            } else {

                // Update input / icon preview
                $('input[data-icon-input-id="' + inputID + '"]').val(chosenIcon);
                iconOutput.addClass('selected').find('.icon-preview').html('<i class="fi fi-' + chosenIcon + ' font-size-62"></i>');

            }

            // Dismiss Modal
            $(this).closest('[data-reveal]').foundation('reveal', 'close');

        }

    });


    /**
     * AUTOEXPAND TEXTAREA
     *
     * Source: http://www.technoreply.com/autogrow-textarea-plugin-3-0/
     */

    $("[data-auto-expand]").autoGrow();



    /**
     * SEARCHABLE SELECT
     *
     * Library: chosen.js
     * Source: https://harvesthq.github.io/chosen/
     */

    $("select[data-searchable]").chosen({
        //disable_search_threshold: 10
    });


    /**
     * TAGS INPUT
     *
     * Library: tagsinput.js
     * Source: https://github.com/xoxco/jQuery-Tags-Input
     */

    $('input[data-tags]').tagsInput({
        'defaultText':'Add tag...',
        'delimiter': ',',   // Or a string with a single delimiter. Ex: ';'
        'removeWithBackspace' : false,
        'minChars' : 0,
        'maxChars' : 50, // if not provided there is no limit
        'placeholderColor' : '#888'
    });


    /**
     * TAGS INPUT

     * Source: https://github.com/xoxco/jQuery-Tags-Input
     */

    $("input[data-colour-picker]").spectrum({
        preferredFormat: "hex",
        showInput: true,
        chooseText: "Select Colour"
    });


    /**
     * TIME (CLOCK) PICKER INPUT

     * Source: https://github.com/weareoutman/clockpicker
     */

    $("input[data-time-picker]").clockpicker();



    /**
     * DATE PICKER INPUT

     * Source: https://eternicode.github.io/bootstrap-datepicker
     */

    $("input[data-date-picker]").datepicker();



    /**
     * MULTISELECT, MULTI-RELATION INPUT

     * Source: http://www.quasipartikel.at/multiselect/
     */

    $("select[data-multi-select]").multiselect();


    /**
     * CUSTOM CLOSE REVEAL MODAL BUTTON
     */
    $(document).on('click', '[data-close-reveal]', function(e){
        e.preventDefault();

        // Find closest parent modal and close
        $(this).closest('[data-reveal]').foundation('reveal', 'close');

    });

    /**
     * CUSTOM REVEAL OPEN AUTOFOCUS
     */
    $(document).on('open.fndtn.reveal', '[data-reveal]', function(){
        // Add a slight delay to allow the foundation .open class to be added to the reveal
        setTimeout( function() {
            // Then, focus on the first input, select or textarea
            $('[data-reveal].open').find('.modal-content .input-field').first().find('input, select, textarea').first().focus();
        }, 250 );
    });


    /**
     * CUSTOM ADMIN TABS
     *
     * Show remaining characters besides input
     */

    $(document).on('click touchend', '.tabs [data-tab-id]', function(e) {
        e.preventDefault();

        var parentTabs = $(this).closest('.tabs');
        var tabGroup = parentTabs.closest('.tabs').data('tab-group');
        var tabID = $(this).data('tab-id');
        //console.log(tabGroup + ':' + tabID);

        // Reset Tab Links
        parentTabs.find('a[data-tab-id]').removeClass('active');
        $(this).addClass('active');

        // Reset Tab Content (in Group)
        $('.tabs-content[data-tab-group="' + tabGroup + '"] [data-tab-id]').removeClass('active');

        // Open Chosen Tab
        $('.tabs-content[data-tab-group="' + tabGroup + '"] [data-tab-id="' + tabID + '"]').addClass('active');

    });


    // -------------------
    //    HEADER HIDE ON MOBILE INPUT FOCUS
    // -------------------

    if (Modernizr.touch) {
        $(document)
        .on('focus', 'input , textarea', function() {
            $("body").addClass('input-focus');
        })
        .on('blur', 'input , textarea', function() {
            $("body").removeClass('input-focus');
        })
    }

    $('.js-accessible-menu , .js-accessible-menu *').focus(function() {
        $('.js-accessible-menu').removeClass('visually-hidden');

    });
    $('.js-accessible-menu *').blur(function() {
        $('.js-accessible-menu').addClass('visually-hidden');
    });

    // When we skip content, add tab index then remove to force the folw of tab indexing upon the point of focus // https://www.bignerdranch.com/blog/web-accessibility-skip-navigation-links/
    $('a[href*="#"]').not('a[href="#"]').on('click', function(){
        hash = this.href.split('#')[1];
        focusTabindex();
    });

    //do the same nas above if we load page with a hash
    if (hash.length) {
        hash = window.location.hash.substring(1);
        focusTabindex();
    }



    /**
     * TOUCH FRIENDLY DROPDOWNS (js-hover, js-hover-delay)
     */
    var hoverTimeout;
    var hoverObj, hoverObjBackup;

    //hoverable items (slight delay)
    if ($('.js-hover-delay').length){

        //if(!$('html').hasClass('touch')){

        //mouse behaviour
        $('.js-hover-delay').hover(
            function() {
                //console.log('mouse enter');
                if ($('.js-hover-delay.open').length){
                    $('.js-hover-delay').removeClass('open');
                    $(this).addClass('open');
                    hoverObjBackup = $(this);
                    setTimeout(function(){
                        //console.log('straight');
                        hoverObj = hoverObjBackup;
                        hoverObjBackup = false;
                    }, 101);
                    return false;
                }
                hoverObj = $(this);
                hoverTimeout = setTimeout(function(){
                    hoverObj.addClass('open');
                },150);

            }, function() {
                //console.log('mouse leave');
                clearTimeout(hoverTimeout);
                setTimeout(function(){
                    //console.log('off');
                    if (hoverObj)
                        hoverObj.removeClass('open');
                }, 100);

            }

        );
        //}

        $('.js-hover-delay > a').on('click touchend', function (e){

            if( $(this).parent().hasClass('open') ){

                var url = $(this).attr('href');

                if (url !== "#"){
                    window.location = url;
                } else {
                    e.preventDefault();
                }

            } else {
                e.preventDefault();

                $('.js-hover-delay, .js-hover').removeClass('open');
                $(this).parent().addClass('open');
            }

        });

    }


    if ($('.js-hover').length) {

        $('.js-hover > a').on('click', function(e) {
            e.preventDefault();

            if ($(this).parent().hasClass('open')) {

                var url = $(this).attr('href');
                if (url !== "#"){
                    window.location = url;
                }

            } else {
                $('.js-hover').removeClass('open');
                $(this).parent().addClass('open');
            }

            return false;

        });

        $('.js-hover').on('mouseenter', function() {
            //console.log('mouseenter');
            $(this).addClass('open');

        }).on('mouseleave', function() {
            //console.log('mouseleave');
            $('.js-hover').removeClass('open');

            if( ($(this).parents('.side-nav').length) && ($(this).find('.dropdown').css('position') == 'relative') ) {
                //console.log('parents side-nav && dropdown position == relative');
            } else {
                $('.js-hover').removeClass('open');
            }

        }).on('touchstart', function(){
            //console.log('touchstart');
            $(this).unbind('mouseenter mouseleave');
            $(this).off('mouseenter mouseleave');
        });

    }


    /**
     * CUSTOM TOOLTIPS (TOOLTIPSTER)
     * Source: http://iamceege.github.io/tooltipster/
     */
    if ( $('[title].tooltip').length ) {
        $('[title].tooltip').tooltipster();
    }

    /**
     * DROPDOWN BUTTONS
     */
    if ( $('[data-dropdown-button]').length ) {

        $('[data-button-dropdown-toggle]').on('click', function(e){
            e.preventDefault();

            $(this).toggleClass('open');
            $(this).next().toggleClass('none');

        });

    }


    /**
     * BACK TO TOP
     */

    if ( $('[data-back-to-top]').length ) {

        $('[data-back-to-top]').on('click', function(e){
            e.preventDefault();

            $('html,body').animate({ scrollTop: 0 }, 0);

        });

    }

    $(window).scroll(function () {

        var scroll = $(window).scrollTop();

        if (scroll < 50) {
            $('[data-back-to-top]').fadeOut();
        } else {
            $('[data-back-to-top]').fadeIn();
        }

    });


    /**
     * LOCK SCROLL FOR MODALS
     */

    $(document).on('open.fndtn.reveal', '[data-reveal][data-scroll-lock]', function () {

        $('body').addClass('scroll-lock');

    });

    $(document).on('close.fndtn.reveal', '[data-reveal][data-scroll-lock]', function () {

        $('body').removeClass('scroll-lock');

    });


    // ---- END BESPOKE TEAM CODE -------------------------------------



});//doc ready


/**
 * Show/hide a loading spinner for when ajax calls are being performed
 * (or anything else you need a page lock loading screen for)
 *
 * @param bool isLoading
 */
function showPageLoadingOverlay(isLoading) {

    if (isLoading) {
        $('.js-page-loading-overlay').show();
    } else {
        $('.js-page-loading-overlay').hide();
    }
}

/**
 * Show/hide a loading spinner for when ajax calls are being performed
 * on modals (showPageLoadingOverlay doesn't look right)
 *
 * @param jQuery Object $targetModal
 * @param bool isLoading
 */
function showModalLoadingOverlay($targetModal, isLoading) {

    var loadingHtml = '';
    loadingHtml += '<div class="js-modal-loading-overlay modal-loading-overlay absolute top-0 left-0 width-100 height-100" style="z-index: 999; display: none;">';
        loadingHtml += '<div class="table background colour-white rgba-4 width-100 height-100">';
            loadingHtml += '<div class="colour-black text-center table-cell width-100 height-100">';
                loadingHtml += '<i class="block fi-adm fi-adm-loading font-size-30 animated spin-cw animation-duration-2s animation-linear animation-infinite"></i>';
            loadingHtml += '</div>';
        loadingHtml += '</div>';
    loadingHtml += '</div>';

    if (!$targetModal.find('.js-modal-loading-overlay').length) {
        $targetModal.prepend(loadingHtml);
    }

    if (isLoading) {
        $targetModal.find('.js-modal-loading-overlay').show();
    } else {
        $targetModal.find('.js-modal-loading-overlay').hide();
    }
}


function focusTabindex() {

    // Setting 'tabindex' to -1 takes an element out of normal
    // tab flow but allows it to be focused via javascript
    $("#" + hash).attr('tabindex', -1).on('blur focusout', function () {

        // when focus leaves this element,
        // remove the tabindex attribute
        $(this).removeAttr('tabindex');

    }).focus(); // focus on the content container

}


function goToHash() {

    if ($('#' + hash).length) {
        var hashTop = $('#' + hash).offset().top;

        $('html,body').animate({
            scrollTop: hashTop - $('.fixed-header').height() - 20
        }, 0);
    }

    return false;
}
