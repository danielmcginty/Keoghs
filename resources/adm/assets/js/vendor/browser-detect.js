
/**
 * Browser android detector
 * Source: http://stackoverflow.com/questions/14403766/how-to-detect-the-stock-android-browser
 */

// Browser android detector
var navU = navigator.userAgent;

// Android Mobile
var isAndroidMobile = navU.indexOf('Android') > -1 && navU.indexOf('Mozilla/5.0') > -1 && navU.indexOf('AppleWebKit') > -1;

// Apple webkit
var regExAppleWebKit = new RegExp(/AppleWebKit\/([\d.]+)/);
var resultAppleWebKitRegEx = regExAppleWebKit.exec(navU);
var appleWebKitVersion = (resultAppleWebKitRegEx === null ? null : parseFloat(regExAppleWebKit.exec(navU)[1]));

// Chrome
var regExChrome = new RegExp(/Chrome\/([\d.]+)/);
var resultChromeRegEx = regExChrome.exec(navU);
var chromeVersion = (resultChromeRegEx === null ? null : parseFloat(regExChrome.exec(navU)[1]));

// Native Android Browser
var isAndroidBrowser = isAndroidMobile && (appleWebKitVersion !== null && appleWebKitVersion < 537) || (chromeVersion !== null && chromeVersion < 37);

//now
if (isAndroidBrowser) {
    $('html').addClass('browser-android');
}


/**
 * Detect iOs 7 and below
 * Source: http://stackoverflow.com/questions/9038625/detect-if-device-is-ios & http://stackoverflow.com/questions/11171895/detecting-ios-version-number-from-user-agent-using-regular-expressions
 */

// Detect if the device is iOS
var detectIphone = navigator.userAgent.match(/(iPhone)/);
if(detectIphone) {
    _iOSVersion = (navigator.userAgent.match(/\b[0-9]+_[0-9]+(?:_[0-9]+)?\b/)||[''])[0].replace(/_/g,'.');
    if ( _iOSVersion < '8' ) {
        $('html').addClass('device-iphone-lt-ios8');
    }
}

/**
 * Detect Browser and Version
 * Source: https://github.com/ded/bowser
 */
//console.log(bowser);
var bowserName = bowser.name,
    bowserName = bowserName.replace(" ", "-"),
    bowserList = 'browser-' + bowserName + ' browser-version-' +  bowser.version,
    bowserList = bowserList.replace(".", "pt"),
    bowserList = bowserList.replace("pt0", ""),
    bowserList = bowserList.toLowerCase(bowserList);
//console.log(bowserList);
$('html').addClass(bowserList);
