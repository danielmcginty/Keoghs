<div class="breadcrumb-bar background colour-white padding-y10">
    <div class="row">
        <div class="column">
            <div class="table width-100 height-100">

                <div class="table-cell width-100">
                    @if( isset($breadcrumbs) && !empty($breadcrumbs) )
                    <ul class="breadcrumbs simple-list inline-list inline-list-dividers greater-than colour-grey font-size-13">
                        @foreach( $breadcrumbs as $ind => $breadcrumb )
                            @if( ($ind+1) == sizeof($breadcrumbs) )
                                <li class="opacity-7">{{ $breadcrumb['label'] }}</li>
                            @else
                                <li>
                                    <a href="{{ $breadcrumb['url'] }}" title="Back to {{ $breadcrumb['label'] }}" class="colour-grey underline-on-hover">{{ $breadcrumb['label'] }}</a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                    @endif
                </div>

                @if( isset( $cloudflare_enabled ) && $cloudflare_enabled )
                    <div class="table-cell text-right">
                        <div class="inline-block vertical-middle nowrap border-r1 colour-grey-light margin-r10">
                            <a data-purge-cloudflare-cache href="javascript:void(0)" class="inline-block vertical-middle nowrap font-size-13 colour-grey underline-on-hover margin-r10 relative" style="top: -1px;" title="Purge all Cloudflare Cache">
                                Purge Cache
                            </a>
                            <a data-switch-development-mode href="javascript:void(0)" class="inline-block vertical-middle nowrap font-size-13 colour-grey underline-on-hover margin-r10 relative" style="top: -1px;" title="Toggle Development Mode (No Caching)">
                                Development Mode: <span data-development-mode class="colour-{{ $cloudflare_dev_mode ? 'success' : 'error' }}">{{ $cloudflare_dev_mode ? 'On' : 'Off' }}</span>
                            </a>
                        </div>
                    </div>
                @endif

                <div class="table-cell text-right">
                    <a href="{{ route('theme') }}" target="_blank" title="View Main Site" class="inline-block vertical-middle nowrap font-size-13 colour-grey underline-on-hover margin-r25 relative" style="top: -1px">
                        View Site
                    </a>
                </div>

                <div class="table-cell text-right">
                    <div data-account-dropdown class="has-dropdown-menu nowrap user-select-none">
                        <div class="dropdown-menu-link account-dropdown-link inline-block colour-grey padding-r15 cursor-pointer hover-fade fade-7">
                            <i class="fi-adm fi-adm-account font-size-18 inline-block vertical-middle margin-r5 colour-grey-lightest"></i>
                            <span class="inline-block vertical-middle font-size-13">Hi, {{ $me->first_name }}</span>
                        </div>
                        <div class="account-dropdown-menu dropdown-menu z-index-5 text-left">
                            <div class="inner background colour-white radius overflow-hidden">
                                <ul class="simple-list margin-0 font-size-14">
                                    @if ( \Auth::user()->canAccess( 'adm/user_accounts' ))
                                        <li>
                                            <a href="{{ route('adm.path', ['user_accounts']) }}" class="button small block text-left margin-0 padding-x10 padding-y10">
                                                <i class="fi-adm fi-adm-people font-size-16 margin-r5 colour-grey inline-block vertical-middle"></i>
                                                <span class="inline-block vertical-middle">User Accounts</span>
                                            </a>
                                        </li>
                                    @endif
                                    @if ( \Auth::user()->canAccess( 'adm/asset-manager' ))
                                        <li>
                                            <a href="{{ route('adm.asset-manager') }}" class="button small block text-left margin-0 padding-x10 padding-y10">
                                                <i class="fi-adm fi-adm-folder font-size-16 margin-r5 colour-grey inline-block vertical-middle"></i>
                                                <span class="inline-block vertical-middle">Asset Manager</span>
                                            </a>
                                        </li>
                                    @endif
                                    <li>
                                        <a href="{{ route('adm.logout') }}" class="button small block text-left margin-0 padding-x10 padding-y10" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                                        <form id="logout-form" action="{{ route('adm.logout') }}" method="POST" class="none">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div id="content"></div>

@if( isset( $cloudflare_enabled ) && $cloudflare_enabled )
@push( 'footer_scripts' )
<script>
$(document).ready( function(){
    var $purge_btn = $('[data-purge-cloudflare-cache]');
    var $switch_dev_mode = $('[data-switch-development-mode]');
    var $dev_mode_status = $switch_dev_mode.find('[data-development-mode]');

    $switch_dev_mode.on( 'click', function( evt ){
        evt.preventDefault();

        $.ajax({
            url: '{{ route('adm.ajax', 'ajax_switch_cf_dev_mode') }}',
            type: 'GET',
            cache: false,
            dataType: 'json',
            beforeSend: function(){
                $dev_mode_status.removeClass( 'colour-success' ).removeClass( 'colour-error' ).html( 'Switching...' );
            },
            success: function( json ){
                if( json.success ){
                    if( json.response == 'on' ){
                        $dev_mode_status.addClass( 'colour-success' ).text( 'On' );
                    } else {
                        $dev_mode_status.addClass( 'colour-error' ).text( 'Off' );
                    }
                }
            }
        });
    });

    $purge_btn.on( 'click', function( evt ){
        evt.preventDefault();

        $.ajax({
            url: '{{ route('adm.ajax', 'ajax_purge_cf_cache') }}',
            type: 'GET',
            cache: false,
            dataType: 'json',
            beforeSend: function(){
                $purge_btn.text( 'Clearing Cache...' );
            },
            success: function( json ){
                if( json.success ){
                    $purge_btn.text( 'Cache Purged.' );

                    setTimeout( function(){
                        $purge_btn.text( 'Purge Cache' );
                    }, 3000 );
                } else {
                    $purge_btn.text( 'Purge Cache' );
                }
            }
        });
    });
});
</script>
@endpush
@endif