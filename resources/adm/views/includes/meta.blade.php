<!DOCTYPE html>
<!--[if lt IE 9]> <html class="no-js lt-ie9 lt-ie10" lang="en" prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9 lt-ie10" lang="en" prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]<!--><html class="no-js" lang="en" prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#"><!--<![endif]-->

<head>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>@yield('page_title') | {{ config('app.name', 'Laravel') }}</title>

    <?php // Favicons (http://realfavicongenerator.net/) ?>
	<link rel="apple-touch-icon" sizes="180x180" href="/admin/dist/assets/favicons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/admin/dist/assets/favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/admin/dist/assets/favicons/favicon-16x16.png">
	<link rel="mask-icon" href="/admin/dist/assets/favicons/safari-pinned-tab.svg" color="#222222">
	<link rel="shortcut icon" href="/admin/dist/assets/favicons/favicon.ico">
	<meta name="msapplication-config" content="/admin/dist/assets/favicons/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">

    <?php // Admin Styles ?>
    <link rel="stylesheet" href="{{ auto_version('/admin/dist/assets/css/app.css') }}">

    <?php // Patches ?>
    <style>
        .is-sorting.overflow-hidden-sort,
        .is-sorting .overflow-hidden-sort {
            overflow: hidden !important;
        }
    </style>

    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->

    <?php // Later than IE10 Styles (Bless) ?>
    <!--[if (lt IE 9)|(IE 9)]>
    <link rel="stylesheet" href="{{ auto_version('/admin/dist/assets/css/lt-ie10-blessed2.css') }}">
    <link rel="stylesheet" href="{{ auto_version('/admin/dist/assets/css/lt-ie10-blessed1.css') }}">
    <link rel="stylesheet" href="{{ auto_version('/admin/dist/assets/css/lt-ie10.css') }}">
    <![endif]-->

    <?php // Admin Google Fonts ?>
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700" rel="stylesheet" type="text/css">

    <?php // Later than IE9 Scripts ?>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="{{ auto_version('/admin/dist/assets/js/ie8/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ auto_version('/admin/dist/assets/js/ie8/foundation.js') }}"></script>
    <script src="//s3.amazonaws.com/nwapi/nwmatcher/nwmatcher-1.2.5-min.js"></script>
    <script src="//html5base.googlecode.com/svn-history/r38/trunk/js/selectivizr-1.0.3b.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <?php // Only needed with headroom.js <script src="/admin/dist/assets/js/vendor/classList.min.js"></script> ?>
    <![endif]-->

</head>

<body id="body" class="{{ isset($table) ? $table : '' }}-page">

    <div id="top"></div>