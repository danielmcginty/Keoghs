

@if( \Session::has('error') || \Session::has('success') || isset($error_message) || isset($success_message) )

    <?php
        if( !isset( $error_message ) && \Session::has('error') ){
            $error_message = session('error');
        }
        if( !isset( $error_message ) || empty( $error_message ) ){
            $error_message = false;
        }

        if( !isset( $success_message ) && \Session::has('success') ){
            $success_message = session('success');
        }
        if( !isset( $success_message ) || empty( $success_message ) ){
            $success_message = false;
        }

        if( !isset( $notice_message ) && \Session::has('notice') ){
            $notice_message = session('notice');
        }
        if( !isset( $notice_message ) || empty( $notice_message ) ){
            $notice_message = false;
        }
    ?>

    <div class="top-notification fixed left-0 width-100 text-center">

        @if ( $error_message )
            <div data-alert class="alert-box background colour-error inline-block radius-b margin-0 vertical-top">
                {!! $error_message !!}
                <a href="" title="Dismiss" class="close">&times;</a>
            </div>
        @endif

        @if ( $success_message )
            <div data-alert class="alert-box background colour-success inline-block radius-b margin-0 vertical-top">
                {!! $success_message !!}
                <a href="" title="Dismiss" class="close">&times;</a>
            </div>
        @endif

        @if ( $notice_message )
            <div data-alert class="alert-box background colour-notice inline-block radius-b margin-0 vertical-top">
                {!! $notice_message !!}
                <a href="" title="Dismiss" class="close">&times;</a>
            </div>
        @endif

    </div>

@elseif( count($errors) > 0 )

    <div class="top-notification fixed left-0 width-100 text-center">
        <div data-alert class="alert-box background colour-error inline-block radius-b">
            Something isn't right. Please check for errors.
            <a href="" title="Dismiss" class="close">&times;</a>
        </div>
    </div>

@endif