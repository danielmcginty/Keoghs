
<?php // "Back to Top" Link ?>
<a href="#top" data-back-to-top title="Back to Top" style="display: none; z-index: 900;" class="back-to-top button colour-black rgba-3 fixed bottom-0 right-0 margin-b20 margin-r20 padding-15 circle">
    <i class="fi-adm fi-adm-arrow-up colour-white block line-height-1 font-size-18 margin-t2-negative"></i>
</a>

<?php // Page Loading Overlay ?>
<div class="js-page-loading-overlay page-loading-overlay fixed top-0 left-0 width-100 height-100" style="display: none; z-index: 999;">
    <div class="table background colour-white rgba-4 width-100 height-100">
        <div class="colour-black text-center table-cell width-100 height-100">
            <i class="block fi-adm fi-adm-loading font-size-30 animated spin-cw animation-duration-2s animation-linear animation-infinite"></i>
            <?php /*<span class="inline-block font-size-14">Loading&hellip;</span>*/ ?>
        </div>
    </div>
</div>

<?php // Dropzone Overlay ?>
<style scoped>

    .dropzone-overlay {
        display: none;
        align-items: center;
        z-index: 99999;
        background: rgba(96, 167, 220, 0.5);
        border: 8px dashed #60a7dc;
    }

    .dropzone-overlay.show {
        display: flex !important;
    }

</style>
<div data-dropzone-overlay class="dropzone-overlay fixed top-0 left-0 width-100 height-100 text-center user-select-none">

    <div class="dropzone width-100">

        <div data-dropzone-text class="colour-white font-size-52 text-center">
            Drop to upload
        </div>

        <div data-dropzone-error style="display: none;" class="margin-b20">
            <div class="inline-block padding-x20 padding-y10 radius background colour-error colour-white font-size-18 text-center">
                Sorry, there has been an error uploading some files. Drag to try again.
            </div>
        </div>

        <div class="previews-container"></div>

    </div>

    <button data-dropzone-close title="Close" style="display: none;" class="absolute top-0 right-0 button colour-white circle padding-15 margin-15">
        <i class="fi-adm fi-adm-close block line-height-1 font-size-22"></i>
        <span class="visually-hidden">Close</span>
    </button>

</div>

@if ( \Auth::check() )
    @push('footer_scripts')
        <script>
            var interval;
            $(document).ready( function(){
                interval = setInterval(auth_check, parseInt('{{ (\Config::get('session.lifetime') * 60000) + 1000 }}'));
            });

            function auth_check() {
                var $sessionTimeoutModal = $('#session-timeout-modal');
                var $sessionTimeoutErrorModal = $('#session-timeout-error-modal');

                $.ajax({
                    url: '{{ route('adm.check-auth') }}',
                    type: "GET",
                    dataType: 'json',
                    success: function( response ){
                        clearInterval(interval);

                        $('input[name="_token"]').val(response.new_csrf);
                        if ( ! response.authenticated ) {
                            if( !$sessionTimeoutModal.is(':visible') ){
                                $(document).on('closed.fndtn.reveal', function(){
                                    $sessionTimeoutModal.foundation('reveal', 'open');
                                    $(document).off('closed.fndtn.reveal');
                                });
                                $('[data-reveal]').foundation('reveal', 'close');
                            } else {
                                $sessionTimeoutModal.foundation('reveal', 'open');
                            }

                            interval = setInterval(auth_check, 5000);
                        } else if ( response.authenticated ) {
                            $sessionTimeoutModal.foundation('reveal', 'close');
                        }
                    },
                    error: function( jqXHR, errorThrown, textStatus ){
                        clearInterval(interval);
                        if( !$sessionTimeoutErrorModal.is(':visible') ){
                            $('[data-reveal]').foundation('reveal', 'close');
                        }
                        $sessionTimeoutErrorModal.foundation('reveal', 'open');
                    }
                });
            }

            $('#session-login').on('submit', function(e) {
                $.ajax({
                    url: '{{ route('adm.login-ajax') }}',
                    type: "POST",
                    data: $('#session-login').serialize(),
                    dataType: 'json',
                    beforeSend: function() {
                        $('#session-login .js-error-message').addClass('none');
                    },
                    success: function( response ){
                        if ( response.authenticated ) {
                            $('input[name="_token"]').val(response.new_csrf);
                            $('#session-timeout-modal').foundation('reveal', 'close');
                            clearInterval(interval);
                        } else {
                            $('#session-login .js-error-message').removeClass('none');
                            $('#session-login .js-error-message').html('Incorrect password. Please try again.');
                        }
                    }
                });
                e.preventDefault();
            });
        </script>
    @endpush
    <div id="session-timeout-modal" class="reveal-modal session-timeout-modal" data-reveal data-options="close_on_background_click:false" >
        <form id="session-login" method="POST" action="{{ route('adm.login') }}" class="padding-x30 padding-t30 padding-b10">
            <div>
                <span class="js-error-message inline-block background colour-error radius padding-x10 padding-y5 font-size-14 margin-b-gutter-full none">
                    Incorrect username or password. Please try again.
                </span>
            </div>
            {{ csrf_field() }}
            <p class="modal-heading font-weight-600 margin-b10">Your session has ended.</p>
            <p class="font-size-14">Please enter your password to verify yourself.</p>
            <hr />
            <label for="password">Your Password</label>
            <div class="table">
                <div class="table-cell width-100 error">
                    <input type="hidden" id="username" name="username" value="{{ \Auth::user()->username }}" />
                    <input type="password" id="password" name="password" placeholder="Password..." required="required" />
                </div>
                <div class="table-cell">
                    <button type="submit" class="button colour-blue gradient round margin-l10"><span>Login</span></button>
                </div>
            </div>
        </form>
    </div>
    <div id="session-timeout-error-modal" class="reveal-modal session-timeout-modal" data-reveal data-options="close_on_bacbkground_clock:false">
        <div class="padding-x30 padding-t30 padding-b10">
            <p class="modal-heading font-weight-600 margin-b10">Your session has ended.</p>
            <p class="font-size-14">Sorry, but your session has ended with an error.</p>
            <p class="font-size-14">Please refresh the page to continue, but note that your changes will be lost.</p>
        </div>
    </div>
@endif

<?php // STYLE GUIDE NAVIGATION ?>

<?php // SYSTEM SCRIPTS ?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js?ver=3.9.1"></script>
<!--<script src="/admin/dist/assets/js/vendor/foundation.min.js"></script>-->
<script src="{{ auto_version('/admin/dist/assets/js/vendor/foundation.js') }}"></script><!-- split up to what you need on a project -->
<script src="{{ auto_version('/admin/dist/assets/js/vendor/foundation.alert.js') }}"></script><!-- split up to what you need on a project -->
<script src="{{ auto_version('/admin/dist/assets/js/vendor/foundation.reveal.js') }}"></script><!-- split up to what you need on a project -->

<?php // VENDOR SCRIPTS ?>
<script src="{{ auto_version('/admin/dist/assets/js/vendor/modernizr.js') }}"></script>
<script src="{{ auto_version('/admin/dist/assets/js/vendor/jquery-ui.min.js') }}"></script>
<script src="{{ auto_version('/admin/dist/assets/js/vendor/jquery.matchHeight-min.js') }}"></script>
<script src="{{ auto_version('/admin/dist/assets/js/vendor/bowser.min.js') }}"></script>
<script src="{{ auto_version('/admin/dist/assets/js/vendor/browser-detect.js') }}"></script>
<script src="{{ auto_version('/admin/dist/assets/js/vendor/iframeResizer.min.js') }}"></script>
<script src="{{ auto_version('/admin/dist/assets/js/vendor/jquery.cookie.js') }}"></script>
<script src="{{ auto_version('/admin/dist/assets/js/vendor/jquery.autogrowtextarea.min.js') }}"></script>
<script src="{{ auto_version('/admin/dist/assets/js/vendor/chosen.jquery.min.js' ) }}"></script>
<script src="{{ auto_version('/admin/dist/assets/js/vendor/jquery.tagsinput.min.js' ) }}"></script>
<script src="{{ auto_version('/admin/dist/assets/js/vendor/spectrum.js') }}"></script>
<script src="{{ auto_version('/admin/dist/assets/js/vendor/jquery-clockpicker.min.js') }}"></script>
<script src="{{ auto_version('/admin/dist/assets/js/vendor/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ auto_version('/admin/dist/assets/js/vendor/ui.multiselect.js') }}"></script>
<script src="{{ auto_version('/admin/dist/assets/js/vendor/jquery.accordion.js') }}"></script>
<script src="{{ auto_version('/admin/dist/assets/js/vendor/cropper.js') }}"></script><?php // http://iamceege.github.io/tooltipster/ ?>
<script src="{{ auto_version('/admin/dist/assets/js/vendor/jquery.form.js') }}"></script>
<script src="{{ auto_version('/admin/dist/assets/js/vendor/jquery.outside-events.min.js') }}"></script>

<?php // BESPOKE SCRIPTS ?>
<script src="{{ auto_version('/admin/dist/assets/js/app.js') }}"></script>

@stack('footer_scripts')

<?php
/* OUTDATED BROWSER CHECK (IE8 and below) */
$browser = false;
preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);
if ( (count($matches)>1) && ($matches[1]<=8) ){
$browser = "under-ie8";
}
define('LTIE9', $browser);
?>
<?php if (LTIE9){ ?>

        <!-- Cookies used to hide message if dismissed -->
<script src="{{ auto_version('/admin/dist/assets/js/vendor/jquery.cookie.js' ) }}"></script>

<!-- ============= Outdated Browser ============= -->
<div id="outdated" class="js-outdated" style="display: none; position: fixed; z-index: 9999; width: 100%; left: 0; bottom: 0; padding: 20px 50px; text-align: center; background-color: #cb463d; color: #fff;">
    <div style="display: table; margin: 0 auto;">
        <div style="display: table-cell; vertical-align: middle;">
            <h3 style="font-size: 20px; color: #fff; margin: 0 10px 10px 0;">Your browser is out-of-date.</h3>
        </div>
        <div style="display: table-cell; vertical-align: middle;">
            <a href="http://outdatedbrowser.com/" target="_blank" class="button" style="display: inline-block; border: 3px solid #fff; font-size: 20px; font-weight: 600; background-color: transparent; padding: 10px 20px; margin: 0 10px 10px;">
                How to update my Browser
            </a>
        </div>
    </div>
    <p style="margin: 15px 0 0 0;">Update your browser to view this website correctly. Upgrading your browser will increase security and improve your experience on all websites.</p>
    <a href="#" class="js-dismiss-outdated" style="position: absolute; top: 0; right: 0; font-size: 20px; color: #fff; text-decoration: none; display: block; padding: 15px;" title="Close Message"><i class="fi-adm fi-adm-menu-close"></i></a>
</div>

<!-- Now with cookies -->
<script>

    /**
     * Outdated Browser message cookie
     */
    var message = $(".js-outdated");

    // If no cookie is set, show the outdated message
    if(message.length){
        if($.cookie('outdated-dismissed') == undefined) {
            message.show();
        }
    }

    // Close outdated message, set cookie (to expire in 1 day)
    $(".js-dismiss-outdated").on("click", function(e) {
        e.preventDefault();

        // Hide message on click
        message.hide();

        // Set cookie to state it has been dismissed (value doesn't matter, just set it.)
        $.cookie('outdated-dismissed', '1', { expires: 1, path: '/' });

    });

</script>

<?php } ?>

</body>
</html>
