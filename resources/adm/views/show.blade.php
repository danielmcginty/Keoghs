@extends('layouts.adm')

@section('page_title', $title)

@section('content')

    <div class="js-section-action-bar-container relative" style="height: 79px;">
        <div class="js-section-action-bar section-action-bar top-0 left-0 width-100">
            <div class="row padding-y-gutter-full">
                <div class="column">
                    <div class="table width-100">
                        <div class="table-cell">
                            <h1 class="font-size-24 font-weight-600 margin-0">
                                {{ $title }}
                                @if( isset($current_language_data) )
                                    <span title="{{ $current_language_data->title }} ({{ $current_language_data->locale }})" class="language-identifier inline-block vertical-top radius padding-x3 padding-b2 font-size-18 line-height-1 margin-l2 margin-t6 opacity-8" style="background-color: #e6e6e6;">
                                        <span class="colour-grey-dark">{{ $current_language_data->locale }}</span>
                                    </span>
                                @endif
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="content-area margin-b-gutter-full">

        <div class="row">
            <div class="column {{ (count($columns['side']) > 0 || (isset($language_links) && !empty($language_links))) ? 'small-9' : 'small-12' }}">

                <div class="edit-main-content section-container background colour-white radius margin-b-gutter-full">

                    <?php
                    // We need to check if errors exist in any of the tabs.
                    // Unfortunately, we can only do this on the view because of the way validation works...
                    $invalid_tabs = [];
                    if( count($errors) > 0 ){
                        foreach( $columns as $group => $fields ){
                            foreach( $fields as $field ){
                                if( $errors->has( $field['field'] ) && !in_array( $group, $invalid_tabs ) ){
                                    $invalid_tabs[] = $group;
                                }
                            }
                        }
                    }
                    ?>

                        <div data-content-tabs class="edit-content-tabs">
                            <div class="table width-100">
                                <div class="table-cell width-100">
                                    <ul data-tab-group="edit-main-content" class="tabs simple-list inline-list font-size-0">
                                        <li class="{{ in_array( 'main', $invalid_tabs ) ? 'has-validation-error' : '' }}">
                                            <a href="#" data-tab-id="content" title="Edit Content" class="active font-size-14 block padding-x-gutter-full padding-y-gutter">Content</a>
                                        </li>
                                        @if ( !empty( $columns['seo'] ) )
                                            <li class="{{ in_array( 'seo', $invalid_tabs ) ? 'has-validation-error' : '' }}">
                                                <a href="#" data-tab-id="seo" title="Edit SEO Details" class="font-size-14 block padding-x-gutter-full padding-y-gutter">SEO</a>
                                            </li>
                                        @endif
                                        @foreach( $columns as $group => $fields )
                                            @if( !in_array( $group, array( 'main', 'side', 'seo' ) ) )
                                                <li class="{{ in_array( $group, $invalid_tabs ) ? 'has-validation-error' : '' }}">
                                                    <a href="#" data-tab-id="{{ str_replace(array(' ', '#'), array('-', ''), strtolower($group)) }}" title="Edit {{ $group }} Details" class="font-size-14 block padding-x-gutter-full padding-y-gutter">{{ $group }}</a>
                                                </li>
                                            @endif
                                        @endforeach
                                        @if(\Auth::user()->userGroup->super_admin && ((isset($system_page_enabled) && $system_page_enabled) || (isset($columns['super_admin']) && !empty($columns['super_admin']))))
                                            <?php
                                            if( $system_page_enabled ){
                                                $path_column = false;
                                                foreach( $columns as $group => $fields ){
                                                    foreach( $fields as $field ){
                                                        if( $field['field'] == 'path' ){ $path_column = $field; }
                                                    }
                                                }
                                            }
                                            ?>
                                            <li>
                                                <a href="#" data-tab-id="super-admin" title="Edit Super Admin Details" class="font-size-14 block padding-x-gutter-full padding-y-gutter">
                                                    Super Admin
                                                </a>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                                @if( isset($item) && !empty($last_updated) )
                                    <div class="table-cell text-right nowrap">
                                        <span class="font-size-12 colour-grey-light margin-r15">
                                            Last edited by {{ $last_updated->who }} {{ \Carbon::parse($last_updated->changed)->format("d/m/Y H:ia") }}
                                        </span>
                                    </div>
                                @endif
                            </div>
                        </div>


                        <div data-tab-group="edit-main-content" class="tabs-content">

                            <div <?php // id="content" ?> data-tab-id="content" class="content active">

                                @foreach ($columns['main'] as $column)
                                    @include('fields.view_fields')
                                @endforeach

                            </div>

                        </div>

                </div>

            </div>

            <div class="column small-3"></div>
            @if( isset($language_links) && !empty($language_links) )
                <div class="language-select section-container background colour-white radius margin-b-gutter-full font-size-14 overflow-hidden">
                    <ul class="simple-list margin-0">
                        @foreach( $language_links as $language )
                            <li>
                                <a href="{{ str_replace('edit', 'show', $language['url']) }}" data-language-select="{{ $language['locale'] }}" title="Edit {{ $language['title'] }}" class="button colour-white block text-left margin-0 <?php if( $language['current']){ ?>current<?php } ?> <?php if( $language['status']){ ?> active<?php } ?>">
                                    {{ $language['title'] }}
                                    <span class="status circle" title="Active"></span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            </div>

        </div>

    </div>

@stop