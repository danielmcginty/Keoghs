@if( $show_analytics )

    @if( $analytics_available )

        <div class="padding-gutter-full">

            <div class="table width-100 font-size-13">
                <div class="table-cell">
                    Conversion Rate
                </div>
                <div class="table-cell text-right">
                    <span class="font-size-10 colour-grey">Please note that all stats are subjective to sessions.</span>
                </div>
            </div>
            <hr class="margin-t-gutter margin-b-gutter-full" />

            <div class="table width-100 margin-b-gutter-full">
                <div class="table-cell vertical-top line-height-1 nowrap">
                    <div class="margin-r40">
                        <span class="font-size-34 font-weight-600 inline-block vertical-top margin-r5">{{ $analytics_data['conversionRate'] }}%</span>
                        @if( $analytics_data['conversionRate'] >= $analytics_data['prevConversionRate'] )
                            <span class="inline-block vertical-top colour-success margin-t2">
                                <i class="fi-adm fi-adm-solid-arrow-up font-size-14 relative inline-block vertical-middle" style="top: -1px;"></i> {{ $analytics_data['conversionRate'] - $analytics_data['prevConversionRate'] }}%
                            </span>
                        @else
                            <span class="inline-block vertical-top colour-blue margin-t2">
                                <i class="fi-adm fi-adm-solid-arrow-down font-size-14 relative inline-block vertical-middle" style="top: -1px;"></i> {{ $analytics_data['prevConversionRate'] - $analytics_data['conversionRate'] }}%
                            </span>
                        @endif
                        <div class="font-size-12 colour-grey margin-t3">Conversion Rate - Last 30 Days</div>
                    </div>
                </div>
                @if( !empty( $analytics_data['goal_data'] ) )
                    <div class="table-cell vertical-top font-size-14 line-height-1 width-100">
                        <div class="inline-block margin-r30">
                            <div class="table">
                                @foreach( array_slice( $analytics_data['goal_data'], 0, 2, true ) as $goal_id => $values )
                                    <div class="table-row">
                                        <div class="table-cell">
                                            <span class="inline-block margin-b-gutter margin-r5">
                                                <span class="font-weight-600">{{ $values['count'] }}</span>
                                                {{ $values['count'] == 1 ? \Illuminate\Support\Str::singular($values['name']) : $values['name'] }}: &pound;{{ $values['value'] }}
                                            </span>
                                        </div>
                                        <div class="table-cell">
                                            <span class="inline-block margin-b-gutter">
                                                @if( $values['current'] >= $values['previous'] )
                                                    <span class="colour-success margin-l5">
                                                        <i class="fi-adm fi-adm-solid-arrow-up font-size-14 relative inline-block vertical-middle relative" style="top: -1px;"></i>
                                                        {{ $values['current'] - $values['previous'] }}%
                                                    </span>
                                                @elseif( $values['current'] == $values['previous'] )
                                                    <span class="colour-warning margin-l5">
                                                        <i class="fi-adm fi-adm-solid-arrow-right font-size-14 relative inline-block vertical-middle relative" style="top: -1px;"></i>
                                                        {{ $values['previous'] - $values['current'] }}%
                                                    </span>
                                                @else
                                                    <span class="colour-blue margin-l5">
                                                        <i class="fi-adm fi-adm-solid-arrow-down font-size-14 relative inline-block vertical-middle"></i>
                                                        {{ $values['previous'] - $values['current'] }}%
                                                    </span>
                                                @endif
                                            </span>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        @if( sizeof( $analytics_data['goal_data'] ) > 2 )
                            <div class="inline-block">
                                <div class="table">
                                    @foreach( array_slice( $analytics_data['goal_data'], 2, 2, true ) as $goal_id => $values )
                                        <div class="table-row">
                                            <div class="table-cell">
                                                <span class="inline-block margin-b-gutter margin-r5">
                                                    <span class="font-weight-600">{{ $values['count'] }}</span>
                                                    {{ $values['count'] == 1 ? \Illuminate\Support\Str::singular($values['name']) : $values['name'] }}: &pound;{{ $values['value'] }}
                                                </span>
                                            </div>
                                            <div class="table-cell">
                                                <span class="inline-block margin-b-gutter">
                                                    @if( $values['current'] >= $values['previous'] )
                                                        <span class="colour-success margin-l5">
                                                            <i class="fi-adm fi-adm-solid-arrow-up font-size-14 relative inline-block vertical-middle relative" style="top: -1px;"></i>
                                                            {{ $values['current'] - $values['previous'] }}%
                                                        </span>
                                                    @elseif( $values['current'] == $values['previous'] )
                                                        <span class="colour-warning margin-l5">
                                                            <i class="fi-adm fi-adm-solid-arrow-right font-size-14 relative inline-block vertical-middle relative" style="top: -1px;"></i>
                                                            {{ $values['previous'] - $values['current'] }}%
                                                        </span>
                                                    @else
                                                        <span class="colour-blue margin-l5">
                                                            <i class="fi-adm fi-adm-solid-arrow-down font-size-14 relative inline-block vertical-middle"></i>
                                                            {{ $values['previous'] - $values['current'] }}%
                                                        </span>
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    </div>

                    <div class="table-cell vertical-top font-size-14 colour-grey line-height-1pt3 text-right nowrap">
                        <div class="inline-block text-left">
                            <div class="font-weight-600">Unsure about these stats?</div>
                            <a href="mailto:support@bespokedigital.agency" class="colour-grey underline-on-hover">Contact <?php if ($relationship_manager) { ?>your Relationship Manager<?php } else { ?>the Support Desk<?php } ?></a>
                        </div>
                    </div>
                @endif
            </div>

            <div data-analytics-graph-placeholder style="height: 150px"></div>

        </div>

    @else

        <div class="relative text-center padding-gutter-full" style="height: 150px;">
            <div class="table width-100 height-100">
                <div class="table-cell">
                    <i class="fi-adm fi-adm-timeline font-size-54 line-height-1 colour-grey-lightest"></i>
                    <div>Sorry, analytics data is currently unavailable.</div>
                </div>
            </div>
        </div>

    @endif

@else

    <div class="relative hover-fade fade-7" style="min-height: 120px;">

        <div class="example-analytics-graph padding-gutter-full">
            <img src="/admin/dist/assets/ui/placeholders/analytics-placeholder.png" alt="" class="width-100" />
        </div>

        <a href="mailto:support@bespokedigital.agency" title="Email <?php if ($relationship_manager) { ?>your Relationship Manager<?php } else { ?>the Support Desk<?php } ?>" class="overlay absolute top-0 left-0 width-100 height-100 background colour-white rgba-9 text-center radius">
            <div class="table width-100 height-100">
                <div class="table-cell vertical-middle">
                    <h3 class="margin-b-gutter">Gain an overview of your Conversions</h3>
                    Contact <?php if ($relationship_manager) { ?>your Relationship Manager<?php } else { ?>the Support Desk<?php } ?> for more information.
                </div>
            </div>
        </a>

    </div>

@endif

@if( $show_analytics && $analytics_available )

    <?php /*

    Flot Graphing Library
    Source: https://github.com/flot/flot, http://www.flotcharts.org/

*/ ?>
    <script type="text/javascript" src="/admin/dist/assets/js/vendor/jquery.flot.js"></script>
    <script type="text/javascript" src="/admin/dist/assets/js/vendor/jquery.flot.time.js"></script>
    <script type="text/javascript" src="/admin/dist/assets/js/vendor/jquery.flot.resize.js"></script>

    <script>
        $(document).ready(function(){

            var analyticsGraphPlaceholder = $('[data-analytics-graph-placeholder]');

            // Create Flot
            var plot = $.plot(
                    analyticsGraphPlaceholder,
                    [
                        {
                            label: 'Sessions',
                            data: {!! json_encode( $analytics_data['session_data'] ) !!},
                            color: '#058dc7'
                        },
                        {{--{--}}
                        {{--label: 'Conversions',--}}
                        {{--data: {!! json_encode( $analytics_data['conversion_data'] ) !!},--}}
                        {{--color: '#71c341'--}}
                        {{--}--}}
                    ],
                    {
                        grid: {
                            hoverable: true,
                            color: '#ddd',
                            axisMargin: 5,
                            borderWidth: {
                                top: 0,
                                left: 0,
                                right: 0,
                                bottom: 1
                            }
                        },
                        xaxis: {
                            mode: 'time',
                            minTickSize: [1, "day"],
                            ticks: 4,
                            timeformat: "%e %b %Y",
                            color: '#fff'
                        },
                        yaxis: {
                            position: 'left',
                            color: '#eee',
                            ticks: 2
                        },
                        colors: ["#FF7070", "#0022FF"],
                        series: {
                            lines: {
                                show: true,
                                fill: true,
                                fillColor: {
                                    colors: [
                                        {
                                            opacity: 0.05
                                        },
                                        {
                                            opacity: 0.25
                                        }
                                    ]
                                }
                            },
                            points: {
                                show: true
                            },
                            shadowSize: 0
                        }
                    }
            );

            // Create tooltip for Flot
            $("<div id='tooltip' class='absolute border-1 colour-grey-light font-size-12'><div class='colour-white background rgba-8 padding-y5 padding-x10'><div data-tooltip-text></div></div></div>").css('display', 'none').appendTo("body");

            // Bind tooltip to existing Flot
            analyticsGraphPlaceholder.bind('plothover', function( evt, pos, item ){
                if( item != null ) {
                    var item_data = item.datapoint;

                    var time = item_data[0];
                    var value = item_data[1];

                    var point_date = new Date( time );

                    var tooltip_label = '<b>' + point_date.toDateString() + '</b>';
                    tooltip_label += '<br/>';
                    tooltip_label += item.series.label + ': ' + '<b>' + value + '</b>';

                    var tooltip_style = {
                        top: item.pageY + 5,
                        left: item.pageX + 5
                    };

                    $("#tooltip").css(tooltip_style).fadeIn(200).find('[data-tooltip-text]').html( tooltip_label );

                } else {

                    $('#tooltip').hide();

                }

            });

        });

    </script>

@endif