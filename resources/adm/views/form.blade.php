@extends('layouts.adm')

@section('page_title', $title)

@section('content')

    <!-- BEGIN FORM-->
    @if (!empty($copy))
        {!! Form::model($item, ['url' => route('adm.path', [$table, 'store'])]) !!}
    @elseif (!empty($item))
        <?php
        $form_attributes = array(
            $table,
            $parameters['action'] == 'edit' ? 'update' : 'store'
        );
        if( isset($parameters['id']) ){ $form_attributes[] = $parameters['id']; }
        if( isset($parameters['locale']) ){ $form_attributes[] = $parameters['locale']; }
        ?>
        {!! Form::model($item, ['url' => route('adm.path', $form_attributes) , 'method' => 'PATCH']) !!}
    @else
        <?php
        $form_attributes = array(
                $table,
                'store'
        );
        if( isset($parameters['id']) ){ $form_attributes[] = $parameters['id']; }
        if( isset($parameters['locale']) ){ $form_attributes[] = $parameters['locale']; }
        ?>
        {!! Form::open(['url' => route('adm.path', $form_attributes)]) !!}
    @endif

        <div class="js-section-action-bar-container relative" style="height: 79px;">
            <div class="js-section-action-bar section-action-bar top-0 left-0 width-100">
                <div class="row padding-y-gutter-full">
                    <div class="column">
                        <div class="table width-100">
                            <div class="table-cell">
                                <h1 class="font-size-24 font-weight-600 margin-0">
                                    {{ $title }}
                                    @if( isset($current_language_data) )
                                        <span title="{{ $current_language_data->title }} ({{ $current_language_data->locale }})" class="language-identifier inline-block vertical-top radius padding-x3 padding-b2 font-size-18 line-height-1 margin-l2 margin-t6 opacity-8" style="background-color: #e6e6e6;">
                                            <span class="colour-grey-dark">{{ $current_language_data->locale }}</span>
                                        </span>
                                    @endif
                                </h1>
                            </div>
                            <div class="table-cell text-right">
                                @if( isset($cancel_action) )
                                    <div class="inline-block vertical-middle margin-r-gutter">
                                        <a href="{{ $cancel_action }}" class="button round bordered colour-cancel margin-b0 margin-b0">Cancel</a>
                                    </div>
                                @endif

                                <?php /*<a href="#" class="button round gradient bordered colour-info margin-b0 margin-r-gutter margin-b0">Preview</a>*/ ?>

                                <div class="inline-block vertical-middle">
                                    <div data-save-entry-button data-dropdown-button class="save-entry-button dropdown-button button round gradient bordered colour-success margin-0 outline-none inline-block vertical-middle relative padding-0">
                                        <div class="table width-100">
                                            <div class="table-cell">
                                                <span data-save-entry onclick="$(this).parents('form').submit();" class="main-action margin-0 relative z-index-2 inline-block padding-l30 padding-r25 padding-y10">Save</span>
                                            </div>
                                            <div class="table-cell">
                                                <span data-button-dropdown-toggle title="View Options" class="dropdown-toggle block padding-l10 padding-r15 padding-y10 relative z-index-2">
                                                    <i class="fi-adm fi-adm-triangle-down font-size-8 colour-white inline-block vertical-middle"></i>
                                                </span>
                                                <div data-button-dropdown class="none dropdown absolute top-100 right-0 nowrap z-index-2">
                                                    <ul class="simple-list margin-0 radius overflow-hidden background colour-white" style="border: 1px solid #e4e8f0;">
                                                        <li><a data-save-entry-go-back href="#" onclick="$(this).parents('form').append('<input type=\'hidden\' name=\'save_and_go_back\' value=\'1\' />'); $(this).parents('form').submit(); return false;" class="button colour-white margin-0 padding-x15 padding-y-gutter">Save and Go Back</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php // Edit View Main Content ?>
        <div class="content-area margin-b-gutter-full">

            <div class="row">
                <div class="column small-9">
                    <div class="edit-main-content section-container background colour-white radius margin-b-gutter-full">

                        <?php
                            // We need to check if errors exist in any of the tabs.
                            // Unfortunately, we can only do this on the view because of the way validation works...
                            $invalid_tabs = [];
                            if( count($errors) > 0 ){
                                foreach( $columns as $group => $fields ){
                                    foreach( $fields as $field ){
                                        if( $errors->has( $field['field'] ) && !in_array( $group, $invalid_tabs ) ){
                                            $invalid_tabs[] = $group;
                                        }
                                    }
                                }
                            }
                        ?>
                        <div data-content-tabs class="edit-content-tabs">
                            <div class="table width-100">
                                <div class="table-cell width-100">
                                    <ul data-tab-group="edit-main-content" class="tabs simple-list inline-list font-size-0">
                                        <li class="{{ in_array( 'main', $invalid_tabs ) ? 'has-validation-error' : '' }}">
                                            <a href="#" data-tab-id="content" title="Edit Content" class="active font-size-14 block padding-x-gutter-full padding-y-gutter">Content</a>
                                        </li>
                                        @if ( !empty( $columns['seo'] ) )
                                            <li class="{{ in_array( 'seo', $invalid_tabs ) ? 'has-validation-error' : '' }}">
                                                <a href="#" data-tab-id="seo" title="Edit SEO Details" class="font-size-14 block padding-x-gutter-full padding-y-gutter">SEO</a>
                                            </li>
                                        @endif
                                        @foreach( $columns as $group => $fields )
                                            @if( !in_array( $group, array( 'main', 'side', 'seo', 'super_admin' ) ) )
                                                <li class="{{ in_array( $group, $invalid_tabs ) ? 'has-validation-error' : '' }}">
                                                    <a href="#" data-tab-id="{{ str_replace(array(' ', '#'), array('-', ''), strtolower($group)) }}" title="Edit {{ $group }} Details" class="font-size-14 block padding-x-gutter-full padding-y-gutter">{{ $group }}</a>
                                                </li>
                                            @endif
                                        @endforeach
                                        @if(\Auth::user()->userGroup->super_admin && ((isset($system_page_enabled) && $system_page_enabled) || (isset($columns['super_admin']) && !empty($columns['super_admin']))))
                                            <?php
                                                if( $system_page_enabled ){
                                                    $path_column = false;
                                                    foreach( $columns as $group => $fields ){
                                                        foreach( $fields as $field ){
                                                            if( $field['field'] == 'path' ){ $path_column = $field; }
                                                        }
                                                    }
                                                }
                                            ?>
                                            <li>
                                                <a href="#" data-tab-id="super-admin" title="Edit Super Admin Details" class="font-size-14 block padding-x-gutter-full padding-y-gutter">
                                                    Super Admin
                                                </a>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                                @if( isset($item) && !empty($last_updated) )
                                <div class="table-cell text-right nowrap">
                                    <span class="font-size-12 colour-grey-light margin-r15">
                                        Last edited by {{ $last_updated->who }} {{ \Carbon::parse($last_updated->changed)->format("d/m/Y g:ia") }}
                                    </span>
                                </div>
                                @endif
                            </div>
                        </div>

                        <div data-tab-group="edit-main-content" class="tabs-content">

                            <div <?php // id="content" ?> data-tab-id="content" class="content active">

                                @foreach ($columns['main'] as $column)
                                    @include('fields.fields')
                                @endforeach

                            </div>

                            @foreach( $columns as $group => $fields )
                                @if( !in_array( $group, array( 'main', 'side', 'seo', 'super_admin' ) ) )
                                    <div id="{{ str_replace(array(' ', '#'), array('-', ''), strtolower($group)) }}" data-tab-id="{{ str_replace(array(' ', '#'), array('-', ''), strtolower($group)) }}" class="content">
                                    @foreach ($columns[$group] as $column)
                                        @include('fields.fields')
                                    @endforeach

                                    </div>
                                @endif
                            @endforeach

                            @if( !empty( $columns['seo'] ) )
                                <div id="seo" data-tab-id="seo" class="content">

                                    @foreach ($columns['seo'] as $column)
                                        @include('fields.fields')
                                    @endforeach

                                </div>
                            @endif

                            @if(\Auth::user()->userGroup->super_admin)
                                <div id="super-admin" data-tab-id="super-admin" class="content">
                                    @if( $system_page_enabled && $path_column !== false )

                                        <div class="input-row">
                                            <label for="path[system_page]">Is this a System Page?</label>

                                            <div class="table margin-b15">
                                                <div class="table-cell width-100">
                                                    <label for="path[system_page]" title="Is this a System Page?" class="fancy-switch inline-block font-size-22 fixed-font-size">
                                                        {!! Form::checkbox('path[system_page]', 1, $path_column['values']->system_page, [ 'class' => 'visually-hidden', 'id' => 'path[system_page]' ]) !!}
                                                        <span class="switch"><span class="toggle"></span></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="input-row">
                                            <label for="path[controller]">Route</label>
                                            {!! Form::text('path[route]', $path_column['values']->route) !!}
                                        </div>

                                    @endif

                                    @if( isset( $columns['super_admin'] ) && !empty( $columns['super_admin'] ) )
                                        @foreach ($columns['super_admin'] as $column)
                                            @include('fields.fields')
                                        @endforeach
                                    @endif
                                </div>
                            @endif

                        </div>

                    </div>

                </div>
                <div class="column small-3">

                    @if( isset($language_links) && !empty($language_links) )
                        <div class="language-select section-container background colour-white radius margin-b-gutter-full font-size-14 overflow-hidden">
                            <ul class="simple-list margin-0">
                                @foreach( $language_links as $language )
                                <li>
                                    <a href="{{ $language['url'] }}" data-language-select="{{ $language['locale'] }}" title="Edit {{ $language['title'] }}" class="button colour-white block text-left margin-0 <?php if( $language['current']){ ?>current<?php } ?> <?php if( $language['status']){ ?> active<?php } ?>">
                                        {{ $language['title'] }}
                                        <span class="status circle" title="Active"></span>
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if ( count($columns['side']) )
                        <div class="content-sidebar section-container background colour-white radius padding-gutter-full padding-b0 margin-b-gutter-full">

                            <?php $is_sidebar = true; ?>
                            @foreach ($columns['side'] as $column)
                                @include('fields.fields')
                            @endforeach

                        </div>
                    @endif

                    @if( isset($item) )
                    <div class="change-log radius padding-gutter-full">
                        <p class="font-size-14 font-weight-600 margin-b-gutter colour-grey-dark">Change Log</p>
                        <div class="table font-size-12 colour-grey">
                            <div class="table-row padding-b5">
                                <div class="table-cell padding-r-gutter">
                                    Date Created:
                                </div>
                                <div class="table-cell">
                                    {{ \Carbon::parse($item->created_at)->format("d/m/Y g:ia") }}
                                </div>
                            </div>
                            @if( !empty($last_updated) )
                            <div class="table-row">
                                <div class="table-cell vertical-top padding-r-gutter">
                                    <span class="block margin-t5">Last Updated:</span>
                                </div>
                                <div class="table-cell vertical-top">
                                    <span class="block margin-t5">
                                        <span class="block">{{ \Carbon::parse($last_updated->changed)->format("d/m/Y g:ia") }}</span>
                                        By <span class="font-weight-600">{{ $last_updated->who }}</span>
                                    </span>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>

                        @if( !empty( $content_versions ) )
                            <div class="change-log radius padding-gutter-full margin-t-gutter-full">
                                <p class="font-size-14 font-weight-600 margin-b-gutter colour-grey-dark">Other Versions</p>
                                <label class="none" for="previous-version">Select a Version</label>
                                <select data-previous-version-select id="previous-version" class="width-100">
                                    @foreach( $content_versions as $version => $created_at )
                                        <option value="{{ $version }}">Version #{{ $version }} - {{ $created_at }}</option>
                                    @endforeach
                                </select>
                                <div class="text-right">
                                    <a href="#" data-reinstate-previous-version-btn class="button round bordered colour-primary margin-b0">Restore Version</a>
                                </div>
                            </div>
                        @endif

                    @endif

                </div>
            </div>

        </div>

    {{ Form::close() }}
    <!-- END FORM -->


    <?php // ASSET MANAGER MODAL ?>
    <div id="asset-manager-modal" class="reveal-modal asset-manager-modal" data-reveal data-scroll-lock aria-hidden="true" role="dialog" style="top: 50px;">

        <div class="js-modal-form-flash top-notification relative top-0 left-0 width-100 text-center">
            <div class="alert-box alert alert-dismissable margin-b0 bottom-0" style="display: none;"></div>
            <div class="alert-box success alert-dismissable margin-b0 bottom-0" style="display: none;"></div>
        </div>

        <div class="js-asset-manager-modal-innards">

            <?php // Loading State (replaced with ajax content) ?>
            <div class="loading-state text-center" style="height: 580px;">
                <div class="table width-100 height-100">
                    <div class="table-cell">
                        <i class="fi-adm fi-adm-loading fi-spin font-size-22"></i>
                    </div>
                </div>
            </div>

        </div>

        <div class="modal-actions margin-t1-negative relative z-index-3 padding-15 text-center">
            <a href="#" data-close-reveal class="button round bordered colour-cancel margin-0 padding-x15 padding-y8">Close</a>
        </div>

        <a class="close-reveal-modal visually-hidden" aria-label="Close">&#215;</a>

    </div>

    {{--
    <div class="js-active-users active-users radius fixed bottom-0 right-0 margin-b-gutter-full margin-r-gutter-full padding-gutter-full pointer-events-none none">
        <div class="table line-height-1pt2 colour-white">
            <div class="table-cell vertical-top">
                <i class="fi-adm fi-adm-account block font-size-30 margin-r-gutter"></i>
            </div>
            <div class="table-cell vertical-top js-active-users-message">
                Robin and Connor are also editing this is page.
            </div>
        </div>
        <div class="font-size-14 colour-grey-light margin-t10">Their changes may overwrite your own.</div>
    </div>
    --}}

    {{-- Previous Version Modal --}}
    @if( !empty( $content_versions ) )
    <div id="previous-version-modal" class="reveal-modal tiny" data-reveal data-scroll-lock aria-hidden="true" role="dialog" style="top: 50px;">

        <div class="text-center padding-gutter-full">
            <h3>Restore Content Version</h3>
            <p class="margin-0">Are you sure you want to restore <span data-version-date></span>?</p>
        </div>

        <div class="modal-actions margin-t1-negative relative z-index-3 padding-15 text-center">
            <a href="#" data-confirm-restore-version-btn data-table="{{ $table_name }}" data-id="{{ $item_id }}" data-language="{{ $current_language }}" class="button round bordered colour-success margin-0 padding-x15 padding-y8">Yes, Restore Version</a>
            <a href="#" data-close-reveal class="button round bordered colour-cancel margin-0 padding-x15 padding-y8">Close</a>
        </div>

        <a class="close-reveal-modal visually-hidden" aria-label="Close">&#215;</a>

    </div>
    @endif

@stop

@push('footer_scripts')

@if( count($errors) )
<script type="text/javascript">
    $('form .inline-error').each( function(){
        var $error = $(this);
        var $tab = $error.parents('div[data-tab-id]').first();

        var $tab_link = $('a[data-tab-id="' + $tab.data('tab-id'));
        $tab_link.parents('li').first().addClass('has-validation-error');
    });
</script>
@endif

<script>
    var cms_alert_users;

    $(document).ready( function(){
        var location = window.location.href;
        if( location.indexOf( 'edit' ) != -1 ) {
            // Call the cms_alert_ping function every 5 seconds
            setInterval(cms_alert_ping, 5000);
            cms_alert_ping(); // And call it for the first time on load
        }
    });

    function cms_alert_ping(){
        @if( config( 'app.debug' ) )
        return;
        @endif
        var location = window.location.href;

        $.ajax({
            url: '{{ route('adm.ajax', ['ajax_call' => 'ajax_whos_on_this_page']) }}',
            type: "POST",
            data: {
                location: location
            },
            dataType: 'json',
            success: function( message ){
                if ( message.length ) {
                    $('.js-active-users').removeClass('none');

                    $('.js-active-users-message').html(message)
                } else {
                    $('.js-active-users').addClass('none');
                }
            }
        });
    }

</script>

<script>
    var formSubmitting = false;
    var edited = false;

    $('form').on('submit', function () {
        $('.top-notification [data-alert]').hide();
        formSubmitting = true;
    });

    setTimeout( function() {
        $('form').on('change', function () {
            edited = true;
        });
    }, 1000 );

    window.onload = function() {
        window.addEventListener("beforeunload", function (e) {

            if (formSubmitting || !edited) {
                return undefined;
            }

            var confirmationMessage = 'It looks like you have been editing something. '
                    + 'If you leave before saving, your changes will be lost.';

            (e || window.event).returnValue = confirmationMessage; //Gecko + IE
            return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
        });
    };
</script>

@if( !empty( $content_versions ) )
<script>
$(document).on('click', '[data-reinstate-previous-version-btn]', function( evt ){
    evt.preventDefault();
    evt.stopPropagation();

    var $select = $('[data-previous-version-select]');
    var $modal = $('#previous-version-modal');

    var version_number = $select.val();
    var version_date = $select.find('option:selected').text();

    $modal.find('[data-version-date]').text( version_date );

    $modal.off( 'click', '[data-confirm-restore-version-btn]' );
    $modal.on( 'click', '[data-confirm-restore-version-btn]', function( evt ){
        evt.preventDefault();
        evt.stopPropagation();

        var $btn = $(this);

        $.ajax({
            url: '{{ route('adm.ajax', 'ajax_restore_content_version') }}',
            type: 'POST',
            data: {
                table_name: $btn.data('table'),
                item_id: $btn.data('id'),
                language_id: $btn.data('language'),
                version: version_number
            },
            dataType: 'json',
            beforeSend: function(){
                $btn.html( '<span class="margin-r-gutter">Restoring...</span><i class="fi fi-loading fi-spin"></i>' );
            },
            success: function( json ){
                if( json.success ){
                    edited = false;
                    window.location.reload();
                } else {

                }
            },
            complete: function(){
                $btn.text( 'Yes, Restore Version' );
            }
        });
    });

    $modal.foundation( 'reveal', 'open' );
});
</script>
@endif

@if( $wysiwygEnabled )
<?php // TinyMCE - https://www.tinymce.com/ ?>
<script src="/admin/dist/assets/plugins/tinymce/tinymce.min.js"></script>

<script type="text/javascript">
var TINYMCE_CONFIG_SETS = [];
</script>

<script src="/admin/dist/assets/plugins/tinymce/tinymce.config.js"></script>
@include('dynamic-scripts.tinymce.tinymce-configs')

<script type="text/javascript">
function initialize_tinymce(){
    for( var i = 0; i < TINYMCE_CONFIG_SETS.length; i++ ){
        var config = TINYMCE_CONFIG_SETS[ i ];
        tinymce.init( config );
    }
}
// Destroy all instances of TinyMCE editors
function rebuild_tinymce(){
    // Because tinymce doesn't have a destroy() function, we need to manually clear the editor instances that
    // it current has
    tinymce.EditorManager.editors = [];

    // Now the instances are removed, we can reinitialize TinyMCE with the configurations again
    initialize_tinymce();
}
initialize_tinymce();
</script>
@endif

@include( 'dynamic-scripts.page-builder.page-builder-sortable-reindexer' )
@if( $pageBuilderEnabled )
    @include('dynamic-scripts.page-builder')
@endif

@include('dynamic-scripts.form-asset-manager')

@include('dynamic-scripts.table-builder')
@include('dynamic-scripts.form-builder')
@include('dynamic-scripts.asset-gallery')
@include('dynamic-scripts.repeatable-field')

<script type="text/javascript">
// Internal/External links
$(document).on('change', '[data-link-selector] [data-link-toggler]', function(){
    var $toggler = $(this);
    var $selector = $toggler.parents('[data-link-selector]').first();

    $selector.find('[data-internal-link]:not(.visually-hidden)').addClass('visually-hidden');
    $selector.find('[data-external-link]:not(.visually-hidden)').addClass('visually-hidden');
    $selector.find('[data-file-link]:not(.visually-hidden)').addClass('visually-hidden');
    if( $toggler.val() == 'internal' ){
        $selector.find('[data-internal-link]').removeClass('visually-hidden');
    } else if( $toggler.val() == 'external' ) {
        $selector.find('[data-external-link]').removeClass('visually-hidden');
    } else if( $toggler.val() == 'file' ) {
        $selector.find('[data-file-link]').removeClass('visually-hidden');
    }
});
</script>

<?php // If we have a set of fields to toggle ?>
@if( !empty( $toggleFields ) )
    <script type="text/javascript">
        var toggleFields = {!! json_encode( $toggleFields ) !!};

        for( var field in toggleFields ){
            $(document).on('change', '[name="' + field + '"]', function(){
                // Get the name of the current field
                var field_name = $(this).attr('name');
                // And use the field name to get the current field set to toggle
                var properties = toggleFields[ field_name ];
                // Mark that the toggle field isn't set to the right condition.
                var condition_passed = false;
                // And now, detect if the fields should display
                switch( $(this).attr('type') ){
                    case 'checkbox':
                    case 'radio':
                        // If it's a radio button or checkbox, make sure it's the right field AND it's checked
                        if( $(this).is(':checked') && $(this).val() == properties['condition'] ){
                            condition_passed = true;
                        }
                        break;
                    default:
                        // Else, check if the value matches
                        if( $(this).val() == properties['condition'] ){
                            condition_passed = true;
                        }
                        break;
                }

                // Now we can iterate over the fields to toggle
                for( var index in properties['fields'] ){
                    // Get the field name
                    var toggle_field = properties['fields'][index];
                    // Get a jQuery reference to the field
                    var $field = $('[name="' + toggle_field + '"]').parents('.input-row').first();

                    // If our 'base' field (above) passes the display condition, show the toggle fields
                    if( condition_passed ){
                        $field.slideDown();
                    }
                    // Else, hide them
                    else {
                        $field.slideUp();
                    }
                }
            });

            // On load, trigger a change of the toggle fields to show/hide the appropriate fields.
            $('[name="' + field + '"]').trigger('change');
        }

    </script>
@endif

@if( isset( $dynamic_scripts ) && !empty( $dynamic_scripts ) )
@foreach( $dynamic_scripts as $script )
{!! $script !!}
@endforeach
@endif
@endpush
