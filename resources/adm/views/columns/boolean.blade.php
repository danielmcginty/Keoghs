<span class="block font-size-10 nowrap">
    @if( $value == 1 )
        <span class="inline-block vertical-middle circle background colour-success width-1em height-1em margin-r5"></span>
        <span class="inline-block vertical-middle font-size-13">{{ isset( $column['extra_data']['enabled_text'] ) ? $column['extra_data']['enabled_text'] : 'Enabled' }}</span>
    @else
        <span class="inline-block vertical-middle circle background colour-error width-1em height-1em margin-r5"></span>
        <span class="inline-block vertical-middle font-size-13">{{ isset( $column['extra_data']['disabled_text'] ) ? $column['extra_data']['disabled_text'] : 'Disabled' }}</span>
    @endif
</span>
