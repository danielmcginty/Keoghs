@if( !empty($value) )
<div class="table width-100 height-100">
    <div class="table-cell">
        <i class="fi fi-{{ $value }} font-size-18 block" title="{{ ucfirst($value) }}"></i>
    </div>
</div>
@endif