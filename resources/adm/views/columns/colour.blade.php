@if( !empty($value) )
    <div class="line-height-1 nowrap">
        <span class="font-size-16 inline-block vertical-middle margin-r2">
            <span class="radius inline-block width-1em height-1em" title="{{ $value }}" style="background-color: {{ $value }}"></span>
        </span>
        <span class="inline-block vertical-middle">{{ $value }}</span>
    </div>
@endif