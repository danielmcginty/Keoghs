@if( !empty($value) && $value )
    <img src="{{ route('image.resize', ['x30', $value->path]) }}" alt="{{ $value->name }}" />
@endif