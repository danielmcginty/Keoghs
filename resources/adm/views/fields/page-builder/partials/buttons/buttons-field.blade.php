<div data-page-builder-sortable data-field-prefix="{{ $column['field'] }}" class="button-page-block table-builder sortable-table input-row border-b0">

    @include('fields.form.label')
    @include('fields.form.help_text')

    <div class="table width-100 margin-b0">
        <div class="table-row">
            <div class="table-cell heading" style="width: 32px;"></div>
            <div class="table-cell heading width-20" style="width: 250px; min-width: 250px;">Button Text</div>
            <div class="table-cell heading width-20">Button Colour</div>
            <div class="table-cell heading width-60">Button Type</div>
            <div class="table-cell heading">Disabled?</div>
            <div class="table-cell heading">&nbsp;</div>
        </div>
        <div data-sortable-table class="table-body">
            @if( $item && data_get( $item, transform_key( $column['field'] ) ) )
                <?php $buttons = data_get( $item, transform_key( $column['field'] ) ); ?>
            @endif
            @if( isset( $buttons ) && is_array( $buttons ) )
                @foreach( $buttons as $row_index => $button_row )
                    @include('fields.page-builder.partials.buttons.row')
                @endforeach
            @else
                <?php $button_row = []; $row_index = 0; ?>
                @include('fields.page-builder.partials.buttons.row')
            @endif
        </div>
    </div>

    {{-- Include an 'Add Button' row --}}
    <a data-sortable-add-button href="#" class="button round gradient bordered colour-default margin-t-gutter margin-b0 outline-none nowrap">
                <span class="line-height-1" style="text-shadow: none;">
                    <i class="fi-adm fi-adm-plus font-size-12 margin-r5 inline-block vertical-baseline"></i>
                    <span class="inline-block vertical-baseline">Add Button</span>
                </span>
    </a>

    @include('fields.form.error')

</div>