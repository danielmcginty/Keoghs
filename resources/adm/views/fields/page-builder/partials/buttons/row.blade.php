<div data-sortable-row data-field-prefix="{{ $column['field'] }}" class="table-row">
    <div class="table-cell vertical-middle handle-cell background colour-off-white" style="width: 32px;">
        <div data-sortable-table-handle class="drag-handle background colour-off-white cursor-drag text-center" title="Drag to Reorder">
            <i class="fi-adm fi-adm-drag-handle font-size-14 colour-grey-dark"></i>
        </div>
    </div>
    <div class="table-cell vertical-top" style="width: 250px; min-width: 250px;">
        <?php $field_prefix = $column['field'] . '[' . $row_index . ']' . '[text]'; ?>
        {!! Form::text($field_prefix, isset($button_row['text']) ? $button_row['text'] : '', ['id' => $field_prefix, 'class' => 'margin-0', 'style' => 'width: 250px; min-width: 250px;']) !!}
        @if( ( count($errors) > 0 && $errors->has( transform_key( $column['field'] . '[' . $row_index . '][text]' ) ) ) )
            <span class="inline-error margin-b0 margin-t5"> {{ $errors->first( transform_key( $column['field'] . '[' . $row_index . '][text]' ) ) }} </span>
        @endif
    </div>
    <div class="table-cell vertical-top">
        <?php $field_prefix = $column['field'] . '[' . $row_index . ']' . '[colour]'; ?>
        {!! Form::select($field_prefix, $column['colours'], isset($button_row['colour']) ? $button_row['colour'] : '', ['id' => $field_prefix, 'class' => 'margin-0']) !!}
        @if( ( count($errors) > 0 && $errors->has( transform_key( $column['field'] . '[' . $row_index . '][colour]' ) ) ) )
            <span class="inline-error margin-b0 margin-t5"> {{ $errors->first( transform_key( $column['field'] . '[' . $row_index . '][colour]' ) ) }} </span>
        @endif
    </div>
    <div class="table-cell width-100">
        <?php
            $temp_col = $column;

            $temp_col['field'] = $column['field'] . '[' . $row_index . ']';
            $method = \Illuminate\Support\Str::camel( "prepare_" . $reference . "_block" );
            if( method_exists( $block_class, $method ) ){
                $block_class->{$method}( $temp_col, $item );
            }

            $temp_col['field'] = $temp_col['field'] . '[link]';
        ?>
        @include('fields.form.partials.link.link-selector', ['column' => $temp_col])
    </div>
    <div class="table-cell vertical-top padding-t-gutter-full">
        <?php $field_prefix = $column['field'] . '[' . $row_index . ']' . '[disabled]'; ?>
        <input type="checkbox" id="{{ $field_prefix }}-enabled" name="{{ $field_prefix }}" value="1" class="fancy-checkbox margin-0"{{ isset($button_row['disabled']) && $button_row['disabled'] ? ' checked="checked"' : '' }} />
        <label for="{{ $field_prefix }}-enabled" class="font-size-16 fixed-font-size"><span class="font-size-14 line-height-1pt2"></span></label>
    </div>
    <div class="table-cell">
        <a data-sortable-remove-button href="#" class="button small round gradient bordered colour-error margin-b0 outline-none">
            <span>Remove</span>
        </a>
    </div>
</div>