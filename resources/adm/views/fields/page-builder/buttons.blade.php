@if( isset( $groups ) && sizeof( $groups ) > 1 )
    @php $group_indices = array_keys( $groups ); $first_group = reset( $group_indices ); @endphp
    <div data-block-tabs class="edit-content-tabs">
        <div class="table width-100">
            <ul data-tab-group="page-builder-{{ $index }}" class="tabs simple-list inline-list font-size-0">
                @foreach( $groups as $group => $fields )
                    <li>
                        <a href="#" data-tab-id="{{ $group }}" title="{{ ucwords( str_replace( "_", " ", $group ) ) }}" class="{{ $group == $first_group ? 'active ' : '' }}font-size-14 block padding-x-gutter-full padding-y-gutter">
                            {{ ucwords( str_replace( "_", " ", $group ) ) }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    <div data-tab-group="page-builder-{{ $index }}" class="tabs-content">
        @foreach( $groups as $group => $fields )
            <div id="{{ $group }}" data-tab-id="{{ $group }}" class="{{ $group == $first_group ? 'active ' : '' }}content">
                @foreach( $fields as $field_name )
                    @if( isset( $columns[ $field_name ] ) )
                        @php $column = $columns[ $field_name ]; @endphp
                        @if( $column['type'] == 'buttons' )
                            @include('fields.page-builder.partials.buttons.buttons-field')
                        @elseif (!empty($column['type']) && View::exists('fields.form.' . $column['type']))
                            @include('fields.form.' . $column['type'])
                        @else
                            <div class="input-row {{ count($errors) > 0 && $errors->has($column['field']) ? 'has-error' : '' }}">

                                @include('fields.form.label')
                                @include('fields.form.help_text')

                                {!! Form::text($column['field'], null, ['class' => 'form-control', 'placeholder' => $column['label']]) !!}

                                @include('fields.form.error')

                            </div>
                        @endif
                    @endif
                @endforeach
            </div>
        @endforeach
    </div>
@elseif( isset( $columns ) )
    @foreach( $columns as $column )
        @if( $column['type'] == 'buttons' )
            @include('fields.page-builder.partials.buttons.buttons-field')
        @elseif (!empty($column['type']) && View::exists('fields.form.' . $column['type']))
            @include('fields.form.' . $column['type'])
        @else
            <div class="input-row {{ count($errors) > 0 && $errors->has($column['field']) ? 'has-error' : '' }}">

                @include('fields.form.label')
                @include('fields.form.help_text')

                {!! Form::text($column['field'], null, ['class' => 'form-control', 'placeholder' => $column['label']]) !!}

                @include('fields.form.error')

            </div>
        @endif
    @endforeach
@endif