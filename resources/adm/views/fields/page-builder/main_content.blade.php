<div data-page-builder-block class="page-builder-block margin-b-gutter-full">
    <div class="block-inner">

        <input type="hidden" id="page_builder[{{ $index }}][reference]" name="page_builder[{{ $index }}][reference]" value="{{ $reference }}"/>
        <input data-block-status-field type="hidden" id="page_builder[{{ $index }}][status]" name="page_builder[{{ $index }}][status]" value="1" />

        <div class="block-heading relative nowrap no-select font-size-14">
            <div data-page-builder-drag-handle title="Drag to Reorder" class="block-handle cursor-drag cursor-drag-active-state padding-gutter absolute top-0 left-0 colour-grey ui-sortable-handle">
                <i class="fi-adm fi-adm-drag-handle colour-grey-dark"></i>
            </div>
            <div class="block-heading-inner ellipsis colour-grey">
                <span class="block-title font-weight-600 colour-slate margin-r-gutter">{{ $block_title }}</span>
            </div>
            <div class="block-actions absolute top-0 right-0 font-size-0 padding-r5">
                <div data-page-block-controls title="Settings" class="has-dropdown-menu inline-block vertical-top font-size-14">
                    <div class="dropdown-menu-link padding-gutter cursor-pointer hover-fade fade-7">
                        <i class="fi-adm fi-adm-settings colour-grey margin-t1"></i>
                    </div>
                    <div class="dropdown-menu appear-right z-index-5">
                        <div class="inner radius overflow-hidden">
                            <ul class="simple-list margin-0">
                                <li><a href="#" data-block-control data-block-collapse class="button colour-off-white margin-0 text-left font-weight-400">Collapse</a></li>
                                <li><a href="#" data-block-control data-block-expand class="button colour-off-white margin-0 text-left font-weight-400 none">Expand</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <a href="#" data-block-view-toggle="" title="Expand/Collapse Block" class="block-toggle inline-block vertical-top padding-gutter font-size-14">
                    <i class="fi-adm colour-grey margin-t1"></i>
                </a>
            </div>
        </div>

        <div class="block-content">
            @if( isset( $innards ) )
                {!! $innards !!}
            @elseif( isset($columns) )
                @foreach( $columns as $column )
                    @if (!empty($column['type']) && View::exists('fields.form.' . $column['type']))
                        @include('fields.form.' . $column['type'])
                    @else
                        <div class="input-row {{ count($errors) > 0 && $errors->has($column['field']) ? 'has-error' : '' }}">

                            @include('fields.form.label')
                            @include('fields.form.help_text')

                            {!! Form::text($column['field'], null, ['class' => 'form-control', 'placeholder' => $column['label']]) !!}

                            @include('fields.form.error')

                        </div>
                    @endif
                @endforeach
            @endif
        </div>

    </div>
</div>