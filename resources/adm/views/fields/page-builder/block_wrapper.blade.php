@php Form::setModel( $item ); @endphp
<div data-page-builder-block class="page-builder-block margin-b-gutter-full{{ !$is_ajax ? ' is-collapsed' : '' }}{{ !$block_status ? ' is-inactive' : '' }}">
    <div class="block-inner">

        <input type="hidden" id="page_builder[{{ $index }}][reference]" name="page_builder[{{ $index }}][reference]" value="{{ $reference }}"/>
        <input data-block-status-field type="hidden" id="page_builder[{{ $index }}][status]" name="page_builder[{{ $index }}][status]" value="{{ (int)$block_status }}" />

        <div class="block-heading relative nowrap no-select font-size-14">
            <div data-page-builder-drag-handle title="Drag to Reorder" class="block-handle cursor-drag cursor-drag-active-state padding-gutter absolute top-0 left-0 colour-grey ui-sortable-handle">
                <i class="fi-adm fi-adm-drag-handle colour-grey-dark"></i>
            </div>
            <div class="block-heading-inner ellipsis colour-grey">
                <span class="block-title font-weight-600 colour-slate margin-r-gutter">{{ $block_title }}</span>
            </div>
            <div class="block-actions absolute top-0 right-0 font-size-0 padding-r5">
                <div title="This block is disabled" class="block-status text-center relative margin-r-gutter">
                    <span class="status-icon inline-block vertical-middle circle background colour-error margin-r5"></span>
                    <span class="inline-block vertical-middle font-size-12 colour-grey">Disabled</span>
                </div>
                <div data-page-block-controls title="Settings" class="has-dropdown-menu inline-block vertical-top font-size-14">
                    <div class="dropdown-menu-link padding-gutter cursor-pointer hover-fade fade-7">
                        <i class="fi-adm fi-adm-settings colour-grey margin-t1"></i>
                    </div>
                    <div class="dropdown-menu appear-right z-index-5">
                        <div class="inner radius overflow-hidden">
                            <ul class="simple-list margin-0">
                                <li><a href="#" data-block-control data-block-collapse class="button colour-off-white margin-0 text-left font-weight-400{{ !$block_status ? ' none' : '' }}">Collapse</a></li>
                                <li><a href="#" data-block-control data-block-expand class="button colour-off-white margin-0 text-left font-weight-400{{ $block_status ? ' none' : '' }}">Expand</a></li>
                                <li><a href="#" data-block-control data-block-set-status="{{ !$block_status ? 1 : 0 }}" class="button colour-off-white margin-0 text-left font-weight-400">{{ !$block_status ? 'Enable' : 'Disable' }}</a></li>
                                <li><a href="#" data-block-control data-block-remove class="button colour-off-white margin-0 text-left font-weight-400"><span class="colour-error">Remove</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                @if( isset( $columns ) && !empty( $columns ) )
                <a href="#" data-block-view-toggle="" title="Expand/Collapse Block" class="block-toggle inline-block vertical-top padding-gutter font-size-14">
                    <i class="fi-adm colour-grey margin-t1"></i>
                </a>
                @endif
            </div>
        </div>

        @if( isset($innards) || (isset($columns) && !empty($columns) && isset($groups) && !empty($groups)) )
        <div class="block-content">
            @if( isset( $innards ) )
                {!! $innards !!}
            @elseif( isset( $groups ) && sizeof( $groups ) > 1 )
                @php $group_indices = array_keys( $groups ); $first_group = reset( $group_indices ); @endphp
                <div data-block-tabs class="edit-content-tabs">
                    <div class="table width-100">
                        <ul data-tab-group="page-builder-{{ $index }}" class="tabs simple-list inline-list font-size-0">
                            @foreach( $groups as $group => $fields )
                                <li>
                                    <a href="#" data-tab-id="{{ $group }}" title="{{ ucwords( str_replace( "_", " ", $group ) ) }}" class="{{ $group == $first_group ? 'active ' : '' }}font-size-14 block padding-x-gutter-full padding-y-gutter">
                                        {{ ucwords( str_replace( "_", " ", $group ) ) }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div data-tab-group="page-builder-{{ $index }}" class="tabs-content">
                    @foreach( $groups as $group => $fields )
                        <div id="{{ $group }}" data-tab-id="{{ $group }}" class="{{ $group == $first_group ? 'active ' : '' }}content">
                            @foreach( $fields as $field_name )
                                @if( isset( $columns[ $field_name ] ) )
                                    @php $column = $columns[ $field_name ]; @endphp
                                    @if (!empty($column['type']) && View::exists('fields.form.' . $column['type']))
                                        @include('fields.form.' . $column['type'])
                                    @else
                                        <div class="input-row {{ count($errors) > 0 && $errors->has($column['field']) ? 'has-error' : '' }}">

                                            @include('fields.form.label')
                                            @include('fields.form.help_text')

                                            {!! Form::text($column['field'], null, ['class' => 'form-control', 'placeholder' => $column['label']]) !!}

                                            @include('fields.form.error')

                                        </div>
                                    @endif
                                @endif
                            @endforeach
                        </div>
                    @endforeach
                </div>
            @elseif( isset( $columns ) )
                @foreach( $columns as $column )
                    @if (!empty($column['type']) && View::exists('fields.form.' . $column['type']))
                        @include('fields.form.' . $column['type'])
                    @else
                        <div class="input-row {{ count($errors) > 0 && $errors->has($column['field']) ? 'has-error' : '' }}">

                            @include('fields.form.label')
                            @include('fields.form.help_text')

                            {!! Form::text($column['field'], null, ['class' => 'form-control', 'placeholder' => $column['label']]) !!}

                            @include('fields.form.error')

                        </div>
                    @endif
                @endforeach
            @endif
        </div>
        @endif

    </div>
</div>

@if( $is_ajax )
@stack('footer_scripts')
@endif