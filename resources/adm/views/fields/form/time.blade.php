<div class="input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }}{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">

    @include('fields.form.label')
    @include('fields.form.help_text')

    <?php
        $field_attributes = ['data-time-picker' => '', 'id' => $column['field'], 'class' => 'inline-block', 'placeholder' => 'hh:mm', 'data-autoclose' => 'true', 'data-default' => 'now', 'data-donetext' => 'Select Time'];
        if( isset( $column['attributes'] ) && !empty( $column['attributes'] ) ){
            $field_attributes = $field_attributes + $column['attributes'];
        }
    ?>
    {!! Form::text( $column['field'], isset($item) && is_object( $item ) ? null : \Carbon::now()->format('H:i'), $field_attributes ) !!}

    @include('fields.form.error')

</div>