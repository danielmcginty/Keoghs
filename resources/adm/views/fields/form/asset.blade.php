<?php
    $asset_box_size = 123;
?>
<div class="input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }}{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">

    @include('fields.form.label')
    @include('fields.form.help_text')

    <div data-image-select-field class="image-select-field margin-t-gutter-full- margin-b-gutter-full font-size-0">

        @if( isset($column['multi_size']) && $column['multi_size'] )

            @foreach( $column['sizes'] as $reference => $details )
                <?php
                    $field_id = $column['field'] . '-' . $reference . '-' . (rand()*1000);

                    $current_field_name = "current_" . $column['field'];
                    $current_asset = false;
                    if( isset($item) && is_object($item) && isset($item->$current_field_name) ){
                        $sizes = $item->$current_field_name;
                        if( isset( $sizes[ $reference ] ) ){
                            $current_asset = $sizes[ $reference ];
                        }
                    }

                    if( $details['width'] > $details['height'] ){
                        $ratio = $details['height'] / $details['width'];
                        $dimensions = $asset_box_size . 'x' . intval($asset_box_size * $ratio);
                    } elseif( $details['height'] > $details['width'] ){
                        $ratio = $details['width'] / $details['height'];
                        $dimensions = intval($asset_box_size * $ratio) . 'x' . $asset_box_size;
                    } else {
                        $dimensions = $asset_box_size . 'x' . $asset_box_size;
                    }
                ?>
                @if( $current_asset !== false && is_object($current_asset) )
                    @include('fields.form.partials.asset.group-asset')
                @else
                    @include('fields.form.partials.asset.group-asset-empty')
                @endif
            @endforeach

        @else

            <?php
            $field_id = $column['field'] . '-' . (rand()*1000);

            $reference = 'single';

            $current_field_name = "current_" . $column['field'];
            $current_asset = false;
            if( isset($item) && is_object($item) && isset($item->$current_field_name) ){
                $sizes = $item->$current_field_name;
                if( isset( $sizes[ $reference ] ) ){
                    $current_asset = $sizes[ $reference ];
                }
            }

            $dimensions = $asset_box_size . 'x' . $asset_box_size;
            if( $current_asset !== false && is_object( $current_asset ) ){
                if( $current_asset->asset_group->width > $current_asset->asset_group->height ){
                    $ratio = $current_asset->asset_group->height / $current_asset->asset_group->width;
                    $dimensions = $asset_box_size . 'x' . intval($asset_box_size * $ratio);
                } elseif( $current_asset->asset_group->height > $current_asset->asset_group->width ){
                    $ratio = $current_asset->asset_group->width / $current_asset->asset_group->height;
                    $dimensions = intval($asset_box_size * $ratio) . 'x' . $asset_box_size;
                }
            }

            $details = [
                'label' => ''
            ];
            ?>
            @if( $current_asset !== false && is_object($current_asset) )
                @include('fields.form.partials.asset.group-asset')
            @else
                @include('fields.form.partials.asset.group-asset-empty')
            @endif

        @endif

    </div>

    @if( isset($column['multi_size']) && $column['multi_size'] )
        @foreach( $column['sizes'] as $reference => $details )
            <?php $error_col = ['field' => $column['field'] . '[' . $reference . ']']; ?>
            @include('fields.form.error', ['column' => $error_col])
        @endforeach
    @else
        <?php $error_col = ['field' => $column['field'] . '[single]']; ?>
        @include('fields.form.error', ['column' => $error_col])
    @endif

</div>