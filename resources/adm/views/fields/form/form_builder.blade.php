<div data-form-builder-wrapper data-field-name-prefix="{{ $column['field'] }}" class="js-table-builder table-builder sortable-table input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }}{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">

    @include('fields.form.label')
    @include('fields.form.help_text')

    <?php
        $table_rows = [];
        if( isset($item) && is_object( $item ) && isset($item->{$column['field']}) ){
            $table_rows = $item->{$column['field']};
        }
    ?>

    <div class="table width-100 margin-b-gutter overflow-hidden">
        <div class="table-row">
            <div class="table-cell heading"></div>
            <div class="table-cell heading">Field Label</div>
            <div class="table-cell heading">Field Type</div>
            <div class="table-cell heading">
                Field Options
                <span class="block font-size-12">One option per line (&#x23ce; Enter)</span>
            </div>
            <div class="table-cell heading">Field Required?</div>
            @if( \Auth::user()->userGroup->super_admin )
                <div class="table-cell heading">
                    Field Reference
                    <span class="block font-size-12">Used with custom templates to pick out a field</span>
                </div>
            @endif
            <div class="table-cell heading"></div>
        </div>
        <div data-form-builder data-column="{{ json_encode( $column ) }}" class="table-body">
            @if( !empty($table_rows) && is_array($table_rows) )
                @foreach ($table_rows as $row_index => $table_row)
                    @include('fields.form.partials.form-builder.row')
                @endforeach
            @else
                @include('fields.form.partials.form-builder.row', ['row_index' => 0, 'table_row' => []])
            @endif
        </div>
    </div>

    @include('fields.form.error')

    <button class="js-add-row add-row colour-white radius padding-x50 padding-y15 line-height-1">
        <i class="fi-adm fi-adm-add inline-block vertical-middle margin-r-gutter"></i>
        <span class="inline-block vertical-middle">Add New Row</span>
    </button>

</div>