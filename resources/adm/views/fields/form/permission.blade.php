<?php $rand = rand(); ?>
<div class="input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }}{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">

    @include('fields.form.label')
    @include('fields.form.help_text')

    <div class="padding-b-gutter">
        {{-- Catch-All in case no permissions are selected --}}
        {{ Form::hidden( $column['field'], '' ) }}

        {{-- Toggle All Permissions --}}
        <?php
            $all_permissions_checked = true;
            foreach( (array)$column['options'] as $permission ){
                if( !in_array( $permission['url'], $column['selected'] ) ){
                    $all_permissions_checked = false;
                }

                if( isset( $permission['sub'] ) && !empty( $permission['sub'] ) ){
                    foreach( $permission['sub'] as $child_permission ){
                        if( !in_array( $child_permission['url'], $column['selected'] ) ){
                            $all_permissions_checked = false;
                        }
                    }
                }
            }
        ?>
        <div data-permissions-block="{{ $rand }}">
            <div class="margin-b-gutter-full">
                <a data-select-all-permissions href="javascript:void(0)" class="button small round gradient colour-success margin-b0 margin-r-gutter"><span class="colour-white">Select All</span></a>
                <a data-clear-all-permissions href="javascript:void(0)" class="button small round gradient colour-error margin-b0 margin-r-gutter"><span class="colour-white">Clear All</span></a>
            </div>

            @foreach( (array)$column['options'] as $permission )
                <div class="margin-b-gutter">
                    <input data-permission type="checkbox" id="{{ $column['field'] }}-{{ $permission['url'] }}-{{ $permission['position'] }}" name="{{ $column['field'] }}" value="{{ $permission['url'] }}" class="fancy-checkbox margin-0"{{ $column['selected'] && in_array($permission['url'], $column['selected']) ? ' checked="checked"' : '' }} />
                    <label for="{{ $column['field'] }}-{{ $permission['url'] }}-{{ $permission['position'] }}" class="font-size-16 fixed-font-size"><span class="font-size-14 line-height-1pt2">{{ $permission['title'] }}</span></label>
                </div>
                @if ( isset($permission['sub']) && !empty($permission['sub']) )
                    @foreach( $permission['sub'] as $child_permission )
                        <div class="margin-b-gutter margin-l-gutter-full">
                            <input data-permission type="checkbox" id="{{ $column['field'] }}-{{ $child_permission['url'] }}-{{ $child_permission['position'] }}" name="{{ $column['field'] }}" value="{{ $child_permission['url'] }}" class="fancy-checkbox margin-0"{{ $column['selected'] && in_array($child_permission['url'], $column['selected']) ? ' checked="checked"' : '' }} />
                            <label for="{{ $column['field'] }}-{{ $child_permission['url'] }}-{{ $child_permission['position'] }}" class="font-size-16 fixed-font-size"><span class="font-size-14 line-height-1pt2">{{ $child_permission['title'] }}</span></label>
                        </div>
                    @endforeach
                @endif
            @endforeach
        </div>
    </div>

    @include('fields.form.error')

</div>

@push( 'footer_scripts' )
<script type="text/javascript">
$(document).ready( function(){
    $('[data-permissions-block="{{ $rand }}"] [data-select-all-permissions]').click( function() {
        $('[data-permissions-block="{{ $rand }}"]').find('[data-permission]').prop('checked', true).attr('checked', 'checked');
    });
    $('[data-permissions-block="{{ $rand }}"] [data-clear-all-permissions]').click( function() {
        $('[data-permissions-block="{{ $rand }}"]').find('[data-permission]').prop('checked', false).removeAttr('checked');
    });
});
</script>
@endpush