<div class="input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }}{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">

    @if( isset( $column['admin_relation_link'] ) )
    <div class="table width-100 height-100">
        <div class="table-cell nowrap">
            @include('fields.form.label')
        </div>
        <div class="table-cell width-100">
            <a href="{{ $column['admin_relation_link'] }}" target="_blank" class="inline-block font-size-12 margin-b-gutter margin-l15 underline-on-hover">&#43; Add {{ $column['label'] }}</a>
        </div>
    </div>
    @else
        @include('fields.form.label')
    @endif

    @include('fields.form.help_text')

    <div class="multi-relation-input">
        <select data-multi-select multiple id="multi-select-{{ \Illuminate\Support\Str::slug( $column['field']) }}" name="{{ $column['field'] }}[]">
            @foreach($column['selected'] as $k)
                @if (array_key_exists($k,$column['values']))
                    <option value="{{$k}}" selected="selected">{{$column['values'][$k]}}</option>
                    <?php unset($column['values'][$k]) ?>
                @endif
            @endforeach
            @foreach($column['values'] as $k => $opt)
                <option value="{{$k}}">{{$opt}}</option>
            @endforeach
        </select>
    </div>

    @include('fields.form.error')

</div>