<div class="form-group {{ count($errors) > 0 && $errors->has($column['field']) ? 'has-error' : '' }}">

    <div>
        <label for="{{ $column['field'] }}" class="inline-block margin-b-gutter">{{ $column['label'] }}</label>
    </div>
    @include('fields.form.help_text')

    <div class="readonly-field padding-y5 margin-b-gutter-full">
        {!! !empty($item) ? $item->{$column['field']} : '' !!}
    </div>

</div>