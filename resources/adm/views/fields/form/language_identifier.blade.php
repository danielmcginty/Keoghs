@if( isset($current_language_data) )
    <span title="{{ $current_language_data->title }} ({{ $current_language_data->locale }})" class="language-identifier inline-block vertical-top radius padding-x3 padding-y2 font-size-12 line-height-1 margin-l2 margin-t2 opacity-8" style="background-color: #e6e6e6;">
        <span class="colour-grey-dark">{{ $current_language_data->locale }}</span>
    </span>
@endif