<div class="input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }}{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">

    @include('fields.form.label')
    @include('fields.form.help_text')

    <?php
        $field_attributes = [
            'data-auto-expand' => '',
            'id' => $column['field']
        ];
        if( isset( $column['length'] ) && $column['length'] <= 500 ){
            $field_attributes['data-max-length'] = $column['length'];
            $field_attributes['data-max-length-warning-length'] = 25;
        }
        if( isset( $column['attributes'] ) && !empty( $column['attributes'] ) ){
            $field_attributes = $field_attributes + $column['attributes'];
        }
    ?>
    {!! Form::textarea($column['field'], null, $field_attributes) !!}

    @include('fields.form.error')

</div>