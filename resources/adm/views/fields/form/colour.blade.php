<div class="input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }}{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">

    @include('fields.form.label')
    @include('fields.form.help_text')

    <div class="clearfix margin-b-gutter-full-">
        <?php
            $field_attributes = ['id' => $column['field'], 'readonly', 'class' => 'width-auto inline-block vertical-top padding-0 border-0 background colour-white outline-none pointer-events-none', 'data-colour-picker' => '', 'data-show-palette' => 'true', 'data-palette' => (isset($column['options']) ? $column['options'] : '')];
            if( isset( $column['attributes'] ) && !empty( $column['attributes'] ) ){
                $field_attributes = $field_attributes + $column['attributes'];
            }
        ?>
        {!! Form::text( $column['field'], null, $field_attributes) !!}
    </div>
    @include('fields.form.error')

</div>