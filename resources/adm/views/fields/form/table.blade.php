<div data-table-builder-wrapper data-field-name-prefix="{{ $column['field'] }}" class="js-table-builder table-builder sortable-table input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }}{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">

    @include('fields.form.label')
    @include('fields.form.help_text')

    <?php
        $table_rows = [];
        if( $item && is_object( $item ) ){
            $table_rows = data_get( $item, transform_key( $column['field'] ) );

            // Get the first row from the table structure as these will be the column heading cell data
            $table_headers = is_array( $table_rows ) ? array_shift($table_rows) : [];
        }
    ?>

    <div class="table width-100 margin-b-gutter overflow-hidden">
        <div class="table-row">
            <div class="table-cell heading"></div>
            @for ($column_index = 0; $column_index < (int)$column['options']['column_limit']; $column_index ++)
                <?php
                $field_prefix = $column['field'] . '[0][' . $column_index . ']';
                ?>
                <div class="table-cell heading text-center">
                    <label class="font-size-12 text-left" for="{{ $field_prefix }}">Column Heading</label>
                    {!! Form::text($field_prefix, isset($table_headers[$column_index]) ? $table_headers[$column_index] : '', ['id' => $field_prefix, 'class' => 'margin-0']) !!}
                </div>
            @endfor
            <div class="table-cell heading"></div>
        </div>
        <div data-table-builder data-column="{{ json_encode( $column ) }}" class="table-body">
            @foreach ( (array)$table_rows as $row_index => $table_row)
                @include('fields.form.partials.table-builder.row')
            @endforeach
        </div>
    </div>

    @include('fields.form.error')

    <button type="button" class="js-add-row add-row colour-white radius padding-x50 padding-y15 line-height-1">
        <i class="fi-adm fi-adm-add inline-block vertical-middle margin-r-gutter"></i>
        <span class="inline-block vertical-middle">Add New Row</span>
    </button>

</div>