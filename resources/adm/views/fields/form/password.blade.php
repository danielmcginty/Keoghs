<?php
if( isset( $errors ) && count( $errors ) > 0 ){
    if( !$errors->has( transform_key( $column['field'] ) ) ){
        $errors->add( transform_key( $column['field'] ), 'Your previously typed password has been cleared for security reasons, please re-enter it.' );
    }
}
?>
<div class="input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }}{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">

    @include('fields.form.label')
    @include('fields.form.help_text')

    <?php
        $field_attributes = ['id' => $column['field'], 'class' => 'form-control', 'placeholder' => 'Password', 'autocomplete' => 'nope'];
        if( isset( $column['attributes'] ) && !empty( $column['attributes'] ) ){
            $field_attributes = $field_attributes + $column['attributes'];
        }
    ?>
    {!! Form::password($column['field'], $field_attributes) !!}

    @include('fields.form.error')

</div>