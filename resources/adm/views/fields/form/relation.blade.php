<div class="input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }}{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">

    @if( isset( $column['admin_relation_link'] ) )
    <div class="table width-100 height-100">
        <div class="table-cell nowrap">
            @include('fields.form.label')
        </div>
        <div class="table-cell width-100">
            <a href="{{ $column['admin_relation_link'] }}" target="_blank" class="inline-block font-size-12 margin-b-gutter margin-l15 underline-on-hover">&#43; Add {{ $column['label'] }}</a>
        </div>
    </div>
    @else
        @include('fields.form.label')
    @endif

    @include('fields.form.help_text')

    <?php
        $field_attributes = ['data-searchable', 'class' => isset($column['path_prefix_field']) && $column['path_prefix_field'] ? 'js-path-prefix-field form-control select2me width-auto' : 'form-control select2me width-auto max-width-100', 'id' => $column['field'], 'data-path-prefix-table' => isset($column['path_prefix_table']) ? $column['path_prefix_table'] : '', 'data-path-prefix-column' => isset($column['path_prefix_column']) ? $column['path_prefix_column'] : ''];
        if( isset( $column['attributes'] ) && !empty( $column['attributes'] ) ){
            $field_attributes = $field_attributes + $column['attributes'];
        }
    ?>
    {!! Form::select($column['field'], $column['values'], null, $field_attributes) !!}

    @include('fields.form.error')

</div>