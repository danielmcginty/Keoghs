<div class="input-row{{ $column['values']->no_index_meta == 1 ? ' none' : '' }}">
    <label for="meta-title" class="margin-b10">
        Meta Preview
    </label>
    <div data-google-preview class="google-preview margin-b-gutter-full" style="max-width: 600px; font-family: arial, sans-serif;">
        <div style="line-height: 1.57;">
            <div class="inline-block padding-b1 padding-t1 font-size-13">
                <div data-google-favicon class="inline-block vertical-middle padding-b2 margin-r9 line-height-1">
                    <img src="/{{ config('app.theme_name') }}/dist/ui/favicons/favicon-32x32.png" alt="Favicon" style="width: 16px; height: 16px;" />
                </div>
                <div data-google-url class="inline-block vertical-middle ellipsis font-size-14 padding-t1 line-height-1pt3" style="color: #3C4043;">
                    {{ !empty($column['values']->url) ? url( $column['values']->url ) : url('') }}
                </div>
            </div>
            <br/>
            <div data-google-meta-title class="ellipsis inline-block underline-on-hover font-size-20 pointer line-height-1pt3 margin-b3 padding-t2" style="color: #1a0dab;">
                {{ $column['values']->meta_title }}
            </div>
        </div>
        <div style="line-height: 1.57;">
            <div data-google-meta-description class="font-size-14" style="color: #3C4043; word-wrap: break-word;">
                {{ $column['values']->meta_description }}
            </div>
        </div>
    </div>
</div>

<div class="input-row @if( count($errors) && $errors->has('seo_options.meta_title') ) has-error @endif">
    <label for="meta-title">
        Meta Title
        @include('fields.form.language_identifier')
    </label>
    <div class="help-text">The meta title of the page. Shown in search results and the current tab.</div>
    {!! Form::text('seo_options[meta_title]', $column['values']->meta_title, ['id' => 'meta-title']) !!}
    @if( count($errors) && $errors->has('seo_options.meta_title') )
        <span class="inline-error"> {{ $errors->first( 'seo_options.meta_title' ) }} </span>
    @endif
</div>

<div class="input-row @if( count($errors) && $errors->has('seo_options.meta_description') ) has-error @endif">
    <label for="meta-description">
        Meta Description
        @include('fields.form.language_identifier')
    </label>
    <div class="help-text">The meta description of the page. Shown in search results and the current tab.</div>
    {!! Form::textarea('seo_options[meta_description]', $column['values']->meta_description, ['id' => 'meta-description', 'data-max-length' => 320]) !!}
    @if( count($errors) && $errors->has('seo_options.meta_description') )
        <span class="inline-error"> {{ $errors->first( 'seo_options.meta_description' ) }} </span>
    @endif
</div>

<div class="input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }}">

    <label for="robots-no-follow-overview">
        Hidden from Search Engines?
        @include('fields.form.language_identifier')
    </label>
    <div class="help-text margin-0">This page can be entirely hidden from search engines. (Uses noindex, nofollow)</div>

    <div class="font-size-16 line-height-1 padding-t-gutter">
        <div class="margin-b-gutter">
            <input type="radio" id="path[no_index_meta]-yes" name="path[no_index_meta]" value="1" class="fancy-radio margin-0"{{ $column['values']->no_index_meta == 1 ? ' checked="checked"' : '' }} />
            <label for="path[no_index_meta]-yes" class="font-size-16 fixed-font-size"><span class="font-size-14 colour-error">Yes, hide from search engines.</span></label>
        </div>
        <div class="margin-b-gutter-full">
            <input type="radio" id="path[no_index_meta]-no" name="path[no_index_meta]" value="0" class="fancy-radio margin-0"{{ $column['values']->no_index_meta == 0 ? ' checked="checked"' : '' }} />
            <label for="path[no_index_meta]-no" class="font-size-16 fixed-font-size"><span class="font-size-14">No, don't hide from search engines.</span></label>
        </div>
    </div>

</div>

@push( 'footer_scripts' )
<script type="text/javascript">

    // Get inputs
    var $preview_path_field = $('input[name="path[url]"]');
    var $preview_meta_title_field = $('input[name="seo_options[meta_title]"]');
    var $preview_meta_description_field = $('textarea[name="seo_options[meta_description]"]');

    // Get preview outputs
    var $preview_url = $('[data-google-url]');
    var $preview_meta_title = $('[data-google-meta-title]');
    var $preview_meta_description = $('[data-google-meta-description]');

    // Generate Meta Title Preview
    $preview_meta_title_field.on('keyup', function(){
        $preview_meta_title.html( $(this).val() );

        if( parseInt( $preview_meta_title.css('width') ) > 599.5 ){
            var meta_title = $preview_meta_title.html();
            while( parseInt( $preview_meta_title.css('width') ) > 599.5 ){
                meta_title = meta_title.substring( 0, meta_title.length-1 );
                $preview_meta_title.html( meta_title + '&hellip;' );
            }
        }
    });
    $preview_meta_title_field.trigger('keyup');

    // Generate Meta URL Preview
    $(document).on('seourlchange', function(){
        $preview_url.html( $('.url-path').text() );
        format_google_url();
    });

    // Generate Meta Description Preview
    $preview_meta_description_field.on('keyup', function(){

        var currentMetaDescription = $(this).val();
        var metaDescriptionLength = currentMetaDescription.length;

        // Limit to 155 characters - trim and add ellipsis if above.
        if (metaDescriptionLength > 155) {
            currentMetaDescription = currentMetaDescription.substring(0,155) + '&hellip;';
        }

        $preview_meta_description.html(currentMetaDescription);

    });
    $preview_meta_description_field.trigger('keyup');

    function format_google_url(){
        var url = $preview_url.html();
        url = url.replace( 'http://', '' );
        url = url.replace( 'https://', '' );
        url = url.replace( /\//g, ' &rsaquo; ' );
        $preview_url.html( url );

        if( parseInt( $preview_url.css( 'width' ) ) > 430 ){
            while( parseInt( $preview_url.css( 'width' ) ) > 430 ){
                url = url.substring( 0, url.length-1 );
                $preview_url.html( url + '&hellip;' );
            }
        }
    }
    format_google_url();

</script>
@endpush