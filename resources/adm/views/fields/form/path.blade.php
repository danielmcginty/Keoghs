@if( isset( $is_home_page ) && $is_home_page )
    <input type="hidden" name="path[url]" value="/" />
@else
    <div class="input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }}{{ count($errors) > 0 && ($errors->has($column['field']) || $errors->has('path.url')) ? ' has-error' : '' }}">

        @include('fields.form.label')
        @include('fields.form.help_text')

        {!! Form::text('path[url]', ($column['values']->url == '' && $column['values']->system_page) ? '/' : str_replace( $column['path_prefix'], '', $column['values']->url ), [ 'id' => $column['field'], 'placeholder' => strtolower($column['label']) ]) !!}
        @if( $crud_action == 'edit' )
        <a href="{{ route('theme', [$column['values']->url]) }}" target="_blank" class="url-path help-text block ellipsis text-left colour-blue padding-t5 margin-t-gutter-full-negative margin-b-gutter-full">{{ url('/') }}/<span class="js-path-prefix">{{$column['path_prefix']}}</span><span data-url-preview class="font-weight-600">{{ str_replace( $column['path_prefix'], '', ltrim($column['values']->url, '/') ) }}</span></a>
        @elseif( $crud_action == 'create' || $crud_action == 'copy' )
            <span class="url-path help-text block ellipsis text-left colour-blue padding-t5 margin-t-gutter-full-negative margin-b-gutter-full">{{ url('/') }}/<span class="js-path-prefix">{{$column['path_prefix']}}</span><span data-url-preview class="font-weight-600">{{ str_replace( $column['path_prefix'], '', ltrim($column['values']->url, '/') ) }}</span></span>
        @endif

        @include('fields.form.error')
        <?php $tempcol = $column; $tempcol['field'] = 'path[url]'; ?>
        @include('fields.form.error', ['column' => $tempcol])

    </div>
@endif

@if( !(\Auth::user()->userGroup->super_admin && $column['system_page_enabled']) )

    {!! Form::hidden('path[system_page]', $column['values']->system_page) !!}
    {!! Form::hidden('path[route]', $column['values']->route) !!}

@endif

@if( !isset( $is_home_page ) || !$is_home_page )
@push( 'footer_scripts' )
<script type="text/javascript">
var valid_path_regex = /^[a-zA-Z0-9-]+$/;
var path_replace_regex = /[^a-zA-Z0-9-]/g;
var ampersand_replace_regex = /[&]/g;
var whitespace_replace_regex = /[\s]/g;
var double_dash_replace_regex = /-[-]+/g;

var $path_url_field = $('input[name="path[url]"]');

$(document).ready(function() {
    $path_url_field.trigger('keyup');
});

$path_url_field.on( 'keyup change', function(){
    var test_string = $path_url_field.val();
    var matches;
    if( (matches = valid_path_regex.exec( test_string )) === null ){
        $path_url_field.val( $path_url_field.val().replace(ampersand_replace_regex, '-and-') );
        $path_url_field.val( $path_url_field.val().replace(whitespace_replace_regex, '-') );
        $path_url_field.val( $path_url_field.val().replace(double_dash_replace_regex, '-') );
        $path_url_field.val( $path_url_field.val().replace(path_replace_regex, '') );
    }

    $('[data-url-preview]').text( $path_url_field.val() );

    $(document).trigger('seourlchange');
});

$path_url_field.on('blur', function(){
    var new_path = $path_url_field.val().toLowerCase();
    new_path = new_path.replace(ampersand_replace_regex, 'and'); // Replace any ampersands with 'and'
    new_path = new_path.replace(whitespace_replace_regex, '-'); // Replace any white space with a dash
    new_path = new_path.replace(double_dash_replace_regex, '-'); // Replace any instances of 2 hyphens with 1 (----- to -)
    new_path = new_path.replace(path_replace_regex, ''); // Replace any invalid characters with nothing
    new_path = new_path.replace(/-+$/, ''); // Replace any trailing hyphens from the URL

    $path_url_field.val(new_path).trigger('change');
});

// Handle the URL generation field changes if it exists
@if( isset( $column['generation_field'] ) && !empty( $column['generation_field'] ) )
    @if(is_array($column['generation_field']))
        var $url_generation_fields = [];
        @foreach($column['generation_field'] as $generation_field)
            $url_generation_fields.push($('input[name="{{ $generation_field }}"]'));
        @endforeach

        if( $path_url_field.val().length == 0 ) {
            $url_generation_fields.forEach(function($url_generation_field) {
                $url_generation_field.on('keyup change', function () {
                    var new_path = $url_generation_fields.map(function(field) { return field.val().toLowerCase() }).join(' '); // Get the input value and lower case it
                    new_path = new_path.replace(ampersand_replace_regex, 'and'); // Replace any ampersands with 'and'
                    new_path = new_path.replace(whitespace_replace_regex, '-'); // Replace any white space with a dash
                    new_path = new_path.replace(double_dash_replace_regex, '-'); // Replace any instances of 2 hyphens with 1 (----- to -)
                    new_path = new_path.replace(path_replace_regex, ''); // Replace any invalid characters with nothing
                    new_path = new_path.replace(/-+$/, ''); // Replace any trailing hyphens from the URL

                    $path_url_field.val( new_path ).trigger('change'); // Then set it into the URL input and trigger a change event
                });
            });

            $path_url_field.on('keypress', function(){
                $url_generation_field.off('keyup').off('change');
            });
        }
    @else
        var $url_generation_field = $('input[name="{{ $column['generation_field'] }}"]');
        if( $path_url_field.val().length == 0 ) {
            $url_generation_field.on('keyup change', function () {
                var new_path = $url_generation_field.val().toLowerCase(); // Get the input value and lower case it
                new_path = new_path.replace(ampersand_replace_regex, 'and'); // Replace any ampersands with 'and'
                new_path = new_path.replace(whitespace_replace_regex, '-'); // Replace any white space with a dash
                new_path = new_path.replace(double_dash_replace_regex, '-'); // Replace any instances of 2 hyphens with 1 (----- to -)
                new_path = new_path.replace(path_replace_regex, ''); // Replace any invalid characters with nothing
                new_path = new_path.replace(/-+$/, ''); // Replace any trailing hyphens from the URL

                $path_url_field.val( new_path ).trigger('change'); // Then set it into the URL input and trigger a change event
            });

            $path_url_field.on('keypress', function(){
                $url_generation_field.off('keyup').off('change');
            });
        }
    @endif
@endif

// Handle the path prefix field change
$('.js-path-prefix-field').change( function(){
    var $select = $(this);

    var table = $select.data('path-prefix-table');
    var id = $select.val();
    var column = $select.data('path-prefix-column');

    $.ajax({
        url: '/adm/ajax_get_path_prefix',
        type: 'POST',
        data: {
            table: table,
            table_key: id,
            column: column
        },
        dataType: 'json',
        beforeSend: function(){
            showPageLoadingOverlay( true );
        },
        success: function(json){
            if( json.success ) {
                var prefix = json.prefix;
                if( prefix == '/' ){ prefix = ''; }

                $('.js-path-prefix').text(prefix);
                $(document).trigger('seourlchange');
            } else {
                alert( json.message );
            }
        },
        complete: function(){
            showPageLoadingOverlay();
        }
    });
});
</script>
@endpush
@endif