<div class="input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }}{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">

    @include('fields.form.label')
    @include('fields.form.help_text')

    <?php
        $field_attributes = ['data-tags', 'id' => transform_key( $column['field'], '-' )];
        if( isset( $column['attributes'] ) && !empty( $column['attributes'] ) ){
            $field_attributes = $field_attributes + $column['attributes'];
        }
    ?>
    {!! Form::text($column['field'], null, $field_attributes) !!}
    @include('fields.form.error')

</div>