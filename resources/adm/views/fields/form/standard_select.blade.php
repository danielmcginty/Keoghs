<div class="input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }}{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">

    @include('fields.form.label')
    @include('fields.form.help_text')

    <?php
        $field_attributes = ['id' => $column['field'], 'class' => 'width-auto' ];
        if( isset( $column['attributes'] ) && !empty( $column['attributes'] ) ){
            if( !empty( $column['attributes']['class'] ) ){ unset( $field_attributes['class'] ); }

            $field_attributes = $field_attributes + $column['attributes'];
        }
    ?>
    {!! Form::select($column['field'], $column['options'], null, $field_attributes ) !!}

    @include('fields.form.error')

</div>