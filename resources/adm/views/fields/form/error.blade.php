@if (count($errors) > 0 && $errors->has( transform_key( $column['field'] ) ) )
    <span class="inline-error"> {{ $errors->first( transform_key( $column['field'] ) ) }} </span>
@endif