<div class="input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }}{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">

    @include('fields.form.label')
    @include('fields.form.help_text')

    <?php
        $field_attributes = ['data-searchable', 'id' => $column['field'], 'class' => 'width-auto'];
        if( isset( $column['attributes'] ) && !empty( $column['attributes'] ) ){
            $field_attributes = $field_attributes + $column['attributes'];
        }
    ?>
    {!! Form::select($column['field'], $column['options'], ($item ? null : (isset($column['extra_data']['default']) ? $column['extra_data']['default'] : null)), $field_attributes ) !!}

    @include('fields.form.error')

</div>