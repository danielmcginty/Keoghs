<div class="input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }}{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">

    @include('fields.form.label')
    @include('fields.form.help_text')

    <?php
        $date_field_attributes = ['data-date-picker' => '', 'id' => $column['field'], 'class' => 'inline-block', 'placeholder' => 'dd/mm/yyyy', 'data-date-today-btn' => 'linked', 'data-date-today-highlight' => 'true', 'data-date-format' => 'dd/mm/yyyy', 'data-date-autoclose' => 'true'];
        if( isset( $column['attributes']['date_field'] ) && !empty( $column['attributes']['date_field'] ) ){
            $date_field_attributes = $field_attributes + $column['attributes']['date_field'];
        }

        $time_field_attributes = ['data-time-picker' => '', 'class' => 'inline-block', 'placeholder' => 'hh:mm', 'data-autoclose' => 'true', 'data-default' => 'now', 'data-donetext' => 'Select Time'];
        if( isset( $column['attributes']['time_field'] ) && !empty( $column['attributes']['time_field'] ) ){
            $time_field_attributes = $field_attributes + $column['attributes']['time_field'];
        }
    ?>
    {!! Form::text( $column['field'] . '[date]', isset($item) && is_object( $item ) ? null : \Carbon::now()->format('d/m/Y'), $date_field_attributes ) !!}
    {!! Form::text( $column['field'] . '[time]', isset($item) && is_object( $item ) ? null : \Carbon::now()->format('H:i'), $time_field_attributes ) !!}

    @include('fields.form.error')

</div>