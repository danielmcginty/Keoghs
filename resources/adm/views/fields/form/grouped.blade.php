<div data-grouped-field class="input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }} border-b0 overflow-hidden-{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">

    @include( 'fields.form.label' )
    @include( 'fields.form.help_text' )

    <?php $tmp_is_sidebar = isset( $is_sidebar ) ? $is_sidebar : false; ?>
    <div class="table width-100 margin-b0 max-width-100 overflow-hidden- margin-x-gutter-negative">
        @foreach( $column['grouped_fields'] as $field_name => $field_details )
            <?php
                $tmp_column = $field_details;
                $tmp_column['field'] = $column['field'] . '[' . $field_name . ']';
                $tmp_column['label'] = false;
                $tmp_column['help'] = false;

                $field_attributes = ['id' => $tmp_column['field'], 'class' => 'form-control margin-b0', 'placeholder' => $field_details['label']];
                if( isset( $column['attributes'] ) && !empty( $column['attributes'] ) ){
                    $field_attributes = $field_attributes + $column['attributes'];
                }
                $tmp_column['attributes'] = $field_attributes;

                $is_sidebar = true;
            ?>
            <div class="table-cell vertical-middle padding-x-gutter">
                @if( view()->exists( 'fields.form.' . $tmp_column['type'] ) )
                    @include( 'fields.form.' . $tmp_column['type'], ['column' => $tmp_column] )
                @else
                    {!! Form::text($tmp_column['field'], null, $tmp_column['attributes']) !!}

                    @include( 'fields.form.error', ['column' => $tmp_column] )
                @endif
            </div>
        @endforeach
    </div>
    <?php $is_sidebar = $tmp_is_sidebar; unset( $tmp_is_sidebar ); ?>

    @include( 'fields.form.error' )

</div>