<div class="external-video-input input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }}{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">

    @include('fields.form.label')
    @include('fields.form.help_text')

    <?php
        $field_attributes = ['id' => $column['field'], 'class' => 'js-external-video-field', 'style' => 'max-width: 450px;'];
        if( isset( $column['attributes'] ) && !empty( $column['attributes'] ) ){
            $field_attributes = $field_attributes + $column['attributes'];
        }
    ?>
    {!! Form::text($column['field'], null, $field_attributes) !!}
    <div class="help-text bottom-help-text margin-b-gutter-full">Examples: https://www.youtube.com/watch?v=egjApkX8sIk, https://vimeo.com/1084537</div>

    @include('fields.form.error')

    <div class="js-external-video-container margin-t-gutter padding-b-gutter-full none">
        <a href="#" class="js-toggle-external-video-preview inline-block font-size-10 underline-on-hover colour-grey">Show/Hide Video Preview</a>
        <div class="small-5">
            <div class="js-external-video-preview video-preview relative padding-b-ratio-16by9"></div>
        </div>
    </div>

    <div class="js-external-video-invalid small-5 margin-b-gutter-full none">
        <div class="background colour-grey-lightest relative radius overflow-hidden">
            <div class="tv-static absolute z-index-1 top-0 left-0 width-100 height-100 opacity-1"></div>
            <div class="relative z-index-2 padding-x-gutter-full padding-y50 radius text-center font-size-14">
                Sorry, that video URL isn't quite right.<br />Please try a valid Youtube or Vimeo URL.
            </div>
        </div>
    </div>

</div>