<div class="input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }}{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">

    @include('fields.form.label')
    @include('fields.form.help_text')

    <div class="padding-b-gutter">
        @foreach( (array)$column['options'] as $value => $label )
            <div class="margin-b-gutter">
                <input type="checkbox" id="{{ $column['field'] }}-{{ $value }}" name="{{ $column['field'] }}" value="{{ $value }}" class="fancy-checkbox  margin-0"{{ in_array($value, $column['selected']) ? ' checked="checked"' : '' }} />
                <label for="{{ $column['field'] }}-{{ $value }}" class="font-size-16 fixed-font-size"><span class="font-size-14 line-height-1pt2">{{ $label }}</span></label>
            </div>
        @endforeach
    </div>

    @include('fields.form.error')

</div>