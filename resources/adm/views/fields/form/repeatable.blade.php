<div data-repeatable-field data-field-prefix="{{ $column['field'] }}" class="table-builder sortable-table input-row border-b0 overflow-hidden-sort{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">

    @include( 'fields.form.label' )
    @include( 'fields.form.help_text' )

    <div class="table width-100 margin-b0 max-width-100 overflow-hidden-sort">

        {{-- Table Headings --}}
        <div class="table-row">

            {{-- Draggable Re-Order cell --}}
            @if( !isset( $column['options']['orderable'] ) || $column['options']['orderable'] )
                <div class="table-cell heading drag-cell-heading" style="width: 32px;"></div>
            @endif

            {{-- Other Field Headings --}}
            @foreach( $column['repeatable_fields'] as $field )
                @if( $field['type'] != 'hidden' )
                    <div class="table-cell heading">{{ $field['label'] }}</div>
                @endif
            @endforeach

            {{-- Remove Cell --}}
            @if( !isset( $column['options']['removable'] ) || $column['options']['removable'] )
                <div class="table-cell heading">&nbsp;</div>
            @endif

        </div>

        {{-- Table Rows --}}
        <div data-repeatable-table class="table-body">
            @if( $item && data_get( $item, transform_key( $column['field'] ) ) )
                <?php $rows = data_get( $item, transform_key( $column['field'] ) ); ?>
            @endif
            @if( isset( $rows ) && is_array( $rows ) )
                @foreach( $rows as $row_index => $row )
                    @include( 'fields.form.partials.repeatable.repeatable-row', ['row_index' => $row_index, 'row' => $row] )
                @endforeach
            @else
                @include( 'fields.form.partials.repeatable.repeatable-row', ['row_index' => 0, 'row' => false] )
            @endif
        </div>

    </div>

    {{-- Add New Button --}}
    @if( !isset( $column['options']['addable'] ) || $column['options']['addable'] )
        <a data-repeatable-add-button href="#" class="button round gradient bordered colour-default margin-t-gutter outline-none">
            <span class="line-height-1" style="text-shadow:none;">
                <i class="fi-adm fi-adm-plus font-size-12 margin-r5 inline-block vertical-baseline"></i>
                <span class="inline-block vertical-baseline">Add {{ \Illuminate\Support\Str::singular( $column['label'] ) }}</span>
            </span>
        </a>
    @else
        <br/>
    @endif

    @include( 'fields.form.error' )

</div>