<div class="input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }}">
    <h3 class="margin-y-gutter">{{ $column['label'] }}</h3>
</div>