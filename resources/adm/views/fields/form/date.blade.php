<div class="input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }}{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">

    @include('fields.form.label')
    @include('fields.form.help_text')

    <?php
    $field_attributes = ['data-date-picker' => '', 'id' => $column['field'], 'class' => 'inline-block', 'placeholder' => 'dd/mm/yyyy', 'data-date-today-btn' => 'linked', 'data-date-today-highlight' => 'true', 'data-date-format' => 'dd/mm/yyyy', 'data-date-autoclose' => 'true'];
        if( isset( $column['attributes'] ) && !empty( $column['attributes'] ) ){
            $field_attributes = $field_attributes + $column['attributes'];
        }
    ?>
    {!! Form::text( $column['field'], isset($item) && is_object( $item ) && !empty( $item->{$column['field']} ) ? \Carbon::createFromFormat("Y-m-d", $item->{$column['field']})->format("d/m/Y") : \Carbon::now()->format('d/m/Y'), $field_attributes) !!}

    @include('fields.form.error')

</div>