<div data-asset-gallery data-field-name-prefix="{{ $column['field'] }}" data-multi_size="{{ $column['multi_size'] }}" data-sizes="{{ json_encode( $column['sizes'] ) }}" class="asset-gallery asset-gallery-page-block sortable-table input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }}{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">

    @include('fields.form.label')
    @include('fields.form.help_text')

    <?php
    $assets = [];
    if( isset( $item ) && is_object( $item ) && data_get( $item, transform_key( $column['field'] ) ) ){
        $assets = json_decode( data_get( $item, transform_key( $column['field'] ) ), true );
    }

    // Remove labels & help text from gallery assets
    $temp_col = $column;
    $column['label'] = '';
    $column['help'] = '';
    ?>

    <div data-asset-gallery-assets class="table width-100 border-t0 margin-b-gutter-full overflow-hidden">
        @foreach( $assets as $index => $asset )
            @include('fields.form.partials.asset-gallery.row')
        @endforeach
    </div>

    <?php
    // And replace the original column after all assets have been outputted
    $column = $temp_col;
    ?>

    @include('fields.form.error')

    <button data-add-gallery-asset class="add-row colour-white radius padding-x50 padding-y15 line-height-1">
        <i class="fi-adm fi-adm-add inline-block vertical-middle margin-r-gutter"></i>
        <span class="inline-block vertical-middle">Add Gallery Image</span>
    </button>

</div>