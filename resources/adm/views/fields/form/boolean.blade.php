<div class="input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }}">

    @include('fields.form.label')
    @include('fields.form.help_text')

    <div class="table margin-b15">
        <div class="table-cell width-100">
            <label for="{{ $column['field'] }}" title="Off/On" class="fancy-switch inline-block font-size-22 fixed-font-size">
                {{ Form::hidden($column['field'], 0) }}
                <?php
                $field_attributes = ['class' => 'visually-hidden', 'id' => $column['field']];
                if( isset( $column['attributes'] ) && !empty( $column['attributes'] ) ){
                $field_attributes = $field_attributes + $column['attributes'];
                }
                ?>
                {!! Form::checkbox($column['field'], 1, null, $field_attributes) !!}
                <span class="switch"><span class="toggle"></span></span>
            </label>
        </div>
        @if( !isset( $item->system_page ) || empty( $item->system_page ) )
            @if( isset($delete_action) && $column['field'] == 'status' && in_array( 'destroy', $actions ) )
                <div class="table-cell text-right">
                    <a href="#" data-reveal-id="confirm-delete-modal" title="Delete this Entry" class="colour-error font-size-12 underline">Delete</a>
                    <div id="confirm-delete-modal" class="reveal-modal notice-modal" data-reveal aria-hidden="true" role="dialog">
                        <div class="modal-content">
                            <p class="modal-heading padding-r40 font-weight-600 margin-b-gutter">Delete this page?</p>
                            <p class="font-size-14">Would you like to delete this page?</p>
                        </div>
                        <div class="modal-actions">
                            <a href="{{ $delete_action }}" class="button round gradient bordered colour-error margin-b0 margin-r-gutter">Delete</a>
                            <a href="#" data-close-reveal class="button round bordered colour-cancel margin-b0">Cancel</a>
                        </div>
                        <a class="close-reveal-modal visually-hidden" aria-label="Close">&#215;</a>
                    </div>
                </div>
            @endif
        @endif
    </div>

</div>