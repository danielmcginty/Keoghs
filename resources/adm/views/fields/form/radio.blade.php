<div class="input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }}{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">

    @include('fields.form.label')
    @include('fields.form.help_text')

    <div class="font-size-16 line-height-1 margin-b-gutter-full">
        @foreach( (array)$column['options'] as $value => $label )
            <div class="margin-b-gutter">
                <input type="radio" id="{{ $column['field'] }}-{{ $value }}" name="{{ $column['field'] }}" value="{{ $value }}" class="fancy-radio margin-0"{{ $column['selected'] == $value ? ' checked="checked"' : '' }} />
                <label for="{{ $column['field'] }}-{{ $value }}" class="font-size-16 fixed-font-size"><span class="font-size-14">{{ $label }}</span></label>
            </div>
        @endforeach
    </div>

    @include('fields.form.error')

</div>