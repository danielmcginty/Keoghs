<div data-page-builder class="input-row ui-sortable">

    {{-- Hidden Input allowing page builder blocks to be empty --}}
    <input type="hidden" name="page_builder" value="" />

    <div class="table width-100 margin-b-gutter">
        <div class="table-cell vertical-top width-100">
            <span class="label font-size-14 font-weight-600 inline-block">Page Builder</span>
            <div class="help-text">Build up a page using re-arrangeable blocks. Drag <i class="fi-adm fi-adm-drag-handle font-size-12 colour-slate relative" style="top: 2px;"></i> to re-order.</div>
        </div>
        <div class="table-cell vertical-top text-right nowrap">
            <a href="#" data-page-builder-expand-all="" title="Expand all Page Blocks" class="font-size-12 underline underline-none-on-hover colour-grey margin-r5">Expand All</a>
            <a href="#" data-page-builder-collapse-all="" title="Collapse all Page Blocks" class="font-size-12 underline underline-none-on-hover colour-grey">Collapse All</a>
        </div>
    </div>

    @if( count($errors) && $errors->has('page_builder') )
        <div>
            <span class="inline-error margin-b-gutter"> {{ $errors->first( 'page_builder' ) }} </span>
        </div>
    @endif

    <div class="page-builder">

        <div data-page-builder-block-container>
        @foreach( (array)$column['item_blocks'] as $item_block )
            {!! $item_block['template'] !!}
        @endforeach
        </div>

        <button data-reveal-id="page-builder-blocks-modal" class="add-block radius colour-white padding-x50 padding-y15 line-height-1">
            <i class="fi-adm fi-adm-add inline-block vertical-middle margin-r-gutter"></i>
            <span class="inline-block vertical-middle">Add New Page Block</span>
        </button>

        <div id="page-builder-blocks-modal" class="reveal-modal page-builder-modal" data-reveal data-scroll-lock aria-hidden="true" role="dialog">
            <div class="modal-content padding-0">

                <div class="modal-heading padding-20 padding-r40 font-weight-600 margin-0">
                    <div class="table">

                        <div class="table-cell vertical-middle">
                            Select Block Type
                        </div>

                        <div class="table-cell vertical-middle padding-l15">
                            <div data-page-builder-block-search>
                                <div class="search-bar table border-1 colour-grey-lightest">
                                    <div class="table-cell width-100">
                                        <label for="page-bulder-search" class="visually-hidden">Search</label>
                                        <input data-page-builder-block-search-input type="text" name="page-builder-search" id="page-bulder-search" placeholder="Search..." class="margin-0 border-0 box-shadow-none padding-l15" style="border-radius:1000px;" />
                                    </div>
                                    <a data-page-builder-block-search-btn href="javascript:void(0)" class="table-cell padding-l5 padding-r15 colour-grey">
                                        <i class="fi-adm fi-adm-search block line-height-1"></i>
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <hr class="colour-grey margin-t-gutter-full margin-0" />

                <div class="modal-scrollable-section padding-t-gutter-full">
                    <div class="row">
                        @foreach( $column['addable_page_builder_blocks'] as $page_builder_block )
                            <div class="column small-6 end">
                                <a href="#" data-page-block-id="{{ $page_builder_block['reference'] }}" class="js-block-type block-type button colour-off-white radius block text-left padding-15">
                                    <span class="block">{{ $page_builder_block['name'] }}</span>
                                    <span class="block colour-grey font-size-14 font-weight-400 ellipsis">
                                        {{ $page_builder_block['description'] }}
                                    </span>
                                </a>
                            </div>
                        @endforeach
                        <div data-no-blocks-found-message class="none">
                            <div class="row">
                                <div class="column end">
                                    <div class="text-center">
                                        <div class="inline-block padding-t-gutter-full padding-x50 background colour-off-white radius">
                                            <h3>Sorry, no matching blocks found.</h3>
                                            <p>Try a broader search.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="modal-actions">
                <a data-select-block href="#" class="button round gradient bordered colour-blue margin-b0 margin-r10"><span>Add Block</span></a>
                <a href="#" data-close-reveal class="button round bordered colour-cancel margin-b0">Cancel</a>
            </div>

            <a class="close-reveal-modal visually-hidden" aria-label="Close">&times;</a>

        </div>

    </div>
</div>