<div class="input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }}{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">

    @include('fields.form.label')
    @include('fields.form.help_text')

    <div class="file-select-field relative radius margin-b-gutter-full">

        <label for="file-select-{{ $column['field'] }}" class="visually-hidden">Selected File</label>
        <?php
            $field_attributes = ['data-file-select-hidden-input' => '', 'id' => 'file-select-' . $column['field']];
            if( isset( $column['attributes'] ) && !empty( $column['attributes'] ) ){
                $field_attributes = $field_attributes + $column['attributes'];
            }
        ?>
        {!! Form::hidden( $column['field'], null, $field_attributes ) !!}

        {!! Form::text( 'file-select-preview-' . $column['field'], isset($column['file_path']) ? $column['file_path'] : null, ['data-file-select-file-preview' => '', 'class' => 'margin-0 border-0 radius-l0 background colour-white padding-r50', 'readonly' => 'readonly']) !!}

        <a href="#" data-file-select="file-select-{{ $column['field'] }}" data-image-input-id="file-select-{{ $column['field'] }}"  data-reveal-id="asset-manager-modal" class="js-asset-manager-modal-opener button radius-l colour-default absolute top-0 left-0 margin-0">Select File</a>
        <a href="#" data-clear-file-select title="Remove File" class="{{ isset($column['file_path']) ? '' : 'none ' }}absolute top-0 right-0 line-height-1 font-size-16 padding-10 margin-t1 hover-fade fade-8"><i class="fi-adm fi-adm-close-circle colour-error"></i></a>

    </div>

    @include('fields.form.error')

</div>