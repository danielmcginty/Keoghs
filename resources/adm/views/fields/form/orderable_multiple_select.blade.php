<div class="input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }}{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">

    @include('fields.form.label')
    @include('fields.form.help_text')

    <div class="multi-relation-input">
        <input type="hidden" name="{{ $column['field'] }}" value=" " />
        <select data-multi-select multiple id="multi-select-{{ \Illuminate\Support\Str::slug( $column['field']) }}" name="{{ $column['field'] }}">
            @foreach( $column['selected'] as $idx => $k )
                @if( array_key_exists( $k['value'], $column['options'] ) )
                    <option value="{{ $k['value'] }}" selected="selected">{{ $column['options'][ $k['value'] ] }}</option>
                    <?php unset( $column['options'][ $k['value'] ] ) ?>
                @endif
            @endforeach
            @foreach( $column['options'] as $k => $opt )
                <option value="{{ $k }}">{{ $opt }}</option>
            @endforeach
        </select>
    </div>

    @include('fields.form.error')

</div>