@if (!empty($column['help']))
<div class="help-text margin-t-gutter-negative">{!! $column['help'] !!}</div>
@elseif( \Auth::user()->userGroup->super_admin )
<div class="help-text margin-t-gutter-negative colour-warning none">You should probably set help text for this field ({{ $column['field'] }})!</div>
@endif