<div class="input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }}{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">

    <?php
        $field_id = $column['field'] . '-' . (rand()*1000);
        $modal_id = str_replace( array( '[', ']' ), '_', $field_id );
        $current_icon = isset($item) && is_object($item) ? data_get($item, transform_key( $column['field'] ) ) : false;
        if( empty( $current_icon ) ){ $current_icon = false; }
    ?>

    @include('fields.form.label')
    @include('fields.form.help_text')

    <?php
        $field_attributes = ['data-icon-input-id' => $modal_id, 'id' => $field_id];
        if( isset( $column['attributes'] ) && !empty( $column['attributes'] ) ){
            $field_attributes = $field_attributes + $column['attributes'];
        }
    ?>
    {!! Form::hidden( $column['field'], null, $field_attributes ) !!}

    <a href="#" title="Select Icon" data-reveal-id="icon-picker-modal-{{ $modal_id }}" data-icon-input-id="{{ $modal_id }}" class="icon-select-placeholder placeholder-block button colour-white inline-block text-center radius padding-0">
        <div class="table width-100 height-100 colour-slate">
            <div class="table-cell">
                <div class="icon-preview">
                    @if( $current_icon !== false )
                        <i class="fi fi-{{ $current_icon }} font-size-62"></i>
                    @else
                        <i class="fi-adm fi-adm-add font-size-32 block margin-b5"></i>
                        <span class="font-size-12 font-weight-400">Select Icon</span>
                    @endif
                </div>
            </div>
        </div>
    </a>

    @include('fields.form.error')

    <div id="icon-picker-modal-{{ $modal_id }}" class="reveal-modal icon-picker-modal" data-reveal aria-hidden="true" role="dialog">
        <div class="modal-content padding-0">
            <p class="modal-heading padding-r40 font-weight-600 margin-0 padding-gutter">Select Icon</p>
            <hr class="margin-0" />
            <div class="icon-select-grid line-height-1 padding-gutter-full font-size-0">
                <a href="#" data-input-id="{{ $modal_id }}" data-icon-class="BLANK" class="js-icon-option icon-option font-size-46 inline-block margin-r-gutter margin-b-gutter radius padding-10 relative">
                    <div class="table width-100 height-100">
                        <div class="table-cell">
                            <i class="fi fi-circle block colour-white rgba-0 opacity-0"></i>
                        </div>
                    </div>
                    <div class="absolute top-0 left-0 width-100 height-100 font-size-12 text-center">
                        <div class="table width-100 height-100">
                            <div class="table-cell">
                                No icon
                            </div>
                        </div>
                    </div>
                </a>
                @foreach( (array)$column['icons'] as $icon )
                    <a href="#" title="{{ ucwords(str_replace("-"," ",$icon)) }}" data-input-id="{{ $modal_id }}" data-icon-class="{{ $icon }}" class="js-icon-option icon-option font-size-46 inline-block margin-r-gutter margin-b-gutter radius padding-10{{ $icon == $current_icon ? ' selected' : '' }}">
                        <div class="table width-100 height-100">
                            <div class="table-cell">
                                <i class="fi fi-{{ $icon }} block"></i>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
        <div class="modal-actions">
            <a href="#" data-input-id="{{ $modal_id }}" class="js-select-icon-button button round gradient bordered colour-blue margin-b0 margin-r-gutter"><span>Select Icon</span></a>
            <a href="#" data-close-reveal class="button round bordered colour-cancel margin-b0">Cancel</a>
        </div>
        <a class="close-reveal-modal visually-hidden" aria-label="Close">&#215;</a>
    </div>

</div>