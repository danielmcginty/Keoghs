<?php $tmp_is_sidebar = isset( $is_sidebar ) ? $is_sidebar : false; ?>
<div data-repeatable-row data-field-prefix="{{ $column['field'] }}" class="table-row">

    {{-- Drag Handle Cell --}}
    @if( !isset( $column['options']['orderable'] ) || $column['options']['orderable'] )
    <div class="table-cell vertical-middle handle-cell background colour-off-white" style="width: 32px;">
        <div data-repeatable-table-handle class="drag-handle background colour-off-white cursor-drag text-center" title="Drag to Reorder">
            <i class="fi-adm fi-adm-drag-handle font-size-14 colour-grey-dark"></i>
        </div>
    </div>
    @endif

    {{-- Hidden Fields --}}
    @foreach( $column['repeatable_fields'] as $field_name => $field )
        @if( $field['type'] == 'hidden' )
            {!! Form::hidden($column['field'] . '[' . $row_index . '][' . $field_name . ']', null) !!}
        @endif
    @endforeach

    {{-- Other Field Cells --}}
    @foreach( $column['repeatable_fields'] as $field_name => $field )
        <?php if( $field['type'] == 'hidden' ){ continue; } ?>

        <?php
            $tmp_column = $field;
            $tmp_column['field'] = $column['field'] . '[' . $row_index . '][' . $field_name . ']';
            $tmp_column['label'] = false;
            $tmp_column['help'] = false;

            $field_attributes = ['id' => $tmp_column['field'], 'class' => 'form-control margin-b0', 'placeholder' => $field['label']];
            if( isset( $column['attributes'] ) && !empty( $column['attributes'] ) ){
                $field_attributes = $field_attributes + $column['attributes'];
            }
            $tmp_column['attributes'] = $field_attributes;

            $is_sidebar = true;
        ?>

        <div class="table-cell vertical-top">
            @if( view()->exists( 'fields.form.' . $field['type'] ) )
                @include( 'fields.form.' . $field['type'], ['column' => $tmp_column] )
            @else
                {!! Form::text($column['field'] . '[' . $row_index . '][' . $field_name . ']', null, $field_attributes) !!}

                @if( ( count($errors) > 0 && $errors->has( transform_key( $column['field'] . '[' . $row_index . '][' . $field_name . ']' ) ) ) )
                    <span class="inline-error margin-b0 margin-t5"> {{ $errors->first( transform_key( $column['field'] . '[' . $row_index . '][' . $field_name . ']' ) ) }} </span>
                @endif
            @endif
        </div>
    @endforeach

    {{-- Remove Cell --}}
    @if( !isset( $column['options']['removable'] ) || $column['options']['removable'] )
    <div class="table-cell">
        <a data-repeatable-remove-button href="#" class="button small round gradient bordered colour-error margin-0 outline-none">
            <span>Remove</span>
        </a>
    </div>
    @endif

</div>
<?php $is_sidebar = $tmp_is_sidebar; ?>