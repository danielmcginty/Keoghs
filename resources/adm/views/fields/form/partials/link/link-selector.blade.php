<?php
use App\Models\File;

$value = empty($item) ? [] : data_get($item, transform_key($column['field']));

if (is_string($value)) {
    $value = json_decode($value, true);
}

$type = !empty($value) ? data_get($value, 'type') : null;

$internal_hidden = $external_hidden = $file_hidden = true;
$internal_link_id = $external_link = $file_path = null;
if (!empty($value) && data_get($value, 'type') == 'internal') {
    $internal_hidden = false;
    $internal_link_id = data_get($value, 'internal');
}
if (!empty($value) && data_get($value, 'type') == 'external') {
    $external_hidden = false;
    $external_link = data_get($value, 'external');
}
if (!empty($value) && data_get($value, 'type') == 'file') {
    $file_hidden = false;
    $file_id = data_get($value, 'file');
    if ($file_id) {
        $file = File::find($file_id);
        if ($file) {
            $file_path = $file->path;
        }
    }
}
?>
<div data-link-selector class="link-type-selector margin-b-gutter-full">

    <?php // Link Type Dropdown ?>
    <div>
        <div class="visually-hidden">
            <label for="{{ $column['field'] }} . '-type'" class="inline-block margin-b-gutter">
                Choose a Link Type
            </label>
        </div>
        {!! Form::select($column['field'] . '[type]', ['' => 'Choose Type...', 'internal' => 'Internal Page', 'external' => 'External Link', 'file' => 'Uploaded File'], $type, ['data-link-toggler', 'id' => $column['field'] . '-type', 'class' => 'width-auto margin-0'] ) !!}
    </div>

    <?php // Link Type Fields ?>

    <div data-internal-link class="button-link-type-content {{ $internal_hidden ? 'visually-hidden' : '' }}">
        <div class="padding-t-gutter">
            <label for="{{ $column['field'] }} . '-internal'" class="inline-block margin-b-gutter">
                Internal Page
            </label>
        </div>
        <div class="help-text margin-t-gutter-negative">Select a page from inside the site</div>

        {!! Form::select($column['field'] . '[internal]', $column['internal_links'], $internal_link_id, ['data-searchable', 'data-placeholder' => 'Select a link', 'id' => $column['field'] . '-internal', 'class' => 'width-auto padding-b0']) !!}
    </div>

    <div data-external-link class="button-link-type-content {{ $external_hidden ? 'visually-hidden' : '' }}">
        <div class="padding-t-gutter">
            <label for="{{ $column['field'] }} . '-external'" class="inline-block margin-b-gutter">
                External Link
            </label>
        </div>
        <div class="help-text margin-t-gutter-negative">Enter a full URL to an external website, including http://</div>

        {!! Form::text($column['field'] . '[external]', $external_link, ['id' => $column['field'] . '-external']) !!}
        <div class="help-text margin-t-gutter-full-negative margin-b0">e.g. http://www.google.co.uk/</div>

    </div>

    <div data-file-link class="button-link-type-content {{ $file_hidden ? 'visually-hidden' : '' }}">

        <?php
        $temp_col = $column;
        $temp_col['field'] = $temp_col['field'].'[file]';
        $temp_col['label'] = 'Select File';
        $temp_col['help'] = 'Select or upload a file to link';
        $temp_col['file_path'] = $file_path;
        ?>

        <div class="padding-t-gutter">
            @include( 'fields.form.file', ['column' => $temp_col, 'class' => 'margin-b0'] )
        </div>

    </div>

</div>