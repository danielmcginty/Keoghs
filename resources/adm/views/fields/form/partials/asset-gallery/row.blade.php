<?php
    $temp_col = $column;
    $temp_col['field'] .= '[' . $index . ']';
    $temp_col['label'] = '';
?>
<div data-asset data-field-prefix="{{ $column['field'] }}" class="table-row">
    <div class="table-cell vertical-middle handle-cell background colour-off-white">
        <div data-gallery-asset-drag-handle class="drag-handle background colour-off-white cursor-drag text-center ui-sortable-handle" title="Drag to Reorder">
            <i class="fi-adm fi-adm-drag-handle font-size-14 colour-grey-dark"></i>
        </div>
    </div>
    <div class="table-cell vertical-top padding-0">
        @include('fields.form.asset', ['column' => $temp_col])
    </div>
    <div class="table-cell vertical-middle action-cell text-right">
        <a data-remove-gallery-asset href="#" class="delete-row button tiny colour-white radius margin-0" title="Delete Image">
            <i class="fi-adm fi-adm-delete colour-error"></i>
        </a>
    </div>
</div>