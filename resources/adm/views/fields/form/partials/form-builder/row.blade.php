<?php
$field_prefix = $column['field'] . '[' . $row_index . ']';

if( isset( $table_row['type'] ) && in_array( $table_row['type'], $column['show_options_type'] ) ){
    $show_options = [];
}else{
    $show_options = [ 'disabled' => 'disabled' ];
}
?>
<div data-form-row class="table-row">
    <div class="table-cell vertical-middle handle-cell background colour-off-white">
        <div data-form-drag-handle class="drag-handle background colour-off-white cursor-drag text-center" title="Drag to Reorder">
            <i class="fi-adm fi-adm-drag-handle font-size-14 colour-grey-dark"></i>
        </div>
    </div>
    <div class="table-cell vertical-top">
        {!! Form::text($field_prefix . '[name]', isset($table_row['name']) ? $table_row['name'] : '', ['id' => $field_prefix . '[name]', 'class' => 'margin-0']) !!}
    </div>
    <div class="table-cell vertical-top">
        {!! Form::select($field_prefix . '[type]', $column['field_types'], isset($table_row['type']) ? $table_row['type'] : '', ['id' => $field_prefix . '[type]', 'class' => 'margin-0']) !!}
    </div>
    <div class="table-cell vertical-top">
        {!! Form::textarea($field_prefix . '[options]', isset($table_row['options']) ? $table_row['options'] : '', ['id' => $field_prefix . '[options]', 'class' => 'margin-0 resize-vertical', 'size' => '0x0'] + $show_options) !!}

        <div data-option-select-placeholder class="margin-t10{{ isset( $table_row['type'] ) && $table_row['type'] == 'select' ? '' : ' none' }}">
            {!! Form::checkbox($field_prefix . '[first_option_placeholder]', 1, (isset($table_row['first_option_placeholder']) && $table_row['first_option_placeholder'] == 1), ['id' => $field_prefix . '[first_option_placeholder]','class' => 'fancy-checkbox margin-t5 margin-x0 margin-b0']) !!}
            <label for="{{ $field_prefix }}[first_option_placeholder]"><span>Use First Option as Placeholder?</span></label>
        </div>

    </div>
    <div class="table-cell vertical-top">
        {!! Form::checkbox($field_prefix . '[required]', 1, (isset($table_row['required']) && $table_row['required'] == 1), ['id' => $field_prefix . '[required]','class' => 'fancy-checkbox margin-0']) !!}
        <label for="{{ $field_prefix }}[required]"></label>
    </div>
    @if( \Auth::user()->userGroup->super_admin )
        <div class="table-cell vertical-top">
            {!! Form::text($field_prefix . '[reference]', isset($table_row['reference']) ? $table_row['reference'] : '', ['id' => $field_prefix . '[reference]', 'class' => 'margin-0']) !!}
        </div>
    @else
        {!! Form::hidden($field_prefix . '[reference]', null) !!}
    @endif
    <div class="table-cell vertical-top action-cell text-right">
        <a href="#" class="js-delete-row colour-error font-size-12 underline" title="Delete Row">
            Delete
        </a>
    </div>
</div>