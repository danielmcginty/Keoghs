<div data-table-row data-field-prefix="{{ $column['field'] }}" class="table-row">
    <div class="table-cell vertical-middle handle-cell background colour-off-white">
        <div data-drag-handle class="drag-handle background colour-off-white cursor-drag text-center" title="Drag to Reorder">
            <i class="fi-adm fi-adm-drag-handle font-size-14 colour-grey-dark"></i>
        </div>
    </div>
    @for ($column_index = 0; $column_index < (int)$column['options']['column_limit']; $column_index ++)
        <?php
        // Set the row index as ($row_index + 1) because we will always have fixed table headers at row 0
        $field_prefix = $column['field'] . '[' . ($row_index + 1) . '][' . $column_index . ']';
        ?>
        <div class="table-cell vertical-top">
            {!! Form::text($field_prefix, isset($table_row[$column_index]) ? $table_row[$column_index] : '', ['id' => $field_prefix, 'class' => 'margin-0']) !!}
        </div>
    @endfor
    <div class="table-cell vertical-middle action-cell text-right">
        <a href="#" class="js-delete-row colour-error font-size-12 underline" title="Delete Row">
            Delete
        </a>
    </div>
</div>