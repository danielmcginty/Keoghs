<div class="image-container inline-block vertical-top margin-r-gutter-full font-size-16">

    {!! Form::hidden( $column['field'] . "[" . $reference . "]", null, ['data-asset-input-id' => $field_id, 'id' => $field_id]) !!}

    <a href="#" title="Select Image" data-reveal-id="asset-manager-modal" data-image-input-id="{{ $field_id }}" data-crop-dimensions="{{ $dimensions }}" data-reference="{{ $reference }}" data-details="{{ json_encode($details) }}" data-column="{{ json_encode($column) }}" data-table="{{ isset($table_name) ? $table_name : '' }}" data-item-id="{{ isset($item_id) ? $item_id : 0 }}" data-language="{{ isset($current_language) ? $current_language : '' }}" class="js-asset-manager-modal-opener image-select-block inline-block">

        @if( !empty( $details['label'] ) )
            <label class="block font-size-13 font-weight-600 colour-grey-darkest">
                {{ $details['label'] }}
                @if( ($column['required'] && isset($details['primary']) && $details['primary']) || (isset($details['required']) && $details['required']) )<span class="required">*</span>@endif
            </label>
        @endif
        @if( isset( $details['width'] ) || isset( $details['height'] ) )
        <?php
            $has_px = false;
            $size = '';

            if( $details['width'] > 0 ){
                $size .= $details['width'];
                $has_px = true;
            } else {
                $size .= 'Any Width';
            }

            if( $details['height'] > 0 ){
                $size .= ' x ' . $details['height'] . 'px';
            } else {
                if( $has_px ){
                    $size .= 'px ';
                }

                $size .= ' x Any Height';
            }
        ?>

        <span title="Recommended Dimensions" class="block colour-grey font-size-12 margin-b5">
            {{ $size }}
            @if( $details['width'] > 0 && $details['height'] > 0 && isset( $details['show_ratio'] ) && $details['show_ratio'] )
            <span title="Image Ratio">&nbsp;({{ image_ratio( $details['width'], $details['height'] ) }})</span>
            @endif
        </span>

        @endif

        <div class="image-select-placeholder relative">

            <?php // Loading State ?>
            <div data-preview-loading-overlay class="image-loading-overlay absolute z-index-3 top-0 left-0 width-100 height-100 background colour-off-white">
                <div class="table width-100 height-100 colour-grey-darkest text-center">
                    <div class="table-cell">
                        <i class="fi-adm fi-adm-loading fi-spin font-size-24"></i>
                    </div>
                </div>
            </div>

            <?php // Select Image Placeholder ?>
            <div class="js-asset-preview width-100 height-100">
                <div class="table width-100 height-100 colour-grey-darkest text-center">
                    <div class="table-cell">
                        <i class="fi-adm fi-adm-add font-size-32 block margin-b5"></i>
                        <span class="font-size-12 font-weight-400">Select Image</span>
                    </div>
                </div>
            </div>

        </div>

    </a>

</div>