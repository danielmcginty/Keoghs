<?php
    $image = route('image.resize', ['path' => $current_asset->path, 'dimensions' => $dimensions]);
    if( $current_asset->crop !== false && !empty( $current_asset->crop ) ){
        $image = route('image.crop', ['path' => $current_asset->path, 'dimensions' => $dimensions, 'crop' => $current_asset->crop]);
    }

    $modal_id = "view-file-modal-" . \Illuminate\Support\Str::slug($column['field']) . "-" . $reference;
?>
<div data-image-container class="image-container inline-block vertical-top margin-r-gutter-full font-size-16">

    {!! Form::hidden( $column['field'] . "[" . $reference . "]", $current_asset->id, ['data-asset-input-id' => $field_id, 'id' => $field_id]) !!}
    @if( !empty( $details['label'] ) )
    <label class="block font-size-13 font-weight-600 colour-grey-darkest">
        {{ $details['label'] }}
        @if( ($column['required'] && isset($details['primary']) && $details['primary']) || (isset($details['required']) && $details['required']) )<span class="required">*</span>@endif
    </label>
    @endif
    @if( isset( $details['width'] ) || isset( $details['height'] ) )
    <?php
        $has_px = false;
        $size = '';

        if( $details['width'] > 0 ){
            $size .= $details['width'];
            $has_px = true;
        } else {
            $size .= 'Any Width';
        }

        if( $details['height'] > 0 ){
            $size .= ' x ' . $details['height'] . 'px';
        } else {
            if( $has_px ){
                $size .= 'px ';
            }

            $size .= ' x Any Height';
        }
    ?>

    <span title="Recommended Dimensions" class="block colour-grey font-size-12 margin-b5">
        {{ $size }}
        @if( $details['width'] > 0 && $details['height'] > 0 && isset( $details['show_ratio'] ) && $details['show_ratio'] )
        <span title="Image Ratio">&nbsp;({{ image_ratio( $details['width'], $details['height'] ) }})</span>
        @endif
    </span>

    @endif

    <a href="#" title="Select Image" data-reveal-id="asset-manager-modal" data-image-input-id="{{ $field_id }}" data-crop-dimensions="{{ $dimensions }}" data-reference="{{ $reference }}" data-details="{{ json_encode($details) }}" data-column="{{ json_encode($column) }}" data-table="{{ isset($table_name) ? $table_name : '' }}" data-item-id="{{ isset($item_id) ? $item_id : '' }}" data-language="{{ isset($current_language) ? $current_language : '' }}" data-image-directory="{{ $current_asset->directory_id }}" class="js-asset-manager-modal-opener image-select-block inline-block">

        <div class="image-preview background colour-off-white relative">

            <?php // Loading State ?>
            <div data-preview-loading-overlay class="image-loading-overlay absolute z-index-3 top-0 left-0 width-100 height-100 background colour-off-white">
                <div class="table width-100 height-100 colour-grey-darkest text-center">
                    <div class="table-cell">
                        <i class="fi-adm fi-adm-loading fi-spin font-size-24"></i>
                    </div>
                </div>
            </div>

            <?php // Image Preview ?>
            <div class="js-asset-preview selected-asset-preview width-100 height-100">
                <div data-asset-preview-image class="width-100 height-100 background-contain background-center" style="background-image: url('{{ $image }}');"></div>
            </div>

        </div>

    </a>

    <div data-group-image-actions class="image-actions">
        <a href="#" data-reveal-id="{{ $modal_id }}" data-edit-asset class="inline-block font-size-13 underline-on-hover margin-r8">Edit/Crop</a>
        <a href="#" data-remove-group-image class="inline-block font-size-13 underline-on-hover colour-error">Remove</a>
    </div>

    @include('asset-manager.crop-file-modal', ['modal_id' => $modal_id, 'file' => $current_asset, 'output_dims' => (isset($details['width']) && isset($details['height'])) ? $details['width'] . 'x' . $details['height'] : 'x'])

    <?php // Hidden reference for placeholder state ?>
    <div data-placeholder-state-reference class="none">
        <div class="image-select-placeholder">
            <div class="js-asset-preview width-100 height-100 relative">
                <div class="table width-100 height-100 colour-grey-darkest text-center">
                    <div class="table-cell">
                        <i class="fi-adm fi-adm-add font-size-32 block margin-b5"></i>
                        <span class="font-size-12 font-weight-400">Select Image</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>