@if( !empty($column['label']) )
<div>
    <label for="{{ $column['field'] }}" class="inline-block margin-b-gutter">
        {{ $column['label'] }}
        @if( $column['required'] )<span class="required">*</span>@endif
        @include('fields.form.language_identifier')
    </label>
</div>
@endif