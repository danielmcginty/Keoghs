@if (!empty($column['type']) && View::exists('fields.form.' . $column['type']))
    @include('fields.form.' . $column['type'])
@else
    <div class="input-{{ isset($is_sidebar) && $is_sidebar ? 'field' : 'row' }} {{ count($errors) > 0 && $errors->has($column['field']) ? 'has-error' : '' }}">

        @include('fields.form.label')
        @include('fields.form.help_text')

        <?php
        $field_attributes = ['id' => $column['field'], 'class' => 'form-control', 'placeholder' => $column['label']];
        if( isset( $column['attributes'] ) && !empty( $column['attributes'] ) ){
            $field_attributes = $field_attributes + $column['attributes'];
        }
        ?>
        {!! Form::text($column['field'], null, $field_attributes) !!}

        @include('fields.form.error')

    </div>
@endif