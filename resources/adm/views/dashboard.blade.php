@extends('layouts.adm')

@section('page_title', 'Dashboard')

@section('content')

    {{-- Dashboard Template --}}
    <div class="dashboard">

        <?php // Sticky Section Title ?>
        <div class="js-section-action-bar-container relative" style="height: 79px;">
            <div class="js-section-action-bar section-action-bar top-0 left-0 width-100">
                <div class="row padding-y-gutter-full">
                    <div class="column">
                        <div class="table width-100">
                            <div class="table-cell">
                                <h1 class="font-size-24 font-weight-600 margin-0 inline-block">Dashboard</h1>
                                <span class="font-size-14 colour-grey margin-l-gutter">Welcome, {{ \Auth::user()->first_name }}. <a href="{{ route('adm.logout') }}" class="underline-on-hover" onclick="event.preventDefault(); document.getElementById('dashboard-logout-form').submit();">Log out</a></span>
                                <form id="dashboard-logout-form" action="{{ route('adm.logout') }}" method="POST" class="none">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                            <div class="table-cell text-right">

                                @if ( \Auth::user()->userGroup->super_admin || in_array('adm/asset-manager', \Auth::user()->userGroup->permissions))
                                    <div class="inline-block vertical-middle margin-r-gutter">
                                        <a href="{{ route('adm.asset-manager') }}" class="button round bordered colour-cancel margin-b0">
                                            <span class="line-height-1">
                                                <i class="fi-adm fi-adm-folder font-size-12 margin-r5 inline-block vertical-baseline relative" style="top: 1px;"></i>
                                                <span class="inline-block vertical-baseline">Asset Manager</span>
                                            </span>
                                        </a>
                                    </div>
                                @endif

                                <div class="inline-block vertical-middle">
                                    <a href="{{ route('adm.path', ['pages', 'create']) }}" class="button round gradient bordered colour-success margin-b0">
                                        <span class="line-height-1">
                                            <i class="fi-adm fi-adm-plus font-size-12 margin-r5 inline-block vertical-baseline"></i>
                                            <span class="inline-block vertical-baseline">Add Page</span>
                                        </span>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <?php // Dashboard Alert ?>
            @if( isset( $show_top_error ) && $show_top_error )
            <div class="column">
                <div class="alert-box radius background colour-blue margin-b-gutter-full">
                    Heads up, the servers on the blink again!
                    <a href="#" class="close">&times;</a>
                </div>
            </div>
            @endif

            <?php // Analytics ?>
            <div class="column">

                <div data-analytics-block class="dashboard-block background colour-white radius margin-b-gutter-full relative">

                    <div class="example-analytics-graph padding-gutter-full">
                        <img src="/admin/dist/assets/ui/placeholders/analytics-placeholder.png" alt="" class="width-100" />
                    </div>

                    <div class="overlay absolute top-0 left-0 width-100 height-100 background colour-white rgba-9 text-center radius">
                        <div class="table width-100 height-100">
                            <div class="table-cell vertical-middle">
                                <i class="fi fi-adm fi-loading fi-spin font-size-48 colour-primary"></i>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <?php // Hide details if site has been provided to client and we no longer support it ?>
        @if( $migrated_to_client == false )

            <?php // Client Information ?>
            <div class="row">

                <?php // Relationship Manager ?>
                <div class="column small-4">
                    <div class="dashboard-block client-information-block background colour-white radius font-size-14 padding-gutter-full margin-b-gutter-full" data-mh="client-information">

                        <div class="table width-100 font-size-13">
                            <div class="table-cell">
                                {{ $relationship_manager ? 'Your Relationship Manager' : 'Support Desk' }}
                            </div>
                        </div>
                        <hr class="margin-t-gutter margin-b-gutter-full" />


                        @if( $relationship_manager )

                            {{-- Relationship Manager Details --}}

                            <div class="table width-100 colour-slate margin-b15">
                                <div class="table-cell">
                                    <div class="rm-image relative padding-b100p background colour-white rgba-1 circle overflow-hidden" style="width: 80px;" title="{{ $relationship_manager->full_name }}">
                                        <img src="{{ route( 'adm.path', 'rm_image' ) }}" class="absolute top-0 left-0 width-100 height-100 inline-block" />
                                    </div>
                                </div>
                                <div class="table-cell width-100">
                                    <div class="padding-l15">
                                        <div class="font-size-18 font-weight-600 margin-b5">{{ $relationship_manager->full_name }}</div>
                                        <a href="mailto:{{ $relationship_manager->email_address }}" title="Email {{ $relationship_manager->first_name }}" class="font-size-16 block line-height-1pt3 colour-slate">{{ strtolower($relationship_manager->first_name . '.' . $relationship_manager->last_name) }}@<span class="inline-block">bespokedigital.agency</span></a>
                                    </div>
                                </div>
                            </div>

                            <div>For help and assistance, email or call <span class="font-weight-600">{{ $relationship_manager->first_name }}</span>:</div>
                            <a href="tel:{{ isset($bespoke_cms_defaults->phone) ? str_replace(' ', '', $bespoke_cms_defaults->phone ) : '01772591100' }}" class="colour-slate inline-block font-size-24 font-weight-600">{{ isset($bespoke_cms_defaults->phone) ? $bespoke_cms_defaults->phone : '01772 591 100' }}</a>
                            <div class="font-size-12 colour-grey">({{ isset( $bespoke_cms_defaults->opening_hours ) ? $bespoke_cms_defaults->opening_hours : 'Mon-Fri 09:00-17:30' }})</div>

                        @else

                            {{-- Fallback Placeholder --}}

                            <div class="table width-100 colour-slate margin-b15">
                                <div class="table-cell">
                                    <div class="rm-image relative padding-b100p background colour-white rgba-1 circle overflow-hidden" style="width: 80px;" title="Support">
                                        <img src="https://bespoke-demo.co.uk/cms-logo-small.jpg" class="absolute top-0 left-0 width-100 height-100 inline-block" />
                                    </div>
                                </div>
                                <div class="table-cell width-100">
                                    <div class="padding-l15">
                                        <div class="font-size-18 font-weight-600 margin-b5">Support Desk</div>
                                        <a href="mailto:support@bespokedigital.agency" title="Email Support" class="font-size-16 block line-height-1pt3 colour-slate">support@<span class="inline-block">bespokedigital.agency</span></a>
                                    </div>
                                </div>
                            </div>

                            <div>For help and assistance, email or call <span class="font-weight-600">Support</span>:</div>
                            <a href="tel:{{ isset($bespoke_cms_defaults->phone) ? str_replace(' ', '', $bespoke_cms_defaults->phone ) : '01772591100' }}" class="colour-slate inline-block font-size-24 font-weight-600">{{ isset($bespoke_cms_defaults->phone) ? $bespoke_cms_defaults->phone : '01772 591 100' }}</a>
                            <div class="font-size-12 colour-grey">({{ isset( $bespoke_cms_defaults->opening_hours ) ? $bespoke_cms_defaults->opening_hours : 'Mon-Fri 09:00-17:30' }})</div>

                        @endif

                    </div>
                </div>

                <?php // Hosting and Support ?>
                <div class="column small-4">
                    <div class="dashboard-block client-information-block background colour-white radius font-size-14 padding-gutter-full margin-b-gutter-full" data-mh="client-information">

                        <div class="table width-100 font-size-13">
                            <div class="table-cell">
                                {{ $bespoke_hosted ? 'Hosting and Support' : 'Support Details' }}
                            </div>
                        </div>
                        <hr class="margin-t-gutter margin-b15" />

                        @if( $bespoke_hosted )

                            {{-- Bespoke Hosted Site --}}

                            <div class="font-size-16 line-height-1pt2 margin-b15">You have a <a href="{{ $bespoke_cms_defaults->support_document_url }}" target="_blank" class="colour-slate font-weight-600">{{ $bespoke_hosted_tier ? $bespoke_hosted_tier : '' }}</a> hosting and support package, which includes:</div>

                            <div class="table width-100">
                                <div class="table-cell vertical-top">
                                    <i class="fi-adm fi-adm-ul-tick colour-success margin-r-gutter"></i>
                                </div>
                                <div class="table-cell width-100">
                                    Technical Support{{ $bespoke_cms_defaults->opening_hours ? ' (' . $bespoke_cms_defaults->opening_hours . ')' : '' }}
                                </div>
                            </div>
                            <div class="table width-100 margin-t-gutter">
                                <div class="table-cell vertical-top">
                                    <i class="fi-adm fi-adm-ul-tick colour-success margin-r-gutter"></i>
                                </div>
                                <div class="table-cell width-100">
                                    One Hour Fast Response promise
                                </div>
                            </div>
                            <div class="table width-100 margin-t-gutter">
                                <div class="table-cell vertical-top">
                                    <i class="fi-adm fi-adm-ul-tick colour-success margin-r-gutter"></i>
                                </div>
                                <div class="table-cell width-100">
                                    Daily Emergency Backup service
                                </div>
                            </div>
                            <div class="table width-100 margin-t-gutter">
                                <div class="table-cell vertical-top">
                                    <i class="fi-adm fi-adm-ul-tick colour-success margin-r-gutter"></i>
                                </div>
                                <div class="table-cell width-100">
                                    24/7 server failure report via <a href="http://checkserver.co.uk" target="_blank">checkserver.co.uk</a>
                                </div>
                            </div>
                            <div class="margin-t-gutter-full">
                                <div class="font-size-16 line-height-1pt2 margin-b5">Contact the support desk</div>
                                <div class="row">
                                    <div class="column small-12 medium-6">
                                        <a href="mailto:support@bespokedigital.agency" class="table width-100 margin-b5 colour-black underline-on-hover font-weight-600">
                                            <div class="table-cell vertical-middle" style="width:20px;">
                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 128 128" enable-background="new 0 0 128 128" xml:space="preserve" width="15" height="15" style="display: inline-block;vertical-align: middle;fill: currentColor;">
                                                    <g>
                                                        <path d="M12.3,44.9c1.6,1.1,6.5,4.6,14.6,10.2s14.4,10,18.7,13.1c0.4,0.4,1.5,1.1,3.1,2.2c1.6,1.1,2.8,2,3.9,2.7
                                                            c1.1,0.7,2.3,1.5,3.7,2.3c1.5,0.9,2.8,1.6,4.1,2c1.3,0.4,2.5,0.7,3.6,0.7H64h0.1c1.1,0,2.3-0.3,3.6-0.7c1.3-0.4,2.6-1.1,4.1-2
                                                            c1.5-0.9,2.7-1.7,3.7-2.3c1-0.6,2.3-1.6,3.9-2.7c1.6-1.1,2.5-1.8,3.1-2.2c4.4-3.1,15.5-10.8,33.4-23.2c3.5-2.5,6.4-5.3,8.7-8.8
                                                            c2.4-3.4,3.5-7,3.5-10.8c0-3.2-1.1-5.8-3.4-8c-2.3-2.3-4.9-3.3-8.1-3.3H11.4c-3.7,0-6.5,1.2-8.5,3.6c-2,2.5-3,5.6-3,9.3
                                                            c0,3,1.3,6.2,3.9,9.7C6.5,40.1,9.3,42.8,12.3,44.9z"></path>
                                                        <path d="M120.9,52.5c-15.6,10.6-27.4,18.8-35.6,24.6c-2.7,2-4.9,3.6-6.6,4.6c-1.7,1.1-3.9,2.3-6.8,3.5c-2.8,1.1-5.4,1.8-7.9,1.8H64
                                                            h-0.1c-2.5,0-5.1-0.6-7.9-1.8c-2.8-1.1-5.1-2.3-6.8-3.4c-1.7-1.1-3.9-2.7-6.6-4.7c-6.4-4.7-18.2-12.9-35.5-24.6
                                                            c-2.7-1.8-5.1-3.9-7.2-6.2V103c0,3.2,1.1,6.2,3.3,8.4c2.3,2.3,4.9,3.6,8.1,3.6h105.2c3.2,0,5.9-1.4,8.1-3.6
                                                            c2.3-2.3,3.3-5.2,3.3-8.4V46.3C126,48.5,123.5,50.6,120.9,52.5z"></path>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="table-cell vertical-middle">
                                                <span style="position: relative;top: 1px;">support@bespokedigital.agency</span>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="column small-12 medium-6">
                                        <a href="tel:01772591100" class="table width-100 colour-black underline-on-hover font-weight-600">
                                            <div class="table-cell vertical-middle" style="width: 20px;">
                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 128 128" enable-background="new 0 0 128 128" xml:space="preserve" width="15" height="15" style="display: inline-block;vertical-align: middle;fill: currentColor;">
                                                    <path d="M122.7,98.9c-0.3-1.1-2.5-2.7-6.4-4.8c-1-0.7-2.6-1.6-4.5-2.7c-2-1.1-3.7-2.2-5.4-3.2c-1.5-1-3.1-1.9-4.5-2.8
                                                        c-0.2-0.2-0.9-0.7-2.1-1.6c-1.1-0.9-2.2-1.6-3-2c-0.8-0.4-1.7-0.7-2.4-0.7c-1.1,0-2.5,0.9-4.2,2.6c-1.7,1.7-3.2,3.6-4.6,5.7
                                                        c-1.4,2-2.9,3.9-4.4,5.7c-1.5,1.7-2.9,2.6-3.8,2.6c-0.5,0-1.1-0.1-1.9-0.4c-0.7-0.3-1.3-0.6-1.8-0.8c-0.4-0.2-1-0.7-2.1-1.2
                                                        c-0.9-0.7-1.4-1-1.5-1c-7.6-4.6-14.3-9.9-19.7-15.8c-5.5-5.9-10.3-13-14.7-21.2c-0.1-0.1-0.4-0.7-0.9-1.7c-0.6-1-1-1.8-1.1-2.2
                                                        c-0.2-0.4-0.4-1-0.7-1.9c-0.3-0.9-0.4-1.4-0.4-2c0-1.1,0.8-2.4,2.4-4.1c1.5-1.7,3.3-3.3,5.3-4.8c1.9-1.6,3.6-3.2,5.3-5
                                                        c1.5-1.8,2.4-3.3,2.4-4.6c0-0.9-0.2-1.7-0.6-2.6s-1-2-1.9-3.2c-0.8-1.2-1.3-2-1.4-2.2C43,21.4,42,19.8,41.2,18
                                                        c-0.9-1.7-1.9-3.7-3-5.8c-1-2.1-1.9-3.8-2.5-4.9c-2-4.2-3.4-6.6-4.4-6.9c-0.4-0.2-1-0.2-1.8-0.2c-1.5,0-3.5,0.3-5.9,0.9
                                                        c-2.5,0.6-4.3,1.2-5.8,1.9C15.1,4.3,12.1,8,9,14c-2.9,5.7-4.3,11.3-4.3,16.9c0,1.7,0.1,3.2,0.3,4.8c0.2,1.6,0.5,3.2,1,5.2
                                                        c0.5,1.9,0.9,3.3,1.2,4.3c0.3,0.9,0.9,2.6,1.8,5c0.8,2.4,1.3,3.9,1.5,4.4c2,5.9,4.3,11.2,7,15.9C22,78.4,28,86.3,35.7,94.5
                                                        s15.1,14.8,22.2,19.6c4.3,2.9,9.2,5.4,14.8,7.6c0.5,0.2,1.9,0.8,4.1,1.7s3.8,1.6,4.6,1.9c0.8,0.3,2.2,0.8,4,1.3
                                                        c1.8,0.6,3.4,0.9,4.9,1.1c1.4,0.2,2.9,0.3,4.4,0.3c5.2,0,10.3-1.6,15.7-4.7c5.7-3.3,9.1-6.6,10.2-9.6c0.6-1.6,1.2-3.6,1.8-6.2
                                                        c0.5-2.7,0.8-4.8,0.8-6.3C123,100,122.9,99.3,122.7,98.9z"></path>
                                                </svg>
                                            </div>
                                            <div class="table-cell vertical-middle">
                                                <span style="position: relative;top: 1px;">01772 591100</span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        @else

                            {{-- Client Hosted Site --}}

                            <div class="font-size-16 line-height-1pt2 margin-b-gutter-full">Your Fast Response Support Package includes:</div>

                            <div class="table width-100">
                                <div class="table-cell vertical-top">
                                    <i class="fi-adm fi-adm-ul-tick colour-success margin-r-gutter"></i>
                                </div>
                                <div class="table-cell width-100">
                                    Technical Support{{ $bespoke_cms_defaults->opening_hours ? ' (' . $bespoke_cms_defaults->opening_hours . ')' : '' }}
                                </div>
                            </div>
                            <div class="table width-100 margin-t-gutter">
                                <div class="table-cell vertical-top">
                                    <i class="fi-adm fi-adm-ul-tick colour-success margin-r-gutter"></i>
                                </div>
                                <div class="table-cell width-100">
                                    In-house Technical Support team
                                </div>
                            </div>
                            <div class="table width-100 margin-t-gutter">
                                <div class="table-cell vertical-top">
                                    <i class="fi-adm fi-adm-ul-tick colour-success margin-r-gutter"></i>
                                </div>
                                <div class="table-cell width-100">
                                    One Hour Fast Response promise
                                </div>
                            </div>
                            <div class="margin-t-gutter-full">
                                <div class="font-size-16 line-height-1pt2 margin-b5">Contact the support desk</div>
                                <div class="row">
                                    <div class="column small-12 medium-6">
                                        <a href="mailto:support@bespokedigital.agency" class="table width-100 margin-b5 colour-black underline-on-hover font-weight-600">
                                            <div class="table-cell vertical-middle" style="width:20px;">
                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 128 128" enable-background="new 0 0 128 128" xml:space="preserve" width="15" height="15" style="display: inline-block;vertical-align: middle;fill: currentColor;">
                                                    <g>
                                                        <path d="M12.3,44.9c1.6,1.1,6.5,4.6,14.6,10.2s14.4,10,18.7,13.1c0.4,0.4,1.5,1.1,3.1,2.2c1.6,1.1,2.8,2,3.9,2.7
                                                            c1.1,0.7,2.3,1.5,3.7,2.3c1.5,0.9,2.8,1.6,4.1,2c1.3,0.4,2.5,0.7,3.6,0.7H64h0.1c1.1,0,2.3-0.3,3.6-0.7c1.3-0.4,2.6-1.1,4.1-2
                                                            c1.5-0.9,2.7-1.7,3.7-2.3c1-0.6,2.3-1.6,3.9-2.7c1.6-1.1,2.5-1.8,3.1-2.2c4.4-3.1,15.5-10.8,33.4-23.2c3.5-2.5,6.4-5.3,8.7-8.8
                                                            c2.4-3.4,3.5-7,3.5-10.8c0-3.2-1.1-5.8-3.4-8c-2.3-2.3-4.9-3.3-8.1-3.3H11.4c-3.7,0-6.5,1.2-8.5,3.6c-2,2.5-3,5.6-3,9.3
                                                            c0,3,1.3,6.2,3.9,9.7C6.5,40.1,9.3,42.8,12.3,44.9z"></path>
                                                        <path d="M120.9,52.5c-15.6,10.6-27.4,18.8-35.6,24.6c-2.7,2-4.9,3.6-6.6,4.6c-1.7,1.1-3.9,2.3-6.8,3.5c-2.8,1.1-5.4,1.8-7.9,1.8H64
                                                            h-0.1c-2.5,0-5.1-0.6-7.9-1.8c-2.8-1.1-5.1-2.3-6.8-3.4c-1.7-1.1-3.9-2.7-6.6-4.7c-6.4-4.7-18.2-12.9-35.5-24.6
                                                            c-2.7-1.8-5.1-3.9-7.2-6.2V103c0,3.2,1.1,6.2,3.3,8.4c2.3,2.3,4.9,3.6,8.1,3.6h105.2c3.2,0,5.9-1.4,8.1-3.6
                                                            c2.3-2.3,3.3-5.2,3.3-8.4V46.3C126,48.5,123.5,50.6,120.9,52.5z"></path>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="table-cell vertical-middle">
                                                <span style="position: relative;top: 1px;">support@bespokedigital.agency</span>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="column small-12 medium-6">
                                        <a href="tel:01772591100" class="table width-100 colour-black underline-on-hover font-weight-600">
                                            <div class="table-cell vertical-middle" style="width: 20px;">
                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 128 128" enable-background="new 0 0 128 128" xml:space="preserve" width="15" height="15" style="display: inline-block;vertical-align: middle;fill: currentColor;">
                                                    <path d="M122.7,98.9c-0.3-1.1-2.5-2.7-6.4-4.8c-1-0.7-2.6-1.6-4.5-2.7c-2-1.1-3.7-2.2-5.4-3.2c-1.5-1-3.1-1.9-4.5-2.8
                                                        c-0.2-0.2-0.9-0.7-2.1-1.6c-1.1-0.9-2.2-1.6-3-2c-0.8-0.4-1.7-0.7-2.4-0.7c-1.1,0-2.5,0.9-4.2,2.6c-1.7,1.7-3.2,3.6-4.6,5.7
                                                        c-1.4,2-2.9,3.9-4.4,5.7c-1.5,1.7-2.9,2.6-3.8,2.6c-0.5,0-1.1-0.1-1.9-0.4c-0.7-0.3-1.3-0.6-1.8-0.8c-0.4-0.2-1-0.7-2.1-1.2
                                                        c-0.9-0.7-1.4-1-1.5-1c-7.6-4.6-14.3-9.9-19.7-15.8c-5.5-5.9-10.3-13-14.7-21.2c-0.1-0.1-0.4-0.7-0.9-1.7c-0.6-1-1-1.8-1.1-2.2
                                                        c-0.2-0.4-0.4-1-0.7-1.9c-0.3-0.9-0.4-1.4-0.4-2c0-1.1,0.8-2.4,2.4-4.1c1.5-1.7,3.3-3.3,5.3-4.8c1.9-1.6,3.6-3.2,5.3-5
                                                        c1.5-1.8,2.4-3.3,2.4-4.6c0-0.9-0.2-1.7-0.6-2.6s-1-2-1.9-3.2c-0.8-1.2-1.3-2-1.4-2.2C43,21.4,42,19.8,41.2,18
                                                        c-0.9-1.7-1.9-3.7-3-5.8c-1-2.1-1.9-3.8-2.5-4.9c-2-4.2-3.4-6.6-4.4-6.9c-0.4-0.2-1-0.2-1.8-0.2c-1.5,0-3.5,0.3-5.9,0.9
                                                        c-2.5,0.6-4.3,1.2-5.8,1.9C15.1,4.3,12.1,8,9,14c-2.9,5.7-4.3,11.3-4.3,16.9c0,1.7,0.1,3.2,0.3,4.8c0.2,1.6,0.5,3.2,1,5.2
                                                        c0.5,1.9,0.9,3.3,1.2,4.3c0.3,0.9,0.9,2.6,1.8,5c0.8,2.4,1.3,3.9,1.5,4.4c2,5.9,4.3,11.2,7,15.9C22,78.4,28,86.3,35.7,94.5
                                                        s15.1,14.8,22.2,19.6c4.3,2.9,9.2,5.4,14.8,7.6c0.5,0.2,1.9,0.8,4.1,1.7s3.8,1.6,4.6,1.9c0.8,0.3,2.2,0.8,4,1.3
                                                        c1.8,0.6,3.4,0.9,4.9,1.1c1.4,0.2,2.9,0.3,4.4,0.3c5.2,0,10.3-1.6,15.7-4.7c5.7-3.3,9.1-6.6,10.2-9.6c0.6-1.6,1.2-3.6,1.8-6.2
                                                        c0.5-2.7,0.8-4.8,0.8-6.3C123,100,122.9,99.3,122.7,98.9z"></path>
                                                </svg>
                                            </div>
                                            <div class="table-cell vertical-middle">
                                                <span style="position: relative;top: 1px;">01772 591100</span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        @endif

                    </div>
                </div>

                <?php // Bespoke News ?>
                <div class="column small-4">
                    <div class="dashboard-block client-information-block background colour-white radius font-size-14 padding-gutter-full margin-b-gutter-full" data-mh="client-information" style="height: 260px;">

                        <div class="table width-100 font-size-13">
                            <div class="table-cell">
                                Bespoke News
                            </div>
                        </div>
                        <hr class="margin-t-gutter margin-b0" />

                        @if( $bespoke_news )

                            {{-- Live Data --}}

                            <div class="scrollable-section-wrap">
                                <div class="scrollable-section padding-r-gutter padding-y-gutter-full">

                                    @foreach( $bespoke_news as $news )
                                        <div class="news-item padding-l10 border-l2 {{ $news->priority == 2 ? 'colour-error' : 'colour-grey-light' }}">
                                            <div class="colour-grey margin-b2 font-size-13">{{ \Carbon::createFromFormat('Y-m-d H:i:s', $news->date_created)->format('jS F Y') }}</div>
                                            <div class="colour-grey-dark font-weight-600 line-height-1pt2 margin-b5">{{ $news->title }}</div>
                                            <div class="colour-grey-dark line-height-1pt3">
                                                {{ $news->message }}
                                                @if( isset( $news->link ) && $news->link )
                                                    <a href="{{ $news->link }}" class="inline-block underline-on-hover" target="_blank">Read more</a>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="border-b1 colour-off-white margin-y15"></div>
                                    @endforeach

                                    <div class="colour-grey margin-t-gutter-full">For more news and updates:</div>

                                    <div class="padding-b-gutter-full">
                                        <a href="https://bespokedigital.agency/blog" target="_blank" class="inline-block underline-on-hover">View Blog</a>
                                        <span class="colour-grey-light margin-x5">-</span>
                                        <a href="https://twitter.com/Bespoke_HQ" target="_blank" class="inline-block underline-on-hover">Follow us on Twitter</a>
                                    </div>

                                </div>
                            </div>

                        @else

                            {{-- Fallback Placeholder --}}

                            <div class="table width-100 height-100 text-center">
                                <div class="table-cell font-size-14 padding-b-gutter-full">

                                    <i class="fi-adm fi-adm-news font-size-54 line-height-1 colour-grey-lightest"></i>
                                    <div class="block margin-b-gutter-full">No recent news.</div>

                                    <a href="https://twitter.com/Bespoke_HQ" target="_blank" class="inline-block underline-on-hover">Follow Bespoke | Digital Agency on Twitter</a>
                                    |
                                    <a href="https://bespokedigital.agency/blog" target="_blank" class="inline-block underline-on-hover">Read Blog</a>

                                </div>
                            </div>

                        @endif

                    </div>
                </div>

            </div>

        @endif


        <?php //  Latest Feeds ?>
        <div class="row">

            <?php // Latest Page Edits ?>
            <div class="column small-6">
                <div class="dashboard-block latest-feed background colour-white radius padding-gutter-full margin-b-gutter-full">

                    <div class="table width-100 font-size-13">
                        <div class="table-cell">
                            Latest Edits
                        </div>
                        <div class="table-cell text-right">
                            <a href="{{ route('adm.path', ['pages']) }}" class="inline-block underline-on-hover">Edit Pages</a>
                        </div>
                    </div>
                    <hr class="margin-t-gutter margin-b0" />

                    @if( $has_latest_edits )

                    {{-- Live Data --}}

                    <div class="scrollable-section-wrap">
                        <div class="scrollable-section padding-t-gutter padding-b-gutter padding-r-gutter">

                            <div class="table width-100 font-size-14">
                                @foreach( $latest_edits as $latest_edit )
                                <a href="{{ route('adm.path', ['table' => $latest_edit->getTableNameLink(), 'action' => 'edit', 'id' => $latest_edit->table_key]) }}" class="table-row line-height-1pt1">
                                    @if( is_object( $latest_edit->edited_item ) )
                                    <div class="table-cell vertical-top width-100">
                                        <span class="block underline-on-hover margin-y-gutter">
                                            {{ strlen( $latest_edit->edited_item->title ) > 30 ? substr( $latest_edit->edited_item->title, 0, 30 ) . '...' : $latest_edit->edited_item->title }}
                                        </span>
                                    </div>
                                    @endif
                                    <div class="table-cell vertical-top text-right nowrap">
                                        <span class="none block-xlarge-up margin-l-gutter-full margin-r30 margin-y-gutter colour-grey-dark opacity-6">{{ ucwords( str_replace('_', ' ', $latest_edit->table_name ) ) }}</span>
                                    </div>
                                    <div class="table-cell vertical-top nowrap">
                                        @if( $latest_edit->adm_user )
                                        <span class="block margin-r-gutter-full margin-y-gutter colour-grey-dark">{{ $latest_edit->adm_user->first_name . ' ' . substr( $latest_edit->adm_user->last_name, 0, 1 ) }}.</span>
                                        @endif
                                    </div>
                                    <div class="table-cell vertical-top text-right nowrap">
                                        <div class="nowrap margin-y-gutter margin-r-gutter colour-grey">{{ \Carbon::parse($latest_edit->changed)->format("jS M, H:i") }}</div>
                                    </div>
                                </a>
                                @endforeach
                            </div>

                            <div class="font-size-14 text-center padding-t-gutter-full padding-b-gutter">
                                <a href="{{ route('adm.path', ['pages']) }}" class="inline-block underline-on-hover margin-b5">View all Pages</a>
                            </div>

                        </div>
                    </div>

                    @else

                    {{-- Fallback Placeholder --}}

                    <div class="table width-100 text-center" style="height: 210px;">
                        <div class="table-cell font-size-14">
                            <i class="fi-adm fi-adm-pencil font-size-42 colour-grey-lightest margin-b-gutter"></i>
                            <div class="block margin-b-gutter-full">No recent edits.</div>
                            <a href="{{ route('adm.path', ['pages']) }}" class="inline-block underline-on-hover margin-b5">View all Pages</a>
                        </div>
                    </div>

                    @endif

                </div>
            </div>

            <?php // Latest Contact Form Submissions ?>
            <div class="column small-6">
                <div class="dashboard-block latest-feed background colour-white radius padding-gutter-full margin-b-gutter-full">

                    <div class="table width-100 font-size-13">
                        <div class="table-cell">
                            Latest Form Submissions
                        </div>
                        <div class="table-cell text-right">
                            <a href="{{ route('adm.form-submissions') }}" class="inline-block underline-on-hover">View All</a>
                        </div>
                    </div>
                    <hr class="margin-t-gutter margin-b0" />

                    @if( $has_latest_submissions )

                    {{-- Live Data --}}

                    <div class="scrollable-section-wrap">
                        <div class="scrollable-section font-size-14 padding-t15 padding-b5 padding-r-gutter">

                            @foreach( $latest_submissions as $submission )
                                <a href="{{ route('adm.form-submissions.submission', [$submission['form_id'], $submission['submission_id']]) }}" title="View Submission" class="block colour-slate margin-b-gutter">
                                    <div class="table width-100 font-size-14" style="table-layout: fixed;">
                                        <div class="table-cell vertical-top">
                                            <div class="margin-r-gutter font-weight-600 ellipsis">{{ $submission['form_title'] }}</div>
                                        </div>
                                    </div>
                                    @if( $submission['email'] )
                                        <div class="margin-r-gutter ellipsis nowrap colour-grey"><span class="colour-grey">{{ $submission['submitted_on'] }}</span> - {{ $submission['email'] }}</div>
                                    @endif
                                    @if( $submission['form_submission'] )
                                        <div class="margin-r-gutter ellipsis nowrap colour-grey">{{ $submission['form_submission'] }}</div>
                                    @endif
                                </a>
                            @endforeach

                            <div class="font-size-14 text-center padding-t-gutter-full padding-b-gutter">
                                <a href="{{ route('adm.form-submissions') }}" class="inline-block underline-on-hover margin-b5">View all Submissions</a>
                            </div>

                        </div>
                    </div>

                    @else

                    {{-- Fallback Placeholder --}}

                    <div class="table width-100 text-center" style="height: 210px;">
                        <div class="table-cell font-size-14">
                            <i class="fi-adm fi-adm-inbox font-size-42 colour-grey-lightest margin-b-gutter"></i>
                            <div class="block margin-b-gutter-full">No recent form submissions.</div>
                            <a href="{{ route('adm.form-submissions') }}" class="inline-block underline-on-hover">View all Submissions</a>

                        </div>
                    </div>

                    @endif

                </div>
            </div>

        </div>

    </div>

@stop


@push('footer_scripts')

<script type="text/javascript">
var $analytics_block;
$(document).ready( function(){
    $analytics_block = $('[data-analytics-block]');

    $.ajax({
        url: '{{ route('adm.ajax', 'ajax_dashboard_analytics_data') }}',
        type: 'GET',
        success: function( html ){
            $analytics_block.html( html );
        }
    });
});
</script>

@endpush