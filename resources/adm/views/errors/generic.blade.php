@extends('layouts.adm')

@section('page_title', 'Something has gone wrong')

@section('content')

    <div class="row margin-y-gutter-full">
        <div class="column">

            <div class="text-center margin-y50 padding-y50">
                <h1 class="font-size-24 font-weight-600">Sorry, something has gone wrong.</h1>
                <a href="{{ route('adm.path') }}" class="button round bordered margin-y-gutter-full">Back to Dashboard</a>
            </div>

            @if( $show_exception )
                <div class="margin-b-gutter-full"><p>{!! $exception->getMessage() !!} (Line {{ $exception->getLine() }})</p></div>
                <pre class="margin-b-gutter-full text-left">{{ $exception->getTraceAsString() }}</pre>
            @endif

        </div>
    </div>

@endsection