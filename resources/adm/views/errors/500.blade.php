<?php extract( $exception->extra_data ); ?>
@extends('layouts.adm')

@section('page_title', $title)

@section('content')

    <div class="row margin-y-gutter-full">
        <div class="column">

            <div class="text-center margin-y50 padding-y50">
                <h1 class="font-size-24 font-weight-600">Sorry, something has gone wrong.</h1>
                <p>{!! $exception->getMessage() !!}</p>
                <a href="{{ route('adm.path') }}" class="button round bordered margin-y-gutter-full">Back to Dashboard</a>
            </div>

            @if( $me->userGroup->super_admin )
                <div>
                    <table class="width-100">
                        <thead>
                        <tr>
                            <th>Class &amp; Function</th>
                            <th>Line</th>
                            <th>File</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $exception->getTrace() as $stack_trace )
                            <tr>
                                <td>{{ isset($stack_trace['class']) ? $stack_trace['class'] . ' -> ' : '' }}{{ isset($stack_trace['function']) ? $stack_trace['function'] . '()' : '' }}</td>
                                <td>{{ isset($stack_trace['line']) ? $stack_trace['line'] : '' }}</td>
                                <td>{{isset($stack_trace['file']) ? $stack_trace['file'] : '' }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif

        </div>
    </div>

@endsection