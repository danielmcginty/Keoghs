@include('includes.meta')
@include('includes.header')

<div class="admin-container relative">
    <?php // Admin Navigation ?>
    @include('includes.menu')

    <?php  // Top Notification Area ?>
    @include('includes.flash')

    <?php // Breadcrumbs ?>
    @include('includes.breadcrumbs')

    @yield('content')

    <div class="copyright text-center padding-b-gutter-full">
        <div class="font-size-10 colour-grey-light margin-b5">
            System by <a href="https://bespokedigital.agency/" target="_blank" class="colour-grey-light underline-on-hover">Bespoke | Digital Agency</a>
        </div>
    </div>

</div>

@include('includes.footer')
