<div class="selected-item-actions row margin-b20">
    <div class="column">
        <div class="table">
            <div class="table-cell vertical-middle width-100">

                <div data-asset-manager-search class="inline-block vertical-middle border-1 colour-grey-lightest margin-r5 overflow-hidden" style="border-radius: 100px;">
                    <div class="table background colour-white">
                        <div class="table-cell width-100 padding-r5">
                            <label for="search" class="visually-hidden">Search</label>
                            <input data-asset-manager-search-input type="text" name="search" id="search" placeholder="Search..." class="margin-0 border-0 box-shadow-none padding-l15" style="border-radius: 100px;">
                        </div>
                        <a data-asset-manager-search-btn href="javascript:void(0)" class="table-cell padding-l5 padding-r15 colour-grey">
                            <i class="fi-adm fi-adm-search block line-height-1"></i>
                        </a>
                    </div>
                </div>

                <a href="#" data-select-all-button class="button colour-default round bordered vertical-middle margin-b0 margin-x5">Select All</a>
                <a href="#" data-deselect-all-button class="button colour-default round bordered vertical-middle margin-b0 margin-x5">De-select All</a>

                <div data-file-actions class="inline-block vertical-middle margin-l5 is-hidden">
                    @if( isset( $move_to_directory_list ) && !empty( $move_to_directory_list ) )
                        <span class="inline-block margin-r-gutter">
                            <label for="filter-column" class="visually-hidden">Move selected items</label>
                            <select data-move-select id="filter-column" name="filter-column" class="margin-0">
                                <option selected disabled>Move selected to...</option>
                                @foreach( $move_to_directory_list as $directory )
                                    <option data-directory-path="{{ $directory->absolute_path }}" value="{{ $directory->id }}" class="{{ $directory->depth == 0 ? 'font-weight-600' : '' }}{{ $directory->depth == 1 ? 'colour-grey' : '' }}{{ $directory->depth > 1 ? 'colour-grey-light' : '' }}">@for( $i = 0; $i < $directory->depth; $i++ )&nbsp;&nbsp;&nbsp;@endfor{{ $directory->name }}</option>
                                @endforeach
                            </select>
                        </span>
                    @endif
                    <a data-delete-selected-assets href="#" class="button-delete button colour-error round bordered margin-0">Delete Selected</a>
                </div>

            </div>

            <div class="table-cell vertical-middle text-right colour-grey nowrap font-size-14">
                Showing <span data-items-count>{{ sizeof( $files ) }}</span> files.
                <span data-selected-items-count>0</span> selected.
            </div>

        </div>
    </div>
</div>

<div class="asset-manager row margin-b-gutter-full">
    <div class="column">

        <div class="table width-100 margin-b-gutter background colour-grey-lightest radius-l" style="min-height: 400px;">

            <?php // FOLDERS ?>
            <div class="folder-tree-cell table-cell vertical-top radius-l background colour-white rgba-6 border-r0 relative">

                <div class="folder-tree">

                    <h4 class="font-size-14 padding-x15 margin-t5 margin-b15">Folders</h4>

                    <div class="padding-b5">
                        <ol class="margin-b-gutter">
                            <li data-root-directory class="has-children is-expanded @if( $directory_id == 0 )current @endif">
                                <div class="inner relative">
                                    <a href="#" data-droppable-folder data-folder-id="0" data-directory-link data-directory-id="0" data-directory-path="" class="folder-item" title="Click to open Folder. Drag to re-order.">
                                        <i class="fi-adm fi-adm-folder margin-r5"></i> Site Assets
                                    </a>
                                    <div data-toggle-child-folders class="folder-toggle" title="Expand/Collapse Folders"></div>
                                </div>
                                <ol data-directory-listing class="margin-b0">
                                    @include('asset-manager.directory-list')
                                </ol>
                            </li>
                        </ol>
                    </div>

                    <div class="divider border-t1 colour-grey-lightest"></div>

                    <div class="padding-x15 padding-y-gutter-full">
                        <a href="#" data-reveal-id="folder-modal" title="Manage Folders" class="button tiny bordered round colour-default margin-0">
                            <i class="fi-adm fi-adm-plus font-size-10 margin-r2"></i>
                            Add New Folder
                        </a>
                    </div>

                </div>

                <div data-folders-loading class="none absolute top-0 left-0 width-100 height-100 background colour-white rgba-8">
                    <div class="table width-100 height-100">
                        <div class="table-cell vertical-middle text-center">
                            <i class="fi-adm fi-adm-loading fi-spin colour-grey font-size-22"></i>
                        </div>
                    </div>
                </div>

            </div>

            <?php // FILES ?>
            <div class="file-list-cell table-cell vertical-top background colour-white relative">

                <div data-files-listing class="section-container border-0 relative">
                    @include('asset-manager.files-list')
                </div>

                <div data-files-loading class="none absolute top-0 left-0 width-100 height-100 background colour-white rgba-8">
                    <div class="table width-100 height-100">
                        <div class="table-cell vertical-middle text-center">
                            <i class="fi-adm fi-adm-loading fi-spin colour-grey font-size-22"></i>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
</div>