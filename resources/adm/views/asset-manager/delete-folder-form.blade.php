<div class="js-modal-form-flash top-notification relative top-0 left-0 width-100 text-center">
    <div class="alert-box alert alert-dismissable margin-b0 bottom-0" style="display: none;"></div>
    <div class="alert-box success alert-dismissable margin-b0 bottom-0" style="display: none;"></div>
</div>

{{ Form::open([ 'url' => route('adm.asset-manager.function', ['function' => 'delete_folder']), 'type' => 'post', 'class' => 'js-modal-delete-folder-form' ]) }}
<div class="modal-content">
    <p class="modal-heading padding-r40 font-weight-600 margin-b10">Delete the folder "{{ $directory->name }}"?</p>
    <p class="font-size-14">This action <strong>cannot</strong> be reversed, and will delete <strong>all files and folders</strong> within.</p>
</div>
<div class="modal-actions">
    <button type="submit" class="button round gradient bordered colour-error margin-b0 margin-r10">Delete Folder</button>
    <a href="#" data-close-reveal class="button round bordered colour-cancel margin-b0">Cancel</a>
</div>
<a class="close-reveal-modal visually-hidden" aria-label="Close">&#215;</a>
{{ Form::hidden('directory_id', $directory->id) }}
{{ Form::hidden('check_file_usage', 1) }}
{{ Form::close() }}