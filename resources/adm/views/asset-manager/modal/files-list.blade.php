<table class="list-table hover-table-row width-100 border-0">

    <thead>
        <tr class="nowrap">
            @if( isset( $multiple_select ) && $multiple_select )
                <th class="list-table-checkbox-col">
                    <div class="block margin-t2">
                        <input data-select-all-checkboxes type="checkbox" id="selected-all" name="selected-all" class="fancy-checkbox margin-0">
                        <label for="selected-all" class="font-size-16 fixed-font-size"><span class="visually-hidden">Select All</span></label>
                    </div>
                </th>
            @endif
            <th class="orderable-">File</th>
            <th class="orderable-">Dimensions</th>
            <th class="orderable-">Filesize</th>
            <th class="orderable-">Date Added</th>
            <th class="orderable-">Added by</th>
            <th>&nbsp;</th>
        </tr>
    </thead>

    <tbody>

    @if( sizeof($list_directories) )
        @foreach( $list_directories as $directory )
            <tr data-action-menu="folder" data-action-id="{{ $directory->id }}" data-droppable-folder data-folder-id="{{ $directory->id }}" class="border-b1 colour-off-white user-select-none">
                @if( isset( $multiple_select ) && $multiple_select )
                    <td data-asset-checkbox class="list-table-checkbox-col"></td>
                @endif
                <td data-name>
                    <a href="#" data-directory-link data-directory-id="{{ $directory->id }}" class="table width-100 height-100 colour-grey">
                    <div class="asset-preview-cell table-cell vertical-middle">
                        <div class="asset-preview icon">
                            <div class="table width-100 height-100 line-height-1">
                                <div class="table-cell">
                                    <i class="fi-adm fi-adm-folder font-size-41 colour-grey-light margin-t2 margin-r4"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-cell vertical-middle width-100 padding-l-gutter line-height-1">
                        <span data-sort-field class="block font-weight-600 colour-slate margin-b2">{{ $directory->name }}</span>
                    </div>
                    </a>
                </td>
                <td>&nbsp;</td>
                <td><?php $count = $directory->files()->count(); echo $count == 1 ? $count . ' asset' : $count . ' assets'; ?></td>
                <td class="nowrap">{{ $directory->date_added }}</td>
                <td>&nbsp;</td>
            </tr>
        @endforeach
    @endif

    @if( sizeof( $files ) )
        @foreach( $files as $file )
            <tr data-draggable-asset data-asset-id="{{ $file->id }}" data-action-menu="file" data-action-id="{{ $file->id }}" data-asset-row class="cursor-pointer user-select-none">
                @if( isset( $multiple_select ) && $multiple_select )
                    <td data-asset-checkbox class="list-table-checkbox-col">
                        <div class="block margin-t2">
                            <input type="checkbox" id="selected[{{ $file->id }}]" name="selected-item" value=""  class="fancy-checkbox margin-0">
                            <label for="selected[{{ $file->id }}]" class="font-size-16 fixed-font-size"><span class="visually-hidden">Select</span></label>
                        </div>
                    </td>
                @endif
                <td data-name>
                    <div class="asset-preview-cell table-cell vertical-middle">
                        <div class="asset-preview{{ !$file->is_image ? 'icon' : '' }}">
                            <div class="table width-100 height-100 line-height-1" >
                                <div class="table-cell{{ $file->height > $file->width ? ' text-center' : '' }}">
                                    @if( $file->is_image )
                                        <img src="{{ route('image.resize', ['path' => $file->path, 'dimensions' => $file->width >= $file->height ? '45x' : 'x45']) }}" title="{{ $file->name }}" class="{{ $file->width >= $file->height ? 'width-100' : 'height-100' }} block" />
                                    @else
                                        <i class="fi-adm fi-adm-file font-size-41 colour-grey-light margin-t2"></i>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-cell vertical-middle width-100 padding-l-gutter line-height-1">
                        <span class="block font-weight-600 colour-slate margin-b2">{{ $file->name }}</span>
                        <span class="block">{{ $file->file_name }}</span>
                    </div>
                </td>
                <td>{{ $file->is_image ? $file->dimensions : '' }}</td>
                <td>{{ $file->filesize }}</td>
                <td>{{ $file->date_added }}</td>
                <td>{{ $file->added_by ? $file->added_by->name : '' }}</td>
                <td class="text-right nowrap">

                    <!-- SELECT FILE -->
                    <a href="#" data-file="{{ $file->id }}" data-path="{{ $file->is_image ? route('image.resize', ['path' => $file->path, 'dimensions' => $dimensions]) : $file->url }}" data-file-path="{{ $file->path }}" class="js-select-modal-asset button tiny round bordered colour-cancel text-shadow-none margin-b0 margin-r5">Select</a>
                    <!-- END SELECT FILE -->

                </td>
            </tr>
        @endforeach
    @elseif( !sizeof( $list_directories ) )
        <tr>
            <td colspan="8">
                <span class="block padding-gutter">No assets found.</span>
            </td>
        </tr>
    @endif
    </tbody>
</table>
