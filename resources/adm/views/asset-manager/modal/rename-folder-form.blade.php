{{ Form::open([ 'url' => route('adm.asset-manager.function', ['function' => 'rename_folder']), 'type' => 'post' ]) }}

    {{ Form::hidden('directory_id', $directory['id']) }}

    <div class="js-modal-form-flash top-notification relative top-0 left-0 width-100 text-center">
        <div class="alert-box alert alert-dismissable margin-b0 bottom-0" style="display: none;"></div>
        <div class="alert-box success alert-dismissable margin-b0 bottom-0" style="display: none;"></div>
    </div>

    <div class="modal-content">
        <p class="modal-heading padding-r40 font-weight-600 margin-b-gutter-full">Rename Folder "{{ $directory->name }}"</p>
        <?php $column = array( 'field' => 'name', 'label' => 'Name', 'required' => true, 'help' => 'New Folder Name' ); ?>
        <div class="input-field{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">
            <span class="visually-hidden">
            @include('fields.form.label')
            </span>
            @include('fields.form.help_text')
            {{ Form::text('name', $directory['name'], [ 'required' => 'required', 'placeholder' => '' ]) }}
            @include('fields.form.error')
        </div>
    </div>

    <div class="modal-actions">
        <button type="submit" class="button round gradient bordered colour-success margin-b0 margin-r10 outline-none">Rename Folder</button>
        <a href="#" data-close-reveal class="button round bordered colour-cancel margin-b0">Cancel</a>
    </div>

{{ Form::close() }}