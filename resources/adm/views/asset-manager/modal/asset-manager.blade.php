<?php // HEADER ?>
<div class="row padding-x-gutter padding-y-gutter-full">
    <div class="column">
        <div class="table width-100">
            <div class="table-cell">

                <h3 class="inline-block font-size-22 font-weight-600 margin-b0 margin-r-gutter-full vertical-middle">Select Asset</h3>

                <?php // SEARCH ?>
                <div data-asset-manager-search class="inline-block vertical-middle border-1 colour-grey-lightest margin-r-gutter overflow-hidden" style="border-radius: 100px;">
                    <div class="table background colour-white">
                        <div class="table-cell width-100">
                            <label for="search" class="visually-hidden">Search</label>
                            <input data-asset-manager-search-input type="text" name="search" id="search" placeholder="Search..." class="margin-0 border-0 box-shadow-none padding-l15" style="border-radius: 100px;">
                        </div>
                        <a data-asset-manager-search-btn href="javascript:void(0)" class="table-cell padding-l5 padding-r15 colour-grey">
                            <i class="fi-adm fi-adm-search"></i>
                        </a>
                    </div>
                </div>

            </div>
            <div class="table-cell text-right">

                <?php // NEW FOLDER ?>
                <div class="inline-block vertical-middle margin-r-gutter">
                    <a href="#" data-reveal-id="folder-modal" class="button round bordered margin-b0 colour-cancel">New Folder</a>
                </div>

                <?php // UPLOAD FILE ?>
                <div class="inline-block vertical-middle">
                    <a href="#" data-upload-file-button class="button round gradient bordered colour-success margin-b0" data-reveal-id="upload-modal"><span>Upload</span></a>
                </div>

                <div data-file-actions class="inline-block vertical-middle is-hidden margin-l-gutter">
                    <a href="#" data-select-multiple-assets-button class="button round gradient bordered margin-b0">Select Files</a>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="border-b1 colour-grey-lightest"></div>

<?php // CONTENT ?>
<div data-asset-manager class="asset-manager asset-scroll-container" style="height: auto; min-height: 300px; max-height: calc(100vh - 240px);">

    <div class="table width-100 border-0 background colour-grey-lightest radius-l" style="min-height: 500px;">

        <?php // FOLDERS ?>
        <div class="folder-tree-cell table-cell vertical-top radius-l background colour-white rgba-6 border-0 relative">

            <div class="folder-tree">

                <h4 class="font-size-14 padding-x15 margin-t5 margin-b15">Folders</h4>

                <ol class="margin-b-gutter-full">
                    <li data-root-directory class="has-children is-expanded @if($directory_id == 0 )current @endif">
                        <div class="inner relative">
                            <a href="#" data-droppable-folder data-folder-id="0" data-directory-link data-directory-id="0" data-directory-path="" class="folder-item" title="Click to open Folder. Drag to re-order.">
                                <i class="fi-adm fi-adm-folder margin-r5"></i> Site Assets
                            </a>
                            <div data-toggle-child-folders class="folder-toggle" title="Expand/Collapse Folders"></div>
                        </div>
                        <ol data-directory-listing class="margin-b0">
                            @include('asset-manager.directory-list')
                        </ol>
                    </li>
                </ol>

                <div class="divider border-t1 colour-grey-lightest"></div>

                <div class="padding-x15 padding-y-gutter-full">
                    <a href="#" data-reveal-id="folder-modal" title="Manage Folders" class="button tiny bordered round colour-default margin-0">
                        <i class="fi-adm fi-adm-plus font-size-10 margin-r2"></i>
                        Add New Folder
                    </a>
                </div>

            </div>

            <div data-folders-loading class="none absolute top-0 left-0 width-100 height-100 background colour-white rgba-8">
                <div class="table width-100 height-100">
                    <div class="table-cell vertical-middle text-center">
                        <i class="fi-adm fi-adm-loading fi-spin colour-grey font-size-22"></i>
                    </div>
                </div>
            </div>

        </div>

        <?php // FILES ?>
        <div class="file-list-cell table-cell vertical-top background colour-white border-y0 relative">

            <div data-files-listing class="section-container border-0 relative">
                @include('asset-manager.modal.files-list')
            </div>

            <div data-files-loading class="none absolute top-0 left-0 width-100 height-100 background colour-white rgba-8">
                <div class="table width-100 height-100">
                    <div class="table-cell vertical-middle text-center">
                        <i class="fi-adm fi-adm-loading fi-spin colour-grey font-size-22"></i>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>

@include('asset-manager.new-folder-modal', ['dimensions' => $dimensions])
@include('asset-manager.upload-modal', ['dimensions' => $dimensions])

<?php // Dynamic modal for general messages ?>
<div id="dynamic-asset-manager-modal" class="reveal-modal notice-modal" data-reveal aria-hidden="true" role="dialog">
    <div class="modal-content">
        <p data-dynamic-modal-title class="modal-heading padding-r40 font-weight-600 margin-b-gutter-full"></p>
        <div data-dynamic-modal-content class="font-size-14"></div>
    </div>
    <div class="modal-actions">
        <a href="#" data-confirm-button class="button round gradient bordered colour-error margin-b0 margin-r10">Confirm</a>
        <a href="#" data-close-reveal class="button round bordered colour-cancel margin-b0">Okay</a>
    </div>
</div>