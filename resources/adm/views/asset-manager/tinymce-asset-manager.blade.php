@include( 'includes.meta' )
<style scoped>
    body {
        margin: 0 !important;
    }
</style>

<div data-tinymce-asset-manager>
    @include('asset-manager.modal.asset-manager')
</div>

<?php // SYSTEM SCRIPTS ?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js?ver=3.9.1"></script>
<!--<script src="/admin/dist/assets/js/vendor/foundation.min.js"></script>-->
<script src="/admin/dist/assets/js/vendor/foundation.js"></script><!-- split up to what you need on a project -->
<script src="/admin/dist/assets/js/vendor/foundation.alert.js"></script><!-- split up to what you need on a project -->
<script src="/admin/dist/assets/js/vendor/foundation.reveal.js"></script><!-- split up to what you need on a project -->

<?php // VENDOR SCRIPTS ?>
<script src="/admin/dist/assets/js/vendor/modernizr.js"></script>
<script src="/admin/dist/assets/js/vendor/jquery-ui.min.js"></script>
<script src="/admin/dist/assets/js/vendor/jquery.matchHeight-min.js"></script>
<script src="/admin/dist/assets/js/vendor/bowser.min.js"></script>
<script src="/admin/dist/assets/js/vendor/browser-detect.js"></script>
<script src="/admin/dist/assets/js/vendor/iframeResizer.min.js"></script>
<script src="/admin/dist/assets/js/vendor/jquery.cookie.js"></script>
<script src="/admin/dist/assets/js/vendor/jquery.autogrowtextarea.min.js"></script>
<script src="/admin/dist/assets/js/vendor/chosen.jquery.min.js"></script>
<script src="/admin/dist/assets/js/vendor/jquery.tagsinput.min.js"></script>
<script src="/admin/dist/assets/js/vendor/spectrum.js"></script>
<script src="/admin/dist/assets/js/vendor/jquery-clockpicker.min.js"></script>
<script src="/admin/dist/assets/js/vendor/bootstrap-datepicker.min.js"></script>
<script src="/admin/dist/assets/js/vendor/ui.multiselect.js"></script>
<script src="/admin/dist/assets/js/vendor/jquery.accordion.js"></script>
<script src="/admin/dist/assets/js/vendor/cropper.js"></script><?php // http://iamceege.github.io/tooltipster/ ?>

<?php // BESPOKE SCRIPTS ?>
<script src="/admin/dist/assets/js/app.js"></script>

<script src="/admin/dist/assets/js/vendor/jquery.mjs.nestedSortable.js"></script>
<script type="text/javascript">

$(document).ready( function(){
    var asset_manager_images_only = false;

    var $modal = $('[data-tinymce-asset-manager]');
    var $assetManagerModal = $modal;

    $directoryListing = $modal.find( '[data-directory-listing]' );
    $filesListing = $modal.find( '[data-files-listing]' );
    $deleteActionArea = $modal.find( '[data-delete-folder-action]' );
    $newFolderModal = $modal.find( '[data-new-folder-modal]' );
    $uploadModal = $modal.find( '[data-upload-modal]' );
    $dynamicModal = $modal.find( '#dynamic-asset-manager-modal' );

    // Register a click event on the entire asset row. Gets the 'Select' button to get the image path, and passes it
    // through to the tinyMCE window
    $(document).on('click', '[data-asset-row]', function(evt){
        evt.preventDefault();

        // Get the 'Select' button
        var $btn = $(this).find('.js-select-modal-asset').first();

        // Get the image URL
        var image_url = $btn.data('path');

        // Remove any sizing
        image_url = image_url.replace( /(\/\d*x\d*\/)/, '/' );

        // pass selected file path to TinyMCE
        parent.tinymce.activeEditor.windowManager.getParams().setUrl( image_url );

        // close popup window
        parent.tinymce.activeEditor.windowManager.close();
    });

    $(document).on('click', '[data-upload-file-button]', function( evt ){
        evt.preventDefault();

        // Open native file explorer
        $('[data-reveal]#upload-modal').find('[data-asset-upload-file-input]').click();

    });

    /**
     * UPLOAD MODAL PRE-POPULATE NAME
     * Pre-populate the 'Name' field with a cleaned up version of the file name
     */
    $(document).on('change', '[data-asset-upload-file-input]', function( evt ){
        var $singleAssetFields = $('[data-reveal]#upload-modal [data-asset-upload-single-fields]');

        var files = this.files;
        if (!files) {
            // IE9 Fix
            // As IE9 only supports single image uploads, we just need to pass the single file into the array
            files = [];
            files.push({
                name: this.value.substring(this.value.lastIndexOf("\\")+1),
                size: 0,  // it's not possible to get file size without flash
                type: this.value.substring(this.value.lastIndexOf(".")+1)
            });
        }

        if( files.length > 1 ){

            if( !$singleAssetFields.hasClass("none") ){ $singleAssetFields.addClass("none"); }
            $('[data-asset-upload-name-input]').val('').removeAttr('required');

        } else {

            $singleAssetFields.removeClass("none");

            var fullPath = this.value;

            if (fullPath) {
                var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                var filename = fullPath.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                }

                var dotIndex = filename.lastIndexOf('.');
                filename = filename.substring(0, dotIndex);

                var path_replace_regex = /[^a-zA-Z0-9-]/g;
                var whitespace_replace_regex = /[\s]/g;
                var double_dash_replace_regex = /-[-]+/g;

                filename = filename.replace(whitespace_replace_regex, '-'); // Replace any white space with a dash
                filename = filename.replace(double_dash_replace_regex, '-'); // Replace any instances of 2 hyphens with 1 (----- to -)
                filename = filename.replace(path_replace_regex, ''); // Replace any invalid characters with nothing
                filename = filename.replace(/-+$/, ''); // Replace any trailing hyphens from the URL
                filename = filename.replace(/-/g, ' '); // Replace any dashes with a space

                // Now uc_words the filename
                filename = (filename + '').replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function ($1) {
                    return $1.toUpperCase()
                });

                $('[data-asset-upload-name-input]').val(filename);
            }

        }
    });

    $(document).on('submit', '#upload-modal form', function( evt ){

        evt.preventDefault();
        evt.stopPropagation();

        var $form = $(this);

        if ($form.data('submitted') === true) {
            // Previously submitted - don't submit again
            return false;
        } else {
            // Mark it so that the next submit can be ignored
            $form.data('submitted', true);
        }

        // Clear any existing flash messages from the asset manager modal
        clearAssetManagerFlashMessages();

        if( typeof( $form.data('validated') ) == 'undefined' || $form.data('validated') == false ){

            if( $form.find('.js-form-file-extension').length == 0 ) {
                var file_extension = $form.find('input[type="file"]').val().split('.')[1];
                $form.append('<input type="hidden" name="file_extension" value="' + file_extension + '" class="js-form-file-extension" />');
            }

            validateUpload( $form );
        } else {
            uploadFiles( $form );
        }
    });

    function validateUpload( $form ) {
        if (typeof( FormData ) == "undefined") {
            ie9ValidateUpload( $form );
        } else {
            var formData = new FormData($('#upload-modal form')[0]);
            $.ajax({
                url: '{{ route('adm.asset-manager.function', ['function' => 'validate_upload']) }}',
                type: "post",
                data: formData,
                dataType: "json",
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function(){
                    showModalLoadingOverlay( $('#upload-modal'), true );
                },
                success: function( json ){
                    $form.find('.js-modal-form-flash .alert-box').hide();
                    $form.find('.inline-error').remove();

                    if( json.error ){
                        showModalLoadingOverlay( $('#upload-modal'), false );
                        if( json.field_errors ){
                            for( var field in json.field_errors ){
                                $form.find('input[name="' + field + '"]').parent().append( '<span class="inline-error">' + json.field_errors[field][0] + '</span>' );
                            }
                        }

                        //$form.find('.js-modal-form-flash .alert').text( json.error ).show();
                        if (json.flash_notification) {
                            $('.admin-container .top-notification').first().remove();
                            $('.admin-container').prepend(json.flash_notification);
                            $('.admin-container .top-notification').first().css('zIndex', 1005);
                        }

                    } else if( json.success ){
                        $form.data('submitted', false);
                        $form.data('validated', 'true');
                        $form.submit();
                    }
                },
                error: function(){
                    showModalLoadingOverlay( $('#upload-modal'), false );
                    $form.find('.js-modal-form-flash .alert').text( 'An unknown error occurred, please try again' ).hide();
                },
                complete: function() {
                    $form.data('submitted', false);
                }
            });
        }
    }
    function ie9ValidateUpload( $form ){
        $form.append('<input type="hidden" name="is_ie9" value="1" />');
        $form.ajaxSubmit({
            url: '{{ route('adm.asset-manager.function', ['function' => 'validate_upload']) }}',
            type: "post",
            dataType: 'json',
            beforeSubmit: function () {
                showModalLoadingOverlay($('#upload-modal'), true);
            },
            success: function( json, statusText, xhr, $form ){
                $form.find('.js-modal-form-flash .alert-box').hide();
                $form.find('.inline-error').remove();

                if( json.error ){
                    showModalLoadingOverlay( $('#upload-modal'), false );
                    if( json.field_errors ){
                        for( var field in json.field_errors ){
                            $form.find('input[name="' + field + '"]').parent().append( '<span class="inline-error">' + json.field_errors[field][0] + '</span>' );
                        }
                    }

                    //$form.find('.js-modal-form-flash .alert').text( json.error ).show();
                    if (json.flash_notification) {
                        $('.admin-container .top-notification').first().remove();
                        $('.admin-container').prepend(json.flash_notification);
                        $('.admin-container .top-notification').first().css('zIndex', 1005);
                    }

                } else if( json.success ){
                    $form.data('submitted', false);
                    $form.data('validated', 'true');
                    $form.submit();
                }
            },
            error: function () {
                showModalLoadingOverlay( $('#upload-modal'), false );
                $form.find('.js-modal-form-flash .alert').text( 'An unknown error occurred, please try again' ).hide();
            }
        });
    }

    function uploadFiles( $form ){
        if( typeof( FormData ) == "undefined" ){
            ie9UploadFiles( $form );
        } else {
            var formData = new FormData( $('#upload-modal form')[0] );
            $.ajax({
                url: '{{ route('adm.asset-manager.function', ['function' => 'upload']) }}',
                type: "post",
                data: formData,
                dataType: false,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function(){
                    showModalLoadingOverlay( $('#upload-modal'), true );
                },
                success: function( json ){
                    $form.find('.js-modal-form-flash .alert-box').hide();
                    $form.find('.inline-error').remove();

                    if( json.error ){
                        if( json.field_errors ){
                            for( var field in json.field_errors ){
                                $form.find('input[name="' + field + '"]').parent().append( '<span class="inline-error">' + json.field_errors[field][0] + '</span>' );
                            }
                        }

                        if (json.flash_notification) {
                            $('.admin-container .top-notification').first().remove();
                            $('.admin-container').prepend(json.flash_notification);
                            $('.admin-container .top-notification').first().css('zIndex', 1005);
                        }
                    }
                    else if( json.success ){

                        if (json.flash_notification) {
                            $('.admin-container .top-notification').first().remove();
                            $('.admin-container').prepend(json.flash_notification);
                            $('.admin-container .top-notification').first().css('zIndex', 1005);
                        }

                        $form.data('submitted', false);
                        $form.data('validated', false);
                        $('#upload-modal').foundation('reveal', 'close');
                        $('#upload-modal').remove();
//                            reloadDirectoriesAndFiles( $form.find('input[name="directory_id"]').val() );
                        reloadDirectoriesAndFiles();
                    }
                },
                error: function(){
                    showModalLoadingOverlay( $('#upload-modal'), false );
                    $form.find('.js-modal-form-flash .alert').text( 'An unknown error occurred, please try again' ).hide();
                },
                complete: function () {
                    showModalLoadingOverlay( $('#upload-modal'), false );
                    $form.data('submitted', false);
                }
            }, 'json');
        }
    }
    function ie9UploadFiles( $form ){
        $form.ajaxSubmit({
            url: '{{ route('adm.asset-manager.function', ['function' => 'upload']) }}',
            type: "post",
            dataType: 'json',
            beforeSend: function(){
                showModalLoadingOverlay( $('#upload-modal'), true );
            },
            success: function( json ){
                showModalLoadingOverlay( $('#upload-modal'), false );

                $form.find('.js-modal-form-flash .alert-box').hide();
                $form.find('.inline-error').remove();

                if( json.error ){
                    if( json.field_errors ){
                        for( var field in json.field_errors ){
                            $form.find('input[name="' + field + '"]').parent().append( '<span class="inline-error">' + json.field_errors[field][0] + '</span>' );
                        }
                    }

                    if (json.flash_notification) {
                        $('.admin-container .top-notification').first().remove();
                        $('.admin-container').prepend(json.flash_notification);
                        $('.admin-container .top-notification').first().css('zIndex', 1005);
                    }
                }
                else if( json.success ){

                    if (json.flash_notification) {
                        $('.admin-container .top-notification').first().remove();
                        $('.admin-container').prepend(json.flash_notification);
                        $('.admin-container .top-notification').first().css('zIndex', 1005);
                    }

                    $form.data('submitted', false);
                    $form.data('validated', false);
                    $('#upload-modal').foundation('reveal', 'close');
                    $('#upload-modal').remove();
//                            reloadDirectoriesAndFiles( $form.find('input[name="directory_id"]').val() );
                    reloadDirectoriesAndFiles();
                }
            },
            error: function(){
                showModalLoadingOverlay( $('#upload-modal'), false );
                $form.find('.js-modal-form-flash .alert').text( 'An unknown error occurred, please try again' ).hide();
            }
        }, 'json');
    }

    $(document).on('submit', '#folder-modal form', function( evt ){

        evt.preventDefault();
        evt.stopPropagation();

        var $form = $(this);

        if ($form.data('submitted') === true) {
            // Previously submitted - don't submit again
            return false;
        } else {
            // Mark it so that the next submit can be ignored
            $form.data('submitted', true);
        }

        // Clear any existing flash messages from the asset manager modal
        clearAssetManagerFlashMessages();

        $(document).foundation('reveal', 'reflow');

        if( typeof( $form.data('validated') ) == 'undefined' ){

            $.ajax({
                url: '{{ route('adm.asset-manager.function', ['function' => 'validate_new_folder']) }}',
                type: "post",
                data: $form.serialize(),
                dataType: "json",
                beforeSend: function() {
                    showModalLoadingOverlay($('#folder-modal'), true);
                },
                success: function( json ){
                    $form.find('.js-modal-form-flash .alert-box').hide();
                    $form.find('.inline-error').remove();

                    if( json.error ){
                        if( json.field_errors ){
                            for( var field in json.field_errors ){
                                $form.find('input[name="' + field + '"]').parent().append( '<span class="inline-error">' + json.field_errors[field][0] + '</span>' );
                            }
                        }

                        if (json.flash_notification) {
                            $('.admin-container .top-notification').first().remove();
                            $('.admin-container').prepend(json.flash_notification);
                            $('.admin-container .top-notification').first().css('zIndex', 1005);
                        }
                    }
                    else if( json.success ){
                        $form.data('submitted', false);
                        $form.data('validated', 'true');
                        $form.submit();
                    }
                },
                complete: function() {
                    showModalLoadingOverlay($('#folder-modal'), false);
                    $form.data('submitted', false);
                }
            });

        } else {

            $.ajax({
                url: '{{ route('adm.asset-manager.function', ['function' => 'new_folder']) }}',
                type: "post",
                data: $form.serialize(),
                dataType: "json",
                beforeSend: function() {
                    showModalLoadingOverlay($('#folder-modal'), true);
                },
                success: function( json ){
                    $form.find('.js-modal-form-flash .alert-box').hide();
                    $form.find('.inline-error').remove();

                    if( json.error ){
                        if( json.field_errors ){
                            for( var field in json.field_errors ){
                                $form.find('input[name="' + field + '"]').parent().append( '<span class="inline-error">' + json.field_errors[field][0] + '</span>' );
                            }
                        }

                        if (json.flash_notification) {
                            $('.admin-container .top-notification').first().remove();
                            $('.admin-container').prepend(json.flash_notification);
                            $('.admin-container .top-notification').first().css('zIndex', 1005);
                        }
                    } else {

                        if ($assetManagerModal.length && json.success) {
                            if (json.flash_notification) {
                                $('.admin-container .top-notification').first().remove();
                                $('.admin-container').prepend(json.flash_notification);
                                $('.admin-container .top-notification').first().css('zIndex', 1005);
                            }

                            $('#folder-modal').foundation('reveal', 'close');

                            // make sure the newly added alert prompts can be removed
                            $(document).foundation('alert', 'reflow');

                            // Add notifications to the asset manager modal
                            //$assetManagerModal.find('.js-modal-form-flash .success').text( json.success ).show();
                        }

                        if( typeof( json.new_directory ) != 'undefined' ){
                            reloadDirectoriesAndFiles( json.new_directory );
                        } else {
                            reloadDirectories();
                        }
                    }
                },
                complete: function(){
                    showModalLoadingOverlay($('#folder-modal'), false);
                    $form.data('submitted', false);
                }
            });

        }
    });

    $(document).on('click', '.js-delete-folder-button', function (evt) {

        evt.preventDefault();

        var directory_id = $(this).data('folder-id');

        $('#confirm-delete-folder-modal').foundation('reveal', 'open', {
            url: '{{ route('adm.asset-manager.function', ['delete_folder']) }}',
            data: {
                directory_id: directory_id,
                get_form: true
            },
            method: 'post'
        });
    });

    $(document).on('submit', '.js-modal-delete-folder-form', function( evt ){

        evt.preventDefault();
        evt.stopPropagation();

        var $form = $(this);
        var $form_modal = $form.parents('.reveal-modal').first();

        if ($form.data('submitted') === true) {
            // Previously submitted - don't submit again
            return false;
        } else {
            // Mark it so that the next submit can be ignored
            $form.data('submitted', true);
        }

        $.ajax({
            url: $form.attr('action'),
            type: "post",
            data: $form.serialize(),
            dataType: "json",
            beforeSend: function(){
                $form.find('.js-modal-form-flash .alert-box').text('').hide();
                showModalLoadingOverlay($form_modal, true);
            },
            success: function( json ){

                if ( json.files_in_use ) {
                    console.log('files_in_use');
                    // If files within this folder are in use, warn the user in a separate modal

                    var content = '';
                    content += '<p class="font-size-14">Assets inside this folder are being used on the site.<br />Deleting this folder may <strong>break images or links to files</strong> on the website.</p>';
                    content += '<span class="block">Assets being used: </span>';
                    content += '<ul class="font-weight-600">';
                    $(json.files_in_use).each(function (ind, val) {
                        content += '<li>' + val + '</li>';
                    });
                    content += '</ul>';
                    content += '<p class="font-size-14">Are you sure you wish to delete the folder? <strong>This cannot be undone</strong>.</p>';

                    var $filesInUseModal = getDynamicModal({
                        title: 'Asset in Use',
                        content: content,
                        confirmButton: 'Delete',
                        closeButton: 'Cancel'
                    });
                    $('body').append($filesInUseModal);
                    $filesInUseModal.foundation('reveal', 'open');

                    $(document).on('closed.fndtn.reveal', $filesInUseModal, function () {
                        $form_modal.foundation('reveal', 'close');
                    });

                    $filesInUseModal.find('[data-confirm-button]').on('click', function() {
                        $form.data('submitted', false);

                        // The next time the form is submitted, don't check for file usage - just delete files
                        $form.find('[name="check_file_usage"]').remove();

                        $form.submit();

                        $filesInUseModal.foundation('reveal', 'close');
                    });
                }

                if( json.success ){
                    // Add notifications to the asset manager modal
                    //$assetManagerModal.find('.js-modal-form-flash .success').text( json.success ).show();
                    if (json.flash_notification) {
                        $('.admin-container .top-notification').first().remove();
                        $('.admin-container').prepend(json.flash_notification);
                        $('.admin-container .top-notification').first().css('zIndex', 1005);
                    }

                    $form_modal.foundation('reveal', 'close');
                    $form_modal.remove();
                    reloadDirectoriesAndFiles( json.parent );
                }

                if( json.error ){
                    //$form.find('.js-modal-form-flash .alert').text( json.error ).show();
                    if (json.flash_notification) {
                        $('.admin-container .top-notification').first().remove();
                        $('.admin-container').prepend(json.flash_notification);
                        $('.admin-container .top-notification').first().css('zIndex', 1005);
                    }
                }
            },
            complete: function(){
                $form.data('submitted', false);
                showModalLoadingOverlay($form_modal, false);
            }
        }, 'json');

    });

    // Click on a directory list link
    $(document).on('click', '[data-directory-link]', function(evt){

        evt.preventDefault();
        evt.stopPropagation();

        reloadDirectoriesAndFiles( $(this).data('directory-id') );

    });

    // Call this function to move a set of specified file IDs into a given directory
    function move_files( file_ids, directory_id ){
        $.ajax({
            url: '{{ route('adm.asset-manager.function', ['function' => 'move_files']) }}',
            type: 'POST',
            data: {
                file_ids: file_ids,
                directory_id: directory_id
            },
            dataType: 'json',
            beforeSend: function(){

                showPageLoadingOverlay(true);

            },
            success: function( json ){

                showPageLoadingOverlay(false);

                if( json.success ){
                    var title = 'Assets moved';
                    var content = '';

                    if( json.files_moved > 0 ){
                        content = '<p>' + json.success + '</p>';
                    } else {
                        title = 'Assets couldn\'t be moved.';
                    }

                    if( json.files_not_moved && json.files_not_moved.length > 0 ){
                        content += '<p>1 or more assets couldn\'t be moved because an asset with the same name exists in the folder you tried to move them to.</p>';
                        content += '<span class="block">The assets are: </span>';
                        content += '<ul class="font-weight-600">';
                        $(json.files_not_moved).each( function(ind, val){
                            content += '<li>' + val + '</li>';
                        });
                        content += '</ul>';
                    }

                    var $modal = getDynamicModal({
                        title: title,
                        content: content
                    });
                    $('body').append( $modal );
                    $modal.foundation('reveal', 'open');

                    if( json.files_moved > 0 ){
                        $modal.find('[data-close-reveal]').off('click').click( function(){
                            reloadDirectoriesAndFiles(directory_id);
                        });
                    }

                } else {
                    var $modal = getDynamicModal({
                        title: 'Assets couldn\'t be moved.',
                        content: json.error
                    });
                    $('body').append($modal);
                    $modal.foundation('reveal', 'open');
                }

            }
        });
    }

    // Call this function to move a set of specified folder IDs into a given parent directory
    function move_folders( folder_ids, parent_id ){
        $.ajax({
            url: '{{ route('adm.asset-manager.function', ['function' => 'move_folders']) }}',
            type: 'POST',
            data: {
                directory_ids: folder_ids,
                parent: parent_id
            },
            dataType: 'json',
            beforeSend: function(){

                showPageLoadingOverlay(true);

            },
            success: function( json ){

                showPageLoadingOverlay(false);

                var title = '';
                var content = '';

                if( json.success ){

                    if( json.folders_not_moved && json.folders_not_moved.length > 0 ){
                        title = 'Sorry, that folder could not be moved.';

                        content += '<p>The folder "';
                        $(json.folders_not_moved).each( function(ind, val){
                            content += val;
                        });
                        content += '" already exists in that directory.</p>';

                        var $modal = getDynamicModal({
                            title: title,
                            content: content
                        });
                        $('body').append($modal);
                        $modal.foundation('reveal', 'open');

                        if( json.folders_moved > 0 ){
                            $modal.find('[data-close-reveal]').off('click').click( function(){
                                reloadDirectoriesAndFiles(parent_id);
                            });
                        }

                    } else {
                        reloadDirectoriesAndFiles(parent_id);
                    }

                } else {

                    // Textual error message
                    var $modal = getDynamicModal({
                        title: 'Sorry, that folder could not be moved.',
                        content: json.error
                    });
                    $('body').append($modal);
                    $modal.foundation('reveal', 'open');

                }

            }
        });
    }

    // Reload the directory listing, passing a target element to change the DOM of
    function reloadDirectories( directory_id ){
        data = { modal: true };
        if( typeof( directory_id ) != 'undefined' ){ data.directory_id = directory_id; }

        $.ajax({
            url: '{{ route('adm.asset-manager.function', ['load_directory_list']) }}',
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function(){
                showPageLoadingOverlay( true );
            },
            success: reloadResponseHandler,
            complete: function(){
                isSiteAssetsCurrent( directory_id );
                showPageLoadingOverlay();
            }
        });
    }

    // Reload the files listing, passing a target element to change the DOM of
    function reloadFiles( directory_id ){
        data = { modal: true, images_only: asset_manager_images_only };
        if( typeof( directory_id ) != 'undefined' ){ data.directory_id = directory_id; }

        $.ajax({
            url: '{{ route('adm.asset-manager.function', ['load_files_list']) }}',
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function(){
                showPageLoadingOverlay( true );
            },
            success: reloadResponseHandler,
            complete: function(){
                showPageLoadingOverlay();
            }
        });
    }

    // Reload both the directory listing and the files listing, passing a target element for both
    function reloadDirectoriesAndFiles( directory_id ){
        data = { modal: true, images_only: asset_manager_images_only };
        if( typeof( directory_id ) != 'undefined' ){ data.directory_id = directory_id; }

        $.ajax({
            url: '{{ route('adm.asset-manager.function', ['load_directory_and_files_list']) }}',
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function(){
                showPageLoadingOverlay( true );
            },
            success: reloadResponseHandler,
            complete: function(){
                isSiteAssetsCurrent( directory_id );
                showPageLoadingOverlay();
            }
        });
    }

    // Common handler function to perform various DOM updated based on AJAX responses
    function reloadResponseHandler( json ){
        // If we have a new directory listing
        if( json.directory_list ){
            $directoryListing.html( json.directory_list );
        }
        // If we have a new files listing
        if( json.files_list ){
            $filesListing.html( json.files_list );
        }
        // If we have a new delete folder button
        if( json.delete_action ){
            $deleteActionArea.html( json.delete_action );
        } else {
            $deleteActionArea.html('');
        }
        // If we have a new new folder modal
        if( json.new_folder ){
            $newFolderModal.html( json.new_folder );
        }
        // If we have a new upload modal
        if( json.upload && typeof( $uploadModal ) != 'undefined' ){
            $uploadModal.html( json.upload );
        }

        initAssetManager();
    }

    // Toggle whether the last clicked directory is 'Site Assets' and add the 'current' class if so
    function isSiteAssetsCurrent( directory_id ){
        if( directory_id == 0 ){
            $('[data-root-directory]').addClass('current');
        } else {
            $('[data-root-directory]').removeClass('current');
        }
    }

    // Show a clone of the dynamic modal, with a given title, content and buttons
    function getDynamicModal( options ){
        var $thisModal = $dynamicModal.clone();

        $thisModal.find('[data-dynamic-modal-title]').text( options.title );
        $thisModal.find('[data-dynamic-modal-content]').html( options.content );

        if (options.confirmButton) {
            $thisModal.find('[data-confirm-button]').text( options.confirmButton );
        } else {
            $thisModal.find('[data-confirm-button]').remove();
        }

        if (options.closeButton) {
            $thisModal.find('[data-close-reveal]').text( options.closeButton );
        }

        $(document).on('closed.fndtn.reveal', $thisModal, function () {
            $(this).remove();
        });

        return $thisModal;
    }

    /**
     * INIT ASSET MANAGER
     * Custom Right Click Content Menu
     */
    function refreshFolderList(){
        // Init Folder List (Draggable/Droppable)
        var draggableFolders = $("[data-draggable-folder]").draggable(draggableFoldersOptions);
        var droppableFolders = $("[data-droppable-folder]:not(.current)").droppable(droppableFoldersOptions);
    }

    function refreshAssetList(){
        // Init Asset List (Draggable)
        var draggableAssets = $("[data-draggable-asset]").draggable(draggableAssetsOptions);
    }

    function initAssetManager(){
        refreshAssetList();
        refreshFolderList();
    }

    initAssetManager();

    // Clear any existing flash messages from the asset manager modal
    function clearAssetManagerFlashMessages() {
        $assetManagerModal.find('.js-modal-form-flash .alert-box').text('').hide();
    }

    /**
     * GET SELECTED ASSETS
     * Get all selected assets on screen
     */
    function getSelected(){
        var selectedAssets = [];

        // Push all selected items
        $("[data-asset-manager] tr[data-selected]").each(function() {
            // Push each selected id into an array
            var assetID = $(this).data('asset-id');
            selectedAssets.push(assetID);
        });

        return selectedAssets;
    }

    /**
     * DRAGGABLE ASSETS
     * Allow asset table rows to be dragged
     * Source: http://api.jqueryui.com/draggable/
     */
    var isDraggingAsset, draggingAsset, draggingAssetID;
    var draggableAssetsOptions = {
        helper: "clone",    // Create copy to style when dragging
        revert: true,       // Animate back to original position if not dropped
        cancel: "input,.button,.list-table-checkbox-col",
        cursorAt: {
            left: -10,
            top: -10
        },
        start: function(event, ui) {
            //console.log('dragging asset');

            // Set dragging state
            isDraggingAsset = true;

            // Store dragged asset
            draggingAsset = $(this);

            // Enable revert (if previously disabled by drop)
            $(this).draggable("option", "revert", true);

            // Apply dragged from state
            $(this).addClass("ui-dragging-from");

            // MULTIPLE ITEMS SELECTED
            selectedItems = getSelected();
            if (selectedItems > 1){

                // Get all selected item id's
                draggingAssetID = [];

                // Push dragged item
                draggingAssetID.push($(this).data('asset-id'));

                // Push all selected items
                $("[data-asset-manager] tr[data-selected]").each(function() {

                    // Push each selected id into an array
                    var assetID = $(this).data('asset-id');
                    draggingAssetID.push(assetID);

                    // Fade each item selected
                    $(this).addClass("ui-dragging-from");

                });

                // Remove duplicates (cloned drag element, dragging an already selected item)
                draggingAssetID = removeDuplicates(draggingAssetID);

                // ONE ITEM SELECTED
            } else {

                // Get currently dragging item
                draggingAssetID = draggingAsset.data('asset-id');

            }

        },
        stop: function(event, ui) {
            //console.log('stop dragging asset');

            // Set dragging state
            isDraggingAsset = false;

            // Reset dragged-from classes (faded out)
            $('[data-asset-manager] tr.ui-draggable').removeClass("ui-dragging-from");

        }
    };

    /**
     * DRAGGABLE FOLDERS
     * Allow folders to be dragged and re-ordered
     * Source: http://api.jqueryui.com/draggable/
     */
    var isDraggingFolder, draggingFolder, draggingFolderID;
    var draggableFoldersOptions = {
        helper: "clone",    // Create copy to style when dragging
        revert: true,       // Animate back to original position if not dropped
        cancel: "",         // Cannot drag from these areas
        cursorAt: {
            left: -10,
            top: -10
        },
        start: function(event, ui) {
            //console.log('dragging folder');

            // Set dragging folder state
            isDraggingFolder = true;

            // Store dragging item
            draggingFolder = $(this);

            // Enable revert (if previously disabled by drop)
            $(this).draggable("option", "revert", true);

            // Apply dragged from state
            $(this).addClass("ui-dragging-from");

            // Get currently dragging item
            draggingFolderID = draggingFolder.data('folder-id');

        },
        stop: function(event, ui) {
            //console.log('stop dragging folder');

            // Set dragging state
            isDraggingFolder = false;

            // Reset dragged-from classes (faded out)
            $('[data-draggable-folder]').removeClass("ui-dragging-from");

        }
    };

    /**
     * DROPPABLE FOLDERS
     * Allow asset table rows to be dropped onto folders in the sidebar
     * Source: http://api.jqueryui.com/droppable/
     */
    var droppableFoldersOptions = {
        accept: "[data-draggable-asset], [data-draggable-folder]",
        greedy: true,
        tolerance: 'pointer',
        drop: function(event, ui) {

            // Get dropped folder ID
            var dropFolderID = $(this).data('folder-id');


            // If dragging folder
            if (isDraggingFolder) {

                // Cancel revert of dragging item if dropped successfully
                draggingFolder.draggable("option", "revert", false);

                //console.log('Dropped folder ' + draggingFolderID + ' onto ' + dropFolderID);

                // Move folder
                move_folders(draggingFolderID, dropFolderID);

                // If dragging asset
            } else if (isDraggingAsset){

                // Cancel revert of dragging item if dropped successfully
                draggingAsset.draggable("option", "revert", false);

                //console.log('Dropped asset ' + draggingAssetID + ' onto ' + dropFolderID);

                // Move asset
                move_files(draggingAssetID, dropFolderID);

            }


        }
    };

    $(document).on( 'closed.fndtn.reveal', '[data-reveal]', function(){
        if( $(this).attr('id') == 'upload-modal' ){
            $(this).remove();
        }
    });
});

</script>