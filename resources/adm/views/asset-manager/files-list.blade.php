<table class="list-table hover-table-row width-100 border-0 margin-b1-negative">

    <thead>
        <tr class="nowrap">
            <th class="list-table-checkbox-col">
                <div class="block margin-t2">
                    <input data-select-all-checkboxes type="checkbox" id="selected-all" name="selected-all" class="fancy-checkbox margin-0">
                    <label for="selected-all" class="font-size-16 fixed-font-size"><span class="visually-hidden">Select All</span></label>
                </div>
            </th>
            <th data-sortable-column data-sort-field="name" data-dir="{{ $sort_field && $sort_field == 'name' ? $sort_direction : '' }}" class="orderable {{ $sort_field && $sort_field == 'name' ? 'ordered ordered-' . $sort_direction : '' }}" title="Order by File">File</th>
            <th data-sortable-column data-sort-field="extension" data-dir="{{ $sort_field && $sort_field == 'extension' ? $sort_direction : '' }}" class="orderable {{ $sort_field && $sort_field == 'extension' ? 'ordered ordered-' . $sort_direction : '' }}" title="Order by Type">Type</th>
            <th title="Order by Dimensions">Dimensions</th>
            <th title="Order by Filesize">Filesize</th>
            <th data-sortable-column data-sort-field="created_at" data-dir="{{ $sort_field && $sort_field == 'created_at' ? $sort_direction : '' }}" class="orderable {{ $sort_field && $sort_field == 'created_at' ? 'ordered ordered-' . $sort_direction : '' }}" title="Order by Date">Date Added</th>
            <th title="Order by User">Added by</th>
            <th>&nbsp;</th>
        </tr>
    </thead>

    <tbody>

    @if( sizeof($list_directories) )
        @foreach( $list_directories as $directory )
            <tr data-action-menu="folder" data-action-id="{{ $directory->id }}" data-droppable-folder data-folder-id="{{ $directory->id }}" class="border-b1 colour-off-white user-select-none">
                <td class="list-table-checkbox-col">
                    <div class="block margin-t2">&nbsp;</div>
                </td>
                <td data-name>
                    <a href="#" data-directory-link data-directory-id="{{ $directory->id }}" class="table width-100 height-100 colour-grey">
                        <div class="asset-preview-cell table-cell vertical-middle">
                            <div class="asset-preview icon">
                                <div class="table width-100 height-100 line-height-1">
                                    <div class="table-cell">
                                        <i class="fi-adm fi-adm-folder font-size-41 colour-grey-light margin-t2 margin-r4"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-cell vertical-middle width-100 padding-l-gutter line-height-1">
                            <span data-sort-field class="block font-weight-600 colour-slate margin-b2">{{ $directory->name }}</span>
                        </div>
                    </a>
                </td>
                <td>Folder</td>
                <td>&nbsp;</td>
                <td><?php $count = $directory->files()->count(); echo $count == 1 ? $count . ' asset' : $count . ' assets'; ?></td>
                <td class="nowrap">{{ $directory->date_added }}</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        @endforeach
    @endif

    @if( sizeof( $files ) )
        @foreach( $files as $file )

            <tr data-draggable-asset data-asset-id="{{ $file->id }}" data-action-menu="file" data-action-id="{{ $file->id }}" class="border-b1 colour-off-white user-select-none{{ in_array( $file->id, $uploaded_files) ? ' highlight-row' : '' }}">
                <td data-asset-checkbox class="list-table-checkbox-col">
                    <div class="block margin-t2">
                        <input type="checkbox" id="selected[{{ $file->id }}]" name="selected-item" value=""  class="fancy-checkbox margin-0">
                        <label for="selected[{{ $file->id }}]" class="font-size-16 fixed-font-size"><span class="visually-hidden">Select</span></label>
                    </div>
                </td>
                <td data-name>
                    <a href="#" data-reveal-id="view-file-modal-{{ $file->id }}" data-file-url-{{ $file->id }}="{{ $file->url }}" title="Edit File" class="table width-100 height-100 colour-grey">
                        <div class="asset-preview-cell table-cell vertical-middle">
                            <div class="asset-preview{{ !$file->is_image ? 'icon' : '' }}">
                                <div class="table width-100 height-100 line-height-1" >
                                    <div class="table-cell{{ $file->height > $file->width ? ' text-center' : '' }}">
                                        @if( $file->is_image )
                                            <img src="{{ route('image.resize', ['path' => $file->path, 'dimensions' => $file->width >= $file->height ? '45x' : 'x45']) }}?{{ time() }}" title="{{ $file->name }}" class="{{ $file->width >= $file->height ? 'width-100' : 'height-100' }} inline-block" />
                                        @else
                                            <i class="fi-adm fi-adm-file font-size-41 colour-grey-light margin-t2"></i>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-cell vertical-middle width-100 padding-l-gutter line-height-1">
                            <span data-sort-field class="block font-weight-600 colour-slate margin-b2">{{ $file->name }}</span>
                            <span class="block">{{ $file->file_name }}</span>
                        </div>
                    </a>
                </td>
                <td>{{ strtoupper( $file->extension ) }}</td>
                <td class="nowrap">{{ $file->is_image ? $file->dimensions : '' }}</td>
                <td>{{ $file->filesize }}</td>
                <td class="nowrap">{{ $file->date_added }}</td>
                <td>{{ $file->added_by ? $file->added_by->name : '' }}</td>
                <td class="text-right nowrap">

                    <!-- EDIT FILE -->
                    <a href="#" data-reveal-id="view-file-modal-{{ $file->id }}" title="Edit File" class="button tiny round bordered colour-cancel text-shadow-none margin-b0 margin-r5">Edit</a>
                    @include('asset-manager.view-file-modal')
                    <!-- END EDIT FILE -->

                </td>
            </tr>

        @endforeach
    @endif

    @if( !sizeof( $files ) && !sizeof( $list_directories ) )
        <tr>
            <td colspan="8">
                <span class="block padding-gutter">No assets found.</span>
            </td>
        </tr>
    @endif

    </tbody>
</table>