<div class="js-section-action-bar-container relative" style="height: 79px;">
    <div class="js-section-action-bar section-action-bar top-0 left-0 width-100">
        <div class="row padding-y20">
            <div class="column">
                <div class="table width-100">
                    <div class="table-cell">
                        <h1 class="font-size-24 font-weight-600 margin-0">Asset Manager</h1>
                    </div>
                    <div class="table-cell text-right">
                        <div data-delete-folder-action class="inline-block vertical-middle margin-r-gutter">
                            @include('asset-manager.delete-folder-action')
                        </div>
                        <div class="inline-block vertical-middle margin-r-gutter">
                            <a href="#" data-reveal-id="folder-modal" class="button round bordered margin-b0 colour-cancel">Add New Folder</a>
                        </div>
                        <div class="inline-block vertical-middle">
                            <a href="#" data-reveal-id="upload-modal" data-upload-file-button class="button round gradient bordered colour-success margin-b0 inline-block vertical-middle font-size-0">
                                <span>
                                    <i class="fi-adm fi-adm-upload inline-block vertical-middle font-size-14 margin-r-gutter line-height-1"></i>
                                    <span class="font-size-14 inline-block vertical-middle">Upload File</span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>