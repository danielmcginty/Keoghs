@foreach( $directories as $directory )
    <li data-draggable-folder data-folder-id="{{ $directory->id }}" data-action-menu="folder" data-action-id="{{ $directory->id }}" class="user-select-none {{ ($directory_id == $directory->id) ? 'current ' : '' }}{{ sizeof($directory->children) ? 'has-children ' : '' }}{{ $directory->expanded ? 'is-expanded ' : '' }}">
        <div class="inner relative">
            <a href="#" data-droppable-folder data-folder-id="{{ $directory->id }}" data-directory-link data-directory-id="{{ $directory->id }}" data-directory-path="{{ $directory->path }}" class="folder-item" title="Click to open Folder. Drag to re-order.">
                <i class="fi-adm fi-adm-folder margin-r5"></i> {{ $directory->name }}
            </a>
            <div data-toggle-child-folders class="folder-toggle" title="Expand/Collapse Folders"></div>
        </div>
        @if( sizeof($directory->children) )
            <ol>
                @include('asset-manager.directory-list', ['directories' => $directory->children] )
            </ol>
        @endif
    </li>
@endforeach