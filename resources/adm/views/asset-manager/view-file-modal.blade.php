<div id="view-file-modal-{{ $file->id }}" class="reveal-modal asset-preview-modal view-asset-modal" data-reveal aria-hidden="true" role="dialog">

    {{ Form::open([ 'url' => route('adm.asset-manager.function', ['function' => 'upload']), 'type' => 'post', 'class' => 'js-file-edit-form' ]) }}

        <div class="js-modal-form-flash top-notification relative top-0 left-0 width-100 text-center">
            <div class="alert-box alert alert-dismissable margin-b0 bottom-0" style="display: none;"></div>
            <div class="alert-box success alert-dismissable margin-b0 bottom-0" style="display: none;"></div>
        </div>

        <div class="modal-content padding-0 height-100">
            <div class="row collapse height-100">

                <?php // Asset Details ?>
                <div class="file-details column small-3 padding-20" style="height: 689px;">

                    <p class="font-weight-600 margin-b-gutter">Asset Details</p>
                    <ul class="simple-list margin-b-gutter-full font-size-14">
                        <li><strong>File Name:</strong> <span class="js-file-name">{{ $file->name }}</span></li>
                        <li><strong>Dimensions:</strong> {{ $file->dimensions }}</li>
                        <li><strong>File Size:</strong> {{ $file->filesize }}</li>
                        <li><strong>File Type:</strong> {{ strtoupper( $file->extension ) }}</li>
                        <li><strong>Uploaded on:</strong> {{ $file->date_added }}</li>
                        <li><strong>Uploaded by:</strong> {{ $file->added_by ? $file->added_by->name : '' }}</li>
                    </ul>

                    <div class="table width-100">
                        <div class="table-cell">
                            @if( $file->is_image )
                                <a href="{{ $file->url }}" target="_blank" class="button tiny round gradient bordered colour-blue margin-r5 margin-b0"><span>View Original</span></a>
                            @else
                                <a href="{{ $file->url }}" target="_blank" class="button tiny round gradient bordered colour-blue margin-r5 margin-b0"><span>Download</span></a>
                            @endif
                        </div>
                        <div class="table-cell text-right">
                            <a data-replace-asset-from-edit-modal data-file-id="{{ $file->id }}" data-name="{{ $file->name }}" class="inline-block colour-blue underline-on-hover margin-r5 font-size-14">Replace</a>
                            <a href="#" data-file-id="{{ $file->id }}" class="js-delete-file-from-edit-modal inline-block colour-error underline-on-hover margin-0 font-size-14">Delete</a>
                        </div>
                    </div>

                    <hr class="colour-grey-light margin-y-gutter-full" />

                    <p class="font-size-14 margin-b20">File URL</p>
                    <p class="js-file-path break-word font-size-14">{{ route( 'image', $file->path ) }}</p>

                    <?php $column = array( 'field' => 'name', 'label' => 'File Name', 'required' => true, 'help' => 'Set the name of the file.' ); ?>
                    <div class="input-field{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">
                        @include('fields.form.label')
                        @include('fields.form.help_text')
                        {{ Form::text('name', $file->name, [ 'required' => 'required' ]) }}
                        @include('fields.form.error')
                    </div>

                    <?php $column = array( 'field' => 'alt_text', 'label' => 'Alt Text', 'required' => true, 'help' => 'A descriptive summary of the file.' ); ?>
                    <div class="input-field{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">
                        @include('fields.form.label')
                        @include('fields.form.help_text')
                        {{ Form::text('alt_text', $file->alt_text, [ 'required' => 'required' ]) }}
                        @include('fields.form.error')
                    </div>

                    {{ Form::hidden('file_id', $file->id) }}
                    {{ Form::hidden('file_extension', $file->extension) }}
                    {{ Form::hidden('directory_id', $directory_id) }}

                </div>

                <?php // Asset Preview ?>
                <div class="column small-9 height-100 relative padding-20">
                    <div class="asset-preview">
                        @if( $file->is_image )

                            <?php // Static Preview ?>
                            <?php
                                $dimensions = '';
                                if( $file->width > 860 || $file->height > 648 ){
                                    if( $file->width > 860 ){
                                        $dimensions = '860x';
                                    } else {
                                        $dimensions = 'x648';
                                    }
                                } else {
                                    if( $file->width > $file->height ){
                                        $dimensions = $file->width . 'x';
                                    } else {
                                        $dimensions = 'x' . $file->height;
                                    }
                                }
                            ?>
                            <img class="static-preview" src="{{ route('image.resize', ['path' => $file->path, 'dimensions' => $dimensions]) }}?{{ time() }}" alt="{{ $file->name }}" title="{{ $file->name }}" />

                        @else

                            <div class="table width-100 height-100">
                                <div class="table-cell vertical-middle text-center">

                                    <i class="fi-adm fi-adm-file font-size-62 colour-grey-lightest"></i>
                                    <p class="margin-b30 colour-grey-light">No preview available.</p>

                                    <a href="{{ $file->url }}" target="_blank" class="button tiny round gradient bordered colour-blue margin-r5 margin-b0"><span>Download File</span></a>

                                </div>
                            </div>

                        @endif
                    </div>
                </div>

            </div>
        </div>

        <div class="modal-actions">
            <button type="submit" class="button round gradient bordered colour-success margin-b0 margin-r10 outline-none"><span>Save</span></button>
            <a href="#" data-close-reveal class="button round bordered colour-cancel margin-b0">Close</a>
        </div>

    {{ Form::close() }}

</div>