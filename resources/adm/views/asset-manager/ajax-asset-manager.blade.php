@include('asset-manager.action-bar')

@include('asset-manager.listing-wrapper')

<!-- MODALS -->
@include('asset-manager.upload-modal')
@include('asset-manager.new-folder-modal')
@include('asset-manager.rename-folder-modal')
<!-- MODALS -->