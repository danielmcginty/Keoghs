<div class="js-modal-form-flash top-notification relative top-0 left-0 width-100 text-center">
    <div class="alert-box alert alert-dismissable margin-b0 bottom-0" style="display: none;"></div>
    <div class="alert-box success alert-dismissable margin-b0 bottom-0" style="display: none;"></div>
</div>

{{ Form::open([ 'url' => route('adm.asset-manager.function', ['function' => 'delete_files']), 'type' => 'post', 'class' => 'js-modal-delete-files-form' ]) }}
<div class="modal-content">
    @if (count($files) > 1)
        <p class="modal-heading padding-r40 font-weight-600 margin-b10">Delete selected files?</p>
        <p class="font-size-14">Permanently delete <strong>all selected files</strong>? This action <strong>cannot</strong> be reversed.</p>
    @else
        <p class="modal-heading padding-r40 font-weight-600 margin-b10">Delete the file?</p>
        <p class="font-size-14">Permanently delete "<strong>{{ $files[0]->file_name }}</strong>"? This action <strong>cannot</strong> be reversed.</p>
    @endif
</div>
<div class="modal-actions">
    <button type="submit" class="button round gradient bordered colour-error margin-b0 margin-r10">Delete File</button>
    <a href="#" data-close-reveal class="button round bordered colour-cancel margin-b0">Cancel</a>
</div>
<a class="close-reveal-modal visually-hidden" aria-label="Close">&#215;</a>
@foreach($file_ids as $index => $file_id)
    {{ Form::hidden('files[' . $index . ']', $file_id) }}
@endforeach
{{ Form::hidden('check_file_usage', 1) }}
{{ Form::close() }}