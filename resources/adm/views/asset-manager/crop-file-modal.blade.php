<?php
    $asset_group_field_name = 'asset_group[' . transform_key( $column['field'] ) . '][' . $reference . ']';
?>

<div id="{{ $modal_id }}" class="reveal-modal asset-preview-modal crop-asset-modal" data-reveal aria-hidden="true" role="dialog">

    <div class="js-modal-form-flash top-notification relative top-0 left-0 width-100 text-center">
        <div class="alert-box alert alert-dismissable margin-b0 bottom-0" style="display: none;"></div>
        <div class="alert-box success alert-dismissable margin-b0 bottom-0" style="display: none;"></div>
    </div>

    <div class="modal-content padding-0 height-100">
        <div class="row collapse height-100">

            <?php // Asset Details ?>
            <div class="file-details column small-3 padding-20" style="height: 689px;">

                <p class="font-weight-600 margin-b-gutter">File Details</p>
                <ul class="simple-list font-size-14">
                    <li><strong>File Name:</strong> <span class="js-file-name">{{ $file->name }}</span></li>
                    <li><strong>Dimensions:</strong> {{ $file->dimensions }}</li>
                    <li><strong>File Size:</strong> {{ $file->filesize }}</li>
                    <li><strong>File Type:</strong> {{ strtoupper( $file->extension ) }}</li>
                    <li><strong>Uploaded on:</strong> {{ $file->date_added }}</li>
                    <li><strong>Uploaded by:</strong> {{ $file->added_by ? $file->added_by->name : '' }}</li>
                </ul>

                <div class="table width-100">
                    <div class="table-cell">
                        <a href="{{ route('image', ['path' => $file->path]) }}" target="_blank" title="View Original" class="button tiny round bordered colour-blue margin-r5 margin-b0">
                            View Original
                        </a>
                    </div>
                    <div class="table-cell text-right">
                        <a data-remove-group-image-from-preview href="#" class="inline-block colour-error underline-on-hover margin-0 font-size-14">Remove</a>
                    </div>
                </div>

                <hr class="colour-grey-light margin-y-gutter-full" />

                <p class="font-weight-600 margin-b20">File URL</p>
                <p class="js-file-path break-word font-size-14">{{ route( 'image', $file->path ) }}</p>

                <div data-view-asset-fields>

                    <div class="input-field{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">
                        <label for="{{ $asset_group_field_name }}[alt]" class="margin-b-gutter">Alt Text</label>
                        <input type="text" name="{{ $asset_group_field_name }}[alt]" value="{{ isset($file->asset_group) ? $file->asset_group->alt : $file->alt_text }}" />
                    </div>

                </div>

            </div>

            <?php // Asset Preview ?>
            <div class="column small-9 padding-20 height-100 relative">

                @if( $file->croppable )
                    {{-- Crop Error --}}
                    <div class="absolute top-0 right-0 width-100 text-right z-index-1">
                        <div data-crop-warning class="none alert-box background colour-error inline-block radius padding-gutter margin-t15 margin-r15 margin-b0">
                            The cropped area is below the recommended image size of {{ $output_dims }}px
                        </div>
                    </div>

                    {{-- Crop Actions --}}
                    <div class="crop-actions margin-b-gutter-full font-size-14 colour-grey relative z-index-2">

                        <a href="#" data-crop-enable class="button tiny round bordered colour-blue gradient margin-r5 margin-b0"><span>Crop</span></a>

                        <a href="#" data-cropping-action data-crop-save class="none button tiny round bordered colour-success gradient margin-r5 margin-b0"><span>Save Crop</span></a>
                        <a href="#" data-cropping-action data-crop-reset class="none button tiny round bordered colour-default margin-r5 margin-b0"><span>Reset Crop</span></a>
                        <a href="#" data-cropping-action data-crop-cancel class="none button tiny round bordered colour-default margin-r5 margin-b0"><span>Cancel</span></a>

                    </div>
                @endif

                <div data-asset-preview class="asset-preview relative">

                    <?php // Crop Preview ?>
                    <?php
                        // Get separated image dimensions - to calculate aspect ratio
                        $image_dimensions = explode("x", $output_dims);
                        $image_width = $image_dimensions[0];
                        $image_height = $image_dimensions[1];

                        $aspect_ratio = ($image_width > 0 && $image_height > 0) ? $image_width / $image_height : 0;

                        $image = route('image.resize', ['path' => $file->path, 'dimensions' => $output_dims]);
                        if( $file->crop !== false && !empty( $file->crop ) ){
                            $image = route('image.crop', ['path' => $file->path, 'dimensions' => $output_dims, 'crop' => $file->crop]);
                        }
                    ?>
                    <img data-crop-preview class="crop-preview" data-base-url="{{ route('theme', ['url' => '']) }}" data-img-path="{{ $file->path }}" data-min-width="<?php echo $image_width; ?>" data-min-height="<?php echo $image_height; ?>" data-crop-aspect-ratio="{{ $aspect_ratio }}" data-img-dimensions="{{ $output_dims }}" src="{{ $image }}" alt="{{ $file->name }}" title="{{ $file->name }}" />

                    <?php // Crop Canvas (Hidden by default) ?>
                    <img data-crop-image class="crop-image none" src="{{ route('image', ['path' => $file->path]) }}" alt="{{ $file->name }}" title="{{ $file->name }}" />

                    <?php // Crop Details ?>
                    <input type="hidden" data-asset-group-field="crop_width"  name="{{ $asset_group_field_name }}[crop_width]" value="{{ isset($file->asset_group) ? $file->asset_group->width : 0 }}" />
                    <input type="hidden" data-asset-group-field="crop_height" name="{{ $asset_group_field_name }}[crop_height]" value="{{ isset($file->asset_group) ? $file->asset_group->height : 0 }}" />
                    <input type="hidden" data-asset-group-field="x_coordinate" name="{{ $asset_group_field_name }}[x_coordinate]" value="{{ isset($file->asset_group) ? $file->asset_group->x_coordinate : 0 }}" />
                    <input type="hidden" data-asset-group-field="y_coordinate" name="{{ $asset_group_field_name }}[y_coordinate]" value="{{ isset($file->asset_group) ? $file->asset_group->y_coordinate : 0 }}" />

                    <input type="hidden" data-asset-group-field="cropbox_left" name="{{ $asset_group_field_name }}[cropbox_left]" value="{{ isset($file->asset_group) ? $file->asset_group->cropbox_left : 0 }}" />
                    <input type="hidden" data-asset-group-field="cropbox_top" name="{{ $asset_group_field_name }}[cropbox_top]" value="{{ isset($file->asset_group) ? $file->asset_group->cropbox_top : 0 }}" />
                    <input type="hidden" data-asset-group-field="cropbox_width" name="{{ $asset_group_field_name }}[cropbox_width]" value="{{ isset($file->asset_group) ? $file->asset_group->cropbox_width : 0 }}" />
                    <input type="hidden" data-asset-group-field="cropbox_height" name="{{ $asset_group_field_name }}[cropbox_height]" value="{{ isset($file->asset_group) ? $file->asset_group->cropbox_height : 0 }}" />

                </div>

            </div>

        </div>

        <div class="modal-actions">
            {{-- <button type="submit" data-view-asset-save-change class="button round gradient bordered colour-success margin-b0 margin-r10 outline-none">Save</button> --}}
            <a href="#" data-close-reveal class="button round bordered colour-cancel margin-b0">Close</a>
        </div>

        <a class="close-reveal-modal visually-hidden" aria-label="Close">&#215;</a>

    </div>
</div>