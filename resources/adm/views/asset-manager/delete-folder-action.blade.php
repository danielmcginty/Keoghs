<!-- DELETE FOLDER -->
<a style="<?php if( $directory_id > 0 ) { ?>display: inline-block;<?php } else { ?>display: none;<?php } ?>" href="#" data-folder-id="<?php if( $directory_id > 0 ) { echo $directory_id; } ?>" class="js-delete-folder-button button round transparent padding-x-gutter-full margin-b0 margin-r-gutter">
    <span class="colour-error underline-none-on-hover">Delete Folder</span>
</a>
<div id="confirm-delete-folder-modal" class="reveal-modal notice-modal" data-reveal aria-hidden="true" role="dialog">

    <!-- Form gets ajax-ed into here -->

</div>
<!-- END DELETE FOLDER -->