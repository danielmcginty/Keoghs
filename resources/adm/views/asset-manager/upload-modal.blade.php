<style scoped>
@keyframes progress-bar-stripes {
    from  { background-position: 40px 0; }
    to    { background-position: 0 0; }
}
.progress-bar-bar {
    background-image: linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
    background-size: 40px 40px;
    animation: progress-bar-stripes 2s linear infinite;
}
</style>
<div data-upload-modal>
    <div id="upload-modal" class="reveal-modal notice-modal" data-reveal aria-hidden="true" role="dialog">

        {{ Form::open([ 'url' => route('adm.asset-manager.function', ['function' => 'upload']), 'type' => 'post', 'files' => true, 'class' => 'relative' ]) }}

            <div data-loading-overlay class="absolute width-100 height-100 left-0 top-0" style="z-index: 99; display: none;">
                <div class="absolute width-100 height-100 || background colour-grey-lightest opacity-7"></div>
                <div class="relative width-100 height-100 table">
                    <div class="table-cell vertical-middle text-center padding-x-gutter-full">
                        <div class="progress-bar relative" style="height: 40px;">
                            <div class="progress-bar-bg background colour-off-white radius || absolute width-100 height-100"></div>
                            <div data-progress-bar class="progress-bar-bar background colour-primary radius || absolute height-100" style="width: 0;"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="js-modal-form-flash top-notification relative top-0 left-0 width-100 text-center">
                <div class="alert-box alert alert-dismissable margin-b0 bottom-0" style="display: none;"></div>
                <div class="alert-box success alert-dismissable margin-b0 bottom-0" style="display: none;"></div>
            </div>

            <div class="modal-content">
                <p class="modal-heading padding-r40 font-weight-600 margin-b10">Upload File @if( $directory_id > 0 && is_object( $current_directory ) )to {{ $current_directory->name }}@endif</p>
                <?php $column = array( 'field' => 'asset', 'label' => 'Select File', 'required' => true, 'help' => 'Pick a file to upload' ); ?>
                <div class="input-field{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">
                    @include('fields.form.label')
                    @include('fields.form.help_text')
                    {{ Form::file('asset[]', [ 'data-asset-upload-file-input' => '', 'multiple' => '']) }}
                    @include('fields.form.error')
                </div>
                <div data-asset-upload-single-fields class="none">
                    <?php $column = array( 'field' => 'name', 'label' => 'File Name', 'required' => true, 'help' => 'Name of the file, in plain english. This should describe the file.' ); ?>
                    <div class="input-field{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">
                        @include('fields.form.label')
                        @include('fields.form.help_text')
                        {{ Form::text('name', null, [ 'data-asset-upload-name-input' => '', 'id' => 'name', 'required' => 'required' ]) }}
                        @include('fields.form.error')
                    </div>
                    <?php $column = array( 'field' => 'alt_text', 'label' => 'Alt Text', 'required' => false, 'help' => 'Text description of the file. Shown if image does not load, read by screen reader.' ); ?>
                    <div class="input-field{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">
                        @include('fields.form.label')
                        @include('fields.form.help_text')
                        {{ Form::text('alt_text', null, [ 'id' => 'alt_text' ]) }}
                        @include('fields.form.error')
                    </div>
                </div>
                {{ Form::hidden('directory_id', $directory_id) }}
                @if( isset($dimensions) )
                    {{ Form::hidden('dimensions', $dimensions) }}
                @endif
            </div>

            <div class="modal-actions">
                <button type="submit" class="button round gradient bordered colour-success margin-b0 margin-r10 outline-none"><span>Upload</span></button>
                <a href="#" data-close-reveal class="button round bordered colour-cancel margin-b0">Cancel</a>
            </div>

        {{ Form::close() }}

    </div>
</div>