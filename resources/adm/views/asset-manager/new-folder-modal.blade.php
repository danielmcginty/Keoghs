<div data-new-folder-modal>
    <div id="folder-modal" class="reveal-modal notice-modal" data-reveal aria-hidden="true" role="dialog">

        {{ Form::open([ 'url' => route('adm.asset-manager.function', ['function' => 'new_folder']), 'type' => 'post' ]) }}

            <div class="js-modal-form-flash top-notification relative top-0 left-0 width-100 text-center">
                <div class="alert-box alert alert-dismissable margin-b0 bottom-0" style="display: none;"></div>
                <div class="alert-box success alert-dismissable margin-b0 bottom-0" style="display: none;"></div>
            </div>

            <div class="modal-content">
                <p class="modal-heading padding-r40 font-weight-600 margin-b-gutter-full">New Folder @if( $directory_id > 0 && is_object( $current_directory ) )(inside "{{ $current_directory->name }}")@endif</p>
                <?php $column = array( 'field' => 'name', 'label' => 'Name', 'required' => true, 'help' => 'Folder Name' ); ?>
                <div class="input-field{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">
                    <span class="visually-hidden">
                    @include('fields.form.label')
                    </span>
                    @include('fields.form.help_text')
                    {{ Form::text('name', null, [ 'required' => 'required', 'placeholder' => '' ]) }}
                    @include('fields.form.error')
                </div>
                {{ Form::hidden('parent', $directory_id) }}
                @if( isset($dimensions) )
                    {{ Form::hidden('dimensions', $dimensions) }}
                @endif
            </div>

            <div class="modal-actions">
                <button type="submit" class="button round gradient bordered colour-success margin-b0 margin-r10 outline-none">Add Folder</button>
                <a href="#" data-close-reveal class="button round bordered colour-cancel margin-b0">Cancel</a>
            </div>

        {{ Form::close() }}

    </div>
</div>