<script type="text/javascript">
// Return a helper with preserved width of cells
var galleryFixHelper = function(e, ui) {
    ui.children().each(function() {
        $(this).width($(this).width());
    });
    return ui;
};

// Init Sortable
function init_asset_gallery_sortable() {
    $('[data-asset-gallery-assets]').sortable({
        helper: galleryFixHelper,
        cursor: 'grabbing',
        axis: 'y',
        handle: '[data-gallery-asset-drag-handle]',
        items: '[data-asset]',
        placeholder: 'sortable-placeholder',
        tolerance: 'pointer',  // pointer, intersect
        revert: 150,
        opacity: 1,
        containment: 'parent',
        forcePlaceholderSize: true,
        create: function() {
            var $parentTable = $(this).parents('[data-asset-gallery]').first();
            $parentTable.find('.table').removeClass('overflow-hidden'); // Correct issues with items which need to overflow the table e.g. http://screencast.com/t/OCDWdPO88
        },
        start: function(){
            var $parentTable = $(this).parents('[data-asset-gallery]').first();
            $parentTable.find('.table').addClass('overflow-hidden'); // Correct issues with items which need to overflow the table e.g. http://screencast.com/t/OCDWdPO88
        },
        stop: function(){
            var $parentTable = $(this).parents('[data-asset-gallery]').first();
            $parentTable.find('.table').removeClass('overflow-hidden'); // Correct issues with items which need to overflow the table e.g. http://screencast.com/t/OCDWdPO88
        },
        update: function () {
            var $parentTable = $(this).parents('[data-asset-gallery]').first();
            setAssetGalleryRowIndexes($parentTable);
        }
    });
}

// Add a new image
$(document).on('click', '[data-add-gallery-asset]', function(evt){
    evt.preventDefault();
    evt.stopPropagation();

    var $container = $(this).parents('[data-asset-gallery]').first();
    var $gallery = $container.find('[data-asset-gallery-assets]');

    $.ajax({
        url: '{{ route('adm.ajax', ['ajax_add_gallery_asset']) }}',
        type: 'POST',
        data: {
            field: $container.data('field-name-prefix'),
            index: $gallery.find('[data-asset]').length,
            multi_size: $container.data('multi_size'),
            sizes: $container.data('sizes')
        },
        dataType: 'json',
        beforeSend: function(){
            showPageLoadingOverlay( true );
        },
        success: function( json ){
            if( json.success ){
                $gallery.append( json.new_asset );
                $gallery.trigger( 'bspk-asset-gallery-asset-added', json.new_asset );
            }
        },
        complete: function(){
            showPageLoadingOverlay();
            init_asset_gallery_sortable();
        }
    });
});

// Remove an image
$(document).on('click', '[data-asset-gallery] [data-remove-gallery-asset]', function(e) {
    e.preventDefault();
    e.stopPropagation();

    var $parentTable = $(this).parents('[data-asset-gallery]').first();

    // Remove the row
    $(this).parents('[data-asset]').first().remove();

    setAssetGalleryRowIndexes($parentTable);
});

// Re-increment the table row index for the name attributes of all inputs in each row
function setAssetGalleryRowIndexes(targetTable) {
    targetTable.find('[data-asset]').each(function( row_index, row ) {
        var $row = $(row);

        var field_prefix = $row.data('field-prefix');
        field_prefix = field_prefix.replace( /(\[)/g, '\\[' );

        var regex = new RegExp( '(' + field_prefix + '\\[)(\\d*)(]\\[.*)' );

        $row.find('input, textarea, select, label').each(function(index, item) {

            if (typeof( this.id ) != 'undefined') {
                this.id = this.id.replace(regex, function( str, p1, p2, p3 ){
                    return p1 + row_index + p3;
                });
            }

            if (typeof( this.for ) != 'undefined') {
                this.for = this.for.replace(regex, function( str, p1, p2, p3 ){
                    return p1 + row_index + p3;
                });
            }

            if (typeof( this.name ) != 'undefined') {
                this.name = this.name.replace(regex, function( str, p1, p2, p3 ){
                    return p1 + row_index + p3;
                });
            }

        });
    });
}

function new_asset_gallery_added(){
    var $last_gallery = $('[data-asset-gallery]').last();

    $last_gallery.find('[data-add-gallery-asset]').trigger('click');
}
</script>