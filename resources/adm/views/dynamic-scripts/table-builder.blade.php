<script>

    // TABLE BUILDER

    // Return a helper with preserved width of cells
    var tableFixHelper = function(e, ui) {
        ui.children().each(function() {
            $(this).width($(this).width());
        });
        return ui;
    };

    // Init Sortable
    function initTableBuilder(){
        $('[data-table-builder]').sortable({
            helper: tableFixHelper,
            cursor: 'grabbing',
            axis: 'y',
            handle: '[data-drag-handle]',
            items: '[data-table-row]',
            placeholder: 'sortable-placeholder',
            tolerance: 'pointer',  // pointer, intersect
            revert: 150,
            opacity: 1,
            containment: "parent",
            forcePlaceholderSize: true,
            create: function() {
                var $parentTable = $(this).parents('[data-table-builder-wrapper]').first();
                $parentTable.find('.table').removeClass('overflow-hidden'); // Correct issues with items which need to overflow the table e.g. http://screencast.com/t/OCDWdPO88
            },
            start: function(){
                var $parentTable = $(this).parents('[data-table-builder-wrapper]').first();
                $parentTable.find('.table').addClass('overflow-hidden'); // Correct issues with items which need to overflow the table e.g. http://screencast.com/t/OCDWdPO88
            },
            stop: function(){
                var $parentTable = $(this).parents('[data-table-builder-wrapper]').first();
                $parentTable.find('.table').removeClass('overflow-hidden'); // Correct issues with items which need to overflow the table e.g. http://screencast.com/t/OCDWdPO88
            },
            update: function(){
                var $parentTable = $(this).parents('[data-table-builder-wrapper]').first();
                setTableBuilderRowIndexes($parentTable);
            }
        });
    }
    initTableBuilder();

    // Add a new row
    $('body').on('click', '[data-table-builder-wrapper] .js-add-row', function(e) {
        e.preventDefault();
        e.stopPropagation();

        var $parentTable = $(this).parents('[data-table-builder-wrapper]').first();

        var postData = {
            column: $parentTable.find('[data-table-builder]').data('column'),
            row_index: $parentTable.find('[data-table-row]').length,
        };

        console.log(postData);

        $.ajax({
            url: '{{ route('adm.ajax', ['ajax_add_table_row']) }}',
            type: 'POST',
            dataType: 'json',
            data: postData,
            beforeSend: function(){
                showPageLoadingOverlay(true);
            },
            success: function(data){
                if(data.rows){
                    $parentTable.find('[data-table-builder]').append(data.rows);
                }
            },
            complete: function(){
                setTableBuilderRowIndexes($parentTable);
                showPageLoadingOverlay(false);
            }
        });
    });


    // Delete row
    $('body').on('click', '[data-table-builder-wrapper] .js-delete-row', function(e) {
        e.preventDefault();
        e.stopPropagation();

        var $parentTable = $(this).parents('[data-table-builder-wrapper]').first();

        // Remove the row
        $(this).parents('[data-table-row]').first().remove();

        setTableBuilderRowIndexes($parentTable);
    });


    // Re-increment the table row index for the name attributes of all inputs in each row
    function setTableBuilderRowIndexes(targetTable) {
        targetTable.find('[data-table-row]').each(function( rowIndex, row ) {
            var $row = $(row);

            var fieldPrefix = $row.data('field-prefix');
            fieldPrefix = fieldPrefix.replace( /(\[)/g, '\\[' );

            var regex = new RegExp( '(' + fieldPrefix + '\\[)(\\d*)(]\\[.*)' );

            var newRowIndex = rowIndex + 1;

            $row.find('input, textarea, select, label').each(function(index, item) {

                if (typeof( this.id ) != 'undefined') {
                    this.id = this.id.replace(regex, function( str, p1, p2, p3 ){
                        return p1 + newRowIndex + p3;
                    });
                }

                if (typeof( this.for ) != 'undefined') {
                    this.for = this.for.replace(regex, function( str, p1, p2, p3 ){
                        return p1 + newRowIndex + p3;
                    });
                }

                if (typeof( this.name ) != 'undefined') {
                    this.name = this.name.replace(regex, function( str, p1, p2, p3 ){
                        console.log(p1);
                        console.log(p2);
                        console.log(p3);
                        return p1 + newRowIndex + p3;
                    });
                }

            });
        });
    }


</script>