<?php // Image Crop Functions ?>
<script src="/admin/dist/assets/js/vendor/cropper.js"></script>
<!--<script src="/admin/dist/assets/js/components/image-crop.js"></script>-->

<script>

    // Remove Single Image
    $(document).on('click', '[data-remove-image]', function (e) {
        e.preventDefault();

        var parentImageContainer = $(this).parents('[data-image-container]');

        // Clear value in hidden input, remove old preview
        parentImageContainer.find('input[data-asset-input-id]').val(0);

        // Switch out preview with "Select Image" block
        parentImageContainer.find('[data-image-preview-container]').addClass('none');
        parentImageContainer.find('[data-image-select-placeholder]').removeClass('none');

        // Hide image actions
        parentImageContainer.find('[data-image-actions]').addClass('none');

    });

    $(document).on('click', '[data-remove-group-image-from-preview]', function(e){
        e.preventDefault();

        if( typeof( currentAssetField ) != 'undefined' ){
            currentAssetField.find('[data-remove-group-image]').click();
            $('[data-reveal].crop-asset-modal.open').foundation('reveal', 'close');
        }
    });

    // Remove Image from Group Image field
    $(document).on('click', '[data-remove-group-image]', function (e) {
        e.preventDefault();

        var parentImageContainer = $(this).parents('[data-image-container]');

        // Clear value in hidden input, remove old preview
        parentImageContainer.find('input[data-asset-input-id]').val(0);

        var $previewer = parentImageContainer.find('.js-asset-manager-modal-opener');

        $.ajax({
            url: '{{ route('adm.asset-manager.function', ['reset_file']) }}',
            type: 'POST',
            data: {
                reference: $previewer.data('reference'),
                details: $previewer.data('details'),
                column: $previewer.data('column'),
                field_id: $previewer.data('image-input-id'),
                dimensions: $previewer.data('crop-dimensions'),
                table: $previewer.data('table'),
                item_id: $previewer.data('item-id'),
                language_id: $previewer.data('language')
            },
            dataType: 'json',
            beforeSend: function(){
                showPageLoadingOverlay( true );
            },
            success: function( json ){
                if( json.success ){
                    var $output = $(json.output);
                    parentImageContainer.html( $output.html() );
                } else {
                    alert( json.error );
                }
            },
            complete: function(){
                showPageLoadingOverlay();
            }
        });

    });


    /**
     * IMAGE CROPPING UI
     * Using: https://fengyuanchen.github.io/cropperjs/
     */

    // Store current field and open modal - used to reference that modals crop ui and crop preview output image
    var currentAssetField,
            currentAssetModal;

    // Store crop area (image cropper is applied to)
    var cropAttributes = {};
    var cropArea,
            cropAspectRatio,
            cropImageWidth,
            cropImageHeight,
            cropMinWidth,
            cropMinHeight;

    // Store crop dimensions
    var inputCropWidth,
            inputCropHeight,
            inputCropOffsetX,
            inputCropOffsetY,
            inputCropBoxLeft,
            inputCropBoxTop,
            inputCropBoxWidth,
            inputCropBoxHeight;


    // Record asset field being edited
    $(document).on('click', '[data-edit-asset]', function () {

        currentAssetField = $(this).parents('[data-image-container]');

    });


    // Init Crop UI
    $(document).on('click', '[data-crop-enable]', function (e) {
        e.preventDefault();

        // GET CROP VARIABLES

        // Store current modal
        currentAssetModal = $('[data-reveal].crop-asset-modal.open');

        // Get image to crop (full, uncropped image)
        cropArea = currentAssetModal.find('[data-crop-image]');

        // Get crop aspect ratio
        cropAspectRatio = currentAssetModal.find('[data-crop-preview]').data('crop-aspect-ratio');

        // Get min width/height - used to show warning if cropped smaller than these sizes
        cropMinWidth = currentAssetModal.find('[data-crop-preview]').data('min-width');
        cropMinHeight = currentAssetModal.find('[data-crop-preview]').data('min-height');

        // Get crop position (either default center/center, or previous custom crop
        inputCropWidth = parseInt( currentAssetModal.find('input[data-asset-group-field="crop_width"]').val() );
        inputCropHeight = parseInt( currentAssetModal.find('input[data-asset-group-field="crop_height"]').val() );

        inputCropBoxLeft = parseInt( currentAssetModal.find('input[data-asset-group-field="cropbox_left"]').val() );
        inputCropBoxTop = parseInt( currentAssetModal.find('input[data-asset-group-field="cropbox_top"]').val() );
        inputCropBoxWidth = parseInt( currentAssetModal.find('input[data-asset-group-field="cropbox_width"]').val() );
        inputCropBoxHeight = parseInt( currentAssetModal.find('input[data-asset-group-field="cropbox_height"]').val() );

        // If previous custom crop has not been applied (defaults to 0 for dimension values, which cannot be selected by the crop box)
        var hasExistingCrop = inputCropWidth + inputCropHeight;

        // If no crop has been previously applied, return false (if so, init cropper in 'default' center/center state)
        if (hasExistingCrop == 0){
            hasExistingCrop = false;
        } else {
            hasExistingCrop = true;
        }


        // INIT CROPPING INTERFACE

        // Show Cropper Canvas
        showCropper();

        // Wrap crop image in container with exact width/height of image - to calculate correct crop canvas size
        cropArea.wrap('<div data-canvas-container class="crop-canvas-container inline-block relative" style="width: ' + cropImageWidth + 'px; height: ' + cropImageHeight + 'px;"></div>');

        // Set crop options
        var cropOptions = {
            viewMode: 1, // https://github.com/fengyuanchen/cropper#viewmode
            dragMode: 'none', // prevent new crop boxes being created if dragging,
            restore: false, // https://github.com/fengyuanchen/cropper/issues/524
            autoCropArea: 1,
            guides: true,
            aspectRatio: cropAspectRatio,
            rotatable: false,
            zoomable: false,
            zoomOnTouch: false,
            zoomOnWheel: false,
            toggleDragModeOnDblclick: false,
            minCropBoxWidth: 70,
            minCropBoxHeight: 70,

            // Whilst building...
            build: function() {

                //cropArea.addClass('building');

            },

            // Once built...
            built: function() {
                //console.log('built');

                //cropArea.removeClass('building');
                setTimeout(function(){
                    cropArea.parent().addClass('built');
                }, 200);

                // If an existing crop has been saved, restore crop position/size
                if (hasExistingCrop){

                    // Slight delay as repositioning does not always get applied
                    //setTimeout(function(){

                    cropArea.cropper("setCropBoxData", {
                        left: inputCropBoxLeft,
                        top: inputCropBoxTop,
                        width: inputCropBoxWidth,
                        height: inputCropBoxHeight
                    });

                    //}, 50);

                }

            },

            // When crop area is moved...
            crop: function(e) {

                // Grab result data for cropping image.
                cropAttributes.x = e.x;
                cropAttributes.y = e.y;
                cropAttributes.width = Math.round(e.width);
                cropAttributes.height = Math.round(e.height);

                // Visual output of dimensions
                $('.cropper-crop-box').attr('data-dimensions', cropAttributes.width + ' x ' + cropAttributes.height);

                // Toggle warning message if cropped area is smaller than recommended size
                if ( cropAttributes.width < cropMinWidth || cropAttributes.height < cropMinHeight ){
                    $('[data-crop-warning]').removeClass('none');
                } else {
                    $('[data-crop-warning]').addClass('none');
                }

            }

        };

        // Init Cropper UI
        cropArea.cropper(cropOptions);

    });


    // "Save Crop" Button
    $(document).on('click', '[data-crop-save]', function (e) {
        e.preventDefault();

        // Get crop data
        var cropData = cropArea.cropper('getData', { rounded: true });
        var cropBoxData = cropArea.cropper('getCropBoxData');
        //console.log(cropData);

        // Get crop box overlay position (used to restore exact position if re-cropping)
        var cropBoxLeft = Math.round(cropBoxData["left"]);
        var cropBoxTop = Math.round(cropBoxData["top"]);
        var cropBoxWidth = Math.round(cropBoxData["width"]);
        var cropBoxHeight = Math.round(cropBoxData["height"]);

        // Calculate crop offset (from top left)
        var cropOffsetX = cropData["x"];
        var cropOffsetY = cropData["y"];
        var cropOffset = cropOffsetX + "x" + cropOffsetY;

        // Calculate crop size
        var cropWidth = cropData["width"];
        var cropHeight = cropData["height"];
        var cropSize = cropWidth + "x" + cropHeight;

        // Get crop preview and image details
        var cropPreview = currentAssetModal.find('[data-crop-preview]');
        var imgBaseURL = cropPreview.data('base-url');
        var imgPath = cropPreview.data('img-path');
        var imgDimensions = cropPreview.data('img-dimensions');

        // Build new cropped image URL
        var cropNewURL = imgBaseURL + "/images/" + imgDimensions + "/" + cropSize + "-" + cropOffset + "/" + imgPath;
        var cropNewSmallURL = imgBaseURL + "/images/123x/" + cropSize + "-" + cropOffset + "/" + imgPath;

        // Update crop preview and image field with new cropped image
        cropPreview.attr("src", cropNewURL);
        currentAssetField.find('.js-asset-preview').html('<div data-asset-preview-image class="width-100 height-100 background-contain background-center" style="background-image: url(' + cropNewSmallURL + ');"></div>');

        // Hide Crop UI
        hideCropper();

        // Update hidden inputs to save crop
        saveCropState(cropWidth, cropHeight, cropOffsetX, cropOffsetY, cropBoxLeft, cropBoxTop, cropBoxWidth, cropBoxHeight);

        // Close model (to better visually confirm crop is applied)
        // MB - Don't close the modal! Leave it open, but the cropper is hidden - showing cropped image
        // $(this).closest('[data-reveal]').foundation('reveal', 'close');

    });


    // Save user chosen crop state
    function saveCropState(cropWidth, cropHeight, cropOffsetX, cropOffsetY, cropBoxLeft, cropBoxTop, cropBoxWidth, cropBoxHeight){

        // Get hidden inputs
        var cropWidthInput = currentAssetModal.find('input[data-asset-group-field="crop_width"]');
        var cropHeightInput = currentAssetModal.find('input[data-asset-group-field="crop_height"]');
        var cropOffsetXInput = currentAssetModal.find('input[data-asset-group-field="x_coordinate"]');
        var cropOffsetYInput = currentAssetModal.find('input[data-asset-group-field="y_coordinate"]');

        var cropBoxLeftInput = currentAssetModal.find('input[data-asset-group-field="cropbox_left"]');
        var cropBoxTopInput = currentAssetModal.find('input[data-asset-group-field="cropbox_top"]');
        var cropBoxWidthInput = currentAssetModal.find('input[data-asset-group-field="cropbox_width"]');
        var cropBoxHeightInput = currentAssetModal.find('input[data-asset-group-field="cropbox_height"]');

        // Update crop hidden inputs with new area
        cropWidthInput.val(cropWidth);
        cropHeightInput.val(cropHeight);
        cropOffsetXInput.val(cropOffsetX);
        cropOffsetYInput.val(cropOffsetY);

        cropBoxLeftInput.val(cropBoxLeft);
        cropBoxTopInput.val(cropBoxTop);
        cropBoxWidthInput.val(cropBoxWidth);
        cropBoxHeightInput.val(cropBoxHeight);

    }


    // Reset Cropper
    $(document).on('click', '[data-crop-reset]', function (e) {
        e.preventDefault();

        currentAssetModal.find('[data-crop-image]').cropper('reset');

    });

    // Cancel Cropper
    $(document).on('click', '[data-crop-cancel]', function (e) {
        e.preventDefault();

        // Hide Cropper
        hideCropper();

    });


    // Show the cropping ui
    function showCropper (){

        // Show Crop Actions
        // ---

        // Hide Enable Button
        currentAssetModal.find('[data-crop-enable]').addClass('none');

        // Show Cropping Action Buttons (Apply/Reset/Cancel etc)
        currentAssetModal.find('[data-cropping-action]').removeClass('none');


        // Show Crop UI
        // ---

        // Hide Crop Preview
        currentAssetModal.find('[data-crop-preview]').addClass('none');

        // Show Crop UI
        currentAssetModal.find('[data-crop-image]').removeClass('none');


        // Get image dimensions
        // ---

        // Get image dimensions (used to size cropper area)
        cropImageWidth = currentAssetModal.find('.crop-image').width();
        cropImageHeight = currentAssetModal.find('.crop-image').height();

    }

    // Hide the cropping ui
    function hideCropper(){

        // Hide Crop UI, show preview
        // ---

        // Destroy Cropper
        cropArea.cropper('destroy');
        cropArea.unwrap();

        // Hide Crop UI
        cropArea.addClass('none');

        // Show (Old, Unchanged) Crop Preview
        currentAssetModal.find('[data-crop-preview]').removeClass('none');


        // Reset Crop Actions
        // ---

        // Show Enable Button
        currentAssetModal.find('[data-crop-enable]').removeClass('none');

        // Hide Cropping Action Buttons (Apply/Reset/Cancel etc)
        currentAssetModal.find('[data-cropping-action]').addClass('none');

    }

</script>



<?php // Asset Select Modal ?>

<?php // TODO Move functions into component js ?>
<?php // <script src="/admin/dist/assets/js/components/asset-select-modal.js"></script> ?>

<script type="text/javascript">

    var asset_manager_opened_by;
    var asset_manager_opened_by_ID;

    var asset_manager_last_directory = 0;

    var asset_manager_images_only = false;

    var asset_manager_multiple_select = false;

    // jQuery object for the directory list container (underneath site assets)
    var $directoryListing;

    // jQuery object for the file list container
    var $filesListing;

    // jQuery object for the delete folder action container
    var $deleteActionArea;

    // jQuery object for the new folder modal container
    var $newFolderModal;

    // jQuery object for the upload modal container
    var $uploadModal;

    // jQuery object for the dynamic asset manager modal
    var $dynamicModal;

    var $assetManagerModal = $('#asset-manager-modal');

    $(document).ready( function(){
        asset_manager_last_directory = 0;
        $.ajax({
            url: '{{ route('adm.asset-manager') }}',
            type: 'get',
            data: {
//                    directory_id: 0,
                modal: true,
                dimensions: $('.js-asset-manager-modal-opener[data-image-input-id="' + asset_manager_opened_by_ID + '"]').data('crop-dimensions')
            },
            success: function( html ){
                $('.js-asset-manager-modal-innards').html( html );
                setAssetManagerVars();
                initAssetManager();
            }
        });
    });

    function setAssetManagerVars(){
        var $modal = $('.js-asset-manager-modal-innards');

        $directoryListing = $modal.find( '[data-directory-listing]' );
        $filesListing = $modal.find( '[data-files-listing]' );
        $deleteActionArea = $modal.find( '[data-delete-folder-action]' );
        $newFolderModal = $modal.find( '[data-new-folder-modal]' );
        $uploadModal = $modal.find( '[data-upload-modal]' );
        $dynamicModal = $modal.find( '#dynamic-asset-manager-modal' );
    }

    // Click on a directory list link
    $(document).on('click', '[data-directory-link]', function(evt){

        evt.preventDefault();
        evt.stopPropagation();

        $('[data-asset-manager-search-input]').val('');
        reloadDirectoriesAndFiles( $(this).data('directory-id') );

    });

    $(document).on('click', '.js-asset-manager-modal-opener', function(){

        // Clear any existing flash messages from the asset manager modal
        clearAssetManagerFlashMessages();

        $('[data-file-actions]').addClass('is-hidden');
        $('[data-asset-manager]').removeClass('multiple-selected');

        // Store which element opened the asset manager
        asset_manager_opened_by = $(this);
        asset_manager_opened_by_ID = asset_manager_opened_by.data('image-input-id');

        asset_manager_images_only = (typeof( asset_manager_opened_by.data('file-select') ) == 'undefined');

        asset_manager_multiple_select = asset_manager_opened_by.parents( '[data-asset-gallery]' ).length > 0;

        var directory_id = $(this).data('image-directory');
        if( typeof( directory_id ) != 'undefined' ){
            reloadDirectoriesAndFiles( directory_id );
        } else {
            reloadDirectoriesAndFiles();
        }

    });

    $(document).on('click', '[data-upload-file-button]', function( evt ){
        evt.preventDefault();

        // Open native file explorer
        $('[data-reveal]#upload-modal').find('[data-asset-upload-file-input]').click();

    });

    $(document).on('click', '[data-clear-file-select]', function( evt ){
        evt.preventDefault();
        evt.stopPropagation();

        var $file_select = $(this).parents('.file-select-field').first();

        $(this).addClass('none');

        $file_select.find('[data-file-select-hidden-input]').val( '' );
        $file_select.find('[data-file-select-file-preview]').val( '' );
    });

    /**
     * UPLOAD MODAL PRE-POPULATE NAME
     * Pre-populate the 'Name' field with a cleaned up version of the file name
     */
    $(document).on('change', '[data-asset-upload-file-input]', function( evt ){
        var $singleAssetFields = $('[data-reveal]#upload-modal [data-asset-upload-single-fields]');

        $('[data-reveal]#upload-modal .alert-box').hide();
        $('[data-reveal]#upload-modal .inline-error').remove();

        var files = this.files;
        if (!files) {
            // IE9 Fix
            // As IE9 only supports single image uploads, we just need to pass the single file into the array
            files = [];
            files.push({
                name: this.value.substring(this.value.lastIndexOf("\\")+1),
                size: 0,  // it's not possible to get file size without flash
                type: this.value.substring(this.value.lastIndexOf(".")+1)
            });
        }

        if( files.length > 1 ){

            if( !$singleAssetFields.hasClass("none") ){ $singleAssetFields.addClass("none"); }
            $('[data-asset-upload-name-input]').val('').removeAttr('required');

        } else {

            $singleAssetFields.removeClass("none");

            var fullPath = this.value;

            if (fullPath) {
                var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                var filename = fullPath.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                }

                var dotIndex = filename.lastIndexOf('.');
                filename = filename.substring(0, dotIndex);

                var path_replace_regex = /[^a-zA-Z0-9-]/g;
                var whitespace_replace_regex = /[\s]/g;
                var double_dash_replace_regex = /-[-]+/g;

                filename = filename.replace(whitespace_replace_regex, '-'); // Replace any white space with a dash
                filename = filename.replace(double_dash_replace_regex, '-'); // Replace any instances of 2 hyphens with 1 (----- to -)
                filename = filename.replace(path_replace_regex, ''); // Replace any invalid characters with nothing
                filename = filename.replace(/-+$/, ''); // Replace any trailing hyphens from the URL
                filename = filename.replace(/-/g, ' '); // Replace any dashes with a space

                // Now uc_words the filename
                filename = (filename + '').replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function ($1) {
                    return $1.toUpperCase()
                });

                $('[data-asset-upload-name-input]').val(filename);
            }

        }
    });

    $(document).on('click', '.js-select-modal-asset', function( evt ){
        evt.preventDefault();
        evt.stopPropagation();

        modal_asset_manager_select_file( $(this) );

    });

    $(document).on('click', '[data-select-multiple-assets-button]', function( evt ){
        evt.preventDefault();
        evt.stopPropagation();

        // Now we need to get all selected items, and in turn add gallery assets and assign the images
        var $galleryWrapper = asset_manager_opened_by.parents('[data-asset-gallery]').first();
        var $galleryAssetsList = $galleryWrapper.find('[data-asset-gallery-assets]');
        var $galleryAddBtn = $galleryWrapper.find('[data-add-gallery-asset]');

        var selected = getSelected();
        var number_of_existing_assets = $galleryAssetsList.find('[data-asset]').length;
        var number_of_new_assets = selected.length - 1;
        var new_assets = [];

        if( number_of_new_assets > 0 ){
            /*
             * Please forgive the ridiculousness of the below.
             * Due to the asset gallery rows loading via AJAX, we can't just add rows in a loop as the AJAX call
             * uses the number of rows as the 'index' to add against the new row.
             * So...
             * We need to wait for a custom trigger (when a row is loaded) before adding the next one
             */
            $galleryAssetsList.on( 'bspk-asset-gallery-asset-added', function( evt, new_row ){
                new_assets.push( $(new_row) );

                if( number_of_existing_assets + number_of_new_assets == $galleryAssetsList.find('[data-asset]').length ){
                    $galleryAssetsList.off( 'bspk-asset-gallery-asset-added' );
                } else {
                    $galleryAddBtn.trigger( 'click' );
                }
            });

            $galleryAddBtn.trigger( 'click' );
        }

        var asset_loading_interval = setInterval( function(){
            var all_new_assets_loaded = number_of_new_assets == new_assets.length;
            if( all_new_assets_loaded ){

                clearTimeout( asset_loading_interval );

                for( var i = 0; i < selected.length; i++ ){
                    var asset_id = selected[ i ];
                    var $assetLink = $('[data-asset-id="' + asset_id + '"] .js-select-modal-asset');

                    if( i == 0 ){ // If we're processing the first asset
                        // Simply select it
                        modal_asset_manager_select_file( $assetLink );
                    }
                    if( i > 0 ){ // If we're not processing the first selected item, we need to add the asset to the new gallery item
                        var $new_asset = new_assets[ i-1 ];

                        var opener = $new_asset.find( '.js-asset-manager-modal-opener' );

                        // So select it using the relevant 'opener'
                        modal_asset_manager_select_file( $assetLink, opener, opener.data('image-input-id') );
                    }
                }
            }
        }, 500 );
    });

    function modal_asset_manager_select_file( $link, opened_by, opened_by_id ){

        if( typeof( opened_by ) == 'undefined' ){ opened_by = asset_manager_opened_by; }
        if( typeof( opened_by_id ) == 'undefined' ){ opened_by_id = asset_manager_opened_by_ID; }

        var file_id = $link.data('file');
        var imgUrl = $link.data('path');

        var selected_file = $('.js-asset-manager-modal-opener[data-file-select="' + opened_by_id + '"]').length > 0;

        if( selected_file ){

            var $file_select_input = $('.js-asset-manager-modal-opener[data-file-select="' + opened_by_id + '"]');
            var $file_select = $file_select_input.parents('.file-select-field').first();

            $file_select.find('[data-file-select-hidden-input]').val( file_id );
            $file_select.find('[data-file-select-file-preview]').val( $link.data('file-path') );

            $file_select.find('[data-clear-file-select]').removeClass('none');

        } else {

            var $previewer = $('.js-asset-manager-modal-opener[data-image-input-id="' + opened_by_id + '"]');
            var $hidden_input = $('input[data-asset-input-id="' + opened_by_id + '"]');

            $hidden_input.val(file_id);

            if (typeof( $previewer.data('crop-dimensions') ) != "undefined") {

                // MULTIPLE IMAGE SIZES

                $.ajax({
                    url: '{{ route('adm.asset-manager.function', ['function' => 'select_file']) }}',
                    type: 'POST',
                    data: {
                        file_id: file_id,
                        reference: $previewer.data('reference'),
                        details: $previewer.data('details'),
                        column: $previewer.data('column'),
                        field_id: opened_by_id,
                        dimensions: $previewer.data('crop-dimensions'),
                        table: $previewer.data('table'),
                        item_id: $previewer.data('item-id'),
                        language_id: $previewer.data('language')
                    },
                    dataType: 'json',
                    beforeSend: function () {

                        // Set as loading
                        opened_by.addClass('is-loading');

                    },
                    success: function (json) {

                        if (json.success) {
                            $previewer.parent('.image-container').html(json.output);
                            $(document).foundation( 'reveal', 'init' );
                        } else {
                            alert(json.error);
                        }

                    },
                    complete: function () {

                        // Unset as loading
                        opened_by.removeClass('is-loading');

                        //$(document).foundation('reveal', 'reflow');

                    }
                });

            } else {

                // SINGLE IMAGE SIZES

                // Hide Select Image Placeholder Box
                $previewer.find('[data-image-select-placeholder]').addClass('none');

                // Show Preview of selected image
                var imagePreview = $previewer.find('[data-image-select-placeholder]').next('[data-image-preview-container]');

                // For unknown reasons, you cannot simply update the background image. Instead, we replace the element with the new bg image
                imagePreview.removeClass('none');
                imagePreview.find('[data-image-preview]').replaceWith('<div data-image-preview class="width-100 height-100 background-contain background-center" style="background-image: url(' + imgUrl + ');"></div>');

                // Show actions
                $previewer.parent().find('[data-image-actions]').removeClass('none');

            }

        } // END IF SELECTED_FILE

        // Close asset manager modal
        $('#asset-manager-modal').foundation('reveal', 'close');

        // Unlock scroll
        $('body').removeClass('scroll-lock');

    }

    $(document).on('submit', '#upload-modal form', function( evt ){

        evt.preventDefault();
        evt.stopPropagation();

        var $form = $(this);

        if ($form.data('submitted') === true) {
            // Previously submitted - don't submit again
            return false;
        } else {
            // Mark it so that the next submit can be ignored
            $form.data('submitted', true);
        }

        // Clear any existing flash messages from the asset manager modal
        clearAssetManagerFlashMessages();

        if( typeof( $form.data('validated') ) == 'undefined' || $form.data('validated') == false ){

            if( $form.find('.js-form-file-extension').length == 0 ) {
                var file_extension = $form.find('input[type="file"]').val().split('.')[1];
                $form.append('<input type="hidden" name="file_extension" value="' + file_extension + '" class="js-form-file-extension" />');
            }

            validateUpload( $form );
        } else {
            uploadFiles( $form );
        }
    });

    function validateUpload( $form ) {
        if (typeof( FormData ) == "undefined") {
            ie9ValidateUpload( $form );
        } else {
            var formData = new FormData($('#upload-modal form')[0]);
            $.ajax({
                url: '{{ route('adm.asset-manager.function', ['function' => 'validate_upload']) }}',
                type: "post",
                data: formData,
                dataType: "json",
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function(){
                    showModalLoadingOverlay( $('#upload-modal'), true );
                },
                success: function( json ){
                    $form.find('.js-modal-form-flash .alert-box').hide();
                    $form.find('.inline-error').remove();

                    if( json.error ){
                        showModalLoadingOverlay( $('#upload-modal'), false );
                        if( json.field_errors ){
                            for( var field in json.field_errors ){
                                $form.find('input[name="' + field + '"]').parent().append( '<span class="inline-error">' + json.field_errors[field][0] + '</span>' );
                            }
                        }

                        //$form.find('.js-modal-form-flash .alert').text( json.error ).show();
                        if (json.flash_notification) {
                            $('.admin-container .top-notification').first().remove();
                            $('.admin-container').prepend(json.flash_notification);
                            $('.admin-container .top-notification').first().css('zIndex', 1005);
                        }

                    } else if( json.success ){
                        $form.data('submitted', false);
                        $form.data('validated', 'true');
                        $form.submit();
                    }
                },
                error: function(){
                    showModalLoadingOverlay( $('#upload-modal'), false );
                    $form.find('.js-modal-form-flash .alert').text( 'An unknown error occurred, please try again' ).hide();
                },
                complete: function() {
                    $form.data('submitted', false);
                }
            });
        }
    }
    function ie9ValidateUpload( $form ){
        $form.append('<input type="hidden" name="is_ie9" value="1" />');
        $form.ajaxSubmit({
            url: '{{ route('adm.asset-manager.function', ['function' => 'validate_upload']) }}',
            type: "post",
            dataType: 'json',
            beforeSubmit: function () {
                showModalLoadingOverlay($('#upload-modal'), true);
            },
            success: function( json, statusText, xhr, $form ){
                $form.find('.js-modal-form-flash .alert-box').hide();
                $form.find('.inline-error').remove();

                if( json.error ){
                    showModalLoadingOverlay( $('#upload-modal'), false );
                    if( json.field_errors ){
                        for( var field in json.field_errors ){
                            $form.find('input[name="' + field + '"]').parent().append( '<span class="inline-error">' + json.field_errors[field][0] + '</span>' );
                        }
                    }

                    //$form.find('.js-modal-form-flash .alert').text( json.error ).show();
                    if (json.flash_notification) {
                        $('.admin-container .top-notification').first().remove();
                        $('.admin-container').prepend(json.flash_notification);
                        $('.admin-container .top-notification').first().css('zIndex', 1005);
                    }

                } else if( json.success ){
                    $form.data('submitted', false);
                    $form.data('validated', 'true');
                    $form.submit();
                }
            },
            error: function () {
                showModalLoadingOverlay( $('#upload-modal'), false );
                $form.find('.js-modal-form-flash .alert').text( 'An unknown error occurred, please try again' ).hide();
            }
        });
    }

    function uploadFiles( $form ){
        if( typeof( FormData ) == "undefined" ){
            ie9UploadFiles( $form );
        } else {
            var formData = new FormData( $('#upload-modal form')[0] );
            $.ajax({
                url: '{{ route('adm.asset-manager.function', ['function' => 'upload']) }}',
                type: "post",
                data: formData,
                dataType: false,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function(){
                    showModalLoadingOverlay( $('#upload-modal'), true );
                },
                success: function( json ){
                    $form.find('.js-modal-form-flash .alert-box').hide();
                    $form.find('.inline-error').remove();

                    if( json.error ){
                        showModalLoadingOverlay( $('#upload-modal'), false );
                        if( json.field_errors ){
                            for( var field in json.field_errors ){
                                $form.find('input[name="' + field + '"]').parent().append( '<span class="inline-error">' + json.field_errors[field][0] + '</span>' );
                            }
                        }

                        if (json.flash_notification) {
                            $('.admin-container .top-notification').first().remove();
                            $('.admin-container').prepend(json.flash_notification);
                            $('.admin-container .top-notification').first().css('zIndex', 1005);
                        }
                    }
                    else if( json.success ){

                        if (json.flash_notification) {
                            $('.admin-container .top-notification').first().remove();
                            $('.admin-container').prepend(json.flash_notification);
                            $('.admin-container .top-notification').first().css('zIndex', 1005);
                        }

                        $form.data('submitted', false);
                        $form.data('validated', false);
                        $('#upload-modal').foundation('reveal', 'close');
                        $('#upload-modal').remove();
//                            reloadDirectoriesAndFiles( $form.find('input[name="directory_id"]').val() );
                        reloadDirectoriesAndFiles();
                    }
                },
                error: function(){
                    showModalLoadingOverlay( $('#upload-modal'), false );
                    $form.find('.js-modal-form-flash .alert').text( 'An unknown error occurred, please try again' ).hide();
                },
                complete: function () {
                    $form.data('submitted', false);
                }
            }, 'json');
        }
    }
    function ie9UploadFiles( $form ){
        $form.ajaxSubmit({
            url: '{{ route('adm.asset-manager.function', ['function' => 'upload']) }}',
            type: "post",
            dataType: 'json',
            beforeSend: function(){
                showModalLoadingOverlay( $('#upload-modal'), true );
            },
            success: function( json ){
                $form.find('.js-modal-form-flash .alert-box').hide();
                $form.find('.inline-error').remove();

                if( json.error ){
                    showModalLoadingOverlay( $('#upload-modal'), false );
                    if( json.field_errors ){
                        for( var field in json.field_errors ){
                            $form.find('input[name="' + field + '"]').parent().append( '<span class="inline-error">' + json.field_errors[field][0] + '</span>' );
                        }
                    }

                    if (json.flash_notification) {
                        $('.admin-container .top-notification').first().remove();
                        $('.admin-container').prepend(json.flash_notification);
                        $('.admin-container .top-notification').first().css('zIndex', 1005);
                    }
                }
                else if( json.success ){

                    if (json.flash_notification) {
                        $('.admin-container .top-notification').first().remove();
                        $('.admin-container').prepend(json.flash_notification);
                        $('.admin-container .top-notification').first().css('zIndex', 1005);
                    }

                    $form.data('submitted', false);
                    $form.data('validated', false);
                    $('#upload-modal').foundation('reveal', 'close');
                    $('#upload-modal').remove();
//                            reloadDirectoriesAndFiles( $form.find('input[name="directory_id"]').val() );
                    reloadDirectoriesAndFiles();
                }
            },
            error: function(){
                showModalLoadingOverlay( $('#upload-modal'), false );
                $form.find('.js-modal-form-flash .alert').text( 'An unknown error occurred, please try again' ).hide();
            }
        }, 'json');
    }


    $(document).on('submit', '#folder-modal form', function( evt ){

        evt.preventDefault();
        evt.stopPropagation();

        var $form = $(this);

        if ($form.data('submitted') === true) {
            // Previously submitted - don't submit again
            return false;
        } else {
            // Mark it so that the next submit can be ignored
            $form.data('submitted', true);
        }

        // Clear any existing flash messages from the asset manager modal
        clearAssetManagerFlashMessages();

        $(document).foundation('reveal', 'reflow');

        if( typeof( $form.data('validated') ) == 'undefined' ){

            $.ajax({
                url: '{{ route('adm.asset-manager.function', ['function' => 'validate_new_folder']) }}',
                type: "post",
                data: $form.serialize(),
                dataType: "json",
                beforeSend: function() {
                    showModalLoadingOverlay($('#folder-modal'), true);
                },
                success: function( json ){
                    $form.find('.js-modal-form-flash .alert-box').hide();
                    $form.find('.inline-error').remove();

                    if( json.error ){
                        if( json.field_errors ){
                            for( var field in json.field_errors ){
                                $form.find('input[name="' + field + '"]').parent().append( '<span class="inline-error">' + json.field_errors[field][0] + '</span>' );
                            }
                        }

                        if (json.flash_notification) {
                            $('.admin-container .top-notification').first().remove();
                            $('.admin-container').prepend(json.flash_notification);
                            $('.admin-container .top-notification').first().css('zIndex', 1005);
                        }
                    }
                    else if( json.success ){
                        $form.data('submitted', false);
                        $form.data('validated', 'true');
                        $form.submit();
                    }
                },
                complete: function() {
                    showModalLoadingOverlay($('#folder-modal'), false);
                    $form.data('submitted', false);
                }
            });

        } else {

            $.ajax({
                url: '{{ route('adm.asset-manager.function', ['function' => 'new_folder']) }}',
                type: "post",
                data: $form.serialize(),
                dataType: "json",
                beforeSend: function() {
                    showModalLoadingOverlay($('#folder-modal'), true);
                },
                success: function( json ){
                    $form.find('.js-modal-form-flash .alert-box').hide();
                    $form.find('.inline-error').remove();

                    if( json.error ){
                        if( json.field_errors ){
                            for( var field in json.field_errors ){
                                $form.find('input[name="' + field + '"]').parent().append( '<span class="inline-error">' + json.field_errors[field][0] + '</span>' );
                            }
                        }

                        if (json.flash_notification) {
                            $('.admin-container .top-notification').first().remove();
                            $('.admin-container').prepend(json.flash_notification);
                            $('.admin-container .top-notification').first().css('zIndex', 1005);
                        }
                    } else {

                        if ($assetManagerModal.length && json.success) {
                            if (json.flash_notification) {
                                $('.admin-container .top-notification').first().remove();
                                $('.admin-container').prepend(json.flash_notification);
                                $('.admin-container .top-notification').first().css('zIndex', 1005);
                            }

                            $('#folder-modal').foundation('reveal', 'close');

                            // make sure the newly added alert prompts can be removed
                            $(document).foundation('alert', 'reflow');

                            // Add notifications to the asset manager modal
                            //$assetManagerModal.find('.js-modal-form-flash .success').text( json.success ).show();
                        }

                        if( typeof( json.new_directory ) != 'undefined' ){
                            reloadDirectoriesAndFiles( json.new_directory );
                        } else {
                            reloadDirectories();
                        }
                    }
                },
                complete: function(){
                    showModalLoadingOverlay($('#folder-modal'), false);
                    $form.data('submitted', false);
                }
            });

        }
    });

    $(document).on('click', '.js-delete-folder-button', function (evt) {

        evt.preventDefault();

        var directory_id = $(this).data('folder-id');

        $('#confirm-delete-folder-modal').foundation('reveal', 'open', {
            url: '{{ route('adm.asset-manager.function', ['delete_folder']) }}',
            data: {
                directory_id: directory_id,
                get_form: true
            },
            method: 'post'
        });
    });

    $(document).on('submit', '.js-modal-delete-folder-form', function( evt ){

        evt.preventDefault();
        evt.stopPropagation();

        var $form = $(this);
        var $form_modal = $form.parents('.reveal-modal').first();

        if ($form.data('submitted') === true) {
            // Previously submitted - don't submit again
            return false;
        } else {
            // Mark it so that the next submit can be ignored
            $form.data('submitted', true);
        }

        $.ajax({
            url: $form.attr('action'),
            type: "post",
            data: $form.serialize(),
            dataType: "json",
            beforeSend: function(){
                $form.find('.js-modal-form-flash .alert-box').text('').hide();
                showModalLoadingOverlay($form_modal, true);
            },
            success: function( json ){

                if ( json.files_in_use ) {
                    console.log('files_in_use');
                    // If files within this folder are in use, warn the user in a separate modal

                    var content = '';
                    content += '<p class="font-size-14">Assets inside this folder are being used on the site.<br />Deleting this folder may <strong>break images or links to files</strong> on the website.</p>';
                    content += '<span class="block">Assets being used: </span>';
                    content += '<ul class="font-weight-600">';
                    $(json.files_in_use).each(function (ind, val) {
                        content += '<li>' + val + '</li>';
                    });
                    content += '</ul>';
                    content += '<p class="font-size-14">Are you sure you wish to delete the folder? <strong>This cannot be undone</strong>.</p>';

                    var $filesInUseModal = getDynamicModal({
                        title: 'Asset in Use',
                        content: content,
                        confirmButton: 'Delete',
                        closeButton: 'Cancel'
                    });
                    $('body').append($filesInUseModal);
                    $filesInUseModal.foundation('reveal', 'open');

                    $(document).on('closed.fndtn.reveal', $filesInUseModal, function () {
                        $form_modal.foundation('reveal', 'close');
                    });

                    $filesInUseModal.find('[data-confirm-button]').on('click', function() {
                        $form.data('submitted', false);

                        // The next time the form is submitted, don't check for file usage - just delete files
                        $form.find('[name="check_file_usage"]').remove();

                        $form.submit();

                        $filesInUseModal.foundation('reveal', 'close');
                    });
                }

                if( json.success ){
                    // Add notifications to the asset manager modal
                    //$assetManagerModal.find('.js-modal-form-flash .success').text( json.success ).show();
                    if (json.flash_notification) {
                        $('.admin-container .top-notification').first().remove();
                        $('.admin-container').prepend(json.flash_notification);
                        $('.admin-container .top-notification').first().css('zIndex', 1005);
                    }

                    $form_modal.foundation('reveal', 'close');
                    $form_modal.remove();
                    reloadDirectoriesAndFiles( json.parent );
                }

                if( json.error ){
                    //$form.find('.js-modal-form-flash .alert').text( json.error ).show();
                    if (json.flash_notification) {
                        $('.admin-container .top-notification').first().remove();
                        $('.admin-container').prepend(json.flash_notification);
                        $('.admin-container .top-notification').first().css('zIndex', 1005);
                    }
                }
            },
            complete: function(){
                $form.data('submitted', false);
                showModalLoadingOverlay($form_modal, false);
            }
        }, 'json');

    });

    $(document).on('click', '[data-asset-row]', function( evt ){
        var $clickTarget = $(evt.target);
        if( $clickTarget.parents('[data-asset-checkbox]' ).length == 0 ){

            evt.preventDefault();

            // Click select asset button
            $(this).find('.js-select-modal-asset').click();

        }
    });

    // Show/hide the action buttons / select when selecting items
    var selectedItems;
    $('body').on('change', '[name="selected-item"]', function() {

        // Count selected items
        selectedItems = $('[name="selected-item"]:checked').length;
        $('[data-selected-items-count]').html(selectedItems);

        // Show/Hide actions if items are selected
        if ( selectedItems > 0 ) {
            $('[data-file-actions]').removeClass('is-hidden');
        } else {
            $('[data-file-actions]').addClass('is-hidden');
        }

        // Add class if multiple items are selected
        if ( selectedItems > 1 ) {
            $('[data-asset-manager]').addClass('multiple-selected');
        } else {
            $('[data-asset-manager]').removeClass('multiple-selected');
        }

        // Toggle highlight on parent row
        if ($(this).prop('checked') == true){
            $(this).parents('tr').addClass('selected-table-row').attr('data-selected', '');
        } else {
            $(this).parents('tr').removeClass('selected-table-row').removeAttr('data-selected');
        }

    });


    /**
     * "MOVE TO..." SELECT BOX
     * Move selected options to chosen option is select
     */
    $(document).on('change', 'select[data-move-select]', function() {

        var destinationFolder = $("select[data-move-select] option:selected");

        var destinationFolderID = destinationFolder.val();

        var destinationFolderPath = destinationFolder.data('directory-path');

        var $confirmMoveAssetsModal = getDynamicModal({
            title: 'Move selected assets to...',
            content: '<p class="font-size-14">Are you sure you wish to move the selected files into "<strong>/' + destinationFolderPath + '</strong>"</p>',
            confirmButton: 'Move Assets',
            closeButton: 'Cancel'
        });
        $('body').append( $confirmMoveAssetsModal );
        $confirmMoveAssetsModal.foundation('reveal', 'open');

        $confirmMoveAssetsModal.find('[data-confirm-button]').on('click', function() {

            move_files(getSelected(), destinationFolderID);
            $confirmMoveAssetsModal.foundation('reveal', 'close');
        });

    });


    /**
     * GET SELECTED ASSETS
     * Get all selected assets on screen
     */
    function getSelected(){

        var selectedAssets = [];

        // Push all selected items
        $("[data-asset-manager] tr[data-selected]").each(function() {

            // Push each selected id into an array
            var assetID = $(this).data('asset-id');
            selectedAssets.push(assetID);

        });

        return selectedAssets;

    }


    /**
     * UPDATED SELECTED ASSETS (STYLE)
     * Force trigger change on selected assets (e.g. checked before js loads)
     */
    function triggerChangeSelected(){

        $('[name="selected-item"]').trigger('change');

    }

    $(window).load(function() {

        // Trigger change on all selected table rows
        triggerChangeSelected();

    });

    /**
     * Remove duplicates in an array
     * Source: http://stackoverflow.com/a/12551709
     */
    function removeDuplicates(array) {
        var result = [];
        $.each(array, function(i, e) {
            if ($.inArray(e, result) == -1) result.push(e);
        });
        return result;
    }


    /**
     * DRAGGABLE ASSETS
     * Allow asset table rows to be dragged
     * Source: http://api.jqueryui.com/draggable/
     */
    var isDraggingAsset,
            draggingAsset,
            draggingAssetID;

    var draggableAssetsOptions = {
        helper: "clone",    // Create copy to style when dragging
        revert: true,       // Animate back to original position if not dropped
        cancel: "input,.button,.list-table-checkbox-col",
        cursorAt: {
            left: -10,
            top: -10
        },
        start: function(event, ui) {
            //console.log('dragging asset');

            // Set dragging state
            isDraggingAsset = true;

            // Store dragged asset
            draggingAsset = $(this);

            // Enable revert (if previously disabled by drop)
            $(this).draggable("option", "revert", true);

            // Apply dragged from state
            $(this).addClass("ui-dragging-from");

            // MULTIPLE ITEMS SELECTED
            if (selectedItems > 1){

                // Get all selected item id's
                draggingAssetID = [];

                // Push dragged item
                draggingAssetID.push($(this).data('asset-id'));

                // Push all selected items
                $("[data-asset-manager] tr[data-selected]").each(function() {

                    // Push each selected id into an array
                    var assetID = $(this).data('asset-id');
                    draggingAssetID.push(assetID);

                    // Fade each item selected
                    $(this).addClass("ui-dragging-from");

                });

                // Remove duplicates (cloned drag element, dragging an already selected item)
                draggingAssetID = removeDuplicates(draggingAssetID);

                // ONE ITEM SELECTED
            } else {

                // Get currently dragging item
                draggingAssetID = draggingAsset.data('asset-id');

            }

        },
        stop: function(event, ui) {
            //console.log('stop dragging asset');

            // Set dragging state
            isDraggingAsset = false;

            // Reset dragged-from classes (faded out)
            $('[data-asset-manager] tr.ui-draggable').removeClass("ui-dragging-from");

        }
    };


    /**
     * DRAGGABLE FOLDERS
     * Allow folders to be dragged and re-ordered
     * Source: http://api.jqueryui.com/draggable/
     */
    var isDraggingFolder,
            draggingFolder,
            draggingFolderID;

    var draggableFoldersOptions = {
        helper: "clone",    // Create copy to style when dragging
        revert: true,       // Animate back to original position if not dropped
        cancel: "",         // Cannot drag from these areas
        cursorAt: {
            left: -10,
            top: -10
        },
        start: function(event, ui) {
            //console.log('dragging folder');

            // Set dragging folder state
            isDraggingFolder = true;

            // Store dragging item
            draggingFolder = $(this);

            // Enable revert (if previously disabled by drop)
            $(this).draggable("option", "revert", true);

            // Apply dragged from state
            $(this).addClass("ui-dragging-from");

            // Get currently dragging item
            draggingFolderID = draggingFolder.data('folder-id');

        },
        stop: function(event, ui) {
            //console.log('stop dragging folder');

            // Set dragging state
            isDraggingFolder = false;

            // Reset dragged-from classes (faded out)
            $('[data-draggable-folder]').removeClass("ui-dragging-from");

        }
    };


    /**
     * DROPPABLE FOLDERS
     * Allow asset table rows to be dropped onto folders in the sidebar
     * Source: http://api.jqueryui.com/droppable/
     */

    var droppableFoldersOptions = {
        accept: "[data-draggable-asset], [data-draggable-folder]",
        greedy: true,
        tolerance: 'pointer',
        drop: function(event, ui) {

            // Get dropped folder ID
            var dropFolderID = $(this).data('folder-id');


            // If dragging folder
            if (isDraggingFolder) {

                // Cancel revert of dragging item if dropped successfully
                draggingFolder.draggable("option", "revert", false);

                //console.log('Dropped folder ' + draggingFolderID + ' onto ' + dropFolderID);

                // Move folder
                move_folders(draggingFolderID, dropFolderID);

                // If dragging asset
            } else if (isDraggingAsset){

                // Cancel revert of dragging item if dropped successfully
                draggingAsset.draggable("option", "revert", false);

                //console.log('Dropped asset ' + draggingAssetID + ' onto ' + dropFolderID);

                // Move asset
                move_files(draggingAssetID, dropFolderID);

            }


        }
    };


    /**
     * INIT ASSET MANAGER
     * Custom Right Click Content Menu
     */

    function refreshFolderList(){

        // Init Folder List (Draggable/Droppable)
        var draggableFolders = $("[data-draggable-folder]").draggable(draggableFoldersOptions);
        var droppableFolders = $("[data-droppable-folder]:not(.current)").droppable(droppableFoldersOptions);

    }

    function refreshAssetList(){

        // Init Asset List (Draggable)
        var draggableAssets = $("[data-draggable-asset]").draggable(draggableAssetsOptions);

    }

    function initAssetManager(){

        refreshAssetList();
        refreshFolderList();

    }

    initAssetManager();

    // Call this function to move a set of specified file IDs into a given directory
    function move_files( file_ids, directory_id ){
        $.ajax({
            url: '{{ route('adm.asset-manager.function', ['function' => 'move_files']) }}',
            type: 'POST',
            data: {
                file_ids: file_ids,
                directory_id: directory_id
            },
            dataType: 'json',
            beforeSend: function(){

                showPageLoadingOverlay(true);

            },
            success: function( json ){

                showPageLoadingOverlay(false);

                if( json.success ){
                    var title = 'Assets moved';
                    var content = '';

                    if( json.files_moved > 0 ){
                        content = '<p>' + json.success + '</p>';
                    } else {
                        title = 'Assets couldn\'t be moved.';
                    }

                    if( json.files_not_moved && json.files_not_moved.length > 0 ){
                        content += '<p>1 or more assets couldn\'t be moved because an asset with the same name exists in the folder you tried to move them to.</p>';
                        content += '<span class="block">The assets are: </span>';
                        content += '<ul class="font-weight-600">';
                        $(json.files_not_moved).each( function(ind, val){
                            content += '<li>' + val + '</li>';
                        });
                        content += '</ul>';
                    }

                    var $modal = getDynamicModal({
                        title: title,
                        content: content
                    });
                    $('body').append( $modal );
                    $modal.foundation('reveal', 'open');

                    if( json.files_moved > 0 ){
                        $modal.find('[data-close-reveal]').off('click').click( function(){
                            reloadDirectoriesAndFiles(directory_id);
                        });
                    }

                } else {
                    var $modal = getDynamicModal({
                        title: 'Assets couldn\'t be moved.',
                        content: json.error
                    });
                    $('body').append($modal);
                    $modal.foundation('reveal', 'open');
                }

            }
        });
    }

    // Call this function to move a set of specified folder IDs into a given parent directory
    function move_folders( folder_ids, parent_id ){
        $.ajax({
            url: '{{ route('adm.asset-manager.function', ['function' => 'move_folders']) }}',
            type: 'POST',
            data: {
                directory_ids: folder_ids,
                parent: parent_id
            },
            dataType: 'json',
            beforeSend: function(){

                showPageLoadingOverlay(true);

            },
            success: function( json ){

                showPageLoadingOverlay(false);

                var title = '';
                var content = '';

                if( json.success ){

                    if( json.folders_not_moved && json.folders_not_moved.length > 0 ){
                        title = 'Sorry, that folder could not be moved.';

                        content += '<p>The folder "';
                        $(json.folders_not_moved).each( function(ind, val){
                            content += val;
                        });
                        content += '" already exists in that directory.</p>';

                        var $modal = getDynamicModal({
                            title: title,
                            content: content
                        });
                        $('body').append($modal);
                        $modal.foundation('reveal', 'open');

                        if( json.folders_moved > 0 ){
                            $modal.find('[data-close-reveal]').off('click').click( function(){
                                reloadDirectoriesAndFiles(parent_id);
                            });
                        }

                    } else {
                        reloadDirectoriesAndFiles(parent_id);
                    }

                } else {

                    // Textual error message
                    var $modal = getDynamicModal({
                        title: 'Sorry, that folder could not be moved.',
                        content: json.error
                    });
                    $('body').append($modal);
                    $modal.foundation('reveal', 'open');

                }

            }
        });
    }

    // Reload the directory listing, passing a target element to change the DOM of
    function reloadDirectories( directory_id ){
        var data = { modal: true };
        if( typeof( directory_id ) != 'undefined' ){ data.directory_id = directory_id; }

        $.ajax({
            url: '{{ route('adm.asset-manager.function', ['load_directory_list']) }}',
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function(){
                showPageLoadingOverlay( true );
            },
            success: reloadResponseHandler,
            complete: function(){
                isSiteAssetsCurrent( directory_id );
                showPageLoadingOverlay();
            }
        });
    }

    // Reload the files listing, passing a target element to change the DOM of
    function reloadFiles( directory_id ){
        var data = { modal: true, images_only: asset_manager_images_only, multiple_select: asset_manager_multiple_select };
        if( typeof( directory_id ) != 'undefined' ){ data.directory_id = directory_id; }

        $.ajax({
            url: '{{ route('adm.asset-manager.function', ['load_files_list']) }}',
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function(){
                showPageLoadingOverlay( true );
            },
            success: reloadResponseHandler,
            complete: function(){
                showPageLoadingOverlay();
            }
        });
    }

    // Reload both the directory listing and the files listing, passing a target element for both
    function reloadDirectoriesAndFiles( directory_id ){
        var data = { modal: true, images_only: asset_manager_images_only, multiple_select: asset_manager_multiple_select };
        if( typeof( directory_id ) != 'undefined' ){ data.directory_id = directory_id; }

        $.ajax({
            url: '{{ route('adm.asset-manager.function', ['load_directory_and_files_list']) }}',
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function(){
                showPageLoadingOverlay( true );
            },
            success: reloadResponseHandler,
            complete: function(){
                isSiteAssetsCurrent( directory_id );
                showPageLoadingOverlay();
            }
        });
    }

    // Common handler function to perform various DOM updated based on AJAX responses
    function reloadResponseHandler( json ){
        // If we have a new directory listing
        if( json.directory_list && typeof( $directoryListing ) != 'undefined' ){
            $directoryListing.html( json.directory_list );
        }
        // If we have a new files listing
        if( json.files_list && typeof( $filesListing ) != 'undefined' ){
            $filesListing.html( json.files_list );
        }
        // If we have a new delete folder button
        if( json.delete_action && typeof( $deleteActionArea ) != 'undefined' ){
            $deleteActionArea.html( json.delete_action );
        } else if( typeof( $deleteActionArea ) != 'undefined' ) {
            $deleteActionArea.html('');
        }
        // If we have a new new folder modal
        if( json.new_folder && typeof( $newFolderModal ) != 'undefined' ){
            $newFolderModal.html( json.new_folder );
        }
        // If we have a new upload modal
        if( json.upload && typeof( $uploadModal ) != 'undefined' ){
            $uploadModal.html( json.upload );
        }

        initAssetManager();

        clear_redundant_modals();
    }

    // Toggle whether the last clicked directory is 'Site Assets' and add the 'current' class if so
    function isSiteAssetsCurrent( directory_id ){
        if( directory_id == 0 ){
            $('[data-root-directory]').addClass('current');
        } else {
            $('[data-root-directory]').removeClass('current');
        }
    }

    // Show a clone of the dynamic modal, with a given title, content and buttons
    function getDynamicModal( options ){
        var $thisModal = $dynamicModal.clone();

        $thisModal.find('[data-dynamic-modal-title]').text( options.title );
        $thisModal.find('[data-dynamic-modal-content]').html( options.content );

        if (options.confirmButton) {
            $thisModal.find('[data-confirm-button]').text( options.confirmButton );
        } else {
            $thisModal.find('[data-confirm-button]').remove();
        }

        if (options.closeButton) {
            $thisModal.find('[data-close-reveal]').text( options.closeButton );
        }

        $(document).on('closed.fndtn.reveal', $thisModal, function () {
            $(this).remove();
        });

        return $thisModal;
    }

    // Clear any existing flash messages from the asset manager modal
    function clearAssetManagerFlashMessages() {
        $assetManagerModal.find('.js-modal-form-flash .alert-box').text('').hide();
    }

    $(document).on('click', '[data-asset-manager-search-btn]', assetManagerSearch);
    $(document).on('keyup', '[data-asset-manager-search-input]', function(evt){
        if( evt.which == 13 ){
            assetManagerSearch();
        }
    });

    function assetManagerSearch(){
        var searchTerm = $('[data-asset-manager-search-input]').val();
        if( searchTerm.length > 0 ){
            data = {
                modal: true,
                images_only: asset_manager_images_only,
                multiple_select: asset_manager_multiple_select,
                search: searchTerm
            };

            $.ajax({
                url: '{{ route('adm.asset-manager.function', ['search']) }}',
                type: 'POST',
                data: data,
                dataType: 'json',
                beforeSend: function(){
                    showPageLoadingOverlay( true );
                },
                success: reloadResponseHandler,
                complete: function(){
                    $('[data-directory-listing] li').removeClass('current');
                    isSiteAssetsCurrent(0);
                    showPageLoadingOverlay();
                }
            });
        }
    }

    // Open first tab with validation errors on load (if found)
    function goToValidationTab(){

        // If an error is found in another tab
        if($('[data-content-tabs] li.has-validation-error').length){

            // Find the first tab with an error
            var toClick = $('[data-content-tabs]').find('.has-validation-error').first();

            // Simulate a click on its link
            toClick.find('[data-tab-id]').click();

        }

    }

    $(document).ready(function(){
        goToValidationTab();
    });

    $(document).on( 'closed.fndtn.reveal', '[data-reveal]', clear_redundant_modals);

    function clear_redundant_modals(){
        var used_modals = new Array();
        $('[data-reveal]').each( function(){
            if( used_modals.indexOf( $(this).attr('id') ) == -1 ){
                used_modals.push( $(this).attr('id') );
            } else {
                console.log( 'Removing ' + $(this).attr('id') );
                $(this).remove();
            }
        })
    }

</script>

<?php // Drag & Drop ?>
<script src="/admin/dist/assets/js/vendor/dropzone.min.js"></script>
<script>
Dropzone.autoDiscover = false;
$(document).ready( function(){
    var drop_error = false;
    var dropzone_overlay = $('[data-dropzone-overlay]');
    var dropzone_text = $('[data-dropzone-text]');
    var dropzone_error = $('[data-dropzone-error]');
    var dropzone_close = $('[data-dropzone-close]');

    var modal_dropzone = false;
    $(document).on( 'open.fndtn.reveal', '[data-reveal]', function(){
        if( modal_dropzone === false ){
            modal_dropzone = new Dropzone(
                "#body",
                {
                    url: '/adm/asset-manager/upload',
                    paramName: 'asset',
                    maxFilesize: 2, // max 2mb
                    parallelUploads: 5,
                    uploadMultiple: true,
                    clickable: false,
                    previewsContainer: '.previews-container',
                    init: function() {

                        // Send form data along with the files
                        this.on("sendingmultiple", function(data, xhr, formData) {

                            var current_dir = $('#upload-modal input[name="directory_id"]').val();

                            formData.append("directory_id", current_dir);
                            formData.append("drag_and_drop", true);

                        });

                    }
                }
            );

            // Show screen overlay on drag
            modal_dropzone.on( "dragenter", function ( e ){
                // Make sure we're dragging a file (not selecting on the page)
                var dt = ( e && e.dataTransfer );
                var isFile = ( dt.types && (dt.types.indexOf ? dt.types.indexOf('Files') != -1 : dt.types.contains('Files')) );

                if ( isFile ) {
                    dropzone_overlay.addClass('show');
                }
            });

            // Hide overlay when dragging out
            modal_dropzone.on( "dragleave", function( e ){
                // If outside the screen
                if ( e && ( e.pageX <= 0 || e.pageY <= 0) ) {
                    dropzone_overlay.removeClass('show');
                }
            });

            // Handle drop
            modal_dropzone.on( "drop", function(){
                // Empty container
                $('.previews-container').html('');

                // Reset states
                resetDropzoneContent();
                dropzone_text.hide();
            });

            // Handle errors
            modal_dropzone.on( "error", function( file, response ){
                console.log('Error: ' + response);

                drop_error = true;
                dropzone_error.show();
            });

            // Handle complete
            modal_dropzone.on( "queuecomplete", function(){
                // Refresh file list
                reloadFiles( $('#upload-modal input[name="directory_id"]').val() );

                if (!drop_error){

                    // Hide overlay
                    setTimeout(function(){
                        hideDropzoneOverlay();
                    }, 1500);

                } else {

                    dropzone_close.show();

                }
            });
        } else {
            modal_dropzone.enable();
        }
    });

    $(document).on( 'closed.fndtn.reveal', '[data-reveal]', function(){
        if( modal_dropzone !== false ){
            modal_dropzone.disable();
        }
    });

    $(document).on('click', '[data-dropzone-close]', function(e){
        e.preventDefault();
        hideDropzoneOverlay();
    });

    function resetDropzoneContent(){

        // Reset content
        drop_error = false;
        dropzone_error.hide();
        dropzone_close.hide();
        dropzone_text.show();

        // Reset previews
        $('.previews-container').html('');

    }

    function hideDropzoneOverlay(){
        dropzone_overlay.fadeOut(500, function(){

            // Reset overlay
            dropzone_overlay.removeClass('show');
            resetDropzoneContent();

        });
    }
});
</script>