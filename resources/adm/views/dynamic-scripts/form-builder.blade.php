<script type="text/javascript">
// FORM BUILDER

// Return a helper with preserved width of cells
var formFixHelper = function(e, ui) {
    ui.children().each(function() {
        $(this).width($(this).width());
    });
    return ui;
};

// Init Sortable
function initialiseFormBuilder(targetFormBuilder) {
    $(targetFormBuilder).sortable({
        helper: formFixHelper,
        cursor: 'grabbing',
        axis: 'y',
        handle: '[data-form-drag-handle]',
        items: '[data-form-row]',
        placeholder: 'sortable-placeholder',
        tolerance: 'pointer',  // pointer, intersect
        revert: 150,
        opacity: 1,
        containment: "parent",
        forcePlaceholderSize: true,
        create: function() {
            var $parentTable = $(this).parents('[data-form-builder-wrapper]').first();
            $parentTable.find('.table').removeClass('overflow-hidden'); // Correct issues with items which need to overflow the table e.g. http://screencast.com/t/OCDWdPO88
        },
        start: function(){
            var $parentTable = $(this).parents('[data-form-builder-wrapper]').first();
            $parentTable.find('.table').addClass('overflow-hidden'); // Correct issues with items which need to overflow the table e.g. http://screencast.com/t/OCDWdPO88
        },
        stop: function(){
            var $parentTable = $(this).parents('[data-form-builder-wrapper]').first();
            $parentTable.find('.table').removeClass('overflow-hidden'); // Correct issues with items which need to overflow the table e.g. http://screencast.com/t/OCDWdPO88
        },
        update: function(){
            var $parentTable = $(this).parents('[data-form-builder-wrapper]').first();
            setFormBuilderRowIndexes($parentTable);
        }
    });
}

$(document).ready( function(){
    $('[data-form-builder]').each( function(){
        if( $(this).find('[data-form-row]').length > 1 ){
            initialiseFormBuilder($(this));
        }
    });
});

// Add a new row
$('body').on('click', '[data-form-builder-wrapper] .js-add-row', function(e) {
    e.preventDefault();
    e.stopPropagation();

    var $parentTable = $(this).parents('[data-form-builder-wrapper]').first();

    var postData = {
        column: $parentTable.find('[data-form-builder]').data('column'),
        row_index: $parentTable.find('[data-form-row]').length,
    };

    $.ajax({
        url: '{{ route('adm.ajax', ['ajax_add_form_row']) }}',
        type: 'POST',
        dataType: 'json',
        data: postData,
        beforeSend: function(){
            showPageLoadingOverlay(true);
        },
        success: function(data){
            if(data.rows){
                $parentTable.find('[data-form-builder]').append(data.rows);
            }
        },
        complete: function(){
            setFormBuilderRowIndexes($parentTable);

            if ( $parentTable.find('[data-form-row]').length > 1 ) {
                initialiseFormBuilder($parentTable.find('[data-form-builder]'));
            }

            showPageLoadingOverlay(false);
        }
    });
});


// Delete row
$('body').on('click', '[data-form-builder] .js-delete-row', function(e) {
    e.preventDefault();
    e.stopPropagation();

    var $parentTable = $(this).parents('[data-form-builder-wrapper]').first();

    // Remove the row
    $(this).parents('[data-form-row]').first().remove();

    setFormBuilderRowIndexes($parentTable);

    if ( $(this).parents('[data-form-row]').length < 2 ) {
        $parentTable.find('[data-form-builder]').sortable('destroy');
    }
});

// Checks if the field type requires options and disables the field if it doesn't
$('body' ).on('change', '[data-form-builder] .table-row select', function(e){
    var option_fields = new Array();

    @if( isset( $columns['main']['structure'] ) )
        // Loops through the allowed types from '\adm\Crud\Types\FormType.php'
        @foreach( $columns['main']['structure']['show_options_type'] as $type )
            option_fields.push( '{{$type}}' );
        @endforeach
    @endif

    var textarea = e.currentTarget.parentNode.parentNode.querySelector('textarea');
    var select_first_option_placeholder = e.currentTarget.parentNode.parentNode.querySelector('[data-option-select-placeholder]');

    if( option_fields.includes( e.currentTarget.value ) ){
        textarea.disabled = '';
        $(select_first_option_placeholder).removeClass('none');
    }else{
        textarea.disabled = 'disabled';
        $(select_first_option_placeholder).addClass('none');
    }
});


// Re-increment the table row index for the name attributes of all inputs in each row
function setFormBuilderRowIndexes(targetTable) {
    targetTable.find('[data-form-row]').each(function( row_index, row ) {
        var $row = $(row);

        var field_regex = /(.*\[)(\d*)(\]\[.*)/;
        var row_regex = /(.*\[)(\d*)(\])/;
        $row.find('input, textarea, select, label').each(function(index, item) {

            if (typeof( this.id ) != 'undefined') {
                this.id = this.id.replace(field_regex, function( str, p1, p2, p3 ){
                    return p1 + row_index + p3;
                });
                this.id = this.id.replace(row_regex, function( str, p1, p2, p3 ){
                    return p1 + row_index + p3;
                });
            }

            if (typeof( this.htmlFor ) != 'undefined') {
                this.htmlFor = this.htmlFor.replace(field_regex, function( str, p1, p2, p3 ){
                    return p1 + row_index + p3;
                });
                this.htmlFor = this.htmlFor.replace(row_regex, function( str, p1, p2, p3 ){
                    return p1 + row_index + p3;
                });
            }

            if (typeof( this.name ) != 'undefined') {
                this.name = this.name.replace(field_regex, function( str, p1, p2, p3 ){
                    return p1 + row_index + p3;
                });
                this.name = this.name.replace(row_regex, function( str, p1, p2, p3 ){
                    return p1 + row_index + p3;
                });
            }

        });
    });
}

</script>