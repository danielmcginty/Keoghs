<script type="text/javascript">
    function initializeRepeatableFields(){
        $('[data-repeatable-field]').pageBuilderSortable({
            row: '[data-repeatable-row]',
            addButton: '[data-repeatable-add-button]',
            removeButton: '[data-repeatable-remove-button]',
            table: '[data-repeatable-table]',
            tableDragHandle: '[data-repeatable-table-handle]'
        });
    }

    $(document).ready( function(){
        initializeRepeatableFields();
    });
</script>