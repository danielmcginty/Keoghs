<script type="text/javascript">
    (function( $ ){
        /**
         * The $.pageBuilderSortable is an object wrapper that's instantiated by the jQuery plugin below.
         * This contains all the functionality for the sortable plugin
         */
        $.pageBuilderSortable = function( element, options ){
            // Set the defaults for our sortable page builder block content
            var defaults = {
                row: '[data-sortable-row]',
                addButton: '[data-sortable-add-button]',
                removeButton: '[data-sortable-remove-button]',
                table: '[data-sortable-table]',
                tableDragHandle: '[data-sortable-table-handle]'
            };

            // And create a centralised function handling the resizing of elements on drag
            var widthHelper = function( e, ui ){
                ui.children().each( function(){
                    $(this).width($(this).width());
                });
                return ui;
            };

            var plugin = this;

            plugin.settings = {};

            var $element = $(element);

            /**
             * The initialize function.
             * Creates an instance of the plugin by:
             * - Setting any custom options
             * - Setting the sortable resize helper
             * - Making the sortable table sortable
             * - Handling click events on the add button
             * - Handling click events on the remove button
             */
            plugin.init = function(){
                plugin.settings = $.extend( {}, defaults, options );
                plugin.widthHelper = widthHelper;

                // Now we've got the settings, and all is well, we can initialize!
                init_sortable();
                init_add_button();
                init_remove_button();
            };

            /**
             * Initialize jQuery UI's sortable on the sortable table within the container block
             */
            var init_sortable = function(){
                $element.find( plugin.settings.table ).sortable({
                    helper: plugin.widthHelper,
                    cursor: 'grabbing',
                    axis: 'y',
                    handle: plugin.settings.tableDragHandle,
                    items: plugin.settings.row,
                    placeholder: 'sortable-placeholder',
                    tolerance: 'pointer',  // pointer, intersect
                    revert: 150,
                    opacity: 1,
                    containment: "parent",
                    forcePlaceholderSize: true,
                    start: function() {
                        $element.addClass('is-sorting');
                    },
                    stop: function() {
                        $element.removeClass('is-sorting');
                    },
                    update: function(){
                        set_row_indexes();
                    }
                });
            };

            /**
             * Whenever the order of elements is changed, make sure that any input names, IDs and label's for attributes
             * are correctly updated and reset any searchable selects
             */
            var set_row_indexes = function(){
                $element.find('textarea[data-tinymce]').each(function () {
                    tinymce.EditorManager.execCommand('mceRemoveEditor', false, $(this).attr('id'));
                    tinymce.remove($(this).attr('id'));
                });

                $element.find(plugin.settings.row).each(function( row_index, row ) {
                    var $row = $(row);

                    var field_prefix = $row.data('field-prefix');
                    field_prefix = field_prefix.replace( /(\[)/g, '\\[' );

                    var regex = new RegExp( '(' + field_prefix + '\\[)(\\d*)(]\\[.*)' );

                    $row.find('input, textarea, select, label, [data-image-select-field] a').each(function(index, item) {

                        if (typeof( this.id ) != 'undefined') {
                            this.id = this.id.replace(regex, function( str, p1, p2, p3 ){
                                return p1 + row_index + p3;
                            });
                        }

                        if (typeof( this.htmlFor ) != 'undefined') {
                            this.htmlFor = this.htmlFor.replace(regex, function( str, p1, p2, p3 ){
                                return p1 + row_index + p3;
                            });
                        }

                        if (typeof( this.name ) != 'undefined') {
                            this.name = this.name.replace(regex, function( str, p1, p2, p3 ){
                                return p1 + row_index + p3;
                            });
                        }

                        if (typeof( $(this).attr('data-image-input-id') ) != 'undefined') {
                            var image_input_attribute = $(this).attr('data-image-input-id');
                            image_input_attribute = image_input_attribute.replace(regex, function( str, p1, p2, p3 ){
                                return p1 + row_index + p3;
                            });
                            $(this).attr('data-image-input-id', image_input_attribute);
                        }

                        if (typeof( $(this).attr('data-asset-input-id') ) != 'undefined') {
                            var asset_input_attribute = $(this).attr('data-asset-input-id');
                            asset_input_attribute = asset_input_attribute.replace(regex, function( str, p1, p2, p3 ){
                                return p1 + row_index + p3;
                            });
                            $(this).attr('data-asset-input-id', asset_input_attribute);
                        }

                        if (typeof( $(this).attr('data-column')) != 'undefined') {
                            var column = JSON.parse( $(this).attr('data-column') );
                            if( typeof( column.field ) != 'undefined' ) {
                                var field = column.field;
                                field = field.replace(regex, function( str, p1, p2, p3 ){
                                    return p1 + row_index + p3;
                                });
                                column.field = field;

                                $(this).attr('data-column', JSON.stringify( column ) );
                            }
                        }

                    });

                    // Inner Tabs
                    var tab_regex = new RegExp( 'page-builder-(\\d+)-inner-tabs-(\\d+)' );
                    $row.find('[data-tab-group]').each( function( index, item ){
                        var group = $(this).data('tab-group');

                        group = group.replace( tab_regex, function( str, p1, p2 ){
                            return 'page-builder-' + p1 + '-inner-tabs-' + row_index;
                        });

                        $(this).data('tab-group', group);
                        $(this).attr('data-tab-group', group);
                    });
                });

                $element.find("select[data-searchable]").chosen({
                    //disable_search_threshold: 10
                });

                $element.find('textarea[data-tinymce]').each(function () {
                    tinymce.EditorManager.execCommand('mceAddEditor', false, $(this).attr('id'));
                });
            };

            /**
             * Set up the on click event for the add button
             */
            var init_add_button = function(){
                $element.off('click', plugin.settings.addButton);
                $element.on('click', plugin.settings.addButton, function( evt ){
                    evt.preventDefault();

                    var $row = $(this).parent().find(plugin.settings.row).first().clone();

                    $row.find('.chosen-container').remove();
                    $row.find('input:not([type="checkbox"],[type="radio"]), select, textarea').val('').show();

                    $row.find('[data-internal-link], [data-external-link], [data-file-link]').addClass('visually-hidden');

                    $row.find('.mce-tinymce').remove();

                    $(this).parent().find(plugin.settings.row).last().after( $row );

                    set_row_indexes();

                    $element.find(plugin.settings.table).sortable( "refresh" );
                });
            };

            /**
             * Set up the on click event for the remove button
             */
            var init_remove_button = function(){
                $element.off('click', plugin.settings.removeButton);
                $element.on('click', plugin.settings.removeButton, function( evt ) {
                    evt.preventDefault();

                    // Count rows
                    var rowCount = $(this).parents(plugin.settings.table).find(plugin.settings.row).length;

                    // Do not remove last row
                    if (rowCount > 1) {
                        var $row = $(this).parents(plugin.settings.row).first();
                        $row.remove();

                        set_row_indexes();
                    }
                });
            };

            // And finally, initialize the plugin ready for use!
            plugin.init();
        };

        /**
         * The jQuery plugin for our pageBuilderSortable object.
         * This can be called like any other jQuery plugin, i.e. $(element).pageBuilderSortable( options )
         * If the element already has a page builder instance associated with it, this does nothing.
         *
         * @param options
         * @returns {*}
         */
        $.fn.pageBuilderSortable = function( options ){
            return this.each( function(){
                if( typeof( $(this).data('pageBuilderSortable') != 'object' ) ){
                    var plugin = new $.pageBuilderSortable( this, options );
                    $(this).data('pageBuilderSortable', plugin);
                }
            });
        };
    }(jQuery));
</script>