<script type="text/javascript">
var $page_builder_modal = $('#page-builder-blocks-modal');
var $page_builder = $('[data-page-builder]');
var $page_builder_blocks_wrapper = $('[data-page-builder-block-container]');

// PAGE BUILDER
$page_builder.sortable({
    helper: 'original',
    cursor: 'grabbing',
    axis: 'y',
    handle: '[data-page-builder-drag-handle]',
    items: '[data-page-builder-block]',
    placeholder: 'sortable-placeholder',
    tolerance: 'intersect',  // pointer, intersect
    revert: 150,
    opacity: 1,
    forcePlaceholderSize: true,
    stop: function( evt, ui ){
        reindexPageBuilder();
    }
});

function initializeSortablePageBuilderBlocks(){
    $('[data-page-builder-sortable]').pageBuilderSortable();
    initializeRepeatableFields();
}

$(document).ready( function(){
    initializeSortablePageBuilderBlocks();
});

// PAGE BUILDER MODAL

// Select Block
$(document).on('click', '.page-builder-modal .js-block-type', function (e) {
    e.preventDefault();

    $(this).closest('.page-builder-modal').find('.js-block-type').removeClass('selected');
    $(this).addClass('selected');

    // Double Click (Quick select and close)
}).on('dblclick', '.page-builder-modal .js-block-type', function(e){
    e.preventDefault();

    var selectButton = $(this).closest('[data-reveal]').find('[data-select-block]');
    selectButton.click();

});

$(document).on('click', '[data-select-block]', function(evt){
    evt.preventDefault();
    evt.stopPropagation();

    // Dismiss Modal
    $(this).closest('[data-reveal]').foundation('reveal', 'close');

    var block = $page_builder_modal.find('.js-block-type.selected').data('page-block-id');

    var add_block_callbacks = [];

    $.ajax({
        url: '{{ route('adm.ajax', ['ajax_get_page_builder_block']) }}',
        type: 'POST',
        data: {
            index: $page_builder_blocks_wrapper.find('[data-page-builder-block]').length,
            reference: block,
            table_name: '{{ $table_name }}'
        },
        dataType: 'json',
        beforeSend: function(){
            $page_builder_modal.foundation('reveal', 'close');
            showPageLoadingOverlay( true );
        },
        success: function( json ){
            if( json.success ){
                $page_builder_blocks_wrapper.append( json.template );

                if( json.callbacks ){
                    add_block_callbacks = json.callbacks;
                }

                $('[data-page-builder-block-search-input]').val('').trigger('keyup');
            }
        },
        complete: function(){
            showPageLoadingOverlay();

            // Initialize default sortable page builder block content
            initializeSortablePageBuilderBlocks();

            for( var index in add_block_callbacks ){
                var callback = add_block_callbacks[ index ];

                if( typeof( eval( callback ) ) == 'function' ){
                    eval(callback)();
                }
            }

            reindexPageBuilder();
        }
    });
});

$(document).on('click', '[data-page-builder-block-search-btn]', search_page_builder);
$(document).on('keyup', '[data-page-builder-block-search-input]', search_page_builder);
function search_page_builder( evt ){
    evt.preventDefault();

    var search_term = $('[data-page-builder-block-search-input]').val().toLowerCase();

    if( search_term == '' ){

        $('[data-page-block-id]').parent().removeClass('none');
        $('[data-no-blocks-found-message]').addClass( 'none' );

    } else {

        var visible_blocks = 0;
        $('[data-page-block-id]').each(function () {
            var $block = $(this);
            var block_text = $block.text().toLowerCase();

            if (block_text.indexOf(search_term) !== -1) {
                visible_blocks++;
                $block.parent().removeClass('none');
            } else {
                $block.parent().addClass('none');
            }
        });

        if( visible_blocks > 0 ){
            $('[data-no-blocks-found-message]').addClass( 'none' );
        } else {
            $('[data-no-blocks-found-message]').removeClass( 'none' );
        }

    }
}

$(document).on('click', '[data-block-set-status]', function(evt){
    evt.preventDefault();
    evt.stopPropagation();

    var $block = $(this).parents('[data-page-builder-block]').first();
    var status = $(this).data('block-set-status');
    $block.find('[data-block-status-field]').val( status );

    if( status == 1 ){
        $block.removeClass('is-inactive');
        $block.removeClass('is-collapsed');
        $(this).data('block-set-status', 0).text('Disable');
    } else {
        $block.addClass('is-inactive');
        $block.addClass('is-collapsed');
        $(this).data('block-set-status', 1).text('Enable');
    }
});

$(document).on('click', '[data-block-collapse]', function(evt){
    evt.preventDefault();
    evt.stopPropagation();

    $(this).addClass('none');
    $(this).parents('[data-page-builder-block]').first().addClass('is-collapsed');
    $(this).parents('[data-page-builder-block]').first().find('[data-block-expand]').removeClass('none');
});

$(document).on('click', '[data-block-expand]', function(evt){
    evt.preventDefault();
    evt.stopPropagation();

    $(this).addClass('none');
    $(this).parents('[data-page-builder-block]').first().removeClass('is-collapsed');
    $(this).parents('[data-page-builder-block]').first().find('[data-block-collapse]').removeClass('none');
});

$(document).on('click', '[data-block-control]', function(evt){
    $(this).parents('.has-dropdown-menu').removeClass('open');
});

$('[data-page-block-controls]').bind('clickoutside', function(){
    $(this).removeClass('open');
});

/**
 * PAGE BUILDER
 */

    // SINGLE BLOCK

    // Remove Block
$(document).on('click', '[data-page-builder-block] [data-block-remove]', function (e) {
    e.preventDefault();
    $(this).closest('[data-page-builder-block]').remove();

    reindexPageBuilder();
});

// Toggle Block Content
$(document).on('click', '[data-page-builder-block] [data-block-view-toggle]', function (e) {
    e.preventDefault();
    var $block = $(this).closest('[data-page-builder-block]');
    $block.toggleClass('is-collapsed');

    if( $block.hasClass('is-collapsed') ){
        $block.find('[data-block-collapse]').addClass('none');
        $block.find('[data-block-expand]').removeClass('none');
    } else {
        $block.find('[data-block-collapse]').removeClass('none');
        $block.find('[data-block-expand]').addClass('none');
    }
});

// Toggle Block Status (Active/Inactive)
$(document).on('click', '[data-page-builder] [data-block-status-toggle]', function (e) {
    e.preventDefault();

    $(this).closest('[data-page-builder-block]').toggleClass('is-inactive');

});

// Enable Block (Status: Active)
$(document).on('click', '[data-page-builder] [data-block-status-set-active]', function (e) {
    e.preventDefault();

    $(this).closest('[data-page-builder-block]').removeClass('is-inactive');

});

// Disable Block (Status: Inactive)
$(document).on('click', '[data-page-builder] [data-block-status-set-active]', function (e) {
    e.preventDefault();

    $(this).closest('[data-page-builder-block]').addClass('is-inactive');

});


// PAGE BUILDER BLOCKS

// Collapse All Blocks
$(document).on('click', '[data-page-builder] [data-page-builder-collapse-all]', function (e) {
    e.preventDefault();

    // Collapse all
    $(this).closest('[data-page-builder]').find('[data-page-builder-block]').addClass('is-collapsed');

    // Close all dropdowns
    $(this).closest('[data-page-builder]').find('.has-dropdown-menu').removeClass('open');

    // Hide all Collapse toggles
    $(this).closest('[data-page-builder]').find('[data-page-builder-block] [data-block-collapse]').addClass('none');

    // Show all Expand toggles
    $(this).closest('[data-page-builder]').find('[data-page-builder-block] [data-block-expand]').removeClass('none');

});

// Expand All Blocks
$(document).on('click', '[data-page-builder] [data-page-builder-expand-all]', function (e) {
    e.preventDefault();
    $(this).closest('[data-page-builder]').find('[data-page-builder-block]').removeClass('is-collapsed');

    // Show all Collapse toggles
    $(this).closest('[data-page-builder]').find('[data-page-builder-block] [data-block-collapse]').removeClass('none');

    // Hide all Expand toggles
    $(this).closest('[data-page-builder]').find('[data-page-builder-block] [data-block-expand]').addClass('none');
});

$(document).on('click', '[data-block-remove]', function(evt){
    evt.preventDefault();
    evt.stopPropagation();

    $(this).parents('[data-page-builder-block]').first().remove();
    reindexPageBuilder();
});

function reindexPageBuilder( callback ){
    $page_builder.find('[data-page-builder-block]').each( function( block_index, block ) {
        var $block = $(block);
        var regex = new RegExp('(page_builder\\[)(\\d*)(]\\[.*)');

        var asset_group_regex = new RegExp('(page_builder\\.)(\\d*)(\\..*)');

        $block.find('textarea[data-tinymce]').each(function () {
            tinymce.EditorManager.execCommand('mceRemoveEditor', false, $(this).attr('id'));
            tinymce.remove($(this).attr('id'));
        });

        $block.find('input, textarea, select, label').each(function (index, item) {

            if (typeof( this.id ) != 'undefined') {
                this.id = this.id.replace(regex, function (str, p1, p2, p3) {
                    return p1 + block_index + p3;
                });
                this.id = this.id.replace(asset_group_regex, function (str, p1, p2, p3) {
                    return p1 + block_index + p3;
                });
            }

            if (typeof( this.htmlFor ) != 'undefined') {
                this.htmlFor = this.htmlFor.replace(regex, function (str, p1, p2, p3) {
                    return p1 + block_index + p3;
                });
                this.htmlFor = this.htmlFor.replace(asset_group_regex, function (str, p1, p2, p3) {
                    return p1 + block_index + p3;
                });
            }

            if (typeof( this.name ) != 'undefined') {
                this.name = this.name.replace(regex, function (str, p1, p2, p3) {
                    return p1 + block_index + p3;
                });
                this.name = this.name.replace(asset_group_regex, function (str, p1, p2, p3) {
                    return p1 + block_index + p3;
                });
            }

        });

        $block.find('[data-asset-input-id], [data-image-input-id]').each(function () {
            if (typeof( $(this).attr('data-asset-input-id') ) != 'undefined') {
                var input_id = $(this).attr('data-asset-input-id');
                input_id = input_id.replace(regex, function (str, p1, p2, p3) {
                    return p1 + block_index + p3;
                });
                $(this).attr('data-asset-input-id', input_id);
            }

            if (typeof( $(this).attr('data-image-input-id') ) != 'undefined') {
                var input_id = $(this).attr('data-image-input-id');
                input_id = input_id.replace(regex, function (str, p1, p2, p3) {
                    return p1 + block_index + p3;
                });
                $(this).attr('data-image-input-id', input_id);
            }

            if (typeof( $(this).data('column') ) != 'undefined') {
                var column_value = $(this).data('column');
                column_value.field = column_value.field.replace(regex, function (str, p1, p2, p3) {
                    return p1 + block_index + p3;
                });
                $(this).data('column', column_value);
            }
        });
    });

    rebuild_tinymce();

    $page_builder.find('[data-page-builder-block]').each(function (block_index, block) {
        var $block = $(block);
        $block.find('textarea[data-tinymce]').each(function () {
            tinymce.EditorManager.execCommand('mceAddEditor', false, $(this).attr('id'));
        });
    });

    $("select[data-searchable]").chosen({
        //disable_search_threshold: 10
    });

    $("select[data-multi-select]").multiselect();
}

</script>

@if( !empty( $pageBuilderDynamicScripts ) )
    @foreach( $pageBuilderDynamicScripts as $script )
        @include( $script )
    @endforeach
@endif