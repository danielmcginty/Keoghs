@if( config('app.use_theme_app_css_in_wysiwyg') )
<script type="text/javascript">
// If we've stated that the current theme's app.css file should be loaded into TinyMCE, we need to extend the default
// options to include the stylesheet
var tinymce_custom_app_css_options = {
    content_css: [
        "/admin/dist/assets/css/tinymce.css", // Load in the default TinyMCE styling
        "/{{ config('app.theme_name', 'default') }}/dist/css/app.css" // Load in our theme specific style
    ]
};
tinymce_default_options = $.extend( {}, tinymce_default_options, tinymce_custom_app_css_options );
</script>
@endif
<script type="text/javascript">
// Load in the default TinyMCE options
TINYMCE_CONFIG_SETS.push( tinymce_default_options );

var tinymce_bold_italic_underline_options = {
    selector: 'textarea[data-tinymce][data-mce-config="bold-italic-underline"]',
    plugins: [ 'contextmenu fullscreen nonbreaking paste searchreplace textcolor visualblocks visualchars wordcount' ],
    toolbar: "undo redo | removeformat | bold italic underline"
};
var tinymce_bold_italic_underline_settings = $.extend( {}, tinymce_default_options, tinymce_bold_italic_underline_options );
TINYMCE_CONFIG_SETS.push( tinymce_bold_italic_underline_settings );

@if( \Auth::user()->userGroup->super_admin )
    var tinymce_change_colour_options = {
        selector: 'textarea[data-tinymce][data-mce-config="change-colour"]',
        plugins: [ 'textcolor' ],
        toolbar: "styleselect | removeformat | bold italic underline"
    };
@else
    var tinymce_change_colour_options = {
        selector: 'body textarea[data-tinymce][data-mce-config="change-colour"]',
        plugins: [ 'textcolor' ],
        toolbar: "styleselect | removeformat"
    };
@endif

var tinymce_change_colour_settings = $.extend( {}, tinymce_default_options, tinymce_change_colour_options );
TINYMCE_CONFIG_SETS.push( tinymce_change_colour_settings );
</script>