@foreach ($menu_array as $menu_item)
    <?php $index = $menu_item['index']; ?>
    <li id="item-{{ $index }}">
        <div class="menu-builder-item">
            <div class="menu-builder-item-inner">
                <div class="js-drag-handle drag-handle" title="Drag to reorder">
                    <i class=""></i>
                </div>
                <div class="item-title ellipsis">
                    <div class="js-show-children show-children" title="Show/hide children">
                        <i class=""></i>
                    </div>
                <span class="font-weight-600 colour-slate margin-r-gutter">
                    {{ $menu_item['link_label'] }}
                </span>
                </div>
                <div class="menu-builder-item-actions">
                    <div class="js-settings-toggle settings-toggle" title="Show/Hide Settings">
                    <span class="inline-block font-size-12">
                        {{ $menu_item['page_type'] }}
                    </span>
                        <i class="fi-adm fi-adm-cog font-size-14 vertical-middle margin-l5"></i>
                    </div>
                </div>
            </div>
            <div class="js-settings settings none">
                <div class="padding-y-gutter padding-x15">

                    <input type="hidden" name="menu_item[{{ $index }}][item_type]" value="{{ $menu_item['item_type'] }}">

                    <div class="js-input-row">
                        <label for="link-label-{{ $index }}">
                            Link Label
                            @include('fields.form.language_identifier')
                        </label>
                        @if ($menu_item['item_type'] == 'dynamic')
                            <div class="help-text">Page Name: <a target="_blank" href="{{ route('theme', [$menu_item['link_url']]) }}" class="underline-on-hover">{{ $menu_item['original_label'] }}</a></div>
                        @endif
                        <input type="text" id="link-label-{{ $index }}" class="margin-b5" name="menu_item[{{ $index }}][link_label]" value="{{ $menu_item['link_label'] }}">
                    </div>

                    <div class="js-input-row">
                        @if ($menu_item['item_type'] == 'custom')
                            <label for="link-url-{{ $index }}">Link URL</label>
                            <input type="text" id="link-url-{{ $index }}" class="margin-b5" name="menu_item[{{ $index }}][link_url]" value="{{ $menu_item['link_url'] }}">
                        @else
                            <input type="hidden" id="link-url-{{ $index }}" class="margin-b5" name="menu_item[{{ $index }}][url_id]" value="{{ $menu_item['url_id'] }}">
                        @endif
                    </div>

                    <input type="hidden" name="menu_item[{{ $index }}][item_target]" value="" />
                    @if ($menu_item['item_type'] == 'custom')
                        <div class="js-input-row">
                            <div class="margin-y5">
                                <input type="checkbox" id="custom-link-target-{{ $index }}" class="fancy-checkbox margin-0" name="menu_item[{{ $index }}][item_target]" value="_blank" {{ isset($menu_item['item_target']) && $menu_item['item_target'] == '_blank' ? 'checked=checked' : '' }} />
                                <label for="custom-link-target-{{ $index }}" class="font-size-16 fixed-font-size">
                                    <span class="font-size-14 line-height-1 colour-grey-dark">Open link in new tab</span>
                                </label>
                            </div>
                        </div>
                    @endif

                    <a href="#" class="js-delete-menu-item colour-error font-size-12 underline">Remove</a>
                </div>
            </div>
        </div>

        @if (!empty($menu_item['children']))
            <ol>
                @include('menu-builder.partials.menu-list', array('menu_array' => $menu_item['children']))
            </ol>
        @endif

    </li>
@endforeach