@extends('layouts.adm')

@section('page_title', $menu_name)

@section('content')

    {!! Form::open(['url' => route('adm.path'), 'class' => 'js-menu-builder-form']) !!}

    <div class="js-section-action-bar-container relative" style="height: 79px;">
        <div class="js-section-action-bar section-action-bar top-0 left-0 width-100">
            <div class="row padding-y20">
                <div class="column">
                    <div class="table width-100">
                        <div class="table-cell">
                            <h1 class="font-size-24 font-weight-600 margin-0">Edit {{ $menu_name }}</h1>
                        </div>
                        <div class="table-cell text-right">
                            <div class="inline-block vertical-middle margin-r-gutter">
                                <button type="submit" class="js-delete-all-menu-items button round gradient bordered colour-error margin-b0"><span>Remove All Links</span></button>
                            </div>
                            @if( \Auth::user()->userGroup->super_admin )
                                <div class="inline-block vertical-middle margin-r-gutter">
                                    <a href="#" data-reveal-id="update-menu-details-modal" class="button round bordered margin-b0 colour-default">Edit Menu Details</a>
                                </div>
                            @endif
                            <div class="inline-block vertical-middle">
                                <button type="submit" class="button round gradient bordered colour-success margin-b0"><span>Save</span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content-area margin-b20">

        <div class="row">
            <div class="column small-3">

                @if( isset($language_links) && !empty($language_links) )
                    <div class="language-select section-container background colour-white radius margin-b-gutter-full font-size-14 overflow-hidden">
                        <ul class="simple-list margin-0">
                            @foreach( $language_links as $language )
                                <li>
                                    <a href="{{ $language['url'] }}" data-language-select="{{ $language['locale'] }}" title="Edit {{ $language['title'] }}" class="button colour-white block text-left margin-0<?php if( $language['current']){ ?> current<?php } ?><?php if( $language['status']){ ?> active<?php } ?>">
                                        {{ $language['title'] }}
                                        <span class="status circle" title="Active"></span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="section-container background colour-white radius margin-b20">

                    <div data-menu-item-loading-wrapper>
                        <div class="text-center padding-y-gutter-full font-size-24">
                            <i class="fi-adm fi-adm-loading fi-spin"></i>
                        </div>
                    </div>

                    <div data-new-menu-items-wrapper class="none">
                        <!-- Ability to create a custom menu item with a custom URL and label -->
                        <div data-custom-link-wrapper data-accordion class="js-page-list-accordion">
                            <div data-control class="font-size-14" title="Expand/Collapse">Custom Link</div>
                            <div data-content>
                                <div class="padding-b-gutter-full padding-x-gutter-full">

                                    <div class="js-custom-link-label-input-row margin-b10">
                                        <label for="custom-link-label">Link Label</label>
                                        <input type="text" id="custom-link-label" class="margin-b5" name="custom_link_label" value="">
                                    </div>

                                    <div class="js-custom-link-url-input-row margin-b10">
                                        <label for="custom-link-url">Link URL</label>
                                        <div class="help-text">Include http://</div>
                                        <input type="text" id="custom-link-url" class="margin-b5" name="custom_link_url" value="">
                                    </div>

                                    <div class="margin-b-gutter-full">
                                        <div class="margin-y5">
                                            <input type="checkbox" id="custom-link-target" class="fancy-checkbox margin-0" name="custom_link_target" value="_blank" />
                                            <label for="custom-link-target" class="font-size-16 fixed-font-size">
                                                <span class="font-size-14 line-height-1 colour-grey-dark">Open link in new tab</span>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="text-right">
                                        <a href="#" class="js-add-item button tiny round bordered colour-cancel text-shadow-none margin-b0">Add to Menu</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="column small-9">

                <div class="section-container background colour-white radius margin-b20">

                    <div class="padding-gutter-full colour-grey">
                        <h1 class="colour-off-black font-size-14 margin-b-gutter">Menu Structure</h1>
                        <p class="font-size-14 margin-b0">Drag each item to re-structure. Drag underneath another item to create dropdowns.</p>
                    </div>

                    <div class="menu-builder margin-b50">
                        <ol class="js-menu-builder">
                            @if( is_array( $menu_array ) && !empty( $menu_array ) )
                            @include('menu-builder.partials.menu-list', array('menu_array' => $menu_array, 'index' => 1))
                            @endif
                        </ol>
                    </div>

                </div>

            </div>
        </div>

    </div>

    {{ Form::close() }}

    <div id="confirm-delete-item-modal" class="reveal-modal notice-modal" data-reveal aria-hidden="true" role="dialog">
        <div class="modal-content">
            <p class="modal-heading padding-r40 font-weight-600 margin-b10">Remove item from Menu</p>
            <p class="font-size-14">Are you sure you want to remove this menu item?</p>
        </div>
        <div class="modal-actions">
            <a href="#" class="js-confirm-delete-menu-item button round gradient bordered colour-error margin-b0 margin-r10">Remove</a>
            <a href="#" data-close-reveal class="button round bordered colour-cancel margin-b0">Cancel</a>
        </div>
    </div>

    <div id="confirm-delete-all-items-modal" class="reveal-modal notice-modal" data-reveal aria-hidden="true" role="dialog">
        <div class="modal-content">
            <p class="modal-heading padding-r40 font-weight-600 margin-b10">Remove all links from Menu</p>
            <p class="font-size-14">Are you sure you want to remove <strong>all</strong> menu items?</p>
        </div>
        <div class="modal-actions">
            <a href="#" class="js-confirm-delete-all-menu-items button round gradient bordered colour-error margin-b0 margin-r10">Remove All</a>
            <a href="#" data-close-reveal class="button round bordered colour-cancel margin-b0">Cancel</a>
        </div>
    </div>

    @if( \Auth::user()->userGroup->super_admin )
    <div id="update-menu-details-modal" class="reveal-modal notice-modal" data-reveal aria-hidden="true" role="dialog">
        {!! Form::open(['url' => route('adm.path', ['menu_builder', 'update', $menu_id])]) !!}
        <div class="modal-content">
            <p class="modal-heading padding-r40 font-weight-600 margin-b10">Update {{ $menu_name }}</p>
            <div class="input-field">
                <label for="title" class="inline-block margin-b-gutter">Menu <span class="required">*</span></label>
                <div class="help-text margin-t-gutter-negative">The title of this Menu</div>
                {{ Form::text('title', $menu_name, ['id' => 'title', 'required']) }}
            </div>
            <div class="input-field">
                <label for="reference" class="inline-block margin-b-gutter">Reference <span class="required">*</span></label>
                <div class="help-text margin-t-gutter-negative">Used as a key reference for accessing the menu on the front end.<br>e.g. <strong>main_menu</strong> which would be called on the front end via <code>$MenuHelper::getMenu( '<strong>main_menu</strong>' );</code></div>
                {{ Form::text('reference', $menu_reference, ['id' => 'reference', 'required']) }}
            </div>
            <div class="input-field">
                <label for="nest_limit" class="inline-block margin-b-gutter">Nest Limit</label>
                <div class="help-text margin-t-gutter-negative">Includes the top level. 1 = Top level, 2 = Dropdown Level etc.</div>
                {{ Form::number('nest_limit', $nest_limit, ['id' => 'nest_limit']) }}
            </div>
        </div>
        <div class="modal-actions">
            <button type="submit" class="button round gradient bordered colour-success margin-b0 margin-r10">Save</button>
            <a data-close-reveal href="#" class="button round bordered colour-cancel margin-b0">Cancel</a>
        </div>
        {{ Form::close() }}
    </div>
    @endif

@endsection

@push('footer_scripts')
<!-- Also required jquery.ui -->
<script src="/admin/dist/assets/js/vendor/jquery.fastLiveFilter.js"></script><!-- https://github.com/awbush/jquery-fastLiveFilter -->
<script src="/admin/dist/assets/js/vendor/jquery.mjs.nestedSortable.js"></script><!-- https://github.com/ilikenwf/nestedSortable -->
<script>
    $(document).ready(function() {

        var MENU_ITEM_TYPE_COUNTER = 0;

        $.ajax({
            url: '{{ route('adm.ajax', 'ajax_get_menu_builder_urls') }}',
            type: 'GET',
            dataType: 'json',
            success: function( json ){
                for( var page_type in json.urls ){
                    MENU_ITEM_TYPE_COUNTER++;

                    var pages = json.urls[ page_type ];

                    var accordion = '';
                    accordion += '<div data-accordion class="js-page-list-accordion">';
                    accordion += '  <div data-control class="font-size-14" title="Expand/Collapse">' + page_type + '</div>';
                    accordion += '  <div data-content>';
                    accordion += '      <div data-page-type-container="' + MENU_ITEM_TYPE_COUNTER + '" class="padding-b-gutter-full padding-x-gutter-full">';
                    accordion += '          <div class="relative margin-t5 margin-b10">';
                    accordion += '              <label for="filter-group-' + MENU_ITEM_TYPE_COUNTER + '" class="visually-hidden">Filter</label>';
                    accordion += '              <input data-page-type-search-input="' + MENU_ITEM_TYPE_COUNTER + '" id="filter-group-' + MENU_ITEM_TYPE_COUNTER + '" class="padding-l30 padding-r5 margin-b0" type="search" placeholder="Filter...">';
                    accordion += '              <i class="fi-adm fi-adm-search absolute top-50 left-0 line-height-1 font-size-14 margin-t7-negative margin-l10 colour-slate"></i>';
                    accordion += '          </div>';
                    accordion += '          <ul data-page-type-search-list="' + MENU_ITEM_TYPE_COUNTER + '" class="simple-list border-1 colour-grey-lightest radius margin-b-gutter padding-t15 padding-l15 padding-b5 padding-r0 overflow-y-scroll" style="height: 210px;">';

                    for( var j in pages ){
                        var page = pages[j];

                        accordion += '          <li class="margin-b10">';
                        accordion += '              <input type="checkbox" name="selected[]" value="' + page.id + '" id="selected-' + page.id + '" class="fancy-checkbox margin-0" />';
                        accordion += '              <label for="selected-' + page.id + '" class="font-size-16 fixed-font-size">';
                        accordion += '                  <span class="font-size-14">';
                        accordion += '                      ' + page.title;
                        accordion += '                  </span>';
                        accordion += '              </label>';
                        accordion += '          </li>';
                    }

                    accordion += '          </ul>';
                    accordion += '          <div data-page-type-search-no-results="' + MENU_ITEM_TYPE_COUNTER + '" class="none padding-t-gutter padding-b-gutter-full margin-b-gutter text-center colour-error font-weight-600 font-size-14 colour-grey" style="height: 200px;">';
                    accordion += '              No match found.';
                    accordion += '          </div>';
                    accordion += '          <div class="table width-100">';
                    accordion += '              <div class="table-cell">';
                    accordion += '                  <a href="#" class="js-check-all colour-grey font-size-13 font-weight-600 underline-on-hover">Select All</a>';
                    accordion += '              </div>';
                    accordion += '              <div class="table-cell text-right">';
                    accordion += '                  <a href="#" class="js-add-item button tiny round bordered colour-cancel text-shadow-none margin-b0">Add to Menu</a>';
                    accordion += '              </div>';
                    accordion += '          </div>';
                    accordion += '      </div>';
                    accordion += '  </div>';
                    accordion += '</div>';

                    $('[data-custom-link-wrapper]').before( accordion );
                }

                initialize_page_pickers();

                $('[data-menu-item-loading-wrapper]').addClass( 'none' ).remove();
                $('[data-new-menu-items-wrapper]').removeClass( 'none' ).find('[data-control]').first().trigger('click');
            }
        });

        function initialize_page_pickers(){
            /**
             * Filter Page list items by name
             * Source: https://github.com/awbush/jquery-fastLiveFilter
             */
            $('[data-page-type-search-input]').each(function(index) {
                $('[data-page-type-search-input="' + (index) + '"]').fastLiveFilter('[data-page-type-search-list="' + (index) + '"]', {
                    callback: function (total) {
                        if (total > 0) {
                            $('[data-page-type-search-no-results="' + (index) + '"]').addClass('none');
                            $('[data-page-type-search-list="' + (index) + '"]').removeClass('none');
                        } else {
                            $('[data-page-type-search-no-results="' + (index) + '"]').removeClass('none');
                            $('[data-page-type-search-list="' + (index) + '"]').addClass('none');
                        }
                    }
                });
            });


            /**
             * ACCORDION
             * https://github.com/vctrfrnndz/jquery-accordion
             *
             * Used on menu builder
             */
            if ( $('.js-page-list-accordion').length ) {
                $('.js-page-list-accordion').accordion({
                    transitionEasing: 'cubic-bezier(0.455, 0.030, 0.515, 0.955)',
                    transitionSpeed: 200,
                });
            }
        }

        /**
         * Filter Page list items by name
         * Source: https://github.com/awbush/jquery-fastLiveFilter
         */
        $('[data-page-type-search-input]').each(function(index) {
            $('[data-page-type-search-input="' + (index) + '"]').fastLiveFilter('[data-page-type-search-list="' + (index) + '"]', {
                callback: function (total) {
                    if (total > 0) {
                        $('[data-page-type-search-no-results="' + (index) + '"]').addClass('none');
                        $('[data-page-type-search-list="' + (index) + '"]').removeClass('none');
                    } else {
                        $('[data-page-type-search-no-results="' + (index) + '"]').removeClass('none');
                        $('[data-page-type-search-list="' + (index) + '"]').addClass('none');
                    }
                }
            });
        });


        /**
         * ACCORDION
         * https://github.com/vctrfrnndz/jquery-accordion
         *
         * Used on menu builder
         */
        if ( $('.js-page-list-accordion').length ) {
            $('.js-page-list-accordion').accordion({
                transitionEasing: 'cubic-bezier(0.455, 0.030, 0.515, 0.955)',
                transitionSpeed: 200,
            });
        }


        /**
         * Check / uncheck all visible menu items (the filter search input may search some)
         */
        $('body').on('click', '.js-check-all', function(e) {
            e.preventDefault();
            e.stopPropagation();

            var $visibleCheckboxes = $(this).parents('[data-page-type-container]').find('[data-page-type-search-list] [name*="selected"]:visible');
            var numberChecked = 0; // Number of visible checkboxes which are checked

            $visibleCheckboxes.each(function() {
                if ($(this).is(':checked')) {
                    numberChecked++;
                }
            });

            if ($visibleCheckboxes.length == numberChecked) {
                // If all the visible checkboxes are checked, uncheck them
                $visibleCheckboxes.prop('checked', false);
            } else {
                // If there are any unchecked checkboxes, check them
                $visibleCheckboxes.prop('checked', true);
            }
        });


        /**
         * Drag and drop nestable menu
         * Source: https://github.com/ilikenwf/nestedSortable
         * Example: http://ilikenwf.github.io/example.html
         * @type {jQuery}
         */
        var $menuList = $('.js-menu-builder').nestedSortable({
            forcePlaceholderSize: true,
            handle: '.js-drag-handle',
            helper:	'clone',
            items: 'li',
            opacity: .6,
            placeholder: 'placeholder',
            revert: 250,
            tabSize: 25,
            tolerance: 'pointer',
            toleranceElement: '> .menu-builder-item',
            maxLevels: '{{ $nest_limit }}',
            isTree: true,
            expandOnHover: 700,
            startCollapsed: false
        });


        /**
         * Show/hide the context/settings menu for a menu item
         */
        $('body').on('click', '.js-settings-toggle', function(e) {
            e.preventDefault();
            e.stopPropagation();

            $(this).parents('.menu-builder-item').find('.js-settings').toggleClass('none');
        });


        /**
         * Show/hide child items on the menu list
         */
        $(document).on('click', '.js-show-children', function(e) {
            e.preventDefault();
            e.stopPropagation();

            var $parentItem = $(this).closest('li');

            if ($parentItem.hasClass('mjs-nestedSortable-collapsed')) {
                $parentItem.removeClass('mjs-nestedSortable-collapsed');
                $parentItem.addClass('mjs-nestedSortable-expanded');
            } else {
                $parentItem.removeClass('mjs-nestedSortable-expanded');
                $parentItem.addClass('mjs-nestedSortable-collapsed');
            }
        });


        /**
         * Make sure the plus and minus toggle icons are correct
         */
        function updateShowHideState() {

            $('.js-menu-builder li').each(function(index, item) {

                var $children = $(this).find('ol li');

                if ($children.length) {
                    if ($(this).hasClass('mjs-nestedSortable-expanded') || $(this).hasClass('mjs-nestedSortable-collapsed')) {
                        $(this).removeClass('mjs-nestedSortable-leaf').addClass('mjs-nestedSortable-branch');
                    }
                } else {
                    $(this).removeClass('mjs-nestedSortable-branch').addClass('mjs-nestedSortable-leaf');
                }
            });
        }


        /**
         * Add dynamic item to drag and drop list
         */
        $(document).on( 'click', '.js-add-item', function(e) {
            e.preventDefault();
            e.stopPropagation();

            if ($(this).parents('[data-page-type-container]').length) {

                // We are trying to add a dynamic page (from the URL table)

                var $checkedUrls = $(this).parents('[data-page-type-container]').find('[data-page-type-search-list] [name*="selected"]:visible:checked')
                var urlIdsToAdd = [];

                $checkedUrls.each(function () {
                    urlIdsToAdd.push($(this).val())
                });

                if (urlIdsToAdd.length) {
                    // Only try to add items if they have been selected
                    addItemToMenu(urlIdsToAdd);

                    // And now we've added items, uncheck all selected items
                    uncheckSelectedNewMenuItems();
                }

            } else {

                // We are trying to add a custom URL

                if (customItemIsValid()) {
                    addItemToMenu(null, 'custom');

                    // And now we've added items, uncheck all selected items
                    uncheckSelectedNewMenuItems();
                }
            }
        });


        function uncheckSelectedNewMenuItems(){
            $('[data-new-menu-items-wrapper]').find('[name*="selected"]').removeAttr('checked').prop('checked', false);
        }


        /**
         * Validate the custom item fields
         */
        function customItemIsValid() {

            var isValid = true;

            var $customLinkUrlInputRow = $('.js-custom-link-url-input-row');
            var $customLinkLabelInputRow = $('.js-custom-link-label-input-row');

            // Clear any error messages and error states
            $customLinkUrlInputRow.removeClass('has-error').find('.inline-error').remove();
            $customLinkLabelInputRow.removeClass('has-error').find('.inline-error').remove();

            // Validate fields
            if ($customLinkUrlInputRow.find('input').val().length == 0) {
                isValid = false;
                $customLinkUrlInputRow.addClass('has-error');
                $('input', $customLinkUrlInputRow).after('<span class="inline-error margin-y0">Link URL is required.</span>');
            }

            if ($customLinkLabelInputRow.find('input').val().length == 0) {
                isValid = false;
                $customLinkLabelInputRow.addClass('has-error');
                $('input', $customLinkLabelInputRow).after('<span class="inline-error margin-y0">Link Label is required.</span>');
            }

            // Trigger window resize event so the custom item accordion panel is the correct size
            // (for when error messages are added and removed)
            window.dispatchEvent(new Event('resize'));
            $('.js-page-list-accordion').accordion('accordion.refresh'); // TODO: https://github.com/vctrfrnndz/jquery-accordion/issues/14#issuecomment-258445838

            return isValid;
        }


        /**
         * Add an item to the menu list
         */
        function addItemToMenu(urlIds, type) {

            var postData = {};

            switch(type) {
                case 'custom':
                    postData['item_url'] = $('[name="custom_link_url"]').val();
                    postData['item_label'] = $('[name="custom_link_label"]').val();
                    postData['item_target'] = $('[name="custom_link_target"]').is(':checked') ? '_blank' : false;
                    postData['item_type'] = 'custom';
                    break;
                default:
                    postData['item_type'] = 'dynamic';
                    postData['url_ids'] = urlIds;
                    break;
            }

            // So we know what value to use for the id="" attribute of
            // any items we are about to add
            postData['total_menu_items'] = $('.js-menu-builder li').length > 0 ? $('.js-menu-builder li').last().attr('id').replace('item-', '') : 0;

            $.ajax({
                url: '/adm/ajax_get_menu_item',
                type: 'POST',
                dataType: 'json',
                data: postData,
                beforeSend: function(){
                    showPageLoadingOverlay(true);
                },
                success: function(data){
                    if(data.items){
                        $('.js-menu-builder').append(data.items);
                    }
                },
                complete: function(){
                    showPageLoadingOverlay(false);

                    // reset custom item fields if a custom item was added
                    if (type == 'custom') {
                        $('[name="custom_link_url"]').val('');
                        $('[name="custom_link_label"]').val('');
                    }
                }
            });
        }


        $('body').on('click touchend', '.js-delete-all-menu-items', function(e) {

            e.preventDefault();
            e.stopPropagation();

            $('#confirm-delete-all-items-modal').foundation('reveal', 'open');

            $('body').on('click', '.js-confirm-delete-all-menu-items', function(e) {

                e.preventDefault();
                e.stopPropagation();

                $('.js-menu-builder li').remove();

                $('#confirm-delete-all-items-modal').foundation('reveal', 'close');
            });
        });


        /**
         * Delete an item from the menu list
         */
        $('body').on('click touchend', '.js-delete-menu-item', function(e) {
            e.preventDefault();
            e.stopPropagation();

            var $targetLink = $(this);
            var $itemToDelete = $targetLink.closest('li');

            // Make a copy of any child elements inside this item before deleting it
            var innerChildren = $itemToDelete.find('ol').html();

            // Open confirm modal
            $('#confirm-delete-item-modal').foundation('reveal', 'open');

            $('body').on('click', '.js-confirm-delete-menu-item', function(e) {

                e.preventDefault();
                e.stopPropagation();

                // Insert the copied child elements before deleting the item
                // so we only delete the parent item and not it's children!
                $itemToDelete.after(innerChildren);
                $itemToDelete.remove(); // Delete item

                $itemToDelete.on('remove', updateShowHideState());

                $('#confirm-delete-item-modal').foundation('reveal', 'close');
            });
        });

        // Store whether we need to update the menu's details as well as save the structure
        var menu_needs_updating = false;

        var $menuForm = $('.js-menu-builder-form');
        $menuForm.on('submit', function(e) {
            e.preventDefault();
            e.stopPropagation();

//            console.log($menuList.nestedSortable('toHierarchy'));

            var arrayToSave = addDataToHierarchy($menuList.nestedSortable('toHierarchy'));

//            console.log(arrayToSave);

            $.ajax({
                url: '/adm/ajax_save_menu',
                type: 'POST',
                dataType: 'json',
                data: {
                    menu_id: '{{ $menu_id }}',
                    language_id: '{{ $current_language }}',
                    menu: arrayToSave
                },
                beforeSend: function(){
                    showPageLoadingOverlay(true);

                    $menuList.find('.inline-error').remove();
                    $menuList.find('.has-error').removeClass('has-error');
                },
                success: function(data){
                    if (data.errors) {
                        for (var key in data.errors) {

                            var $targetInput = $('[name="' + key +'"]');

                            $targetInput.parents('.mjs-nestedSortable-branch').removeClass('mjs-nestedSortable-collapsed').addClass('mjs-nestedSortable-expanded');
                            $targetInput.parents('.js-settings').first().removeClass('none');

                            $targetInput.parent('.js-input-row').addClass('has-error');
                            $targetInput.after('<span class="inline-error margin-t5-negative margin-b5">' + data.errors[key] + '</span>')

                        }
                    }

                    if( $menuList.find('.has-error').length == 0 ){

                        if( menu_needs_updating ){
                            $('#update-menu-details-modal form').trigger('submit');
                        } else {
                            if (data.redirect) {
                                window.location = data.redirect;
                            } else {
                                window.location.reload();
                            }
                        }

                    }
                },
                complete: function(){
                    showPageLoadingOverlay(false);
                }
            });

//            console.log($menuForm.serialize());
        });

        /**
         * Super Admin Update Menu Form
         * Hijack the form submit and save the menu before reloading the page
         */
        $('#update-menu-details-modal form').on('submit', function(evt){
            if( !menu_needs_updating ) {
                evt.preventDefault();
                evt.stopPropagation();

                menu_needs_updating = true;

                $menuForm.trigger('submit');
            }
        });


        function addDataToHierarchy(arr) {

            for (var key in arr) {

                $('input[name*="menu_item[' + arr[key]['id'] + ']"]', $menuList).each(function(index, input) {

                    var propName = $(input).attr('name');
                    propName = propName.replace('menu_item[' + arr[key]['id'] + ']', '').replace('[', '').replace(']', '');
                    var propValue = $(input).val();

                    if( $(input).attr('type') == 'checkbox' ){
                        propValue = $(input).is(':checked') ? $(input).val() : '';
                    }

                    arr[key][propName] = propValue;
                });

                if (arr[key]['children']) {
                    addDataToHierarchy(arr[key]['children']);
                }
            }

            return arr;
        }

    });
</script>
@endpush