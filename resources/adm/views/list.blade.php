@extends('layouts.adm')

@section('page_title', $title)

@section('content')

    <div class="js-section-action-bar-container relative" style="height: 79px;">
        <div class="js-section-action-bar section-action-bar top-0 left-0 width-100">
            <div class="row padding-y-gutter-full">
                <div class="column">
                    <div class="table width-100">
                        <div class="table-cell">
                            <h1 class="font-size-24 font-weight-600 margin-0">{{ $title }}</h1>
                        </div>
                        <div class="table-cell text-right">
                            @if( $export_action )
                                <div class="inline-block vertical-middle margin-r-gutter">
                                    <a data-export-btn href="{{ $export_action }}" class="button round bordered colour-default text-shadow-none margin-b0">Export CSV</a><!-- TODO: only appears if rows have been selected -->
                                </div>
                            @endif
                            @if( isset($extra_actions) && !empty($extra_actions) )
                                @foreach( $extra_actions as $action )
                                    <div class="inline-block vertical-middle margin-r-gutter">
                                        <a href="{{ $action['url'] }}" class="button round bordered margin-b0 colour-default">{{ $action['name'] }}</a>
                                    </div>
                                @endforeach
                            @endif
                            @if( isset( $extra_bulk_actions ) && !empty( $extra_bulk_actions ) )
                                @foreach( $extra_bulk_actions as $bulk_action )
                                    <div class="inline-block vertical-middle margin-x-gutter">
                                        <a href="#" data-bulk-action="{{ $action['url'] }}" class="button round bordered margin-b0 {{ $action['colour'] }}">{{ $action['text'] }}</a>
                                    </div>
                                @endforeach
                            @endif
                            @if( $create_action )
                                <div class="inline-block vertical-middle">
                                    <a href="{{ $create_action }}" class="button round gradient bordered colour-success margin-b0" id="add-new-button">
                                        <i class="fi-adm fi-adm-plus font-size-12 margin-r5 inline-block vertical-baseline"></i>
                                        <span class="inline-block vertical-baseline">Add New</span>
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content-area margin-b-gutter-full">

        <!-- form is shown by default but gets hidden if rows are selected -->
        <div class="js-search-form row margin-b-gutter-full">
            <form action="" method="get" enctype="multipart/form-data" id="listing-search-form" class="block">

                <div class="column small-9 end">
                    @if( isset($search_data) && !empty($search_data) )
                        <div class="inline-block vertical-middle relative">
                            <label for="search" class="visually-hidden">Search</label>
                            <input name="search" id="search" class="padding-l30 padding-r5 margin-b0" type="search" placeholder="Search..." value="{{ $search_data['search_term'] }}">
                            <i class="fi-adm fi-adm-search absolute top-50 left-0 line-height-1 font-size-14 margin-t7-negative margin-l-gutter colour-slate"></i>
                        </div>
                        @if( isset($languages) && sizeof($languages) > 1 )
                        <div class="inline-block vertical-middle margin-l5">
                            <label for="filter-language" class="visually-hidden">Language</label>
                            <select id="filter-language" name="language" class="inline-block margin-b0" style="min-width: 150px;">
                            @foreach( $languages as $language )
                                <option value="{{ $language->locale }}" @if($current_language == $language->id)selected="selected"@endif>{{ $language->title }}</option>
                            @endforeach
                            </select>
                        </div>
                        @endif
                        <div class="inline-block vertical-middle margin-l5">
                            <label for="column" class="visually-hidden">Column to search</label>
                            <select name="search-by" id="column" class="margin-b0" style="min-width: 150px;">
                                <option value="all">All Columns</option>
                                @foreach( $search_data['columns'] as $column => $label )
                                    <option value="{{ $column }}" @if( $column == $search_data['search_field'] )selected="selected"@endif>{{ $label }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="inline-block vertical-middle margin-l5">
                            <button type="submit" class="button small round bordered colour-default margin-b0 outline-none"><span>Search</span></button>
                        </div>
                    @endif
                </div>

                <div class="column small-3 end text-right">
                    @if( isset($show_number_options) && !empty($show_number_options) )
                    <label for="show" class="inline-block margin-r5">Show</label>
                    <select id="show" name="show" class="inline-block margin-b0" style="max-width: 80px;">
                        @foreach( $show_number_options as $value => $label )
                            <option value="{{ $value }}" @if( $value == $current_show_option || ($current_show_option === false && $value == -1 ) )selected="selected"@endif>{{ $label }}</option>
                        @endforeach
                    </select>
                    @endif
                </div>

            </form>
        </div>

        @if( $show_action_bar )
        <div class="js-action-form row margin-b-gutter-full none">
            <div class="column">
                @if( $has_status_field )
                <div class="inline-block vertical-middle">
                    <label for="set-status" class="visually-hidden">Set Status</label>
                    <select id="set-status" name="set-status" class="js-set-status margin-b0" style="min-width: 150px;">
                        <option disabled selected>-- Set Status --</option>
                        <option value="1">Enabled</option>
                        <option value="0">Disabled</option>
                    </select>
                    <div id="change-status-modal" class="reveal-modal notice-modal" data-reveal aria-hidden="true" role="dialog">
                        <div class="modal-content">
                            <p class="modal-heading padding-r40 font-weight-600 margin-b-gutter">Update Status</p>
                            <p class="font-size-14">Are you sure you want to update status on the selected rows?</p>
                        </div>
                        <div class="modal-actions">
                            <a href="#" class="js-update-status-confirm button round gradient bordered colour-success margin-b0 margin-r-gutter">Yes</a>
                            <a data-close-reveal href="#" class="js-cancel-status-update button round bordered colour-cancel margin-b0">Cancel</a>
                        </div>
                        <a class="close-reveal-modal visually-hidden" aria-label="Close">&#215;</a>
                    </div>
                </div>
                @endif
                @if (in_array('destroy', $actions))
                <div class="inline-block vertical-middle margin-l5">
                    <a href="#" data-reveal-id="delete-modal" class="js-delete-button button round bordered colour-error margin-b0">Delete Selected</a>

                    <div id="delete-modal" class="reveal-modal notice-modal" data-reveal aria-hidden="true" role="dialog">
                        <div class="modal-content">
                            <p class="modal-heading padding-r40 font-weight-600 margin-b-gutter">Delete Selected {{ $title }}</p>
                            <p class="font-size-14">Are you sure you want to delete the selected {{ $title }}?</p>
                        </div>
                        <div class="modal-actions">
                            <a href="#" class="js-delete-confirm button round gradient bordered colour-error margin-b0 margin-r-gutter">Delete</a>
                            <a href="#" class="js-cancel-delete button round bordered colour-cancel margin-b0">Cancel</a>
                        </div>
                        <a class="close-reveal-modal visually-hidden" aria-label="Close">&#215;</a>
                    </div>
                </div>
                @endif
            </div>
        </div>
        @endif

        <div class="row">
            <div class="column">

                <div class="section-container background colour-white radius margin-b-gutter-full">

                    <table class="list-table width-100 border-0">
                        <thead>
                        <tr>
                            @if( $show_action_bar )
                            <th class="list-table-checkbox-col">
                                <div class="block">
                                    <input type="checkbox" name="selected[]" id="selected-all" class="js-check-all fancy-checkbox margin-0">
                                    <label for="selected-all" class="font-size-16 fixed-font-size" title="Select All"><span class="visually-hidden">Select All</span></label>
                                </div>
                            </th>
                            @endif
                            @foreach( $columns as $column )
                            <th class="{{ implode( " ", $column['classes'] ) }}" {{ $column['data_str'] }}>
                                @if( isset($column['orderable']) && $column['orderable'] )
                                    <a href="{{ $column['orderable_url'] }}" class="block colour-grey-dark">
                                        {{ $column['label'] }}
                                    </a>
                                @else
                                    {{ $column['label']  }}
                                @endif
                            </th>
                            @endforeach
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>

                        @if( sizeof( $items ) == 0 )

                            <tr>
                                <td colspan="99">
                                    <span class="block padding-gutter">No results found.</span>
                                </td>
                            </tr>

                        @else

                            @foreach( $items as $i => $item )
                            <tr>
                                @if( $show_action_bar )
                                <td class="list-table-checkbox-col text-center">
                                    @if( isset( $item->system_page ) && !empty( $item->system_page ) && !\Auth::user()->userGroup->super_admin )
                                        <i class="fi-adm fi-adm-padlock opacity-6" title="Key pages cannot be deleted or disabled."></i>
                                    @else
                                        <div class="block">
                                            <input type="checkbox" name="selected[]" value="<?php echo $item->$primaryColumn; ?>" id="selected-<?php echo $item->$primaryColumn; ?>" class="fancy-checkbox margin-0">
                                            <label for="selected-<?php echo $item->$primaryColumn; ?>" class="font-size-16 fixed-font-size" title="Select"><span class="visually-hidden">Select</span></label>
                                        </div>
                                    @endif
                                </td>
                                @endif
                                @foreach( $columns as $column )
                                <td @if( $linked_field == $column['field'] && in_array('edit', $actions) ) class="padding-y0"@endif>
                                    @if( $linked_field == $column['field'] && in_array('edit', $actions) )
                                        <?php
                                            $editParams = [$table, 'edit', $item->$primaryColumn];
                                            if( isset($item->language_id) && $item->language_id != $defaultLanguage ){
                                                $editParams[] = $item->locale;
                                            }

                                            $to_display = $item->{$column['field']};
                                            if( strlen( $to_display ) > 25 ){
                                                $to_display = substr( $to_display, 0, 25 ) . '...';
                                            }
                                        ?>
                                        <a href="{{ route('adm.path', $editParams) }}" title="Edit {{ $item->{$column['field']} }}" class="block padding-y-gutter">
                                            {!! $to_display !!}
                                        </a>
                                    @else
                                        {!! $item->{$column['field']} !!}
                                    @endif
                                </td>
                                @endforeach
                                <td class="text-right nowrap font-size-0">

                                    {{--
                                    @if (in_array('destroy', $actions) && empty($item->system_page))
                                        <a href="#" class="button tiny colour-white radius padding-5 margin-b0 margin-r-gutter" data-reveal-id="confirm-delete-modal-{{ $item->{$primaryColumn} }}">
                                            <i class="fi-adm fi-adm-delete colour-error line-height-0"></i><span class="visually-hidden">Delete</span>
                                        </a>
                                        <div id="confirm-delete-modal-{{ $item{$primaryColumn} }}" class="reveal-modal notice-modal" data-reveal aria-hidden="true" role="dialog">
                                            <div class="modal-content">
                                                <p class="modal-heading padding-r40 font-weight-600 margin-b-gutter">Delete this page?</p>
                                                <p class="font-size-14">Would you like to delete this page?</p>
                                            </div>
                                            <div class="modal-actions">
                                                <a href="{{ route('adm.path', [$table, 'destroy', $item->{$primaryColumn}]) }}" class="button round gradient bordered colour-error margin-b0 margin-r-gutter">Delete</a>
                                                <a href="#" data-close-reveal class="button round bordered colour-cancel margin-b0">Cancel</a>
                                            </div>
                                            <a class="close-reveal-modal visually-hidden" aria-label="Close">&#215;</a>
                                        </div>
                                    @endif
                                    --}}

                                    @if (in_array('copy', $actions) )
                                        @if ((!isset($item->system_page) || (isset($item->system_page) && !$item->system_page)) || \Auth::user()->userGroup->super_admin)
                                            <a href="{{ route('adm.path', [$table, 'copy', $item->{$primaryColumn}]) }}" class="button tiny round bordered colour-cancel text-shadow-none margin-b0 margin-l-gutter-full padding-x15">{{ $button_labels['copy'] }}</a>
                                        @else
                                            <span class="button disabled tiny round bordered colour-cancel text-shadow-none margin-b0 margin-l-gutter-full padding-x15" style="opacity: .3 !important;">{{ $button_labels['copy'] }}</span>
                                        @endif
                                    @endif

                                    @if (in_array('edit', $actions))
                                        <?php
                                            $editParams = [$table, 'edit', $item->$primaryColumn];
                                            if( isset($item->language_id) && $item->language_id != $defaultLanguage ){
                                                $editParams[] = $item->locale;
                                            }
                                        ?>
                                        <a href="{{ route('adm.path', $editParams) }}" class="button tiny round bordered colour-cancel text-shadow-none margin-b0 margin-l-gutter padding-x-gutter-full">{{ $button_labels['edit'] }}</a>
                                    @endif

                                </td>
                            </tr>
                            @endforeach

                        @endif

                        </tbody>
                    </table>

                    @if( isset($pagination) && !empty($pagination) )
                        <?php $pages_per_side = floor( $pagination['visible_pages'] / 2 ); ?>
                        <div class="table width-100 font-size-14 padding-y-gutter padding-x-gutter-full">

                            <div class="results-text table-cell vertical-top margin-b-gutter">
                                <span class="block padding-y5">
                                    Showing {{ $pagination['from'] }}-{{ $pagination['to'] }} of {{ $pagination['total'] }} results
                                    @if( $pagination['num_pages'] > 1 )
                                        <span class="inline-block font-size-10">({{ $pagination['num_pages'] }})</span></span>
                                @endif
                            </div>

                            @if( $pagination['num_pages'] > 1 )

                                <div class="page-links table-cell vertical-top medium-text-right">

                                    <span class="block">

                                        @if( $pagination['show_prev'] )

                                            <span class="">

                                                @if( ($pagination['current_page'] - $pages_per_side) > 1 )

                                                    <a href="{{ $pagination['url_base'] }}" title="First Page" class="button colour-white inline-block vertical-middle font-size-14 padding-x-gutter padding-y5 margin-0">First</a>
                                                @endif

                                                <a href="{{ ($pagination['current_page']-1) == 1 ? $pagination['url_base'] : sprintf( $pagination['url_structure'], $pagination['current_page']-1 ) }}" title="Previous Page" class="button colour-white inline-block vertical-middle font-size-14 padding-x-gutter padding-y5 margin-0">Previous</a>

                                            </span>

                                        @endif

                                        <span class="pages">

                                            @for( $page = 1; $page <= $pagination['num_pages']; $page++ )

                                                @if( $page >= ($pagination['current_page'] - $pages_per_side) && $page <= ($pagination['current_page'] + $pages_per_side) )

                                                    @if( $page == $pagination['current_page'] )

                                                        <span class="padding-x-gutter font-weight-700 inline-block vertical-middle font-size-14 padding-y5">{{ $page }}</span>

                                                    @else

                                                        <a href="{{ sprintf( $pagination['url_structure'], $page ) }}" title="Go to Page {{ $page }}" class="button colour-white inline-block vertical-middle font-size-14 padding-x-gutter padding-y5 margin-0">{{ $page }}</a>

                                                    @endif

                                                @endif

                                            @endfor

                                        </span>

                                        @if( $pagination['show_next'] )

                                            <span class="">

                                                <a href="{{ sprintf( $pagination['url_structure'], $pagination['current_page']+1 ) }}" title="Next Page" class="button colour-white inline-block vertical-middle font-size-14 padding-x-gutter padding-y5 margin-0">Next</a>

                                                @if( ($pagination['current_page'] + $pages_per_side) < $pagination['num_pages'] )

                                                    <a href="{{ sprintf( $pagination['url_structure'], $pagination['num_pages'] ) }}" title="Last Page" class="button colour-white inline-block vertical-middle font-size-14 padding-x-gutter padding-y5 margin-0">Last</a>

                                                @endif

                                            </span>

                                        @endif

                                    </span>

                                </div>

                            @endif

                        </div>

                    @endif

                </div>

            </div>
        </div>

    </div>

    <?php // Dynamic modal for general messages ?>
    <div id="dynamic-modal" class="reveal-modal notice-modal" data-reveal aria-hidden="true" role="dialog">
        <div class="modal-content">
            <p data-dynamic-modal-title class="modal-heading padding-r40 font-weight-600 margin-b-gutter-full"></p>
            <div data-dynamic-modal-content class="font-size-14"></div>
        </div>
        <div class="modal-actions">
            <a href="#" data-confirm-button class="button round gradient bordered colour-error margin-b0 margin-r10">Confirm</a>
            <a href="#" data-close-reveal class="button round bordered colour-cancel margin-b0">Okay</a>
        </div>
    </div>

@stop

@push('footer_scripts')
<script>
var $dynamicModal;
$(document).ready(function () {

    $dynamicModal = $('#dynamic-modal');

    $('body').on('change', '.js-check-all', function() {

        if(this.checked) {

            var $notChecked = $('[name*="selected"]:not(:disabled)');

            $notChecked.prop('checked', true);
            $notChecked.closest('tr').addClass('selected-row');

        } else {

            var $checked = $('[name*="selected"]');

            $checked.prop('checked', false);
            $checked.closest('tr').removeClass('selected-row');

        }

    });

    // Show/hide the search and action forms
    $('body').on('change', '[name*="selected"]', function() {

        $(this).closest('tr').toggleClass('selected-row');

        // Toggle seleted item actions bar
        if ( $('[name*="selected"]:checked').length > 0 ) {
            $('.js-search-form').addClass('none');
            $('.js-action-form').removeClass('none');
        } else {
            $('.js-search-form').removeClass('none');
            $('.js-action-form').addClass('none');
        }

    });

    $('.js-cancel-delete').on('click touchend', function(e) {
        e.preventDefault();

        //$('#delete-modal').foundation('reveal', 'close'); // more simple alternative but requires having to update the ID

        var parentModal = $(this).closest('[data-reveal]');
        parentModal.find('.close-reveal-modal').click();
    });
    $('#delete-modal .js-delete-confirm').on('click touchend', function(){
        $('#delete-modal').foundation('reveal', 'close');
        bulk_delete();
    });

    $('.js-set-status').change( function(){
        $('#change-status-modal').foundation('reveal', 'open');
    });
    $('#change-status-modal .js-update-status-confirm').on('click touchend', function(){
        $('#change-status-modal').foundation('reveal', 'close');
        bulk_update_status( $('.js-set-status').val() );
    });

    // Show a clone of the dynamic modal, with a given title, content and buttons
    function getDynamicModal( options ){
        var $thisModal = $dynamicModal.clone();

        $thisModal.find('[data-dynamic-modal-title]').text( options.title );
        $thisModal.find('[data-dynamic-modal-content]').html( options.content );

        if (options.confirmButton) {
            $thisModal.find('[data-confirm-button]').text( options.confirmButton );
        } else {
            $thisModal.find('[data-confirm-button]').remove();
        }

        if (options.closeButton) {
            $thisModal.find('[data-close-reveal]').text( options.closeButton );
        }

        $(document).on('closed.fndtn.reveal', $thisModal, function () {
            $(this).remove();
        });

        return $thisModal;
    }

    function getSelected(){

        var selectedItems = [];

        // Push all selected items
        $('[name*="selected"]:checked').each(function() {

            // Push each selected id into an array
            var itemID = $(this).val();
            selectedItems.push(itemID);

        });

        return selectedItems;

    }

    function bulk_update_status( status ){
        $.ajax({
            url: '{{ route('adm.ajax', ['ajax_bulk_update_status']) }}',
            type: 'POST',
            data: {
                table: '{{ $table_name }}',
                items: getSelected(),
                status: status
            },
            dataType: 'json',
            beforeSend: function(){
                showPageLoadingOverlay( true );
            },
            success: function( json ){
                if( json.success ){
                    window.location.reload();
                } else {
                    var title = 'Update Status';
                    var content = '<p>Some {{ $title }} couldn\'t be updated. Please see the affected {{ $title }} below.</p>';
                    content += '<ul class="font-weight-600">';
                    $(json.non_updates).each( function(ind, val){
                        content += '<li>' + val + '</li>';
                    });
                    content += '</ul>';

                    var $modal = getDynamicModal({
                        title: title,
                        content: content
                    });
                    $('body').append($modal);
                    $modal.foundation('reveal', 'open');

                    $modal.find('[data-close-reveal]').off('click').click( function(){
                        window.location.reload();
                    });
                }
            },
            complete: function(){
                showPageLoadingOverlay( false );
            }
        });
    }

    function bulk_delete(){
        $.ajax({
            url: '{{ route('adm.ajax', ['ajax_bulk_delete']) }}',
            type: 'POST',
            data: {
                table: '{{ $table_name }}',
                items: getSelected()
            },
            dataType: 'json',
            beforeSend: function(){
                showPageLoadingOverlay( true );
            },
            success: function( json ){
                if( json.success ){
                    window.location.reload();
                } else {
                    var title = 'Delete {{ $title }}';
                    var content = '<p>Some {{ $title }} couldn\'t be deleted. Please see the affected {{ $title }} below.</p>';
                    content += '<ul class="font-weight-600">';
                    $(json.non_deletes).each( function(ind, val){
                        content += '<li>' + val + '</li>';
                    });
                    content += '</ul>';

                    var $modal = getDynamicModal({
                        title: title,
                        content: content
                    });
                    $('body').append($modal);
                    $modal.foundation('reveal', 'open');

                    $modal.find('[data-close-reveal]').off('click').click( function(){
                        window.location.reload();
                    });
                }
            },
            complete: function(){
                showPageLoadingOverlay( false );
            }
        });
    }

    $(document).on('change', 'select#show', function(){
        $(this).parents('form').first().submit();
    });

    $(document).on('click', '[data-export-btn]', function( evt ){
        evt.preventDefault();
        evt.stopPropagation();

        var url = $(this).attr('href');
        var selected = getSelected();

        if( selected ){
            url += '?';
            for( var i = 0; i < selected.length; i++ ){
                url += 'selected[]=' + encodeURIComponent( selected[i] ) + '&';
            }

            if($("input[name='search']" != "")) {
                url += '&search=' + encodeURIComponent($("input[name='search']").val());
            }
        }

        window.location.href = url;
    });


    /**
     * ADDITIONAL BULK ACTIONS
     */
    $(document).on( 'click', '[data-bulk-action]', function( evt ){
        evt.preventDefault();

        var $btn = $(this);

        var ajax_url = $btn.data('bulk-action');
        if( !ajax_url ){ return false; }

        $.ajax({
            url: '{{ route('adm.path') }}/' + ajax_url,
            type: 'POST',
            data: {
                items: getSelected()
            },
            dataType: 'json',
            beforeSend: function(){
                showPageLoadingOverlay( true );
            },
            success: function( json ){
                if( json.success ){
                    window.location.reload();
                } else {
                    var title = 'Bulk Action';
                    var content = '<p>Sorry, something went wrong when performing that action.</p>';
                    content += '<p class="colour-error">' + json.error + '</p>';

                    var $modal = getDynamicModal({
                        title: title,
                        content: content
                    });
                    $('body').append( $modal );
                    $modal.foundation( 'reveal', 'open' );

                    $modal.find('[data-close-reveal]').off('click').click( function(){
                        window.location.reload();
                    });
                }
            },
            complete: function(){
                showPageLoadingOverlay( false );
            }
        });
    });
});
</script>

@if( isset( $dynamic_scripts ) && !empty( $dynamic_scripts ) )
@foreach( $dynamic_scripts as $script )
{!! $script !!}
@endforeach
@endif
@endpush