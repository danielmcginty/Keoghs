@extends( 'layouts.adm' )

@section( 'page_title', $title )

@section( 'content' )

    <form action="" method="post" enctype="multipart/form-data" class="relative">

        {{ csrf_field() }}

        <div class="js-section-action-bar-container relative" style="height: 79px;">
            <div class="js-section-action-bar section-action-bar top-0 left-0 width-100">
                <div class="row padding-y-gutter-full">
                    <div class="column">
                        <div class="table width-100">
                            <div class="table-cell">
                                <h1 class="font-size-24 font-weight-600 margin-0">{{ $title }}</h1>
                            </div>
                            <div class="table-cell text-right">
                                <div class="inline-block vertical-middle">
                                    <button type="submit" class="button round gradient bordered colour-success margin-b0">
                                        <i class="fi-adm fi-adm-upload font-size-12 margin-r5 inline-block vertical-baseline"></i>
                                        <span class="inline-block vertical-baseline">Save</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-area margin-b-gutter-full">
            <div class="row">
                <div class="column small-9">

                    <div class="edit-main-content section-container background colour-white radius margin-b-gutter-full">

                        <div data-content-tabs="" class="edit-content-tabs">
                            <div class="table width-100">
                                <div class="table-cell width-100">
                                    <ul data-tab-group="edit-main-content" class="tabs simple-list inline-list font-size-0">
                                        <li class="">
                                            <a href="#" data-tab-id="content" title="CSV Settings" class="active font-size-14 block padding-x-gutter-full padding-y-gutter">CSV</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div data-tab-group="edit-main-content" class="tabs-content">
                            <div id="content" data-tab-id="content" class="content active">

                                <div class="input-row">
                                    <div>
                                        <label class="inline-block margin-b-gutter">
                                            Available Modules
                                            <span class="required">*</span>
                                        </label>
                                    </div>
                                    <div class="help-text margin-t-gutter-negative">Select the modules that should be made available on the website.</div>

                                    <div class="padding-b-gutter">
                                        <div data-modules-block>

                                            <div class="margin-b-gutter-full">
                                                <a data-select-all href="javascript:void(0)" class="button small round gradient colour-success margin-b0 margin-r-gutter"><span class="colour-white">Select All</span></a>
                                                <a data-deselect-all href="javascript:void(0)" class="button small round gradient colour-error margin-b0 margin-r-gutter"><span class="colour-white">Clear All</span></a>
                                            </div>

                                            @foreach( $all_modules as $module )
                                                <div class="margin-b-gutter">
                                                    <input data-module type="checkbox" id="{{ $module }}" name="modules[]" value="{{ $module }}" class="fancy-checkbox margin-0"{!! in_array( $module, $available_modules ) ? ' checked="checked"' : '' !!} />
                                                    <label for="{{ $module }}" class="font-size-16 fixed-font-size">
                                                        <span class="font-size-14 line-height-1pt2">{{ $module }}</span>
                                                    </label>
                                                </div>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>

    </form>

    @push( 'footer_scripts' )
    <script type="text/javascript">
        $(document).ready( function(){
            $('[data-modules-block] [data-select-all]').click( function() {
                $('[data-modules-block]').find('[data-module]').prop('checked', true).attr('checked', 'checked');
            });
            $('[data-modules-block] [data-deselect-all]').click( function() {
                $('[data-modules-block]').find('[data-module]').prop('checked', false).removeAttr('checked');
            });
        });
    </script>
    @endpush

@endsection