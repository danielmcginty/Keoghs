@extends('layouts.adm')

@section('page_title', $title)

@section('content')

    <form data-csv-import-form action="" method="POST" enctype="multipart/form-data" class="relative">

        {{ csrf_field() }}

        <div data-importer-loading-overlay class="fixed left-0 top-0 width-100 height-100 none" style="z-index:99">
            <div class="relative width-inherit height-inherit">
                <div class="background colour-white rgba-8 width-inherit height-inherit">
                    <div class="table width-100 height-100">
                        <div class="table-cell vertical-middle text-center colour-grey-dark">
                            <i class="fi fi-loading animated animation-infinite animation-duration-2s animation-linear spin-cw font-size-60"></i>
                            <span class="block font-size-28">Importing</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="js-section-action-bar-container relative" style="height: 79px;">
            <div class="js-section-action-bar section-action-bar top-0 left-0 width-100">
                <div class="row padding-y-gutter-full">
                    <div class="column">
                        <div class="table width-100">
                            <div class="table-cell">
                                <h1 class="font-size-24 font-weight-600 margin-0">{{ $title }}</h1>
                            </div>
                            <div class="table-cell text-right">
                                <div class="inline-block vertical-middle">
                                    <button type="submit" class="button round gradient bordered colour-success margin-b0">
                                        <i class="fi-adm fi-adm-upload font-size-12 margin-r5 inline-block vertical-baseline"></i>
                                        <span class="inline-block vertical-baseline">Import</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-area margin-b-gutter-full">
            <div class="row">
                <div class="column small-9">

                    <div class="edit-main-content section-container background colour-white radius margin-b-gutter-full">

                        <div data-content-tabs="" class="edit-content-tabs">
                            <div class="table width-100">
                                <div class="table-cell width-100">
                                    <ul data-tab-group="edit-main-content" class="tabs simple-list inline-list font-size-0">
                                        <li class="">
                                            <a href="#" data-tab-id="content" title="CSV Settings" class="active font-size-14 block padding-x-gutter-full padding-y-gutter">CSV</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div data-tab-group="edit-main-content" class="tabs-content">
                            <div id="content" data-tab-id="content" class="content active">

                                <div class="input-row">
                                    <div>
                                        <label for="csv-file" class="inline-block margin-b-gutter">CSV File<span class="required">*</span></label>
                                    </div>
                                    <div class="help-text margin-t-gutter-negative">Please select a CSV file containing the 301 redirects to import.</div>
                                    <div class="file-select-field relative radius margin-b-gutter-full">

                                        <label for="file-select-csv-import" class="visually-hidden">Selected File</label>
                                        <input data-file-select-hidden-input type="file" name="csv_file" class="visually-hidden" id="file-select-csv-import" />
                                        <label for="not-in-use" class="visually-hidden">Selected File</label>
                                        <input data-file-select-file-preview type="text" name="not-in-use" id="not-in-use" class="margin-0 border-0 radius-l0 background colour-white padding-r50" readonly="readonly" />
                                        <a href="#" data-file-select="file-select-csv-import" data-image-input-id="file-select-csv-import" class="button radius-l colour-default absolute top-0 left-0 margin-0">Select File</a>

                                    </div>
                                    @if( isset( $errors ) && count( $errors ) > 0 && $errors->has( 'csv_file' ) )
                                        <span class="inline-error">{{ $errors->first( 'csv_file' ) }}</span>
                                    @endif

                                </div>

                                <div class="input-row">
                                    <div>
                                        <label for="old-domain" class="inline-block margin-b-gutter">Old Domain</label>
                                    </div>
                                    <div class="help-text margin-t-gutter-negative">If the Old URLs contain the existing domain, e.g. http://www.mywebsite.com, please enter the domain here.</div>
                                    {!! Form::text('old_domain', null, ['id' => 'old-domain', 'class' => 'form-control', 'placeholder' => 'Old Domain']) !!}
                                </div>

                            </div>
                        </div>

                    </div>

                </div>
                <div class="column small-3">

                </div>
            </div>
        </div>

    </form>

    @push( 'footer_scripts' )
    <script type="text/javascript">
        $(document).ready( function(){
            $('[data-file-select]').click( function(evt){
                evt.preventDefault();

                $('[data-file-select-hidden-input]').click();
            });

            $('[data-file-select-hidden-input]').change( function(evt){
                evt.preventDefault();

                var files = this.files;
                if( !files ){
                    // IE9 Fix
                    // As IE9 only supports single file uploads, we just need to pass the single file into the array
                    files = [];
                    files.push({
                        name: this.value.substring(this.value.lastIndexOf("\\")+1),
                        size: 0, // Not possible to get this
                        type: this.value.substring(this.value.lastIndexOf(".")+1)
                    });
                }

                if( typeof( files[0] ) == "undefined" ){ return false; }

                var selected_file = files[0];
                $('[data-file-select-file-preview]').val( selected_file.name );
            });

            $('[data-csv-import-form]').on( 'submit', function(){
                $('[data-importer-loading-overlay]').removeClass( 'none' );
            });
        });
    </script>
    @endpush
@endsection