@section('page_title', 'Reset Password')

@include('includes.meta')
@include('includes.header')

<div class="login-contents table width-100 height-100">
    <div class="table-cell vertical-middle text-center">

        <div class="logo margin-b20">
            <a href="/adm" title="Admin Login" class="inline-block hover-fade fade-8">

            </a>
        </div>

        <div class="margin-t30 margin-b20">
            <div style="width: 450px;" class="login-form-container inline-block padding-40 radius background colour-black rgba-1 text-left">

                @if (session('status'))
                    <div>
                        <span class="inline-block background colour-success radius padding-x10 padding-y5 font-size-14 margin-b-gutter-full">
                            {{ session('status') }}
                        </span>
                    </div>
                @endif

                <form class="form-horizontal" role="form" method="POST" action="{{ route('adm.reset-password') }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                        <label for="email" class="colour-white">Confirm your Email Address</label>
                        <input id="email" type="email" class="width-100" name="email" value="{{ $email or old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                            <span class="inline-error colour-error font-size-14">{{ $errors->first('email') }}</span>
                        @endif

                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                        <label for="password" class="colour-white">New Password</label>
                        <input id="password" type="password" class="width-100" name="password" required>

                        @if ($errors->has('password'))
                            <span class="inline-error colour-error font-size-14">{{ $errors->first('password') }}</span>
                        @endif

                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">

                        <label for="password-confirm" class="colour-white">Confirm your New Password</label>
                        <input id="password-confirm" type="password" class="width-100" name="password_confirmation" required>

                        @if ($errors->has('password_confirmation'))
                            <span class="inline-error colour-error font-size-14">{{ $errors->first('password_confirmation') }}
                        @endif

                    </div>

                    <button type="submit" class="background colour-white rgba-3 round margin-0">
                        <span class="colour-white">Reset Password</span>
                    </button>

                </form>
            </div>
        </div>

        <a href="https://bespokedigital.agency/" target="_blank" class="inline-block" title="System by Bespoke | Digital Agency">
            <i class="sprite-bespoke-icon"></i>
        </a>

    </div>
</div>

@include('includes.footer')
