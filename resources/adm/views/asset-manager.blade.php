@extends('layouts.adm')

@section('page_title', 'Asset Manager')

@section('content')

    <div data-asset-manager class="asset-manager relative">

        @include('asset-manager.ajax-asset-manager')

        <?php // AJAX Loading State ?>
        <div data-asset-manager-loading style="z-index: 99;" class="none absolute top-0 left-0 width-100 height-100 text-center background colour-white rgba-5 line-height-1">
            <i class="fixed top-50 left-50 fi-adm fi-adm-loading fi-spin font-size-32 colour-grey"></i>
        </div>

    </div>

    <?php // Custom Right Click Option Menu ?>
    <div data-option-menu class="option-menu none">
        <ul data-actions-list="folder" class="simple-list margin-0 none">
            <li><a href="#" data-menu-action="folder-open" class="block button colour-white margin-0 text-left padding-x-gutter-full padding-y-gutter">Open Folder</a></li>
            <li><a href="#" data-menu-action="folder-rename" class="block button colour-white margin-0 text-left padding-x-gutter-full padding-y-gutter">Rename Folder</a></li>
            <li><a href="#" data-menu-action="folder-delete" class="block button colour-white margin-0 text-left padding-x-gutter-full padding-y-gutter"><span class="colour-error">Delete Folder</span></a></li>
        </ul>
        <ul data-actions-list="file" class="simple-list margin-0 none">
            <li><a href="#" data-menu-action="asset-edit" class="block button colour-white margin-0 text-left padding-x-gutter-full padding-y-gutter">Edit File</a></li>
            <li><a href="#" data-menu-action="asset-view" class="block button colour-white margin-0 text-left padding-x-gutter-full padding-y-gutter">View File</a></li>
            <li><a href="#" data-menu-action="asset-delete" class="block button colour-white margin-0 text-left padding-x-gutter-full padding-y-gutter"><span class="colour-error">Delete File</span></a></li>
        </ul>
        <ul data-actions-list="multiple-files" class="simple-list margin-0 none">
            <li><a href="#" data-menu-action="multiple-asset-delete" class="block button colour-white margin-0 text-left padding-x-gutter-full padding-y-gutter"><span class="colour-error">Delete Files</span></a></li>
        </ul>
    </div>

    <div id="confirm-delete-files-modal" class="reveal-modal notice-modal" data-reveal aria-hidden="true" role="dialog">
        <!-- Form is loaded into this container by ajax -->
    </div>

    <div data-replace-file-modal class="reveal-modal notice-modal" data-reveal aria-hidden="true" role="dialog">
        {{ Form::open([ 'url' => route('adm.asset-manager.function', 'replace_file'), 'type' => 'post', 'files' => true, 'data-replace-file-form' => '' ]) }}

            <input data-file-id-field type="hidden" name="file_id" value="" />

            <div class="js-modal-form-flash top-notification relative top-0 left-0 width-100 text-center">
                <div class="alert-box alert alert-dismissable margin-b0 bottom-0" style="display: none;"></div>
                <div class="alert-box success alert-dismissable margin-b0 bottom-0" style="display: none;"></div>
            </div>

            <div class="modal-content">
                <p class="modal-heading padding-r40 font-weight-600 margin-b10">Replace <span data-file-name></span></p>

                <?php $column = array( 'field' => 'asset', 'label' => 'Select File', 'required' => true, 'help' => 'Pick a file to upload' ); ?>
                <div data-image-field class="input-field{{ count($errors) > 0 && $errors->has($column['field']) ? ' has-error' : '' }}">
                    @include('fields.form.label')
                    @include('fields.form.help_text')
                    {{ Form::file('asset[]', [ 'data-asset-upload-file-input' => '', 'multiple' => '']) }}
                    @include('fields.form.error')
                </div>

            </div>

            <div class="modal-actions">
                <button type="submit" class="button round gradient bordered colour-success margin-b0 margin-r10 outline-none"><span>Upload</span></button>
                <a href="#" data-close-reveal class="button round bordered colour-cancel margin-b0">Cancel</a>
            </div>

        {{ Form::close() }}
    </div>

    <?php // Dynamic modal for general messages ?>
    <div id="dynamic-asset-manager-modal" class="reveal-modal notice-modal" data-reveal aria-hidden="true" role="dialog">
        <div class="modal-content">
            <p data-dynamic-modal-title class="modal-heading padding-r40 font-weight-600 margin-b-gutter-full"></p>
            <div data-dynamic-modal-content class="font-size-14"></div>
        </div>
        <div class="modal-actions">
            <a href="#" data-confirm-button class="button round gradient bordered colour-error margin-b0 margin-r10">Confirm</a>
            <a href="#" data-close-reveal class="button round bordered colour-cancel margin-b0">Okay</a>
        </div>
    </div>

@stop

@push('footer_scripts')

<script src="/admin/dist/assets/js/vendor/jquery.mjs.nestedSortable.js"></script>
<script src="/admin/dist/assets/js/vendor/dropzone.min.js"></script>

<script>

    // jQuery object for the directory list container (underneath site assets)
    var $directoryListing = $('[data-directory-listing]');

    // jQuery object for the file list container
    var $filesListing = $('[data-files-listing]');

    // jQuery object for the 'Showing X files' counter
    var $filesCount = $('[data-items-count]');

    // jQuery object for the delete folder action container
    var $deleteActionArea = $('[data-delete-folder-action]');

    // jQuery object for the new folder modal container
    var $newFolderModal = $('[data-new-folder-modal]');

    // jQuery object for the new folder modal container
    var $renameFolderModal = $('[data-rename-folder-modal]');

    // jQuery object for the upload modal container
    var $uploadModal = $('[data-upload-modal]');

    // jQuery object for the dynamic asset manager modal
    var $dynamicModal = $('#dynamic-asset-manager-modal');

    Dropzone.autoDiscover = false;

    $(document).ready( function(){

        /**
         * DRAG AND DROP UPLOADS (Dropzone)
         */

        var drop_error = false;
        var dropzone_overlay = $('[data-dropzone-overlay]');
        var dropzone_text = $('[data-dropzone-text]');
        var dropzone_error = $('[data-dropzone-error]');
        var dropzone_close = $('[data-dropzone-close]');

        var page_dropzone = new Dropzone(
            "#body",
            {
                url: '/adm/asset-manager/upload',
                paramName: 'asset',
                maxFilesize: 2, // max 2mb
                parallelUploads: 5,
                uploadMultiple: true,
                clickable: false,
                previewsContainer: '.previews-container',
                init: function() {

                    // Send form data along with the files
                    this.on("sendingmultiple", function(data, xhr, formData) {

                        var current_dir = $('#upload-modal input[name="directory_id"]').val();

                        formData.append("directory_id", current_dir);
                        formData.append("drag_and_drop", true);

                    });

                }
            }
        );

        // Show screen overlay on drag
        page_dropzone.on("dragenter", function(e) {

            // Make sure we're dragging a file (not selecting on the page)
            var dt = ( e && e.dataTransfer );
            var isFile = ( dt.types && (dt.types.indexOf ? dt.types.indexOf('Files') != -1 : dt.types.contains('Files')) );

            if ( isFile ) {
                dropzone_overlay.addClass('show');
            }

        });

        // Hide overlay when dragging out
        page_dropzone.on("dragleave", function(e) {

            // If outside the screen
            if ( e && ( e.pageX <= 0 || e.pageY <= 0) ) {
                dropzone_overlay.removeClass('show');
            }

        });

        // Handle drop
        page_dropzone.on("drop", function() {

            // Empty container
            $('.previews-container').html('');

            // Reset states
            resetDropzoneContent();
            dropzone_text.hide();

        });

        // Handle errors
        page_dropzone.on("error", function(file, response) {

            console.log('Error: ' + response);

            drop_error = true;
            dropzone_error.show();

        });

        // Handle complete
        page_dropzone.on("queuecomplete", function() {

            // Refresh file list
            reloadFiles( $('#upload-modal input[name="directory_id"]').val() );

            if (!drop_error){

                // Hide overlay
                setTimeout(function(){
                    hideDropzoneOverlay();
                }, 1500);

            } else {

                dropzone_close.show();

            }

        });

        $(document).on('click', '[data-dropzone-close]', function(e){
            e.preventDefault();
            hideDropzoneOverlay();
        });

        function resetDropzoneContent(){

            // Reset content
            drop_error = false;
            dropzone_error.hide();
            dropzone_close.hide();
            dropzone_text.show();

            // Reset previews
            $('.previews-container').html('');

        }

        function hideDropzoneOverlay(){
            dropzone_overlay.fadeOut(500, function(){

                // Reset overlay
                dropzone_overlay.removeClass('show');
                resetDropzoneContent();

            });
        }


        /**
         * OPEN UPLOAD FILE EXPLORER
         * Trigger the native file explorer when clicking the upload file button
         */
        $(document).on('click', '[data-upload-file-button]', function( evt ){
            evt.preventDefault();

            // Open native file explorer
            $('[data-reveal]#upload-modal').find('[data-asset-upload-file-input]').click();

        });

        /**
         * UPLOAD MODAL PRE-POPULATE NAME
         * Pre-populate the 'Name' field with a cleaned up version of the file name.
         * If multiple files have been selected hide the 'name' and 'alt text' field, else show them.
         */
        $(document).on('change', '[data-asset-upload-file-input]', function( evt ){
            var $singleAssetFields = $('[data-reveal]#upload-modal [data-asset-upload-single-fields]');

            $('[data-reveal]#upload-modal .alert-box').hide();
            $('[data-reveal]#upload-modal .inline-error').remove();

            var files = this.files;
            if( !files ){
                // IE9 Fix
                // As IE9 only supports single image uploads, we just need to pass the single file into the array
                files = [];
                files.push({
                    name: this.value.substring(this.value.lastIndexOf("\\")+1),
                    size: 0, // it's not possible to get file size without flash or similar
                    type: this.value.substring(this.value.lastIndexOf(".")+1)
                });
            }
            if( files.length > 1 ){

                if( !$singleAssetFields.hasClass("none") ){ $singleAssetFields.addClass("none"); }
                $('[data-asset-upload-name-input]').val('').removeAttr('required');

            } else {

                $singleAssetFields.removeClass("none");

                var fullPath = this.value;

                if (fullPath) {
                    var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                    var filename = fullPath.substring(startIndex);
                    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                        filename = filename.substring(1);
                    }

                    var dotIndex = filename.lastIndexOf('.');
                    filename = filename.substring(0, dotIndex);

                    var path_replace_regex = /[^a-zA-Z0-9-]/g;
                    var whitespace_replace_regex = /[\s]/g;
                    var double_dash_replace_regex = /-[-]+/g;

                    filename = filename.replace(whitespace_replace_regex, '-'); // Replace any white space with a dash
                    filename = filename.replace(double_dash_replace_regex, '-'); // Replace any instances of 2 hyphens with 1 (----- to -)
                    filename = filename.replace(path_replace_regex, ''); // Replace any invalid characters with nothing
                    filename = filename.replace(/-+$/, ''); // Replace any trailing hyphens from the URL
                    filename = filename.replace(/-/g, ' '); // Replace any dashes with a space

                    // Now uc_words the filename
                    filename = (filename + '').replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function ($1) {
                        return $1.toUpperCase()
                    });

                    $('[data-asset-upload-name-input]').val(filename).attr('required', 'required');
                }

            }
        });

        /**
         * LIST CHECKBOXES
         * Toggle selected classes, select/de-select all
         */

        // Check all checkboxes with top option
        $('body').on('change', '[data-select-all-checkboxes]', function() {

            var $checkboxes = $('[name="selected-item"]');

            if(this.checked) {
                $checkboxes.prop('checked', true).trigger('change');
            } else {
                $checkboxes.prop('checked', false).trigger('change');
            }

        });

        // Select all button
        $(document).on( 'click', '[data-select-all-button]', function(e) {
            e.preventDefault();

            $('[data-select-all-checkboxes]').prop('checked', true).trigger('change');

        });

        // De-select all button
        $(document).on( 'click', '[data-deselect-all-button]', function(e) {
            e.preventDefault();

            $('[data-select-all-checkboxes]').prop('checked', false).trigger('change');

        });

        // Toggle checkbox when clicking select box table cell
        $(document).on( 'click', '[data-asset-checkbox]', function(e) {
            e.preventDefault();

            var checkbox = $(this).find('input[type="checkbox"]');
            var checkboxProp = checkbox.prop('checked');

            if (checkboxProp == true){
                checkbox.prop('checked', false);
            } else {
                checkbox.prop('checked', true);
            }

            checkbox.trigger('change');

        });

        // Show/hide the action buttons / select when selecting items
        var selectedItems;
        $(document).on('change', '[name="selected-item"]', function() {
            // Count selected items
            selectedItems = $('[name="selected-item"]:checked').length;
            $('[data-selected-items-count]').html(selectedItems);

            // Show/Hide actions if items are selected
            if ( selectedItems > 0 ) {
                $('[data-file-actions]').removeClass('is-hidden');
            } else {
                $('[data-file-actions]').addClass('is-hidden');
            }

            // Add class if multiple items are selected
            if ( selectedItems > 1 ) {
                $('[data-asset-manager]').addClass('multiple-selected');
            } else {
                $('[data-asset-manager]').removeClass('multiple-selected');
            }

            // Toggle highlight on parent row
            if ($(this).prop('checked') == true){
                $(this).parents('tr').addClass('selected-row').attr('data-selected', '');
            } else {
                $(this).parents('tr').removeClass('selected-row').removeAttr('data-selected');
            }

        });


        /**
         * "MOVE TO..." SELECT BOX
         * Move selected options to chosen option is select
         */
        $(document).on('change', 'select[data-move-select]', function() {

            var destinationFolder = $("select[data-move-select] option:selected");

            var destinationFolderID = destinationFolder.val();

            var destinationFolderPath = destinationFolder.data('directory-path');

            var $confirmMoveAssetsModal = getDynamicModal({
                title: 'Move selected assets to...',
                content: '<p class="font-size-14">Are you sure you wish to move the selected files into "<strong>/' + destinationFolderPath + '</strong>"</p>',
                confirmButton: 'Move Assets',
                closeButton: 'Cancel'
            });
            $('body').append( $confirmMoveAssetsModal );
            $confirmMoveAssetsModal.foundation('reveal', 'open');

            $confirmMoveAssetsModal.find('[data-confirm-button]').on('click', function() {
                $('select[data-move-select] option').removeAttr('selected');
                $('select[data-move-select] option[disabled]').attr('selected', 'selected');
                move_files(getSelected(), destinationFolderID);
                $confirmMoveAssetsModal.foundation('reveal', 'close');
            });

        });

        $(document).on('click', '[data-delete-selected-assets]', function(){

            $('#confirm-delete-files-modal').foundation('reveal', 'open', {
                url: '{{ route('adm.asset-manager.function', ['delete_files']) }}',
                data: {
                    files: getSelected(),
                    get_form: true
                },
                method: 'post'
            });

        });


        /**
         * GET SELECTED ASSETS
         * Get all selected assets on screen
         */
        function getSelected(){

            var selectedAssets = [];

            // Push all selected items
            $("[data-asset-manager] tr[data-selected]").each(function() {

                // Push each selected id into an array
                var assetID = $(this).data('asset-id');
                selectedAssets.push(assetID);

            });

            return selectedAssets;

        }


        /**
         * UPDATED SELECTED ASSETS (STYLE)
         * Force trigger change on selected assets (e.g. checked before js loads)
         */
        function triggerChangeSelected(){

            $('[name="selected-item"]').trigger('change');

        }

        $(window).load(function() {

            // Trigger change on all selected table rows
            triggerChangeSelected();

        });


        /**
         * TOGGLE CHILD FOLDERS
         * Show/hide child items in nested folders
         */
        $(document).on('click', '[data-toggle-child-folders]', function() {

            var parentItem = $(this).closest('li');
            parentItem.toggleClass('is-expanded');

        });


        <?php /*

         // EXPAND CHILD FOLDERS ON HOVER
         // Open collapsed folders on hover if dragging asset

        // This works, but has a bug where the droppable script 'drop zones' are not updated with the change in content

        var folderOpenOnHover;

        $('[data-directory-listing] li').on('mouseover', function() {

            var folder = $(this);

            // If the user is dragging an asset...
            if (isDraggingAsset == true && folder.hasClass('mjs-nestedSortable-collapsed')){

                // Set timeout to auto-open the hovered folder
                folderOpenOnHover = setTimeout(function(){

                    // Expand sub-folders
                    folder.removeClass('mjs-nestedSortable-collapsed');
                    folder.addClass('mjs-nestedSortable-expanded');

                }, expandOnHoverDelay);

            }

        // On mouseout
        }).mouseout(function() {

            // Clear the auto open timeout
            clearTimeout(folderOpenOnHover);

        });

        */ ?>



        /**
         * Remove duplicates in an array
         * Source: http://stackoverflow.com/a/12551709
         */
        function removeDuplicates(array) {
            var result = [];
            $.each(array, function(i, e) {
                if ($.inArray(e, result) == -1) result.push(e);
            });
            return result;
        }


        /**
         * Asset Manager Search
         */
        $(document).on('click', '[data-asset-manager-search-btn]', assetManagerSearch);
        $(document).on('keyup', '[data-asset-manager-search-input]', function(evt){
            if( evt.which == 13 ){
                evt.preventDefault();
                evt.stopPropagation();

                assetManagerSearch();
                return false;
            }
        });

        function assetManagerSearch(){
            var searchTerm = $('[data-asset-manager-search-input]').val();
            if( searchTerm.length > 0 ){
                $.ajax({
                    url: '{{ route('adm.asset-manager.function', ['search']) }}',
                    type: 'POST',
                    data: {
                        images_only: false,
                        search: searchTerm
                    },
                    dataType: 'json',
                    beforeSend: function(){
                        showPageLoadingOverlay( true );
                    },
                    success: reloadResponseHandler,
                    complete: function(){
                        $('[data-directory-listing] li').removeClass('current');
                        isSiteAssetsCurrent(0);
                        showPageLoadingOverlay();
                    }
                });
            }
        }


        /**
         * DRAGGABLE ASSETS
         * Allow asset table rows to be dragged
         * Source: http://api.jqueryui.com/draggable/
         */
        var isDraggingAsset,
                draggingAsset,
                draggingAssetID;

        var draggableAssetsOptions = {
            helper: "clone",    // Create copy to style when dragging
            revert: true,       // Animate back to original position if not dropped
            cancel: "input,.button,.list-table-checkbox-col",
            cursorAt: {
                left: -10,
                top: -10
            },
            start: function(event, ui) {
                //console.log('dragging asset');

                // Set dragging state
                isDraggingAsset = true;

                // Store dragged asset
                draggingAsset = $(this);

                // Enable revert (if previously disabled by drop)
                $(this).draggable("option", "revert", true);

                // Apply dragged from state
                $(this).addClass("ui-dragging-from");

                // MULTIPLE ITEMS SELECTED
                if (selectedItems > 1){

                    // Get all selected item id's
                    draggingAssetID = [];

                    // Push dragged item
                    draggingAssetID.push($(this).data('asset-id'));

                    // Push all selected items
                    $("[data-asset-manager] tr[data-selected]").each(function() {

                        // Push each selected id into an array
                        var assetID = $(this).data('asset-id');
                        draggingAssetID.push(assetID);

                        // Fade each item selected
                        $(this).addClass("ui-dragging-from");

                    });

                    // Remove duplicates (cloned drag element, dragging an already selected item)
                    draggingAssetID = removeDuplicates(draggingAssetID);

                    // ONE ITEM SELECTED
                } else {

                    // Get currently dragging item
                    draggingAssetID = draggingAsset.data('asset-id');

                }

            },
            stop: function(event, ui) {
                //console.log('stop dragging asset');

                // Set dragging state
                isDraggingAsset = false;

                // Reset dragged-from classes (faded out)
                $('[data-asset-manager] tr.ui-draggable').removeClass("ui-dragging-from");

            }
        };


        /**
         * DRAGGABLE FOLDERS
         * Allow folders to be dragged and re-ordered
         * Source: http://api.jqueryui.com/draggable/
         */
        var isDraggingFolder,
                draggingFolder,
                draggingFolderID;

        var draggableFoldersOptions = {
            helper: "clone",    // Create copy to style when dragging
            revert: true,       // Animate back to original position if not dropped
            cancel: ".folder-toggle",         // Cannot drag from these areas
            cursorAt: {
                left: -10,
                top: -10
            },
            start: function(event, ui) {
                //console.log('dragging folder');

                // Set dragging folder state
                isDraggingFolder = true;

                // Store dragging item
                draggingFolder = $(this);

                // Enable revert (if previously disabled by drop)
                $(this).draggable("option", "revert", true);

                // Apply dragged from state
                $(this).addClass("ui-dragging-from");

                // Get currently dragging item
                draggingFolderID = draggingFolder.data('folder-id');

            },
            stop: function(event, ui) {
                //console.log('stop dragging folder');

                // Set dragging state
                isDraggingFolder = false;

                // Reset dragged-from classes (faded out)
                $('[data-draggable-folder]').removeClass("ui-dragging-from");

            }
        };


        /**
         * DROPPABLE FOLDERS
         * Allow asset table rows to be dropped onto folders in the sidebar
         * Source: http://api.jqueryui.com/droppable/
         */

        var droppableFoldersOptions = {
            accept: "[data-draggable-asset], [data-draggable-folder]",
            greedy: true,
            tolerance: 'pointer',
            out: function( event, ui ) {

                //console.log('drag out');

            },
            drop: function( event, ui ) {

                // Get dropped folder ID
                var dropFolderID = $(this).data('folder-id');

                // If dragging folder
                if (isDraggingFolder) {

                    // Cancel revert of dragging item if dropped successfully
                    draggingFolder.draggable("option", "revert", false);

                    //console.log('Dropped folder ' + draggingFolderID + ' onto ' + dropFolderID);

                    // Move folder
                    move_folders(draggingFolderID, dropFolderID);

                    // If dragging asset
                } else if (isDraggingAsset){

                    // Cancel revert of dragging item if dropped successfully
                    draggingAsset.draggable("option", "revert", false);

                    //console.log('Dropped asset ' + draggingAssetID + ' onto ' + dropFolderID);

                    // Move asset
                    move_files(draggingAssetID, dropFolderID);

                }


            }
        };


        /**
         * INIT ASSET MANAGER
         * Custom Right Click Content Menu
         */

        function refreshFolderList(){

            // Init Folder List (Draggable/Droppable)
            var draggableFolders = $("[data-draggable-folder]").draggable(draggableFoldersOptions);
            var droppableFolders = $("[data-droppable-folder]:not(.current)").droppable(droppableFoldersOptions);

        }

        function refreshAssetList(){

            // Init Asset List (Draggable)
            var draggableAssets = $("[data-draggable-asset]").draggable(draggableAssetsOptions);

        }

        function initAssetManager(){

            refreshAssetList();
            refreshFolderList();

        }

        initAssetManager();


        /**
         * CUSTOM CONTEXT (RIGHT CLICK) ACTION MENUS
         * Custom Right Click Content Menu
         */

        var actionItemClicked; // Store item right-clicked on

        // Show custom menu when right clicking on
        $(document).on("contextmenu", "[data-action-menu]", function(e){

            var xOffset = 0;
            var yOffset = 0;
            var mouseX = e.pageX;
            var mouseY = e.pageY;
            var menu = $('[data-option-menu]');
            var menuType = $(this).data('action-menu');

            if( menuType == 'file' ){
                var selectedAssets = getSelected();
                if( selectedAssets.length > 1 ){
                    menuType = 'multiple-files';
                }
            }

            // Add action menu active state to clicked item
            actionItemClicked = $(this);
            actionItemClicked.addClass('has-action-menu-open');

            // Show correct actions list
            menu.find('[data-actions-list="' + menuType + '"]').removeClass('none');

            // Get window dimensions
            var windowWidth = $(window).width();
            var windowHeight = $(window).height();

            // Get menu width
            var menuWidth = menu.width();
            var menuHeight = menu.height();

            // Check if past halfway of window width, switch side menu is shown.
            if (mouseX > (windowWidth / 2)) {
                xOffset = menuWidth;
            }

            // Check if past halfway of window height, show menu above mouse.
            if (mouseY > (windowHeight - (menuHeight + 100))) {
                yOffset = menuHeight;
            }

            // Position option menu at mouse
            menu.css("left", mouseX - xOffset);
            menu.css("top", mouseY);

            // Show option menu
            menu.removeClass('none');

            // Stop browser contextmenu opening
            return false;

        });

        // On mouse up...
        $(document).mousedown(function (e) {
            var menu = $('[data-option-menu]');

            // If menu is open...
            if (!menu.hasClass('none')) {

                // If not clicking on menu...
                if (!menu.is(e.target) && menu.has(e.target).length === 0) {

                    // Close menu and lists
                    closeActionMenu();

                }

            }

        });

        function closeActionMenu(){

            var menu = $('[data-option-menu]');

            // Hide menu, close all lists
            menu.addClass('none');
            menu.find('[data-actions-list]').addClass('none');

            // Remove action menu active state to clicked item
            actionItemClicked.removeClass('has-action-menu-open');

        }

        /**
         * CONTEXT MENU ACTIONS
         * Custom Right Click Content Menu
         */
            // De-select all button
        $(document).on( 'click', '[data-menu-action]', function(e) {
            e.preventDefault();

            // Get action information
            var action = $(this).data('menu-action');
            var itemID = actionItemClicked.data('action-id');
            //console.log(action + ' on: ' + itemID);

            // Close menu and lists
            closeActionMenu();

            // Run correct action
            switch (action) {

                // Folder Actions
                case 'folder-open':
                    $('a[data-folder-id="' + itemID + '"]').trigger('click');
                    break;

                case 'folder-rename':
                    $('#rename-folder-modal').foundation('reveal', 'open', {
                        url: '{{ route('adm.asset-manager.function', ['rename_folder']) }}',
                        data: {
                            directory_id: itemID,
                            get_form: true
                        },
                        method: 'post'
                    });
                    break;

                case 'folder-delete':
                    $('#confirm-delete-folder-modal').foundation('reveal', 'open', {
                        url: '{{ route('adm.asset-manager.function', ['delete_folder']) }}',
                        data: {
                            directory_id: itemID,
                            get_form: true
                        },
                        method: 'post'
                    });
                    break;

                // Asset Actions
                case 'asset-edit':
                    $('a[data-reveal-id="view-file-modal-' + itemID + '"]').trigger('click');
                    break;

                case 'asset-view':
                    var fileUrl = $('a[data-file-url-' + itemID).data('file-url-' + itemID);
                    window.open(
                        fileUrl,
                        '_blank' // Open in a new tab
                    );
                    break;

                case 'asset-delete':
                    $('#confirm-delete-files-modal').foundation('reveal', 'open', {
                        url: '{{ route('adm.asset-manager.function', ['delete_files']) }}',
                        data: {
                            files: [itemID],
                            get_form: true
                        },
                        method: 'post'
                    });
                    break;

                case 'multiple-asset-delete':
                    $('[data-delete-selected-assets]').click();
                    break;
            }

        });


        $(document).on('click', '.js-delete-file-from-edit-modal', function(evt) {

            evt.preventDefault();

            var file_id = $(this).data('file-id');

            $('#confirm-delete-files-modal').foundation('reveal', 'open', {
                url: '{{ route('adm.asset-manager.function', ['delete_files']) }}',
                data: {
                    files: [file_id],
                    get_form: true
                },
                method: 'post'
            });
        });


        $(document).on('submit', '.js-modal-delete-files-form', function( evt ){

            evt.preventDefault();
            evt.stopPropagation();

            var $form = $(this);
            var $form_modal = $form.parents('.reveal-modal').first();

            if ($form.data('submitted') === true) {
                // Previously submitted - don't submit again
                return false;
            } else {
                // Mark it so that the next submit can be ignored
                $form.data('submitted', true);
            }

            $.ajax({
                url: $form.attr('action'),
                type: "post",
                data: $form.serialize(),
                dataType: "json",
                beforeSend: function(){
                    $form.find('.js-modal-form-flash .alert-box').text('').hide();
                    showModalLoadingOverlay($form_modal, true);
                },
                success: function( json ){

                    if ( json.files_in_use ) {
                        // If files are in use, warn the user in a separate modal

                        var content = '';
                        content += '<p class="font-size-14">Some assets are being used.<br />Deleting them may <strong>break images or links to files</strong> on the site.</p>';
                        content += '<span class="block">Assets in use:</span>';
                        content += '<ul class="font-weight-600">';
                        $(json.files_in_use).each(function (ind, val) {
                            content += '<li>' + val + '</li>';
                        });
                        content += '</ul>';
                        content += '<p class="font-size-14">Are you sure you wish to delete the file(s)? <strong>This cannot be undone</strong>.</p>';

                        var $filesInUseModal = getDynamicModal({
                            title: 'Asset in Use',
                            content: content,
                            confirmButton: 'Delete',
                            closeButton: 'Cancel'
                        });
                        $('body').append($filesInUseModal);
                        $filesInUseModal.foundation('reveal', 'open');

                        $(document).on('closed.fndtn.reveal', $filesInUseModal, function () {
                            $form_modal.foundation('reveal', 'close');
                        });

                        $filesInUseModal.find('[data-confirm-button]').off('click');
                        $filesInUseModal.find('[data-confirm-button]').on('click', function() {
                            $form.data('submitted', false);

                            // The next time the form is submitted, don't check for file usage - just delete files
                            $form.find('[name="check_file_usage"]').remove();

                            $form.submit();

                            $filesInUseModal.foundation('reveal', 'close');
                        });
                    }

                    if( json.success ){

                        if (json.flash_notification) {
                            $('.admin-container .top-notification').first().remove();
                            $('.admin-container').prepend(json.flash_notification);

                            $('#folder-modal').foundation('reveal', 'close');

                            // make sure the newly added alert prompts can be removed
                            $(document).foundation('alert', 'reflow');
                        }

                        $form_modal.foundation('reveal', 'close');
                        $('.reveal-modal-bg').remove();

                        $('.view-asset-modal.open').foundation('reveal', 'close');
                        $('.view-asset-modal.open').remove();

                        reloadDirectoriesAndFiles( json.parent );
                    }

                    if( json.error ) {
                        $form_modal.find('.js-modal-form-flash .alert').text( json.error ).show();
                    }
                },
                complete: function(){
                    $form.data('submitted', false);
                    showModalLoadingOverlay($form_modal, false);
                }
            }, 'json');

        });


        $(document).on('click', '[data-replace-asset-from-edit-modal]', function( evt ){
            evt.preventDefault();

            var file_id = $(this).data('file-id');
            var name = $(this).data('name');

            $('[data-replace-file-modal] [data-file-name]').text( name );
            $('[data-replace-file-modal] [data-file-id-field]').val( file_id );
            $('[data-replace-file-modal]').foundation( 'reveal', 'open' );

            $('[data-replace-file-modal] [data-image-field] input').click();
        });


        $(document).on('submit', '[data-replace-file-form]', function( evt ){
            evt.preventDefault();
            evt.stopPropagation();

            var $form = $(this);

            if( typeof( FormData ) == "undefined" ){
                // IE9
            } else {
                var formData = new FormData( $form[0] );
                $.ajax({
                    url: $form.attr('action'),
                    type: $form.attr('method'),
                    data: formData,
                    dataType: 'json',
                    cache: false,
                    processData: false,
                    contentType: false,
                    beforeSend: function(){
                        showModalLoadingOverlay( $('[data-replace-file-modal]' ), true );
                    },
                    success: function( json ){
                        $form.find('.js-modal-form-flash .alert-box').hide();
                        $form.find('.inline-error').remove();

                        if( json.error ){
                            showModalLoadingOverlay( $('[data-replace-file-modal]' ), false);

                            if( json.field_errors ){
                                for( var field in json.field_errors ){
                                    $form.find('input[name="' + field + '"]').parent().append( '<span class="inline-error">' + json.field_errors[field][0] + '</span>' );
                                }
                            }

                            $form.find('.js-modal-form-flash .alert').text( json.error ).show();
                        }
                        else if( json.success ){
                            if (json.flash_notification) {
                                $('.admin-container .top-notification').first().remove();
                                $('.admin-container').prepend(json.flash_notification);

                                // make sure the newly added alert prompts can be removed
                                $(document).foundation('alert', 'reflow');
                            }

                            showModalLoadingOverlay( $('[data-replace-file-modal]' ), false );

                            $('#view-file-modal-' + $form.find('[name="file_id"]').val() ).foundation( 'reveal', 'close' );
                            $('[data-replace-file-modal]' ).foundation('reveal', 'close');
                            $('.reveal-modal-bg').remove();

                            reloadFiles( json.directory );
                        }
                    },
                    error: function(){
                        $form.find('.js-modal-form-flash .alert').text( 'An unknown error occurred, please try again' ).show();
                    }
                });
            }
        });


        $(document).on('click', '.js-delete-folder-button', function (evt) {

            evt.preventDefault();

            var directory_id = $(this).data('folder-id');

            $('#confirm-delete-folder-modal').foundation('reveal', 'open', {
                url: '{{ route('adm.asset-manager.function', ['delete_folder']) }}',
                data: {
                    directory_id: directory_id,
                    get_form: true
                },
                method: 'post'
            });
        });

        $(document).on('submit', '.js-modal-delete-folder-form', function( evt ){

            evt.preventDefault();
            evt.stopPropagation();

            var $form = $(this);
            var $form_modal = $form.parents('.reveal-modal').first();

            if ($form.data('submitted') === true) {
                // Previously submitted - don't submit again
                return false;
            } else {
                // Mark it so that the next submit can be ignored
                $form.data('submitted', true);
            }

            $.ajax({
                url: $form.attr('action'),
                type: "post",
                data: $form.serialize(),
                dataType: "json",
                beforeSend: function(){
                    $form.find('.js-modal-form-flash .alert-box').text('').hide();
                    showModalLoadingOverlay($form_modal, true);
                },
                success: function( json ){

                    if ( json.files_in_use ) {
                        // If files within this folder are in use, warn the user in a separate modal

                        var content = '';
                        content += '<p class="font-size-14">Some assets inside this folder are being used.<br />Deleting this folder may <strong>break images or links to files</strong> on the website.</p>';
                        content += '<span class="block">The assets being used are: </span>';
                        content += '<ul class="font-weight-600">';
                        $(json.files_in_use).each(function (ind, val) {
                            content += '<li>' + val + '</li>';
                        });
                        content += '</ul>';
                        content += '<p class="font-size-14">Are you sure you wish to delete the folder? <strong>This cannot be undone</strong>.</p>';

                        var $filesInUseModal = getDynamicModal({
                            title: 'Asset in Use',
                            content: content,
                            confirmButton: 'Delete',
                            closeButton: 'Cancel'
                        });
                        $('body').append($filesInUseModal);
                        $filesInUseModal.foundation('reveal', 'open');

                        $(document).on('closed.fndtn.reveal', $filesInUseModal, function () {
                            $form_modal.foundation('reveal', 'close');
                        });

                        $filesInUseModal.find('[data-confirm-button]').off('click');
                        $filesInUseModal.find('[data-confirm-button]').on('click', function() {
                            $form.data('submitted', false);

                            // The next time the form is submitted, don't check for file usage - just delete files
                            $form.find('[name="check_file_usage"]').remove();

                            $form.submit();

                            $filesInUseModal.foundation('reveal', 'close');
                        });
                    }

                    if( json.success ){

                        if (json.flash_notification) {
                            $('.admin-container .top-notification').first().remove();
                            $('.admin-container').prepend(json.flash_notification);

                            $('#folder-modal').foundation('reveal', 'close');

                            // make sure the newly added alert prompts can be removed
                            $(document).foundation('alert', 'reflow');
                        }

                        $form_modal.foundation('reveal', 'close');
                        $form_modal.remove();
                        reloadDirectoriesAndFiles( json.parent );
                    }

                    if( json.error ) {
                        $form_modal.find('.js-modal-form-flash .alert').text( json.error ).show();
                    }
                },
                complete: function(){
                    $form.data('submitted', false);
                    showModalLoadingOverlay($form_modal, false);
                }
            }, 'json');

        });

        // Click on a directory list link
        $(document).on('click', '[data-directory-link]', function(evt){

            evt.preventDefault();
            evt.stopPropagation();

            $('[data-asset-manager-search-input]').val('');
            reloadDirectoriesAndFiles( $(this).data('directory-id') );

        });

        // Submit the file edit form
        $(document).on('submit', '.js-file-edit-form', function(evt){
            var $form = $(this);

            evt.preventDefault();
            evt.stopPropagation();

            $.ajax({
                url: '{{ route('adm.asset-manager.function', ['function' => 'edit_file']) }}',
                type: 'post',
                data: $form.serialize(),
                dataType: 'json',
                beforeSend: function(){

                    showPageLoadingOverlay(true);

                },
                success: function( json ){

                    showPageLoadingOverlay(false);

                    $form.find('.js-modal-form-flash .alert-box').hide();
                    $form.find('.inline-error').remove();

                    if( json.error ){
                        if( json.field_errors ){
                            for( var field in json.field_errors ){
                                $form.find('input[name="' + field + '"]').parent().append( '<span class="inline-error">' + json.field_errors[field][0] + '</span>' );
                            }
                        }

                        if (json.flash_notification) {
                            $('.admin-container .top-notification').first().remove();
                            $('.admin-container').prepend(json.flash_notification);
                            $('.admin-container .top-notification').first().css('zIndex', 1005);
                        }
                    }
                    else if( json.success ){
                        if (json.flash_notification) {
                            $('.admin-container .top-notification').first().remove();
                            $('.admin-container').prepend(json.flash_notification);
                            $('.admin-container .top-notification').first().css('zIndex', 1005);
                        }

                        $form.find('.js-file-name').text( json.name );
                        $form.find('.js-file-path').text( json.path );

                        setTimeout( function(){
                            window.location = json.redirect;
                        }, 1500);
                    }
                }
            });
        });

        // Submit new folder form
        $(document).on('submit', '#folder-modal form', function( evt ){

            evt.preventDefault();
            evt.stopPropagation();

            var $form = $(this);

            if ($form.data('submitted') === true) {
                // Previously submitted - don't submit again
                return false;
            } else {
                // Mark it so that the next submit can be ignored
                $form.data('submitted', true);
            }

            $(document).foundation('reveal', 'reflow');

            if( typeof( $form.data('validated') ) == 'undefined' ){

                $.ajax({
                    url: '{{ route('adm.asset-manager.function', ['function' => 'validate_new_folder']) }}',
                    type: "post",
                    data: $form.serialize(),
                    dataType: "json",
                    beforeSend: function() {
                        showModalLoadingOverlay($('#folder-modal'), true);
                    },
                    success: function( json ){
                        $form.find('.js-modal-form-flash .alert-box').hide();
                        $form.find('.inline-error').remove();

                        if( json.error ){
                            if( json.field_errors ){
                                for( var field in json.field_errors ){
                                    $form.find('input[name="' + field + '"]').parent().append( '<span class="inline-error">' + json.field_errors[field][0] + '</span>' );
                                }
                            }

                            $form.find('.js-modal-form-flash .alert').text( json.error ).show();
                        }
                        else if( json.success ){
                            $form.data('submitted', false);
                            $form.data('validated', 'true');
                            $form.submit();
                        }
                    },
                    complete: function() {
                        showModalLoadingOverlay($('#folder-modal'), false);
                        $form.data('submitted', false);
                    }
                });
            } else {

                $.ajax({
                    url: '{{ route('adm.asset-manager.function', ['function' => 'new_folder']) }}',
                    type: "post",
                    data: $form.serialize(),
                    dataType: "json",
                    beforeSend: function() {
                        showModalLoadingOverlay($('#folder-modal'), true);
                    },
                    success: function( json ){
                        $form.find('.js-modal-form-flash .alert-box').hide();
                        $form.find('.inline-error').remove();

                        if( json.error ){
                            if( json.field_errors ){
                                for( var field in json.field_errors ){
                                    $form.find('input[name="' + field + '"]').parent().append( '<span class="inline-error">' + json.field_errors[field][0] + '</span>' );
                                }
                            }

                            $form.find('.js-modal-form-flash .alert').text( json.error ).show();
                        } else {

                            if (json.flash_notification) {
                                $('.admin-container .top-notification').first().remove();
                                $('.admin-container').prepend(json.flash_notification);

                                $('#folder-modal').foundation('reveal', 'close');

                                // make sure the newly added alert prompts can be removed
                                $(document).foundation('alert', 'reflow');
                            }

                            if( typeof( json.new_directory ) != 'undefined' ){
                                $form.find('input[name="parent"]').val( json.new_directory );
                            }
                        }
                    },
                    complete: function(){
                        showModalLoadingOverlay($('#folder-modal'), false);
                        $form.data('submitted', false);
                        reloadDirectoriesAndFiles( $form.find('input[name="parent"]').val() );
                    }
                });

            }
        });


        // Rename a folder
        $(document).on('submit', '#rename-folder-modal form', function( evt ){

            evt.preventDefault();
            evt.stopPropagation();

            var $form = $(this);

            if ($form.data('submitted') === true) {
                // Previously submitted - don't submit again
                return false;
            } else {
                // Mark it so that the next submit can be ignored
                $form.data('submitted', true);
            }

            $(document).foundation('reveal', 'reflow');

            $.ajax({
                url: $form.attr('action'),
                type: "post",
                data: $form.serialize(),
                dataType: "json",
                beforeSend: function() {
                    showModalLoadingOverlay($('#rename-folder-modal'), true);
                },
                success: function( json ){
                    $form.find('.js-modal-form-flash .alert-box').hide();
                    $form.find('.inline-error').remove();

                    if( json.error ){
                        if( json.field_errors ){
                            for( var field in json.field_errors ){
                                $form.find('input[name="' + field + '"]').parent().append( '<span class="inline-error">' + json.field_errors[field][0] + '</span>' );
                            }
                        }

                        $form.find('.js-modal-form-flash .alert').text( json.error ).show();
                    } else {

                        if (json.flash_notification) {
                            $('.admin-container .top-notification').first().remove();
                            $('.admin-container').prepend(json.flash_notification);

                            $('#rename-folder-modal').foundation('reveal', 'close');

                            // make sure the newly added alert prompts can be removed
                            $(document).foundation('alert', 'reflow');
                        }
                    }
                },
                complete: function(){
                    showModalLoadingOverlay($('#rename-folder-modal'), false);
                    $form.data('submitted', false);
                    reloadDirectoriesAndFiles();
                }
            });
        });


        // Upload file form
        $(document).on('submit', '#upload-modal form', function( evt ){

            evt.preventDefault();
            evt.stopPropagation();

            var $form = $(this);

            if ($form.data('submitted') === true) {
                // Previously submitted - don't submit again
                return false;
            } else {
                // Mark it so that the next submit can be ignored
                $form.data('submitted', true);
            }

            if( typeof( $form.data('validated') ) == 'undefined' || $form.data('validated') == false ){

                if( $form.find('.js-form-file-extension').length == 0 ) {
                    var file_extension = $form.find('input[type="file"]').val().split('.')[1];
                    $form.append('<input type="hidden" name="file_extension" value="' + file_extension + '" class="js-form-file-extension" />');
                }

                validateUpload( $form );
            } else {
                uploadFiles( $form );
            }
        });

        function validateUpload( $form ) {
            if (typeof( FormData ) == "undefined") {
                ie9ValidateUpload( $form );
            } else {
                var formData = new FormData( $('#upload-modal form')[0] );
                $.ajax({
                    url: '{{ route('adm.asset-manager.function', ['function' => 'validate_upload']) }}',
                    type: "post",
                    data: formData,
                    dataType: "json",
                    cache: false,
                    processData: false,
                    contentType: false,
                    beforeSend: function() {
                        //showModalLoadingOverlay($('#upload-modal'), true);
                    },
                    success: function( json ){
                        $form.find('.js-modal-form-flash .alert-box').hide();
                        $form.find('.inline-error').remove();

                        if( json.error ){
                            showModalLoadingOverlay($('#upload-modal'), false);

                            if( json.field_errors ){
                                for( var field in json.field_errors ){
                                    $form.find('input[name="' + field + '"]').parent().append( '<span class="inline-error">' + json.field_errors[field][0] + '</span>' );
                                }
                            }

                            $form.find('.js-modal-form-flash .alert').text( json.error ).show();
                        }
                        else if( json.success ){
                            $form.data('submitted', false);
                            $form.data('validated', 'true');
                            $form.submit();
                        }
                    },
                    error: function(){
                        $form.find('.js-modal-form-flash .alert').text( 'An unknown error occurred, please try again' ).show();
                    },
                    complete: function() {
                        $form.data('submitted', false);
                    }
                });
            }
        }
        function ie9ValidateUpload( $form ){
            $form.append('<input type="hidden" name="is_ie9" value="1" />');
            $form.ajaxSubmit({
                url: '{{ route('adm.asset-manager.function', ['function' => 'validate_upload']) }}',
                type: "post",
                dataType: 'json',
                beforeSubmit: function () {
                    showModalLoadingOverlay($('#upload-modal'), true);
                },
                success: function( json, statusText, xhr, $form ){
                    $form.find('.js-modal-form-flash .alert-box').hide();
                    $form.find('.inline-error').remove();

                    if (json.error) {
                        showModalLoadingOverlay($('#upload-modal'), false);

                        if (json.field_errors) {
                            for (var field in json.field_errors) {
                                $form.find('input[name="' + field + '"]').parent().append('<span class="inline-error">' + json.field_errors[field][0] + '</span>');
                            }
                        }

                        $form.find('.js-modal-form-flash .alert').text(json.error).show();
                    }
                    else if (json.success) {
                        $form.data('submitted', false);
                        $form.data('validated', 'true');
                        $form.submit();
                    }
                },
                error: function () {
                    $form.find('.js-modal-form-flash .alert').text('An unknown error occurred, please try again').show();
                    showModalLoadingOverlay($('#upload-modal'), false);
                }
            });
        }

        function uploadFiles( $form ){
            $form.ajaxSubmit({
                url: '{{ route('adm.asset-manager.function', ['function' => 'upload']) }}',
                type: "post",
                beforeSend: function(){
                    //showModalLoadingOverlay($('#upload-modal'), true);
                    $form.find('[data-loading-overlay]').show();
                },
                uploadProgress: function( evt, position, total, percentComplete ){
                    $form.find('[data-progress-bar]').css( 'width', percentComplete + '%' );
                },
                success: function( json ){
                    // IE9 Fix (the JSON response may be a string, and be wrapped in a <textarea> element)
                    if( typeof( json ) == 'string' ){
                        json = JSON.parse( json.replace( '<textarea>', '').replace( '</textarea>', '' ) );
                    }

                    $form.find('.js-modal-form-flash .alert-box').hide();
                    $form.find('.inline-error').remove();

                    if( json.error ){
                        if( json.field_errors ){
                            for( var field in json.field_errors ){
                                $form.find('input[name="' + field + '"]').parent().append( '<span class="inline-error">' + json.field_errors[field][0] + '</span>' );
                            }
                        }

                        $form.find('.js-modal-form-flash .alert').text( json.error ).show();

                    } else if( json.success ){

                        if (json.flash_notification) {
                            $('.admin-container .top-notification').first().remove();
                            $('.admin-container').prepend(json.flash_notification);

                            // make sure the newly added alert prompts can be removed
                            $(document).foundation('alert', 'reflow');
                        }

                        $('#upload-modal').foundation('reveal', 'close');

                        $form.data('validated', false);
                    }

                    $form.data('submitted', false);
                },
                error: function(){
                    $form.find('.js-modal-form-flash .alert').text( 'An unknown error occurred, please try again' ).show();
                },
                complete: function(){
                    //showModalLoadingOverlay($('#upload-modal'), false);
                    $form.find('[data-loading-overlay]').hide();
                    $form.data('submitted', false);
                    reloadFiles( $form.find('input[name="directory_id"]').val() );
                }
            });
        }

        // Call this function to move a set of specified file IDs into a given directory
        function move_files( file_ids, directory_id ){
            $.ajax({
                url: '{{ route('adm.asset-manager.function', ['function' => 'move_files']) }}',
                type: 'POST',
                data: {
                    file_ids: file_ids,
                    directory_id: directory_id
                },
                dataType: 'json',
                beforeSend: function(){

                    showPageLoadingOverlay(true);

                },
                success: function( json ){

                    showPageLoadingOverlay(false);

                    if( json.success ){
                        var title = '';
                        var content = '';

                        if( json.files_not_moved && json.files_not_moved.length > 0 ){
                            title = 'Assets couldn\'t be moved.';

                            content += '<p>1 or more assets couldn\'t be moved because an asset with the same name exists in the folder you tried to move them to.</p>';
                            content += '<span class="block">The assets are: </span>';
                            content += '<ul class="font-weight-600">';
                            $(json.files_not_moved).each( function(ind, val){
                                content += '<li>' + val + '</li>';
                            });
                            content += '</ul>';

                            var $modal = getDynamicModal({
                                title: title,
                                content: content
                            });
                            $('body').append($modal);
                            $modal.foundation('reveal', 'open');

                            if( json.files_moved > 0 ){
                                $modal.find('[data-close-reveal]').off('click').click( function(){
                                    reloadDirectoriesAndFiles(directory_id);
                                });
                            }
                        } else {
                            reloadDirectoriesAndFiles(directory_id);
                        }

                    } else {
                        var $modal = getDynamicModal({
                            title: 'Assets couldn\'t be moved.',
                            content: json.error
                        });
                        $('body').append($modal);
                        $modal.foundation('reveal', 'open');
                    }

                }
            });
        }

        // Call this function to move a set of specified folder IDs into a given parent directory
        function move_folders( folder_ids, parent_id ){
            $.ajax({
                url: '{{ route('adm.asset-manager.function', ['function' => 'move_folders']) }}',
                type: 'POST',
                data: {
                    directory_ids: folder_ids,
                    parent: parent_id
                },
                dataType: 'json',
                beforeSend: function(){

                    showPageLoadingOverlay(true);

                },
                success: function( json ){

                    showPageLoadingOverlay(false);

                    var title = '';
                    var content = '';

                    if( json.success ){

                        if( json.folders_not_moved && json.folders_not_moved.length > 0 ){
                            title = 'Sorry, that folder could not be moved.';

                            content += '<p>The folder "';
                            $(json.folders_not_moved).each( function(ind, val){
                                content += val;
                            });
                            content += '" already exists in that directory.</p>';

                            var $modal = getDynamicModal({
                                title: title,
                                content: content
                            });
                            $modal.foundation('reveal', 'open');

                            if( json.folders_moved > 0 ){
                                $modal.find('[data-close-reveal]').off('click').click( function(){
                                    reloadDirectoriesAndFiles(parent_id);
                                });
                            }

                        } else {
                            reloadDirectoriesAndFiles(parent_id);
                        }

                    } else {
                        // Textual error message
                        var $modal = getDynamicModal({
                            title: 'Sorry, that folder could not be moved.',
                            content: json.error
                        });
                        $('body').append($modal);
                        $modal.foundation('reveal', 'open');

                    }

                }
            });
        }

        // Reload the directory listing, passing a target element to change the DOM of
        function reloadDirectories( directory_id ){
            if( $('[data-asset-manager-search-input]').val().length > 0 ){
                assetManagerSearch();
                return false;
            }

            var data = {
                _asset_manager: true
            };
            if( typeof( directory_id ) != 'undefined' ){ data.directory_id = directory_id; }

            $.ajax({
                url: '{{ route('adm.asset-manager.function', ['load_directory_list']) }}',
                type: 'POST',
                data: data,
                dataType: 'json',
                beforeSend: function(){
                    showPageLoadingOverlay( true );
                },
                success: reloadResponseHandler,
                complete: function(){
                    isSiteAssetsCurrent( directory_id );
                    showPageLoadingOverlay();
                }
            });
        }

        // Reload the files listing, passing a target element to change the DOM of
        function reloadFiles( directory_id ){
            if( $('[data-asset-manager-search-input]').val().length > 0 ){
                assetManagerSearch();
                return false;
            }

            var data = {
                _asset_manager: true
            };
            if( typeof( directory_id ) != 'undefined' ){ data.directory_id = directory_id; }

            $.ajax({
                url: '{{ route('adm.asset-manager.function', ['load_files_list']) }}',
                type: 'POST',
                data: data,
                dataType: 'json',
                beforeSend: function(){
                    showPageLoadingOverlay( true );
                },
                success: reloadResponseHandler,
                complete: function(){
                    showPageLoadingOverlay();
                }
            });
        }

        // Reload both the directory listing and the files listing, passing a target element for both
        function reloadDirectoriesAndFiles( directory_id ){
            if( $('[data-asset-manager-search-input]').val().length > 0 ){
                assetManagerSearch();
                return false;
            }

            var data = {
                _asset_manager: true
            };
            if( typeof( directory_id ) != 'undefined' ){ data.directory_id = directory_id; }

            $.ajax({
                url: '{{ route('adm.asset-manager.function', ['load_directory_and_files_list']) }}',
                type: 'POST',
                data: data,
                dataType: 'json',
                beforeSend: function(){
                    showPageLoadingOverlay( true );
                },
                success: reloadResponseHandler,
                complete: function(){
                    isSiteAssetsCurrent( directory_id );
                    showPageLoadingOverlay();
                }
            });
        }

        // Common handler function to perform various DOM updated based on AJAX responses
        function reloadResponseHandler( json ){
            // If we have a new directory listing
            if( json.directory_list ){
                $directoryListing.html( json.directory_list );
            }
            // If we have a new files listing
            if( json.files_list ){
                $filesListing.html( json.files_list );
            }
            // If we have a file count
            if( json.files_count ){
                $filesCount.text( json.files_count );
            }
            // If we have a new delete folder button
            if( json.delete_action ){
                $deleteActionArea.html( json.delete_action );
            } else {
                $deleteActionArea.html('');
            }
            // If we have a new new folder modal
            if( json.new_folder ){
                $newFolderModal.html( json.new_folder );
            }
            // If we have a new upload modal
            if( json.upload && typeof( $uploadModal ) != 'undefined' ){
                $uploadModal.html( json.upload );
            }
            // If we have a new set of 'flat' directories (for the move files dropdown)
            if( json.flat_directories ){
                var $select = $('select[data-move-select]');

                var options_html = '';
                options_html += '<option selected disabled>Move selected to...</option>';
                for( var i in json.flat_directories ){
                    var directory = json.flat_directories[ i ];

                    var opt_class = 'font-weight-600';
                    if( directory.depth == 1 ){ opt_class= 'colour-grey'; }
                    if( directory.depth > 1 ){ opt_class = 'colour-grey-light'; }

                    var spacing = '';
                    for( var j = 0; j < directory.depth; j++ ){
                        spacing += "&nbsp;&nbsp;&nbsp;";
                    }

                    options_html += '<option data-directory-path="' + directory.absolute_path + '" value="' + directory.id + '" class="' + opt_class + '">' + spacing + directory.name + '</option>';
                }

                $select.html( options_html );
            }

            clear_redundant_modals();

            initAssetManager();

            triggerChangeSelected();

            scrollToUploadedAssets();
        }

        // Toggle whether the last clicked directory is 'Site Assets' and add the 'current' class if so
        function isSiteAssetsCurrent( directory_id ){
            if( directory_id == 0 ){
                $('[data-root-directory]').addClass('current');
            } else {
                $('[data-root-directory]').removeClass('current');
            }
        }

        // Show a clone of the dynamic modal, with a given title, content and buttons
        function getDynamicModal( options ){
            var $thisModal = $dynamicModal.clone();

            $thisModal.find('[data-dynamic-modal-title]').text( options.title );
            $thisModal.find('[data-dynamic-modal-content]').html( options.content );

            if (options.confirmButton) {
                $thisModal.find('[data-confirm-button]').text( options.confirmButton );
            } else {
                $thisModal.find('[data-confirm-button]').remove();
            }

            if (options.closeButton) {
                $thisModal.find('[data-close-reveal]').text( options.closeButton );
            }

            $(document).on('closed.fndtn.reveal', $thisModal, function () {
                $(this).remove();
            });

            return $thisModal;
        }

        $(document).on('click', '[data-sortable-column]', function(){
            var $col = $(this);

            var sort_field = $col.data('sort-field');
            var order = $col.data('dir');
            if( typeof( order ) == "undefined" || order.length == 0 ){ order = 'desc'; }

            order = (order == 'desc') ? 'asc' : 'desc';

            var directory_id = $('[data-asset-manager] li.current').data('folder-id');

            $.ajax({
                url: '{{ route('adm.asset-manager.function', ['load_files_list']) }}',
                type: 'POST',
                data: {
                    directory_id: directory_id,
                    sort_by: sort_field,
                    sort_direction: order
                },
                dataType: 'json',
                beforeSend: function(){
                    showPageLoadingOverlay( true );
                },
                success: reloadResponseHandler,
                complete: function(){
                    showPageLoadingOverlay();
                }
            });
        });

        // Shift click to select ALL files between the last checked box and the one just checked
        var $lastCheckedBox;
        var shift_pressed = false;
        $(document).on('change', '[name="selected-item"]', function(evt){

            // If we've previously checked a box...
            if( typeof( $lastCheckedBox ) != 'undefined' && $lastCheckedBox !== false ){

                // ... then we can check for a shift press...
                if( shift_pressed ){

                    // ... and if shift IS pressed, then we need to check all boxes in between the two
                    var $thisRow = $(this).parents('[data-draggable-asset]').first();
                    var $rowTo = $lastCheckedBox.parents('[data-draggable-asset]').first();

                    // So, work out the index within [data-draggable-asset] for the top and bottom box
                    var indexFrom = $thisRow.index('[data-draggable-asset]');
                    var indexTo = $rowTo.index('[data-draggable-asset]');

                    // If we've just selected a row thats below the first box, swap the indices
                    if( indexFrom > indexTo ){
                        var tempIndex = indexFrom;
                        indexFrom = indexTo;
                        indexTo = tempIndex;
                        tempIndex = null;
                    }

                    // For all the [data-draggable-asset] rows
                    $('[data-draggable-asset]').each( function( ind, val ){

                        // Check if this row's index is between our above limits
                        if( ind >= indexFrom && ind <= indexTo ){

                            // And if so, check the box, and add the highlight class
                            if( !$(this).find('[name="selected-item"]').is(':checked') ){
                                $(this).find('[data-asset-checkbox]').click();
                            }
                            $(this).addClass('selected-row');

                        }

                    });

                    // And finally, reset our last checked box variable to false
                    $lastCheckedBox = false;

                }
                // ... else, set the box as the last checked box if it's checked
                else {
                    if ($(this).is(':checked')) {
                        $lastCheckedBox = $(this);
                    }
                }

            }
            // ... else, set the box as the last checked box if it's checked
            else {
                if ($(this).is(':checked')) {
                    $lastCheckedBox = $(this);
                }
            }

        });
        // If the shift key is pressed down, then mark shift_pressed as true
        $(document).on('keydown', function(evt){
            if( evt.which == 16 ){
                shift_pressed = true;
            }
        });
        // If any key is released, we're not holding shift any more so mark it as false
        $(document).on('keyup', function(){
            shift_pressed = false;
        });

        /**
         * If any files have recently been uploaded, scroll to them and remove the temporary highlight after an interval
         */
        function scrollToUploadedAssets() {
            if ($('[data-draggable-asset].highlight-row').length > 0) {
                var $firstAsset = $('[data-draggable-asset].highlight-row').first();

                $('html, body').scrollTop($firstAsset.offset().top - 60);

                setTimeout( function(){
                    $('[data-draggable-asset].highlight-row').removeClass('highlight-row');
                }, 7500);
            }
        }
    });

    $(document).on( 'closed.fndtn.reveal', '[data-reveal]', clear_redundant_modals);

    function clear_redundant_modals(){
        var used_modals = new Array();
        $('[data-reveal]').each( function(){
            if( used_modals.indexOf( $(this).attr('id') ) == -1 ){
                used_modals.push( $(this).attr('id') );
            } else {
                console.log( 'Removing ' + $(this).attr('id') );
                $(this).remove();
            }
        })
    }

</script>
@endpush