module.exports = {
  purge: {
    enabled: true,
    content: ['./views/**/*'],
    options: {
      keyframes: true,
      fontFace: true
    },
    safelist: {
      standard: [
        /card-orange$/,
        /card-blue$/,
        /card-green$/,
        /card-red$/,
        /card-purple$/
      ]
    }
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        grey: {
          100: '#EBEBEB',
          400: '#A5A5A5',
          500: '#54565A',
          900: '#2A2A2A'
        },
        'teal': '#114A5F',
        'pink': '#F76C6C',
        'blue': '#5FC7C7',
        'green': '#8BD16E',
        'tur': '#5FC7C7',
        'orange': '#CB6500',
      },
      backgroundColor: {
        grey: {
          100: '#EBEBEB',
          400: '#A5A5A5',
          500: '#54565A',
          900: '#2A2A2A'
        },
        'teal': '#114A5F',
        'pink': '#F76C6C',
        'blue': '#5FC7C7',
        'green': '#8BD16E',
        'tur': '#5FC7C7',
        'card-orange': '#CB6500',
        'card-blue': '#2A96B7',
        'card-green': '#93D300',
        'card-red': '#AE2D2E',
        'card-purple': '#B267C8'
      },
      transitionDuration: {
        DEFAULT: '250ms'
      },
      fontSize: {
        'zero': '0',
        'xs': ['.875rem', '1.0714'],
        'sm': ['.875rem', '1.0714'],
        'base': ['1rem', '1.1875'],
        'lg': ['2.5rem', '1.1'],
        'xl': ['1rem', '1.125'], //h6 - 16
        '2xl': ['1.25rem', '1.2'], //h5 - 20
        '3xl': ['1.5rem', '1.083'], //h4 - 24
        '4xl': ['1.875rem', '1.1'], //h3 - 30
        '5xl': ['2.5rem', '1.1'], //h2 - 40
        '6xl': ['3.75rem', '1.1'], //h1 - 60
        '7xl': '4.25rem',
        '8xl': '4.5rem',
        '9xl': '4.75rem',
        'f1': ['1.875rem', '1.1'],
        'f2': ['1.25rem', '1.2'],
        '2rem': '2rem'
      },
      fontFamily: {
        'sans': ['Gotham HTF', 'sans-serif']
      },
      maxWidth: {
        'xl': '1416px',
        '2xl': '1416px',
        '100': '100px',
        '175': '175px',
      },
      boxShadow: {
        'card': '10px 10px 50px 0 rgba(0, 0, 0, 0.2)'
      },
      // borderRadius: {
      //   '4xl': '2.875rem',
      //   '5xl': '4.063rem'
      // },
      container: {
        center: true,
        padding: {
          DEFAULT: '20px',
        }
      },
      screens: {
        'sm': '640px',
        // => @media (min-width: 640px) { ... }

        'md': '769px',
        // => @media (min-width: 769px) { ... }

        'lg': '1024px',
        // => @media (min-width: 1024px) { ... }

        'xl': '1416px',
        // => @media (min-width: 1280px) { ... }

        '2xl': '1416px',
        // => @media (min-width: 1536px) { ... }
      },

    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
