@extends( 'templates.default' )

@section( 'content' )

    @include( 'partials.page-banner' )

    @include( 'page-builder.block-wrapper' )

@endsection