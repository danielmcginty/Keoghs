@extends( 'templates.default' )

@section( 'content' )

    <div class="height-v100 bg-gradient-primary-40 flex">
        <div class="row flex items-center justify-center">
            <div class="large-6 white">
                <h1>Sorry.</h1>
                <div>
                    <p>Sorry, we can't find what you're looking for.</p>
                </div>
            </div>
        </div>
    </div>

@endsection