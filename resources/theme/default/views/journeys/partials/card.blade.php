<div class="waypoint w-full md:w-6/12 md:px-8">
    <div class="card transform rounded-b-lg bg-white mb-12 md:mb-14 shadow-card">
        <a href="{{ route('theme', $journey->url) }}" class="over"></a>
        @if( !empty($journey->image) && !empty($journey->image['listing']) )
        <img 
            src="{{ $journey->image['listing']['src'] }}"
            alt="{{ $journey->image['listing']['alt'] }}"
            width="{{ $journey->image['listing']['width'] }}"
            height="{{ $journey->image['listing']['height'] }}"
            loading="lazy"
            class="w-full"
        />
        @endif

        <div class="bottom p-6 md:p-8">
            <img src="/default/dist/images/quote-single.svg" class="insights-section-quote">
            <h3 class="text-3xl md:mb-3 md:text-4xl text-teal mt-4">{{ $journey->title }}</h3>
            <p class="text-teal mb-6 mt-4"><strong>{{ $journey->name }}, {{ $journey->position }}</strong></p>
            <p class="mb-6">{{ $journey->excerpt }}</p>
            <a href="{{ route('theme', $journey->url) }}" class="btn bg-pink text-teal inline-block align-middle">View Story</a>
        </div>
    </div>
</div>