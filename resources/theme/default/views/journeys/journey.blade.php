@extends( 'templates.default' )

<?php $random_id = uniqid(); ?>

@if( isset( $journey->image ) && !empty( $journey->image ) && !empty( $journey->image['article'] ) )
    @push('header_styles')
        <style>
            [data-modal-video][data-video="{{ $random_id }}"] {
                background-image: url('{{ $journey->image['article']['src'] }}');
            }
        </style>
    @endpush
@endif

@section( 'content' )

    @include( 'partials.page-banner', ['this_page' => $archive] )

    <section class="content-section">
        <div class="container flex flex-wrap items-end pt-6 md:pb-14 pb-12">
            <div class="waypoint w-full">
                <h2 class="text-4xl md:text-5xl text-teal">{{ $journey->name }}, {{ $journey->position }}</h2>

                <p class="text-f2 text-teal mb-6">{{ $journey->excerpt }}</p>

                @if( !empty($journey->image) && !empty($journey->image['article']) && !empty($journey->video) )
                    <a data-modal-video href="{{ $journey->video }}" title="Play Video" class="block relative bg-center bg-cover rounded-lg bg-gray-200 mb-8">

                        @if( !empty($journey->image) && !empty($journey->image['article']) )
                            <img
                                src="{{ $journey->image['article']['src'] }}"
                                alt="{{ $journey->image['article']['alt'] }}"
                                width="{{ $journey->image['article']['width'] }}"
                                height="{{ $journey->image['article']['height'] }}"
                                loading="lazy"
                            />
                        @endif

                        {{-- Video overlay --}}
                        <span class="absolute inset-0 bg-black opacity-40 hover:opacity-25 tr-opacity"></span>

                        {{-- Play button --}}
                        <span class="absolute inset-0 flex flex-col items-center justify-center">
                            <img 
                                src="/default/dist/images/play-button.png"
                                alt="Play button"
                                width="200"
                                height="200"
                                class="w-32"
                                loading="lazy"
                            />
                        </span>

                    </a>
                @elseif( !empty($journey->image) && !empty($journey->image['article']) )
                    <img 
                        src="{{ $journey->image['article']['src'] }}" 
                        alt="{{ $journey->image['article']['alt'] }}" 
                        width="{{ $journey->image['article']['width'] }}"
                        height="{{ $journey->image['article']['height'] }}"
                        loading="lazy"
                        class="mb-8"
                    />
                @endif

                <div class="cms-content">
                    {!! $journey->content !!}
                </div>

            </div>
        </div>
    </section>

    @if( isset($other_journeys) && $other_journeys->isNotEmpty() )
        <section class="insights-section bg-grey-100">
            <div class="container flex flex-wrap">
                <div class="w-full pt-10 pb-5 md:pt-6 md:pb-5 title">
                    <h2 class="waypoint text-4xl md:text-5xl mt-10 text-center text-teal">Other Stories</h2>
                </div>

                @foreach($other_journeys as $journey)
                    @include('journeys.partials.card', ['journey' => $journey])
                @endforeach

                <div class="waypoint w-full text-center pt-1 pb-10 md:pb-16">
                    <a href="{{ route('theme', $archive->url) }}" class="btn bg-pink text-teal inline-block align-middle">View all</a>
                </div>
            </div>
        </section>
    @endif

    @include( 'page-builder.block-wrapper', ['this_page' => $archive] )

@endsection