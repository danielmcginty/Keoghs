@extends( 'templates.default' )

@section( 'content' )

    @include( 'partials.page-banner' )

    @include( 'partials.page-intro-content' )

    <section class="insights-section bg-grey-100">
        <div class="container flex flex-wrap">
            <div class="w-full pt-10 pb-5 md:pt-6 md:pb-5 title">
                <h2 class="waypoint text-4xl md:text-5xl mt-10 text-center text-teal">Keoghs Journey</h2>
            </div>

            @foreach($journeys as $journey)
                @include('journeys.partials.card', ['journey' => $journey])
            @endforeach
        </div>
    </section>

    @include( 'page-builder.block-wrapper' )

@endsection