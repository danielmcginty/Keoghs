@extends( 'templates.default' )

@section( 'content' )

    @include( 'partials.page-banner' )

    <div class="row flex flex-wrap lg:p-0 my-30 md:my-50">

        {{-- Articles --}}
        <div class="flex flex-wrap w-full md:w-2/3 lg:w-3/4">

            @if( isset( $news_articles ) && $news_articles->isNotEmpty() )

                @foreach( $news_articles as $index => $article )

                    <div class="w-full lg:w-1/2 lg:px-20 mb-30 md:mb-40">

                        <div class="flex flex-col bg-gray-200 h-full rounded-lg overflow-hidden">

                            <a
                                href="{{ route('theme', $article->url) }}"
                                class="w-full bg-center bg-cover pb-16:9 hover:opacity-80 tr-opacity"
                                @if( !empty( $article->image ) && !empty( $article->image['listing'] ) )
                                style="background-image:url('{{ $article->image['listing']['src'] }}')"
                                @endif
                            ></a>

                            <div class="p-30">

                                <a href="{{ route('theme', $article->url) }}">
                                    <h2 class="text-xl md:text-2xl font-semibold mb-20 hover:underline">{{ $article->title }}</h2>
                                </a>

                                {!! $article->excerpt !!}

                            </div>

                            <div class="p-30 pt-0 mt-auto">
                                <a href="{{ route('theme', $article->url) }}" class="inline-flex bg-black hover:bg-gray-900 text-white font-bold px-30 py-10 rounded-full">
                                    Read More
                                </a>
                            </div>

                        </div>

                    </div>

                @endforeach

                <div class="pagination w-full lg:px-20 mb-30">
                    {!! $news_articles->links() !!}
                </div>

            @else

                <h2 class="text-3xl md:mt-20 font-semibold">
                    Sorry, no articles were found{{ isset( $search_term ) ? ' for "' . $search_term . '"' : '' }}.
                </h2>

                @if( isset( $search_term ) && !empty( $search_term ) )
                    <h3 class="text-xl text-gray-700 mt-30">Check for spelling errors, or try a less specific search.</h3>
                @endif

            @endif

        </div>

        {{-- Sidebar --}}
        <div class="w-full md:w-1/3 lg:w-1/4 md:pl-30 lg:px-20">

            @if( isset( $news_categories ) && $news_categories->isNotEmpty() )
            <div class="bg-gray-200 rounded-lg p-30 mb-30">

                <h3 class="text-xl font-semibold mb-15">
                    Categories
                </h3>

                <ul>
                    <li class="block">
                        <a href="{{ route('theme', $news_archive_page->url) }}" class="block hover:underline py-5">
                            All News
                        </a>
                    </li>
                    @foreach( $news_categories as $news_category )
                        <li class="block">
                            <a href="{{ route('theme', $news_category->url) }}" class="block hover:underline py-5">
                                {{ $news_category->title }}
                            </a>
                        </li>
                    @endforeach
                </ul>

            </div>
            @endif

            @if( $news_archive_page )
            <div class="bg-gray-200 rounded-lg p-20 mb-30">

                <label for="article-search" class="text-hidden">
                    Search
                </label>

                <form class="flex items-center" action="{{ route('theme', $news_archive_page->url) }}" method="get" enctype="multipart/form-data">

                    <input type="search" id="article-search" name="search_term" class="form-input w-full border-2" placeholder="Search..." value="{{ isset( $search_term ) ? $search_term : '' }}" />

                    <button type="submit" class="inline-flex h-full p-10 bg-black text-white rounded-r">
                        <svg class="block fill-current size-text text-lg" viewBox="0 0 512 512">
                            <path d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"></path>
                        </svg>
                        <span class="text-hidden">Search</span>
                    </button>

                </form>

            </div>
            @endif

        </div>

    </div>

    @include( 'page-builder.block-wrapper' )

@endsection