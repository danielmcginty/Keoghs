@extends( 'templates.default' )

@section( 'content' )

    @include( 'includes.breadcrumbs' )

    <div class="row my-30">

        @if( !empty( $this_page->image ) && !empty( $this_page->image['article'] ) )
            <div class="max-w-1000 mb-20 lg:mb-0">
                <div class="pb-16:9 bg-center bg-cover rounded" style="background-image: url('{{ $this_page->image['article']['src'] }}');"></div>
            </div>
        @endif

        <div class="max-w-800 bg-white lg:px-30 lg:py-20 lg:-mt-30 lg:ml-30">

            <h1 class="text-2xl md:text-3xl font-semibold mb-20">
                {{ $this_page->h1_title }}
            </h1>

            <div class="flex flex-wrap">

                @if( $this_page->author )
                    <div class="border-r-2 border-gray-300 pr-10 mr-10">
                        {{ $this_page->author->name }}
                    </div>
                @endif

                @if( !empty( $this_page->article_date ) )
                    <div class="border-r-2 border-gray-300 pr-10 mr-10">{{ $this_page->article_date->format( 'jS F Y' ) }}</div>
                @endif

                @if( $this_page->all_categories->isNotEmpty() )
                    <div class="pr-10 mr-10">
                        {{ $this_page->all_categories->implode( 'title', ', ' ) }}
                    </div>
                @endif

            </div>

        </div>

    </div>

    <div class="row mb-30">
        @include( 'page-builder.partials.social-share' )
    </div>

    <div class="row mb-30">
        <div class="cms-content">
            {!! $this_page->content !!}
        </div>
    </div>

    <div class="row mb-30">
        @include( 'page-builder.partials.social-share' )
    </div>

    <div class="row flex flex-wrap p-0 my-30 md:my-50">

        <div class="w-full md:order-1 md:w-1/2 px-20 my-20">

            @if( $this_page->next_article )
                <a href="{{ route('theme', [$this_page->next_article->url]) }}" class="block md:text-right">
                    <div class="font-semibold mb-15">
                        Next Article
                    </div>
                    <div class="text-xl hover:underline">{{ $this_page->next_article->title }}</div>
                </a>
            @endif

        </div>

        <div class="w-full md:w-1/2 px-20 my-20">

            @if( $this_page->previous_article )
                <a href="{{ route('theme', [$this_page->previous_article->url]) }}" class="block">
                    <div class="font-semibold mb-15">
                        Previous Article
                    </div>
                    <div class="text-xl hover:underline">{{ $this_page->previous_article->title }}</div>
                </a>
            @endif

        </div>

    </div>

    @include( 'page-builder.block-wrapper' )

@endsection

@push( 'schema_scripts' )
@include( 'includes.schema.news-article', ['article' => $this_page] )
@endpush