@extends( 'templates.default' )

@section( 'content' )

    @include( 'partials.page-banner' )

    @if( isset( $Offices ) && $Offices->isNotEmpty() )

        <?php $random_id = uniqid(); ?>

        <div class="row relative z-10 my-50">

            <div data-osm-map-multi data-map-id="{{ $random_id }}" data-zoom="14" data-theme="wikimedia" class="osm-map bg-gray-200 pb-16:9"></div>

        </div>

        <div class="row px-10 flex flex-wrap justify-center my-20 md:my-40">
            @foreach( $Offices as $Office )

                <div class="w-full md:w-1/2 xl:w-1/3 px-15 my-15">

                    <div class="flex flex-col bg-gray-200 p-30 rounded-lg h-full">

                        <div>

                            <a href="{{ route('theme', $Office->url) }}" class="block text-2xl font-bold mb-20 text-black hover:underline">
                                {{ $Office->title }}
                            </a>

                            @if( !empty( $Office->address ) )
                                <div class="mb-15">
                                    <span class="block || font-bold">Address</span>
                                    {!! $Office->address !!}
                                </div>
                            @endif

                            @if( !empty( $Office->opening_times ) )
                                <div class="mb-15">
                                    <span class="block || font-bold">Opening Times</span>
                                    {!! $Office->opening_times !!}
                                </div>
                            @endif

                        </div>

                        <div class="pt-10 mt-auto">

                            <a href="{{ route('theme', $Office->url) }}" class="inline-flex bg-primary hover:bg-darken text-white font-semibold px-25 py-10 rounded-full">
                                Read more...
                            </a>

                        </div>

                    </div>

                </div>

            @endforeach
        </div>

        @push( 'footer_scripts' )
            <script>

                if( typeof( osm_map_markers ) == "undefined" ){
                    var osm_map_markers = {};
                }

                var tmp_markers = [];
                @foreach( $Offices as $Office )
                tmp_markers.push( [{{ $Office->latitude }}, {{ $Office->longitude }}, '{!! $Office->map_popup_content !!}'] );
                @endforeach
                osm_map_markers[ '{{ $random_id }}' ] = tmp_markers;

            </script>
        @endpush

    @endif

@endsection