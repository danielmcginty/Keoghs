@extends( 'templates.default' )

@section( 'content' )

    <?php $random_id = uniqid(); ?>

    <div
        class="bg-primary text-white py-50 md:py-75 bg-cover bg-center"
        @if( !empty( $this_page->image ) && !empty( $this_page->image['desktop'] ) )
            style="background-image:url('{{ $this_page->image['desktop']['src'] }}');"
        @endif
    >

        <h1 class="row text-2xl md:text-3xl xl:text-4xl font-bold text-center">
            {{ $this_page->h1_title }}
        </h1>

    </div>

    <div class="row flex flex-wrap p-0 my-50 lg:my-75">

        <div class="w-full lg:w-2/3 px-20 mb-30 lg:mb-0">
            <div data-osm-map data-map-id="{{ $random_id }}" data-lat="{{ $this_page->latitude }}" data-lng="{{ $this_page->longitude }}" data-theme="wikimedia" data-zoom="12" data-popup="{{ $this_page->h1_title }}" class="osm-map bg-gray-200 pb-16:9 radius z-10"></div>
        </div>

        <div class="w-full lg:w-1/3 px-20">

            @if( !empty( $this_page->address ) )
                <div class="mb-30">
                    <div class="font-semibold mb-10">Address</div>
                    <address class="text-lg not-italic">
                        {!! $this_page->address !!}
                    </address>
                </div>
            @endif

            @if( !empty( $this_page->opening_times ) )
                <div class="mb-30">
                    <div class="font-semibold mb-10">Opening Times</div>
                    <div class="cms-content text-lg">
                        {!! $this_page->opening_times !!}
                    </div>
                </div>
            @endif

            @if( !empty( $this_page->telephone ) || !empty( $this_page->email ) )

                @if( !empty( $this_page->telephone ) )
                    <a href="tel:{{ str_replace( ' ', '', $this_page->telephone ) }}" class="block text-xl mb-20 hover:underline">
                        {{ $this_page->telephone }}
                    </a>
                @endif

                @if( !empty( $this_page->email ) )
                    <a href="mailto:{{ $this_page->email }}" class="block text-xl mb-20 hover:underline">
                        {{ $this_page->email }}
                    </a>
                @endif

            @endif

        </div>

    </div>

    @include( 'page-builder.block-wrapper' )

@endsection