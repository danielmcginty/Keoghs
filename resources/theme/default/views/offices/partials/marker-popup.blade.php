@if( isset( $Office ) && $Office )
    <div class="marker-content p-20 font-sans" style="min-width: 250px;">

        <h4 class="text-xl font-semibold">{{ $Office->title }}</h4>

        <div class="cms-content text-sm mb-20">
            {!! $Office->address !!}
        </div>

        {{--<div class="cms-content text-sm">--}}
            {{--{!! $Office->opening_times !!}--}}
        {{--</div>--}}

        <a href="{{ route('theme', $Office->url) }}" class="inline-flex bg-black hover:bg-gray-900 tr-color font-semibold text-base px-25 py-10 rounded-full">
            <span class="text-white">Read more...</span>
        </a>

    </div>
@endif