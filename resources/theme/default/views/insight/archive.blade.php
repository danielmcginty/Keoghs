@extends( 'templates.default' )

@section( 'content' )

    @include('includes.breadcrumbs')

    @include('partials.page-hero', [
        'hero_image' => $this_page->image['desktop']['src'] ?? "{$dist_dir}ui/h1.jpg",
        'title' => $this_page->h1_title,
        'description' => $this_page->description ?? '',
    ])

    <section class="insights-list-section">
        <div class="container flex flex-wrap items-start pt-4 pb-16">
            <aside class="w-full md:w-3/12 pt-2">
                <button class="ml-auto mr-auto btn-no-arrow btn md:hidden btn-full text-white bg-pink mb-8 filter-btn">Filter your Search</button>

                <div class="wrap">
                    <h3 class="text-4xl title md:hidden p-4 text-white bg-teal">Keoghs Insights <button class="close"></button></h3>

                    <div class="inner">
                        <h4 class="text-3xl text-teal mb-8 flex items-center md:block justify-between">Top Categories  <button class="text-base mb-0 block md:hidden"><u>Clear All</u></button></h4>

                        <form data-insight-search-form>
                            <div class="view insights-larger">
                                @foreach($top_categories as $category)
                                    <div class="pb-5">
                                        <input
                                            data-insight-category
                                            type="checkbox"
                                            id="insight-top-category-{{ $category->category_id }}"
                                            {{ in_array($category->category_id, $selected_categories) ? 'checked' : '' }}
                                            value="{{ $category->category_id }}"
                                        />
                                        <label for="insight-top-category-{{ $category->category_id }}">{{ $category->title }}</label>
                                    </div>
                                @endforeach
                                <div class="pb-5">
                                    <div class="w-full bg-grey-900 pb-1 mb-2" style="max-width: 250px;"></div>
                                </div>
                                <h4 class="text-3xl text-teal mb-8 flex items-center md:block justify-between">A-Z</h4>
                                @foreach($categories as $category)
                                    <div class="pb-5">
                                        <input
                                            data-insight-category
                                            type="checkbox"
                                            id="insight-category-{{ $category->category_id }}"
                                            {{ in_array($category->category_id, $selected_categories) ? 'checked' : '' }}
                                            value="{{ $category->category_id }}"
                                        />
                                        <label for="insight-category-{{ $category->category_id }}">{{ $category->title }}</label>
                                    </div>
                                @endforeach
                            </div>
                            <div class="show-all text-teal">Show All</div>

                            <div class="pb-5">
                                <div class="w-full bg-grey-900 pb-1 mb-2" style="max-width: 250px;"></div>
                            </div>
                            <h4 class="text-3xl text-teal mb-4 flex items-center md:block justify-between">Search</h4>
                            <div class="mb-8">
                                <label for="search-term" class="sr-only">Search term</label>
                                <input
                                    data-insight-search
                                    type="search"
                                    name="term"
                                    id="search-term"
                                    class="w-full border-2 border-gray-300 text-black rounded form-input"
                                    style="max-width: 250px;"
                                    value="{{ $search_term }}"
                                />
                            </div>

                            <button class="ml-auto mr-auto btn-no-arrow btn md:hidden block btn-full text-white bg-pink mb-8">Apply Filters</button>

                            <button class="ml-auto mr-auto block md:hidden"><u>Clear All</u></button>
                        </form>
                    </div>
                </div>
            </aside>

            <div class="waypoint w-full md:w-9/12 flex flex-wrap pt-2">

                <div class="w-full top-filter flex flex-wrap justify-between items-center md:px-8 mb-8">
                    <h4 class="text-3xl text-teal mb-0">Showing <span data-insight-results>{{ $insights->count() }}</span> of <span data-insight-total>{{ $insights->total() }}</span> Results</h4>

                    <div class="flex items-center">
                        <label for="insight-sort" class="hidden md:block mb-0 mr-4 text-xl font-normal normal-case text-teal not-italic">Sort by</label>
                        <select class="text-teal border-none mb-0" data-insight-sort id="insight-sort">
                            @foreach($sort_options as $key => $option)
                                <option value="{{ $key }}" {{ $key === $selected_sort_key ? 'selected' : '' }}>{{ $option['label'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="flex flex-wrap" data-insights>
                    @if( isset( $insights ) && $insights->isNotEmpty() )
                        @foreach( $insights as $index => $insight )
                            <div class="waypoint w-full md:w-6/12 md:px-8 mb-12 md:mb-14">
                                @include('insight.partials.card')
                            </div>
                        @endforeach
                    @endif
                </div>

                <div class="w-full text-center hidden" data-pagination>
                    <a href="{{ $insights->nextPageUrl() }}" class="btn bg-pink btn-full text-white" data-load-more>Load more</a>
                </div>

                <div data-no-results class="w-full lg:px-20 mb-30 hidden">
                    <h2 class="text-3xl md:mt-20 font-semibold">
                        Sorry, no insights were found.
                    </h2>
                </div>

            </div>

        </div>
    </section>

    @include('partials.newsletter-signup')

    @include( 'page-builder.block-wrapper' )

@endsection

@push('footer_scripts')
    <script>
        var pagination = {
            current_page: 1,
            hasMorePages: {{ $insights->hasMorePages() ? 'true' : 'false' }},
            results: {{ $insights->count() }},
            total: {{ $insights->total() }},
        };

        var selectedCategories = new Set(@json($selected_categories));

        updatePagination();

        $('[data-load-more]').click(function (evt) {
            evt.preventDefault();

            pagination.current_page++;

            var selectedCategoriesString = selectedCategoriesToString();

            $.ajax({
                url: '{{ route('theme.ajax', ['insight', 'loadMoreInsights']) }}',
                type: 'GET',
                data: {
                    page: pagination.current_page,
                    categories: selectedCategoriesString,
                    'sort-by': getCurrentSort(),
                    'term': $('[data-insight-search]').val()
                },
                dataType: 'json',
                beforeSend: function () {
                    // TODO: Add a loading state
                },
                success: function (data) {
                    var insights = data.insights.data;

                    pagination.hasMorePages = data.insights.next_page_url !== null;
                    pagination.results = data.insights.to;
                    pagination.total = data.insights.total;

                    updatePagination();
                    $('[data-insights]').append(data.rendered_insights);
                    //insights.forEach(appendInsight);
                },
                complete: function () {
                    // TODO: Hide loading state
                }
            });
        });

        $('[data-insight-category]').change(function () {
            var selected = $(this).is(':checked');

            if (selected) {
                selectedCategories.add($(this).val());
            } else {
                selectedCategories.delete($(this).val());
            }

            var selectedCategoriesString = selectedCategoriesToString();

            var url = new URL(window.location);
            if (selectedCategoriesString.length === 0) {
                url.searchParams.delete('categories');
            } else {
                url.searchParams.set('categories', selectedCategoriesString);
            }
            history.pushState({}, '', url);

            pagination.current_page = 1;

            $.ajax({
                url: '{{ route('theme.ajax', ['insight', 'filterInsights']) }}',
                type: 'GET',
                data: {
                    page: pagination.current_page,
                    categories: selectedCategoriesString,
                    'sort-by': getCurrentSort(),
                    'term': $('[data-insight-search]').val()
                },
                dataType: 'json',
                beforeSend: addLoadingState,
                success: function (data) {
                    var insights = data.insights.data;

                    pagination.hasMorePages = data.insights.next_page_url !== null;
                    pagination.results = data.insights.to;
                    pagination.total = data.insights.total;

                    updatePagination();
                    clearCurrentInsights();
                    $('[data-insights]').append(data.rendered_insights);
                    //insights.forEach(appendInsight);
                },
                complete: removeLoadingState
            });
        });

        $('[data-insight-sort]').change(function () {
            var sort = $(this).val();

            var selectedCategoriesString = selectedCategoriesToString();

            var url = new URL(window.location);
            url.searchParams.set('sort-by', sort);
            history.pushState({}, '', url);

            pagination.current_page = 1;

            $.ajax({
                url: '{{ route('theme.ajax', ['insight', 'filterInsights']) }}',
                type: 'GET',
                data: {
                    page: pagination.current_page,
                    categories: selectedCategoriesString,
                    'sort-by': sort,
                    'term': $('[data-insight-search]').val()
                },
                dataType: 'json',
                beforeSend: addLoadingState,
                success: function (data) {
                    var insights = data.insights.data;

                    pagination.hasMorePages = data.insights.next_page_url !== null;
                    pagination.results = data.insights.to;
                    pagination.total = data.insights.total;

                    updatePagination();
                    clearCurrentInsights();
                    $('[data-insights]').append(data.rendered_insights);
                    //insights.forEach(appendInsight);
                },
                complete: removeLoadingState
            });
        });

        $('[data-insight-search-form]').on('submit', function (evt) {
            evt.preventDefault();

            var $form = $(this);
            var sort = $('[data-insight-sort]').val();

            var selectedCategoriesString = selectedCategoriesToString();

            var url = new URL(window.location);
            url.searchParams.set('term', $('[data-insight-search]').val());
            history.pushState({}, '', url);

            pagination.current_page = 1;

            $.ajax({
                url: '{{ route('theme.ajax', ['insight', 'filterInsights']) }}',
                type: 'GET',
                data: {
                    page: pagination.current_page,
                    categories: selectedCategoriesString,
                    'sort-by': sort,
                    'term': $('[data-insight-search]').val()
                },
                dataType: 'json',
                beforeSend: addLoadingState,
                success: function (data) {
                    var insights = data.insights.data;

                    pagination.hasMorePages = data.insights.next_page_url !== null;
                    pagination.results = data.insights.to;
                    pagination.total = data.insights.total;

                    updatePagination();
                    clearCurrentInsights();
                    $('[data-insights]').append(data.rendered_insights);
                    $form.parents('aside .wrap').removeClass('open');
                    //insights.forEach(appendInsight);
                },
                complete: removeLoadingState
            });
        });

        function addLoadingState() {
            // TODO
        }

        function removeLoadingState() {
            // TODO
        }

        function updatePagination() {
            if (pagination.hasMorePages) {
                $('[data-pagination]').removeClass('hidden');
            } else {
                $('[data-pagination]').addClass('hidden');
            }

            $('[data-insight-results]').text(pagination.results);
            $('[data-insight-total]').text(pagination.total);
        }

        function clearCurrentInsights() {
            $('[data-insights]').empty();
        }

        function selectedCategoriesToString()
        {
            return Array.from(selectedCategories).join(',');
        }

        function getCurrentSort()
        {
            return $('[data-insight-sort]').val();
        }

        function appendInsight(insight) {
            var cat_links = [];
            for (var i = 0; i < insight.categories.length; i++) {
                cat_links.push('<a href="{{ route('theme', $this_page->url) }}?categories=' + insight.categories[i].url + '" class="">' + insight.categories[i].title + '</a>');
            }
            var cats = '';
            if (cat_links.length) {
                cats = '<div class="cat-links">' + cat_links.join('') + '</div>';
            }
            $('[data-insights]').append(
                `
                <div class="w-full md:w-6/12 md:px-8">
                    <div class="card transform rounded-b-lg bg-white mb-12 md:mb-14 shadow-card">
                        <a href="{{ route('theme') }}/${insight.url}" class="over"></a>
                        <img src="${insight?.image?.listing?.src}">

                        <div class="bottom p-6 md:p-8">
                            <h3 class="text-3xl md:mb-3 md:text-4xl text-teal">${insight.title}</h3>
                        </div>
                        ${cats}
                    </div>
                </div>
                `
            );
        }

        $(document).ready( function () {
            $('aside .view').each( function () {
                var h = 0;
                $(this).find('>div').each ( function (ind, elem) {
                    if (ind < 8) {
                        h += $(this).outerHeight();
                    } else {
                        return false;
                    }
                });
                $(this).css('maxHeight', h);
            });
        });
    </script>
@endpush