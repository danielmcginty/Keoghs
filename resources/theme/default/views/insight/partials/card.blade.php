<div class="h-full card transform rounded-b-lg bg-white shadow-card">
    <a href="{{ route('theme', $insight->url) }}" class="over"></a>
    @if( !empty( $insight->image ) && !empty( $insight->image['listing'] ) )
        <img 
            src="{{ $insight->image['listing']['src'] }}"
            alt="{{ $insight->image['listing']['alt'] }}"
            width="{{ $insight->image['listing']['width'] }}"
            height="{{ $insight->image['listing']['height'] }}"
            loading="lazy"
        >
    @endif

    <div class="bottom p-6 md:p-8">
        <h3 class="text-3xl md:mb-3 md:text-4xl text-teal">{{ $insight->title }}</h3>

        @if( $insight->visible_categories->isNotEmpty() && isset($landing_page) )
        <div class="cat-links">
            @foreach( $insight->visible_categories as $category )
            <a href="{{ route('theme', $landing_page->url) }}?categories={{ $category->category_id }}" class="text-white bg-grey-400 hover:bg-pink">{{ $category->title }}</a>
            @endforeach
        </div>
        @endif
    </div>
</div>