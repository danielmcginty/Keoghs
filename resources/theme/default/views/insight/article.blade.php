@extends( 'templates.default' )

@section( 'content' )

    @include( 'includes.breadcrumbs' )

    <section class="hero-section bg-white padded more-margin">
        <div class="full-bg" style="background-image:url({{ $this_page->image['article']['src'] ?? '' }});">

            <div class="container flex flex-wrap items-end">
                <div class="w-full">

                    <div class="theme-dark bg-teal bg-opacity-95 text-white border-l-8 border-solid border-pink px-5 md:px-16 py-14 md:py-12 pr-8 flex-wrap flex items-center">

                        <div class="w-full md:w-4/5">
                            <h1 class="text-5xl md:text-6xl mb-10 md:mb-14">{{ $this_page->title }}</h1>
                        </div>

                        <div class="w-full md:w-4/5">
                            <strong class="mb-5 block mb:mb-0">{{ $this_page->published_at->format('d/m/Y') }}</strong>
                        </div>

                        <div class="w-full md:w-1/5 relative">
                            <div class="flex items-center justify-end share">
                                <strong class="mr-3">Share:</strong>

                                <a href="https://www.linkedin.com/shareArticle?mini=true&amp;title={{ rawurlencode( $this_page->h1_title ) }}&amp;url={{ rawurlencode($canonical_url) }}" class="mr-3" target="_blank" rel="noopener">
                                    <img src="{{ "{$dist_dir}ui/linkedin-pink.svg" }}">
                                    <img src="{{ "{$dist_dir}ui/linkedin-pink-hover.svg" }}">
                                </a>

                                <a href="https://twitter.com/intent/tweet?text={{ rawurlencode( $this_page->h1_title ) }}%20-%20{{ rawurlencode($canonical_url) }}" class="mr-3" target="_blank" rel="noopener">
                                    <img src="{{ "{$dist_dir}ui/twitter-pink.svg" }}">
                                    <img src="{{ "{$dist_dir}ui/twitter-pink-hover.svg" }}">
                                </a>

                                <a href="http://www.facebook.com/sharer.php?p%5Btitle%5D={{ rawurlencode( $this_page->h1_title ) }} &amp;p%5Burl%5D={{ rawurlencode($canonical_url) }}" class="mr-3" target="_blank" rel="noopener">
                                    <img src="{{ "{$dist_dir}ui/facebook-pink.svg" }}">
                                    <img src="{{ "{$dist_dir}ui/facebook-pink-hover.svg" }}">
                                </a>

                                <a data-copy-to-clipboard href="#" rel="noopener">
                                    <img src="{{ "{$dist_dir}ui/share-pink.svg" }}">
                                    <img src="{{ "{$dist_dir}ui/share-pink-hover.svg" }}">
                                </a>

                            </div>
                            <div class="absolute top-100 right-0 pt-1 text-white">
                                <span data-copied-to-clipboard class="hidden">Link copied</span>
                                <span data-not-copied-to-clipboard class="whitespace-nowrap hidden">Sorry, we can't copy the link</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-section bg-white">
        <div class="container flex flex-wrap items-end pt-6 md:pb-14 pb-12">
            <div class="waypoint w-full">
                <div class="cms-content">{!! $this_page->body !!}</div>

                @if( $this_page->pdf_download )
                <div class="text-center pt-6 pb-12">
                    <a
                        href="{{ $this_page->pdf_download->url }}"
                        target="_blank"
                        class="btn bg-pink text-teal"
                    >{{ $this_page->pdf_download_text }}</a>
                </div>
                @endif

                @if( $this_page->visible_categories->isNotEmpty() && isset($landing_page) )
                <div class="cat-links condensed">
                    @foreach( $this_page->visible_categories as $category )
                    <a href="{{ route('theme', $landing_page->url) }}?categories={{ $category->category_id }}" class="text-white bg-grey-400 hover:bg-pink">{{ $category->title }}</a>
                    @endforeach
                </div>
                @endif

                @if( !empty($author) && !empty($author->full_name) )
                <div class="w-full flex flex-wrap items-center bg-grey-100 border-teal border-t-4 border-solid px-14 md:px-20 py-12 mt-6">
                    @if( !empty($author->image) && !empty($author->image['primary']) )
                    <div class="w-full md:w-4/12 pl-4 pr-4 md:pl-8 md:pr-4">
                        <img 
                            src="{{ $author->image['primary']['src'] ?? '' }}" 
                            alt="{{ $author->full_name }}"
                            width="{{ $author->image['primary']['width'] }}"
                            height="{{ $author->image['primary']['height'] }}"
                            loading="lazy"
                            class="rounded-full shadow-2xl"
                        >
                    </div>
                    @endif

                    <div class="w-full md:w-6/12 {{ empty($author->image) || empty($author->image['primary']) ? '' : 'pl-4 md:pl-8' }}">

                        <h5 class="text-3xl text-teal mb-2 md:mb-4 md:mt-0 mt-12">Author</h5>
                        <p class="text-base text-grey-900 mb-5">
                            <strong>{{ $author->full_name }}</strong> 
                            @if( !empty($author->job_title) || !empty($author->location) )
                            <br>
                            {{ !empty($author->job_title) ? $author->job_title : '' }}
                            {{ !empty($author->job_title) && !empty($author->location) ? ' - ' : '' }}
                            {{ !empty($author->location) ? $author->location : '' }}
                            @endif
                            @if( !empty($author->expertise_area) )
                            <br>{{ $author->expertise_area }}
                            @endif
                        </p>

                        @if(!empty($author->linkedin))
                            <a 
                                href="{{ $author->linkedin }}"
                                target="_blank"
                                class="inline-block mr-4 align-middle" 
                                title="View {{ $author->first_name }} on LinkedIn"
                            >
                                <img 
                                    src="{{ "{$dist_dir}ui/linkedin-pink.svg" }}"
                                    alt="LinkedIn Icon"
                                    width="42"
                                    height="42"
                                />
                            </a>
                        @endif

                        @if( !empty($author->url) )
                        <a href="{{ route('theme', $author->url) }}" class="btn bg-pink text-teal inline-block align-middle">Contact</a>
                        @endif
                    </div>
                </div>
                @endif
            </div>
        </div>
    </section>

    @if( isset($related_insights) && $related_insights->isNotEmpty() )
    <section class="insights-section bg-grey-100">
        <div class="container flex flex-wrap">
            <div class="waypoint w-full pt-10 pb-5 md:pt-14 md:pb-5 title">
                <h2 class="text-4xl md:text-5xl text-center text-teal">Related Insights</h2>
            </div>

            @foreach($related_insights as $related_insight)
                <div class="waypoint w-full md:w-6/12 md:px-8 mb-12 md:mb-14">
                    @include('insight.partials.card', ['insight' => $related_insight])
                </div>
            @endforeach

            <div class="waypoint w-full text-center pt-10 pb-10 md:pb-16">
                <a href="{{ route('theme', $insights_page->url) }}" class="btn bg-pink text-teal">View all</a>
            </div>
        </div>
    </section>
    @endif

    @include('partials.newsletter-signup')

    @include('expertise.partials.other', [
        'other_expertise' => $other_expertise,
        'title' => 'Our Expertise',
        'subtitle' => 'Keoghs is the leading provider of claims-related services to insurers, businesses and other suppliers to the insurance sector',
    ])

    <?php $Banner = \Modules\Banners\Theme\Models\Banner::first(); ?>
    @if( $Banner )
    @include('page-builder.blocks.banner', ['banner' => $Banner])
    @endif

    @push('footer_scripts')
    <script>
    var copyBtn = document.querySelector('[data-copy-to-clipboard]');
    copyBtn.addEventListener('click', function (evt) {
        evt.preventDefault();
        var textArea = document.createElement("textarea");
        textArea.style.position = 'fixed';
        textArea.style.top = 0;
        textArea.style.left = 0;
        textArea.style.width = '2em';
        textArea.style.height = '2em';
        textArea.style.padding = 0;
        textArea.style.border = 'none';
        textArea.style.outline = 'none';
        textArea.style.boxShadow = 'none';
        textArea.style.background = 'transparent';
        textArea.value = window.location.href;
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();

        try {
            var successful = document.execCommand('copy');
            if (successful) {
                document.querySelector('[data-copied-to-clipboard]').classList.remove('hidden');
            } else {
                document.querySelector('[data-not-copied-to-clipboard]').classList.remove('hidden');
            }
        } catch (err) {
            document.querySelector('[data-not-copied-to-clipboard]').classList.remove('hidden');
        }

        setTimeout( function () {
            document.querySelector('[data-not-copied-to-clipboard]').classList.add('hidden');
            document.querySelector('[data-copied-to-clipboard]').classList.add('hidden');
        }, 2000);

        document.body.removeChild(textArea);
    });
    </script>
    @endpush

@endsection