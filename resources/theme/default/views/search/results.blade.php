@extends( 'templates.default' )

@section( 'content' )

    @include( 'partials.page-banner' )

    <div class="container py-4">
        @if( isset( $results ) && $results->isNotEmpty() )

            <?php $used_headings = []; ?>
            <div class="flex flex-wrap w-full -mx-4">
                @foreach( $results as $result )

                    @if( !in_array($result['section_heading'], $used_headings) )
                    <div class="w-full mb-4 px-4">
                        <h3 class="text-3xl md:mb-3 md:text-4xl text-teal">{{ $result['section_heading'] }}</h3>
                    </div>
                    <?php $used_headings[] = $result['section_heading']; ?>
                    @endif

                    <div class="w-full lg:w-1/2 mb-8 px-4">
                        <div class="rounded-lg card transform shadow-card p-4">
                            <a href="{{ $result['url'] }}" class="over"></a>

                            <h3 class="text-3xl md:mb-3 md:text-4xl text-teal">
                                {{ $result['title'] }}
                            </h3>

                            <p class="mb-4 text-pink text-xl">
                                {{ $result['url'] }}
                            </p>

                            <div>
                                <a href="{{ $result['url'] }}" class="btn bg-pink">
                                    Read more...
                                </a>
                            </div>

                        </div>
                    </div>

                @endforeach
            </div>

            <div class="pagination">
                {{ $results->links() }}
            </div>

        @else

            @if( isset( $term ) && !empty( $term ) )
                <h2 class="text-3xl">Sorry, no results could be found for '{{ $term }}'.</h2>
                <h3 class="text-xl">Check for spelling errors, or try a less specific search.</h3>
            @else
                <h2 class="text-3xl">Please fill in the search form.</h2>
            @endif

        @endif
    </div>

@endsection