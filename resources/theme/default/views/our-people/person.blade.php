@extends( 'templates.default' )

@section( 'content' )

    @include('partials.page-hero', [
        'hero_image' => $our_people_page->image['desktop']['src'] ?? "{$dist_dir}ui/h1.jpg",
        'title' => $our_people_page->h1_title,
    ])

    <section class="person-section">
        <div class="container flex flex-wrap items-start pt-6 pb-8 md:pb-16">

            <div class="waypoint w-full md:w-4/12 px-8">
                @if( !empty($this_page->image) && !empty($this_page->image['primary']) )
                <img 
                    src="{{ $this_page->image['primary']['src'] }}"
                    alt="{{ $this_page->image['primary']['alt'] }}"
                    width="{{ $this_page->image['primary']['width'] }}"
                    height="{{ $this_page->image['primary']['height'] }}"
                    class="rounded-full"
                    loading="lazy"
                />
                @endif
            </div>

            <div class="waypoint w-full md:w-6/12 pt-8 md:pt-2">
                <div class="top border-b-2 border-teal border-solid pb-4">
                    <h2 class="text-3xl md:text-5xl text-teal mb-2 md:mb-3">{{ $this_page->full_name }}</h2>
                    <p class="md:text-f1 text-teal mb-2">{{ $this_page->job_title }}{{ !empty($this_page->location) ? ' - ' . $this_page->location : '' }}</p>
                    <p class="md:text-f1 text-teal mb-7">{{ $this_page->expertise_area }}</p>

                    @if(!empty($this_page->phone))
                        <a href="tel:{{ $this_page->phone }}" class="inline-block text-2xl mb-5 text-teal hover:underline"><strong>T: {{ $this_page->phone }}</strong></a>
                        <br>
                    @endif

                    @if(!empty($this_page->linkedin))
                        <a 
                            href="{{ $this_page->linkedin }}"
                            target="_blank"
                            class="inline-block mr-4 align-middle" 
                            title="View {{ $this_page->first_name }} on LinkedIn"
                        >
                            <img 
                                src="{{ "{$dist_dir}ui/linkedin-pink.svg" }}"
                                alt="LinkedIn Icon"
                                width="42"
                                height="42"
                            />
                        </a>
                    @endif

                    <a href="#contact-form" data-modal-inline data-contact-form-modal class="btn bg-pink text-teal inline-block align-middle">Contact</a>
                </div>

                <div class="bottom pt-8 md:pt-10 cms-content">
                    {!! $this_page->biography !!}
                </div>
            </div>

        </div>
    </section>

    @if( $authored_insights->isNotEmpty() )
    <section class="insights-section bg-grey-100">
        <div class="container flex flex-wrap">
            <div class="waypoint w-full pt-10 pb-5 md:pt-14 md:pb-5 title">
                <h2 class="text-4xl md:text-5xl text-center text-teal">Authored Insights</h2>
            </div>

            @foreach($authored_insights as $authored_insight)
                <div class="waypoint w-full md:w-6/12 md:px-8 mb-12 md:mb-14">
                    @include('insight.partials.card', ['insight' => $authored_insight])
                </div>
            @endforeach

            <div class="waypoint w-full text-center pt-1 pb-10 md:pb-16">
                <a href="{{ $insights_page->url }}" class="btn bg-pink text-teal">View all</a>
            </div>

        </div>
    </section>
    @endif

@endsection

@push('footer_scripts')
    @include('our-people.includes.contact-form', ['person' => $this_page])
@endpush