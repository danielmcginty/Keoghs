<div id="contact-form" data-modal="form" class="mfp-fade mfp-hide relative relative bg-blue w-full max-w-screen-md mx-auto p-8 shadow-lg">

    <button type="button" data-modal-close title="Close" class="flex items-center justify-center absolute top-0 right-0 m-2 text-white hover:text-black">
        <svg xmlns="http://www.w3.org/2000/svg" class="h-8 w-8" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
        <span class="sr-only">Close</span>
    </button>

    <div class="w-full mx-auto relative">
        @include('form-templates.partials.form-inner', [
            'form' => \Modules\FormBuilder\Theme\Models\PageBuilder\FormBuilder::getEmbedForm(1),
            'person' => $person,
            'button_class' => 'btn bg-white text-white arrow-pink inline-block align-middle'
        ])
    </div>

</div>