@extends( 'templates.default' )

@section( 'content' )

    @include('partials.page-hero', [
        'hero_image' => $this_page->image['desktop']['src'] ?? "{$dist_dir}ui/h1.jpg",
        'title' => $this_page->h1_title,
        'description' => $this_page->description ?? '',
    ])

    <section class="our-people-list-section">
        <div class="container flex flex-wrap items-start pt-10 md:pt-4 pb-14">

            <div class="w-full flex flex-wrap px-4 mb-8">
                <h4 class="text-3xl text-teal mb-8 flex items-center md:block justify-between">Search by Surname</h4>
                <div class="flex flex-wrap items-center justify-center">
                    @foreach( $surname_letters as $letter )
                        <a
                            data-letter-anchor
                            data-letter="{{ $letter }}"
                            href="#" 
                            class="inline-flex items-center justify-center w-12 h-12 text-3xl text-white bg-teal mr-4 mb-4"
                        >
                            {{ $letter }}
                        </a>
                    @endforeach
                </div>
            </div>

            <div class="waypoint w-full flex flex-wrap">
                <div class="flex flex-wrap" data-people>
                    @if( isset( $people ) && $people->isNotEmpty() )
                        <?php $used_letters = []; ?>
                        @foreach( $people as $index => $person )
                            @if( !in_array(substr($person->last_name, 0, 1), $used_letters) )
                            <div id="anchor-{{ strtoupper(substr($person->last_name, 0, 1)) }}" class="waypoint w-6/12 md:w-4/12 lg:w-1/5 px-4 mb-8">
                                <div class="h-full bg-teal text-white text-5xl flex items-center justify-center">
                                    {{ strtoupper(substr($person->last_name, 0, 1)) }}
                                </div>
                            </div>
                            <?php $used_letters[] = substr($person->last_name, 0, 1); ?>
                            @endif
                            <a href="{{ route('theme', $person->url) }}" class="waypoint w-6/12 md:w-4/12 lg:w-1/5 text-center px-4 text-grey-900 mb-8">
                                <div class="img-con">
                                    <img 
                                        src="{{ $person->image['primary']['src'] ?? '' }}"
                                        alt="{{ $person->image['primary']['alt'] ?? '' }}"
                                        width="{{ $person->image['primary']['width'] ?? '' }}"
                                        height="{{ $person->image['primary']['height'] ?? '' }}"
                                        loading="lazy"
                                        class="ml-auto mr-auto mb-7 mt-2 rounded-full" 
                                    />
                                </div>
                                <p class="text-base">
                                    <strong>{{ $person->full_name }}</strong> <br>
                                    {{ $person->job_title }}{{ !empty($person->location) ? ' - ' . $person->location : '' }}<br>
                                    {{ $person->expertise_area }}
                                </p>
                            </a>
                        @endforeach
                    @endif
                </div>
                
            </div>
        </div>
    </section>

    @include( 'page-builder.block-wrapper' )

@endsection

@push('footer_scripts')
<script>
    $('[data-letter-anchor]').on('click', function (evt) {
        evt.preventDefault();

        var letter = $(this).data('letter');
        var $target = $('#anchor-' + letter);
        if ($target.length) {
            $('html, body').animate(
                {
                    scrollTop: $target.offset().top - 60
                }, 
                750,
                'swing',
                function () {
                    $(window).trigger('scroll')
                }
            );
        }
    });
</script>
@endpush