@extends( 'templates.default' )

@section( 'content' )

    @include( 'partials.page-banner' )

    <div class="row flex flex-wrap lg:p-0 my-30 md:my-50">

        {{-- FAQs --}}
        <div class="flex flex-wrap w-full md:w-2/3 lg:w-3/4">
            @if( isset( $Faqs ) && $Faqs->isNotEmpty() )

                @foreach( $Faqs as $ind => $Faq )

                    <div data-accordion-item class="w-full mb-20">

                        <button data-accordion-toggle title="Show / Hide Answer" class="flex w-full bg-primary hover:bg-darken tr-shadow text-white pl-20 md:pl-30 pr-25 py-20">
                            <span class="text-lg md:text-2xl font-semibold w-full text-left leading-tighter">
                                {{ $Faq->question }}
                            </span>
                            <span class="pl-20">
                                <svg data-arrow xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="block size-text text-2xl fill-current">
                                    <path d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"></path>
                                </svg>
                            </span>
                        </button>

                        <div data-accordion-content class="overflow-hidden h-0">
                            <div class="cms-content px-15 pt-20 pb-30 md:p-30 md:pb-50">
                                {!! $Faq->answer !!}
                            </div>
                        </div>

                    </div>

                @endforeach

                <div class="pagination w-full lg:px-20 mb-30">
                    {!! $Faqs->links() !!}
                </div>

            @else
                
                <h2 class="text-3xl md:mt-20 font-semibold">
                    Sorry, no FAQs were found.
                </h2>

            @endif
        </div>

        {{-- Sidebar --}}
        <div class="w-full md:w-1/3 lg:w-1/4 md:pl-30 lg:px-20">

            @if( isset( $FaqCategories ) && $FaqCategories->isNotEmpty() )
                <div class="bg-gray-200 rounded-lg p-30 mb-30">

                    <h3 class="text-xl font-semibold mb-15">
                        Categories
                    </h3>

                    <ul>
                    @if( isset( $LandingPage ) && $LandingPage )
                        <li class="block">
                            <a href="{{ route('theme', $LandingPage->url) }}" class="block hover:underline py-5">
                                All FAQs
                            </a>
                        </li>
                    @endif
                    @foreach( $FaqCategories as $FaqCategory )
                        <li class="block">
                            <a href="{{ route('theme', $FaqCategory->url) }}" class="block hover:underline py-5">
                                {{ $FaqCategory->title }}
                            </a>
                        </li>
                    @endforeach
                    </ul>

                </div>
            @endif

        </div>

    </div>

@endsection