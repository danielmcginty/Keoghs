@extends( 'templates.default' )

@section( 'content' )

    @include( 'includes.breadcrumbs' )

    @include('partials.page-hero', [
        'hero_image' => $this_page->image['article']['src'] ?? "{$dist_dir}ui/h1.jpg",
        'title' => $this_page->title_long,
        'description' => $this_page->subtitle ?? '',
    ])

    <section class="content-section">
        <div class="container flex flex-wrap items-end">
            <div class="waypoint w-full mb-6">
                <div class="cms-content">
                    @if( !empty($this_page->introduction) )
                    {!! $this_page->introduction !!}
                    <br>
                    @endif
                    {!! $this_page->body !!}
                </div>
            </div>
        </div>
    </section>

    @if( $key_contacts->isNotEmpty() )
    <section class="key-contacts-section bg-teal">
        <div class="container">
            <div class="waypoint w-full pt-10 md:pt-12 pb-2 title">
                <h2 class="text-4xl md:text-5xl text-center text-white">Key Contacts</h2>
            </div>

            <div class="waypoint w-full key-slide slick pb-20">
                @foreach($key_contacts as $key_contact)
                    <a href="{{ route('theme', $key_contact->url) }}" class="text-center sm:px-8 text-white">
                        <div class="img-con">
                            <img class="ml-auto mr-auto mb-7 mt-4 rounded-full" src="{{ $key_contact->image['primary']['src'] ?? '' }}">
                        </div>
                        <p class="text-base"><strong>{{ $key_contact->full_name }}</strong> <br>{{ $key_contact->job_title }}<br>{{ $key_contact->expertise_area }}</p>
                    </a>
                @endforeach
            </div>

        </div>
    </section>
    @endif

    @if( $specialist_areas->isNotEmpty() )
    <div class="waypoint w-full over rounded-lg bg-grey-100 p-4 md:p-12 py-8 flex flex-wrap justify-between relative">
        <div class="absolute inset-0 bg-cover full-bg" style="background-image:url('{{ "{$dist_dir}ui/related-expertise.jpg" }}')"></div>
        <div class="w-full relative content-section">
            <div class="container rounded-lg bg-grey-100 p-4 md:p-12 py-8 flex flex-wrap justify-between">
                <h3 class="w-full text-3xl md:text-4xl text-center text-teal">Related Expertise</h3>

                @foreach($specialist_areas as $specialist_area)
                    <a href="{{ route('theme', $specialist_area->url) }}" class="w-full md:w-5/12 mb-8 hover:bg-tur bg-white border-l-8 border-solid border-tur px-4 py-7 flex justify-center items-center text-teal hover:text-white rounded-r-lg text-center">
                        <h5 class="text-2xl">{{ $specialist_area->title }}</h5>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
    @endif

    @if( $related_insights->isNotEmpty() )
    <section class="insights-section bg-grey-100">
        <div class="container flex flex-wrap">
            <div class="waypoint w-full pt-10 pb-5 md:pt-14 md:pb-5 title">
                <h2 class="text-4xl md:text-5xl text-center text-teal">Related Insights</h2>
            </div>

            @foreach($related_insights as $related_insight)
                <div class="waypoint w-full md:w-6/12 md:px-8 mb-12 md:mb-14">
                    @include('insight.partials.card', ['insight' => $related_insight, 'landing_page' => $insights_landing_page])
                </div>
            @endforeach

            <div class="waypoint w-full text-center pt-1 pb-10 md:pb-16">
                <a href="{{ route('theme', $insights_page->url) }}" class="btn bg-pink text-teal">View all</a>
            </div>

        </div>
    </section>
    @endif

    @include('expertise.partials.other', ['other_expertise' => $other_expertise, 'title' => 'Other Areas of Expertise'])

    @include( 'page-builder.block-wrapper' )

@endsection