<section class="our-expertise-section bg-teal pt-10 pb-12 md:pb-28">
	<div class="container flex flex-wrap text-white">

		<div class="waypoint w-full md:w-6/12 text-center ml-auto mr-auto pb-6 md:pb-8">
			<h2 class="text-4xl md:text-5xl text-pink">{{ $title }}</h2>
		</div>

		<div class="waypoint w-full text-center flex">
            @foreach($other_expertise as $expertise)
                <a href="{{ route('theme', $expertise->url) }}" class="w-full md:w-3/12 text-center px-8">
                    <div class="icon-con bg-white flex items-center justify-center">
                        @if(!is_null($expertise->svg_icon))
                            @include($expertise->svg_icon)
                        @elseif(!empty($expertise->icon) && !empty($expertise->icon['primary']))
                            <img src="{{ $expertise->icon['primary']['src'] }}" alt="{{ $expertise->icon['primary']['alt'] }}">
                        @endif
                    </div>

                    <h4 class="text-3xl">{{ $expertise->title }}</h4>
                </a>
            @endforeach
		</div>
	</div>
</section>