@extends( 'templates.default' )

@section( 'content' )

    <div class="bg-black text-white py-50">
        <div class="row">

            <h1 class="text-3xl font-bold">
                {{ $this_page->h1_title }}
            </h1>

        </div>
    </div>

    <div class="row flex flex-wrap px-0 py-50">

        @foreach( array_partition( $sitemap_urls, 3 ) as $column )

            <div class="w-full md:w-1/2 lg:w-1/3 px-20">

                @foreach( $column as $table_name => $pages )

                    <div class="mb-50">

                        <h3 class="text-xl font-bold mb-20">
                            {{ ucwords( str_replace( '_', ' ', $table_name ) ) }}
                        </h3>

                        <ul>
                            @foreach( $pages as $page )
                                <li>
                                    <a href="{{ route('theme', $page->url) }}" title="{{ $page->object->title }}" class="block py-5 hover:underline">
                                        {{ $page->object->title }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>

                    </div>

                @endforeach

            </div>

        @endforeach

    </div>

@endsection