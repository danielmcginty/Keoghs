<?php $random_id = uniqid(); ?>

<form data-custom-form data-form="{{ $form->form_id }}-{{ $random_id }}" action="#" method="post" enctype="multipart/form-data" class="relative">

    {{ csrf_field() }}

    {{ Form::hidden( 'form_id', $form->form_id ) }}
    {{ Form::hidden( 'page_url', url()->current() ) }}

    @if(isset($person) && $person instanceof \Modules\OurPeople\Theme\Models\Person)
        {{ Form::hidden( 'person', $person->person_id ) }}
    @endif

    <div data-field-wrap>

        @foreach( $form->structure as $index => $field )

            <div data-field="{{ $field['field_name'] }}">

                <label for="{{ $field['field_id'] }}" class="block font-normal not-italic mb-5">

                    {{ $field['name'] }}

                    @if( isset( $field['required'] ) )
                        <span class="text-error">*</span>
                    @endif

                </label>

                @if( $field['type'] == 'text' )
                    {{ Form::text( $field['field_name'], null, $field['field_attrs'] ) }}
                @elseif( $field['type'] == 'textarea' )
                    {{ Form::textarea( $field['field_name'], null, $field['field_attrs'] ) }}
                @elseif( $field['type'] == 'number' )
                    {{ Form::number( $field['field_name'], null, $field['field_attrs'] ) }}
                @elseif( $field['type'] == 'tel' )
                    {{ Form::tel( $field['field_name'], null, $field['field_attrs'] ) }}
                @elseif( $field['type'] == 'email' )
                    {{ Form::email( $field['field_name'], null, $field['field_attrs'] ) }}
                @elseif( $field['type'] == 'file' )
                    {{ Form::file( $field['field_name'], ['id' => $field['field_id']] ) }}
                @elseif( $field['type'] == 'radio' )

                    @if( !empty( $field['options'] ) )
                        <div class="flex flex-wrap">
                            @foreach( $field['options'] as $option )
                                <label class="inline-flex mt-5 mr-20">
                                    <input type="radio" name="{{ $field['field_name'] }}" value="{{ $option }}" class="form-radio text-2xl" />
                                    <span class="ml-10 select-none">{{ $option }}</span>
                                </label>
                            @endforeach
                        </div>
                    @endif

                @elseif( $field['type'] == 'checkbox' )

                    @if( !empty( $field['options'] ) )
                        <div class="flex flex-wrap">
                            @foreach( $field['options'] as $option )
                                <label class="inline-flex mt-5 mr-20">
                                    <input type="checkbox" name="{{ $field['field_name'] }}[]" value="{{ $option }}" class="form-checkbox text-2xl" />
                                    <span class="ml-10 select-none">{{ $option }}</span>
                                </label>
                            @endforeach
                        </div>
                    @endif

                @elseif( $field['type'] == 'select' )
                    {{ Form::select( $field['field_name'], $field['options'], null, $field['field_attrs'] ) }}
                @endif

            </div>

        @endforeach

        @include( 'form-templates.partials.gdpr-compliance', ['form' => $form] )

        <div class="flex flex-wrap items-center mt-30">

            @if( isset($button_class) && !empty($button_class) )
            <button type="submit" class="{{ $button_class }}">
                {{ $form->submit_button_text }}
            </button>
            @else
            <button type="submit" class="bg-primary hover:bg-darken tr-shadow text-white font-bold px-30 py-15 rounded-full">
                {{ $form->submit_button_text }}
            </button>
            @endif

            @if( $form->use_invisible_recaptcha )
                <div class="w-full text-center mt-20">

                    <div id="form-{{ $form->form_id }}-{{ $random_id }}-recaptcha-div" data-sitekey="{{ env('GOOGLE_INVISIBLE_RECAPTCHA_SITE_KEY') }}" data-callback="onSubmitInvisibleRecaptcha{{ $form->form_id }}{{ $random_id }}" data-size="invisible" class="absolute"></div>

                    <a href="https://policies.google.com/privacy?hl=en" target="_blank" class="block text-xs m-0">
                        <span class="inline-block vertical-middle my-5 mr-5">protected by</span>
                        <span class="inline-block vertical-middle">
                            <img class="inline-block vertical-middle margin-r2 margin-b0" style="width: 20px;" alt="Badge" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAC7IEhfAAAA/1BMVEUAAABygLUtX9E/hvSzs7MaNayzs7Ozs7Ozs7M9gvGzs7MaNaw/hvQaNayzs7M+hfM/hvSzs7Ozs7Ozs7Ozs7Ozs7MaNaw6e+oaNayzs7Ozs7MaNayzs7MaNayzs7Ozs7M/hvSzs7M8gO+zs7MaNayzs7Ozs7Ozs7M/hvSzs7MaNayzs7M3deWzs7M4eOgaNawaNaw/hvQ9gvGzs7M4d+c4duazs7MaNaw/hvQ2c+M/hvSzs7Ozs7M/hvQaNawaNaw/hvSzs7MaNayzs7M/hvSzs7MaNaw/hvQaNawzbN0aNawwZNazs7MaNawaNayzs7MzbN2zs7MaNaw/hvQ9gfAZZSlxAAAAUXRSTlMAAwXwBaXzsqQJ7bOy82n0vpJzGxAL/vbu3tXVzMHAqKGZhYNyYFlJQT0oJSIgGRAM/v787ezi393Zvbe0rqmRjouFeGtjY2FNREA6KiIbFRM194giAAABYUlEQVR4AY3Q156aUBSF8UURFDXGotGU2HvRlOm993Jg3v9ZZoTtEfYIP76rdfG/WgDUjBPszc7upM/vVfCklNCtejIKlwSpSuJZEikZpLLXBNLwes0xKEuorrM9N9tyOJT9mi+dB1P/HQYDcpG2Ce45UdCu2gRvHA5ZBGe5mLDhsHK3nePsZzj5w6EGYNFhFDj1kW93+wSBpx9BqGg+N0Hq6wpiHpAYrN3fB2ApCWJU8cPWGrYAVxJUErYfZqTTUvDkUdAR3JWwAUolF4SahJegyDHoyAbMMShYSQWbi+sQ1yGuQ5Qbh8HHoCsaFyHwe8nvSj+FKDAo5RSy4ocT4guHlFEow03vWkKQ5JBovdDv9/LbgpJSREQyLhRdCa1omF9Bs/w7yh0oBE0dpQhp0g9LB+j1MNfU4WXSuDI2MauHVeSAYlLw/jVfsKlx3vIz62yKsPRh+7BmCGHUzPZQgb93L9c5w0BCxz8AAAAASUVORK5CYII=" />
                            <span class="inline-block vertical-middle">reCAPTCHA</span>
                        </span>
                    </a>

                </div>
            @endif

        </div>

    </div>

    {{-- Loading State --}}
    <div data-loading class="absolute z-10 left-0 top-0 w-full h-full" style="display: none;">
        <div class="flex items-end justify-center w-full h-full relative">
            <div class="absolute z-10 left-0 top-0 w-full h-full bg-gray-100 opacity-80"></div>
            <div class="flex items-center relative z-20 text-2xl text-gray-700 font-semibold pb-50">
                <svg xmlns="http://www.w3.org/2000/svg" class="ani-spin-stepped fill-current size-text text-3xl mr-20" viewBox="0 0 512 512">
                    <path d="M304 48c0 26.51-21.49 48-48 48s-48-21.49-48-48 21.49-48 48-48 48 21.49 48 48zm-48 368c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.49-48-48-48zm208-208c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.49-48-48-48zM96 256c0-26.51-21.49-48-48-48S0 229.49 0 256s21.49 48 48 48 48-21.49 48-48zm12.922 99.078c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48c0-26.509-21.491-48-48-48zm294.156 0c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48c0-26.509-21.49-48-48-48zM108.922 60.922c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.491-48-48-48z"/>
                </svg>
                Sending...
            </div>
        </div>
    </div>

    {{-- Generic Error --}}
    <div data-alert data-alert-error class="w-full mt-20" style="display: none;">
        <div class="bg-error inline-flex rounded text-lg font-semibold text-white px-15 py-10 mx-auto">
            Please correct any validation errors.
        </div>
    </div>

    {{-- Upload Error --}}
    <div data-alert data-alert-file-error class="w-full mt-20" style="display: none;">
        <div class="bg-error inline-flex rounded text-lg font-semibold text-white px-15 py-10 mx-auto">
            There was a problem uploading your file(s). Please try again.
        </div>
    </div>

    {{-- Success Message --}}
    <div data-alert data-alert-success class="w-full" style="display: none;">
        <div class="bg-success cms-content inline-flex flex-col rounded text-lg font-semibold text-white px-25 py-20">
            Thank you for your enquiry. We'll be in touch shortly.
        </div>
    </div>

</form>

@if( $form->use_invisible_recaptcha )
    @push( 'footer_scripts' )

        <script type="text/javascript">
            function onSubmitInvisibleRecaptcha{{ $form->form_id }}{{ $random_id }}(){
                {{-- Our form has been submitted, so grab the form --}}
                var $form = $('form[data-custom-form][data-form="{{ $form->form_id }}-{{ $random_id }}"]');
                {{-- And set that we've reached the Recaptcha callback --}}
                $form.data('recaptcha-callback', true);
                {{-- And submit the form, allowing the 'g-recaptcha-response' field to be POSTed --}}
                $form.submit();
            }
        </script>

    @endpush
@endif