@if( isset( $form ) && $form && !empty( $form->gdpr_compliance_text ) )
    <div data-gdpr-field class="mb-20">

        {{-- Hidden field used to record the text displayed to the user --}}
        <input type="hidden" name="gdpr_consent_text" value="{{ $form->full_gdpr_compliance_text }}" />

        <label class="flex">

            @if( $form->gdpr_compliance )
            <input type="checkbox" name="gdpr_consent" value="1" class="form-checkbox text-2xl border-2 border-blue" />

            <span class="pl-10 pt-1">
            @else
            <span class="pt-1">
            @endif

                {{ $form->gdpr_compliance_text }}

                @if( $form->gdpr_compliance_opt_out_text )
                    <span class="block text-sm opacity-70 mt-5">
                        {{ $form->gdpr_compliance_opt_out_text }}
                    </span>
                @endif

            </span>

        </label>

    </div>
@endif