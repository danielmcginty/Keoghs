<div class="w-full max-w-700 px-20 py-25 md:p-30 lg:p-40 bg-gray-100 rounded-lg">

    <h3 class="mb-20 text-2xl font-semibold">{{ $form->heading }}</h3>

    @if( !empty( $form->description ) )
        <div class="cms-content mb-20">
            {!! $form->description !!}
        </div>
    @endif

    @include( 'form-templates.partials.form-inner', [ 'form' => $form ] )

</div>