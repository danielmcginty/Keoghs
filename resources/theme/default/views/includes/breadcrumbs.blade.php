@if( isset( $breadcrumbs ) && !empty( $breadcrumbs ) )
    <section class="breadcrum-section">
        <div class="container flex flex-wrap items-center">
            <div class="w-full pt-10 pb-10 md:pt-10 md:pb-20 text-white text-sm">
                @foreach( $breadcrumbs as $crumb )
                    @if( $loop -> last )
                        <strong>{{ $crumb['label'] }}</strong>
                    @else
                        <a href="{{ $crumb['url'] }}">{{ $crumb['label'] }}</a> /
                    @endif
                @endforeach
            </div>
        </div>
    </section>
@endif