@if( isset( $article ) && $article )
<?php
$schema_array = [
    '@context' => 'http://schema.org',
    '@type' => 'Article',
    'headline' => $article->h1_title,
    'url' => route('theme', $article->url),
    'mainEntityOfPage' => route('theme', $article->url),
    'publisher' => [
        '@type' => 'Organization',
        'name' => 'Red Flag Alert',
        'logo' => [
            '@type' => 'ImageObject',
            'url' => $dist_dir . 'assets/ui/schema-logo.png'
        ]
    ],
    'datePublished' => date("Y-m-d H:i:s", strtotime((!is_null($article->updated_at) ? $article->updated_at : $article->created_at))),
    'dateModified' => date("Y-m-d H:i:s", strtotime((!is_null($article->updated_at) ? $article->updated_at : $article->created_at)))
];

if( $article->image ){
    $schema_array['image'] = [
        '@type' => 'ImageObject',
        'width' => $article->image['schema']['width'],
        'height' => $article->image['schema']['height'],
        'url' => $article->image['schema']['src']
    ];
}
if( $article->author ){
    $schema_array['author'] = [
        '@type' => 'Person',
        'name' => $article->author->name
    ];
    if( $article->author->role ){
        $schema_array['author']['hasOccupation'] = [
            '@type' => 'Occupation',
            'name' => $article->author->role
        ];
    }
}
$schema_json = json_encode( $schema_array, JSON_PRETTY_PRINT );
?>
<script type="application/ld+json">
{!! $schema_json !!}
</script>

@endif