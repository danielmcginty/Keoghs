<div class="mt-40 bg-primary py-50">
    <div class="row flex flex-wrap">

        <div class="w-full lg:w-7/12 lg:pr-10">
            <h3 class="text-3xl md:text-5xl xl:text-6xl font-black text-white max-w-600 mb-30">Scorecard Heading</h3>
            <div class="md:text-2xl max-w-600 text-white mb-30 lg:mb-50">
                <p>Blurb about why a user should complete the website's scorecard.</p>
            </div>
            <a href="{{ route('theme') }}" class="inline-flex bg-black hover:bg-gray-900 text-white font-bold px-30 py-10 rounded-full">
                Take the test
            </a>
        </div>

        <div class="inline-block lg:w-5/12 lg:pl-10 mt-20 lg:mt-0">
            <p>Graphic to go here</p>
        </div>

    </div>
</div>