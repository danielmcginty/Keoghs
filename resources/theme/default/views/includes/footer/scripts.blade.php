{{-- instant.page --}}
<script src="https://instant.page/3.0.0" type="module" defer integrity="sha384-OeDn4XE77tdHo8pGtE1apMPmAipjoxUQ++eeJa6EtJCfHlvijigWiJpD7VDPWXV1"></script>

{{-- Vendor Scripts --}}
<script src="{{ auto_version( $dist_dir . 'js/jquery/jquery-3.5.1.min.js' ) }}"></script>

{{-- Site Scripts --}}
<script src="{{ auto_version( $dist_dir . 'js/app.js' ) }}" defer></script>
@stack( 'footer_scripts' )

{{-- Dynamic Scripts --}}
@foreach( Document::getDynamicScripts() as $script )
    {!! $script !!}
@endforeach

{{-- Regenerate Form Token (for Cloudflare) --}}
<?php $CF = new \Adm\Library\Cloudflare\Cloudflare(); ?>
@if( $CF->get_status() )
<script>var CF_STATUS = true;</script>
@endif
@if( isset( $current_url_object ) && $current_url_object )
<script>var _ADM_URL_ID = parseInt('{{ $current_url_object->id }}');</script>
@endif

{{-- Tracking Scripts --}}
@if( config('app.env') == 'production' )
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-5622512-5', 'auto');
  ga('send', 'pageview');
</script>
{{-- Hotjar Tracking Code for www.keoghs.co.uk --}}
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1264552,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

<noscript>
    <img src="https://ws.zoominfo.com/pixel/6256a35517cd08001bada037" width="1" height="1" style="display: none;" alt="websights"/>
</noscript>
@endif