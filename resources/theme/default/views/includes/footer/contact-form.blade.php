<?php
// Form ID 2 = Quick Contact Form (Footer)
$Form = \Modules\FormBuilder\Theme\Models\PageBuilder\FormBuilder::getEmbedForm( 2 );
?>
@if( $Form )
    <div class="mt-40 md:mt-75 bg-gray-200">
        <div class="row py-50">

            <div class="flex flex-wrap items-start md:-mx-20">
                <div class="w-full md:w-1/2 md:px-20">

                    {{-- Form --}}
                    <h3 class="mb-20 text-2xl font-semibold">{{ $Form->heading }}</h3>

                    @if( !empty( $Form->description ) )
                        <div class="cms-content mb-20">
                            {!! $Form->description !!}
                        </div>
                    @endif

                    @include( 'form-templates.partials.form-inner', [ 'form' => $Form ] )

                </div>
                <div class="w-full md:w-1/2 md:px-20">

                    {{-- Graphic --}}
                    <p>Graphic (or whatever is required) to go here</p>

                </div>
            </div>

        </div>
    </div>
@endif