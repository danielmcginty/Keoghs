@if( isset( $is_ie ) && $is_ie )
<div data-outdated-message style="position: fixed; z-index: 9999; width: 100%; left: 0; bottom: 0; padding: 20px 50px; text-align: center; background-color: #cb463d; color: #fff;">
    <div style="display: table; margin: 0 auto;">
        <div style="display: table-cell; vertical-align: middle;">
            <h3 style="font-size: 20px; color: #fff; margin: 0 10px 10px 0;">Your browser is out-of-date.</h3>
        </div>
        <div style="display: table-cell; vertical-align: middle;">
            <a href="https://bestvpn.org/outdatedbrowser/en" target="_blank" style="display: inline-block; border: 3px solid #fff; font-size: 20px; font-weight: 600; background-color: transparent; padding: 10px 20px; margin: 0 10px 10px;">How do I update my browser?</a>
        </div>
    </div>
    <p style="margin: 15px 0 0 0;">Update your browser to view this website correctly. Upgrading your browser will increase security and improve your experience on all websites.</p>
</div>
@endif