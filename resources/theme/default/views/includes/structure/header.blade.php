<?php

    $MenuHelper = load_helper( 'MenuHelper' );
    $header_menu = $MenuHelper::getMenu( 'header_menu' );
    $subheader_menu = $MenuHelper::getMenu( 'subheader_menu' );

    $search_page = \App\Models\Url::getSystemPage( 'search', 'search' );

    $phone_number = translate( 'telephone_number' );

    $social_links = array_filter([
        'facebook' => translate('social_facebook'),
        'twitter' => translate('social_twitter'),
        'linkedin' => translate('social_linkedin'),
    ]);

    $header_action_url  = translate('header_action_url');
    $header_action_text = translate('header_action_text');

?>
<header class="main-header">
	<div class="bg-teal">
		<div class="container mx-auto flex flex-wrap items-center justify-between">
			<a href="/" class="site-logo">
				<img src="{{ "{$dist_dir}ui/logo.svg" }}" width="122" height="34">
			</a>

			@if( $header_menu )
				<nav>
					<ul class="main-menu remove-bullets">
						@foreach( $header_menu as $nav_item )
							<li class="{{ (isset( $nav_item['children'] ) && !empty( $nav_item['children'] )) ? 'has-child' : '' }}{{ (isset( $nav_item['is_current'] ) && !empty( $nav_item['is_current'] )) ? ' current' : '' }}">
								<a
									href="{{ $nav_item['item_type'] == 'custom' ? $nav_item['link_url'] : route('theme', ['url' => $nav_item['link_url']]) }}"
									{{ isset($nav_item['item_target']) && $nav_item['item_target'] == '_blank' ? ' target="_blank"' : '' }}
								>
									<span>{{ $nav_item['link_label'] }}</span>
								</a>

								@if( isset( $nav_item['children'] ) && !empty( $nav_item['children'] ) )
									<div class="sub-menu-big bg-pink">
										<div class="container flex flex-wrap items-start">
											@foreach( $nav_item['children'] as $child_item )
												<div class="w-full md:w-1/5">
													<a
														href="{{ $child_item['item_type'] == 'custom' ? $child_item['link_url'] : route('theme', ['url' => $child_item['link_url']]) }}"
														{{ isset($child_item['item_target']) && $child_item['item_target'] == '_blank' ? ' target="_blank"' : '' }}
														class="text-xl text-white{{ (isset( $child_item['is_current'] ) && !empty( $child_item['is_current'] )) ? ' current' : '' }}"><strong>{{ $child_item['link_label'] }}</strong></a>
													@if( isset( $child_item['children'] ) && !empty( $child_item['children'] ) )
														<ul>
															@foreach($child_item['children'] as $grandchild_item)
																<li>
																	<a
																		href="{{ $grandchild_item['item_type'] == 'custom' ? $grandchild_item['link_url'] : route('theme', ['url' => $grandchild_item['link_url']]) }}"
																		{{ isset($grandchild_item['item_target']) && $grandchild_item['item_target'] == '_blank' ? ' target="_blank"' : '' }}
																		class="{{ (isset( $grandchild_item['is_current'] ) && !empty( $grandchild_item['is_current'] )) ? 'current' : '' }}"
																	>
																		<span>{{ $grandchild_item['link_label'] }}</span>
																	</a>
																</li>
															@endforeach
														</ul>
													@endif
												</div>
											@endforeach
										</div>
									</div>
								@endif
							</li>
						@endforeach
					</ul>

					@if($search_page)
						<form action="{{ route('theme', $search_page->url) }}">
							<input class="search" placeholder="Search" name="term">
						</form>
					@endif
				</nav>
			@endif

			@if($header_menu)
				<nav class="mobile-menu-con bg-pink">

					<form action="{{ route('theme', $search_page->url) }}">
						<input class="search" placeholder="Search…" name="term">
					</form>

					<ul class="mobile-main-menu remove-bullets">
						@foreach( $header_menu as $nav_item )
							<li class="{{ (isset( $nav_item['children'] ) && !empty( $nav_item['children'] )) ? 'has-child' : '' }}">
								<a
									href="{{ $nav_item['item_type'] == 'custom' ? $nav_item['link_url'] : route('theme', ['url' => $nav_item['link_url']]) }}"
									{{ isset($nav_item['item_target']) && $nav_item['item_target'] == '_blank' ? ' target="_blank"' : '' }}
								>
									{{ $nav_item['link_label'] }}
								</a>

								@if( isset( $nav_item['children'] ) && !empty( $nav_item['children'] ) )
									<ul>
										@foreach( $nav_item['children'] as $child_item )
											<li>
												<a
													class="{{ isset($child_item['children']) && !empty($child_item['children']) ? 'has-sub ' : '' }}arrow"
													href="{{ $child_item['item_type'] == 'custom' ? $child_item['link_url'] : route('theme', ['url' => $child_item['link_url']]) }}"
													{{ isset($child_item['item_target']) && $child_item['item_target'] == '_blank' ? ' target="_blank"' : '' }}
												>
													{{ $child_item['link_label'] }}
												</a>
												@if( isset( $child_item['children'] ) && !empty( $child_item['children'] ) )
													<div class="mob-sub">

														<a class="back">{{ $nav_item['link_label'] }}</a>

														<h4 class="text-3xl">
															<a 
																href="{{ $child_item['item_type'] == 'custom' ? $child_item['link_url'] : route('theme', ['url' => $child_item['link_url']]) }}"
																style="padding: 0; font-size: inherit; font-weight: inherit;"
															>{{ $child_item['link_label'] }}</a>
														</h4>

														<ul>
															@foreach($child_item['children'] as $grandchild_item)
																<li>
																	<a
																		href="{{ $grandchild_item['item_type'] == 'custom' ? $grandchild_item['link_url'] : route('theme', ['url' => $grandchild_item['link_url']]) }}"
																		{{ isset($grandchild_item['item_target']) && $grandchild_item['item_target'] == '_blank' ? ' target="_blank"' : '' }}
																	>
																		{{ $grandchild_item['link_label'] }}
																	</a>
																</li>
															@endforeach
														</ul>
													</div>
												@endif
											</li>
										@endforeach
									</ul>
								@endif
							</li>
						@endforeach

						@if( $subheader_menu )
							@foreach( $subheader_menu as $nav_item )
								<li class="{{ (isset( $nav_item['children'] ) && !empty( $nav_item['children'] )) ? 'has-child' : '' }}">
									<a
										href="{{ $nav_item['item_type'] == 'custom' ? $nav_item['link_url'] : route('theme', ['url' => $nav_item['link_url']]) }}"
										{{ isset($nav_item['item_target']) && $nav_item['item_target'] == '_blank' ? ' target="_blank"' : '' }}
									>
										{{ $nav_item['link_label'] }}
									</a>

									@if( isset( $nav_item['children'] ) && !empty( $nav_item['children'] ) )
										<ul>
											@foreach( $nav_item['children'] as $child_item )
												<li>
													<a
														class="{{ isset($child_item['children']) && !empty($child_item['children']) ? 'has-sub ' : '' }}arrow"
														href="{{ $child_item['item_type'] == 'custom' ? $child_item['link_url'] : route('theme', ['url' => $child_item['link_url']]) }}"
														{{ isset($child_item['item_target']) && $child_item['item_target'] == '_blank' ? ' target="_blank"' : '' }}
													>
														{{ $child_item['link_label'] }}
													</a>
													@if( isset( $child_item['children'] ) && !empty( $child_item['children'] ) )
														<div class="mob-sub">

															<a class="back">{{ $nav_item['link_label'] }}</a>

															<h4 class="text-3xl">
																<a 
																	href="{{ $child_item['item_type'] == 'custom' ? $child_item['link_url'] : route('theme', ['url' => $child_item['link_url']]) }}"
																	style="padding: 0; font-size: inherit; font-weight: inherit;"
																>{{ $child_item['link_label'] }}</a>
															</h4>

															<ul>
																@foreach($child_item['children'] as $grandchild_item)
																	<li>
																		<a
																			href="{{ $grandchild_item['item_type'] == 'custom' ? $grandchild_item['link_url'] : route('theme', ['url' => $grandchild_item['link_url']]) }}"
																			{{ isset($grandchild_item['item_target']) && $grandchild_item['item_target'] == '_blank' ? ' target="_blank"' : '' }}
																		>
																			{{ $grandchild_item['link_label'] }}
																		</a>
																	</li>
																@endforeach
															</ul>
														</div>
													@endif
												</li>
											@endforeach
										</ul>
									@endif
								</li>
							@endforeach
						@endif
					</ul>
				</nav>
			@endif

			<div id="burger" class="burger clearfix close">
				<div class="line1"></div>
			</div>
		</div>
	</div>
	@if( $subheader_menu )
		<div class="bg-white">
			<div class="container mx-auto flex flex-wrap items-center justify-end">

				<nav>
					<ul class="small-menu remove-bullets">
						@foreach( $subheader_menu as $nav_item )
							<li class="{{ (isset( $nav_item['children'] ) && !empty( $nav_item['children'] )) ? 'has-child' : '' }}">
								<a
									href="{{ $nav_item['item_type'] == 'custom' ? $nav_item['link_url'] : route('theme', ['url' => $nav_item['link_url']]) }}"
									{{ isset($nav_item['item_target']) && $nav_item['item_target'] == '_blank' ? ' target="_blank"' : '' }}
								>
									{{ $nav_item['link_label'] }}
								</a>

								@if( isset( $nav_item['children'] ) && !empty( $nav_item['children'] ) )
									<div class="sub-menu-small bg-pink">
										<ul class=" text-white">
											@foreach($nav_item['children'] as $child_item)
												<li>
													<a
														href="{{ $child_item['item_type'] == 'custom' ? $child_item['link_url'] : route('theme', ['url' => $child_item['link_url']]) }}"
														{{ isset($child_item['item_target']) && $child_item['item_target'] == '_blank' ? ' target="_blank"' : '' }}
													>
														{{ $child_item['link_label'] }}
													</a>
												</li>
											@endforeach
										</ul>
									</div>
								@endif
							</li>
						@endforeach
					</ul>
				</nav>
			</div>
		</div>
	@endif
</header>