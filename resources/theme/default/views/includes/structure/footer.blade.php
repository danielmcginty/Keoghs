@if( false )
    <html>
    <body>
    @endif

    <?php
    $social_links = array_filter([
        'facebook' => translate('social_facebook'),
        'twitter' => translate('social_twitter'),
        'linkedin' => translate('social_linkedin'),
    ]);

    $MenuHelper = load_helper('MenuHelper');
    $footer_menu = $MenuHelper::getMenu('footer_menu');
    $bottom_links = $MenuHelper::getMenu('bottom_links');
    ?>

    <footer>
        <div class="bg-teal pt-10 md:pt-12 pb-8 md:pb-6">
            <div class="container flex flex-wrap text-white">
                <div class="w-full logo-con">
                    <a href="/" class="site-logo">
                        <img src="{{ "{$dist_dir}ui/logo.svg" }}" width="122" height="34" loading="lazy">
                    </a>
                </div>

                @if( !empty( $footer_menu ) )
                    @foreach( $footer_menu as $nav_item )
                        <div class="w-6/12 md:w-3/12">
                            <h6 class="">{{ $nav_item['link_label'] }}</h6>

                            @if( isset( $nav_item['children'] ) && !empty( $nav_item['children'] ) )
                                <nav>
                                    <ul class="footer-menu">
                                        @foreach( $nav_item['children'] as $child_item )
                                            <li>
                                                <a
                                                    href="{{ $child_item['item_type'] == 'custom' ? $child_item['link_url'] : route('theme', ['url' => $child_item['link_url']]) }}"
                                                    {{ isset($child_item['item_target']) && $child_item['item_target'] == '_blank' ? ' target="_blank"' : '' }}
                                                >
                                                    {{ $child_item['link_label'] }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </nav>
                            @endif
                        </div>
                    @endforeach
                @endif

                <div class="w-full md:w-3/12 mt-4 md:mt-0">
                    <div class="relative" style="width: 175px; height: 103px; max-width: 100%;">
                        <iframe id="sra-iframe" frameborder="0" scrolling="no" allowtransparency="true" src="" style="border: 0px; margin: 0px; padding: 0px; background-color: transparent; top: 0px; left: 0px; width: 100%; height: 100%; position: absolute;"></iframe>
                    </div>

                    <div class="social-links">
                        @foreach( $social_links as $platform => $url )
                            <a href="{{ $url }}">
                                @if ($platform == "twitter")
                                    <img src="{{ "{$dist_dir}ui/twitter.svg" }}" width="40" height="40" loading="lazy">
                                    <img src="{{ "{$dist_dir}ui/twitter-hover.svg" }}" width="40" height="40" loading="lazy">
                                @elseif($platform == "linkedin")
                                    <img src="{{ "{$dist_dir}ui/linkedin.svg" }}" width="40" height="40" loading="lazy">
                                    <img src="{{ "{$dist_dir}ui/linkedin-hover.svg" }}" width="40" height="40" loading="lazy">
                                @endif
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="bg-teal-dark pt-6 pb-6">
            <div class="container flex flex-wrap text-white">
                <div class="w-full md:w-3/12">
                    <a href="https://davies-group.com/" class="by">Davies Group</a>
                </div>

                <div class="w-full md:w-9/12">
                    <span class="copy">©2021  All Rights Reserved</span>

                    @if( !empty( $bottom_links ) )
                        <nav>
                            <ul class="footer-botton-menu">
                                @foreach( $bottom_links as $nav_item )
                                    <li>
                                        <a
                                            href="{{ $nav_item['item_type'] == 'custom' ? $nav_item['link_url'] : route('theme', ['url' => $nav_item['link_url']]) }}"
                                            {{ isset($nav_item['item_target']) && $nav_item['item_target'] == '_blank' ? ' target="_blank"' : '' }}
                                        >
                                            {{ $nav_item['link_label'] }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </nav>
                    @endif
                </div>
            </div>
        </div>
    </footer>

    {{-- Scripts + Schema --}}
    @include( 'includes.footer.scripts' )
    @include( 'includes.footer.schema' )

</body>
</html>