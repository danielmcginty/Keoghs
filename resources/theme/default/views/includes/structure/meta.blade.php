<!DOCTYPE html>
<html id="BTT" class="no-js" lang="en">
<head>

    <meta charset="utf-8" />
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=5.0, user-scalable=1" name="viewport" />

    <title>{{ $meta_title }} | {{ config('app.name', 'Laravel') }}</title>

    @if( !empty( $meta_description ) )
        <meta name="description" content="{{ $meta_description }}">
    @endif

    @if( $no_index )
        <meta name="robots" content="noindex, nofollow">
    @endif

    @if( !empty( $canonical_url ) )
        <link rel="canonical" href="{{ $canonical_url }}" />
    @endif

    @if( !empty( $amp_html_url ) )
        <link rel="amphtml" href="{{ $amp_html_url}}" />
    @endif

    @if( !empty( $social_meta ) )
        @foreach( $social_meta as $meta_item )
            <meta {{ $meta_item['type'] }}="{{ $meta_item['property'] }}" content="{!! $meta_item['content'] !!}" />
        @endforeach
    @endif

    @if( !empty( $extra_meta ) )
        @foreach( $extra_meta as $key => $value )
            <meta name="{{ $key }}" content="{!! $value !!}">
        @endforeach
    @endif

    {{-- CSRF Token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    {{-- Preloads/etc --}}
    <link rel="preload" href="{{ $dist_dir }}fonts/GothamHTF-Book.woff2" as="font" crossorigin="anonymous" />
    <link rel="preload" href="{{ $dist_dir }}fonts/GothamHTF-Medium.woff2" as="font" crossorigin="anonymous" />
    <link rel="preload" href="{{ $dist_dir }}fonts/GothamHTF-Bold.woff2" as="font" crossorigin="anonymous" />

    @stack('preloads')

    {{-- Site Styles --}}
    <link rel="stylesheet" href="{{ auto_version( $dist_dir . 'css/app.css' ) }}" />
    {{-- <link rel="stylesheet" href="{{ auto_version( $dist_dir . 'css/style.css' ) }}" /> --}}

    @stack('header_styles')

    {{-- Favicons --}}
    <link rel="apple-touch-icon" sizes="192x192" href="{{ $dist_dir }}ui/favicons/android-chrome-192x192.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ $dist_dir }}ui/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ $dist_dir }}ui/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ $dist_dir }}ui/favicons/favicon-16x16.png">
    <link rel="manifest" href="{{ $dist_dir }}ui/favicons/site.webmanifest" crossorigin="use-credentials">
    <link rel="mask-icon" href="{{ $dist_dir }}ui/favicons/safari-pinned-tab.svg" color="#ffffff">
    <link rel="shortcut icon" href="{{ $dist_dir }}ui/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <script>
        (function () {
            var zi = document.createElement('script');
            zi.type = 'text/javascript';
            zi.async = true;
            zi.referrerPolicy = 'unsafe-url';
            zi.src = 'https://ws.zoominfo.com/pixel/6256a35517cd08001bada037';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(zi, s);
        })();
    </script>


</head>

<body class="text-grey-900{{ isset($blue_hero) && $blue_hero ? ' bg-teal' : '' }}">

@if( false )
</body>
</html>
@endif