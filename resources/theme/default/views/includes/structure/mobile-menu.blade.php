<?php
    $MenuHelper = load_helper( 'MenuHelper' );
    $header_menu = $MenuHelper::getMenu( 'header_menu' );
?>

@if( $header_menu )

    <nav class="pushy pushy-right bg-white">
        <div class="pushy-content">

            <ul class="mb-20">

                @foreach( $header_menu as $nav_item )

                    @if( isset( $nav_item['children'] ) && !empty( $nav_item['children'] ) )

                        <li class="has-dropdown">

                            <div class="flex justify-between hover:bg-gray-200 tr-bg">
                                <a
                                    data-dropdown-link
                                    href="{{ $nav_item['item_type'] == 'custom' ? $nav_item['link_url'] : route('theme', ['url' => $nav_item['link_url']]) }}"
                                    {{ isset($nav_item['item_target']) && $nav_item['item_target'] == '_blank' ? ' target="_blank"' : '' }}
                                    class="block w-full pl-20 py-15 font-bold"
                                >
                                    {{ $nav_item['link_label'] }}
                                </a>
                                <button data-dropdown-toggle class="px-20 py-15">
                                    <svg viewBox="0 0 320 512" class="block size-text text-blue">
                                        <use href="#dropdown-caret" />
                                    </svg>
                                </button>
                            </div>

                            <div class="dropdown bg-gray-100 w-auto" style="display: none;">
                                <ul class="pt-5 pb-15">

                                    @foreach( $nav_item['children'] as $child_item )

                                        <li class="pushy-link">
                                            <a
                                                href="{{ $child_item['item_type'] == 'custom' ? $child_item['link_url'] : route('theme', ['url' => $child_item['link_url']]) }}"
                                                {{ isset($child_item['item_target']) && $child_item['item_target'] == '_blank' ? ' target="_blank"' : '' }}
                                                class="block text-black px-20 py-10 hover:underline"
                                            >
                                                {{ $child_item['link_label'] }}
                                            </a>
                                        </li>

                                    @endforeach

                                </ul>
                            </div>

                        </li>

                    @else

                        <li>
                            <a
                                href="{{ $nav_item['item_type'] == 'custom' ? $nav_item['link_url'] : route('theme', ['url' => $nav_item['link_url']]) }}"
                                {{ isset($nav_item['item_target']) && $nav_item['item_target'] == '_blank' ? ' target="_blank"' : '' }}
                                class="flex px-20 py-15 hover:bg-gray-200 tr-bg font-bold"
                            >
                                {{ $nav_item['link_label'] }}
                            </a>
                        </li>

                    @endif

                @endforeach
            </ul>

        </div>
    </nav>

    {{-- Mobile Menu Overlay --}}
    <div class="site-overlay hover:opacity-70 tr-opacity cursor-pointer" title="Close Menu">
        <span class="text-hidden">Close Menu</span>
    </div>

@endif