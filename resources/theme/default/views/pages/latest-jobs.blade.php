@extends( 'templates.default' )

@section( 'content' )

    @include( 'partials.page-banner' )

    <div>
        <iframe src="https://keoghshr.secure.force.com/hr/" id="jobs-iframe" class="jobs-iframe" width="100%" height="1750" frameborder="0" scrolling="yes"></iframe>
    </div>

    @include( 'page-builder.block-wrapper' )

@endsection

@push('footer_scripts')
    <script>
        // Selecting the iframe element
        var iframe = document.getElementById("jobs-iframe");

        // Adjusting the iframe height onload event
        iframe.onload = function(){
            iframe.style.height = iframe.contentWindow.document.body.scrollHeight + 'px';
        }
    </script>
@endpush