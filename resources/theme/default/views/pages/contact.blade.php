@extends( 'templates.default' )

@section( 'content' )

    @include( 'partials.page-banner' )

    <div>
        <div>
            @include('page-builder.blocks.locations', ['locations' => $locations, 'map_enabled' => true, 'full_map' => true])
        </div>
        <div class="bg-teal">
            <div class="container flex flex-wrap justify-center">
                <h4 class="w-full text-center text-2rem mb-8 text-white">Location</h4>
                <div class="w-full">
                    <div data-location-map class="w-full h-full clustered" style="min-height: 30rem;"></div>
                </div>
            </div>
        </div>
    </div>

    @include( 'page-builder.block-wrapper' )

@endsection

@push('footer_scripts')
<script>
    var locations = @json($locations);
</script>
@endpush