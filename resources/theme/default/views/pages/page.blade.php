@extends( 'templates.default' )

@section( 'content' )

    @if( !isset( $is_home_page ) || !$is_home_page )
        @include( 'partials.page-banner' )
    @endif

    @if( isset($is_home_page) && $is_home_page)
        @include('includes.breadcrumbs')
    @endif

    @include( 'page-builder.block-wrapper' )

@endsection