@extends( 'templates.default' )

@section( 'content' )

    <div class="bg-gray-200 relative">
        <div class="row flex flex-wrap items-center px-0 py-30 md:py-75">

            <div class="w-full md:w-4/12 px-20 mb-30 md:mb-0">
                <div class="max-w-400">
                    <div class="pb-square rounded-full bg-gray-300 bg-cover bg-center"@if( !empty( $this_page->image ) && !empty( $this_page->image['listing'] ) ) style="background-image:url('{{ $this_page->image['listing']['src'] }}')"@endif></div>
                </div>
            </div>

            <div class="w-full md:w-8/12 px-20">

                <h1 class="text-2xl md:text-3xl lg:text-4xl font-semibold">
                    {{ $this_page->h1_title }}
                </h1>

                @if( $this_page->job_title )
                    <div class="text-xl md:text-2xl mt-10 md:mt-20">
                        {{ $this_page->job_title }}
                    </div>
                @endif

                @if( $this_page->telephone || $this_page->email )
                    <div class="mt-30">

                        @if( $this_page->telephone )
                            <a href="tel:{{ str_replace( ' ', '', $this_page->telephone ) }}" class="block md:inline-flex hover:underline text-xl mt-10 mr-30">
                                {{ $this_page->telephone }}
                            </a>
                        @endif

                        @if( $this_page->email )
                            <a href="mailto:{{ $this_page->email }}" class="block md:inline-flex hover:underline text-xl mt-10">
                                {{ $this_page->email }}
                            </a>
                        @endif

                    </div>
                @endif

            </div>

        </div>
    </div>

    @include( 'page-builder.block-wrapper' )

@endsection