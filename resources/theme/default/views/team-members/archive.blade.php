@extends( 'templates.default' )

@section( 'content' )

    @include( 'partials.page-banner' )

    @if( isset( $TeamMembers ) && $TeamMembers->isNotEmpty() )

        <div class="row flex flex-wrap px-0 my-30 md:my-50">

            <div class="w-full mb-50">

                <form method="GET" action="{{ route('theme', $this_page->url) }}" class="relative max-w-400 mx-auto">

                    <label for="search-input" class="text-hidden">Search</label>
                    <input id="search-input" class="pl-20 pr-50 py-10 w-full rounded-full border-2 border-gray-300 focus:border-gray-400 focus:bg-gray-100 outline-none" data-search-input="" placeholder="Search the team..." name="search_term" type="text" value="{{ $search_term }}">

                    <button title="Search" type="submit" class="absolute right-0 top-0 bg-transparent h-full px-15 leading-none">
                        <svg xmlns="http://www.w3.org/2000/svg" class="block fill-current size-text text-xl" viewBox="0 0 512 512">
                            <path d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"></path>
                        </svg>
                        <span class="text-hidden">Search</span>
                    </button>

                </form>

            </div>

            @foreach( $TeamMembers as $index => $TeamMember )

                <div class="w-full md:w-1/2 lg:w-1/3 px-20 mb-30">

                    <a href="{{ route('theme', $TeamMember->url) }}" class="flex flex-col bg-gray-200 p-30 rounded-lg h-full">

                        <div class="pb-square bg-center bg-cover bg-gray-300 rounded-lg mb-20" @if( !empty( $TeamMember->image ) && !empty( $TeamMember->image['listing'] ) ) style="background-image:url('{{ $TeamMember->image['listing']['src'] }}')"@endif></div>

                        <div>

                            <h2 class="text-2xl font-semibold mb-10">
                                {{ $TeamMember->title }}
                            </h2>

                            @if( !empty( $TeamMember->job_title ) )
                                <h4 class="mb-10">{{ $TeamMember->job_title }}</h4>
                            @endif

                        </div>

                        <div class="pt-10 mt-auto">

                            <div class="inline-flex bg-black hover:bg-gray-900 text-white font-bold px-30 py-10 rounded-full">
                                Read More
                            </div>

                        </div>

                    </a>

                </div>

            @endforeach

            <div class="pagination w-full lg:px-20 my-30">
                {{ $TeamMembers->links() }}
            </div>

        </div>

    @endif

@endsection