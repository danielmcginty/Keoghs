@extends( 'templates.default' )

@section( 'content' )

    <div class="height-v25 bg-gradient-primary-40 flex">
        <div class="row flex items-end">
            <div class="large-6 white">
                <h1 class="margin-b60">Login</h1>
            </div>
        </div>
    </div>

    <div class="row padding-t40 margin-b40">
        <div class="flex || justify-center">
            <div class="large-6">

                <form action="{{ route('login') }}" method="post" role="form" class="shadow padding-30">
                    {{ csrf_field() }}

                    <h3 class="margin-b5">Login</h3>
                    <h5 class="font-weight-400 margin-b20">Login using your email address &amp; password</h5>

                    <div class="margin-b20">
                        <label for="email-address" class="margin-b10">Email Address</label>
                        <div class="relative">
                            <input type="email" name="email" id="email-address" class="width-100" style="padding-left: 30px;" required autofocus />
                            <span class="absolute top-0 left-0 || flex items-center height-100 || padding-l5">
                                <i class="fas fa-envelope fa-fw primary"></i>
                            </span>
                        </div>
                        @if( $errors->has( 'email' ) )
                            <div class="margin-t10 error">{{ $errors->first( 'email' ) }}</div>
                        @endif
                    </div>

                    <div class="margin-b20">
                        <label for="password" class="margin-b10">Password</label>
                        <div class="relative">
                            <input type="password" name="password" id="password" class="width-100" style="padding-left: 30px;" required />
                            <span class="absolute top-0 left-0 || flex items-center height-100 || padding-l5">
                                <i class="fas fa-key fa-fw primary"></i>
                            </span>
                        </div>
                        @if( $errors->has( 'password' ) )
                            <div class="margin-t10 error">{{ $errors->first( 'password' ) }}</div>
                        @endif
                    </div>

                    <div class="">
                        <div class="flex justify-between items-center">
                            <div class="inline-block">
                                <button class="button bg-primary white">
                                    <i class="fas fa-sign-in-alt fa-fw margin-r5"></i>
                                    Login
                                </button>
                            </div>
                            <div class="inline-block">
                                <a href="{{ url('/password/reset') }}" class="black hover-underline font-size-14">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>

@endsection