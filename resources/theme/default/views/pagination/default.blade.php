@if ($paginator->hasPages())

    <div data-block="pagination" class="flex flex-wrap text-4xl">

        @if( $paginator->currentPage() > 1 )

            <a href="{{ $paginator->url(1) }}" title="First Page" class="inline-flex items-center justify-center h-12 bg-gray-200 hover:bg-gray-300 font-semibold px-12 rounded-full mr-4 mb-4">
                <span class="text-xl m-0">First</span>
            </a>

            <a href="{{ $paginator->previousPageUrl() }}" title="Previous Page" class="inline-flex items-center justify-center h-12 bg-gray-200 hover:bg-gray-300 font-semibold px-12 rounded-full mr-4 mb-4">
                <span class="text-xl m-0">Previous</span>
            </a>

        @endif

        @foreach( $elements as $element )

            @if( is_string( $element ) )
                <div class="inline-flex items-center justify-center size-text font-semibold mr-4 mb-4">
                    {{ $element }}
                </div>
            @endif

            @if( is_array( $element ) )
                @foreach( $element as $page => $url )
                    <a href="{{ $url }}" title="Page {{ $page }}" class="inline-flex items-center justify-center w-12 h-12 font-semibold rounded-full mr-4 mb-4 {{ $page == $paginator->currentPage() ? 'bg-pink text-white' : 'bg-gray-200 hover:bg-gray-300' }}">
                        <span class="text-xl m-0">{{ $page }}</span>
                    </a>
                @endforeach
            @endif

        @endforeach

        @if( $paginator->currentPage() < ($paginator->total() / $paginator->perPage()) )

            @if( $paginator->hasMorePages() )

                <a href="{{ $paginator->nextPageUrl() }}" title="Next Page" class="inline-flex items-center justify-center h-12 bg-gray-200 hover:bg-gray-300 font-semibold px-12 rounded-full mr-4 mb-4">
                    <span class="text-xl m-0">Next</span>
                </a>

            @endif

            <a href="{{ $paginator->url($paginator->lastPage()) }}" title="Last Page" class="inline-flex items-center justify-center h-12 bg-gray-200 hover:bg-gray-300 font-semibold px-12 rounded-full mr-4 mb-4">
                <span class="text-xl m-0">Last</span>
            </a>

        @endif

    </div>

@endif