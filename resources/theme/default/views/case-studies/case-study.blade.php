@extends( 'templates.default' )

@section( 'content' )

    @include( 'includes.breadcrumbs' )

    <div class="row my-30">

        @if( !empty( $this_page->image ) && !empty( $this_page->image['desktop'] ) )
            <div class="max-w-1000 mb-20 lg:mb-0">
                <div class="pb-16:9 bg-center bg-cover rounded" style="background-image: url('{{ $this_page->image['desktop']['src'] }}');"></div>
            </div>
        @endif

        <div class="max-w-800 bg-white lg:px-30 lg:py-20 lg:-mt-30 lg:ml-30">

            <h1 class="text-2xl md:text-3xl font-semibold mb-20">
                {{ $this_page->h1_title }}
            </h1>

            <div class="cms-content mb-30">
                {!! $this_page->content !!}
            </div>

        </div>

    </div>

    @include( 'page-builder.block-wrapper' )

@endsection