<section class="hero-section{{ isset($blue_hero) && $blue_hero ? '-blue' : '' }}">
    <div class="full-bg" style="background-image:url({{ $hero_image ?? "{$dist_dir}ui/h1.jpg"}});">
        <div class="container flex flex-wrap items-end">
            <div class="w-full">
                <div class="theme-dark bg-teal bg-opacity-95 text-white border-l-8 border-solid border-pink px-5 md:px-8 py-8 md:py-12">
                    <h1 class="text-4xl md:text-5xl">{{ $title }}</h1>
                    @if(isset($description) && !empty($description))
                        <p class="text-xl md:text-2xl mb-0">{{ $description }}</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>