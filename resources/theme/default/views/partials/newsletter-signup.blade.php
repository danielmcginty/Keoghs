<section class="mid-section bg-tur">
    <div class="container text-white pt-8 pb-8 md:pt-4 md:pb-4">
        <div class="waypoint w-full md:flex md:flex-wrap md:items-center text-center justify-center">
            <h3 class="text-3xl md:text-4xl md:mb-0 md:mr-10">{{ translate('newsletter_signup_text') }}</h3>
            <a href="{{ translate('newsletter_signup_link') }}" class="btn bg-white btn-full text-tur btn-no-arrow">{{ translate('newsletter_signup_button_text') }}</a>
        </div>
    </div>
</section>