@if( isset( $this_page ) && $this_page )

    @include('includes.breadcrumbs')

    @include('partials.page-hero', [
        'hero_image' => $this_page->image['desktop']['src'] ?? null,
        'title' => $this_page->h1_title,
        'description' => $this_page->description,
        'blue_hero'  => isset($blue_hero) && $blue_hero
    ])

@endif