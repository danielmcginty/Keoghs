@if( !empty($this_page->intro_content_heading) || !empty($this_page->intro_content) )
<section class="content-section">
    <div class="container flex flex-wrap items-end pt-6 mb-12">
        <div class="waypoint w-full">
            @if(!empty($this_page->intro_content_heading))
            <h2 class="text-4xl md:text-5xl text-teal">{{ $this_page->intro_content_heading }}</h2>
            @endif
            @if(!empty($this_page->intro_content))
            <div class="text-f2">{!! $this_page->intro_content !!}</div>
            @endif
        </div>
    </div>
</section>
@endif