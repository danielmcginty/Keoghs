<section class="wave-section{{ !empty($banner->background_colour) ? ' bg-' . $banner->background_colour : '' }}">
	<div class="container flex flex-wrap items-start">

		<div class="waypoint w-full md:w-6/12 self-end order-2 md:order-1">
			<div class="inner">
				<img 
					src="{{ $banner->image['primary']['src'] }}"
					alt="{{ $banner->image['primary']['alt'] }}"
					width="{{ $banner->image['primary']['width'] }}"
					height="{{ $banner->image['primary']['height'] }}"
					loading="lazy"
				/>
			</div>
		</div>

		<div class="waypoint w-full md:w-6/12 md:px-8 lg:px-16 md:pb-52 text-white md:pt-12 order-1 md:order-2">
			<div class="inner">
				<h4 class="text-2xl md:text-3xl">{{ $banner->title }}</h4>

                <div class="cms-content">
                    {!! $banner->content !!}
                </div>

                @include('page-builder.partials.buttons', ['buttons' => $banner->buttons])
			</div>
		</div>
	</div>
</section>