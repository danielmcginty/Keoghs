@if( isset( $this_page ) && $this_page->page_builder )
    @foreach( $this_page->page_builder as $block )
        @if( view()->exists( 'page-builder.blocks.' . $block['reference'] ) )
            @include( 'page-builder.blocks.' . $block['reference'], $block['content'] )
        @endif
    @endforeach
@endif