<?php
// Spacing applied to the logos block
$spacing = 'my-30 md:my-50';
?>

@if( isset( $logos ) && $logos->isNotEmpty() )
<div data-block="logos-stacked" class="row {{ $spacing }}">

    {{-- Logo Wrapper --}}
    <div class="flex flex-wrap items-center justify-center -mx-20">
        {{-- Individual logos --}}
        @foreach( $logos as $Logo )
            @if( !empty( $Logo->image ) && !empty( $Logo->image['single'] ) )
                {{-- Logo column --}}
                <div class="w-1/2 md:w-1/3 lg:w-1/4 px-20">
                    <div class="flex justify-center">
                        {{-- Actual logo image --}}
                        <img src="{{ $Logo->image['single']['src'] }}" alt="{{ $Logo->title }}" />
                    </div>
                </div>
            @endif
        @endforeach

    </div>

</div>
@endif