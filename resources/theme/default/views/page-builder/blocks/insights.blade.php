@if( isset( $insights ) && $insights->isNotEmpty() )
    <section class="insights-section bg-grey-100">
        <div class="container flex flex-wrap">
            @if( !empty( $heading ) )
                <div class="w-full pt-10 pb-5 md:pt-6 md:pb-5 title">
                    <h2 class="waypoint text-4xl md:text-5xl text-center text-teal">{{ $heading }}</h2>
                </div>
            @endif

            @foreach( $insights as $insight )
                <div class="waypoint w-full md:w-6/12 md:px-8 mb-12 md:mb-14">
                    <div class="h-full card transform rounded-b-lg bg-white shadow-card">
                        <a href="{{ route('theme', $insight->url) }}" class="over"></a>
                        <img 
                            src="{{ $insight->image['listing']['src'] }}"
                            alt="{{ $insight->image['listing']['alt'] }}"
                            width="{{ $insight->image['listing']['width'] }}"
                            height="{{ $insight->image['listing']['height'] }}"
                            loading="lazy"
                            class="w-full" 
                        >

                        <div class="bottom p-6 md:p-8">
                            <h3 class="text-3xl md:mb-3 md:text-4xl text-teal">{{ $insight->title }}</h3>

                            @if( $insight->visible_categories->isNotEmpty() && isset($landing_page) )
                            <div class="cat-links">
                                @foreach( $insight->visible_categories as $category )
                                <a href="{{ route('theme', $landing_page->url) }}?categories={{ $category->category_id }}" class="text-white bg-grey-400 hover:bg-pink">{{ $category->title }}</a>
                                @endforeach
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach

            @if(isset($landing_page))
                <div class="waypoint w-full text-center pt-1 pb-10 md:pb-16">
                    <a href="{{ $landing_page->url }}" class="btn bg-pink text-teal">View all</a>
                </div>
            @endif
        </div>
    </section>
@endif