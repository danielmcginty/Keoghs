@if(isset($locations) && !empty($locations))
	<section data-locations-section class="locations-section bg-teal pb-12">
		<div class="container flex flex-wrap text-white">
			<div class="waypoint w-full md:w-7/12 map-side">
				<div data-map-container data-map="{{ isset($full_map) && $full_map ? 'full' : 'block' }}" class="map-con">
					@if( isset($full_map) && $full_map )
					<img 
						src="{{ "{$dist_dir}ui/map-full.svg" }}"
						alt="UK Map"
						width="772"
						height="891"
						loading="lazy"
						class=""
					/>
					@else
					<img 
						src="{{ "{$dist_dir}ui/map.svg" }}"
						alt="UK Map"
						width="792"
						height="753"
						loading="lazy"
						class=""
					/>
					@endif

					@foreach($locations as $location)
						<div 
							class="pin {{ $loop->first ? 'active' : '' }}" 
							data-pin="{{ $location->location_id }}"
							data-lat="{{ $location->latitude }}"
							data-lng="{{ $location->longitude }}"
						></div>
					@endforeach
				</div>
			</div>
			<div class="waypoint w-full md:w-5/12 md:px-4 pt-10 md:pt-52">
				<h2 class="text-4xl md:text-5xl">Keoghs Locations</h2>

				<select data-location-dropdown>
					<option value="#">Select from List or Map</option>
					@foreach($locations as $location)
						<option 
							value="{{ $location->location_id }}" 
							data-latitude="{{ $location->latitude }}"
							data-longitude="{{ $location->longitude }}"
							{{ $loop->first ? ' selected' : '' }}
						>{{ $location->title }}</option>
					@endforeach
				</select>

				@foreach($locations as $location)
					<div class="location {{ $loop->first ? 'active' : '' }}" data-location="{{ $location->location_id }}">
						<h4 class="text-3xl">{{ $location->title }}</h4>

						<address>
							{!! $location->address !!}
						</address>

						<a href="{{ $location->telephone }}">T: {{ $location->telephone }}</a><br>
						<div class="cms-content">{!! $location->contact_details !!}</div><br>

						@if(isset($map_enabled) && $map_enabled)
							<button
								class="btn bg-pink text-white"
								data-map-position
								data-latitude="{{ $location->latitude }}"
								data-longitude="{{ $location->longitude }}"
							>
								View Map
							</button>
							<br>
						@endif

						@if( false && isset($PeoplePage) && $PeoplePage )
						<a href="{{ route('theme', $PeoplePage->url) }}?locations={{ \Illuminate\Support\Str::slug($location->people_filter) }}" class="btn bg-pink text-white">Our People at this Location</a>
						@endif
					</div>
				@endforeach
			</div>
		</div>
	</section>
@endif