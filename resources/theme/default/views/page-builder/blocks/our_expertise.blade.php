@if( isset( $expertise ) && $expertise->isNotEmpty() )
    <section class="our-expertise-section bg-teal pt-10 pb-16 md:pb-28">
        <div class="container flex flex-wrap text-white">

            <div class="waypoint w-full md:w-6/12 text-center ml-auto mr-auto pb-8">
                @if( !empty( $heading ) )
                    <h2 class="text-4xl md:text-5xl">{{ $heading }}</h2>
                @endif
                <div class="cms-content">
                    {!! $description !!}
                </div>
            </div>

            <div class="waypoint w-full text-center flex">
                @foreach( $expertise as $article )
                    <a href="{{ route('theme', $article->url) }}" class="w-full md:w-1/5 text-center px-8">
                        <div class="icon-con bg-white flex items-center justify-center">
                            @if(!is_null($article->svg_icon))
                                @include($article->svg_icon)
                            @elseif(!empty($article->icon) && !empty($article->icon['primary']))
                                <img src="{{ $article->icon['primary']['src'] }}" alt="{{ $article->icon['primary']['alt'] }}">
                            @endif
                        </div>

                        <h4 class="text-3xl">{{ $article->title }}</h4>
                    </a>
                @endforeach
            </div>

        </div>
    </section>
@endif