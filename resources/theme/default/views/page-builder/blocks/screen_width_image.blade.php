@if( isset( $image ) && !empty( $image ) && !empty( $image['desktop'] ) )

    <?php $random_id = uniqid(); ?>

    @push('header_styles')
        <style>

            [data-screen-image="{{ $random_id }}"] {
                background-image: url('{{ $image['mobile']['src'] }}');
                padding-bottom: {{ $image['desktop']['ratio'] }};
            }

            @media (min-width: 640px){
                [data-screen-image="{{ $random_id }}"] {
                    background-image: url('{{ $image['desktop']['src'] }}');
                    padding-bottom: {{ $image['desktop']['ratio'] }};
                }
            }

        </style>
    @endpush

    <div data-block="screen-width-image" data-screen-image="{{ $random_id }}" class="bg-gray-100 bg-cover bg-center"></div>

@endif