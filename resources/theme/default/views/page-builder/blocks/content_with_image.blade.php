<div data-block="content-with-image" class="content-section">
    <div class="wide-container items-end pt-6 mb-12 mx-auto">
        <div class="w-full flex flex-wrap bg-grey-100">
            <div class="w-full md:w-1/2 px-10 {{ isset( $alignment ) && $alignment ? 'lg:order-0' : 'lg:order-1' }}">
                @if( isset( $video_url ) && !empty( $video_url ) )

                    {{-- Video modal --}}
                    <a data-modal-video href="{{ $video_url }}" title="Play Video" class="image-shift block relative bg-center bg-cover rounded-lg bg-gray-200">

                        @if( !empty($image) && !empty($image['single']) )
                            <img
                                src="{{ $image['single']['src'] }}"
                                alt="{{ $image['single']['alt'] }}"
                                width="{{ $image['single']['width'] }}"
                                height="{{ $image['single']['height'] }}"
                                class="rounded-lg"
                                loading="lazy"
                            />
                        @endif

                        {{-- Video overlay --}}
                        <span class="absolute inset-0 bg-black opacity-40 hover:opacity-25 tr-opacity"></span>

                        {{-- Play button --}}
                        <span class="absolute inset-0 flex flex-col items-center justify-center">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-36 w-36 text-white" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM9.555 7.168A1 1 0 008 8v4a1 1 0 001.555.832l3-2a1 1 0 000-1.664l-3-2z" clip-rule="evenodd" />
                            </svg>
                        </span>

                    </a>
                @elseif( !empty( $image ) && !empty( $image['single'] ) )
                    {{-- Image --}}
                    <img
                        src="{{ $image['single']['src'] }}"
                        alt="{{ $image['single']['alt'] }}"
                        width="{{ $image['single']['width'] }}"
                        height="{{ $image['single']['height'] }}"
                        class="image-shift rounded-lg"
                        loading="lazy"
                    />
                @endif
            </div>
            <div class="w-full md:w-1/2 px-10 py-12 {{ isset( $alignment ) && $alignment ? 'lg:order-1' : 'lg:order-0' }}">
                @if( !empty( $heading ) )
                <h3 class="text-3xl text-teal">{{ $heading }}</h3>
                @endif
                @if( !empty( $content ) )
                    <div class="cms-content mb-30">
                        {!! $content !!}
                    </div>
                @endif

                {{-- Buttons --}}
                @if( !empty( $buttons ) )
                    @include( 'page-builder.partials.buttons', ['buttons' => $buttons, 'default_class' => 'bg-pink'] )
                @endif
            </div>
        </div>
    </div>
</div>