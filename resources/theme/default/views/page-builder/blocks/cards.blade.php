@if(isset($cards) && $cards->isNotEmpty())
<div data-block="cards" class="container mb-16">
    @foreach( $cards as $card )
        <div data-accordion class="accordion-card shadow-card{{ $loop->first ? ' open none-under' : '' }}{{ $loop->last ? ' none-under' : '' }}">
            <div data-accordion-toggle class="accordion-card-toggle bg-{{ $card->colour }} p-4 md:py-4 md:px-12 rounded-t-3xl cursor-pointer">
                <div class="flex items-center justify-between">
                    <h4 class="text-white text-2xl mb-0">{{ $card->title }}</h4>
                    <svg data-toggle-icon xmlns="http://www.w3.org/2000/svg" class="toggle-icon transform transition-transform text-white h-8 w-8{{ $loop->first ? ' rotate-180' : '' }}" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
                    </svg>
                </div>
            </div>
            <div data-accordion-content class="accordion-card-content bg-{{ $card->colour }} text-white" style="{{ !$loop->first ? 'display:none;' : '' }}">
                <div class="accordion-wave">
                    <div class="flex flex-wrap items-start p-4 md:py-8 md:px-12">
                        <div class="waypoint w-full md:w-6/12 md:pr-8 mb-8 md:mb-0">
                            <div class="inner">
                                <div class="card-content">{!! $card->main_content !!}</div>
                            </div>
                        </div>

                        <div class="waypoint w-full md:w-6/12 self-end">
                            @if( !empty($card->image) && !empty($card->image['single']) )
                            <div class="inner">
                                <img 
                                    src="{{ $card->image['single']['src'] }}" 
                                    alt="{{ $card->image['single']['alt'] }}"
                                    width="{{ $card->image['single']['width'] }}"
                                    height="{{ $card->image['single']['height'] }}"
                                    loading="lazy"
                                >
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="bg-teal text-white p-4 md:py-8 md:px-12{{ !$loop->first && !$loop->last ? ' pb-8 md:pb-16' : '' }}">
                    @if( !empty($card->logos) )
                        <div class="w-full flex flex-wrap items-center justify-center mb-8">
                            @if( !empty($card->prelogo_text) )
                            <p class="w-full text-center mb-4">{{ $card->prelogo_text }}</p>
                            @endif
                            @foreach($card->logos as $logo)
                                <img 
                                    src="{{ $logo['single']['src'] }}" 
                                    alt="{{ $logo['single']['alt'] }}" 
                                    width="{{ $logo['single']['width'] }}" 
                                    height="{{ $logo['single']['height'] }}" 
                                    loading="lazy"
                                    class="max-w-175" 
                                />
                            @endforeach
                        </div>
                    @endif

                    <div class="w-full flex flex-wrap mb-8">
                        <div class="w-full lg:w-1/2 lg:pr-4 mb-8 md:mb-0">
                            {!! $card->left_content !!}
                        </div>
                        <div class="w-full lg:w-1/2 lg:pl-4 mb-8 md:mb-0">
                            {!! $card->right_content !!}
                        </div>
                    </div>

                    <div class="w-full flex flex-wrap items-center justify-center">
                        @include('page-builder.partials.buttons', ['buttons' => $card->buttons, 'button_class' => 'btn bg-white btn-external'])
                    </div>
                </div>
            </div>
        </div>

        @if( $loop->first && $loop->count > 1 && !empty($other_heading) )
            <div class="my-8 text-center">
                <h3 class="text-2xl md:text-3xl text-teal">{{ $other_heading }}</h3>
            </div>
        @endif
    @endforeach
</div>
@push('footer_scripts')
<script>
$(document).on('click', '[data-accordion-toggle]', function (evt) {
    toggleAccordion($(this));
});
$(document).on('touchend', '[data-accordion-toggle]', function (evt) {
    toggleAccordion($(this));
});
function toggleAccordion($tog) {
    var $wrap = $tog.parent();
    var $cont = $wrap.find('[data-accordion-content]');
    var $icon = $tog.find('[data-toggle-icon]');

    if ($wrap.hasClass('open')) {
        $wrap.removeClass('open');
        $icon.removeClass('rotate-180');
        $cont.slideUp(300);
    }
    else {
        $wrap.addClass('open');
        $icon.addClass('rotate-180');
        $cont.slideDown(300);
    }
}
</script>
@endpush
@endif