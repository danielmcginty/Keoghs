<section class="our-people-section bg-tur pt-12 md:pt-16 pb-10 md:pb-14">
	<div class="container flex flex-wrap text-white">
		<div class="waypoint w-full md:w-8/12 text-center ml-auto mr-auto pb-7 md:pb-8">
            @if( !empty( $heading ) )
                <h2 class="text-4xl md:text-5xl">{{ $heading }}</h2>
            @endif
            <div class="cms-content">
                {!! $description !!}
            </div>
		</div>
	</div>

	<div class="waypoint img-con">
		@if( isset($image) && !empty($image) && !empty($image['single']) )
		<img
			src="{{ $image['single']['src'] }}"
			alt="{{ $image['single']['alt'] }}"
			width="{{ $image['single']['width'] }}"
			height="{{ $image['single']['height'] }}"
			loading="lazy"
		/>
		@else
		<img
			src="{{ $dist_dir . 'ui/people-v2.png' }}"
			alt="Our People"
			width="1949"
			height="218"
			loading="lazy"
		/>
		@endif
	</div>

	<div class="waypoint container flex flex-wrap text-white pt-10 md:pt-14">
		<div class="w-full md:w-6/12 text-center ml-auto mr-auto">
			<a href="{{ $link->url }}" class="btn bg-teal text-white">Find a Specialist</a>
		</div>
	</div>
</section>