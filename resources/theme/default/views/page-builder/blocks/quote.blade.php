@if( isset( $quote ) && !empty( $quote ) )
    <section class="working-section bg-teal full-bg pt-16 pb-14 md:pb-16 lazyload" data-done="false" data-img="{{ $background_image['single']['src'] }}">
        <div class="container flex flex-wrap text-white items-center">
            <div class="waypoint w-full md:w-6/12">
                <img 
                    src="{{ $background_image['single']['src'] }}"
                    alt="{{ $background_image['single']['alt'] }}"
                    width="{{ $background_image['single']['width'] }}"
                    height="{{ $background_image['single']['height'] }}"
                    loading="lazy"
                    class="md:hidden -mt-16 mb-10"
                />
            </div>

            <div class="waypoint w-full md:w-6/12 md:px-8 xl:px-24">
                <div class="p-8 bg-teal">
                    <h2 class="text-4xl md:text-5xl">Working at Keoghs</h2>

                    <blockquote class="text-2xl">{{ $quote }}</blockquote>

                    @include('page-builder.partials.buttons', ['buttons' => $buttons, 'default_class' => 'bg-pink'])
                </div>
            </div>

        </div>
    </section>
@endif