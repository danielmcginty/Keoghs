<?php
// Spacing applied to the news listing
$spacing = 'my-40 md:my-75';
?>

@if( isset( $team_members ) && $team_members->isNotEmpty() )
<div data-block="meet-the-team" class="row {{ $spacing }}">

    @if( !empty( $heading ) )
        {{-- Heading --}}
        <h3 class="text-3xl md:text-4xl font-semibold">{{ $heading }}</h3>

        {{-- Underline --}}
        <div class="pb-3 bg-secondary my-15 md:my-20 max-w-100"></div>
    @endif

    {{-- Team members --}}
    <div class="flex flex-wrap items-stretch lg:-mx-20">
        @foreach( $team_members as $TeamMember )

            {{-- Team member column --}}
            <div class="w-full lg:w-1/3 lg:px-20 mb-30 md:mb-40">

                <div class="flex flex-col bg-gray-200 h-full rounded-lg overflow-hidden">
                    {{-- Team member content --}}
                    <div class="flex flex-col justify-between h-full">
                        <div class="p-30">

                            {{-- Team member image --}}
                            <div class="pb-square bg-center bg-cover bg-gray-300 rounded-lg mb-20" @if( !empty( $TeamMember->image ) && !empty( $TeamMember->image['listing'] ) ) style="background-image:url('{{ $TeamMember->image['listing']['src'] }}')"@endif></div>

                            {{-- Team member title --}}
                            <a href="{{ route('theme', $TeamMember->url) }}">
                                <div class="text-xl md:text-2xl font-semibold mb-20 hover:underline">{{ $TeamMember->title }}</div>
                            </a>

                            {{-- Team member job title --}}
                            @if( !empty( $TeamMember->job_title ) )
                                <div class="mb-10">{{ $TeamMember->job_title }}</div>
                            @endif

                        </div>

                        {{-- Read more button --}}
                        <div class="p-30 pt-0">
                            <a href="{{ route('theme', $TeamMember->url) }}" class="inline-flex bg-primary hover:bg-darken tr-shadow text-white font-bold px-30 py-10 rounded-full">
                                Read More
                            </a>
                        </div>
                    </div>

                </div>
            </div>

        @endforeach
    </div>

</div>
@endif