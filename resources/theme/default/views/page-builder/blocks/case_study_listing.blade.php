<?php
// Spacing applied to the case study listing
$spacing = 'my-40 md:my-75';
?>

@if( isset( $case_studies ) && $case_studies->isNotEmpty() )
<div data-block="case-study-listing" class="row {{ $spacing }}">

    @if( !empty( $heading ) )
        {{-- Heading --}}
        <h3 class="text-3xl md:text-4xl font-semibold">{{ $heading }}</h3>

        {{-- Underline --}}
        <div class="pb-3 bg-secondary my-15 md:my-20 max-w-100"></div>
    @endif

    {{-- Case studies --}}
    <div class="flex flex-wrap items-stretch lg:-mx-20">
        @foreach( $case_studies as $CaseStudy )

            {{-- Case study column --}}
            <div class="w-full lg:w-1/3 lg:px-20 mb-30 md:mb-40">

                <div class="flex flex-col bg-gray-200 h-full rounded-lg overflow-hidden">

                    {{-- Case study image --}}
                    <a
                        href="{{ route('theme', $CaseStudy->url) }}"
                        class="w-full bg-center bg-cover pb-16:9 hover:opacity-80 tr-opacity"
                        @if( !empty( $CaseStudy->image ) && !empty( $CaseStudy->image['archive'] ) )
                        style="background-image:url('{{ $CaseStudy->image['archive']['src'] }}')"
                        @endif
                    ></a>

                    {{-- Case study content --}}
                    <div class="flex flex-col justify-between h-full">
                        <div class="p-30">

                            {{-- Case study title --}}
                            <a href="{{ route('theme', $CaseStudy->url) }}">
                                <div class="text-xl md:text-2xl font-semibold mb-20 hover:underline">{{ $CaseStudy->title }}</div>
                            </a>

                            {{-- Case study content --}}
                            {!! $CaseStudy->excerpt !!}

                        </div>

                        {{-- Read more button --}}
                        <div class="p-30 pt-0">
                            <a href="{{ route('theme', $CaseStudy->url) }}" class="inline-flex bg-primary hover:bg-darken tr-shadow text-white font-bold px-30 py-10 rounded-full">
                                Read More
                            </a>
                        </div>
                    </div>

                </div>

            </div>

        @endforeach
    </div>

</div>
@endif