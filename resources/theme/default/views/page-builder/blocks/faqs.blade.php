<?php
// Spacing applied to the block
$spacing = 'my-40 md:my-75';
?>

@if( isset( $faqs ) && $faqs->isNotEmpty() )
    <div data-block="faqs" class="row text-center {{ $spacing }}">

        @if( !empty( $heading ) )
            {{-- Heading --}}
            <h3 class="text-3xl md:text-4xl font-semibold">{{ $heading }}</h3>

            {{-- Underline --}}
            <div class="pb-3 bg-secondary my-15 md:my-20 max-w-100 mx-auto"></div>
        @endif

        {{-- FAQ wrapper --}}
        <div class="max-w-1000 mx-auto divide-y divide-gray-300">
            @foreach( $faqs as $FAQ )

                {{-- FAQ accordion --}}
                <div data-accordion-item>

                    {{-- Accordion toggle --}}
                    <a href="javascript:void(0)" data-accordion-toggle title="Show answer" class="flex items-center justify-between py-25">

                        {{-- Question --}}
                        <div class="text-lg md:text-xl lg:text-2xl font-semibold">
                            {{ $FAQ->question }}
                        </div>

                        {{-- Arrow --}}
                        <svg data-arrow xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="size-text fill-current text-2xl text-gray-400">
                            <path d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"></path>
                        </svg>
                    </a>

                    {{-- Accordion content --}}
                    <div data-accordion-content class="overflow-hidden h-0 text-left">

                        {{-- Answer --}}
                        <div class="cms-content md:text-lg lg:text-xl pb-25">
                            {!! $FAQ->answer !!}
                        </div>
                    </div>

                </div>

            @endforeach
        </div>

    </div>
@endif