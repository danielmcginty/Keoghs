@if(!empty($slides))
    <section class="hero-slide-section hero-slide slick">
        @foreach($slides as $ind => $slide)
        @if( $loop->first )
        @push('preloads')
        <link rel="preload" href="{{ $slide['background']['desktop']['src'] }}" as="image" media="(min-width:640px)" />
        <link rel="preload" href="{{ $slide['background']['mobile']['src'] }}" as="image" media="(max-width:639px)" />
        @endpush
        @endif
        @push('header_styles')
        <style>
        [data-hero-slide="{{ $ind }}"] {
            background-image: url('{{ $slide['background']['mobile']['src'] }}');
        }
        @media (min-width: 640px) {
            [data-hero-slide="{{ $ind }}"] {
                background-image: url('{{ $slide['background']['desktop']['src'] }}');
            }   
        }
        </style>
        @endpush
        <div data-hero-slide="{{ $ind }}" class="relative full-bg{{ $loop->first ? '' : ' slick:visible' }}">
            <div class="container flex flex-wrap items-end">
                <div class="w-full md:w-6/12 pb-24">

                    <div class="theme-dark bg-teal bg-opacity-95 text-white border-l-8 border-solid border-pink px-16 py-12">
                        <h2 class="text-5xl lg:text-6xl">{{ $slide['title'] }}</h2>
                        <p class="text-f2 lg:text-f1">{{ $slide['subtitle'] }}</p>

                        <a href="{{ $slide['link']['url'] }}" target="{{ $slide['link']['target'] }}" class="btn bg-pink text-white">{{ $slide['link_text'] }}</a>
                    </div>

                </div>
            </div>
        </div>
        @endforeach
    </section>
@endif