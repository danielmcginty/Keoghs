<div data-page-block="values" class="container pt-6 md:pb-14 pb-12">
    <div class="values-wrapper relative pt-40">
        <div class="values-line-wrapper absolute inset-0 flex flex-col items-center">
            <div class="values-circle z-1 w-14 h-14 border-pink bg-white rounded-full"></div>
            <div class="values-line h-full bg-pink z-1"></div>
        </div>
        <div class="values bg-grey-100 p-8 md:px-14 rounded-xl mb-16">
            @foreach($sections as $section)
                <div class="value flex flex-wrap{{ !$loop->even ? ' mb-0 md:mb-24' : ' -mb-16' }}">
                    <div class="w-full md:w-1/2 mb-8 md:mb-0 {{ $loop->even ? 'md:pl-14 order-1' : 'md:pr-14' }}">
                        @if( !empty($section['image']) && !empty($section['image']['single']) )
                            <img 
                                src="{{ $section['image']['single']['src'] }}" 
                                alt="{{ $section['image']['single']['alt'] }}" 
                                width="{{ $section['image']['single']['width'] }}" 
                                height="{{ $section['image']['single']['height'] }}" 
                                class="relative {{ $loop->even ? 'mt-3 md:-mt-16 -mb-8 md:mb-0' : '-mt-16' }} rounded-xl shadow-xl"
                                loading="lazy"
                            />
                        @endif
                    </div>
                    <div class="w-full md:w-1/2 md:mb-4 {{ $loop->even ? 'md:pr-14 order-0' : 'md:pl-14' }}">
                        <div class="relative bg-grey-100 pb-4 md:pb-0">
                            @if( !empty($section['title']) )
                                <h4 class="text-4xl md:text-5xl text-pink mb-8">{{ $section['title'] }}</h4>
                            @endif

                            <div class="content">
                                {!! $section['content'] !!}
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="summary relative border-pink bg-white py-8 px-14 rounded-xl">
            <div class="flex flex-wrap justify-center">
                @if( !empty($summary_title) )
                    <h4 class="w-full text-center text-4xl md:text-5xl text-pink">{{ $summary_title }}</h4>
                @endif
                @if( !empty($summary_image) && !empty($summary_image['single']) )
                <div class="w-full md:w-1/3 md:pr-8">
                    <img 
                        src="{{ $summary_image['single']['src'] }}" 
                        alt="{{ $summary_image['single']['alt'] }}" 
                        width="{{ $summary_image['single']['width'] }}" 
                        height="{{ $summary_image['single']['height'] }}" 
                        class=""
                        loading="lazy"
                    />
                </div>
                @endif
                @if( !empty($summary_content) )
                <div class="w-full md:w-2/3 content">
                    {!! $summary_content !!}
                </div>
                @endif
            </div>
        </div>
    </div>
</div>