<?php
// Spacing applied to the logos block
$spacing = 'my-30 md:my-50';

// Settings for the slick Carousel
$slick_settings = [
    'autoplay'          => 'true',
    'autoplaySpeed'     => 2000,
    'slidesToShow'      => 4,
    'slidesToScroll'    => 4,
    'responsive'        => [
        [
            'breakpoint'    => 1024,
            'settings'      => [
                'slidesToShow'      => 3,
                'slidesToScroll'    => 3
            ]
        ],
        [
            'breakpoint'    => 640,
            'settings'      => [
                'slidesToShow'      => 2,
                'slidesToScroll'    => 2,
            ]
        ]
    ]
];
?>

@if( isset( $logos ) && $logos->isNotEmpty() )
    <div data-block="logos-scrolling" class="row {{ $spacing }}">

        {{-- Logo Wrapper --}}
        <div class="-mx-20">
            {{-- Slick initializer --}}
            <div data-slick="{{ json_encode( $slick_settings ) }}">
                {{-- Individual logos --}}
                @foreach( $logos as $Logo )
                    @if( !empty( $Logo->image ) && !empty( $Logo->image['single'] ) )
                        {{-- Logo column --}}
                        <div class="w-1/2 md:w-1/3 lg:w-1/4 px-20">
                            <div class="flex justify-center">
                                {{-- Actual logo image --}}
                                <img src="{{ $Logo->image['single']['src'] }}" alt="{{ $Logo->title }}" />
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>

    </div>
@endif