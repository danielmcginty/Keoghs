@if( isset( $video ) && !empty( $video ) )

    <?php $rand_id = uniqid(); ?>

    <div data-block="video_upload" data-video-module class="video-module">

        <div class="video-container relative bg-black" style="padding-top: 56.25%">

            <video data-video-embed data-state="pause" class="video-embed absolute top-0 left-0 w-full h-full" preload="metadata" controlsList="nodownload" oncontextmenu="return false;">

                {{-- STANDARD VIDEO PATHS --}}
                @if( !empty( $video ) )
                    <source type="video/mp4" src="{{ $video }}">
                @endif
                @if( !empty( $video_webm ) )
                    <source type="video/webm" src="{{ $video_webm }}">
                @endif

                {{-- HASHED VIDEOS --}}
                {{--<source type="video/mp4" src="{{ route('theme') }}/video.php?video={{ $video }}">--}}
                {{--<source type="video/webm" src="{{ route('theme') }}/video.php?video={{ $video_webm }}">--}}

                {{-- TEST VIDEO --}}
                {{--<source type="video/mp4" src="/temp/test-video.mp4">--}}

            </video>

            <div data-video-overlay class="video-overlay">
                <div class="table w-full h-full text-center">
                    <div class="table-cell">
                        <div data-overlay-icon="play" class="overlay-icon" style="display: none;">
                            <i class="fi fi-play-solid block"></i>
                        </div>
                        <div data-overlay-icon="pause" class="overlay-icon" style="display: none;">
                            <i class="fi fi-pause-solid block"></i>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="video-controls bg-gray-100 p-5 table w-full text-sm mb-20">

            <div class="table-cell">

                <a href="#" data-video-playback-toggle class="button rounded bg-gray-100 text-primary p-10 m-0 text-lg no-underline">
                    <span data-play title="Play">
                        <i class="fi fi-play-solid block leading-4"></i>
                        <span class="invisible">Play</span>
                    </span>
                    <span data-pause title="Pause" style="display: none;">
                        <i class="fi fi-pause-solid block leading-4"></i>
                        <span class="invisible">Pause</span>
                    </span>
                </a>

            </div>

            <div data-current-time class="time-cell table-cell align-middle text-sm text-right">
                0:00
            </div>

            <div class="table-cell align-middle w-full px-5 md:pl-10 md:pr-15">
                <label for="seek-<?php echo $rand_id; ?>" class="invisible">Seek</label>
                <input data-video-seek type="range" id="seek-<?php echo $rand_id; ?>" name="seek" value="0" class="invisible" />
            </div>

            <div data-video-duration class="time-cell hidden md:table-cell align-middle text-sm"></div>

            <div class="table-cell text-right nowrap pl-20 md:pr-5">
                <div data-video-volume-control class="volume-control relative">

                    <a href="#" data-video-volume-toggle data-volume="1" title="Volume" class="button rounded bg-gray-100 text-primary p-10 m-0 text-lg no-underline">
                        <span data-mute>
                            <i class="fi fi-volume-high block leading-4"></i>
                            <span class="invisible">Mute</span>
                        </span>
                        <span data-unmute style="display: none;">
                            <i class="fi fi-volume-mute block leading-4"></i>
                            <span class="invisible">Unmute</span>
                        </span>
                    </a>

                    <div data-video-volume-bar class="volume-bar absolute bottom-100 right-0 hidden lg:block">
                        <div class="inner table w-full p-10 text-white rounded">
                            <div class="table-cell text-lg">
                                <i data-volume-low-icon class="fi fi-volume-medium block leading-4"></i>
                            </div>
                            <div class="light-rangeslider table-cell w-full pl-10 pr-15">
                                <label for="volume-<?php echo $rand_id; ?>" class="invisible">Seek</label>
                                <input data-video-volume type="range" id="volume-<?php echo $rand_id; ?>" name="volume" value="1" min="0" max="1" step="0.1" class="invisible" />
                            </div>
                            <div class="table-cell text-lg">
                                <i class="fi fi-volume-high block leading-4"></i>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="table-cell text-right nowrap">

                <a href="#" data-video-fullscreen title="Fullscreen" class="button rounded bg-gray-100 p-10 m-0 no-underline">
                    <i class="fi fi-fullscreen block leading-4"></i>
                    <span class="invisible">Fullscreen</span>
                </a>

            </div>

        </div>

    </div>

    @push( 'footer_scripts' )
    <script src="{{ auto_version( $dist_dir . 'assets/js/components/video-player.js' ) }}"></script>
    @endpush

@endif