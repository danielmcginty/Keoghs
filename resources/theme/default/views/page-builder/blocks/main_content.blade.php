@if( isset( $content ) && !empty( $content ) )
    <div data-block="main-content" class="content-section">
        <div class="container flex flex-wrap items-end pt-6 md:pb-14 pb-12">
            <div class="waypoint w-full">

                @if( !empty( $heading ) )
                <h2 class="text-2rem text-teal">{{ $heading }}</h2>
                @endif

                <div class="text-base cms-content">{!! $content !!}</div>

                @if( !empty($buttons) )
                <div class="w-full flex flex-wrap items-center justify-center">
                    @include('page-builder.partials.buttons', ['buttons' => $buttons])
                </div>
                @endif

            </div>
        </div>
    </div>
@endif