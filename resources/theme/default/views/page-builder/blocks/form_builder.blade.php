<?php
// Spacing applied to the block
$spacing = 'my-20 md:my-40';
?>

@if( isset( $form ) && is_object( $form ) )

    @if( !empty( $form->template ) && view()->exists( 'form-templates.' . $form->template ) )

        {{-- Custom form template --}}
        @include( 'form-templates.' . $form->template, ['form' => $form] )

    @else

        {{-- Default form template --}}
        <div data-block="form" class="row {{ $spacing }}">
            @include( 'form-templates.default', ['form' => $form] )
        </div>

    @endif

@endif