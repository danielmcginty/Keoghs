@if(isset($awards) && $awards->isNotEmpty())
<section data-block="awards" class="awards-section bg-teal">

	<div class="container">
		<div class="waypoint w-full pt-12 pb-2 title">
			<h2 class="text-5xl text-center text-white">Our Awards</h2>
		</div>

		<div 
            class="waypoint w-full key-slide-full slick pb-20"
            data-slick='{"dots": true, "arrows": true, "infinite": true, "slidesToShow": 1, "slidesToScroll": 1}'
        >
            @foreach($awards as $award)
			<div class="px-8 text-white">
				<div class="ml-auto mr-auto mt-10 mb-10 w-full md:w-3/4 flex flex-wrap">
                    <img 
                        src="{{ $award->logo['single']['src'] }}" 
                        alt="{{ $award->logo['single']['alt'] }}"
                        width="{{ $award->logo['single']['width'] }}"
                        height="{{ $award->logo['single']['height'] }}"
                        loading="lazy"
                        class="w-full md:w-auto"
                    >
					<p class="mt-5 ml-10 w-full md:w-auto">
                        <strong class="text-4xl pb-4">{{ $award->title }}</strong>
                        <br><br>{{ $award->subtitle }}
                        <br><br><a href="{{ $award->link['url'] }}" target="{{ $award->link['target'] }}" class="btn mt-1 bg-pink text-white">Read More</a>
                    </p>
				</div>
			</div>
            @endforeach
        </div>

	</div>

</section>
@endif