@if( isset($opportunities) && !empty($opportunities) )
<section class="opportunities-section relative">

    @if( isset($background_image) && !empty($background_image['desktop']) )
        <div class="absolute inset-0 full-bg lazyload hidden md:block" data-done="false" data-img="{{ $background_image['desktop']['src'] }}"></div>
        <div class="absolute inset-0 full-bg lazyload md:hidden" data-done="false" data-img="{{ $background_image['mobile']['src'] }}"></div>
    @endif

	<div class="relative container md:pt-20 md:pb-20">
		<div class="md:w-7/12 bg-blue text-white md:p-8 rounded-lg">
            @if( isset($heading) && !empty($heading) )
			    <h4 class="text-4xl mb-6">{{ $heading }}</h4>
            @endif

			<div class="flex flex-wrap -mx-2">
                @foreach( $opportunities as $opportunity )
				<div class="w-1/2 px-2">
					<h5 class="text-2xl">{{ $opportunity['heading'] }}</h5>
					<p class="mb-6">{{ $opportunity['content'] }}</p>
                    @if( !empty($opportunity['link']) && !empty($opportunity['link']['url']) && !empty($opportunity['link_text']) )
					    <a 
                            href="{{ $opportunity['link']['url'] }}"
                            {!! !empty($opportunity['link']['target']) ? ' target="' . $opportunity['link']['target'] . '"' : '' !!} 
                            class="btn bg-white arrow-pink text-white inline-block align-middle"
                        >{{ $opportunity['link_text'] }}</a>
                    @endif
				</div>
                @endforeach
			</div>
		</div>
	</div>

</section>
@endif