@if( isset( $video ) && !empty( $video ) && isset( $image ) && !empty( $image ) && !empty( $image['desktop'] ) )

    <?php $random_id = uniqid(); ?>

    @if( isset( $image ) && !empty( $image ) && !empty( $image['desktop'] ) )
        @push('header_styles')
            <style>

                [data-modal-video][data-video="{{ $random_id }}"] {
                    background-image: url('{{ $image['mobile']['src'] }}');
                }

                @media (min-width: 640px){
                    [data-modal-video][data-video="{{ $random_id }}"] {
                        background-image: url('{{ $image['desktop']['src'] }}');
                    }
                }

            </style>
        @endpush
    @endif

    <div data-block="popup-video" class="row my-30 md:my-50">

        <div class="max-w-1000 mx-auto">

            @if( isset( $heading ) && !empty( $heading ) )
                <h3 class="text-3xl md:text-4xl font-semibold">{{ $heading }}</h3>
                <div class="pb-3 bg-secondary my-15 md:my-20 max-w-100"></div>
            @endif

            @if($video_type === 'youtube' || $video_type === 'vimeo')
                <a data-modal-video data-video="{{ $random_id }}" href="{{ $video }}" title="Play video" class="block relative bg-cover bg-center rounded-lg overflow-hidden pb-16:9 hover-parent">

                    <span class="absolute inset-0 bg-black opacity-40 hover-child:opacity-25 tr-opacity"></span>

                    <span class="absolute inset-0 flex flex-col items-center justify-center">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 128" class="block size-text text-4xl md:text-6xl fill-current text-white">
                            <path d="M64 0C28.699 0 0 28.699 0 64s28.699 64 64 64 64-28.699 64-64S99.301 0 64 0zm20.615 67.234L54.434 87.309c-2.56 1.752-6.063-.135-6.063-3.234V43.924c0-3.099 3.503-4.985 6.063-3.234l30.181 20.076c2.29 1.482 2.29 4.986 0 6.468z"/>
                        </svg>
                    </span>

                </a>
            @elseif($video_type === 'wistia')
                <div class="video-block small-12 large-7 xlarge-8 margin-b10-medium-up margin-b0-large-up">
                    {{-- Wistia Embed Code --}}
                    <script src="https://fast.wistia.com/embed/medias/{{ $video_id }}.jsonp" async></script>
                    <script src="https://fast.wistia.com/assets/external/E-v1.js" async></script>

                    <div class="wistia_responsive_padding" style="padding:56.25% 0 0 0; position:relative;">
                        <div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;">
                            <div class="wistia_embed wistia_async_{{ $video_id }} popover=true popoverAnimateThumbnail=true" style="height:100%;position:relative;width:100%">
                                <div class="wistia_swatch" style="height:100%;left:0;opacity:0;overflow:hidden;position:absolute;top:0;transition:opacity 200ms;width:100%;">
                                    <img src="https://fast.wistia.com/embed/medias/{{ $video_id }}/swatch" style="filter:blur(5px);height:100%;object-fit:contain;width:100%;" alt="" aria-hidden="true" onload="this.parentNode.style.opacity=1;" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

        </div>

    </div>

@endif