@if( isset($contacts) && $contacts->isNotEmpty() )
<section class="key-contacts-section bg-teal">
    <div class="container">
        @if( isset($heading) && !empty($heading) )
        <div class="waypoint w-full pt-10 md:pt-12 pb-2 title">
            <h2 class="text-4xl md:text-5xl text-center text-white">{{ $heading }}</h2>
        </div>
        @endif

        <div class="waypoint w-full key-slide slick pb-20">
            @foreach($contacts as $key_contact)
                <a href="{{ route('theme', $key_contact->url) }}" class="text-center sm:px-8 text-white">
                    <div class="img-con">
                        <img class="ml-auto mr-auto mb-7 mt-4 rounded-full" src="{{ $key_contact->image['primary']['src'] ?? '' }}">
                    </div>
                    <p class="text-base"><strong>{{ $key_contact->full_name }}</strong> <br>{{ $key_contact->job_title }}<br>{{ $key_contact->expertise_area }}</p>
                </a>
            @endforeach
        </div>

    </div>
</section>
@endif