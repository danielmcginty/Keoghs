@if( isset( $testimonials ) && $testimonials->isNotEmpty() )
    <div data-block="testimonials" class="mt-50 mb-75 lg:my-75">
        <div class="fade-edges row px-0 text-center max-w-1000">

            <div class="sm:slick-col-1 md:slick-col-2 flex flew-wrap justify-center" data-slick='{
                "slidesToShow": 2,
                "slidesToScroll": 1,
                "arrows": false,
                "dots": true,
                "autoplay": true,
                "autoplaySpeed": 5000,
                "swipeToSlide": true,
                "responsive": [
                     {
                         "breakpoint": 640,
                         "settings": {
                            "slidesToShow": 1
                         }
                     }
                ]
            }'>
                @foreach( $testimonials as $Testimonial )
                    <div class="slide w-full md:w-1/2 px-15 md:px-30 pb-50 outline-none">
                        <div class="max-w-400 mx-auto">

                            <div class="w-full max-w-200 mx-auto mb-30">
                                <div class="rounded-full bg-gray-200 bg-cover bg-center pb-square" style="background-image: url('{{ !empty( $Testimonial->image ) && !empty( $Testimonial->image['single'] ) ? $Testimonial->image['single']['src'] : '' }}');"></div>
                            </div>

                            <div class="text-xl font-semibold mb-15">
                                {{ $Testimonial->citation }}
                            </div>

                            <div class="flex text-lg">

                                @include( 'partials.svg.quote-open' )

                                <div class="w-full px-20">
                                    {{ $Testimonial->testimonial }}
                                </div>

                                @include( 'partials.svg.quote-close' )

                            </div>


                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </div>
@endif