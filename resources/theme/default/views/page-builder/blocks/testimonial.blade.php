@if( isset( $testimonial ) && $testimonial )
<div data-block="testimonial" class="py-30 my-30 md:my-50">

    <div class="row flex flex-wrap items-center">

        @if( !empty( $testimonial->image ) && !empty( $testimonial->image['single'] ) )
            <div class="w-full md:w-1/3 lg:w-1/4 px-30 flex items-center">
                <img src="{{ $testimonial->image['single']['src'] }}" alt="{{ $testimonial->image['single']['alt'] }}" class="rounded-full" />
            </div>
        @endif

        <div class="flex-1 px-30">
            <div class="flex mb-20">
                <div class="text-secondary text-2xl">@include( 'partials.svg.quote-open' )</div>
                <div class="text-black text-xl px-10 py-20">{{ $testimonial->testimonial }}</div>
                <div class="self-end text-secondary text-2xl">@include( 'partials.svg.quote-close' )</div>
            </div>

            @if( !empty( $testimonial->citation ) )
            <div class="mb-10 pl-30 ml-5">
                {{ $testimonial->citation }}
            </div>
            @endif
        </div>

    </div>

</div>
@endif