<?php
$random_id = uniqid();

// Spacing applied to the map
$spacing = 'my-30 md:my-50';
?>
@if( isset( $latitude ) && !empty( $latitude ) && isset( $longitude ) && !empty( $longitude ) && isset( $address ) && !empty( $address ) )
<div
    data-block="interactive-map"
    class="row relative z-0 {{ $spacing }}"
>
    {{-- OpenStreet multi-pin map --}}
    <div data-osm-map data-map-id="{{ $random_id }}" data-lat="{{ $latitude }}" data-lng="{{ $longitude }}" data-popup="{{ $address }}" data-zoom="{{ isset( $zoom ) && !empty( $zoom ) ? $zoom : 12 }}" class="osm-map bg-gray-200 h-half-screen"></div>
</div>
@endif