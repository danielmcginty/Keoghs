@if( isset($benefits) && !empty($benefits) )
<section class="employee-benefits-section bg-teal">

	<div class="container">

        @if( isset($heading) && !empty($heading) )
		<div class="waypoint w-full pt-10 md:pt-12 pb-6 title">
			<h2 class="text-4xl md:text-5xl text-center text-white">{{ $heading }}</h2>
		</div>
        @endif

		<div class="md:w-8/12 ml-auto mr-auto pb-6">
			<div class="-mx-4 flex flex-wrap">
				@foreach( $benefits as $benefit )
				<div class="w-full lg:w-1/2 mb-10 px-4 text-white">
					<h4 class="text-4xl mb-4">{{ $benefit['heading'] }}</h4>
					<p>{{ $benefit['content'] }}</p>
				</div>
				@endforeach
				@if( isset($cta_link) && !empty($cta_link) && !empty($cta_link['url']) && isset($cta_text) && !empty($cta_text) )
					<div class="w-full lg:w-1/2 mb-10 px-4 text-white">
						<a
							href="{{ $cta_link['url'] }}"{{ !empty( $cta_link['target'] ) ? ' target="' . $cta_link['target'] . '"' : '' }}
							class="btn bg-pink text-white btn-full right-arrow"
						>
							{{ $cta_text }}
						</a>
					</div>
				@endif
			</div>
		</div>
	</div>
</section>
@endif