@if( isset( $heading ) && !empty( $heading ) )
    <{{ $heading['size'] }} class="{{ isset( $heading['class'] ) ? $heading['class'] . ' ' : '' }} mb-20">
        {{ $heading['text'] }}
    </{{ $heading['size'] }}>
@endif