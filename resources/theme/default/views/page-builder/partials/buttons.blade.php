@if( isset( $buttons ) && !empty( $buttons ) )
    <div class="btn-con">
        @foreach( $buttons as $ind => $button )
        <?php
            $class = 'btn bg-teal';
            if (isset($default_class) && !empty($default_class)) {
                $class = 'btn ' . $default_class;
            }
            if (($ind+1) < sizeof($buttons)) { $class .= ' mr-4'; }
        ?>
            <a
                href="{{ $button['link']['url'] }}"{{ !empty( $button['link']['target'] ) ? ' target="' . $button['link']['target'] . '"' : '' }}
                class="{{ isset($button_class) && !empty($button_class) ? $button_class : $class }}"
            >
                {{ $button['text'] }}
            </a>
        @endforeach
    </div>
@endif