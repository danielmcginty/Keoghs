<style>
body,td { color:#2f2f2f; font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; }
</style>
<body style="background:#F6F6F6; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<div style="background:#F6F6F6; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
        <tr>
            <td align="center" valign="top" style="padding:20px 0 20px 0">
                <table bgcolor="FFFFFF" cellspacing="0" cellpadding="10" border="0" width="650" style="border:1px solid #E0E0E0;">
                    <tr>
                        <td valign="top">
                            <a href="{{ url('/') }}"><img src="{{ route('theme') }}/default/dist/ui/email/keoghs.png" alt="Keoghs" style="margin-bottom:10px;" border="0"/></a></td>
                    </tr>
                    <tr>
                        <td bgcolor="#007CB1" align="center" style="background:#007CB1; text-align:center;"><center><p style="font-size:12px; margin:0;">
                            <h2 style="font-size:24px; line-height:48px; margin:0; color: #ffffff;">@yield('heading')</h2>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="padding:40px 20px 60px 20px">
                            @yield('content')
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#007CB1" align="center" style="background:#007CB1; text-align:center;"><center><p style="font-size:12px; margin:0;">
                            <p style="font-size:12px; line-height:16px; margin:0; color: #ffffff;"><a href="{{ url('/') }}" style="color:#ffffff;">{{ url('/') }}</a></p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>
