@include( 'includes.structure.meta' )
@include( 'includes.structure.header' )

@yield( 'content' )

@include( 'includes.structure.footer' )