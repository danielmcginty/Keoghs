<div class="w-full md:w-1/3 md:px-8">
    <div class="card transform rounded-lg bg-white mb-12 md:mb-14 shadow-card">
        <a href="{{ $award->link['url'] }}" class="over"></a>
        <div class="bottom p-6 md:p-8">
            @if( !empty($award->logo) && !empty($award->logo['single']) )
                <img 
                    src="{{ $award->logo['single']['src'] }}"
                    alt="{{ $award->logo['single']['alt'] }}"
                    width="{{ $award->logo['single']['width'] }}"
                    height="{{ $award->logo['single']['height'] }}"
                    loading="lazy"
                    class="mb-6"
                />
            @endif
            <h3 class="text-3xl md:mb-3 md:text-4xl text-teal mt-4">{{ $award->title }}</h3>
            <p class="text-xl font-bold mb-6">{{ $award->subtitle }}</p>
            <a href="{{ $award->link['url'] }}" class="btn bg-pink text-teal inline-block align-middle">Read More</a>
        </div>
    </div>
</div>