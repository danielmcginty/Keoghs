@extends( 'templates.default' )

@section( 'content' )

    @include( 'partials.page-banner' )

    @include( 'partials.page-intro-content' )

    @if(isset($featured_award) && !empty($featured_award))
    <section class="employee-benefits-section bg-teal py-10 md:py-12">
        <div class="container flex flex-wrap items-start md:items-center">
            <div class="w-full md:w-1/2 text-center">
                @if( !empty($featured_award->logo) && !empty($featured_award->logo['single']) )
                    <img 
                        src="{{ $featured_award->logo['single']['src'] }}"
                        alt="{{ $featured_award->logo['single']['alt'] }}"
                        width="{{ $featured_award->logo['single']['width'] }}"
                        height="{{ $featured_award->logo['single']['height'] }}"
                        loading="lazy"
                        class="mb-8 md:mb-0"
                    />
                @endif
            </div>
            <div class="w-full md:w-1/2 py-20 pr-10">
                <h3 class="text-4xl text-white">{{ $featured_award->title }}</h3>
                @if( !empty($featured_award->wordy_date) )
                <span class="text-white text-2xl"><strong>{{ $featured_award->wordy_date }}</strong></span>
                @endif
                <p class="text-white mt-4 mb-4 text-f2">{{ !empty($featured_award->excerpt) ? $featured_award->excerpt : $featured_award->subtitle }}</p>
                <a href="{{ $featured_award->link['url'] }}" target="{{ $featured_award->link['target'] }}" class="btn bg-pink text-white inline-block align-middle">Read More</a>
            </div>
        </div>
    </section>
    @endif

    <section class="insights-section bg-grey-100">
        <div class="container flex flex-wrap pb-16">
            <div class="w-full pt-10 pb-5 md:pt-6 md:pb-5 title">
                <h2 class="waypoint text-4xl md:text-5xl mt-10 text-center text-teal">Our Awards</h2>
            </div>

            <div class="w-full text-center flex flex-col justify-center items-center mb-8">
                <label for="award-sort" class="sr-only">Sort by:</label>
                <select data-award-sort id="award-sort" class="max-w-md border-transparent text-teal">
                    @foreach($sort_options as $key => $option)
                        <option value="{{ $key }}" {{ $key === $selected_sort_key ? 'selected' : '' }}>Sort by {{ $option['label'] }}</option>
                    @endforeach
                </select>
            </div>

            <div data-awards class="w-full flex flex-wrap">
            @foreach( $awards as $award )
                @include('awards.partials.card', ['award' => $award])
            @endforeach
            </div>

            <div class="w-full text-center hidden" data-pagination>
                <button href="#" class="btn bg-pink btn-full text-white" data-load-more>Load more</button>
            </div>

            <div data-no-results class="w-full lg:px-20 mb-30 hidden">
                <h2 class="text-3xl md:mt-20 font-semibold">
                    Sorry, no awards were found.
                </h2>
            </div>
        </div>
    </section>

    @include( 'page-builder.block-wrapper' )

@endsection

@push('footer_scripts')
    <script>
        var pagination = {
            current_page: 1,
            hasMorePages: {{ $awards->hasMorePages() ? 'true' : 'false' }},
            results: {{ $awards->count() }},
            total: {{ $awards->total() }},
        };

        updatePagination();

        $('[data-load-more]').click(function () {
            pagination.current_page++;

            $.ajax({
                url: '{{ route('theme.ajax', ['award', 'loadMoreAwards']) }}',
                type: 'GET',
                data: {
                    page: pagination.current_page,
                    'sort-by': getCurrentSort(),
                },
                dataType: 'json',
                beforeSend: function () {
                    // TODO: Add a loading state
                },
                success: function (data) {
                    var awards = data.awards.data;

                    pagination.hasMorePages = data.awards.next_page_url !== null;
                    pagination.results = data.awards.to;
                    pagination.total = data.awards.total;

                    updatePagination();
                    $('[data-awards]').append(data.rendered_awards);
                    //awards.forEach(appendAward);
                },
                complete: function () {
                    // TODO: Hide loading state
                }
            });
        });

        $('[data-award-sort]').change(function () {
            var sort = $(this).val();

            var url = new URL(window.location);
            url.searchParams.set('sort-by', sort);
            history.pushState({}, '', url);

            pagination.current_page = 1;

            $.ajax({
                url: '{{ route('theme.ajax', ['award', 'filterAwards']) }}',
                type: 'GET',
                data: {
                    page: pagination.current_page,
                    'sort-by': sort,
                },
                dataType: 'json',
                beforeSend: addLoadingState,
                success: function (data) {
                    var awards = data.awards.data;

                    pagination.hasMorePages = data.awards.next_page_url !== null;
                    pagination.results = data.awards.to;
                    pagination.total = data.awards.total;

                    updatePagination();
                    clearCurrentAwards();
                    $('[data-awards]').append(data.rendered_awards);
                    //awards.forEach(appendAward);
                },
                complete: removeLoadingState
            });
        });

        function addLoadingState() {
            // TODO
        }

        function removeLoadingState() {
            // TODO
        }

        function updatePagination() {
            if (pagination.hasMorePages) {
                $('[data-pagination]').removeClass('hidden');
            } else {
                $('[data-pagination]').addClass('hidden');
            }

            $('[data-award-results]').text(pagination.results);
            $('[data-award-total]').text(pagination.total);
        }

        function clearCurrentAwards() {
            $('[data-awards]').empty();
        }

        function selectedCategoriesToString()
        {
            return Array.from(selectedCategories).join(',');
        }

        function getCurrentSort()
        {
            return $('[data-award-sort]').val();
        }

        function appendAward(award) {
            $('[data-awards]').append(
                `
                <div>
                    <img src="${award.logo.single.src}" alt="${award.logo.single.alt}" />

                    <h5>${award.title}</h5>

                    <p>${award.subtitle} {{ $award->subtitle }}</p>

                    <a href="${award.link.url}">Read more</a>
                </div>
                `
            );
        }
    </script>
@endpush