@extends( 'templates.default' )

@section( 'content' )

    @include( 'partials.page-banner' )

    <div class="row flex flex-wrap lg:p-0 my-30 md:my-50">

        @if( isset( $testimonials ) && $testimonials->isNotEmpty() )

            @foreach( $testimonials as $ind => $Testimonial )

                <div class="w-full flex flex-wrap lg:flex-no-wrap items-center mb-100">
                    
                    @if( !empty( $Testimonial->image ) && !empty( $Testimonial->image['single'] ) )
                        @push( 'header_styles' )
                        <style>
                        [data-testimonial-image="{{ $Testimonial->testimonial_id }}"] {
                            background-image: url('{{ $Testimonial->image['single']['src'] }}');
                        }
                        </style>
                        @endpush
                        <div class="w-full lg:w-1/3 flex items-center justify-center mb-20 lg:mb-0 {{ $ind % 2 == 1 ? 'order-1 lg:ml-50' : 'lg:mr-50' }}">
                            <div class="flex-1 max-w-200">
                                <div data-testimonial-image="{{ $Testimonial->testimonial_id }}" class="rounded-full w-full bg-contain bg-center bg-no-repeat pb-square"></div>
                            </div>
                        </div>
                    @endif

                    <div class="flex-grow text-center {{ $ind % 2 == 1 ? 'lg:text-right order-0' : 'lg:text-left' }}">
                        <p class="text-xl lg:text-2xl mb-10 lg:mb-0">{{ $Testimonial->testimonial }}</p>
                        @if( !empty( $Testimonial->citation ) || !empty( $Testimonial->date ) )
                            <p class="text-md lg:text-lg italic flex items-center justify-center {{ $ind % 2 == 1 ? 'lg:justify-end' : 'lg:justify-start' }}">
                                <span class="mr-10">-</span>
                                @if( !empty( $Testimonial->citation ) )
                                <span>{{ $Testimonial->citation }}</span>
                                @endif
                                @if( !empty( $Testimonial->citation ) && !empty( $Testimonial->date ) )
                                <span class="mr-10">,</span>
                                @endif
                                @if( !empty( $Testimonial->date ) )
                                <span>{{ Carbon::parse( $Testimonial->date )->format( 'jS F Y' ) }}</span>
                                @endif
                                <span class="ml-10">-</span>
                            </p>
                        @endif
                    </div>

                </div>

            @endforeach

        @else

            <h2 class="text-3xl md:mt-20 font-semibold">
                Sorry, no Testimonials were found.
            </h2>

        @endif

    </div>

@endsection