$(document).ready(function () {

    /**
     * CLOUDFLARE CACHE BUSTING
     */
    $(document).on('token_regenerated', function () {
        $.ajax({
            url: '/ajax/common/advice_bar',
            type: 'POST',
            data: {
                _token: $('[name="csrf-token"]').attr('content')
            },
            cache: false,
            success: function (html) {
                $('body').append(html);
            }
        });

        if (typeof (_ADM_URL_ID) != 'undefined') {
            $.ajax({
                url: '/ajax/common/adm_url',
                type: 'POST',
                data: {
                    _token: $('[name="csrf-token"]').attr('content'),
                    url_id: _ADM_URL_ID
                },
                cache: false,
                success: function (url) {
                    if (url) {
                        $('body').append('<a href="' + url + '" target="_blank" class="fixed bottom-0 right-0 mr-4 mb-4 py-4 px-8 rounded bg-black text-white">Edit this Page</a>');
                    }
                }
            });
        }
    });

    if (typeof(CF_STATUS) != 'undefined') {
        $.ajax({
            url: '/ajax/common/token',
            type: 'POST',
            data: {
                _token: $('[name="csrf-token"]').attr('content')
            },
            success: function (token) {
                $('[name="csrf-token"]').attr('content', token);
                $('[name="_token"]').val(token);
                $(document).trigger('token_regenerated');
            }
        });
    }
    else {
        $(document).trigger('token_regenerated');
    }

    /**
     * MODAL (INLINE)
     * -------------
     */

    var modal_inline = $('[data-modal-inline]');
    var modal_video = $('[data-modal-video]');

    if (modal_inline.length) {
        modal_inline.magnificPopup({
            type: 'inline',
            mainClass: 'mfp-fade',
            preloader: false,
            showCloseBtn: false
        });
    }

    if (modal_video.length) {
        modal_video.magnificPopup({
            type: 'iframe',
            mainClass: 'mfp-fade'
        });
    }


    /**
     * CUSTOM MODAL CLOSE BUTTON
     */

    $(document).on('click touchend', '[data-modal-close]', function () {
        $.magnificPopup.close();
    });


    /**
     * SIMPLE CAROUSELS
     * Source: http://kenwheeler.github.io/slickw/
     * -------------
     */

    $('[data-slick]').slick();

    initGoogleLocationMap();


    /**
     * SOCIAL SHARE WINDOWS
     * --------------
     */

    $("[data-social-share]").on("click touchend", function (e) {
        e.preventDefault();

        windowPopup($(this).attr("href"), 700, 750);

    });


    /**
     * GOOGLE ANALYTICS (GENERAL)
     * -------------
     */

    $(document).on('click', '[data-ga-event]', function () {
        if (typeof (gtag) != 'undefined') {
            var category = $(this).data('ga-category');
            var action = $(this).data('ga-action');
            var label = $(this).data('ga-label');
            var value = $(this).data('ga-value');

            // Ensure that all variables are unified in casing.
            category = typeof (category) == 'string' ? category : category;
            action = typeof (action) == 'string' ? action : action;
            label = typeof (label) == 'string' ? label : label;
            value = typeof (value) == 'string' ? value : value;

            if (typeof (category) != 'undefined' && typeof (action) != 'undefined') {
                var event_settings = {
                    event_category: category
                };
                if (typeof (label) != 'undefined') {
                    event_settings.event_label = label;
                }
                if (typeof (value) != 'undefined') {
                    event_settings.value = value;
                }

                gtag('event', action, event_settings);
            }
        }
    });


}); // END DOC READY


/**
 * CUSTOM WINDOW POPUPS
 */
function windowPopup(url, w, h) {

    // Fixes dual-screen position
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;

    window.open(url, "", 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

}

var $location_map_instance;
function initGoogleLocationMap() {
    if (!$('[data-location-map]').length) {
        return false;
    }
    $location_map = $('[data-location-map]')

    $location_map.lazyLoadGoogleMaps({
        api_key: 'AIzaSyAehQEMdI2JZr2GFI08BoE_0M6nbUS09ek', // TODO - Replace with live key
        callback: function (container, map) {
            map = new google.maps.Map(container);
            $location_map_instance = map;

            var markers = locations.map(function (location) {
                return new google.maps.Marker({
                    position: {
                        lat: parseFloat(location.latitude),
                        lng: parseFloat(location.longitude),
                    },
                    map,
                    title: location.title
                })
            });

            $('[data-locations-section] [data-location-dropdown]').trigger('change');

            google.maps.event.addListenerOnce(map, 'idle', function () {
                var zoom = parseInt(map.getZoom());
                if (zoom < 5) {
                    zoom = 5;
                }
                if (zoom > 15) {
                    zoom = 15;
                }
                map.setZoom(zoom);
            });
        }
    });

    $('[data-locations-section] [data-map-position]').click(function () {
        if ($('[data-location-map]').length) {
            $('html,body').animate({
                scrollTop: $('[data-location-map]').offset().top - 90
            }, 500);
        }
    });
    $('[data-locations-section] [data-pin]').click(function () {
        select_location($(this).data('lat'), $(this).data('lng'))
    });
    $('[data-locations-section] [data-location-dropdown]').change(function () {
        var $opt = $(this).find('option:selected');
        select_location($opt.data('latitude'), $opt.data('longitude'))
    });
}
function select_location(latitude, longitude) {
    var bounds = new google.maps.LatLngBounds();
    bounds.extend(new google.maps.LatLng(latitude, longitude));
    $location_map_instance.fitBounds(bounds);

    var zoom = $location_map_instance.getZoom();
    if (zoom > 15) { zoom = 15; }
    $location_map_instance.setZoom(zoom - 1);
}

//Smooth scroll START
$(function() {
	$('a[href*="#"]:not([href="#"])').click(function() {
	if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		var target = $(this.hash);
		target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		if (target.length) {
			$('html, body').animate({
				scrollTop: target.offset().top
			}, 100);
		return false;
		}
	}
	});
});
//Smooth scroll END


//Lazy load - works with waypoints START
function lazyload() {

	$('.lazyload').each(function() {
		var thisImg = $(this);

		var waypoint = new Waypoint({
			element: thisImg,
			handler: function(direction) {
				if(direction == 'down'){
					var done = thisImg.data('done');

					if(done === false){

						if(thisImg.is('img')){
							var lazy = thisImg.data('img');
							thisImg.attr('src',lazy).on('load', function(){
								waypoints()
							});
							thisImg.addClass('loaded');
							thisImg.data('done', 'true')
						} else {
							var lazy = thisImg.data('img');
							thisImg.css('background-image','url(' + lazy + ')').on('load', function(){
								waypoints()
							});
							thisImg.addClass('loaded');
							thisImg.data('done', 'true')
						}
					}
				}
			},
			offset: '90%',
		});
	});
}
//Lazy load - works with waypoints END

//Waypoints START
function waypoints() {
	$('.waypoint').each(function() {
		var thisOne = $(this);

		var waypoint = new Waypoint({
			element: thisOne,
			handler: function(direction) {
				if(direction == 'down'){
					thisOne.addClass('animate');
				} else {
					//thisOne.removeClass('animate');
				}
			},
			offset: '90%',
		});

		var waypoint = new Waypoint({
			element: thisOne,
			handler: function(direction) {
				if(direction == 'down'){
					//thisOne.removeClass('animate');
				} else {
					thisOne.addClass('animate');
				}
			},
			offset: function () {
				var windowH = $(window).height() / 5.5;
				var elementH = thisOne.outerHeight();
				var newOffset = -elementH + windowH;
				return newOffset
			}
		});
	});
}
//Waypoints END

$(window).ready(function() {

	$('#burger').click(function(){
		$(this).toggleClass('open');
		$(this).addClass('close');
		$('html').toggleClass('menu-open');
	});

	$(document).on('click', 'nav li.has-child', function(){
		$('li.has-child').removeClass('open');
		$(this).addClass('open');
	});

	$(document).on('click', 'nav li.has-child.open', function(){
		$('li.has-child').removeClass('open');
		$(this).removeClass('open');
	});

	$(document).on('click', '.filter-btn', function(){
		$(this).next('.wrap').addClass('open');
	});

	$(document).on('click', 'aside .wrap .close', function(){
		$('aside .wrap ').removeClass('open');
	});


	$('nav li.has-child > a').click(function(e){
		e.preventDefault();
	});

	$(document).on('click', '.has-sub', function(e){
		e.preventDefault();
		$(this).next('.mob-sub').addClass('open');
	});

	$(document).on('click', 'nav .back', function(e){
		e.preventDefault();
		$(this).parent('.mob-sub').removeClass('open');
	});

	$(document).on('click', '.sub-menu-big, .sub-menu-small, .small-menu li, .main-menu li' ,function(){
		e.stopPropagation();
	});

	$(document).on('click', 'body' ,function(){
		$('li.has-child').removeClass('open');
	});

	$('.locations-section select').on('change', function(){
		var thisOne = $(this);
		var thisVal = $(this).val();

		if (thisVal != '#') {
			$('.locations-section .location').removeClass('active');
			$('.locations-section .location[data-location="'+thisVal+'"]').addClass('active');

			$('.locations-section .pin').removeClass('active')
			$('.locations-section .pin[data-pin="'+thisVal+'"]').addClass('active');
		}
	});

	$('.locations-section .pin').on('click', function(){
		var thisOne = $(this);
		var thisPin = $(this).data('pin');

		$('.locations-section .pin').removeClass('active')
		thisOne.addClass('active')

		$('.locations-section .location').removeClass('active');
		$('.locations-section .location[data-location="'+thisPin+'"]').addClass('active');

		$('.locations-section select').val(thisPin);
	});

	$('aside .show-all').on('click', function(){
		var thisOne = $(this);

		thisOne.toggleClass('open');
		thisOne.prev('.view').toggleClass('open');

        if (thisOne.hasClass('open')) {
            thisOne.text('Show Less');
        } else {
            thisOne.text('Show All');
        }
	});
});

$(window).on('load', function(){
    document.getElementById('sra-iframe').src = 'https://cdn.yoshki.com/iframe/55845r.html';

	$('html').addClass('loaded');

	setTimeout(function(){
		$('.slick').addClass('loaded');
		$(window).trigger('resize');

		setTimeout(function(){
			$('.slick').addClass('no-overflow');
			$(window).trigger('resize');
			waypoints();
			lazyload();
		}, 0);
	}, 0);

	$('.hero-slide').slick({
		autoplay: true,
		dots: true,
		arrows: false,
		infinite: true,
		fade: true,
	});

	$('.key-slide').slick({
		//autoplay: true,
		dots: true,
		arrows: true,
		infinite: true,
		slidesToShow: 4,
  		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					arrows: true
				}
			},
			{
				breakpoint: 769,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					arrows: false
				}
			},
			{
				breakpoint: 640,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false
				}
			}
		]
	});
});

// SVG Map Pins
$(window).on('load', function () {
    if ($('[data-locations-section]').length) {
        $('[data-locations-section]').each( function () {
            var $section = $(this);

            var $map_container = $section.find('[data-map-container]');
            var map_type = $map_container.data('map');
            var p0 = {  // Top-left point
                scrX: 0,
                scrY: 0,
                lat: (map_type == 'full' ? 59.02444 : 57.151851),
                lng: (map_type == 'full' ? -11.894184 : -10.887414)
            };
            var p1 = {  // Bottom-right point
                scrX: $map_container.width(),
                scrY: $map_container.height(),
                lat: (map_type == 'full' ? 49.943862 : 50.100283),
                lng: (map_type == 'full' ? 3.195279 : 2.424946)
            }
            var radius = 6371;  // Earth radius

            p0.pos = latlngToGlobalXY(p0.lat, p0.lng, p0, p1, radius);
            p1.pos = latlngToGlobalXY(p1.lat, p1.lng, p0, p1, radius);

            $section.find('[data-pin]').each( function () {
                var $pin = $(this);

                var pin_pos = latlngToScreenXY($pin.data('lat'), $pin.data('lng'), p0, p1, radius);
                pin_pos.x = pin_pos.x - ($pin.width()/2);
                pin_pos.y = pin_pos.y - ($pin.width()/2);
                var pin_pos_pcnt = {
                    x: ((pin_pos.x / p1.scrX) * 100),
                    y: ((pin_pos.y / p1.scrY) * 100)
                };
                
                $pin.css('left', pin_pos_pcnt.x + '%');
                $pin.css('top', pin_pos_pcnt.y + '%');
            });
        });
    }
});
function latlngToGlobalXY(lat, lng, p0, p1, radius)
{
    var x = radius * lng * Math.cos((p0.lat + p1.lat) / 2);
    var y = radius * lat;
    return { x: x, y: y };
}
function latlngToScreenXY(lat, lng, p0, p1, radius)
{
    var pos = latlngToGlobalXY(lat, lng, p0, p1, radius);
    pos.perX = ((pos.x - p0.pos.x) / (p1.pos.x - p0.pos.x));
    pos.perY = ((pos.y - p0.pos.y) / (p1.pos.y - p0.pos.y));

    return {
        x: p0.scrX + (p1.scrX - p0.scrX) * pos.perX,
        y: p0.scrY + (p1.scrY - p0.scrY) * pos.perY
    }
}