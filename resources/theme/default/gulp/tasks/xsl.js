var gulp        = require( 'gulp' );

const config    = require( '../config' ).paths;

var dirpath     = config.dirpath;
var themepath   = config.themepath;

var distsrc     = dirpath + 'xsl/*';
var distdest    = themepath + 'xsl/';

gulp.task( 'copy:xsl', function(cb){
  gulp.src( distsrc )
    .pipe( gulp.dest( distdest ) );
  cb();
});