var gulp                = require('gulp');
var iconfont            = require('gulp-iconfont');
var iconfontCss         = require('gulp-iconfont-css');
var runSequence         = require('run-sequence');
var runTimestamp        = Math.round(Date.now()/1000);

var fontName = 'iconFont';
var cssClass = 'fi';

const config          = require('../config').paths;

var dirpath         = config.dirpath;
var themepath       = config.themepath;

gulp.task('icons', function(cb) {
    runSequence(
        ['iconfont'],
        ['moveIcons'],
        cb);
});

// run the svg to font task, converting the svgs AND setting up a scss file ready to be ran into the styles gulp
gulp.task('iconfont', function(cb){
  return gulp.src([dirpath + 'icons/*.svg']) // Source folder containing the SVG images
    .pipe(iconfontCss({
      fontName: fontName, // The name that the generated font will have
      path: dirpath + 'scss/fonts/_template.scss', // The path to the template that will be used to create the SASS/LESS/CSS file
      targetPath: '../../scss/fonts/_icons.scss', // The path where the file will be generated
      fontPath: '../iconfont/', // The path to the icon font file
      cssClass: cssClass

    }))
    .pipe(iconfont({
      prependUnicode: false, // Recommended option
      fontName: fontName, // Name of the font
      normalize: true,
      fontHeight: 1001, // fixes badly rendered icons
      timestamp: runTimestamp, // Recommended to get consistent builds when watching files
      log: () => {} // suppress unnecessary logging
    }))
    .pipe(gulp.dest(dirpath + 'icons/iconfont'))
    cb();
});

// move the icons across to the dist folder
gulp.task('moveIcons', function(cb){
    gulp.src([dirpath + 'icons/iconfont/**/*.*'])
    .pipe(gulp.dest(themepath + 'iconfont/'))
    cb();
});



// WHAT DOES IT DO?
//
//
