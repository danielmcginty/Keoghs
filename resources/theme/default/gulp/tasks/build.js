var gulp = require('gulp');
var runSequence = require('run-sequence');
var argv = require('yargs').argv;

/**
 * Build task
 *
 * Runs all the build tasks in this file from one command.
 *
 * Usage: gulp build, gulp build --prod
 */

gulp.task('build', function(cb) {
  if (argv.prod === undefined) {
    gulp.start('build:development', cb);
  } else {
    gulp.start('build:production', cb);
  }
});

gulp.task('build:development', function (cb) {
  runSequence(
    //['clean'],
    ['scripts', 'styles', 'images'],
  cb);
});

gulp.task('build:production', function (cb) {
  runSequence(
    //['clean'],
    ['scripts:minify', 'styles', 'images', 'copy:xsl'],
  cb);
});
