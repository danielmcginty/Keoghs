var gulp            = require('gulp');

var order           = require('gulp-order');
var concat          = require('gulp-concat');
var rename          = require('gulp-rename');
var uglify          = require('gulp-uglify');

const config        = require('../config').paths;
var themepath       = config.themepath;

// optimise scripts
gulp.task('scripts', function(cb) {
    gulp.src(['assets/js/plugins/**/*.js', 'assets/js/app.js'])
      .pipe(concat('app.js'))
      .pipe(gulp.dest(themepath + 'js/'));
    gulp.src(['assets/js/jquery/*.js'])
        .pipe(gulp.dest(themepath + 'js/jquery/'));
      cb();
});

gulp.task('scripts:minify', function(cb) {
    gulp.src(['assets/js/plugins/**/*.js', 'assets/js/app.js'])
      .pipe(concat('app.js'))
      .pipe(uglify())
      .pipe(gulp.dest(themepath + 'js/'));
    gulp.src(['assets/js/jquery/*.js'])
      .pipe(gulp.dest(themepath + 'js/jquery/'));
      cb();
});
