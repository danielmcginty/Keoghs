var gulp 		= require('gulp');
var dirpath = '';

// watch
gulp.task('watch:assets', function(){
    gulp.watch(dirpath + 'assets/ui/**/*.*', ['images']);
    gulp.watch(dirpath + 'assets/js/**/*.*', ['scripts']);
    gulp.watch([
        dirpath + 'assets/styles/**/*.*',
        'tailwind.config.js',
    ], ['styles:site']);
})
