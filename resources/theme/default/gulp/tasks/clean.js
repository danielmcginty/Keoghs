var gulp = require('gulp');
var del = require('del');

const config        = require('../config').paths;
var themepath       = config.themepath;

/**
 * Clean output directory
 *
 * Gets rid of all the folders and files created by 'gulp build'
 *
 * Usage: gulp clean
 */
gulp.task('clean', function () {
  return del(themepath, {force: true});
});
