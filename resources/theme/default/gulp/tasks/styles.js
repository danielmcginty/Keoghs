var gulp = require('gulp'),
    rename = require('gulp-rename'),
    SASS = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    tailwindcss = require('tailwindcss'),
    postcss = require('gulp-postcss'),
    runSequence = require('run-sequence');

const config = require('../config').paths;
var themepath = config.themepath;

gulp.task('styles', function (cb) {
    runSequence(
        ['styles:site', 'styles:fonts'],
        cb);
});

gulp.task('styles:fonts', function () {
    return gulp.src('./assets/fonts/*')
        .pipe(gulp.dest(themepath + 'fonts/'));
});

gulp.task('styles:site', function () {
    return gulp.src('./assets/styles/app.scss')
        .pipe(SASS())
        .pipe(postcss([
            tailwindcss('tailwind.config.js'),
            require('autoprefixer'),
            require("cssnano")({
                preset: [
                    'default', {
                        discardComments: {
                            removeAll: true
                        }
                    }
                ]
            })
        ]))
        .pipe(sourcemaps.write('/maps'))
        .pipe(rename("app.css"))
        .pipe(gulp.dest(themepath + 'css/'));
});