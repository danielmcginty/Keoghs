var gulp = require('gulp');

var requireDir = require('require-dir');
var dir = requireDir('./gulp', { recurse: true });
var runSequence = require('run-sequence');

gulp.task('default', function (cb) {
  gulp.start('build', cb);
});

gulp.task('watch', function (cb) {
  runSequence(
    ['build:development'],
    ['watch:assets'],
  cb);
});