{{-- User Analytics --}}
@if( !empty( $google_analytics_ua_code ) )
    <amp-analytics type="googleanalytics">
        <script type="application/json">
            {
              "vars": {
                "account": "{{ $google_analytics_ua_code }}"
              },
              "triggers": {
                "default pageview": {
                  "on": "visible",
                  "request": "pageview",
                  "vars": {
                    "title": "[AMP] {{ !empty( $this_article->h1_title ) ? $this_article->h1_title : $this_article->title }}"
                  }
                }
              }
            }
        </script>
    </amp-analytics>
@endif

@push( 'schema_scripts' )
@include( 'schema.organisation' )
@endpush
@stack( 'schema_scripts' )

</body>
</html>