@if( isset( $breadcrumbs ) && !empty( $breadcrumbs ) )

<div class="page-breadcrumbs">
    <ul class="breadcrumbs">
        @foreach( $breadcrumbs as $ind => $crumb )
            @if( ($ind+1) == sizeof( $breadcrumbs ) )
                <li>{{ $crumb['label'] }}</li>
            @else
                <li><a href="{{ $crumb['url'] }}">{{ $crumb['label'] }}</a></li>
            @endif
        @endforeach
    </ul>
</div>

@endif