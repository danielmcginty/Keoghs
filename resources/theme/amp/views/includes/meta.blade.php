<!doctype html>
<html amp lang="en">
<head>

    <title>{{ $meta_title }} | {{ config('app.name', 'Laravel') }}</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no,minimal-ui">

@if( !empty( $canonical_url ) )
    <link rel="canonical" href="{{ $canonical_url }}" />
@endif

    {{-- AMP Boilerplate Styles --}}
    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>

    {{-- Custom Styles --}}
    <style amp-custom>
        <?php

        // Make the path to the font file absolute
        $css_content = '';
        if( file_exists( public_path( $amp_stylesheet ) ) ){
            $css_content = str_replace( '../fonts/', '/dist/assets/fonts/', file_get_contents( public_path( $amp_stylesheet ) ) );
        }

        // Remove any !important rules as these are not allowed by AMP
        // See: https://www.ampproject.org/docs/reference/spec#important
        echo str_replace( "!important", "", $css_content );

        ?>
    </style>

    {{-- Google Fonts --}}
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,700" rel="stylesheet" type="text/css">

    {{-- AMP Scripts --}}
    <script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
    <script src="https://cdn.ampproject.org/v0.js" async></script>

</head>
<body>