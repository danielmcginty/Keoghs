<header>
    @if( file_exists( public_path( $amp_svg_logo ) ) )
    <a href="#">
        {!! str_replace( '<?xml version="1.0" encoding="utf-8"?>', '', file_get_contents( public_path( $amp_svg_logo ) ) ) !!}
    </a>
    @endif
</header>