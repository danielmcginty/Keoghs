<?php
    $schema_street_address = [];
    $company_name = translate('address_company_name');
    $first_line = translate('address_line_one');
    $second_line = translate('address_line_two');
    if( !empty( $company_name ) ){ $schema_street_address[] = $company_name; }
    if( !empty( $first_line ) ){ $schema_street_address[] = $first_line; }
    if( !empty( $second_line ) ){ $schema_street_address[] = $second_line; }
    $schema_street_address = array_filter( $schema_street_address );

    // Create variables so we can check if any elements of the address are empty
    $address_city = translate( 'address_city' );
    $address_county = translate( 'address_county' );
    $address_country = translate( 'address_country' );
    $address_postcode = translate( 'address_postcode' );

    // Ensure that no empty social links appear
    $social_links = array_filter( [
            'facebook' => translate('social_facebook'),
            'twitter' => translate('social_twitter'),
            'linkedin' => translate('social_linkedin'),
            'youtube' => translate('social_youtube'),
            'google_plus' => translate('social_google_plus'),
            'pinterest' => translate('social_pinterest'),
            'instagram' => translate('social_instagram')
    ] );
    $social_link_index = 0;

    $schema_array = [
            '@context' => 'http://schema.org',
            '@type' => 'Organization',
            'name' => $company_name,
            'url' => route('theme'),
            'logo' => $dist_dir . 'assets/ui/schema-logo.png', // TODO - create an OG:logo 500px square
    ];
    if( !empty( translate( 'meta_description' ) ) ){
        $schema_array['description'] = translate('meta_description');
    }
    if( !empty( $website_email ) ){
        $schema_array['email'] = $website_email;
    }
    if( !empty( $website_telephone ) ){
        $schema_array['telephone'] = $website_telephone;
    }
    if( !empty( $schema_street_address ) && !empty( $address_city ) && !empty( $address_county ) && !empty( $address_country ) && !empty( $address_postcode ) ){
        $schema_array['address'] = [
                '@type' => 'PostalAddress',
                'streetAddress' => $schema_street_address,
                'addressLocality' => $address_city,
                'addressRegion' => $address_county,
                'addressCountry' => $address_country,
                'postalCode' => $address_postcode
        ];
    }
    if( !empty( $social_links ) ){
        $schema_array['sameAs'] = array_values( $social_links );
    }
    $schema_json = json_encode( $schema_array, JSON_PRETTY_PRINT );
?>
<script type="application/ld+json">
{!! $schema_json !!}
</script>
