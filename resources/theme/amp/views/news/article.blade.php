@extends('layouts.default')

@section('content')
<div class="container">
    @include( 'includes.breadcrumbs' )

    <h1 class="title">{{ $this_page->h1_title }}</h1>

    <div class="article-info">
        <a href="{{ route('theme', [$this_page->category->url]) }}" title="View all {{ $this_page->category->title }} articles" class="article-category">{{ $this_page->category->title }}</a>
        <span class="article-author">{{ !empty( $this_page->author ) ? $this_page->author->name . ' | ' : '' }}{{ $this_page->created_at->format('jS F Y') }}</span>
    </div>

    @if( $this_page->image )

        <div class="article-image">
            <amp-img
                src="{{ $this_page->image['amp']['src'] }}"
                alt="{{ $this_page->h1_title }}"
                width="{{ $this_page->image['amp']['width'] }}"
                height="{{ $this_page->image['amp']['height'] }}"
                layout="responsive"
                class="width-100 margin-b30"
            >
            </amp-img>
        </div>

    @endif

    <div class="cms-content">

        {!! strip_tags( $this_page->content, '<h1><h2><h3><h4><h5><h6><p><span><em><strong><b><i><a><br><hr><ul><ol><li>' ) !!}

    </div>

    <?php
        $phone = translate( 'telephone_number' );
        $phone_link = str_replace( ' ', '', $phone );

        $contact_page = \App\Models\Url::getSystemPage( 'page', 'contact' );
    ?>
    <div class="article-footer">
        @if( !empty( $contact_page ) )
            <a href="{{ route('theme', [$contact_page->url]) }}" class="button">Contact our Experts</a>
            <br />
        @endif
        <a href="tel:{{ $phone_link }}" class="tel-link">{{ $phone }}</a>
    </div>

</div>

@push( 'schema_scripts' )
@include( 'schema.news-article', ['article' => $this_page] )
@endpush
@endsection