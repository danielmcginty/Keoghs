@include('includes.meta')
<div class="main-body">
    @include('includes.header')

    @yield('content')

</div>
@include('includes.footer')