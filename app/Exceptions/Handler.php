<?php

namespace App\Exceptions;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ErrorController;
use Throwable;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Psy\Exception\ErrorException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Throwable  $exception
     * @return void
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Throwable $exception){
        // If the thrown exception is a HTTP Exception, then use the normal routine
        if( $exception instanceof HttpException ){
            return $this->renderHttpException( $exception );
        }

        // If the thrown exception was related to a HTTP POST being too large, redirect the user back with a generic error
        if( $exception instanceof \Illuminate\Http\Exceptions\PostTooLargeException ){
            $request->session()->flash( 'warning', $exception->getMessage() );
            return redirect()->back()->withInput( $request->except('_token') );
        }

        // We need to check the type of exception, to ensure that it's not just a 500 error
        $exception_throwable = false;
        if( $exception instanceof \Illuminate\Auth\AuthenticationException ){ $exception_throwable = true; }
        if( $exception instanceof \Illuminate\Auth\Access\AuthorizationException ){ $exception_throwable = true; }
        if( $exception instanceof \Symfony\Component\HttpKernel\Exception\HttpException ){ $exception_throwable = true; }
        if( $exception instanceof \Illuminate\Database\Eloquent\ModelNotFoundException ){ $exception_throwable = true; }
        if( $exception instanceof \Illuminate\Session\TokenMismatchException ){ $exception_throwable = true; }
        if( $exception instanceof \Illuminate\Validation\ValidationException ){ $exception_throwable = true; }

        // Else, if the theme has a generic error template, throw a controlled error
        if( !$exception_throwable && view()->exists( "errors.generic" ) ){
            $ErrorController = new ErrorController();
            return $ErrorController->throwError( $exception );
        }

        // Else, let Laravel decide what to do
        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        $action = $request->route()->getAction();
        if( $action['prefix'] == 'adm' ){
            return redirect()->route('adm.login');
        }

        return redirect()->guest('login');
    }

    /**
     * Custom Exception Handle function.
     * Based on the HTTP Status Code, we can show a different template (if required) - i.e. a different page for 404,
     * 503, etc.
     *
     * @param \Symfony\Component\HttpKernel\Exception\HttpExceptionInterface $e
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function renderHttpException( HttpExceptionInterface $e ){
        $status = $e->getStatusCode();

        // If we have a dedicated error page for this HTTP Status Code
        if( view()->exists("errors.{$status}") ){
            return response()->view("errors.{$status}", ['exception' => $e], $status, $e->getHeaders());
        }
        // Else, render as Laravel will
        else {
            return $this->convertExceptionToResponse($e);
        }
    }
}
