<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use App\Library\MyBladeCompiler;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Bind our custom blade compiler to allow us to turn caching on or off
        $app = \App::make('app');
        $app->bind('blade.compiler', function($app){
            $cache = $app['path.storage'] . '/framework/cache';
            return new MyBladeCompiler( $app['files'], $cache );
        });

        // Only bind the Debugbar to display if debug is enabled
        if( config( 'app.debug' ) ){
            $app->register('\Barryvdh\Debugbar\ServiceProvider');
        }

        Blade::withoutDoubleEncoding();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
