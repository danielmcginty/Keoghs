<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class MigrationServiceProvider extends ServiceProvider {

    /**
     * When Performing Database Migrations, also run migrations from modular database/migrations directories.
     */
    public function boot(){
        $modules_dir = base_path( 'modules' );
        $modules = array_filter( glob($modules_dir.'/*', GLOB_ONLYDIR) );

        foreach( $modules as $module_path ) {
            if( is_dir( $module_path . '/database' ) ){
                $this->loadMigrationsFrom( $module_path . '/database/migrations' );
            }

            if( is_dir( $module_path . '/Database' ) ){
                $this->loadMigrationsFrom( $module_path . '/Database/migrations' );
            }
        }
    }
}