<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;

class LanguageServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     * Determines the currently browsed language.
     * If the current URL doesn't belong to a language, use the environment default.
     *
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    public function boot(Request $request)
    {
        /*
         * If we don't have a language in the session,
         * or the language in the session doesn't follow the same URL base as the language we have,
         * then we need to grab it afresh from the database
         */
        if( !\Session::has('language_id') && stripos( $request->url(), (string)\Session::get('language_base') ) !== 0 ){
            $language = $this->_determineLanguage( $request );
            if( $language !== false ){
                \Session::put('language_id', $language['id']);
                \Session::put('language_base', $language['base']);
                \Session::put('language_locale', $language['locale']);
            } else {
                \Session::put('language_id', env('DEFAULT_LANGUAGE'));
                \Session::put('language_base', env('APP_URL'));
            }
        }
    }

    /**
     * From the current request, determine the current language
     *
     * @param \Illuminate\Http\Request $request
     * @return bool
     */
    private function _determineLanguage( $request ){
        if( !\Schema::hasTable('languages') ){ return false; }
        $languages = \DB::table('languages')->get();

        $current_url = $request->url();
        foreach( $languages as $language ){
            $language_base = $language->url;

            if( stripos( $current_url, $language_base ) === 0 ){
                return ['id' => $language->id, 'base' => $language_base, 'locale' => $language->locale];
            }
        }

        return false;
    }
}
