<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';
    protected $cms_namespace = 'Adm\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapAdmRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/web.php');
        });
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapAdmRoutes()
    {
        // Before we load the base 'adm/' routes, check for any modular specific routes.
        // We need to do this first, as the main 'adm.path' route is a catch-all and will selfishly hog all other
        // routes that exist in the modules.
        $modules_dir = base_path( 'modules' );
        $modules = array_filter( glob($modules_dir.'/*', GLOB_ONLYDIR) );

        foreach( $modules as $module_path ) {
            $path_parts = explode('/', $module_path);
            $module_ns = end($path_parts);

            $routes_dir = base_path( 'modules/' . $module_ns . '/routes/' );
            $routes_file = rtrim( $routes_dir, '/' ) . '/adm.php';

            if ( is_dir( $routes_dir ) && file_exists( $routes_file ) && is_file( $routes_file ) ) {

                $namespace = 'Modules\\' . $module_ns . '\\Adm\\Controllers';

                Route::group([
                    'middleware' => ['web'],
                    'namespace' => $namespace,
                    'prefix' => 'adm',
                ], function ($router) use ($routes_file) {
                    require $routes_file;
                });
            }
        }

        Route::group([
            'middleware' => ['web'],
            'namespace' => $this->cms_namespace,
            'prefix' => 'adm',
        ], function ($router) {
            require base_path('routes/adm.php');
        });
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::group([
            'middleware' => 'api',
            'namespace' => $this->namespace,
            'prefix' => 'api',
        ], function ($router) {
            require base_path('routes/api.php');
        });
    }
}
