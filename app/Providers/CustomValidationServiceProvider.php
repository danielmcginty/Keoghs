<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class CustomValidationServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the validation rules, applying them to all instances of the Validator class
     */
    public function boot(){

        $this->_applyValidationRules();

        $this->_applyValidationMessages();

    }

    private function _applyValidationRules(){

        /**
         * Adds a validation rule to check the number of words submitted, and if over the limit, throws an error
         */
        Validator::extend( 'max_words', function( $attribute, $value, $parameters, $validator ){
            $words = preg_split( '@\s+@i', $value );
            return count( $words ) <= $parameters[0];
        });

        /**
         * Counterpart to 'required_with_all' validation rule.
         * Checks if all fields provided are present and not empty, else throws a validation error
         */
        Validator::extend( 'required_and_not_empty_without_all', function( $attribute, $value, $parameters, $validator ){
            // First, get the POSTed inputs so we can check the required elements are there
            $inputs = $validator->getData();

            $other_fields_filled_in = false;
            foreach( $parameters as $other_field ){
                $other_value = data_get( $inputs, $other_field );
                if( !empty( $other_value ) ){
                    $other_fields_filled_in = true;
                }
            }

            return !empty( $value ) || $other_fields_filled_in;
        });

        /**
         * Counterpart to 'mimes' validation rule.
         * Checks if the file uploaded is NOT one of the mime types provided, and throws an error if so
         */
        Validator::extend( 'not_mimes', function( $attribute, $value, $parameters, $validator ){
            if( !$validator->isAValidFileInstance( $value ) ){
                return false;
            }

            return $value->getPath() != '' && !in_array($value->getClientOriginalExtension(), $parameters);
        });

    }

    private function _applyValidationMessages(){

        /**
         * Adds a validation rule to check the number of words submitted, and if over the limit, throws an error
         */
        Validator::replacer( 'max_words', function( $message, $attribute, $rule, $parameters ){
            if( $message != 'validation.max_words' ){
                return $message;
            }

            return sprintf(
                "%s must contain fewer than %d words.",
                ucwords( str_replace( array( "_", "-" ), " ", $attribute ) ),
                (int)$parameters[0]
            );
        });

        /**
         * Counterpart to 'required_with_all' validation rule.
         * Checks if all fields provided are present and not empty, else throws a validation error
         */
        Validator::replacer( 'required_and_not_empty_without_all', function( $message, $attribute, $rule, $parameters ){
            if( $message != 'validation.required_and_not_empty_without_all' ){
                return $message;
            }

            $other_fields = [];
            foreach( $parameters as $parameter ){
                $other_fields[] = ucwords( str_replace( array( "_", "-", "." ), " ", $parameter ) );
            }

            return sprintf(
                "%s is required without %s",
                ucwords( str_replace( array( "_", "-", "." ), " ", $attribute ) ),
                implode( ", ", $other_fields )
            );
        });

        /**
         * Counterpart to 'mimes' validation rule.
         * Checks if the file uploaded is NOT one of the mime types provided, and throws an error if so
         */
        Validator::replacer( 'not_mimes', function( $message, $attribute, $rule, $parameters ){
            return str_replace(':values', implode( ', ', $parameters ), $message);
        });

    }
}