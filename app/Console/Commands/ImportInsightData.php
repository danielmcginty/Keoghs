<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Insights\Library\InsightImportProcessor;

class ImportInsightData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insights:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import & Combine old & new insight data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        //return;
        $processor = new InsightImportProcessor;

        $processor->process_additional_categories();
    }
}
