<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\OurPeople\Library\PeopleImportProcessor;

class ImportPeopleData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'people:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import & Combine old & new people data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $processor = new PeopleImportProcessor();

        $processor->process();
    }
}
