<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreateModule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:module';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Module for the CMS 2016 system';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->namespace = ucfirst( \Illuminate\Support\Str::camel( str_replace( " ", "_", $this->ask('What is your module\'s name? (Used for the module namespace)') ) ) );

        $needsNavigation = $this->confirm('Does your module need a navigation item adding to the menu?');

        $needsConfig = $this->confirm('Would you like to create a config class to define field labels and help text?');

        $needsMigration = $this->confirm('Will you be creating a database migration for your module?');

        $needsSeed = $this->confirm('Will you be creating a database seed for your module?');

        $needsTheme = $this->confirm('Will your module be used on the site\'s front end?');

        $needsThemeController = $needsThemeModel = $needsThemeView = false;
        $themeControllerName = $themeModelName = $themeModelMultiLingual = $themeViewName = false;
        if( $needsTheme ){
            $needsThemeController = $this->confirm('Will your module need a front end controller?');

            $themeControllerName = false;
            if( $needsThemeController ){
                $themeControllerName = $this->ask('What do you want to call your controller? (No need to add \'Controller\' after it, this will automatically be added)');
                $themeControllerName = ucfirst( \Illuminate\Support\Str::camel( str_replace( " ", "_", $themeControllerName ) ) );
            }

            $needsThemeModel = $this->confirm('Will your module need a front end model?');

            $themeModelName = $themeModelMultiLingual = false;
            if( $needsThemeModel ){
                $themeModelName = $this->ask('What do you want to call your model?');
                $themeModelMultiLingual = $this->confirm('Does this model relate to translatable content?');

                $themeModelName = ucfirst( \Illuminate\Support\Str::camel( str_replace( " ", "_", $themeModelName ) ) );
            }

            $needsThemeView = $this->confirm('Will your module need it\'s own view templates?');

            $themeViewName = false;
            if( $needsThemeView ){
                $themeViewName = $this->ask('What you want to call your template? (No need to add \'.blade.php\' after this, this will automatically be added)');

                $themeViewName = strtolower( str_replace( " ", "_", $themeViewName ) );
            }
        }

        $this->_createModuleBaseDir();

        $this->_createAdm( $needsConfig );

        if( $needsNavigation || $needsConfig ){
            $this->_createConfigDir( $needsNavigation, $needsConfig );
        }

        if( $needsMigration || $needsSeed ){
            $this->_createDBDir( $needsMigration, $needsSeed );
        }

        if( $needsTheme ){
            $this->_createThemeDir( $themeControllerName, $themeModelName, $themeModelMultiLingual, $themeViewName, $needsConfig );
        }
    }

    /*
     * Properties & Functions custom to this command
     */
    /**
     * The new module's base directory
     *
     * @var string
     */
    private $module_dir;

    /**
     * The new module's namespace
     *
     * @var string
     */
    private $namespace;

    /**
     * What permissions to give to newly created directories
     *
     * @var int
     */
    private $directory_permissions = 0755;

    /**
     * Creates the 'base' directory for the module (/modules/namespace_provided_above)
     * Presents an error if the namespace already exists, or if the directory cannot be created.
     */
    private function _createModuleBaseDir(){
        $modules_dir = base_path( 'modules' );

        $this->module_dir = $modules_dir . '/' . $this->namespace . '/';

        // If the module already exists, prompt the user with an error message
        if( is_dir( $this->module_dir ) ){
            $this->error( 'That module already exists. You\'ll need to change the namespace to something else, or modify the module to suit.' );
            die();
        }

        if( $this->_createNewDirectory( $this->module_dir ) ){
            $this->info( 'Module base directory created.' );
        }
    }

    /**
     * Creates the 'Adm' directory for the module (/modules/namespace_provided_above/Adm)
     * Also created the 'Adm/Controllers' directory and the ModuleController scaffold.
     *
     * @param $needsConfig
     */
    private function _createAdm( $needsConfig ){
        // First, create the 'module/namespace/adm' directory
        $adm_dir = $this->module_dir . 'Adm/';
        $this->_createNewDirectory( $adm_dir );

        // Now, we can create the 'module/namespace/adm/Controllers' directory
        $controller_dir = $adm_dir . 'Controllers/';
        $this->_createNewDirectory( $controller_dir );

        // Now, we can create the 'module/namespace/adm/Controllers/ModuleController' controller
        $controller_contents = $this->_getModuleAdmControllerScaffold( $needsConfig );
        $controller_path = $controller_dir . 'ModuleController.php';
        $this->_writeNewFile( $controller_contents, $controller_path );

        $this->info( 'Module Admin scaffold created.' );
    }

    /**
     * Creates the 'Config' directory for the module (/modules/namespace_provided_above/Config)
     * Creates the 'Navigation' and 'Config' model scaffolds if required
     *
     * @param $navigation
     * @param $config
     */
    private function _createConfigDir( $navigation, $config ){
        // First, create the 'module/namespace/config' directory
        $config_dir = $this->module_dir . 'Config/';
        $this->_createNewDirectory( $config_dir );

        // If we need to add the navigation class
        if( $navigation ){
            $navigation_contents = $this->_getModuleNavigationScaffold();
            $navigation_path = $config_dir . 'Navigation.php';
            $this->_writeNewFile( $navigation_contents, $navigation_path );

            $this->info( "Admin Navigation scaffold created." );
        }

        // If we need to add the config class
        if( $config ){
            $config_contents = $this->_getModuleConfigScaffold();
            $config_path = $config_dir . $this->namespace . 'Config.php';
            $this->_writeNewFile( $config_contents, $config_path );

            $this->info( "Shared Config scaffold created." );
        }
    }

    /**
     * Creates the 'database' directory for the module (/modules/namespace_provided_above/database)
     * Creates the 'migrations' and 'seeds' directory if required.
     *
     * @param $migrations
     * @param $seeds
     */
    private function _createDBDir( $migrations, $seeds ){
        // First, create the 'module/namespace/config' directory
        $db_dir = $this->module_dir . 'database/';
        $this->_createNewDirectory( $db_dir );

        // If we need to add the navigation class
        if( $migrations ){
            $migration_dir = $db_dir . 'migrations/';
            $this->_createNewDirectory( $migration_dir );

            $this->info( "Migrations directory created." );
        }

        // If we need to add the config class
        if( $seeds ){
            $seeds_dir = $db_dir . 'seeds/';
            $this->_createNewDirectory( $seeds_dir );

            $this->info( "Seeds directory created." );
        }
    }

    /**
     * Creates the 'Theme' directory for the module (/modules/namespace_provided_above/Theme)
     * Also creates the controller, model & view if required
     *
     * @param $controller
     * @param $model
     * @param $multilingual
     * @param $view
     * @param $needsConfig
     */
    private function _createThemeDir( $controller, $model, $multilingual, $view, $needsConfig ){
        $theme_dir = $this->module_dir . 'Theme/';
        $this->_createNewDirectory( $theme_dir );

        if( $controller !== false ){
            $controller_dir = $theme_dir . 'Controllers/';
            $this->_createNewDirectory( $controller_dir );

            // Generate Controller Scaffold
            $controller_contents = $this->_getModuleThemeControllerScaffold( $controller, $model );
            $controller_path = $controller_dir . $controller . 'Controller.php';

            $this->_writeNewFile( $controller_contents, $controller_path );

            $this->info( "Front End Controller created." );
        }

        if( $model !== false ){
            $model_dir = $theme_dir . 'Models/';
            $this->_createNewDirectory( $model_dir );

            // Generate Model Scaffold
            $model_contents = $this->_getModuleThemeModelScaffold( $model, $multilingual, $needsConfig );
            $model_path = $model_dir . $model . '.php';

            $this->_writeNewFile( $model_contents, $model_path );

            $this->info( "Front End Model created." );
        }

        if( $view !== false ){
            $view_dir = $theme_dir . 'views/';
            $this->_createNewDirectory( $view_dir );

            // Create view blade
            $blade_path = $view_dir . $view . '.blade.php';
            $this->_writeNewFile( '\@extends(\'layouts.theme\')', $blade_path );

            $this->info( "Front End View (blade) created." );
        }
    }

    /**
     * Generate a scaffold PHP file for the Admin ModuleController.php
     *
     * @param $needsConfig
     * @return string
     */
    private function _getModuleAdmControllerScaffold( $needsConfig ){
        $controllerStr = "<?php
namespace Modules\\" . $this->namespace . "\\Adm\\Controllers;

use Adm\\Controllers\\AdmController;
";
        if( $needsConfig ){
            $controllerStr .= "
use Modules\\" . $this->namespace . "\\Config\\" . $this->namespace . "Config;
            ";
        }

        $controllerStr .= "
class ModuleController extends AdmController {
    /**
     * " . \Illuminate\Support\Str::plural( $this->namespace ) . "
     *
     * CRUD function for the " . strtolower( \Illuminate\Support\Str::plural( $this->namespace ) ) . " area of the CMS
     */
    public function " . strtolower( \Illuminate\Support\Str::plural( $this->namespace ) ) . "(){
        // Create your CRUD functionality here
        // You should access \$this->crud to manipulate it
    }
}
";

        return $controllerStr;
    }

    /**
     * Generate a scaffold PHP file for the Admin Navigation.php class
     *
     * @return string
     */
    private function _getModuleNavigationScaffold(){
        $navigationStr = "<?php
namespace Modules\\" . $this->namespace . "\\Config;

use Adm\\Interfaces\\NavigationInterface;

class Navigation implements NavigationInterface {
    public function getNavItems(){
        // Add your admin navigation items inside the array rather
        return array(
            // array( 'title' => '" . $this->namespace . "', 'url' => 'adm/" . strtolower( \Illuminate\Support\Str::plural( $this->namespace ) ) . "', 'position' => 30 ),
        );
    }
}
";

        return $navigationStr;
    }

    /**
     * Generate a scaffold PHP file for the Shared Config.php class
     *
     * @return string
     */
    private function _getModuleConfigScaffold(){
        $configStr = "<?php
namespace Modules\\" . $this->namespace . "\\Config;

class " . $this->namespace . "Config {
    /**
     * Stores input labels
     * @var array
     */
    public \$labels = [
        // 'field_name' => 'Field Label'
    ];

    /**
     * Stores input help text
     * @var array
     */
    public \$help = [
        // 'field_name' => 'Field Help Text'
    ];

    /**
     * Stores a shared configuration for image sizes
     * @var array
     */
    public \$image_sizes = [
        // 'image' => [
        //      'desktop' => [
        //          'label' => 'Desktop',
        //          'width' => 1920,
        //          'height' => 400,
        //          'primary' => true
        //      ],
        // ];
    ];
}
";

        return $configStr;
    }

    /**
     * Generate a scaffold PHP file for the Theme Controller
     *
     * @param $controller
     * @param $model
     * @return string
     */
    private function _getModuleThemeControllerScaffold( $controller, $model ){
        $controllerStr = "<?php
namespace Modules\\" . $this->namespace . "\\Theme\\Controllers;

use App\\Http\\Controllers\\Controller;
use Illuminate\\Http\\Request;
";
        if( $model !== false ){
            $controllerStr .= "
use Modules\\" . $this->namespace . "\\Theme\\Models\\" . $model . ";
            ";
        }

        $controllerStr .= "
use App\\Models\\Url;

class " . $controller . "Controller extends Controller {

    /**
     * The default " . strtolower( $this->namespace ) . " loader
     * Takes a " . $this->namespace . " ID and a HTTP Request
     *
     * @param \$" . strtolower( $this->namespace ) . "_id
     * @param Request \$request
     * @return bool|\\Illuminate\\Contracts\\View\\Factory|\\Illuminate\\View\\View
     */
    public function index( \$" . strtolower( $this->namespace ) . "_id, Request \$request ){

    }
}
";

        return $controllerStr;
    }

    /**
     * Generate a scaffold PHP file for the Theme Model
     *
     * @param $model
     * @param $modelMultilingual
     * @param $needsConfig
     * @return string
     */
    private function _getModuleThemeModelScaffold( $model, $modelMultilingual, $needsConfig ){
        $modelStr = "<?php
namespace Modules\\" . $this->namespace . "\\Theme\\Models;

use App\\Models\\CommonModel;
use Illuminate\\Database\\Eloquent\\SoftDeletes;";
        if( $modelMultilingual ){
            $modelStr .= "
use App\\Models\\Traits\\HasCompositePrimaryKey;
use App\\Models\\Scopes\\LanguageScope;
            ";
        }

        $modelStr .= "
use App\\Models\\Scopes\\PublishedScope;";

        if( $needsConfig ) {
            $modelStr .= "
use Modules\\" . $this->namespace . "\\Config\\" . $this->namespace . "Config;
use App\\Helpers\\AssetHelper;
            ";
        }

        $modelStr .= "
class " . $model . " extends CommonModel {
    use SoftDeletes";
        if( $modelMultilingual ){
            $modelStr .= ", HasCompositePrimaryKey;";
        }

        $modelStr .= "
    protected \$table = ''; // Table name goes here
        ";

        if( $modelMultilingual ){
            $modelStr .= "
    protected \$primaryKey = ['" . strtolower( $this->namespace ) . "_id', 'language_id']; // Primary keys go in here
            ";
        } else {
            $modelStr .= "
    protected \$primaryKey = '" . strtolower( $this->namespace ) . "_id'; // Primary key goes here
            ";
        }

        $modelStr .= "

    protected static function boot(){
        parent::boot();";

        if( $modelMultilingual ) {
            $modelStr .= "
        static::addGlobalScope(new LanguageScope);
            ";
        }

        $modelStr .= "
        static::addGlobalScope(new PublishedScope);
    }";

        if( $needsConfig ) {
            $modelStr .= "

    public function getImageAttribute(){
        \$config = new " . $this->namespace . "Config;
        \$helper = new AssetHelper;";

            if( $modelMultilingual ){
                $modelStr .= "
        return \$helper->getAssets( \$this->table, \$this->" . strtolower( $this->namespace ) . "_id, \$this->language_id, 'image', \$config->image_sizes['image'] );
                ";
            } else {
                $modelStr .= "
        return \$helper->getAssets( \$this->table, \$this->" . strtolower( $this->namespace ) . "_id, 0, 'image', \$config->image_sizes['image'] );
                ";
            }

            $modelStr .= "
    }";
        }

        $modelStr .= "
}
";

        return $modelStr;
    }

    /**
     * Centralised function to create a new directory within the module.
     *
     * @param $directory_name
     * @return bool
     */
    private function _createNewDirectory( $directory_name ){
        if( mkdir( $directory_name, $this->directory_permissions ) ){
            return true;
        } else {
            $this->error( 'Something has gone wrong creating the directory \'' . $directory_name . '\'. Please try again or check file permissions.' );
            die();
        }
    }

    /**
     * Centralised function to create a new file within the module, and write contents to it
     *
     * @param $contents
     * @param $path
     */
    private function _writeNewFile( $contents, $path ){
        if( !is_file( $path ) ){
            $handle = fopen( $path, 'w' );
            fwrite( $handle, $contents );
            fclose( $handle );
        } else {
            $this->error( "File $path already exists! Are you sure this is a new module?" );
            die();
        }
    }
}
