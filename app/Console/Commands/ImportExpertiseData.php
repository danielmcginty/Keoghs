<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Expertise\Library\ExpertiseImportProcessor;

class ImportExpertiseData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expertise:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import expertise data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $processor = new ExpertiseImportProcessor();

        $processor->process();
    }
}
