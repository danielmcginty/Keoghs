<?php
namespace App\Helpers;

use App\Models\File;
use App\Models\Url;
use App\Models\Redirect;

/**
 * Class ShortCodeHelper
 *
 * The ShortCode Helper is used to convert various short codes used within the system into usable dynamic content.
 * For example, all images in the CMS are stored as short codes - then, when a template is processed on the front-end
 * the short codes are replaced with <img/> tags.
 *
 * @package App\Helpers
 */
class ShortCodeHelper {

    /**
     * Variable storing the format of an asset short code, ready for a sprintf call
     * @var string
     */
    public static $asset_format = '[[asset id="%d"]]';

    /**
     * Variable storing a regex version of the above asset format, ready for preg_match
     * @var string
     */
    private static $asset_regex = '/\[\[asset\sid="\d*"\s?\]\]\s?/';

    /**
     * Variable storing a regex template to get the fields out from an asset short code
     * @var string
     */
    private static $asset_field_regex = '/\[\[asset\sid="(\d*)"\s?\]\]\s?/';

    /**
     * Variable storing the format of an image URL (needs to be dynamic as the website URL changes!)
     * @var string
     */
    private static $asset_url_format = '';

    /**
     * Variable storing the format of a file short code, ready for a sprintf call
     * @var string
     */
    public static $file_format = '[[file id="%d"]]';

    /**
     * Variable storing a regex version of the above file format, ready for preg_match
     * @var string
     */
    private static $file_regex = '/\[\[file\sid="\d*"\s?\]\]\s?/';

    /**
     * Variable storing a regex template to get the fields out from a file short code
     * @var string
     */
    private static $file_field_regex = '/\[\[file\sid="(\d*)"\s?\]\]\s?/';

    /**
     * Variable storing the format of a file URL (needs to be dynamic as the website URL changes!)
     * @var string
     */
    private static $file_url_format = '';

    /**
     * Variable storing the format of a link short code, ready for a sprintf call
     * @var string
     */
    public static $link_format = '[[url table_name="%s" table_key="%d"]]';

    /**
     * Variable storing a regex version of the above link format, ready for preg_match
     * @var string
     */
    private static $link_regex = '/\[\[url\stable_name="[^\"]*"\stable_key="\d*"\s?\]\]\s?"/';

    /**
     * Variable storing a regex template to get the fields out from a link short code
     * @var string
     */
    private static $link_field_regex = '/\[\[url\stable_name="([^\"]*)"\stable_key="(\d*)"\s?\]\]\s?/';

    /**
     * Variable storing the format of an link URL (needs to be dynamic as the website URL changes!)
     * @var string
     */
    private static $link_url_format = '';

    /**
     * Helper function converting link [[ short codes ]] into <a> tags.
     *
     * @param $content
     * @return mixed
     */
    public static function shortCodeToURLs( $content ){
        self::setURLFormats();

        // First, check to see if there are any asset short codes in the content
        preg_match_all( self::$asset_regex, $content, $matches );

        if( sizeof( $matches ) > 0 ){
            foreach( $matches[0] as $match ){
                $match = trim( rtrim( strip_tags( $match ), '"' ) );
                $image_url = self::imageShortCodeToURL( $match );

                $content = str_replace( $match, $image_url, $content );
            }
        }

        // Then, check to see if there are any file short codes in the content
        preg_match_all( self::$file_regex, $content, $matches );

        if( sizeof( $matches ) > 0 ){
            foreach( $matches[0] as $match ){
                $match = trim( rtrim( strip_tags( $match ), '"' ) );
                $file_url = self::fileShortCodeToURL( $match );

                $content = str_replace( $match, $file_url, $content );
            }
        }

        // Then, check to see if there are any URL short codes in the content
        preg_match_all( self::$link_regex, $content, $matches );

        if( sizeof( $matches ) > 0 ){
            foreach( $matches[0] as $match ){
                $match = trim( rtrim( strip_tags( $match ), '"' ) );
                $link_url = self::themeShortCodeToURL( $match );

                $content = str_replace( $match, $link_url, $content );
            }
        }

        return $content;

    }

    /**
     * Helper function converting an <a> tag into a link [[ short code ]].
     *
     * @param $content
     * @return mixed
     */
    public static function urlsToShortCode( $content ){
        self::setURLFormats();

        // Because an Image URL is an extension of a main front-end URL, we need to check for those first
        $img_regex = '/' . str_replace( array('/', '.'), array('\/', '\.'), self::$asset_url_format ) . '([\w\/.\-]*)"/';
        preg_match_all( $img_regex, $content, $matches );

        if( sizeof( $matches ) > 0 ){
            foreach( $matches[0] as $match ){
                $match = trim( rtrim( strip_tags( $match ), '"' ) );
                $shortCode = self::imageURLToShortCode( $match );

                $content = str_replace( $match, $shortCode, $content );
            }
        }

        // Now that we've parsed image URLs, we can check for file URLs
        $file_regex = '/' . str_replace( '/', '\/', self::$file_url_format ) . '([\w\/.\-]*)"/';
        preg_match_all( $file_regex, $content, $matches );

        if( sizeof( $matches ) > 0 ){
            foreach( $matches[0] as $match ){
                $match = trim( rtrim( strip_tags( $match ), '"' ) );
                $shortCode = self::fileURLToShortCode( $match );

                $content = str_replace( $match, $shortCode, $content );
            }
        }

        // Now that we've parsed everything else, we can check for standard URLs
        $link_regex = '/' . str_replace( '/', '\/', self::$link_url_format ) . '([\w\/\-]*)?"/';
        preg_match_all( $link_regex, $content, $matches );

        if( sizeof( $matches ) > 0 ){
            foreach( $matches[0] as $match ){
                $match = trim( rtrim( strip_tags( $match ), '"' ) );
                $shortCode = self::themeURLToShortCode( $match );

                $content = str_replace( $match, $shortCode, $content );
            }
        }

        return $content;

    }

    /**
     * Function called internally to dynamically set the Front End URL formats.
     */
    private static function setURLFormats(){
        self::$asset_url_format = route('image', '') . '/';
        self::$file_url_format = route('file', '') . '/';
        self::$link_url_format = route('theme') . '/';
    }

    /**
     * For a given image URL, get the [[ short code ]] equivalent.
     *
     * @param $image_url
     * @return string
     */
    private static function imageURLToShortCode( $image_url ){
        // Get the 'slug' for the image URL (i.e. without the http://domain/image)
        $slug = str_replace( self::$asset_url_format, '', $image_url );
        // And also strip any demo / staging site URLs
        $slug = self::replaceOtherEnvUrlsInUrl( $slug );

        // Now, we need to check if the image URL contains any sizing and crop data
        $slug = preg_replace( '/(\d*x\d*)\/(.*)"/', "$2", $slug );
        $slug = preg_replace( '/(\d*x\d*-\d*x\d*)\/(.*)"/', "$2", $slug );
        $slug = preg_replace( '/(.*)"/', "$1", $slug );

        // Now we've got the image slug, including directory, find the file
        $path_parts = array_filter( explode( "/", $slug ) ); // Remove empty 'elements' from the exploded array ( images//path.jpg )
        $file_path = array_pop( $path_parts );

        // If we've got a directory with the image URL, find the directory
        if ($path_parts) {
            $parent_dir = File::getClosestDirectory($path_parts);
        }

        // Now, we can find the image in question
        $file = File::where([
            'file_name' => $file_path,
            'directory_id' => isset($parent_dir) ? $parent_dir->id : 0
        ])->with('directory', 'directory.directory')->first();

        // If the image can't be found, return the original URL
        if( empty( $file ) ){
            return $image_url;
        }

        // Now we've got a valid file, we can generate the short code to return
        $shortCode = sprintf( self::$asset_format, $file->id );

        return $shortCode;
    }

    /**
     * For a given image [[ short code ]], get the image URL equivalent.
     *
     * @param $image_short_code
     * @return string
     */
    private static function imageShortCodeToURL( $image_short_code ){
        $asset_id = preg_replace( self::$asset_field_regex, "$1", $image_short_code );

        if( empty( $asset_id ) ){
            return $image_short_code;
        }

        $image = File::where([
            'id' => $asset_id
        ])->with('directory', 'directory.directory')->first();

        if( empty( $image ) ){
            return $image_short_code;
        }

        $is_amp = request()->route()->getName() == 'amp';

        if( $is_amp ){
            return route('image.inline-resize', ['path' => $image->directory_path . '/' . $image->file_name, 'dimensions' => '580x']);
        }
        return route('image.inline-resize', ['path' => $image->directory_path . '/' . $image->file_name, 'dimensions' => '900x']);
    }

    /**
     * For a given file URL, get the [[ short code ]] equivalent.
     *
     * @param $file_url
     * @return string
     */
    private static function fileURLToShortCode( $file_url ){
        // Get the 'slug' for the file URL (i.e. without the http://domain/files)
        $slug = str_replace( self::$file_url_format, '', $file_url );
        // And also strip any demo / staging site URLs
        $slug = self::replaceOtherEnvUrlsInUrl( $slug );

        // Now we've got the file slug, including directory, find it
        $path_parts = array_filter( explode( "/", $slug ) ); // Remove empty 'elements' from the exploded array ( file//path.jpg )
        $file_path = array_pop( $path_parts );

        // If we've got a directory with the file URL, find the directory
        if( $path_parts ){
            $parent_dir = File::getClosestDirectory( $path_parts );
        }

        // Now, we can find the file in question
        $file = File::where([
            'file_name' => $file_path,
            'directory_id' => isset($parent_dir) ? $parent_dir->id : 0
        ])->with('directory', 'directory.directory')->first();

        // If the file can't be found, return the original URL
        if( empty( $file ) ){
            return $file_url;
        }

        // Now we've got a valid file, we can generate the short code to return
        $shortCode = sprintf( self::$file_format, $file->id );

        return $shortCode;
    }

    /**
     * For a given image [[ short code ]], get the image URL equivalent.
     *
     * @param $file_short_code
     * @return string
     */
    private static function fileShortCodeToURL( $file_short_code ){
        $file_id = preg_replace( self::$file_field_regex, "$1", $file_short_code );

        if( empty( $file_id ) ){
            return $file_short_code;
        }

        $file = File::where([
            'id' => $file_id
        ])->with('directory', 'directory.directory')->first();

        if( empty( $file ) ){
            return $file_short_code;
        }

        return route('file', ['path' => $file->directory_path . '/' . $file->file_name]);
    }

    /**
     * For a given link URL, get the [[ short code ]] equivalent.
     *
     * @param $theme_url
     * @return string
     */
    private static function themeURLToShortCode( $theme_url ){
        // Get the 'slug' for the image URL (i.e. without the http://domain/image)
        $slug = str_replace( self::$link_url_format, '', $theme_url );
        // And also strip any demo / staging site URLs
        $slug = self::replaceOtherEnvUrlsInUrl( $slug );

        // Now we've got the URL slug, we can query the URL table for a match
        $url = Url::where([
            'url' => $slug
        ])->first();

        // If the URL can't be found, check to see if a redirect has been added
        if( empty( $url ) ){

            // So, check the 301_redirect table
            $redirect = Redirect::where([
                'old_url' => $slug
            ])->first();

            // And if we HAVE a redirect, get the URL row based on the 'new_url'
            if( !empty( $redirect ) ){
                $url = Url::where([
                    'url' => $redirect->new_url
                ])->first();
            }

            // And if we haven't been able to get a URL match, just return the original URL
            if( empty( $url ) ) {
                return $theme_url;
            }
        }

        // Now we've got a valid URL, we can generate the short code to return
        $shortCode = sprintf( self::$link_format, trim($url->table_name), trim($url->table_key) );

        return $shortCode;
    }

    /**
     * For a given link [[ short code ]], get the link URL equivalent.
     *
     * @param $theme_short_code
     * @return string
     */
    private static function themeShortCodeToURL( $theme_short_code ){
        $table_name = preg_replace( self::$link_field_regex, "$1", $theme_short_code );
        $table_key = preg_replace( self::$link_field_regex, "$2", $theme_short_code );

        if( empty( $table_name ) || empty( $table_key ) ){
            return $theme_short_code;
        }

        $url = Url::where([
            'table_name' => $table_name,
            'table_key' => $table_key
        ])->first();

        if( empty( $url ) ){
            return $theme_short_code;
        }

        return route('theme', $url->url);
    }

    /**
     * For a given link, replace all possibly stored URL formats based on
     * the website's ENV files.
     * Used to reduce friction on go-lives & content migration
     *
     * @param string $url
     * @return string
     */
    private static function replaceOtherEnvUrlsInUrl( $url ){
        $env_urls = [];
        $env_file_paths = glob( base_path( '.env*' ) );
        foreach( $env_file_paths as $env_file_path ){
            $env_contents = file_get_contents( $env_file_path );
            preg_match_all( '/APP_URL=(.*)/m', $env_contents, $url_matches );
            if( !empty( $url_matches[1] ) ){
                $env_urls[] = trim( str_replace( array( "\r", "\n", "\r\n" ), "", $url_matches[1][0] ) );
            }
        }

        foreach( $env_urls as $env_url ){
            $url = str_replace( $env_url, '', $url );
        }

        return $url;
    }
}