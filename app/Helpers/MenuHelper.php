<?php
namespace App\Helpers;

use App\Models\Url;
use Illuminate\Support\Facades\Cache;

/**
 * Class MenuHelper
 * @package App\Helpers
 *
 * The MenuHelper provides easy access to custom menus within the CMS, and provides caching of menus.
 */
class MenuHelper {
    /**
     * Returns an array of menu items for the provided reference.
     *
     * @param $reference
     * @return bool|mixed
     */
    public static function getMenu( $reference ){
        $cache_key = 'site_menu.' . $reference;
        $menu = Cache::get( $cache_key );
        if( $menu ){
            self::parseMenuItems( $menu );
            return $menu;
        }

        $menu_class = '\\Modules\\MenuBuilder\\Models\\Menu';
        if( class_exists( $menu_class ) ){
            $menu = new $menu_class;

            $this_menu = $menu::where('reference', $reference)->first();

            if( !empty( $this_menu ) ){
                if( is_serialized( $this_menu->structure ) ){ $this_menu->structure = json_encode( unserialize( $this_menu->structure ) ); }
                $structure = json_decode( $this_menu->structure, true );

                Cache::put( $cache_key, $structure, now()->addMinutes( config('app.cache_time') ) );

                self::parseMenuItems( $structure );
                return $structure;
            }
        }

        return false;
    }

    /**
     * Processes the provided menu structure, determining if the current link is 'active' or 'current' based on the
     * current URL.
     *
     * @param $structure
     */
    private static function parseMenuItems( &$structure ){
        if( !is_array( $structure ) ){ return; }

        foreach( $structure as $ind => $item ){
            if( $item['item_type'] != 'custom' ){
                $current_url = str_replace( route('theme' ), '', \Request::url() );
                $current_parts = explode( '/', ltrim( $current_url, '/' ) );
                $this_url_parts = explode( '/', ltrim( $item['link_url'], '/' ) );

                $matched = true;
                foreach( $this_url_parts as $url_ind => $part ){
                    if( !isset( $current_parts[ $url_ind ] ) || $current_parts[ $url_ind ] != $part ){
                        $matched = false;
                    }
                }

                $structure[ $ind ]['is_current'] = $matched;
            }

            if( !empty( $item['children'] ) ){
                self::parseMenuItems( $structure[ $ind ]['children'] );
            }
        }
    }
}