<?php
/**
 * The helper_loader file provides simply, commonly used custom PHP functions to all areas of the system
 */

if( !function_exists( 'load_helper' ) ){
    /**
     * Loads an Application Helper based on it's name, from the App\Helpers namespace.
     *
     * @param $helper_name
     * @return mixed
     */
    function load_helper( $helper_name ){
        $helper_reference = '\\App\\Helpers\\' . $helper_name;

        if( class_exists( $helper_reference ) ){
            return App::make( $helper_reference );
        }
    }
}

if( !function_exists('translate') ){
    /**
     * Translates a Common Text string in the current language.
     *
     * @param $string_key
     * @return mixed
     */
    function translate( $string_key ){
        return Document::translate_common_text( $string_key );
    }
}

if( !function_exists('is_serialized') ){
    /**
     * Simply checks if the variable provided is a serialized array or not.
     *
     * @param $value
     * @param null $result
     * @return bool
     */
    function is_serialized($value, &$result = null)
    {
        // Bit of a give away this one
        if (!is_string($value) || empty($value))
        {
            return false;
        }
        // Serialized false, return true. unserialize() returns false on an
        // invalid string or it could return false if the string is serialized
        // false, eliminate that possibility.
        if ($value === 'b:0;')
        {
            $result = false;
            return true;
        }
        $length	= strlen($value);
        $end	= '';
        switch ($value[0])
        {
            case 's':
                if ($value[$length - 2] !== '"')
                {
                    return false;
                }
            case 'b':
            case 'i':
            case 'd':
                // This looks odd but it is quicker than isset()ing
                $end .= ';';
            case 'a':
            case 'O':
                $end .= '}';
                if ($value[1] !== ':')
                {
                    return false;
                }
                switch ($value[2])
                {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                        break;
                    default:
                        return false;
                }
            case 'N':
                $end .= ';';
                if ($value[$length - 1] !== $end[0])
                {
                    return false;
                }
                break;
            default:
                return false;
        }
        if (($result = @unserialize($value)) === false)
        {
            $result = null;
            return false;
        }
        return true;
    }
}

if( !function_exists( 'is_json' ) ){
    /**
     * Simple function checking if the provided variable is a JSON string.
     *
     * @param $string
     * @return bool
     */
    function is_json( $string ){
        if( !is_string( $string ) ){ return false; }

        json_decode( $string );
        return json_last_error() == JSON_ERROR_NONE;
    }
}

if( !function_exists( 'transform_key' ) ){
    /**
     * Transform key from array to dot syntax.
     *
     * @param  string $key
     * @param string $delimiter
     *
     * @return mixed
     */
    function transform_key($key, $delimiter = '.') {
        if( empty( $delimiter ) ){
            $delimiter = '.';
        }
        return str_replace([$delimiter, '[]', '[', ']'], ['_', '', $delimiter, ''], $key);
    }
}

if( !function_exists( 'auto_version' ) ){
    /**
     * When provided with a file path, returns the path with a timestamp of it's last updated time appended.
     * Useful for CSS & JS paths on the front end as a cache buster.
     *
     * @param $file_path
     * @return string
     */
    function auto_version( $file_path ){
        // Before checking the file version, ensure we have a file path rather than a URL
        $has_http = stristr( $file_path, route('theme') );
        $file_path = str_replace( route('theme'), '', $file_path );

        $file = public_path( $file_path );
        $suffix = '';
        if( is_file( $file ) ){
            $mod_time = filemtime( $file );

            if( $mod_time > 0 ){
                $suffix = '?' . $mod_time;
            }
        }

        // And if we were provided a URL rather than a path, return a URL
        if( $has_http ){
            $file_path = route('theme') . '/' . ltrim( $file_path, '/' );
        }

        return $file_path . $suffix;
    }
}

if( !function_exists( 'array_partition' ) ){
    /**
     * Splits an array into the number of sections provided.
     *
     * @param array $list
     * @param $p
     * @return array
     */
    function array_partition(Array $list, $p) {
        $listlen = count($list);
        $partlen = floor($listlen / $p);
        $partrem = $listlen % $p;
        $partition = array();
        $mark = 0;
        for($px = 0; $px < $p; $px ++) {
            $incr = ($px < $partrem) ? $partlen + 1 : $partlen;
            $partition[$px] = array_slice($list, $mark, $incr);
            $mark += $incr;
        }
        return $partition;
    }
}

if( !function_exists( 'image_ratio' ) ){
    /**
     * Quick helper function allowing us to return the ratio between width and height for an image.
     *
     * @param $width
     * @param $height
     * @return string
     */
    function image_ratio( $width, $height ){
        $greatest_common_denominator = gmp_gcd( $width, $height );
        return ($width / $greatest_common_denominator) . ':' . ($height / $greatest_common_denominator);
    }
}