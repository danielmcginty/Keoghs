<?php
namespace App\Helpers;

use App\Models\File;
use App\Models\Url;

/**
 * Class LinkHelper
 * @package App\Helpers
 *
 * The LinkHelper provides us with centralised functionality for parsing saved Link fields in the CMS
 */
class LinkHelper {
    /**
     * Based on the provided link_array, containing all details saved in the CMS, parse the values out and return
     * a more sensible array of url & target back.
     * Just keeps things centralised, rather than having to update link parsing left right and center.
     *
     * @param $link_array
     * @return array
     */
    public static function parseLink( $link_array ){
        $this_link = [];
        // If it's an internal link, find the corresponding Page URL
        if( isset($link_array['type']) && $link_array['type'] == "internal" ){
            $url = Url::find( $link_array['internal'] );
            if( $url ){
                $link_array['internal'] = route('theme', [$url->url]);
            } else {
                unset( $link_array['internal'] );
            }
        }

        // If it's a file link, find the corresponding File URL
        if( isset($link_array['type']) && $link_array['type'] == "file" ){
            $file = File::find( $link_array['file'] );
            if( $file ) {
                $link_array['file'] = route('file', [$file->path]);
            } else {
                unset( $link_array[ 'file' ] );
            }
        }

        // Get the URL, for ease of output
        $this_link['url'] = isset( $link_array[ $link_array['type'] ] ) ? $link_array[ $link_array['type'] ] : false;

        // If we're serving an external, or file, link, set it to open in a new tab
        $this_link['target'] = isset($link_array['type']) && in_array($link_array['type'], ['file', 'external']) ? '_blank' : false;

        return $this_link;
    }
}