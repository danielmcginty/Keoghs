<?php
namespace App\Helpers;

class BlockHelper {

    public static $spacing_options = [
        'my-0'     => 'No Spacing',
        'my-30'    => 'Small',
        'my-50'    => 'Medium',
        'my-75'    => 'Large'
    ];

    public static $text_alignment_options = [
        'text-left'     => 'Left',
        'text-center'   => 'Center',
        'text-right'    => 'Right'
    ];

    // REMOVE
    public static $screen_height_options = [
        'h-screen'   => 'Screen Height'
    ];

    public static $opacity_options = [
        'opacity-0'     => '0%',
        'opacity-10'    => '10%',
        'opacity-20'    => '20%',
        'opacity-30'    => '30%',
        'opacity-40'    => '40%',
        'opacity-50'    => '50%',
        'opacity-60'    => '60%',
        'opacity-70'    => '70%',
        'opacity-80'    => '80%',
        'opacity-90'    => '90%',
        'opacity-100'   => '100%',
    ];

    // REMOVE
    public static $darken_options = [
        'darken-0'     => '0%',
    ];

    // REMOVE
    public static $flex_content_alignment_options = [
        'e'     => 'Left',
        'c'     => 'Center',
        'w'     => 'Right',
    ];

    // REMOVE
    public static $hover_reveal_indicator_position_options = [
        'top-0 right-0'     => 'Top Right',
        'top-0 left-0'      => 'Top Left',
        'bottom-0 right-0'  => 'Bottom Right',
        'bottom-0 left-0'   => 'Bottom Left'
    ];

    // REMOVE
    public static $flex_justify_alignment_options = [
        'justify-center'    => 'Centered',
        'justify-start'     => 'Left',
        'justify-end'       => 'Right',
    ];

    // REMOVE
    public static $flex_item_alignment_options = [
        'items-center'      => 'Center',
        'items-start'       => 'Top',
        'items-end'         => 'Bottom',
    ];

    // REMOVE
    public static $flex_item_content_alignment_options = [
        'justify-center'    => 'Center',
        'justify-start'     => 'Top',
        'justify-end'       => 'Bottom',
    ];

    public static $block_heading_field_settings = [
        'display_name'      => 'Heading',
        'help'              => 'Set a heading to the block, and optionally change it\'s size',
        'type'              => 'grouped',
        'required'          => false,
        'validation'        => [],
        'group'             => 'Copy',
        'grouped_fields'    => [

            'text'  => [
                'label' => 'Heading',
                'type'  => 'string',
            ],

            // REMOVE
            'size'  => [
                'label'     => 'Size',
                'type'      => 'standard_select',
                'options'   => [
                    'h2' => 'H2',
                    'h3' => 'H3',
                    'h4' => 'H4',
                    'h5' => 'H5',
                ]
            ]

        ]
    ];

    public static function getBlockFieldHeading( $override_settings = array() ){
        $field = self::$block_heading_field_settings;

        if( !empty( $override_settings ) ){
            foreach( $override_settings as $key => $value ){
                $field[ $key ] = $value;
            }
        }

        return $field;
    }

    public static function parseBlockFieldHeading(){}

    public static function generateColumnSizeOptions( $breakpoint, $num_columns = 12 ){
        $classes = [];

        for( $i = 1; $i <= $num_columns; $i++ ){
            $class_name = $breakpoint . '-' . $i;
            $width = $i . ' / ' . $num_columns . ' width';

            $classes[ $class_name ] = $width;
        }

        return $classes;
    }

    public static function getFlexAlignmentClassesForOption( $alignment_option = 'e' ){
        $classes = 'items-center';

        switch( $alignment_option ){
            case 'ne':
                $classes = 'items-start';
                break;
            case 'n':
                $classes = 'items-start justify-center';
                break;
            case 'nw':
                $classes = 'items-start justify-end';
                break;
            case 'w':
                $classes = 'items-center justify-end';
                break;
            case 'sw':
                $classes = 'items-end justify-end';
                break;
            case 's':
                $classes = 'items-end justify-center';
                break;
            case 'se':
                $classes = 'items-end';
                break;
            case 'c':
                $classes = 'items-center justify-center';
                break;
        }

        return $classes;
    }

    public static function parseBlockButtonsField( $buttons ){
        $buttons = is_json( $buttons ) ? json_decode( $buttons, true ) : [];
        foreach( $buttons as $ind => $button ){
            if( empty( $button['link'] ) || empty( $button['text'] ) ){
                unset( $buttons[ $ind ] );
                continue;
            }

            if( is_json( $button['link'] ) ){
                $button['link'] = json_decode( $button['link'], true );
            }

            if( !is_array( $button['link'] ) ){
                unset( $buttons[ $ind ] );
                continue;
            }

            $button_link = LinkHelper::parseLink( $button['link'] );
            if( empty( $button_link['url'] ) ){
                unset( $buttons[ $ind ] );
                continue;
            }

            if (isset($button['colour'])) {
                $buttons[ $ind ]['colour'] = ColourHelper::getButtonColourClass( $button['colour'] );
            }

            $buttons[ $ind ]['link'] = $button_link;
        }

        return $buttons;
    }

    public static function blockSpacingClass( $background_colour, $block_spacing, $default = '' ){
        $type = 'margin';
        if( !empty( $background_colour ) ){ $type = 'padding'; }

        if( empty( $block_spacing ) ){ $block_spacing = $default; }

        // Remove first char from spacing class (most likely m for margin)
        $type_agnostic_spacing_class = substr( $block_spacing, 1 );

        $classes = [
            $background_colour,
        ];

        if( $type == 'padding' ){ $classes[] = 'p' . $type_agnostic_spacing_class; }
        else { $classes[] = 'm' . $type_agnostic_spacing_class; }

        $classes = array_filter( $classes );
        return implode( ' ', $classes );
    }

}