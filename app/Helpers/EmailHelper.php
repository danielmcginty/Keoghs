<?php
namespace App\Helpers;

class EmailHelper {

    private static $_STYLES = [
        '.ReadMsgBody'                  => 'width: 100%; background-color: #ffffff;',
        '.ExternalClass'                => 'width: 100%; background-color: #ffffff;',
        'body'                          => 'width: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased; font-family: arial, sans-serif;',
        'a'                             => 'color:#D8D8D8; text-decoration:none',
        'table'                         => 'border-collapse: collapse;',
        'body[yahoo] .desktop-hidden'   => 'max-height: 0px; overflow:hidden; max-height: 0px !important; overflow:hidden !important; display: none; display: none !important;',
        '@media only screen and (max-width: 640px)' => [
            'body[yahoo] .deviceWidth'  => 'width:600px!important; padding:0;',
            'body[yahoo] .center'       => 'text-align: center!important; display:block; margin-left:auto; margin-right:auto;',
            'body[yahoo] .desktop-hidden'   => 'max-height: 0px; overflow:hidden;  max-height: 0px !important; overflow:hidden !important; display: none; display: none !important;',
             'body[yahoo] .mobile-bg'       => 'background-color:#ffffff!important',
             'body[yahoo] .linkColor'       => 'color: #d8d8d8!important; text-decoration: none!important;'
        ],
        '@media only screen and (max-width: 479px)' => [
            'body[yahoo] .mobile-font'      => 'font-size: 11px!important;color:#777777!important;',
            'body[yahoo] .mobile-font2'     => 'font-size: 14px!important;color:#666666!important;',
            'body[yahoo] .mobile-font22'    => 'font-size: 22px!important;color:#4d4d4d!important;display:block!important;',
            'body[yahoo] .mobile-font20'    => 'font-size: 20px!important;color:#4d4d4d!important;display:block!important;',
            'body[yahoo] .mobile-font14'    => 'font-size: 14px!important;color:#4d4d4d!important;display:block!important;',
			'body[yahoo] .mobile-font10'    => 'font-size: 10px!important;color:#aaaaaa!important;display:block!important;',
            'body[yahoo] .mobile-font12'    => 'font-size: 12px!important;color:#4d4d4d!important;display:block!important;',
            'body[yahoo] .mobile-font16'    => 'font-size: 16px!important;color:#4d4d4d!important;display:block!important;',
            'body[yahoo] .mobile-font24'    => 'font-size: 24px!important;color:#4d4d4d!important;display:block!important;',
            'body[yahoo] .mobile-font24c'   => 'font-size: 24px!important;color:#fff!important;display:block!important;',
            'body[yahoo] .mobile-font22c'   => 'font-size: 22px!important;color:#ffffff!important;display:block!important;',
            'body[yahoo] .mobile-font14c'   => 'font-size: 14px!important;color:#ffffff!important;display:block!important;',
            'body[yahoo] .mobile-font16c'   => 'font-size: 16px!important;color:#ffffff!important;display:block!important;',
            'body[yahoo] .mobile-font12c'   => 'font-size: 12px!important;color:#ffffff!important;display:block!important;',
            'body[yahoo] .mobile-link'      => 'color:#f47d31!important;',
            'body[yahoo] .mobile-bg'        => 'background-color:#ffffff!important',
            'body[yahoo] .center'           => 'text-align: center!important;',
            'body[yahoo] .deviceWidth'      => 'width:320px!important; padding:0;',
            'body[yahoo] .mobile-hidden'    => 'display:none !important;',
            'body[yahoo] .desktop-hidden'   => 'max-height: none; overflow:visible;  max-height: none !important; overflow:visible!important; display: block; display: block !important;',
            'body[yahoo] .linkColor'        => 'color: #d8d8d8!important;text-decoration: none!important;'
        ],

        /***** SMARTPHONE STYLING RULES *****/
        '@media only screen and (max-width: 600px)' => [
            'body[yahoo] .mobile-font'      => 'font-size: 11px!important;color:#777777!important;',
            'body[yahoo] .mobile-font2'     => 'font-size: 14px!important;color:#666666!important;',
            'body[yahoo] .mobile-font22'    => 'font-size: 22px!important;color:#4d4d4d!important;display:block!important;',
            'body[yahoo] .mobile-font20'    => 'font-size: 20px!important;color:#4d4d4d!important;display:block!important;',
            'body[yahoo] .mobile-font14'    => 'font-size: 14px!important;color:#4d4d4d!important;display:block!important;',
            'body[yahoo] .mobile-font12'    => 'font-size: 12px!important;color:#4d4d4d!important;display:block!important;',
            'body[yahoo] .mobile-font16'    => 'font-size: 16px!important;color:#4d4d4d!important;display:block!important;',
            'body[yahoo] .mobile-font24'    => 'font-size: 24px!important;color:#4d4d4d!important;display:block!important;',
            'body[yahoo] .mobile-font24c'   => 'font-size: 24px!important;color:#fff!important;display:block!important;',
            'body[yahoo] .mobile-font22c'   => 'font-size: 22px!important;color:#ffffff!important;display:block!important;',
            'body[yahoo] .mobile-font14c'   => 'font-size: 14px!important;color:#ffffff!important;display:block!important;',
            'body[yahoo] .mobile-font16c'   => 'font-size: 16px!important;color:#ffffff!important;display:block!important;',
            'body[yahoo] .mobile-font12c'   => 'font-size: 12px!important;color:#ffffff!important;display:block!important;',
            'body[yahoo] .mobile-link'      => 'color:#f47d31!important;',
            'body[yahoo] .mobile-bg'        => 'background-color:#ffffff!important',
            'body[yahoo] .center'           => 'text-align: center!important;',
            'body[yahoo] .deviceWidth'      => 'width:320px!important; padding:0;',
            'body[yahoo] .mobile-hidden'    => ' display:none !important; ',
            'body[yahoo] .desktop-hidden'   => ' max-height: none; overflow:visible;  max-height: none !important; overflow:visible!important; display: block; display: block !important;',
            'body[yahoo] .linkColor'        => ' color: #d8d8d8!important;text-decoration: none!important;'
        ]
    ];

    public static $_CLASSES  = [
        /** WRAPPERS */
        'td.wrapper'                => 'padding-top: 0px; margin: 0; border: 0; vertical-align: top;',
        'td.min-width'              => 'min-width: 600px;',
        'td.logo-wrapper'           => 'font-size: 0; background: #ffffff;',

        /** LOGO */
        'img.logo'                  => 'display: block; max-width: 600px; width: 600px; margin: 0 auto;',

        /** MOBILE HEADER */
        'div.mobile-header'         => 'max-height: 0px; overflow: hidden; display: none;',
        'table.mobile-table'        => 'max-height: 0px; overflow: hidden; display: none; margin-left: auto; margin-right: auto; width: 0px;',
        'td.mobile-deviceWidth'     => 'font-size: 0;',
        'td.mobile-logo-wrap'       => 'font-size: 0; padding: 20px 0; margin-top: 20px;',
        'img.mobile-logo'           => 'max-width: 170px; width: 170px; display: block; margin: 0 auto;',

        /** TOP */
        'td.top-part'               => 'padding-top: 0px; margin: 0; border: 0; vertical-align: top',

        /** CONTENTS */
        /** DESKTOP CONTENTS */
        'td.desktop-contents'       => 'font-size: 0; background: #fffff; width: 552px; padding: 24px 24px 32px;',
        'p.desktop-para'            => 'line-height: 28px; font-size: 16px; letter-spacing: .8px; color: #065f5d;',
        /** MOBILE CONTENTS */
        'div.mobile-contents'       => 'max-height: 0px; overflow-hidden; display: none;',
        'span.mobile-para'          => 'line-height: 28px; margin-left: 16px; margin-top: 0px; margin-right: 16px; margin-bottom: 0px; padding: 32px; font-size: 0px; color: #4d4d4d; background: #ffffff;',

        /** FOOTER */
        'td.footer'                 => 'font-size: 0; padding: 20px 0;',
        'td.footer-line-wrap'       => 'font-size: 0; padding: 20px 0;',
        'td.footer-line'            => 'font-size: 0;',
        'td.social-wrapper'         => 'font-size: 0; line-height: 0; text-align: center',

        'div.footer-text'           => 'color: #aaaaaa; font-size: 10px; line-height: 12px; text-align: center; width: 360px;',
        'div.mobile-footer-text'    => 'color: #aaaaaa; font-size: 10px; line-height: 12px; text-align: center; padding: 0 16px;',
        'a.link-footer'             => 'color: #aaaaaa; text-decoration: underline;',
        'span.footer-copy'          => 'color: #aaaaaa; text-decoration: none;',
        'table.footer-spacer'       => 'font-size: 0pt; line-height: 0pt; text-align: center; width: 100%; min-width: 100%;',

        /** BUTTONS */
        'button.primary'            => 'display: block; display: inline-block; max-width: 100%; min-height: 20px; padding: 10px 30px; background-color: #3869D4; border-radius: 20px; color: #ffffff; font-size: 15px; line-height: 25px; text-align: center; text-decoration: none; -webkit-text-size-adjust: none;',

        /** HEADINGS */
        'h1'                        => 'margin-top: 0; color: #2F3133; font-size: 19px; font-weight: bold;'
    ];

    public static $_FONT    = 'Arial Regular, sans-serif';

    public static function outputStyles(){
        $styles = '<style type="text/css">' . PHP_EOL;

        foreach( self::$_STYLES as $directive => $details ){
            if( is_array( $details ) ){
                $styles .= $directive . ' { ' . PHP_EOL;
                foreach( $details as $subdirective => $subdirective ){
                    $styles .= $subdirective . ' { ' . $subdirective . ' }' . PHP_EOL;
                }
                $styles .= ' }' . PHP_EOL;
            } else {
                $styles .= $directive . ' { ' . $details . ' }' . PHP_EOL;
            }
        }

        $styles .= '</style>';

        return $styles;
    }

    public static function getAddress(){
        return array_filter([
            //translate( 'address_company_name' ),
            translate( 'address_line_one' ),
            translate( 'address_line_two' ),
            translate( 'address_city' ),
            translate( 'address_county' ),
            translate( 'address_postcode' ),
            //translate( 'address_country' )
        ]);
    }

    public static function getSocial(){
        return [
            'facebook'  => translate( 'social_facebook' ),
            'twitter'   => translate( 'social_twitter' ),
            'youtube'   => translate( 'social_youtube' )
        ];
    }
}