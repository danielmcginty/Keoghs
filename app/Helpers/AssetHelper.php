<?php
namespace App\Helpers;

use App\Models\AssetGroup;
use App\Models\File;

/**
 * Class AssetHelper
 * @package App\Helpers
 *
 * The Asset Helper provides us with centralised functionality for processing images saved as 'assets' in the CMS.
 */
class AssetHelper {

    /**
     * Array containing details for the default, fallback asset defined in general settings.
     *
     * @var array
     */
    protected static $default_asset = [];

    private static function setDefaultAsset(){
        self::$default_asset = AssetGroup::where([
            'table_name' => 'defaults',
            'table_key' => 1,
            'version' => 0,
            'language_id' => 1,
            'field' => 'image'
        ])->with( 'file' )->first();
    }

    /**
     * Simply return a 'raw' AssetGroup object for the current object.
     *
     * @param $table
     * @param $key
     * @param $language_id
     * @param $version
     * @param $field
     * @param $size
     * @return mixed
     */
    public static function getRawAsset( $table, $key, $language_id, $version, $field, $size ){
        $asset = AssetGroup::where([
            'table_name' => $table,
            'table_key' => $key,
            'language_id' => $language_id,
            'version' => $version,
            'field' => $field,
            'image_size' => $size
        ])->with('file')->first();

        return $asset;
    }

    /**
     * Return an array of assets for the current object, in an easy to use format
     *
     * @param $table
     * @param $key
     * @param $language_id
     * @param $version
     * @param $field
     * @param $config
     * @return array
     */
    public static function getAssets( $table, $key, $language_id, $version, $field, $config ){
        self::setDefaultAsset();

        $assets = AssetGroup::where([
            'table_name' => $table,
            'table_key' => $key,
            'language_id' => $language_id,
            'version' => $version,
            'field' => $field
        ])->with('file')->get();

        $primary_asset = [];

        $images = [];
        $raw_assets = [];
        foreach( $assets as $asset ){

            $size_config = isset( $config[ $asset->image_size ] ) ? $config[ $asset->image_size ] : [];
            if( empty( $size_config ) ){
                continue;
            }

            if( isset($size_config['primary']) && $size_config['primary'] ){
                $primary_asset = $asset;
            }

            $this_asset = self::_generateAssetArray( $size_config, $asset );
            if( !empty( $this_asset ) ) {
                $images[$asset->image_size] = $this_asset;
                $raw_assets[$asset->image_size] = $asset;
            }
        }

        // Before processing fallback images, ensure that any images set to use the default are assigned, so that
        // they fall back correctly
        foreach( $config as $size => $details ){
            if( !isset( $images[ $size ] ) && !empty( self::$default_asset ) && isset( $details['use_default'] ) && $details['use_default'] ){

                // Ensure that if the primary asset isn't set, and is set to use the default fallback, that the
                // $primary_asset variable is set to this, to ensure it tiers down through to other sizes
                if( isset( $details['primary'] ) && $details['primary'] ){
                    $primary_asset = self::$default_asset;
                }

                $default_asset = self::_generateAssetArray( $details, self::$default_asset );
                if( !empty( $default_asset ) ){
                    $images[ $size ] = $default_asset;
                }
            }
        }

        foreach( $config as $size => $details ){
            if( !isset( $images[ $size ] ) && (!isset($details['no_fallback']) || !$details['no_fallback']) ){
                $this_asset = self::_generateAssetArray( $details, $primary_asset );
                if( !empty( $this_asset ) ) {
                    $images[$size] = $this_asset;

                    if( !isset( $raw_assets[ $size ] ) ){
                        $raw_assets[ $size ] = $primary_asset;
                    }
                }
            }
        }

        // Now that we've processed the assets and generated fallbacks, we can run over the 'fallback_to' elements
        foreach( $config as $size => $details ){
            if( !isset( $images[ $size ] ) && isset( $details['fallback_to'] ) && $details['fallback_to'] ){
                $fallback_size = $details['fallback_to'];
                if( isset( $raw_assets[ $fallback_size ] ) ){
                    $this_asset = self::_generateAssetArray( $details, $raw_assets[ $fallback_size ] );
                    if( !empty( $this_asset ) ){
                        $images[$size] = $this_asset;
                    }
                }
            }
        }

        return $images;
    }

    /**
     * For the provided details and asset, generate an array as follows:
     *
     * [
     *      'alt' => Alt Text for the asset
     *      'src' => The URL path for the image
     *      'breakpoint_class' => Any CSS breakpoint classes for the image size, e.g. "none block-small-only"
     *      'width' => The generated width of the asset, as an integer
     *      'height' => The generated height of the asset, as an integer
     *      'ratio' => The aspect ration for the asset, as a percentage of Height / Width
     * ]
     *
     * @param $size_config
     * @param $asset
     * @return array
     */
    private static function _generateAssetArray( $size_config, $asset ){
        if( !$asset || !is_object($asset) || !is_object( $asset->file ) ){ return []; }

        $dimensions = $size_config['width'] . 'x' . $size_config['height'];
        $breakpoint_class = isset( $size_config['breakpoint_class'] ) ? $size_config['breakpoint_class'] : '';

        $file_path = $asset->file->path;

        $image_route = 'image';
        $image_route_params = ['path' => $file_path];

        if( $dimensions != '0x0' ){
            $image_route = 'image.resize';
            $image_route_params['dimensions'] = $dimensions;

            $crop = AssetGroup::generateAssetCrop( $asset );
            if( !empty( $crop ) ){
                $image_route = 'image.crop';
                $image_route_params['crop'] = $crop;
            }
        }

        // If we've got a specified height
        if( $size_config['height'] != 0 ){
            $height = $size_config['height'];
        }
        // Else if height is automatic based on width
        elseif( $size_config['width'] != 0 ){
            $ratio = $size_config['width'] / $asset->file->width;
            $height = ceil($asset->file->height * $ratio);
        }
        // Else we have no dimensions
        else {
            $height = $asset->file->height;
        }

        // If we've got a specified width
        if( $size_config['width'] != 0 ){
            $width = $size_config['width'];
        }
        // Else if width is automatic based on height
        elseif( $size_config['height'] != 0 ){
            $ratio = $size_config['height'] / $asset->file->height;
            $width = ceil($asset->file->width * $ratio);
        }
        // Else we have no dimensions
        else {
            $width = $asset->file->width;
        }

        $return_ratio = (($height / $width) * 100) . '%';

        return [
            'file_id'           => $asset->file->id,
            'alt'               => $asset->alt,
            'src'               => route( $image_route, $image_route_params ),
            'breakpoint_class'  => $breakpoint_class,
            'width'             => $width,
            'height'            => $height,
            'ratio'             => $return_ratio
        ];
    }

}