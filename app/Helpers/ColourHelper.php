<?php
namespace App\Helpers;
use App\Library\ThemeMapper;

/**
 * Class ColourHelper
 *
 * Common Helper Class designed to streamline colour selection within the CMS.
 *
 * It's a centralised repository of all colours available for the website, and
 * contains default Tailwind CSS colours as standard.
 *
 * Colours can be updated, added and removed by using a theme-map.json file
 * within the theme directory (e.g. /resources/theme/default/theme-map.json).
 *
 * If a top-level key of 'colours', 'button_colours', or 'background_colours'
 * exists in that file, it will completely overwrite the default data.
 *
 * If the theme-map.json file doesn't exist for the theme in use, or if a
 * top-level key doesn't exist within it, then the helper will retain it's
 * default classes.
 *
 * --------------------- PLEASE NOTE ------------------------------------------
 * If you change the 'key' of a colour (the array key), website colours will
 * stop mapping up correctly, as this is the value stored in the database.
 * If you need to change a colour key, please proceed with caution!
 * ----------------------------------------------------------------------------
 *
 * @package App\Helpers
 */
class ColourHelper {

    /**
     * Boolean determining if the theme map file has already been read, to
     * prevent needless overprocessing
     *
     * @var bool
     */
    private static $_MAP_READ                   = false;

    /**
     * Array containing default colour labels
     *
     * @var array
     */
    private static $_COLOURS                    = [

        'primary'       => 'Primary',
        'secondary'     => 'Secondary',

        'white'         => 'White',
        'off-white'     => 'Off White',
        'lightest-grey' => 'Lightest Gray',
        'lighter-grey'  => 'Lighter Gray',
        'light-grey'    => 'Light Gray',
        'grey'          => 'Gray',
        'dark-grey'     => 'Dark Gray',
        'darker-grey'   => 'Darker Gray',
        'darkest-grey'  => 'Darkest Gray',
        'off-black'     => 'Off Black',
        'black'         => 'Black',

    ];

    /**
     * Array containing default colour classes
     *
     * @var array
     */
    private static $_COLOUR_CLASSES             = [

        'primary'       => 'primary',
        'secondary'     => 'secondary',

        'white'         => 'white',
        'off-white'     => 'gray-100',
        'lightest-grey' => 'gray-200',
        'lighter-grey'  => 'gray-300',
        'light-grey'    => 'gray-400',
        'grey'          => 'gray-500',
        'dark-grey'     => 'gray-600',
        'darker-grey'   => 'gray-700',
        'darkest-grey'  => 'gray-800',
        'off-black'     => 'gray-900',
        'black'         => 'black'
    ];

    /**
     * Array containing default button colours labels
     *
     * @var array
     */
    private static $_BUTTON_COLOURS             = [

        'primary'       => 'Primary',
        'secondary'     => 'Secondary',

        'white'         => 'White',
        'white-border'  => 'White (Border)',

        'black'         => 'Black',
        'black-border'  => 'Black (Border)',

    ];

    /**
     * Array containing default button colour classes
     *
     * @var array
     */
    private static $_BUTTON_COLOUR_CLASSES      = [
        'primary'       => 'bg-primary text-white',
        'secondary'     => 'bg-secondary text-white',

        'white'         => 'bg-white text-black',
        'white-border'  => 'border-2 border-white text-white',

        'black'         => 'bg-black text-white',
        'black-border'  => 'border-2 border-black text-black',
    ];

    /**
     * Array containing default background colour labels
     *
     * @var array
     */
    private static $_BACKGROUND_COLOURS         = [

        'primary'       => 'Primary',
        'secondary'     => 'Secondary',

        'white'         => 'White',
        'off-white'     => 'Off White',
        'lightest-grey' => 'Lightest Gray',
        'lighter-grey'  => 'Lighter Gray',
        'light-grey'    => 'Light Gray',
        'grey'          => 'Gray',
        'dark-grey'     => 'Dark Gray',
        'darker-grey'   => 'Darker Gray',
        'darkest-grey'  => 'Darkest Gray',
        'off-black'     => 'Off Black',
        'black'         => 'Black',

    ];

    /**
     * Array containing default background colour classes
     *
     * @var array
     */
    private static $_BACKGROUND_COLOUR_CLASSES  = [
        'primary'       => 'bg-primary text-white',
        'secondary'     => 'bg-secondary text-white',

        'white'         => 'bg-white text-gray-700',
        'off-white'     => 'bg-gray-100 text-gray-700',
        'lightest-grey' => 'bg-gray-200 text-gray-700',
        'lighter-grey'  => 'bg-gray-300 text-gray-700',
        'light-grey'    => 'bg-gray-400 text-gray-700',
        'grey'          => 'bg-gray-500 text-gray-700',
        'dark-grey'     => 'bg-gray-600 text-white',
        'darker-grey'   => 'bg-gray-700 text-white',
        'darkest-grey'  => 'bg-gray-800 text-white',
        'off-black'     => 'bg-gray-900 text-white',
        'black'         => 'bg-black text-white',
    ];

    /**
     * Simple access function to the main colours array
     *
     * @return array
     */
    public static function getColours(){
        self::_getColourMappings();
        return self::$_COLOURS;
    }

    /**
     * Returns a colour class based on reference key
     *
     * @param string $key
     * @return string
     */
    public static function getColourClass( $key ){
        self::_getColourMappings();
        return isset( self::$_COLOUR_CLASSES[ $key ] ) ? self::$_COLOUR_CLASSES[ $key ] : '';
    }

    /**
     * Simple access function to the main button colours array
     *
     * @return array
     */
    public static function getButtonColours(){
        self::_getColourMappings();
        return self::$_BUTTON_COLOURS;
    }

    /**
     * Returns a button colour class based on reference key
     *
     * @param string $key
     * @return string
     */
    public static function getButtonColourClass( $key ){
        self::_getColourMappings();
        return isset( self::$_BUTTON_COLOUR_CLASSES[ $key ] ) ? self::$_BUTTON_COLOUR_CLASSES[ $key ] : '';
    }

    /**
     * Simple access function to the main background colours array
     *
     * @return array
     */
    public static function getBackgroundColours(){
        self::_getColourMappings();
        return self::$_BACKGROUND_COLOURS;
    }

    /**
     * Returns a background colour class based on reference key
     *
     * @param string $key
     * @return string
     */
    public static function getBackgroundColourClass( $key ){
        self::_getColourMappings();
        return isset( self::$_BACKGROUND_COLOUR_CLASSES[ $key ] ) ? self::$_BACKGROUND_COLOUR_CLASSES[ $key ] : '';
    }

    /**
     * Get Colour Mappings
     * Reads in theme mapping settings from the theme-map.json file, and
     * updates the class' colour references, labels and classes
     */
    private static function _getColourMappings(){
        if( self::$_MAP_READ ){ return; }

        $colours = ThemeMapper::getMappedSettings( "colours" );
        if( !empty( $colours ) ){
            self::$_COLOURS = [];

            foreach( $colours as $key => $details ){
                self::$_COLOURS[ $key ] = $details['label'];
                self::$_COLOUR_CLASSES[ $key ] = $details['class'];
            }
        }

        $button_colours = ThemeMapper::getMappedSettings( "button_colours" );
        if( !empty( $button_colours ) ){
            self::$_BUTTON_COLOURS = [];

            foreach( $button_colours as $key => $details ){
                self::$_BUTTON_COLOURS[ $key ] = $details['label'];
                self::$_BUTTON_COLOUR_CLASSES[ $key ] = $details['class'];
            }
        }

        $background_colours = ThemeMapper::getMappedSettings( "background_colours" );
        if( !empty( $background_colours ) ){
            self::$_BACKGROUND_COLOURS = [];

            foreach( $background_colours as $key => $details ){
                self::$_BACKGROUND_COLOURS[ $key ] = $details['label'];
                self::$_BACKGROUND_COLOUR_CLASSES[ $key ] = $details['class'];
            }
        }

        self::$_MAP_READ = true;
    }
}