<?php
namespace App\Models\Mailable;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CommonMailable extends Mailable {
    use Queueable, SerializesModels;

    /**
     * Create a new message instance, setting the current theme view path.
     */
    public function __construct(){
        $theme = config('app.theme_name', 'default');

        // Get the current 'view finder' to use after we've reset this to include previously namespaced view paths
        $currentViewFinder = \View::getFinder();

        // Now, register the laravel 'view finder' to read from our theme directory
        $themeFinder = new \Illuminate\View\FileViewFinder(app()['files'], array(realpath(base_path('resources/theme/' . $theme . '/views'))));
        \View::setFinder($themeFinder);

        // And after that, re-register the previous namespaced view paths from the previous view finder
        foreach( $currentViewFinder->getHints() as $namespace => $paths ){
            view()->addNamespace( $namespace, $paths );
        }
    }
}