<?php
namespace App\Models;

use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class File extends Model {
    // As we've got a deleted_at flag, and want to cache DB calls, include the relevant traits
    use SoftDeletes, HasQueryBuilderCache;

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'files';

    /**
     * The fields that are 'bulk assignable' for this model
     * @var array
     */
    protected $fillable = ['storage_path', 'file_name', 'name', 'extension', 'created_by'];

    /**
     * Any date fields for this model
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Any fields to be appended to the object should it be output as an array or JSON object.
     * @var array
     */
    protected $appends = ['api_id', 'is_image', 'url', 'filesize', 'dimensions', 'width', 'height', 'date_added', 'path', 'directory_path', 'usages', 'croppable'];

    /**
     * Internal array containing file extensions to treat as images.
     *
     * @var array
     */
    private $image_extensions = [ 'jpg', 'jpeg', 'png', 'gif', 'bmp', 'svg' ];

    /**
     * Internal array containing file extensions that the Image Manipulator can manage and crop.
     *
     * @var array
     */
    private $croppable_extensions = [ 'jpg', 'jpeg', 'png', 'bmp' ];

    public function getApiIdAttribute(){
        return $this->id;
    }

    /**
     * Eloquent Relationship.
     * Returns the directory that this file lives in.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function directory(){
        return $this->belongsTo( 'App\\Models\\FileDirectory', 'directory_id' );
    }

    /**
     * Eloquent Relationship.
     * Returns instances of any locations this file is assigned to (i.e. pages, news, etc)
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function usage(){
        return $this->hasMany( 'Adm\\Models\\FileUsage', 'file_id' );
    }

    /**
     * Eloquent Relationship.
     * Returns the user that uploaded the file.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function added_by(){
        return $this->belongsTo( 'Adm\\Models\\AdmUser', 'created_by' );
    }

    /**
     * Query Scope allowing us to return only files in the 'root' directory.
     *
     * @param $query
     * @return mixed
     */
    public function scopeRootFiles( $query ){
        return $query->where('directory_id', '=', 0);
    }

    /**
     * Query Scope allowing us to return only files that are images (based on the $image_extensions array)
     *
     * @param $query
     * @return mixed
     */
    public function scopeImagesOnly( $query ){
        return $query->whereIn('extension', $this->image_extensions);
    }

    /**
     * Simple function formatting the number of bytes the image is into something readable with Mb, Gb, etc.
     *
     * @param $size
     * @param int $precision
     * @return string
     */
    public static function formatBytes( $size, $precision = 2 ){
        $base = log($size, 1024);
        $suffixes = array('b', 'kb', 'mb', 'gb', 'tb');

        return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
    }

    /**
     * Simple Accessor Function returning the number of bytes the file is.
     *
     * @return string
     */
    public function getFilesizeAttribute(){
        return File::formatBytes( Storage::size( $this->storage_path ) );
    }

    /**
     * Simple Accessor Function returning the image dimensions as width x height string.
     *
     * @return string
     */
    public function getDimensionsAttribute(){
        return $this->width . 'x' . $this->height . 'px';
    }

    /**
     * Simple Accessor Function returning the image width.
     *
     * @return mixed
     */
    public function getWidthAttribute(){
        $dimensions = getimagesize( storage_path( 'app/' . $this->storage_path ) );
        return $dimensions[0];
    }

    /**
     * Simple Accessor Function returning the image height.
     *
     * @return mixed
     */
    public function getHeightAttribute(){
        $dimensions = getimagesize( storage_path( 'app/' . $this->storage_path ) );
        return $dimensions[1];
    }

    /**
     * Simple Accessor Function returning the file upload date in a pretty format.
     *
     * @return mixed
     */
    public function getDateAddedAttribute(){
        return \Carbon::parse($this->created_at)->format("d/m/Y H:i");
    }

    /**
     * Simple Accessor Function returning the FULL file path to this file, including the directory tree.
     *
     * @return mixed|string
     */
    public function getPathAttribute(){
        $directory = $this->directory_path;
        if( !empty($directory) ){
            return $directory . '/' . $this->file_name;
        }

        return $this->file_name;
    }

    /**
     * Simple Accessor Function returning the FULL path to this file's directory, including the directory tree.
     *
     * @return string
     */
    public function getDirectoryPathAttribute(){
        $hierarchy = [];
        if( !empty($this->directory) ){
            FileDirectory::hierarchicalPath( $this->directory, $hierarchy );
            $hierarchy = array_reverse( $hierarchy );

            return implode("/", $hierarchy);
        }

        return '';
    }

    /**
     * Simple Accessor Function acting as a wrapper for the usage() relationship, returning the pages this file is
     * used on.
     *
     * @return array
     */
    public function getUsagesAttribute(){
        $usages = [];
        foreach( $this->usage as $usage ){
            $usages[] = \Adm\Models\FileUsage::getUsageItem( $usage );
        }

        return $usages;
    }

    /**
     * Simple Accessor Function allowing us to quickly determine if this file is an image.
     *
     * @return bool
     */
    public function getIsImageAttribute(){
        return in_array( strtolower($this->extension), $this->image_extensions );
    }

    /**
     * Simple Accessor Function allowing us to quickly determine if this file is manipulatable by the image library.
     *
     * @return bool
     */
    public function getCroppableAttribute(){
        return in_array( strtolower($this->extension), $this->croppable_extensions );
    }

    /**
     * Simple Accessor Function providing quick access to the Front-End URL for the file.
     *
     * @return string
     */
    public function getUrlAttribute(){
        $path = $this->path;

        if( $this->is_image ){
            return route('image', $path);
        } else {
            return route('file', $path);
        }
    }

    /**
     * Simple Accessor Function returning a fallback for this file's Alt Text.
     *
     * @param $value
     * @return mixed
     */
    public function getAltTextAttribute( $value ){
        return !empty( $value ) ? $value : $this->name;
    }

    /**
     * Simple Checker function looking for the closest directory path when loading the file over the front-end.
     *
     * @param $directory_paths
     * @return mixed
     */
    public static function getClosestDirectory( $directory_paths ){
        return self::_findClosestParentDirectory( $directory_paths );
    }

    /**
     * Recursively search through a directory tree for the last directory
     *
     * Because there can be multiple directories with the same name, e.g.
     *
     * "parent-directory/child-directory/grandchild-directory"
     *
     * we need to loop through each segment of the directory tree (left to right)
     * to make sure we get the exact directory we need.
     *
     * We would first find "parent-directory" with a parent id of 0
     * Then find "child-directory" where the parent id == "parent-directory"'s id
     * Then find "grandchild-directory" where the parent id == "child-directory"'s id
     * and so on...
     *
     * @param $directory_paths
     * @param int $parent
     * @return mixed
     */
    private static function _findClosestParentDirectory($directory_paths, $parent = 0) {

        $top_directory_path = $directory_paths[0];

        $current_directory = FileDirectory::where([
            'path' => $top_directory_path,
            'parent' => $parent,
        ])->first();

        if (count($directory_paths) > 1 && $current_directory) {
            // There's still more directories below this directory,
            // so keep searching using the current directory's id as the parent

            // Remove current (first) directory in the array as we now need to search it's children
            array_shift($directory_paths);

            return self::_findClosestParentDirectory($directory_paths, $current_directory->id);

        } else {
            // We've reached the last directory in the tree, so return it
            return $current_directory;
        }

    }
}