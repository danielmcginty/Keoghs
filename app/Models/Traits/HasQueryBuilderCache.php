<?php
namespace App\Models\Traits;

use App\Library\CachingQueryBuilder;

trait HasQueryBuilderCache {

    /**
     * If a model uses this trait, ensure that the CachingQueryBuilder is used instead of the 'base' Query Builder.
     *
     * @return CachingQueryBuilder
     */
    protected function newBaseQueryBuilder(){
        $conn = $this->getConnection();

        $grammar = $conn->getQueryGrammar();

        return new CachingQueryBuilder($conn, $grammar, $conn->getPostProcessor());
    }
}