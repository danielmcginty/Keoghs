<?php
namespace App\Models;

use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FileDirectory extends Model {
    // As we've got a deleted_at flag, and want to cache DB calls, include the relevant traits
    use SoftDeletes, HasQueryBuilderCache;

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'file_directories';

    /**
     * The fields that are 'bulk assignable' for this model
     * @var array
     */
    protected $fillable = ['parent', 'path', 'name'];

    /**
     * Any date fields for this model
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Any fields to be appended to the object should it be output as an array or JSON object.
     * @var array
     */
    protected $appends = ['depth', 'absolute_path'];

    /**
     * Eloquent Relationship.
     * Returns the parent directory for this directory.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function directory(){
        return $this->belongsTo( 'App\\Models\\FileDirectory', 'parent' )->with('directory');
    }

    /**
     * Eloquent Relationship.
     * Returns child directories of this directory.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children(){
        return $this->hasMany( 'App\\Models\\FileDirectory', 'parent' )->with('children')->orderBy('name');
    }

    /**
     * Eloquent Relationship.
     * Returns files within this directory.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files(){
        return $this->hasMany( 'App\\Models\\File', 'directory_id' );
    }

    /**
     * Simple Accessor Function returning the file upload date in a pretty format.
     *
     * @return mixed
     */
    public function getDateAddedAttribute(){
        return \Carbon::parse($this->created_at)->format("d/m/Y H:i");
    }

    /**
     * Simple Function allowing us to return the reversed path to this directory.
     *
     * @param $directory
     * @param $paths
     */
    public static function hierarchicalPath( $directory, &$paths ){
        if( isset($directory->path) && !empty($directory->path)){
            $paths[] = $directory->path;

            if( !empty($directory->parent) ) {
                self::hierarchicalPath( $directory->directory, $paths );
            }
        }
    }

    /**
     * Simple Function returning the 'depth' of this directory, i.e. how many parents it has.
     *
     * @return int
     */
    public function getDepthAttribute(){
        $depth = 0;
        $this->_getDepth($this, $depth);

        return $depth;
    }

    /**
     * Recursive Function generating the depth to return in getDepthAttribute().
     *
     * @param $directory
     * @param $depth
     */
    private function _getDepth( $directory, &$depth ){
        if( !empty( $directory->parent ) ){
            $depth++;

            $this->_getDepth( $directory->directory, $depth );
        }
    }

    /**
     * Simple Function returning the full path to this directory.
     *
     * @return string
     */
    public function getAbsolutePathAttribute(){
        $paths = [];
        self::hierarchicalPath( $this, $paths );
        return implode( "/", array_reverse( $paths ) );
    }

    /**
     * Simple Function used to check if files within the directory are being used when deleting a directory.
     *
     * @param $files_in_use
     */
    public function get_folder_usages( &$files_in_use ){
        foreach( $this->children as $child ){

            $files_used_in_child = [];

            // Now, for each child, we need to find the files
            $files = File::where('directory_id', '=', $child->id)->get();

            foreach( $files as $file ){
                if( !empty( $file->usages ) ){
                    $files_used_in_child[] = $child->name . '/<strong>' . $file->file_name . '</strong>';
                }
            }

            if ($files_used_in_child) {
//                $files_in_use[$child->id] = $files_used_in_child;
                $files_in_use = $files_in_use + $files_used_in_child;
            }

            $child->get_folder_usages( $files_in_use );
        }

        $files = File::where('directory_id', '=', $this->id)->get();
        $files_used_in_me = [];
        foreach( $files as $file ){
            if( !empty( $file->usages ) ){
                $files_used_in_me[] = $this->name . '/<strong>' . $file->file_name . '</strong>';
            }
        }
//        array_merge( $files_in_use, $files_used_in_me );
        $files_in_use = $files_in_use + $files_used_in_me;
    }

    /**
     * Custom Delete function allowing us to delete a directory, it's child directories, and the files within them.
     *
     * @throws \Exception
     */
    public function delete_folder(){
        foreach( $this->children as $child ){

            // Now, for each child, we need to find the files
            $files = File::where('directory_id', '=', $child->id)->get();

            foreach( $files as $file ){
                $file->delete();
            }

            $child->delete_folder();
        }

        $files = File::where('directory_id', '=', $this->id)->get();
        foreach( $files as $file ){
            $file->delete();
        }

        $this->delete();
    }
}