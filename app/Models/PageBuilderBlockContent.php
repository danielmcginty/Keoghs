<?php
namespace App\Models;

use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\Model;

class PageBuilderBlockContent extends Model {
    // We want to cache DB calls, so include the relevant trait
    use HasQueryBuilderCache;

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'page_builder_block_content';

    /**
     * The fields that are 'bulk assignable' for this model
     * @var array
     */
    protected $fillable = ['page_builder_block_id', 'field', 'value'];

    /**
     * We don't have timestamps on Page Builder Block Content, so define that
     * @var bool
     */
    public $timestamps = false;

    /**
     * Eloquent Relationship.
     * Returns the page builder block that this content belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function page_builder_block(){
        return $this->belongsTo( '\\App\\Models\\PageBuilderBlock', 'page_builder_block_id' );
    }

    /**
     * Custom Accessor Function.
     * When getting the page of a page builder block content record, we might need to process it in a more custom
     * manner. This function allows us to do that.
     *
     * It calls the particular Page Builder Block model, based on reference, and performs custom functionality
     * before returning the value back here for use in templates.
     *
     * @param $value
     * @return mixed
     */
    public function getValueAttribute( $value ){
        $BlockClass = $this->page_builder_block->reference_model_class;
        if( $BlockClass ){
            if( method_exists( $BlockClass, 'getValue' ) ){
                return $BlockClass->getValue( $this->page_builder_block, $this->field, $value );
            }
        }

        if( is_serialized( $value ) ){ $value = json_encode( unserialize( $value ) ); }
        return is_json( $value ) ? json_decode( $value, true ) : $value;
    }
}