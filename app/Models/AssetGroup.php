<?php
namespace App\Models;

use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\Model;

class AssetGroup extends Model {
    // As we want to cache DB calls, include the relevant trait
    use HasQueryBuilderCache;

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'asset_groups';

    /**
     * The fields that are 'bulk assignable' for this model
     * @var array
     */
    protected $fillable = ['table_name', 'table_key', 'language_id', 'field', 'image_size', 'file_id', 'alt', 'width', 'height', 'x_coordinate', 'y_coordinate'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Eloquent Relationship.
     * Returns the file assigned to this asset group entry.
     *
     * @return mixed
     */
    public function file(){
        return $this->belongsTo('\\App\\Models\\File', 'file_id')->with('directory');
    }

    /**
     * Centralised function used to pull out dimensions and crop details into a format that's usable within
     * the image route
     *
     * @param $asset
     * @return bool|string
     */
    public static function generateAssetCrop( $asset ){
        $crop = false;

        if( !is_null($asset->width) && !is_null($asset->height) ){
            $crop = $asset->width . 'x' . $asset->height;

            if( !is_null( $asset->x_coordinate ) && !is_null( $asset->y_coordinate ) ){
                $crop .= '-' . $asset->x_coordinate . 'x' . $asset->y_coordinate;
            }
        }

        return $crop;
    }
}