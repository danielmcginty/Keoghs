<?php
namespace App\Models;

use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\Model;

class CommonModel extends Model {
    // As we want to cache DB calls, include the relevant trait
    use HasQueryBuilderCache;

    /**
     * Internal variable storing the 'primary key' column that is NOT language ID.
     *
     * @var string
     */
    protected static $primaryColumn;

    /**
     * As we can pull out the "Main Content" section of a page, we need a way of overriding what's set against the
     * page's blocks.
     * So... We have an override!
     * If this variable is populated and not false, it contains a modified version of the page's blocks.
     *
     * @var bool
     */
    public $page_builder_override = false;

    /**
     * Simple a class variable allowing us to store the content from the "Main Content" page builder block, allowing
     * access in multiple places if required.
     *
     * @var bool
     */
    public $pulled_out_main_content = false;

    /**
     * Create a new model instance straight from the database.
     *
     * @param  array  $attributes
     * @param  string|null  $connection
     * @return static
     */
    public function newFromBuilder( $attributes = [], $connection = null ){
        $model = $this->newInstance([], true);

        $model->setRawAttributes( (array)$attributes, true );

        $model->setConnection( $connection ?: $this->getConnectionName() );

        $this->_setBaseId();
        $this->_setUrl( $model );

        return $model;
    }

    /**
     * Set the base 'ID' field for the model on load
     */
    private function _setBaseId(){
        if( is_array( $this->primaryKey ) ) {
            foreach ($this->primaryKey as $primary_col) {
                if ($primary_col != 'language_id' && $primary_col != 'version') {
                    self::$primaryColumn = $primary_col;
                }
            }
        } else {
            self::$primaryColumn = $this->primaryKey;
        }
    }

    /**
     * Set the URL for this page on load
     * @param $model
     */
    public function _setUrl( &$model ){
        $my_url = Url::where([
            'table_name' => $model->table,
            'table_key' => $model->{self::$primaryColumn}
        ])->first();

        $model->url_object = $my_url;
        $model->url = is_object($my_url) ? $my_url->url : false;
    }

    /**
     * Common accessor function allowing us to pull out the H1 title for a page, falling back to the regular title
     * if not provided.
     *
     * @param $value
     * @return mixed
     */
    public function getH1TitleAttribute( $value ){
        return $value ? $value : $this->title;
    }

    public function getMetaTitleAttribute(){
        return $this->url_object ? $this->url_object->meta_title : '';
    }

    public function getMetaDescriptionAttribute(){
        return $this->url_object ? $this->url_object->meta_description : '';
    }

    public function getNoIndexAttribute(){
        return $this->url_object && $this->url_object->no_index ? true : 0;
    }

    /**
     * Custom accessor function for Main Content.
     * Allows us to pull out the HTML content set against a page's "Main Content" page builder block.
     * When the ->main_content attribute is accessed, the set of page builder blocks is modified to remove the main
     * content, assuming that it has been output on the template.
     *
     * @return string|bool
     */
    public function getMainContentAttribute(){
        // First of all, check to see if we've already pulled it out
        if( $this->pulled_out_main_content !== false ){ return $this->pulled_out_main_content ; }

        // Before we can override the page builder blocks and remove the main content (as it's being pulled out)
        // We first need to set it to the page builder instance
        $this->page_builder_override = $this->page_builder;

        // Now, scan over all the blocks
        foreach( $this->page_builder_override as $index => $block ){
            // And if it's the main content block
            if( $block['reference'] == 'main_content' ){
                // Pull out the content
                $main_content = $block['content']['content'];
                // Set it against our class variable for re-use
                $this->pulled_out_main_content = $main_content;
                // And remove the block from the page builder
                unset( $this->page_builder_override[ $index ] );
                // And then return the content
                return $main_content;
            }
        }

        // If we've got this far, there is no main content, so return false
        return false;
    }

    /**
     * Custom accessor function for Page Builder Blocks.
     * Simply returns an array of all page builder blocks assigned to the current page.
     * If the set of blocks has been modified to remove "Main Content", the override set will be returned.
     *
     * @return array|bool
     */
    public function getPageBuilderAttribute(){
        if( $this->page_builder_override !== false ){
            return $this->page_builder_override;
        }

        $pk_field = $this->primaryKey;
        if( is_array( $this->primaryKey ) ) {
            foreach ($this->primaryKey as $field) {
                if ($field != "language_id") {
                    $pk_field = $field;
                }
            }
        }

        $blocks = PageBuilderBlock::where([
            'table_name' => $this->table,
            'table_key' => $this->{$pk_field},
            'language_id' => $this->language_id,
            'version' => $this->version,
            'status' => 1
        ])->get();

        $page_builder_arr = [];
        foreach( $blocks as $block ){
            $page_builder_arr[ $block->sort_order ] = [
                'page_builder_block_id' => $block->id,
                'reference' => $block->reference,
                'content' => $block->content
            ];
        }

        return $page_builder_arr;
    }
}