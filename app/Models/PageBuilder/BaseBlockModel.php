<?php
namespace App\Models\PageBuilder;

use App\Helpers\AssetHelper;
use App\Helpers\ColourHelper;

abstract class BaseBlockModel {

    public function getValue( $page_builder_block, $field, $original_value ){
        if( is_serialized( $original_value ) ){ $original_value = json_encode( unserialize( $original_value ) ); }
        return is_json( $original_value ) ? json_decode( $original_value, true ) : $original_value;
    }

    public function getAssetFieldValue( $page_builder_block, $field, $original_value, $size_config ){
        return AssetHelper::getAssets(
            $page_builder_block->table_name,
            $page_builder_block->table_key,
            $page_builder_block->language_id,
            $page_builder_block->version,
            'page_builder.' . $page_builder_block->sort_order . '.content.' . $field,
            $size_config
        );
    }

    public function mapColourClass( $key, $colour_type ){
        switch( $colour_type ){
            case 'colour':
                return ColourHelper::getColourClass( $key );
            case 'button':
                return ColourHelper::getButtonColourClass( $key );
            case 'background':
                return ColourHelper::getBackgroundColourClass( $key );
        }

        return $key;
    }

}