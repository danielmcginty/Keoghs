<?php
namespace App\Models\PageBuilder;

use App\Helpers\BlockHelper;

class MainContent extends BaseBlockModel {

    public function getValue( $page_builder_block, $field, $original_value ){
        switch( $field ){
            case 'buttons':
                return BlockHelper::parseBlockButtonsField( $original_value );
            default:
                return parent::getValue( $page_builder_block, $field, $original_value );
        }
    }

}