<?php
namespace App\Models;

use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\SoftDeletes;

class Redirect extends CommonModel {
    // As we've got a deleted_at flag, and want to cache DB calls, include the relevant traits
    use SoftDeletes, HasQueryBuilderCache;

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = '301_redirects';

    /**
     * The fields that are 'bulk assignable' for this model
     * @var array
     */
    protected $fillable = ['old_url', 'new_url'];

    /**
     * We have created_at and updated_at timestamps
     * @var bool
     */
    public $timestamps = true;

}