<?php
namespace App\Models;

use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PageBuilderBlock extends Model {
    // As we've got a deleted_at flag, and want to cache DB calls, include the relevant traits
    use SoftDeletes, HasQueryBuilderCache;

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'page_builder_blocks';

    /**
     * The fields that are 'bulk assignable' for this model
     * @var array
     */
    protected $fillable = ['content', 'sort_order'];

    /**
     * We have created_at and updated_at timestamps
     * @var bool
     */
    public $timestamps = true;

    protected $appends = ['content', 'api_content'];

    /**
     * Custom Accessor Function.
     * When getting 'content' for the block, return an array of fields assigned to the block content record.
     *
     * @return array
     */
    public function getContentAttribute(){
        $raw_fields = PageBuilderBlockContent::where([
            'page_builder_block_id' => $this->id
        ])->get();

        $field_arr = [];
        foreach( $raw_fields as $field ){
            $field_arr[ $field->field ] = $field->value;
        }

        $BlockModel = $this->reference_model_class;
        if( $BlockModel ){
            if( method_exists( $BlockModel, 'postProcess' ) ){
                $field_arr = $BlockModel->postProcess( $field_arr );
            }
        }

        return $field_arr;
    }

    /**
     * Custom Accessor Function.
     * Allows us to pull out the model name for this page builder block in a central location.
     *
     * @return \StdClass|bool
     */
    public function getReferenceModelClassAttribute(){
        $reference_class = ucfirst( \Illuminate\Support\Str::camel( $this->reference ) );
        $available_modules = (array)config('modules.available');

        $default_class = '\\App\\Models\\PageBuilder\\' . $reference_class;
        if( class_exists( $default_class ) ){
            return new $default_class;
        }

        $modules_dir = base_path( 'modules' );
        $modules = array_filter( glob( $modules_dir . '/*', GLOB_ONLYDIR ) );
        foreach( $modules as $module_path ){
            $path_parts = explode( '/', $module_path );
            $module_ns = end( $path_parts );

            if( !in_array( $module_ns, $available_modules ) ){
                continue;
            }

            $modular_class = '\\Modules\\' . $module_ns . '\\Theme\\Models\\PageBuilder\\' . $reference_class;
            if( class_exists( $modular_class ) ){
                return new $modular_class;
            }
        }

        return false;
    }

    public function getApiContentAttribute(){
        $raw_fields = PageBuilderBlockContent::where([
            'page_builder_block_id' => $this->id
        ])->get();

        $field_arr = [];
        $base_field_arr = [];
        foreach( $raw_fields as $field ){
            $base_field_arr[ $field->field ] = $field->value;
            $field_arr[ $field->field ] = $this->_recursiveJsonDecode( $field->value );
        }

        $BlockModel = $this->reference_model_class;
        if( $BlockModel ){
            if( method_exists( $BlockModel, 'postProcess' ) ){
                $field_arr = $BlockModel->postProcess( $field_arr );
            }
        }

        return json_encode( $field_arr );
    }

    protected function _recursiveJsonDecode( $value ){
        if( is_array( $value ) ){
            foreach( $value as $ind => $sub_value ){
                $value[ $ind ] = $this->_recursiveJsonDecode( $sub_value );
            }
            return $value;
        }

        if( !is_json( $value ) ){ return $value; }

        $value = json_decode( $value, true );
        if( !is_array( $value ) ){ return $value; }

        foreach( $value as $ind => $sub_value ){
            $value[ $ind ] = $this->_recursiveJsonDecode( $sub_value );
        }

        return $value;
    }
}