<?php

namespace App\Models;

use App\Models\Traits\HasQueryBuilderCache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Scopes\LanguageScope;
use Illuminate\Support\Facades\Cache;

class Url extends Model {
    // As we've got a deleted_at flag, and want to cache DB calls, include the relevant traits
    use SoftDeletes, HasQueryBuilderCache;

    /**
     * The database table to interact with
     * @var string
     */
    protected $table = 'urls';

    /**
     * We don't have timestamps on URLs, so define that
     * @var bool
     */
    public $timestamps = false;

    /**
     * Any fields to be appended to the object should it be output as an array or JSON object.
     * @var array
     */
    protected $appends = ['object', 'cms_slug'];

    /**
     * When 'booting' this model, we only want to include URLs that are in the correct language, so add a global scope
     */
    protected static function boot(){
        parent::boot();

        static::addGlobalScope(new LanguageScope);
    }

    /**
     * Custom Function allowing us to get System Pages from the Database through a central location.
     *
     * @param $controller
     * @param $method
     * @param array $params
     * @return mixed
     */
    public static function getSystemPage( $controller, $method, $params = array() ){
        $route = 'controller=' . $controller . '&method=' . $method;
        $route_with_params = $route;
        if( !empty($params) ){
            foreach( $params as $key => $value ){
                $route_with_params .= '&' . $key . '=' . $value;
            }
        }

        $url_with_params = self::where([
            'route' => $route_with_params
        ])->first();

        if( $url_with_params ){
            $url = $url_with_params;
        } else {
            $url = self::where([
                ['route', 'like', $route . '%']
            ])->first();
        }

        if( $url ){
            switch( $url->table_name ){
                case 'pages':
                    $page = \Modules\Page\Theme\Models\Page::where([
                        'page_id' => $url->table_key,
                        'language_id' => $url->language_id
                    ])->first();

                    if( !empty( $page ) ) {
                        return $page;
                    } else {
                        $url = false;
                    }
                    break;
            }
        }

        return $url;
    }

    /**
     * Custom Accessor Function.
     * Allows us to pull out the page, case study, news article, etc. this URL belongs to as an object.
     *
     * @return bool|null
     */
    public function getObjectAttribute(){
        $primary_keys = \DB::connection()->getDoctrineSchemaManager()->listTableDetails( $this->table_name )->getPrimaryKeyColumns();

        $primary_key = false;
        $has_version = false;
        foreach( $primary_keys as $pk ){
            if( $pk != 'language_id' && $pk != 'version' ){
                $primary_key = $pk;
            }
            elseif( $pk == 'version' ){
                $has_version = true;
            }
        }

        if( $primary_key == false ){ return false; }

        $object_cache_key = md5( 'object_cache.' . $this->table_name . $this->table_key . $this->language_id );
        $record = Cache::get( $object_cache_key );
        if( !$record || $record == NULL ) {
            switch( $this->table_name ){
                default:
                    $query = \DB::table($this->table_name)->where([
                        $primary_key => $this->table_key,
                        'language_id' => $this->language_id
                    ]);

                    if( $has_version ){
                        $query->whereNull( 'deleted_at' );
                    }

                    $record = $query->first();
                    break;
            }

            if (empty($record)) {
                return null;
            }

            Cache::put($object_cache_key, $record, now()->addMinutes(config('app.cache_time')));
        }

        if( !isset( $record->status ) || $record->status ){
            if( empty( $record->deleted_at ) ) {
                return $record;
            }
        }

        return null;
    }

    /**
     * Custom Accessor Function.
     * When viewing this URL as an Admin, get the correct URL slug for the CMS.
     *
     * @return mixed|string
     */
    public function getCmsSlugAttribute(){
        switch( $this->table_name ){
            case 'news_articles':
                return 'news';
            case 'people':
                return 'our-people';
            default:
                return $this->table_name;
        }
    }
}