<?php
namespace App\Http\Controllers;

use App\Models\Url;

/**
 * Class CommonController
 * @package App\Http\Controllers
 * 
 * The CommonController is used to handle common commands for the website front-end
 */
class CommonController extends Controller {

    /**
     * Adm Ping
     * 
     * Simply returns a JSON response with a logged_in value of true / false
     * depending on whether the user is logged in to the CMS
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function adm_ping(){
        return response()->json([
            'logged_in'    => \Auth::guard( 'adm' )->check()
        ]);
    }

    public function adm_url()
    {
        if (!\Auth::guard('adm')->check()) {
            return response('');
        }

        $url_id = request()->input('url_id');
        $Url = Url::find($url_id);

        if ($Url) {
            return response(route('adm.path', [$Url->cms_slug, 'edit', $Url->table_key, request()->session()->get('language_locale')]));
        }

        return response('');
    }

    /**
     * Advice Bar
     * 
     * Simply loads the IE8 Advice Bar (if required) ready for rendering through
     * JavaScript
     * 
     * @return \Illuminate\Http\Response
     */
    public function advice_bar(){
        $is_ie = false;

        if( isset( $_SERVER['HTTP_USER_AGENT'] ) ){
            preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);
            if( ( count( $matches ) > 1 ) && ( $matches[1] <= 8 ) ){
                $is_ie = true;
            }
        }

        $view_data = [
            'dist_dir'  => route('theme') . '/' . config('app.theme_name', 'default') . '/dist/',
            'is_ie'     => $is_ie
        ];

        return response()->view(  'includes.footer.ie8-advice-bar', $view_data );
    }

    public function token()
    {
        return response(session()->token());
    }

    /**
     * Regenerate Token
     * 
     * Regenerates a CSRF token to get around Cloudflare Caching & CSRF Protection
     * 
     * @return \Illuminate\Http\Response
     */
    public function regenerate_token(){
        $session = app('session');
        if( !$session ){ return response(''); }

        $session->regenerateToken();
        return response( $session->token() );
    }
}