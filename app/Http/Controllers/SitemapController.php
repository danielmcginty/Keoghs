<?php
namespace App\Http\Controllers;

use App\Models\Url;

class SitemapController extends Controller {

    /**
     * The default 'weight' for pages output in the sitemap
     * @var float
     */
    private $default_weight = 0.5;

    /**
     * Any custom 'weight' values for certain page types in the sitemap
     * @var array
     */
    private $table_weights = [
        'pages' => 0.7,
        'news_articles' => 0.8
    ];

    /**
     * The default 'change frequency' for pages output in the sitemap
     * @var string
     */
    private $default_frequency = 'weekly';

    /**
     * Any custom 'change frequency' values for certain page types in the sitemap
     * @var array
     */
    private $table_change_frequencies = [

    ];

    /**
     * Renders the HTML sitemap for the application
     */
    public function index(){
        $this_page = Url::getSystemPage('sitemap', 'index');

        $this->home_url = Url::where('route', 'controller=home&method=index')->first();

        // Now, just add all other URLs at will
        $sitemap_urls = [];
        $urls = Url::where([
            [ 'route', '!=', 'controller=home&method=index' ],
            [ 'route', '!=', 'controller=sitemap&method=index' ],
            [ 'no_index_meta', '=', 0 ]
        ])->get();
        foreach( $urls as $url ){
            if( !empty( $url->object ) ) {
                if( !isset( $sitemap_urls[ $url->table_name ] ) ){
                    $sitemap_urls[ $url->table_name ] = [];
                }

                $sitemap_urls[ $url->table_name ][] = $url;
            }
        }

        $this->pageTitle = $this_page->title;
        $this->this_page = $this_page;
        $this->sitemap_urls = $sitemap_urls;

        $this->view = 'sitemap.html';
    }

    /**
     * Renders the XML sitemap for the application
     *
     * @return $this
     */
    public function xml(){
        // Create an array for our sitemap URLs
        $sitemap_urls = [];

        // First, add the home page to the sitemap URLs
        $home_url = Url::where('route', 'controller=home&method=index')->first();
        $sitemap_urls[] = [
            'loc' => route('theme', [$home_url->url]),
            'changefreq' => isset( $this->table_change_frequencies[ $home_url->table_name] ) ? $this->table_change_frequencies[ $home_url->table_name ] : $this->default_frequency,
            'priority' => 1.0
        ];

        // Now, just add all other URLs at will
        $urls = Url::where([
            [ 'route', '!=', 'controller=home&method=index' ],
            [ 'route', '!=', 'controller=sitemap&method=index' ],
            [ 'no_index_meta', '=', 0 ]
        ])->get();
        foreach( $urls as $url ){
            if( !empty( $url->object ) ) {
                $sitemap_urls[] = [
                    'loc' => route('theme', [$url->url]),
                    'changefreq' => isset($this->table_change_frequencies[$url->table_name]) ? $this->table_change_frequencies[$url->table_name] : $this->default_frequency,
                    'priority' => isset($this->table_weights[$url->table_name]) ? $this->table_weights[$url->table_name] : $this->default_weight
                ];
            }
        }

        // And finally, order the URLs by their priority. Highest first.
        uasort( $sitemap_urls, function( $a, $b ){
            return $a['priority'] < $b['priority'];
        });

        $xsl_location = '/' . config('app.theme_name') . '/dist/xsl/xml-sitemap.xsl';

        $this->setViewLoaderTheme();

        return response()->view( 'sitemap.xml', ['urls' => $sitemap_urls, 'xsl_location' => $xsl_location] )->header('Content-Type', 'application/xml');
    }

}