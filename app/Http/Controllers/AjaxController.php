<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Url;
use App\Models\Redirect;
use Intervention\Image\Exception\NotFoundException;

/**
 * Class AjaxController
 * @package App\Http\Controllers
 *
 * The AjaxController handles all AJAX requests through the Front-End of the website.
 * It is accessed using the route 'theme.ajax' - check the /routes/web.php file for details
 */
class AjaxController extends Controller {

    /**
     * All requests to the AjaxController run through the index() function.
     * This function loads in the requested controller, calls the requested method, and returns output as necessary.
     *
     * @param Request $request
     * @return mixed
     */
    public function index( Request $request ){
        $parameters = $request->route()->parameters();

        if( !isset( $parameters['controller'] ) || !isset( $parameters['ajax_function'] ) ){
            throw new NotFoundException;
        }

        $function = $parameters['ajax_function'];
        $controller_name = $parameters['controller'];

        $controller = $this->_getController( $controller_name );
        if( $controller === false ){
            throw new NotFoundException;
        }

        if( !method_exists( $controller, $function ) ){
            throw new NotFoundException;
        }

        $this->setViewLoaderTheme();

        $controller = new $controller();
        return $controller->{$function}( $request );
    }

    /**
     * For a given controller name, check if the controller exists within the 'core' App namespace
     * If not, check it exists within a module
     *
     * @param $controller_name
     * @return bool|string
     */
    private function _getController( $controller_name ){
        // First check that the controller exists within the 'core' App namespace
        $controller_reference = '\\App\\Http\\Controllers\\' . ucfirst( \Illuminate\Support\Str::camel( \Illuminate\Support\Str::singular( $controller_name ) . "_controller" ) );

        // And if it does, return the class name
        if( class_exists( $controller_reference ) ){ return $controller_reference; }

        // Else, we'll need to check our modules
        $modules_dir = base_path( 'modules' );
        $modules = array_filter( glob($modules_dir.'/*', GLOB_ONLYDIR) );

        $available_modules = (array)config( 'modules.available' );

        foreach( $modules as $module_path ) {
            $path_parts = explode('/', $module_path);
            $module_ns = end($path_parts);

            if( !in_array( $module_ns, $available_modules ) ){
                continue;
            }

            // Now we can check if the controller exists within a module
            $controller_reference = '\\Modules\\' . $module_ns . '\\Theme\\Controllers\\' . ucfirst( \Illuminate\Support\Str::camel( \Illuminate\Support\Str::singular( $controller_name ) . "_controller" ) );

            // And if it does, return the class name
            if( class_exists( $controller_reference ) ){ return $controller_reference; }
        }

        // Else, we can't find the controller so return false.
        return false;
    }
}
