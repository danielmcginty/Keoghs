<?php

namespace App\Http\Controllers;

use App\Models\Url;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $home = Url::getSystemPage('home', 'index');

        if( $home ){
            $this->pageTitle = $home->title;
            $this->this_page = $home;

            $this->is_home_page = true;
            $this->view = 'pages.page';
        }
    }
}
