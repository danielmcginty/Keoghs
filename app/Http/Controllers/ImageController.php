<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Models\File;
use App\Models\FileDirectory;

use Intervention\Image\Exception\NotFoundException;
use Intervention\Image\Facades\Image;

class ImageController extends Controller {
    /**
     * Stores the URL path parameters passed to the controller
     *
     * @var array
     */
    private $parameters;

    /**
     * Stores the File object retrieved from the database
     *
     * @var \App\Models\File
     */
    private $file;

    /**
     * Stores the local image to be manipulated and served
     *
     * @var \Intervention\Image\Image
     */
    private $image;

    /**
     * Boolean determining whether the current image is the 'no-image' image, as it requires a different load path
     *
     * @var bool
     */
    private $is_no_image = false;

    /**
     * Local reference to the cache directory
     *
     * @var string
     */
    private $cache_dir;

    /**
     * Local reference to the cache file for the current image & dimensions
     *
     * @var string
     */
    private $cache_file;

    /**
     * Local array of file extensions that technically are images, but the manipulation library cannot handle, so serve
     * them through the FileController as they are, i.e. without being manipulated.
     *
     * @var array
     */
    private $extensions_to_serve_as_they_are = ['gif', 'svg'];

    /**
     * The handler function for the image resizing controller
     * Takes a set of parameters and manipulates an image for rendering
     *
     * @param Request $request
     * @return mixed
     */
    public function index( Request $request ){
        $this->parameters = $request->route()->parameters();

        $isInline = $request->route()->getName() == 'image.inline-resize';
        $hasCrop = isset($this->parameters['crop']);
        $hasDimensions = isset($this->parameters['dimensions']);

        $this->_getFile();

        // If, somehow, a file that isn't an image is accessed through the ImageController, reroute it through
        // the file controller
        if( !$this->file->is_image ){
            return response()->redirectTo( route('file', $this->file->path ) );
        }

        // If we're trying to get a .gif file, the Intervention Library can't handle animation, so return it through
        // the file controller to serve it as is.
        if( in_array( $this->file->extension, $this->extensions_to_serve_as_they_are ) ){
            $fileController = new FileController();

            return $fileController->index( $request );
        }

        $cached = $this->_checkCache();
        if( $cached ){
            return $this->_serveImage( false );
        }

        $image_path = storage_path( 'app/' . $this->file->storage_path );
        if( $this->is_no_image ){
            $image_path = $this->file->storage_path;
        }

        ini_set( "memory_limit", "2G" );

        $this->image = Image::make( $image_path );

        // Ensure that if resizing, or cropping, an image that the original image is larger than the requested size
        if( $hasDimensions ){
            $dimensions = explode( 'x', $this->parameters['dimensions'] );

            $width = !empty($dimensions[0]) ? $dimensions[0] : null;
            $height = !empty($dimensions[1]) ? $dimensions[1] : null;

            if( (!is_null( $width ) && $width > $this->file->width) || (!is_null($height) && $height > $this->file->height) ){
                $hasCrop = false;
                $hasDimensions = false;
            }
        }

        if( $hasCrop ){
            $this->_cropImage();
        }

        if( $hasDimensions ){
            $this->_resizeImage();
        }

        return $this->_serveImage();
    }

    /**
     * Loads the file from the database
     */
    private function _getFile(){
        $path = $this->parameters['path'];
        $path_parts = array_filter( explode( "/", $path ) ); // Remove empty 'elements' from the exploded array ( images//path.jpg )
        $file_path = array_pop( $path_parts );

        if( str_replace( '_', '-', $file_path ) == 'no-image.jpg' ){
            $this->is_no_image = true;

            $tmp_file = new \StdClass();
            $tmp_file->storage_path = resource_path( 'theme/' . config('app.theme_name') . '/assets/no_image.jpg');
            $tmp_file->file_path = 'no_image.jpg';
            $tmp_file->extension = 'jpg';
            $tmp_file->directory_id = 0;
            $tmp_file->directory_path = '';

            $image_size = getimagesize( $tmp_file->storage_path );
            $tmp_file->width = $image_size[0];
            $tmp_file->height = $image_size[1];

            $this->file = $tmp_file;
            return;
        }
        elseif( str_replace( '_', '-', $file_path ) == 'no-image.png' ){
            $this->is_no_image = true;

            $tmp_file = new \StdClass();
            $tmp_file->storage_path = resource_path( 'theme/' . config('app.theme_name') . '/assets/no_image.png');
            $tmp_file->file_path = 'no_image.png';
            $tmp_file->extension = 'png';
            $tmp_file->directory_id = 0;
            $tmp_file->directory_path = '';

            $image_size = getimagesize( $tmp_file->storage_path );
            $tmp_file->width = $image_size[0];
            $tmp_file->height = $image_size[1];

            $this->file = $tmp_file;
            return;
        }

        if ($path_parts) {
            $parent_dir = File::getClosestDirectory($path_parts);
        }

        $this->file = File::where([
            'file_name' => $file_path,
            'directory_id' => isset($parent_dir) ? $parent_dir->id : 0
        ])->with('directory', 'directory.directory')->first();

        if( empty( $this->file ) ){
            throw new NotFoundException;
        }

        $directory_path = $this->file->directory_path;
        if( sizeof( $path_parts ) > 1 ){
            $request_dir = implode( "/", $path_parts );

            if( $request_dir != $directory_path ){
                throw new NotFoundException;
            }
        }
    }

    /**
     * Serves the image to the browser
     *
     * @param bool|true $with_cache
     * @return mixed
     */
    private function _serveImage( $with_cache = true ){
        if( $with_cache ) {
            $this->_cacheImage();
        }
        $image_response = $this->image->response( $this->file->extension );

        // Need to call ->setPublic() so that pagespeed is able to rewrite the images and cache them
        $image_response->setPublic();

        return $image_response;
    }

    /**
     * Based on the dimensions parameter, resize the image
     */
    private function _resizeImage(){
        $dimensions = explode( 'x', $this->parameters['dimensions'] );

        $width = !empty($dimensions[0]) ? $dimensions[0] : null;
        $height = !empty($dimensions[1]) ? $dimensions[1] : null;

        if( !empty($width) && !empty($height) ){
            $this->image->fit( $width, $height );
        } elseif( !empty($width) ){
            $this->image->resize( $width, null, function( $constraint ){
                $constraint->aspectRatio();
            });
        } elseif( !empty($height) ){
            $this->image->resize( null, $height, function( $constraint ){
                $constraint->aspectRatio();
            });
        }
    }

    /**
     * Based on the crop parameter, crop the image
     */
    private function _cropImage(){
        $crop_values = explode("-", $this->parameters['crop']);

        $crop_size = !empty($crop_values[0]) ? explode("x", $crop_values[0]) : null;
        $position = !empty($crop_values[1]) ? explode("x", $crop_values[1]) : null;

        if( !empty($crop_size) ){
            $w = !empty($crop_size[0]) ? $crop_size[0] : false;
            $h = !empty($crop_size[1]) ? $crop_size[1] : false;
        } else {
            $w = false;
            $h = false;
        }

        if( !empty( $position ) && sizeof( $position ) == 2 ){
            $x = $position[0];
            $y = $position[1];
        } else {
            $x = null;
            $y = null;
        }

        if( $w !== false && $h !== false ){
            $this->image->crop( $w, $h, $x, $y );
        }
    }

    /**
     * Check if the file exists in the cache and load into $this->image if so
     *
     * @return bool
     */
    private function _checkCache(){
        $this->cache_dir = storage_path( 'app/cache/' );
        if( !is_dir( $this->cache_dir ) ){
            mkdir( $this->cache_dir, 0755 );
        }

        $this->cache_file = '';
        // First add the image's dimensions to the cache file name if they exist
        if( isset($this->parameters['dimensions']) ){
            $this->cache_file .= $this->parameters['dimensions'] . '_';
        }

        // Then, add the image's crop dimensions to the cache file name if they exist
        if( isset($this->parameters['crop']) ){
            $this->cache_file .= $this->parameters['crop'] . '_';
        }

        // And finally, add the file's name to the cache file name
        if( $this->is_no_image ){
            $this->cache_file .= 'dist_assets_' . $this->file->file_path;
        } else {
            $this->cache_file .= $this->file->storage_path;
        }

        $cache_file = $this->cache_dir . $this->cache_file;
        if( file_exists( $cache_file ) && filemtime( $cache_file ) >= strtotime( $this->file->created_at ) ){
            $this->image = Image::make( $cache_file );
            return true;
        }

        return false;
    }

    /**
     * Save the image to the cache
     */
    private function _cacheImage(){
        $this->image->save( $this->cache_dir . $this->cache_file );
    }
}