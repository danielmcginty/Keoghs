<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Models\File;
use App\Models\FileDirectory;
use Intervention\Image\Exception\NotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FileController extends Controller {
    /**
     * Stores the URL path parameters passed to the controller
     *
     * @var array
     */
    private $parameters;

    /**
     * Stores the File object retrieved from the database
     *
     * @var \App\Models\File
     */
    private $file;

    /**
     * Stores the local image to be manipulated and served
     *
     * @var \Intervention\Image\Image
     */
    private $stored_file;

    /**
     * Local reference to the cache directory
     *
     * @var string
     */
    private $cache_dir;

    /**
     * Local reference to the cache file for the current image & dimensions
     *
     * @var string
     */
    private $cache_file;

    /**
     * The handler function for the image resizing controller
     * Takes a set of parameters and manipulates an image for rendering
     *
     * @param Request $request
     * @return mixed
     */
    public function index( Request $request ){
        $this->parameters = $request->route()->parameters();

        $this->_getFile();

        return $this->_serveFile();
    }

    /**
     * Custom function used to view files uploaded through custom forms.
     * As the file storage path is different, this is handled slightly differently to files uploaded to the asset
     * manager.
     *
     * @param $filename
     * @return mixed|void
     */
    public function customFormUpload( $filename ){
        $file = new File;

        $full_path = 'custom_form_uploads/' . $filename;
        if( is_file( storage_path('app/' . $full_path) ) ) {
            $file->storage_path = $full_path;

            $this->file = $file;

            return $this->_serveFile();
        }

        $controller = new \App\Http\Controllers\Controller;
        return $controller->error404();
    }

    /**
     * Loads the file from the database
     */
    private function _getFile(){
        $path = $this->parameters['path'];
        $path_parts = array_filter( explode( "/", $path ) ); // Remove empty 'elements' from the exploded array ( images//path.jpg )
        $file_path = array_pop( $path_parts );

        if ($path_parts) {
            $parent_dir = File::getClosestDirectory($path_parts);
        }

        $this->file = File::where([
            'file_name' => $file_path,
            'directory_id' => isset($parent_dir) ? $parent_dir->id : 0
        ])->with('directory', 'directory.directory')->first();

        if( empty( $this->file ) ){
            throw new NotFoundException;
        }

        $directory_path = $this->file->directory_path;
        if( sizeof( $path_parts ) > 1 ){
            $request_dir = implode( "/", $path_parts );

            if( $request_dir != $directory_path ){
                throw new NotFoundException;
            }
        }
    }

    /**
     * Serves the file to the browser
     *
     * @return mixed
     */
    private function _serveFile(){
        return response()->file(storage_path('app/' . $this->file->storage_path));
    }
}