<?php
namespace App\Http\Controllers;

use App\Library\MinifyHtml;
use Illuminate\Http\Request;
use App\Models\Url;
use App\Models\Redirect;

class RouteController extends Controller {

    /**
     * Internal 'map' array, allowing us to point particular 'table_name' values to custom controllers & methods
     * through the application.
     *
     * @var array
     */
    private $route_map = [
        'news_articles' => [
            'controller' => 'news',
            'method' => 'article'
        ],
        'news_categories' => [
            'controller' => 'news',
            'method' => 'category'
        ],
        'case_studies' => [
            'controller' => 'case_study',
            'method' => 'index'
        ],
        'case_study_categories' => [
            'controller' => 'case_study',
            'method' => 'category'
        ],
        'faq_categories' => [
            'controller' => 'faq',
            'method' => 'category'
        ],
        'expertise' => [
            'controller' => 'expertise',
            'method' => 'article'
        ],
        'insights' => [
            'controller' => 'insight',
            'method' => 'article'
        ],
        'people' => [
            'controller' => 'people',
            'method' => 'person'
        ],
        'journeys' => [
            'controller' => 'journey',
            'method' => 'journey'
        ],
    ];

    /**
     * Our default routing controller, called by EVERY URL request to the system that isn't an image or admin route
     * Or starting with an underscore for the debug bar's CSS & JS...)
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index( Request $request ){
        $this->request = $request;

        // Grab the URL from the query string
        $url = ltrim($request->route()->parameter('url'), '/');

        // Does the URL requested have a 301 redirect in place?
        $this->_checkRedirects( $url );

        // Check that the URL is all lower-case
        if( strtolower( $url ) != $url ){
            // And if not, 301 redirect to the lower-case version
            return redirect( route($request->route()->getName(), strtolower($url)), 301 );
        }

        // And check it exists in the database.
        $this_url = Url::where('url', $url)->first();

        $response = false;

        // Does the URL exist?
        if( $this_url ){
            $this->url_object = $this_url;

            $param = null;

            // Is the URL defined as a system page URL with a route?
            if( $this_url->system_page && $this_url->route ){
                // First, we need to 'chunk' out the url's route to get the parameters
                $route_bits = explode("&", $this_url->route);

                // Then, we need to get at least a controller from the route
                $controller = $method = false;

                foreach( $route_bits as $bit ){
                    $params = explode("=", $bit);

                    if( $params[0] == "controller" ){ // Do we have a controller?
                        $controller = $params[1];
                    }
                    elseif( $params[0] == "method" ){ // Do we have a method?
                        $method = $params[1];
                    }
                    else { // Else, pass any extras as new parameters...
                        if( is_null( $param ) ){ $param = []; }
                        $param[ $params[0] ] = $params[1];
                    }
                }

                // If the provided system page route doesn't have a controller, we need to 404.
                if( $controller == false ){
                    return $this->render();
                }

                // If the provided system page route doesn't have a method, assume it's index.
                if( $method == false ){
                    $method = "index";
                }
            }
            // Else, check if the URL's table exists in our route map
            elseif( array_key_exists( $this_url->table_name, $this->route_map ) ){
                $controller = $this->route_map[ $this_url->table_name ]['controller'];
                $method = isset($this->route_map[ $this_url->table_name ]['method']) ? $this->route_map[ $this_url->table_name ]['method'] : 'index';
                $param = $this_url->table_key;
            }
            // Else, we can generate the controller from the URL item's page name, defaulting to the index method
            else {
                $controller = $this_url->table_name;
                $method = 'index';
                $param = $this_url->table_key;
            }

            // Now, we need to see if the controller exists for the item
            $controller_class = $this->_getController( $controller );

            // If we have a controller, we can call the $method method in it,
            // optionally passing the $param parameter if it exists
            if( $controller_class !== false ) {
                // Generate the controller
                $route = \App::make($controller_class);

                // If it has the method we're looking for, call it
                if (method_exists($route, $method)) {
                    if (!empty($param)) {
                        $route->{$method}($param, $request);
                    } else {
                        $route->{$method}($request);
                    }
                }
            }
        }

        if( config( 'app.minify_output' ) ){
            $output = $this->render();

            // Minify the output
            $Minifier = new MinifyHtml();
            $output = $Minifier->minifyContent( $output );

            return $output;
        } else {
            return $this->render();
        }
    }

    /**
     * Custom request handler function for AMP (Accelerated Mobile Pages).
     * Sets a private variable is_amp to true and performs a normal request.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function amp( Request $request ){
        $this->is_amp = true;
        return $this->index( $request );
    }

    /**
     * Custom render() function override, taking is_amp into account.
     * If we're loading an AMP page, then call the amp_render() function rather than the render() function.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function render(){
        if( $this->is_amp ){
            return parent::amp_render();
        }

        return parent::render();
    }

    /**
     * For a given controller name, check if the controller exists within the 'core' App namespace
     * If not, check it exists within a module
     *
     * @param $controller_name
     * @return bool|string
     */
    private function _getController( $controller_name ){
        // First check that the controller exists within the 'core' App namespace
        $controller_reference = '\\App\\Http\\Controllers\\' . ucfirst( \Illuminate\Support\Str::camel( \Illuminate\Support\Str::singular( $controller_name ) . "_controller" ) );

        // And if it does, return the class name
        if( class_exists( $controller_reference ) ){ return $controller_reference; }

        // Else, we'll need to check our modules
        $modules_dir = base_path( 'modules' );
        $modules = array_filter( glob($modules_dir.'/*', GLOB_ONLYDIR) );

        $available_modules = (array)config('modules.available');

        foreach( $modules as $module_path ) {
            $path_parts = explode('/', $module_path);
            $module_ns = end($path_parts);

            if( !in_array( $module_ns, $available_modules ) ){
                continue;
            }

            // Now we can check if the controller exists within a module
            $controller_reference = '\\Modules\\' . $module_ns . '\\Theme\\Controllers\\' . ucfirst( \Illuminate\Support\Str::camel( \Illuminate\Support\Str::singular( $controller_name ) . "_controller" ) );

            // And if it does, return the class name
            if( class_exists( $controller_reference ) ){ return $controller_reference; }
        }

        // Else, we can't find the controller so return false.
        return false;
    }

    /**
     * For a given URL, check if there is a 301 redirect in place for it. If so, redirect to the new URL.
     *
     * @param $url
     */
    private function _checkRedirects( $url ){
        $redirect_exists = Redirect::where('old_url', $url)->first();

        if( $redirect_exists ){

            if( stripos( $redirect_exists->new_url, 'http://' ) !== false || stripos( $redirect_exists->new_url, 'https://' ) !== false ){
                abort( 301, '', ['Location' => $redirect_exists->new_url] );
            }

            $route_name = $this->is_amp ? 'amp' : 'theme';
            abort( 301, '', ['Location' => route($route_name, $redirect_exists->new_url)] );
        }
    }
}