<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;

class BeanstalkController extends Controller
{
    private $_webhook_key;

    public function __construct()
    {
        $this->_webhook_key = env('BEANSTALK_WEBHOOK_KEY');
    }

    public function pre(Request $request)
    {
        if (!$this->_isValidRequest($request)) {
            throw new AuthorizationException();
        }

        if ($this->_isWebhookTest($request)) {
            return response()->json("");
        }

        Artisan::call('down');
    }

    public function post(Request $request)
    {
        if (!$this->_isValidRequest($request)) {
            throw new AuthorizationException();
        }

        if ($this->_isWebhookTest($request)) {
            return response()->json("");
        }

        Artisan::call('migrate', [
            '--force'   => true
        ]);
        Artisan::call('cache:clear');
        Artisan::call('up');
    }

    private function _isValidRequest(Request $request)
    {
        return $request->has('webhook-key') && $request->query('webhook-key') == $this->_webhook_key;
    }

    private function _isWebhookTest(Request $request)
    {
        return !$request->isMethod('post')
            || ($request->has('comment') && $request->input('comment') == 'WebHook Test');
    }
}
