<?php

namespace App\Http\Controllers;

use App\Models\Url;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Cache;

// Load in the AMP library
//use Lullabot\AMP\AMP;
//use Lullabot\AMP\Validate\Scope;

/**
 * Class Controller
 * @package App\Http\Controllers
 *
 * The Controller class acts as a 'Base' controller for the application.
 * It extends the Laravel BaseController, so we've got access to that as required, but provides an extra layer of
 * functionality on top.
 */
class Controller extends BaseController {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Stores data to pass down to the given view.
     * Accessed via magic __get and __set methods.
     *
     * @var array
     */
    private static $data = [];

    /**
     * Stores the view to load.
     * Access via magic __get and __set methods.
     *
     * @var bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private static $view = false;

    /**
     * Stores the row from the URL table obtained from the RouteController's index function
     *
     * @var bool
     */
    private static $url_object = false;

    /**
     * Stores the current canonical URL if required
     *
     * @var string|bool
     */
    private static $canonical_url = false;

    /**
     * Stores the current AMP page URL if present
     *
     * @var string|bool
     */
    private static $amp_html_url = false;

    /**
     * Stores meta data related to social sharing.
     *
     * @var array
     */
    private static $social_meta = [];

    /**
     * Stores extra meta data to pass to a page.
     *
     * @var array
     */
    private static $extra_meta = [];

    /**
     * Stores breadcrumbs to be loaded into the site.
     *
     * @var array
     */
    private static $breadcrumbs = [];

    /**
     * Stores the defaults data from the database
     *
     * @var bool
     */
    private static $website_defaults = false;

    /**
     * Stores the current website 'theme' used for front end templates
     * If empty, the system will load from the default laravel /resources/views/ directory
     *
     * @var string
     */
    private $theme = '';

    /**
     * Constructor.
     * Sets the system up ready to load views from our theme's directory
     */
    public function __construct(){
        $this->theme = config('app.theme_name', 'default');
        $this->setViewLoaderTheme();
    }

    /**
     * Populate the website_defaults variable with the current website defaults object from the database
     */
    private static function getWebsiteDefaults(){
        $defaults_cache_key = 'system_defaults.query_cache';
        if( !Cache::has( $defaults_cache_key ) ){
            $defaults = \DB::table( 'defaults' )->first();

            Cache::put( $defaults_cache_key, $defaults, now()->addMinutes( config('app.cache_time') ) );
        }
        self::$website_defaults = Cache::get( $defaults_cache_key );
    }

    /**
     * Renders the current page.
     * Called from RouteController, loading in the data set to the instance in whatever controller is called.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function render(){
        // Else, return a 'Not Found' page
        if( $this->view === false || !\View::exists( $this->view ) ) {
            $this->error404();
        }

        return view( $this->view, array_merge( $this->_loadDefaultData(), $this->data ) );
    }

    /**
     * Custom render function for Google AMP (Accelerated Mobile Pages) templates.
     * Sets the system to load from the resources/theme/amp/views directory, rather than the config theme_name.
     * If the template doesn't exist in AMP form, throw a 404 error.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function amp_render(){
        $this->setViewLoaderTheme( 'amp' );

        if( $this->view === false || !\View::exists( $this->view ) ){
            // If the AMP specific template doesn't exist, reload the default system theme and throw a 404 error.
            $this->setViewLoaderTheme();
            $this->error404();
        }

        // Disable the debug bar to prevent AMP validation errors due to scripts
        \Debugbar::disable();

        // Disable New Relic to make sure the page passes Google's validation rules
        // See: https://docs.newrelic.com/docs/agents/php-agent/configuration/php-agent-api
        // See: https://docs.newrelic.com/docs/browser/new-relic-browser/troubleshooting/google-amp-validator-fails-due-3rd-party-script
        if (extension_loaded('newrelic') && function_exists('newrelic_disable_autorum')) {
            newrelic_disable_autorum();
        }

        $this->amp_page = true;
        $this->amp_stylesheet = $this->theme . '/dist/css/amp.css';
        $this->amp_svg_logo = $this->theme . '/dist/ui/svg/amp-logo.svg';

        // Load the entire HTML document ready for rendering.
        return view( $this->view, array_merge( $this->_loadDefaultData(), $this->data ) );
        /*
        // Now, load the AMP library and create a new Instance.
        $AMP = new AMP();

        // Because we're passing an entire HTML document, we need to use the HTML_SCOPE scope
        $AMP->loadHtml( $html, ['scope' => Scope::HTML_SCOPE] );

        // Then, convert the markup to AMP-friendly code
        $amp_html = $AMP->convertToAmpHtml();

        // Then return the response and render it.
        return response( $amp_html );
        */
    }

    /**
     * 404 Page.
     * Throws a 404 error, serving the template at /resources/views/errors/404.blade.php.
     * Passes all available data down to the view for use (e.g. default meta information, breadcrumbs, etc)
     *
     * Can be called when needed using $this->error404(),
     * otherwise it's called if the render() function is hit without a view
     */
    protected function error404(){
        // Get the 404 system page
        $error_page = Url::getSystemPage( 'controller', 'error404' );

        if( $error_page ) {
            $this->this_page = $error_page;

            self::$url_object = Url::where(['url' => $error_page->url])->first();
        }

        foreach( array_merge( $this->_loadDefaultData(), $this->data ) as $key => $value ){
            view()->share( $key, $value );
        }
        abort( 404 );
    }

    /**
     * Centralised redirect function.
     * Redirect to the specified location, optionally specifying a HTTP status code
     *
     * @param $location - The location to redirect to
     * @param int $status_code - The HTTP status code to use, defaults to 301
     */
    protected function redirect( $location, $status_code = 301 ){
        abort( $status_code, '', ['Location' => $location] );
    }

    /**
     * Set the view loader to look for views in a custom theme directory.
     * If no theme name is passed, then the config default theme will be used.
     *
     * @param bool|false $custom_theme
     */
    protected function setViewLoaderTheme( $custom_theme = false ){
        $theme_name = ($custom_theme !== false) ? $custom_theme : $this->theme;

        // If we have a custom theme set
        if( !empty( $theme_name ) ){
            // Get the current 'view finder' to use after we've reset this to include previously namespaced view paths
            $currentViewFinder = \View::getFinder();

            // Now, register the laravel 'view finder' to read from our theme directory
            $themeFinder = new \Illuminate\View\FileViewFinder(app()['files'], array(realpath(base_path('resources/theme/' . $theme_name . '/views'))));
            \View::setFinder($themeFinder);

            // After resetting the view finder, reinstate the default views
            foreach( $currentViewFinder->getPaths() as $defaultPath ){
                \View::getFinder()->addLocation( $defaultPath );
            }

            // And after that, re-register the previous namespaced view paths from the previous view finder
            foreach( $currentViewFinder->getHints() as $namespace => $paths ){

                if( $namespace == 'pagination' ){
                    array_unshift( $paths, realpath(base_path('resources/theme/' . $theme_name . '/views/pagination')) );
                }

                view()->addNamespace( $namespace, $paths );
            }
        }
    }

    /**
     * Add Dynamic Script.
     * Adds a dynamic script to the current page.
     *
     * @param $script - The blade to load
     * @param array $data - Optional data array to pass to the blade
     * @throws \Exception
     * @throws \Throwable
     */
    public function addDynamicScript( $script, $data = array() ){
        \Document::addDynamicScript( $script, $data );
    }

    /**
     * Add Dynamic Style.
     * Adds a dynamic style to the current page.
     *
     * @param $style - The blade to load
     * @param array $data - Optional data array to pass to the blade
     * @throws \Exception
     * @throws \Throwable
     */
    public function addDynamicStyle( $style, $data = array() ){
        \Document::addDynamicStyle( $style, $data );
    }

    /**
     * Add Meta.
     * Adds extra meta data to the current page.
     *
     * @param $key
     * @param $value
     */
    public function addMeta( $key, $value ){
        if( !isset( self::$extra_meta[ $key ] ) ) {
            self::$extra_meta[ $key ] = $value;
        }
    }

    /**
     * Initializes default social meta data on load.
     * If any custom values are set to these properties, this won't be overwritten
     */
    private function initSocialMeta(){
        // Set variables to be used multiple times
        $defaults = self::$website_defaults;

        $website_title = config( 'app.name' );
        $title = (isset(self::$url_object->meta_title) && !empty(self::$url_object->meta_title)) ? self::$url_object->meta_title : translate('meta_title');
        $description = (isset(self::$url_object->meta_description) && !empty(self::$url_object->meta_description)) ? self::$url_object->meta_description : translate('social_share_default_description');
        $url = url()->current();

        // Should be main page image, if available
        $og_image = route('theme') . '/' . $this->theme . '/dist/ui/og-share.png';
        $twitter_image = route('theme') . '/' . $this->theme . '/dist/ui/twitter-share.png';

        // Should be username of article author, if available
        $twitter_username = translate( 'twitter_username' );
        $facebook_link = translate( 'social_facebook' );

        // Facebook App ID
        if( !isset( self::$social_meta[md5('fb:app_id')] ) ){ $this->addSocialMeta('fb:app_id', '335918716570799'); }

        // OpenGraph
        if( !isset( self::$social_meta[md5('og:type')] ) ){ $this->addSocialMeta('og:type', 'website'); }
        if( !isset( self::$social_meta[md5('og:site_name')] ) ){ $this->addSocialMeta('og:site_name', $website_title); }
        if( !isset( self::$social_meta[md5('og:title')] ) ){ $this->addSocialMeta( 'og:title', $title ); }
        if( !isset( self::$social_meta[md5('og:image')] ) ){ $this->addSocialMeta('og:image', $og_image); }
        if( !isset( self::$social_meta[md5('og:image:width')] ) ){ $this->addSocialMeta('og:image:width', 1200); }
        if( !isset( self::$social_meta[md5('og:image:height')] ) ){ $this->addSocialMeta('og:image:height', 630); }
        if( !isset( self::$social_meta[md5('og:url')] ) ){ $this->addSocialMeta('og:url', $url); }
        if( !isset( self::$social_meta[md5('og:description')] ) ){ $this->addSocialMeta('og:description', $description); }
        //if( !isset( self::$social_meta[md5('og:locale')] ) ){ $this->addSocialMeta('og:locale','en_GB'); }

        // Twitter Card
        if( !isset( self::$social_meta[md5('twitter:card')] ) ){ $this->addSocialMeta('twitter:card', 'summary_large_image'); }
        if( !isset( self::$social_meta[md5('twitter:site')] ) ){ $this->addSocialMeta('twitter:site', '@'.$twitter_username); }
        if( !isset( self::$social_meta[md5('twitter:creator')] ) ){ $this->addSocialMeta('twitter:creator','@'.$twitter_username); }
        if( !isset( self::$social_meta[md5('twitter:domain')] ) ){ $this->addSocialMeta('twitter:domain', str_replace( array('http://', 'https://'), '', config('app.url'))); }
        if( !isset( self::$social_meta[md5('twitter:title')] ) ){ $this->addSocialMeta('twitter:title', $title); }
        if( !isset( self::$social_meta[md5('twitter:image')] ) ){ $this->addSocialMeta('twitter:image', $twitter_image); }
        if( !isset( self::$social_meta[md5('twitter:image:alt')] ) ){ $this->addSocialMeta('twitter:image:alt', $website_title); }
        if( !isset( self::$social_meta[md5('twitter:url')] ) ){ $this->addSocialMeta('twitter:url', $url); }
        if( !isset( self::$social_meta[md5('twitter:description')] ) ){ $this->addSocialMeta('twitter:description', $description); }

        uasort( self::$social_meta, function( $a, $b ){
            $a_prop = $a['property'];
            $b_prop = $b['property'];

            if( $a_prop == 'og:image' ){
                if( in_array( $b_prop, array( 'og:image:width', 'og:image:height', 'og:image:type' ) ) ){
                    return -1;
                }
            }
            elseif( $a_prop == 'og:image:width' ){
                if( $b_prop == 'og:image' ){ return 1; }
                if( in_array( $b_prop, array('og:image:height', 'og:image:type') ) ){ return -1; }
            }
            elseif( $a_prop == 'og:image:height' ){
                if( in_array( $b_prop, array('og:image', 'og:image:width') ) ){
                    return 1;
                }
                if( $b_prop == 'og:image:type' ){
                    return -1;
                }
            }
            elseif( $a_prop == 'og:image:type' ){
                if( in_array( $b_prop, array( 'og:image', 'og:image:width', 'og:image:height' ) ) ){
                    return 1;
                }
            }

            return strcmp( $a['property'], $b['property'] );
        });

        foreach( self::$social_meta as $hash => $tag_attributes ){
            if( empty( $tag_attributes['content'] ) ){
                unset( self::$social_meta[ $hash ] );
                continue;
            }

            if( stristr( $tag_attributes['property'], 'og:' ) !== false || stristr( $tag_attributes['property'], 'fb:' ) !== false || stristr( $tag_attributes['property'], 'article:' ) !== false ){
                self::$social_meta[ $hash ]['type'] = 'property';
            } else {
                self::$social_meta[$hash]['type'] = 'name';
            }
        }
    }

    /**
     * Add Social Meta
     * Function used to add custom social meta to the page.
     *
     * @param $property - The meta key (e.g. og:image)
     * @param $content - The meta value
     */
    public function addSocialMeta( $property, $content ) {
        self::$social_meta[md5($property)] = array(
            'property'  => $property,
            'content'   => $content
        );
    }

    /**
     * Setter for the Canonical URL property
     *
     * @param $canonical_url
     */
    public function setCanonicalURL( $canonical_url ){
        self::$canonical_url = $canonical_url;
    }

    /**
     * Getter for the Canonical URL property
     * @return bool|string
     */
    public function getCanonicalURL(){
        return self::$canonical_url;
    }

    /**
     * Setter for the AMP Url property
     *
     * @param $amp_html_url
     */
    public function setAMPHtmlURL( $amp_html_url ){
        self::$amp_html_url = $amp_html_url;
    }

    /**
     * Getter for the AMP Url Property
     *
     * @return bool|string
     */
    public function getAMPHtmlUrl(){
        return self::$amp_html_url;
    }

    /**
     * Add a breadcrumb for the current page.
     *
     * @param $url
     * @param $label
     */
    public function addBreadcrumb( $url, $label ){
        self::$breadcrumbs[] = [ 'url' => $url, 'label' => $label ];
    }

    /**
     * Reset the breadcrumb array
     */
    public function clearBreadcrumbs(){
        self::$breadcrumbs = [];
    }

    /**
     * Returns the a set of breadcrumbs for an item, tiering up through 'parent' fields
     *
     * @param $item
     * @param $label_field
     * @param $parent_field
     * @return array
     */
    public function getBreadcrumbs( $item, $label_field, $parent_field ){
        $breadcrumbs = [];
        if( !is_object( $item ) ){
            return $breadcrumbs;
        }

        $breadcrumbs[] = ['url' => $item->url, 'label' => $item->{$label_field}];

        if( !empty( $item->{$parent_field} ) ){
            $this->_parentBreadcrumbs( $label_field, $parent_field, $item->{$parent_field}, $breadcrumbs );
        }

        return array_reverse( $breadcrumbs );
    }

    /**
     * Used in conjunction with getBreadcrumbs().
     * Recursively generates parent breadcrumbs for the given item
     *
     * @param $label_field
     * @param $parent_field
     * @param $parent
     * @param $breadcrumbs
     */
    private function _parentBreadcrumbs( $label_field, $parent_field, $parent, &$breadcrumbs ){
        $breadcrumbs[] = ['url' => $parent->url, 'label' => $parent->{$label_field}];
        if( !empty( $parent->{$parent_field} ) ){
            $this->_parentBreadcrumbs( $label_field, $parent_field, $parent->{$parent_field}, $breadcrumbs );
        }
    }

    /**
     * Loads in the default data to pass down to the view.
     * e.g. get the meta values from $url_object
     *
     * @return array
     */
    private function _loadDefaultData(){
        self::getWebsiteDefaults();

        $defaults = [];

        // Pass through website title & email
        $defaults['website_title'] = config( 'app.name' );
        $defaults['website_email'] = translate( 'website_email' );
        $defaults['website_telephone'] = translate('telephone_number');

        // Pass through meta data
        $default_meta_title = translate('meta_title');
        if( isset( self::$data['pageTitle'] ) ){
            $default_meta_title = self::$data['pageTitle'] . ' | ' . $default_meta_title;
        }

        $defaults['meta_title'] = (isset(self::$url_object->meta_title) && !empty(self::$url_object->meta_title)) ? self::$url_object->meta_title : $default_meta_title;
        $defaults['meta_description'] = (isset(self::$url_object->meta_description) && !empty(self::$url_object->meta_description)) ? self::$url_object->meta_description : translate('meta_description');
        $defaults['no_index'] = (isset(self::$url_object->no_index_meta) && !empty(self::$url_object->no_index_meta)) ? self::$url_object->no_index_meta : false;

        $this->initSocialMeta();
        $defaults['social_meta'] = self::$social_meta;
        $defaults['extra_meta'] = self::$extra_meta;

        // Pass through dynamic scripts & styles
        $defaults['dynamic_scripts'] = \Document::getDynamicScripts();
        $defaults['dynamic_styles'] = \Document::getDynamicStyles();

        // Pass through the canonical link
        $defaults['canonical_url'] = self::$canonical_url ? self::$canonical_url : url()->current();
        $defaults['amp_html_url'] = self::$amp_html_url ? self::$amp_html_url : false;

        // Pass through the base URL with the theme
        $defaults['theme_url'] = route('theme') . '/' . $this->theme;

        // Set a default 'dist' directory to be used across all templates
        $defaults['dist_dir'] = $defaults['theme_url'] . '/dist/';

        // Pass through the Google Analytics UA Code
        $defaults['google_analytics_ua_code'] = self::$website_defaults->google_analytics_ua_code;

        // Pass through breadcrumbs
        $defaults['breadcrumbs'] = self::$breadcrumbs;

        $defaults['current_url_object'] = self::$url_object;

        return $defaults;
    }

    /**
     * Magic get method.
     * Returns a variable from either the class, or from the class' $data variable
     *
     * @param $variable
     * @return bool
     */
    public function __get( $variable ){
        if( property_exists( '\\App\\Http\\Controllers\\Controller', $variable ) ){
            return self::$$variable;
        }

        return isset( self::$data[$variable] ) ? self::$data[$variable] : false;
    }

    /**
     * Magic set method.
     * Sets a value to a variable either in the class, or in the class' $data variable
     *
     * @param $variable
     * @param $value
     */
    public function __set( $variable, $value ){
        if( property_exists( '\\App\\Http\\Controllers\\Controller', $variable ) ){
            self::$$variable = $value;
        }

        self::$data[$variable] = $value;
    }
}
