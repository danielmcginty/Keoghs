<?php
namespace App\Http\Controllers\Api;

use App\Models\File;
use App\Models\PageBuilderBlock;
use App\Helpers\MenuHelper;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\CaseStudies\Theme\Models\CaseStudy;
use Modules\CaseStudies\Theme\Models\CaseStudyCategory;
use Modules\Faqs\Theme\Models\Faq;
use Modules\Faqs\Theme\Models\FaqCategory;
use Modules\FormBuilder\Models\Form;
use Modules\News\Theme\Models\NewsArticle;
use Modules\News\Theme\Models\NewsCategory;
use Modules\Offices\Theme\Models\Office;
use Modules\Page\Theme\Models\Page;
use Modules\TeamMembers\Theme\Models\TeamMember;
use Modules\Testimonials\Theme\Models\Testimonial;

/**
 * Class RouteController
 *
 * API Route Controller
 * Handles API Requests and serves a response
 */
class RouteController extends Controller {

    /**
     * Array containing the valid resource endpoints, and their respective
     * Eloquent Classes
     *
     * key => value, as
     * endpoint => class
     *
     * @var array
     */
    private static $_RESOURCES      = [
        'news_articles'         => NewsArticle::class,
        'news_categories'       => NewsCategory::class,
        'case_studies'          => CaseStudy::class,
        'case_study_categories' => CaseStudyCategory::class,
        'faqs'                  => Faq::class,
        'faq_categories'        => FaqCategory::class,
        'offices'               => Office::class,
        'pages'                 => Page::class,
        'page_builder'          => PageBuilderBlock::class,
        'team_members'          => TeamMember::class,
        'testimonials'          => Testimonial::class,
    ];

    /**
     * Constructor
     *
     * Checks the Auth Token in the Header against the config settings to
     * determine if the request is valid
     */
    public function __construct(){
        $auth_token = request()->header( config( 'cors.auth_header' ) );

        $access_override = $_SERVER['REMOTE_ADDR'] == '82.70.220.206';

        if( $auth_token != config( 'cors.auth_token' ) && request()->method() != 'OPTIONS' && !$access_override ){
            abort( 403, 'Access Denied' );
        }
    }

    /**
     * Table
     * The table function is used to return a collection of objects, for
     * example News Articles or Case Studies
     *
     * e.g. https://www.domain.com/api/resource/news_articles
     *
     * @return \Illuminate\Http\Response
     */
    public function table(){
        $table_name = ltrim( request()->route()->parameter( 'table_name' ), '/' );

        if( array_key_exists( $table_name, self::$_RESOURCES ) ){
            $Class = new self::$_RESOURCES[ $table_name ];
            return $this->_serve( $Class->get() );
        }

        return $this->_serve( ['success' => false], Response::HTTP_NOT_FOUND );
    }

    /**
     * Component
     * The component function is used to return miscellaneous objects from the
     * system, for example menus
     *
     * e.g. https://www.domain.com/api/component/menu/header_menu
     *
     * @return \Illuminate\Http\Response
     */
    public function component(){
        $component = request()->route()->parameter( 'component' );
        $component_param = request()->route()->parameter( 'component_param' );

        switch( $component ){
            case 'menu':
                return $this->_serve( MenuHelper::getMenu( $component_param ) );
                break;
            case 'form':
                return $this->_serve( [Form::where( 'form_id', $component_param )->first()] );
                break;
        }

        return $this->_serve( ['success' => false], Response::HTTP_NOT_FOUND );
    }

    /**
     * Serve
     * Centralised function used to serve a consistent response format
     * from the API
     *
     * @param mixed $data
     * @param null|int $status_code
     * @return \Illuminate\Http\Response
     */
    protected function _serve( $data, $status_code = null ){
        $Response = response()->json( $data );

        if( !is_null( $status_code ) ){
            $Response->setStatusCode( $status_code );
        }

        return $Response;
    }

}