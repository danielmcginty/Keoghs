<?php
namespace App\Http\Controllers;

use \Throwable;

/**
 * Class ErrorController
 * @package App\Http\Controllers
 *
 * The ErrorController is used to output most application errors in a user-friendly way.
 */
class ErrorController extends Controller {

    /**
     * This function is called whenever a handle-able error is encountered.
     *
     * @param Throwable $exception
     * @return \Illuminate\Http\Response
     */
    public function throwError( Throwable $exception ){

        // IP Locked error state - I.e. only show the debug info if it's our office IP
        $this->show_exception = ($_SERVER['REMOTE_ADDR'] == '82.70.220.206') || (app('env') == 'dev');
        if( $this->show_exception ){
            $this->exception = $exception;
        }

        // We need to return a HTTP Response, rather than a view here, so render the HTML first
        $this->view = 'errors.generic';
        $response_html = $this->render();

        // Then, manually build the HTTP Response
        $response = new \Illuminate\Http\Response;
        $response->setContent( $response_html );
        $response->setStatusCode(500);

        // And return it
        return $response;
    }
}