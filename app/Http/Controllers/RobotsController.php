<?php
namespace App\Http\Controllers;

use Illuminate\Routing\Controller;

class RobotsController extends Controller {

    protected $lines = [
        '#Created via RobotsController'
    ];

    public function index(){
        switch( config( 'app.env' ) ){
            case 'production': $this->live(); break;
            default: $this->demo(); break;
        }

        return response( implode( PHP_EOL, $this->lines ) )
            ->header( 'Content-Type', 'text/plain' );
    }

    protected function live(){
        $this->lines[] = 'Sitemap: ' . route('sitemap.xml' );
        $this->lines[] = '';
        $this->lines[] = 'User-Agent: *';
        $this->lines[] = 'Allow: /';
        $this->lines[] = 'Disallow: /adm';
    }

    protected function demo(){
        $this->lines[] = 'User-Agent: *';
        $this->lines[] = 'Disallow: /';
    }

}