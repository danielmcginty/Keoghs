<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Exceptions\PostTooLargeException;
use Illuminate\Http\Request;

/**
 * Class VerifyPostSize
 * @package App\Http\Middleware
 *
 * Custom Middleware allowing us to detect if the current HTTP POST is too large for the application to handle
 */
class VerifyPostSize {

    /**
     * This function is called by the application internals when the middleware is run.
     *
     * It detects if the current POST request is too large, and throws an exception that we can handle correctly if so.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws PostTooLargeException
     */
    public function handle( Request $request, Closure $next ){
        if( !( $request->isMethod( 'POST' ) || $request->isMethod( 'PUT' ) ) ){
            return $next( $request );
        }

        $maxPostSize = $this->_getPostMaxSize();
        $contentSize = $request->server('CONTENT_LENGTH');

        if( $contentSize > $maxPostSize || ($request->isMethod('POST') && empty($_POST)) ){
            throw new PostTooLargeException( "Sorry, we couldn't handle that submission. If you have selected files to upload, please be sure they are below the maximum file size (" . $this->_getMaxFileSize() . ") and try again." );
        }

        return $next( $request );
    }

    /**
     * Simple function returning the application's PHP post_max_size setting in bytes
     * @return int
     */
    private function _getPostMaxSize(){
        if (is_numeric($postMaxSize = ini_get('post_max_size'))) {
            return (int) $postMaxSize;
        }

        $metric = strtoupper(substr($postMaxSize, -1));

        switch ($metric) {
            case 'K':
                return (int) $postMaxSize * 1024;
            case 'M':
                return (int) $postMaxSize * 1048576;
            case 'G':
                return (int) $postMaxSize * 1073741824;
            default:
                return (int) $postMaxSize;
        }
    }

    /**
     * Simple function returning the application's PHP upload_max_filesize setting as a string to show to the user.
     * @return string
     */
    private function _getMaxFileSize(){
        if( is_numeric( $maxFileSize = ini_get('upload_max_filesize' ) ) ){
            return ((int)$maxFileSize) . 'bytes';
        }

        $metric = strtoupper( substr( $maxFileSize, -1 ) );

        switch( $metric ){
            case 'K':
                return ((int)$maxFileSize * 1024) . 'kb';
            case 'M':
                return ((int)$maxFileSize * 1048576) . 'mb';
            case 'G':
                return ((int)$maxFileSize * 1073741824) . 'gb';
            default:
                return ((int)$maxFileSize) . 'bytes';
        }
    }
}