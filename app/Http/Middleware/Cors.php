<?php
namespace App\Http\Middleware;

use Closure;

class Cors {

    public function handle( $request, Closure $next ){
        $origin = isset( $_SERVER['HTTP_ORIGIN'] ) ? $_SERVER['HTTP_ORIGIN'] : null;

        $closure = $next( $request );

        if( !is_null( $origin ) ){
            if( in_array( $origin, config( 'cors.origins' ) ) ){
                $closure
                    ->header( 'Access-Control-Allow-Origin', $origin )
                    ->header( 'Access-Control-Allow-Methods', implode( ', ', config( 'cors.methods' ) ) )
                    ->header( 'Access-Control-Allow-Headers', implode( ', ', config( 'cors.headers' ) ) );
            }
        }

        return $closure;
    }

}