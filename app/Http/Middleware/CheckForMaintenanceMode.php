<?php
namespace App\Http\Middleware;

class CheckForMaintenanceMode extends \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode {

    protected $except = [
        '/beanstalk/pre',
        '/beanstalk/post'
    ];

}