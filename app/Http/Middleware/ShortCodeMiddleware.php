<?php
namespace App\Http\Middleware;

use App\Helpers\ShortCodeHelper;
use Closure;

class ShortCodeMiddleware {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle( $request, Closure $next ){
        $response = $next($request);

        if(!method_exists($response, 'content')):
            return $response;
        endif;

        $content = ShortCodeHelper::shortCodeToURLs( $response->content() );

        $response->setContent($content);

        return $response;
    }
}