<?php

namespace App\Http\Middleware;

use Closure;

class ProtectFrontend
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->ip() !== "34.91.187.1" && config('app.env') !== 'dev') {
            return redirect('adm/login');
        }

        return $next($request);
    }
}