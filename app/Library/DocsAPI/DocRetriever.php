<?php
namespace App\Library\DocsAPI;

class DocRetriever {

    protected static $_HIERARCHY = [];

    public static function getDocs(){
        $files = [];

        $folders_to_scan = [
            'adm',
            'app',
            'modules'
        ];
        foreach( $folders_to_scan as $folder ){
            $files = array_merge( [], $files, self::_scanForFiles( base_path( $folder ) ) );
        }

        return ['files' => $files, 'hierarchy' => array_values( self::$_HIERARCHY )];
    }

    protected static function _scanForFiles( $base_path ){
        $files = [];
        $paths = glob( $base_path . '/*' );

        foreach( $paths as $path ){
            if( is_file( $path ) ){
                $files[] = self::_parseDoc( $path );
            }
            elseif( is_dir( $path ) ){
                $files = array_merge( [], $files, self::_scanForFiles( $path ) );
            }
        }

        return $files;
    }

    protected static function _parseDoc( $file_path ){
        $file_contents = file_get_contents( $file_path );

        return [
            'file'          => ltrim( str_replace( base_path(), '', $file_path ), '\\' ),
            'namespace'     => self::_getNamespace( $file_contents ),
            'class'         => self::_getClass( $file_contents ),
            'properties'    => self::_getProperties( $file_contents ),
            'methods'       => self::_getMethods( $file_contents )
        ];
    }

    protected static function _getNamespace( $contents ){
        $namespace = '';

        preg_match_all( '/namespace (.*);/m', $contents, $matches );
        if( isset( $matches[1] ) && isset( $matches[1][0]) ){ $namespace = $matches[1][0]; }

        self::_addNamespaceToHierarchy( $namespace );

        return $namespace;
    }

    protected static function _addNamespaceToHierarchy( $namespace ){
        if( empty( $namespace ) ){ return; }

        $parts = explode( '\\', $namespace );
        $parts = array_reverse( $parts );

        $parent = '';
        while( sizeof( $parts ) > 0 ){
            $part = array_pop( $parts );
            $key = $parent . $part;

            if( !isset( self::$_HIERARCHY[ $key ] ) ){
                self::$_HIERARCHY[ $key ] = [
                    'parent'    => rtrim( $parent, '\\' ),
                    'name'      => $part,
                    'children'  => []
                ];
            }

            if( isset( self::$_HIERARCHY[ rtrim( $parent, '\\' ) ] ) && !in_array( $key, self::$_HIERARCHY[ rtrim( $parent, '\\' ) ]['children'] ) ){
                self::$_HIERARCHY[ rtrim( $parent, '\\' ) ]['children'][] = $key;
            }

            $parent = $key . '\\';
        }
    }

    protected static function _getClass( $contents ){
        preg_match_all( '/class ([^\s]*)(\sextends .*)?(\simplements .*)?\s?{/m', $contents, $matches );

        return [
            'class'         => !empty( $matches[1] ) ? $matches[1][0] : false,
            'extends'       => !empty( $matches[2] ) ? trim( str_ireplace( 'extends', '', $matches[2][0] ) ) : false,
            'implements'    => !empty( $matches[3] ) ? trim( str_ireplace( 'implements', '', $matches[3][0] ) ) : false
        ];
    }

    protected static function _getProperties( $contents ){
        $props = [];

        preg_match_all( '/(public|protected|private) \$(.*)(\s?=?\s?.*);/m', $contents, $dynamic_matches );
        preg_match_all( '/(public|protected|private) static \$(.*)(\s?=?\s?.*);/m', $contents, $static_matches );

        if( !empty( $dynamic_matches[0] ) ){
            foreach( $dynamic_matches[0] as $match => $property ){
                $props[] = self::_parseProperty( $property, self::_parseComment( $contents, $property ) );
            }
        }

        if( !empty( $static_matches[0] ) ){
            foreach( $static_matches[0] as $match => $property ){
                $props[] = self::_parseProperty( $property, self::_parseComment( $contents, $property ) );
            }
        }

        return $props;
    }

    protected static function _parseProperty( $property_string, $comments_arr ){
        $dollar_pos = stripos( $property_string, '$' );

        $type_static_str = trim( substr( $property_string, 0, $dollar_pos ) );
        if( stristr( $type_static_str, 'static' ) !== false ){
            $static = true;
            $type = trim( str_ireplace( 'static', '', $type_static_str ) );
        } else {
            $static = false;
            $type = trim( $type_static_str );
        }

        $var_str = trim( str_ireplace( '$', '', substr( $property_string, $dollar_pos ) ) );

        if( stristr( $var_str, '=' ) !== false ){
            $var_parts = explode( '=', $var_str );
            $var = trim( $var_parts[0] );
            $default = trim( str_ireplace( ';', '', $var_parts[1] ) );

            if( $default == '[]' ){
                $default = 'array()';
            }
        } else {
            $default = false;
            $var = trim( str_ireplace( ';', '', $var_str ) );
        }

        return [
            'control'   => $type,
            'type'      => ($static ? 'static' : 'dynamic'),
            'var'       => $var,
            'default'   => $default,
            'comments'  => $comments_arr
        ];
    }

    protected static function _getMethods( $contents ){
        $methods = [];

        preg_match_all( '/(public|protected|private) function (.*)\s?\(\s?(.*)\s?\)\s?{/m', $contents, $dynamic_matches );
        preg_match_all( '/(public|protected|private) static function (.*)\s?\(\s?(.*)\s?\)\s?{/m', $contents, $static_matches );

        if( !empty( $dynamic_matches[0] ) ){
            foreach( $dynamic_matches[0] as $match => $method ){
                $methods[] = self::_parseMethod( $method, self::_parseComment( $contents, $method ) );
            }
        }

        if( !empty( $static_matches[0] ) ){
            foreach( $static_matches[0] as $match => $method ){
                $methods[] = self::_parseMethod( $method, self::_parseComment( $contents, $method ) );
            }
        }

        return $methods;
    }

    protected static function _parseMethod( $method_string, $comments_arr ){
        $params = [];

        $function_pos = stripos( $method_string, 'function' );

        $type_static_str = trim( substr( $method_string, 0, $function_pos ) );
        if( stristr( $type_static_str, 'static' ) !== false ){
            $static = true;
            $type = trim( str_ireplace( 'static', '', $type_static_str ) );
        } else {
            $static = false;
            $type = trim( $type_static_str );
        }

        $function_str = trim( str_ireplace( 'function', '', substr( $method_string, $function_pos ) ) );
        $first_bracket_pos = stripos( $function_str, '(' );

        $name = trim( substr( $function_str, 0, $first_bracket_pos ) );

        $param_str = trim( substr( $function_str, $first_bracket_pos ) );
        $param_str = trim( rtrim( $param_str, '{' ) );
        $param_str = trim( substr( $param_str, 1, -1 ) );

        if( !empty( $param_str ) ){
            foreach( explode( ',', $param_str ) as $param ){
                $param_parts = explode( ' ', trim( $param ) );
                $has_default = stristr( $param, '=' ) !== false;

                $param_type = sizeof( $param_parts ) > 1 ? trim( $param_parts[0] ) : false;
                $var = trim( $param_parts[0] );
                $default = false;

                if( stristr( $param_type, '$' ) !== false ){
                    $param_type = false;
                } elseif( sizeof( $param_parts ) > 1 ) {
                    $var = trim( $param_parts[1] );
                }

                if( $has_default && sizeof( $param_parts ) > 1 ){
                    $default = trim( $param_parts[2] );
                }

                $params[] = [
                    'type'      => $param_type,
                    'var'       => $var,
                    'default'   => $default
                ];
            }
        }

        return [
            'control'   => $type,
            'type'      => ($static ? 'static' : 'dynamic'),
            'name'      => $name,
            'params'    => $params,
            'comments'  => $comments_arr
        ];
    }

    protected static function _parseComment( $contents, $matched_string ){
        $match_pos = strpos( $contents, $matched_string );

        $contents_before_match = substr( $contents, 0, $match_pos );
        $comment_start_pos = strrpos( $contents_before_match, '/**' );

        $comment = substr( $contents_before_match, $comment_start_pos );

        $comment_lines = explode( "\r\n", $comment );
        foreach( $comment_lines as $ind => $line ){
            $comment_lines[ $ind ] = trim( $line );
        }

        $description_parts = [];
        $vars = [];
        $return = [];

        foreach( $comment_lines as $line ){
            if( trim( $line ) == '/**' || trim( $line ) == '*/' || trim( $line ) == '*' ){
                continue;
            }

            $line = trim( str_ireplace( '*', '', $line ) );

            if( stristr( $line, '@var' ) !== false || stristr( $line, '@param' ) !== false ){
                $var_str = trim( str_ireplace( array( '@var', '@param' ), '', $line ) );

                $name = '';
                $type = '';

                $var_parts = explode( ' ', str_replace( '-', '', $var_str ) );
                if( sizeof( $var_parts ) > 1 ){
                    $type = $var_parts[0];
                    $name = $var_parts[1];

                    if( stripos( $type, '$' ) === 0 ){
                        $name = $type;
                        $type = '';
                    }

                    $comment_parts = explode( ' ', $name );
                    array_shift( $comment_parts );
                    $var_comments = implode( ' ', $comment_parts );
                } else {
                    if( stripos( $var_str, '$' ) === 0 ){
                        $name = $var_str;

                        $comment_parts = explode( ' ', $name );
                        array_shift( $comment_parts );
                        $var_comments = implode( ' ', $comment_parts );
                    } else {
                        $type = $var_str;

                        $comment_parts = explode( ' ', $type );
                        array_shift( $comment_parts );
                        $var_comments = implode( ' ', $comment_parts );
                    }
                }

                $vars[] = [
                    'type'      => $type,
                    'name'      => $name,
                    'comments'  => nl2br( $var_comments )
                ];
            }
            elseif( stristr( $line, '@return' ) !== false ){
                $return_str = trim( str_ireplace( '@return', '', $line ) );

                $return_parts = explode( ' ', $return_str );

                $type = array_shift( $return_parts );
                $return_comments = implode( ' ', $return_parts );

                $return = [
                    'type'      => $type,
                    'comments'  => nl2br( $return_comments )
                ];
            }
            else {
                $description_parts[] = trim( ltrim( trim( $line ), '*' ) );
            }
        }

        if( stristr( $matched_string, 'function' ) === false ){
            $vars = reset( $vars );

            return [
                'description'   => implode( '<br/>', $description_parts ),
                'var'           => $vars,
                'return'        => $return
            ];
        }

        return [
            'description'   => implode( '<br/>', $description_parts ),
            'vars'          => $vars,
            'return'        => $return
        ];
    }

}