<?php

namespace App\Library\Policies;

use Spatie\Csp\Directive;
use Spatie\Csp\Keyword;
use Spatie\Csp\Policies\Policy;

class BaselinePolicy extends Policy
{
    public function configure()
    {
        $this
            ->addDirective(Directive::BASE, Keyword::SELF)

            ->addDirective(Directive::CONNECT, Keyword::SELF)
            ->addDirective(Directive::CONNECT, 'https://*.hotjar.com')
            ->addDirective(Directive::CONNECT, 'https://*.google-analytics.com')
            ->addDirective(Directive::CONNECT, 'https://*.linkedin.com')
            ->addDirective(Directive::CONNECT, 'https://*.facebook.com')
            ->addDirective(Directive::CONNECT, 'https://*.twitter.com')

            ->addDirective(Directive::DEFAULT, Keyword::SELF)

            ->addDirective(Directive::FORM_ACTION, Keyword::SELF)
            ->addDirective(Directive::FORM_ACTION, 'https://*.facebook.com')

            ->addDirective(Directive::IMG, '*')
            ->addDirective(Directive::IMG, 'data:')

            ->addDirective(Directive::MEDIA, Keyword::SELF)

            ->addDirective(Directive::OBJECT, Keyword::NONE)

            ->addDirective(Directive::SCRIPT, Keyword::SELF)
            ->addDirective(Directive::SCRIPT, Keyword::UNSAFE_INLINE)
            ->addDirective(Directive::SCRIPT, 'https://*.googleapis.com')
            ->addDirective(Directive::SCRIPT, 'https://*.googletagmanager.com')
            ->addDirective(Directive::SCRIPT, 'https://*.google-analytics.com')
            ->addDirective(Directive::SCRIPT, 'https://*.hotjar.com')
            ->addDirective(Directive::SCRIPT, 'https://*.facebook.net')
            ->addDirective(Directive::SCRIPT, 'https://*.twitter.com')
            ->addDirective(Directive::SCRIPT, 'https://instant.page')

            ->addDirective(Directive::STYLE, Keyword::SELF)
            ->addDirective(Directive::STYLE, Keyword::UNSAFE_INLINE)
            ->addDirective(Directive::STYLE, 'https://*.googleapis.com')

            ->addDirective(Directive::FRAME, 'https://*.googletagmanager.com')
            ->addDirective(Directive::FRAME, 'https://*.hotjar.com')
            ->addDirective(Directive::FRAME, 'https://*.facebook.com')
            ->addDirective(Directive::FRAME, 'https://*.twitter.com')
            ->addDirective(Directive::FRAME, 'https://cdn.yoshki.com')

            ->addDirective(Directive::FONT, Keyword::SELF)
            ->addDirective(Directive::FONT, 'data:')
            ->addDirective(Directive::FONT, 'https://*.gstatic.com')
            ->addDirective(Directive::FONT, 'https://*.googleapis.com');

        $slug = ltrim(str_replace(route('theme'), '', request()->url()), '/');
        if (stripos($slug, '?') !== false) {
            $slug = substr($slug, 0, stripos($slug, '?'));
        }
        $Url = \App\Models\Url::where('url', $slug)->first();
        if ($Url && $Url->route && $Url->route == 'controller=page&method=contact') {
            $this->addDirective(Directive::SCRIPT, Keyword::UNSAFE_EVAL);
        }
    }
}