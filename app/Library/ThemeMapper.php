<?php
namespace App\Library;

class ThemeMapper {

    private static $_MAPPED_SETTINGS = [];

    public static function getMappedSettings( $section ){
        self::_mapSettings();

        return isset( self::$_MAPPED_SETTINGS[ $section ] ) ? self::$_MAPPED_SETTINGS[ $section ] : [];
    }

    private static function _mapSettings(){
        if( !empty( self::$_MAPPED_SETTINGS ) ){ return; }

        $map_file = realpath( base_path( 'resources/theme/' . config( 'app.theme_name' ) . '/theme-map.json' ) );
        if( !file_exists( $map_file ) || !is_file( $map_file ) ){ return; }

        $map_contents = @file_get_contents( $map_file );
        if( empty( $map_contents ) ){ return; }

        $map_json = @json_decode( $map_contents, true );
        if( empty( $map_json ) ){ return; }

        self::$_MAPPED_SETTINGS = $map_json;
    }

}