<?php
namespace App\Library;

use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Cache;

/**
 * Class CachingQueryBuilder
 * @package App\Library
 *
 * The Caching Query Builder is an extension of the Laravel Builder, that allows the caching of SQL queries
 */
class CachingQueryBuilder extends Builder {

    /**
     * Override of the get() function.
     *
     * When performing a get() function on an eloquent model, check if there is a cached query, and serve that if found.
     * Else, perform the query and then cache it.
     *
     * @param array $columns
     * @return \Illuminate\Support\Collection
     */
    public function get( $columns = ['*'] ){

        if( !config('database.cache') ){
            return parent::get( $columns );
        }

        $cacheKey = hash( 'sha256', 'cached_query.' . $this->connection->getName() . $this->toSql() . serialize( $this->getBindings() ) );

        if( !Cache::has( $cacheKey ) ){
            $results = parent::get( $columns );

            Cache::put( $cacheKey, $results, now()->addMinutes( config('app.cache_time') ) );
        }

        return Cache::get( $cacheKey );
    }
}