<?php
/**
 * Created by PhpStorm.
 * User: santo
 * Date: 01/09/2015
 * Time: 15:15
 */

namespace App\Library;

use Spatie\Analytics\AnalyticsFacade as Analytics;
use Spatie\Analytics\Period;
use Carbon\Carbon;

/**
 * Class CmsAnalytics
 * @package App\Library
 *
 * This Library is used by the CMS to process Google Analytics data, and format it in a way it can be visualised
 */
class CmsAnalytics {
    /**
     * Returns Google Analytics data for the last 30 days.
     *
     * @return array
     */
    public function getData(){
        list( $startDate, $midPoint, $endDate ) = $this->calculateNumberOfDays( 30 );

        $previousMetrics = ['ga:goalConversionRateAll'];
        $currentMetrics = ['ga:sessions', 'ga:goalValueAll', 'ga:goalConversionRateAll'];

        $previousPeriod = Analytics::performQuery(
            new Period( $startDate, $midPoint ),
            'ga:sessions',
            [
                'metrics'       => implode(',', $previousMetrics),
                'dimensions'    => 'ga:date'
            ]
        );
        $currentPeriod = Analytics::performQuery(
            new Period( $midPoint, $endDate ),
            'ga:sessions',
            [
                'metrics'       => implode(',', $currentMetrics),
                'dimensions'    => 'ga:date'
            ]
        );

        if( is_null( $currentPeriod->rows ) ){
            return [];
        }

        $session_data = [];
        $conversion_data = [];

        foreach( $currentPeriod->rows as $dateRow ){
            $this_date = Carbon::createFromFormat( 'Ymd', $dateRow[0], null )->timestamp * 1000;

            $session_data[] = [$this_date, $dateRow[1]];
            $conversion_data[] = [$this_date, $dateRow[2]];
        }

        $conversionRate = number_format( $currentPeriod->totalsForAllResults['ga:goalConversionRateAll'], 2 );
        $prevConversionRate = number_format( $previousPeriod->totalsForAllResults['ga:goalConversionRateAll'], 2 );

        // Handle Goals
        $goal_ids = config( 'cms-analytics.goal_ids' );
        $goal_labels = config( 'cms-analytics.goal_labels' );
        $goals = [];
        foreach( $goal_ids as $index => $goal_id ){
            if( $goal_id && $goal_labels[ $index ] ){
                $goals[ $goal_id ] = $goal_labels[ $index ];
            }
        }

        $goal_data = [];
        if( !empty( $goals ) ){
            $goals = array_slice( $goals, 0, 4, true ); // Limit the goals to track to 4.

            foreach( $goals as $goal_id => $name ){
                $previousMetrics = [];
                $currentMetrics = [];

                $previousMetrics[] = 'ga:goal' . $goal_id . 'ConversionRate';

                $currentMetrics[] = 'ga:goal' . $goal_id . 'ConversionRate';
                $currentMetrics[] = 'ga:goal' . $goal_id . 'Value';
                $currentMetrics[] = 'ga:goal' . $goal_id . 'Completions';

                $previous = Analytics::performQuery(
                    new Period( $startDate, $midPoint ),
                    'ga:sessions',
                    [
                        'metrics'       => implode(',', $previousMetrics),
                        'dimensions'    => 'ga:date'
                    ]
                );
                $current = Analytics::performQuery(
                    new Period( $midPoint, $endDate ),
                    'ga:sessions',
                    [
                        'metrics'       => implode(',', $currentMetrics),
                        'dimensions'    => 'ga:date'
                    ]
                );

                $goal_data[ $goal_id ] = [
                    'name'      => $name,
                    'previous'  => number_format( $previous->totalsForAllResults['ga:goal' . $goal_id . 'ConversionRate'], 2 ),
                    'current'   => number_format( $current->totalsForAllResults['ga:goal' . $goal_id . 'ConversionRate'], 2 ),
                    'count'     => (int)$current->totalsForAllResults['ga:goal' . $goal_id . 'Completions'],
                    'value'     => number_format( $current->totalsForAllResults['ga:goal' . $goal_id . 'Value'], 2)
                ];
            }
        }

        return [
            'prevConversionRate'    => $prevConversionRate,
            'conversionRate'        => $conversionRate,
            'session_data'          => $session_data,
            'conversion_data'       => $conversion_data,
            'goal_data'             => $goal_data
        ];

    }

    /**
     * Based on the number of days provided, return a list of:
     * Yesterday, $numberOfDays ago, and ($numberOfDays * 2) ago.
     *
     * @param $numberOfDays
     * @return array
     */
    private function calculateNumberOfDays($numberOfDays){
        $endDate = Carbon::yesterday();
        $midDate = Carbon::yesterday()->subDays($numberOfDays);
        $startDate = Carbon::yesterday()->subDays(($numberOfDays*2));

        return [$startDate, $midDate, $endDate];
    }

}