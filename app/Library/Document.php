<?php
/**
 * Created by PhpStorm.
 * User: santo
 * Date: 10/08/2015
 * Time: 09:02
 */

namespace App\Library;

class Document {

    /**
     * Internal array containing all dynamic scripts to use on the current page.
     *
     * @var array
     */
    private static $dynamic_scripts = [];

    /**
     * Internal Array containing all dynamic styles to use on the current page.
     *
     * @var array
     */
    private static $dynamic_styles = [];

    /**
     * Internal array containing all common text translations for the current language, to save on volume of DB calls.
     *
     * @var array
     */
    private static $translations = [];

    /**
     * Add Dynamic Script.
     * Adds a dynamic script to the current page.
     *
     * @param $script - The blade to load
     * @param array $data - Optional data array to pass to the blade
     * @throws \Exception
     * @throws \Throwable
     */
    public static function addDynamicScript( $script, $data = array() ){
        $script_hash = md5( $script );
        if( !isset( self::$dynamic_scripts[ $script_hash ] ) ) {

            $theme_name = config('app.theme_name');
            // If we have a custom theme set
            if( !empty( $theme_name ) ){
                // Get the current 'view finder' to use after we've reset this to include previously namespaced view paths
                $currentViewFinder = \View::getFinder();

                // Now, register the laravel 'view finder' to read from our theme directory
                $themeFinder = new \Illuminate\View\FileViewFinder(app()['files'], array(realpath(base_path('resources/theme/' . $theme_name . '/views'))));
                \View::setFinder($themeFinder);

                // And after that, re-register the previous namespaced view paths from the previous view finder
                foreach( $currentViewFinder->getHints() as $namespace => $paths ){
                    view()->addNamespace( $namespace, $paths );
                }
            }

            $script_view = view( $script, $data );
            self::$dynamic_scripts[md5($script)] = $script_view->render();
        }
    }

    /**
     * Gets all assigned dynamic scripts.
     *
     * @return array
     */
    public static function getDynamicScripts(){
        return self::$dynamic_scripts;
    }

    /**
     * Add Dynamic Style.
     * Adds a dynamic style to the current page.
     *
     * @param $style - The blade to load
     * @param array $data - Optional data array to pass to the blade
     * @throws \Exception
     * @throws \Throwable
     */
    public static function addDynamicStyle( $style, $data = array() ){
        $style_hash = md5( $style );
        if( !isset( self::$dynamic_styles[ $style_hash ] ) ) {

            $theme_name = config('app.theme_name');
            // If we have a custom theme set
            if( !empty( $theme_name ) ){
                // Get the current 'view finder' to use after we've reset this to include previously namespaced view paths
                $currentViewFinder = \View::getFinder();

                // Now, register the laravel 'view finder' to read from our theme directory
                $themeFinder = new \Illuminate\View\FileViewFinder(app()['files'], array(realpath(base_path('resources/theme/' . $theme_name . '/views'))));
                \View::setFinder($themeFinder);

                // And after that, re-register the previous namespaced view paths from the previous view finder
                foreach( $currentViewFinder->getHints() as $namespace => $paths ){
                    view()->addNamespace( $namespace, $paths );
                }
            }

            $style_view = view( $style, $data );
            self::$dynamic_styles[md5($style)] = $style_view->render();
        }
    }

    /**
     * Gets all assigned dynamic styles.
     *
     * @return array
     */
    public static function getDynamicStyles(){
        return self::$dynamic_styles;
    }

    /**
     * Translates a common text string 'key' into a translated variable based on the current language.
     *
     * @param $string_key
     * @return string
     */
    public static function translate_common_text( $string_key ){
        if( isset( self::$translations[ $string_key ] ) ){
            return self::$translations[ $string_key ];
        }

        $available_modules = (array)config('modules.available');

        if( !in_array( 'TextTranslation', $available_modules ) ){ return ''; }
        if( !class_exists( '\\Modules\\TextTranslation\\Theme\\Models\\TranslationString' ) ){ return ''; }

        $strings = \Modules\TextTranslation\Theme\Models\TranslationString::select("key")->get();
        foreach( $strings as $string ){
            self::$translations[ $string->key ] = $string->getTranslation();
        }

        return isset( self::$translations[ $string_key ] ) ? self::$translations[ $string_key ] : '';
    }
}