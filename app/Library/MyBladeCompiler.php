<?php
namespace App\Library;

use Illuminate\View\Compilers\BladeCompiler;

/**
 * Class MyBladeCompiler
 * @package App\Library
 *
 * Custom Blade Compiler allowing us to prevent blade caching
 */
class MyBladeCompiler extends BladeCompiler {

    /**
     * Override function allowing us to prevent the application from caching blades, always saying that they have
     * expired.
     *
     * @param string $path
     * @return bool
     */
    public function isExpired( $path ){
        if( !config('view.cache') ){
            return true;
        }

        return parent::isExpired( $path );
    }
}