<?php

namespace App\Library;

use Illuminate\Support\Facades\Config as Config;

use Illuminate\Support\Facades\Cache;

/**
 * Class MyBespokeApi
 * @package App\Library
 *
 * Class used for interaction with the MyBespoke API (http://myapi.bespokedigital.agency)
 */
class MyBespokeApi {

    /**
     * The Authorisation token provided by the API.
     *
     * @var string
     */
    protected $authorization;

    /**
     * Whether the API service is enabled or not.
     * Toggled when the API finds an OAuth token from the configuration.
     *
     * @var bool
     */
    protected $enabled = false;

    /**
     * The API endpoint to communicate with.
     *
     * @var string
     */
    protected $api_url = "http://myapi.bespokedigital.agency/api/";

    /**
     * How long should results be cached for before being refreshed.
     *
     * @var int
     */
    private $cache_time_in_minutes = 60 * 24; // Cache for 24 hours

    /**
     * Constructor.
     * When the class is loaded, retrieve the auth token from the config file, and if found, enable the API.
     */
    public function __construct(){
        $this->authorization = Config::get('my-bespoke-api.auth_token');
        if( !empty( $this->authorization ) ){
            $this->enabled = true;
        }
    }

    /**
     * Gets the Support Tier for the application's Organisation in MyBespoke, based on the ID set in the ENV file.
     *
     * @return array|bool|\Illuminate\Http\JsonResponse
     */
    public function getOrganisationSupportTier(){
        // If we don't have an organisation or a sitedocs (site) ID set up in the application config, return nothing
        if ( !Config::get('my-bespoke-api.organisation_id') || !Config::get('my-bespoke-api.sitedocs_id') || !$this->enabled ) {
            return [];
        }

        $cache_type = 'organisation-support-tier-' . Config::get('my-bespoke-api.organisation_id') . '-' . Config::get('my-bespoke-api.sitedocs_id');

        // If we've already cached the organisation's support tier...
        if( $this->_checkCache( $cache_type ) ){
            // ... retrieve it
            return $this->_getCache( $cache_type );
        } else {

            // If we have a sitedocs ID set, we can check that.
            if( Config::get('my-bespoke-api.sitedocs_id') ) {

                // So, get it from the API
                $sitedocs_entries = $this->call('organisation_to_sitedocs/sitedocs_id/' . Config::get('my-bespoke-api.sitedocs_id'));
                if (!is_array($sitedocs_entries)) {
                    $sitedocs_entries = array($sitedocs_entries);
                }

                // In some cases, a site belongs to multiple organisations, so we need to check each one
                foreach ($sitedocs_entries as $sitedocs_entry) {

                    if( !is_object( $sitedocs_entry ) ){ continue; }
                    if( !isset( $sitedocs_entry->organisation_id ) ){ continue; }

                    // If the website to organisation link is for the correct organisation, and a specific support tier
                    // ID is set, use that
                    if ($sitedocs_entry->organisation_id == Config::get('my-bespoke-api.organisation_id') && isset($sitedocs_entry->support_tier_id) && !empty($sitedocs_entry->support_tier_id)) {

                        // Then get the support tier from the API
                        $support_tier = $this->call('support_tier/' . $sitedocs_entry->support_tier_id);

                        if ($support_tier) {

                            // ... and if we get a valid response, cache it
                            $this->_cacheResults($cache_type, $support_tier);

                            return $support_tier;
                        }
                    }
                }
            }

            // ... else, call the API...
            $organisation = $this->call( 'organisation/' . Config::get('my-bespoke-api.organisation_id') );

            if ( $organisation && isset($organisation->support_tier) ) {
                $support_tier = $this->call( 'support_tier/' . $organisation->support_tier );
                if ( $support_tier ) {

                    // ... and if we get a valid response, cache it
                    $this->_cacheResults( $cache_type, $support_tier );

                    return $support_tier;
                }
            }
        }

        // ... else, there's no point it calling the API every time so cache an empty value
        $this->_cacheResults( $cache_type, false );
        return false;

    }

    /**
     * Gets the Relationship Manager for the application's Organisation in MyBespoke, based on the ID set in the ENV file.
     *
     * @return array|bool|\Illuminate\Http\JsonResponse
     */
    public function getRelationshipManager(){
        // If we don't have an organisation ID set up in the application config, return nothing
        if ( !Config::get('my-bespoke-api.organisation_id') || !$this->enabled ) {
            return [];
        }

        $cache_type = 'relationship-manager-' . Config::get('my-bespoke-api.organisation_id');

        // If we've already cached the relationship manager...
        if( $this->_checkCache( $cache_type ) ){
            // ... retrieve it
            return $this->_getCache( $cache_type );
        } else {
            // ... else, call the API...
            $organisation = $this->call( 'organisation/' . Config::get('my-bespoke-api.organisation_id') );

            if ( $organisation && isset($organisation->relationship_manager) ) {
                $relationship_manager = $this->call( 'user/' . $organisation->relationship_manager );
                if ( $relationship_manager ) {
                    $relationship_manager->full_name = $relationship_manager->first_name . ' ' . $relationship_manager->last_name;

                    // ... and if we get a valid response, cache it
                    $this->_cacheResults( $cache_type, $relationship_manager );

                    return $relationship_manager;
                }
            }
        }

        // ... else, there's no point it calling the API every time so cache an empty value
        $this->_cacheResults( $cache_type, false );
        return false;
    }

    /**
     * Gets the set of messages to display on the CMS Dashboard from MyBespoke.
     *
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function getCmsMessages(){
        if( !$this->enabled ){
            return false;
        }

        $cache_type = 'cms-messages';

        // If we've already cached some CMS messages...
        if( $this->_checkCache( $cache_type ) ){
            // ... retrieve them
            return $this->_getCache( $cache_type );
        } else {
            // ... else, call the API...
            $cms_messages = $this->call( 'cms_message' );

            if ( $cms_messages ) {

                // ... and if we get a valid response, cache them for 15 minutes
                $this->_cacheResults( $cache_type, $cms_messages, 15 );

                return $cms_messages;
            }
        }

        return false;
    }

    /**
     * Returns any cached results for the MyBespokeAPI Class.
     *
     * @return array|bool|\Illuminate\Http\JsonResponse|mixed
     */
    public function getCmsDefaults(){
        if( !$this->enabled ){
            return false;
        }

        $cache_type = 'cms-defaults';

        // If we've already cached the CMS defaults...
        if( $this->_checkCache( $cache_type ) ){
            // ... retrieve them
            return $this->_getCache( $cache_type );
        } else {
            // ... else, call the API...
            $cms_config = $this->call( 'cms_config' );

            if ( $cms_config && is_array($cms_config) ) {
                $cms_config = reset($cms_config);

                // ... and if we get a valid response, cache it
                $this->_cacheResults( $cache_type, $cms_config );

                return $cms_config;
            }
        }

        // ... else, there's no point it calling the API every time so cache an empty value
        $this->_cacheResults( $cache_type, false );
        return false;
    }

    /**
     * Accepts a $request param which appends this to the api_url
     * using the authorization key defined
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function call($request){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->api_url . $request);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->authorization, 'Accept: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result);
    }

    /**
     * Centralised function used to generate a cache key, keeping things consistent within the class.
     *
     * @param $cache_type
     * @return string
     */
    private function _generateCacheKey( $cache_type ){
        return 'myapi-bespokeinternet-' . \Illuminate\Support\Str::slug( $cache_type );
    }

    /**
     * Centralised function used to check if the provided cache type is available, or needs to be generated again.
     *
     * @param $cache_type
     * @return mixed
     */
    private function _checkCache( $cache_type ){
        $cache_key = $this->_generateCacheKey( $cache_type );

        return Cache::has( $cache_key );
    }

    /**
     * Centralised function used to get the cached value, based on the provided type.
     *
     * @param $cache_type
     * @return mixed
     */
    private function _getCache( $cache_type ){
        $cache_key = $this->_generateCacheKey( $cache_type );

        return Cache::get( $cache_key );
    }

    /**
     * Centralised function used to cache results from API calls.
     *
     * @param $cache_type
     * @param $results
     * @param bool|false $ttl
     */
    private function _cacheResults( $cache_type, $results, $ttl = false ){
        $cache_key = $this->_generateCacheKey( $cache_type );

        $cache_time = $ttl !== false ? $ttl : $this->cache_time_in_minutes;
        Cache::put( $cache_key, $results, now()->addMinutes( $cache_time ) );
    }
}